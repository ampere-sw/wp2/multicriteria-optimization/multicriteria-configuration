// This code is auto-generated

#include <chrono>
#include <memory>

#include "rclcpp/rclcpp.hpp"
#include "std_msgs/msg/string.hpp"

#include "runnables.hpp"

using namespace std::chrono_literals;
using std::placeholders::_1;
#define CONSOLE_ENABLED

#if defined(_OPENMP)
int check_Node_Offloaded_CAMERA_Processing(int *original, int *replicated){ return 1;}
#endif
class Node_Offloaded_CAMERA_Processing : public rclcpp::Node
{
	private:
		rclcpp::Subscription<std_msgs::msg::String>::SharedPtr CAMERA_frame_subscription_;
		rclcpp::Publisher<std_msgs::msg::String>::SharedPtr Bounding_box_in_VBF_coord_publisher;

	public:
		Node_Offloaded_CAMERA_Processing()
		: Node("node_offloaded_camera_processing")
		{
			CAMERA_frame_subscription_ = this->create_subscription<std_msgs::msg::String>(
				"camera_frame", rclcpp::QoS(10), std::bind(&Node_Offloaded_CAMERA_Processing::CAMERA_frame_subscription_callback, this, _1));
			Bounding_box_in_VBF_coord_publisher = this->create_publisher<std_msgs::msg::String>("bounding_box_in_vbf_coord", 10);
		}
	void CAMERA_frame_subscription_callback(const std_msgs::msg::String::SharedPtr msg) {
	#ifdef CONSOLE_ENABLED
		std::cout << "ROS2: subscription callback " << msg->data << std::endl ;
	#endif
		#if defined(_OPENMP)
		 #pragma omp taskgraph tdg_type(static) nowait
		#endif
		{
		#if defined(_OPENMP)
		 #pragma omp task
		#endif
		run_CAMERA_object_detection();
		#if defined(_OPENMP)
		#pragma omp task
		#endif
		run_Homography_transformation(Bounding_box_in_VBF_coord_publisher);
			}
		#pragma omp taskwait;
	}
	
	
};
