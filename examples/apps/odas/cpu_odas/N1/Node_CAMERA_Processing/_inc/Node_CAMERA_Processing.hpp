// This code is auto-generated

#include <chrono>
#include <memory>

#include "rclcpp/rclcpp.hpp"
#include "std_msgs/msg/string.hpp"

#include "runnables.hpp"

using namespace std::chrono_literals;
using std::placeholders::_1;
#define CONSOLE_ENABLED

#if defined(_OPENMP)
int check_Node_CAMERA_Processing(int *original, int *replicated){ return 1;}
#endif
class Node_CAMERA_Processing : public rclcpp::Node
{
	private:
		rclcpp::TimerBase::SharedPtr timer_CAMERA_Processing_40_ms_;
		rclcpp::Publisher<std_msgs::msg::String>::SharedPtr CAMERA_frame_publisher;

	public:
		Node_CAMERA_Processing()
		: Node("node_camera_processing")
		{
			timer_CAMERA_Processing_40_ms_ = this->create_wall_timer(
					40ms, std::bind(&Node_CAMERA_Processing::timer_CAMERA_Processing_40_ms_callback, this));
			CAMERA_frame_publisher = this->create_publisher<std_msgs::msg::String>("camera_frame", 10);
		}
	void timer_CAMERA_Processing_40_ms_callback() {
	#ifdef CONSOLE_ENABLED
		std::cout << "Timer_CAMERA_Processing_40_ms_callback (40ms)" << std::endl;
	#endif
		run_ETH_CAMERA_raw_data_management(CAMERA_frame_publisher);
	}
	
	
};
