// This code is auto-generated

#pragma once 

extern void executeGPUTicks(long long average,long long lowerBound, long long upperBound);
extern void executeGPUTicksConstant(long long ticks);
#include "ticksUtils.hpp"
#include "labels.hpp"

// Runnable CAN_msgs_management----
void run_CAN_msgs_management();
#include "labels.hpp"
#include "ticksUtils.hpp"

// Runnable RADAR_spatial_synchronization----
void run_RADAR_spatial_synchronization();
#include "labels.hpp"
#include "ticksUtils.hpp"
#include "channelSendUtils.hpp"

// Runnable RADAR_object_detection----
void run_RADAR_object_detection(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& RADAR_objects_in_VBF_coord_publisher);
#include "ticksUtils.hpp"
#include "channelSendUtils.hpp"
#include "labels.hpp"

// Runnable ETH_CAMERA_raw_data_management----
void run_ETH_CAMERA_raw_data_management(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& CAMERA_frame_publisher);
#include "ticksUtils.hpp"
#include "labels.hpp"

// Runnable ETH_LiDAR_msgs_management----
void run_ETH_LiDAR_msgs_management();
#include "labels.hpp"
#include "ticksUtils.hpp"

// Runnable LiDAR_spatial_synchronization----
void run_LiDAR_spatial_synchronization();
#include "labels.hpp"
#include "ticksUtils.hpp"
#include "channelSendUtils.hpp"

// Runnable LiDAR_object_detection----
void run_LiDAR_object_detection(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& LiDAR_objects_in_VBF_coord_publisher);
#include "ticksUtils.hpp"
#include "labels.hpp"
void run_CAMERA_object_detection_GPU();
#pragma omp declare variant(run_CAMERA_object_detection_GPU) match(construct={target}) 

// Runnable CAMERA_object_detection----
void run_CAMERA_object_detection();
#include "labels.hpp"
#include "ticksUtils.hpp"
#include "channelSendUtils.hpp"
void run_Homography_transformation_GPU(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Bounding_box_in_VBF_coord_publisher);
#pragma omp declare variant(run_Homography_transformation_GPU) match(construct={target}) 

// Runnable Homography_transformation----
void run_Homography_transformation(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Bounding_box_in_VBF_coord_publisher);
#include "ticksUtils.hpp"
#include "labels.hpp"

// Runnable Time_synchronization----
void run_Time_synchronization();
#include "labels.hpp"
#include "ticksUtils.hpp"

// Runnable Data_association----
void run_Data_association();
#include "labels.hpp"
#include "ticksUtils.hpp"
#include "channelSendUtils.hpp"

// Runnable Track_management----
void run_Track_management(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Tracked_object_label_N_publisher);
#include "ticksUtils.hpp"
#include "labels.hpp"

// Runnable UKF_management----
void run_UKF_management();
#include "labels.hpp"
#include "ticksUtils.hpp"
#include "channelSendUtils.hpp"

// Runnable UKF_00----
void run_UKF_00(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Potential_obstacle_N00_publisher, rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Predicted_track_00_publisher);
#include "labels.hpp"
#include "ticksUtils.hpp"
#include "channelSendUtils.hpp"

// Runnable UKF_01----
void run_UKF_01(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Potential_obstacle_N01_publisher, rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Predicted_track_01_publisher);
#include "labels.hpp"
#include "ticksUtils.hpp"
#include "channelSendUtils.hpp"

// Runnable UKF_02----
void run_UKF_02(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Potential_obstacle_N02_publisher, rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Predicted_track_02_publisher);
#include "labels.hpp"
#include "ticksUtils.hpp"
#include "channelSendUtils.hpp"

// Runnable UKF_03----
void run_UKF_03(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Potential_obstacle_N03_publisher, rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Predicted_track_03_publisher);
#include "labels.hpp"
#include "ticksUtils.hpp"
#include "channelSendUtils.hpp"

// Runnable UKF_04----
void run_UKF_04(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Potential_obstacle_N04_publisher, rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Predicted_track_04_publisher);
#include "labels.hpp"
#include "ticksUtils.hpp"
#include "channelSendUtils.hpp"

// Runnable UKF_05----
void run_UKF_05(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Potential_obstacle_N05_publisher, rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Predicted_track_05_publisher);
#include "labels.hpp"
#include "ticksUtils.hpp"
#include "channelSendUtils.hpp"

// Runnable UKF_06----
void run_UKF_06(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Potential_obstacle_N06_publisher, rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Predicted_track_06_publisher);
#include "labels.hpp"
#include "ticksUtils.hpp"
#include "channelSendUtils.hpp"

// Runnable UKF_07----
void run_UKF_07(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Potential_obstacle_N07_publisher, rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Predicted_track_07_publisher);
#include "labels.hpp"
#include "ticksUtils.hpp"
#include "channelSendUtils.hpp"

// Runnable UKF_08----
void run_UKF_08(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Potential_obstacle_N08_publisher, rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Predicted_track_08_publisher);
#include "labels.hpp"
#include "ticksUtils.hpp"
#include "channelSendUtils.hpp"

// Runnable UKF_09----
void run_UKF_09(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Potential_obstacle_N09_publisher, rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Predicted_track_09_publisher);
#include "labels.hpp"
#include "ticksUtils.hpp"
#include "channelSendUtils.hpp"

// Runnable UKF_10----
void run_UKF_10(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Potential_obstacle_N10_publisher, rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Predicted_track_10_publisher);
#include "labels.hpp"
#include "ticksUtils.hpp"
#include "channelSendUtils.hpp"

// Runnable UKF_11----
void run_UKF_11(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Potential_obstacle_N11_publisher, rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Predicted_track_11_publisher);
#include "labels.hpp"
#include "ticksUtils.hpp"
#include "channelSendUtils.hpp"

// Runnable UKF_12----
void run_UKF_12(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Potential_obstacle_N12_publisher, rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Predicted_track_12_publisher);
#include "labels.hpp"
#include "ticksUtils.hpp"
#include "channelSendUtils.hpp"

// Runnable UKF_13----
void run_UKF_13(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Potential_obstacle_N13_publisher, rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Predicted_track_13_publisher);
#include "labels.hpp"
#include "ticksUtils.hpp"
#include "channelSendUtils.hpp"

// Runnable UKF_14----
void run_UKF_14(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Potential_obstacle_N14_publisher, rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Predicted_track_14_publisher);
#include "labels.hpp"
#include "ticksUtils.hpp"
#include "channelSendUtils.hpp"

// Runnable UKF_15----
void run_UKF_15(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Potential_obstacle_N15_publisher, rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Predicted_track_15_publisher);
#include "labels.hpp"
#include "ticksUtils.hpp"
#include "channelSendUtils.hpp"

// Runnable UKF_16----
void run_UKF_16(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Potential_obstacle_N16_publisher, rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Predicted_track_16_publisher);
#include "labels.hpp"
#include "ticksUtils.hpp"
#include "channelSendUtils.hpp"

// Runnable UKF_17----
void run_UKF_17(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Potential_obstacle_N17_publisher, rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Predicted_track_17_publisher);
#include "labels.hpp"
#include "ticksUtils.hpp"
#include "channelSendUtils.hpp"

// Runnable UKF_18----
void run_UKF_18(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Potential_obstacle_N18_publisher, rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Predicted_track_18_publisher);
#include "labels.hpp"
#include "ticksUtils.hpp"
#include "channelSendUtils.hpp"

// Runnable UKF_19----
void run_UKF_19(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Potential_obstacle_N19_publisher, rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Predicted_track_19_publisher);
#include "labels.hpp"
#include "ticksUtils.hpp"
#include "channelSendUtils.hpp"

// Runnable UKF_20----
void run_UKF_20(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Potential_obstacle_N20_publisher, rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Predicted_track_20_publisher);
#include "labels.hpp"
#include "ticksUtils.hpp"
#include "channelSendUtils.hpp"

// Runnable UKF_21----
void run_UKF_21(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Potential_obstacle_N21_publisher, rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Predicted_track_21_publisher);
#include "labels.hpp"
#include "ticksUtils.hpp"
#include "channelSendUtils.hpp"

// Runnable UKF_22----
void run_UKF_22(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Potential_obstacle_N22_publisher, rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Predicted_track_22_publisher);
#include "labels.hpp"
#include "ticksUtils.hpp"
#include "channelSendUtils.hpp"

// Runnable UKF_23----
void run_UKF_23(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Potential_obstacle_N23_publisher, rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Predicted_track_23_publisher);
#include "labels.hpp"
#include "ticksUtils.hpp"
#include "channelSendUtils.hpp"

// Runnable UKF_24----
void run_UKF_24(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Potential_obstacle_N24_publisher, rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Predicted_track_24_publisher);
#include "labels.hpp"
#include "ticksUtils.hpp"
#include "channelSendUtils.hpp"

// Runnable UKF_25----
void run_UKF_25(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Potential_obstacle_N25_publisher, rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Predicted_track_25_publisher);
#include "labels.hpp"
#include "ticksUtils.hpp"
#include "channelSendUtils.hpp"

// Runnable UKF_26----
void run_UKF_26(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Potential_obstacle_N26_publisher, rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Predicted_track_26_publisher);
#include "labels.hpp"
#include "ticksUtils.hpp"
#include "channelSendUtils.hpp"

// Runnable UKF_27----
void run_UKF_27(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Potential_obstacle_N27_publisher, rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Predicted_track_27_publisher);
#include "labels.hpp"
#include "ticksUtils.hpp"
#include "channelSendUtils.hpp"

// Runnable UKF_28----
void run_UKF_28(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Potential_obstacle_N28_publisher, rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Predicted_track_28_publisher);
#include "labels.hpp"
#include "ticksUtils.hpp"
#include "channelSendUtils.hpp"

// Runnable UKF_29----
void run_UKF_29(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Potential_obstacle_N29_publisher, rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Predicted_track_29_publisher);
#include "labels.hpp"
#include "ticksUtils.hpp"
#include "channelSendUtils.hpp"

// Runnable UKF_30----
void run_UKF_30(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Potential_obstacle_N30_publisher, rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Predicted_track_30_publisher);
#include "labels.hpp"
#include "ticksUtils.hpp"
#include "channelSendUtils.hpp"

// Runnable UKF_31----
void run_UKF_31(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Potential_obstacle_N31_publisher, rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Predicted_track_31_publisher);
#include "labels.hpp"
#include "ticksUtils.hpp"
#include "channelSendUtils.hpp"

// Runnable UKF_32----
void run_UKF_32(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Potential_obstacle_N32_publisher, rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Predicted_track_32_publisher);
#include "labels.hpp"
#include "ticksUtils.hpp"
#include "channelSendUtils.hpp"

// Runnable UKF_33----
void run_UKF_33(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Potential_obstacle_N33_publisher, rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Predicted_track_33_publisher);
#include "labels.hpp"
#include "ticksUtils.hpp"
#include "channelSendUtils.hpp"

// Runnable UKF_34----
void run_UKF_34(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Potential_obstacle_N34_publisher, rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Predicted_track_34_publisher);
#include "labels.hpp"
#include "ticksUtils.hpp"
#include "channelSendUtils.hpp"

// Runnable UKF_35----
void run_UKF_35(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Potential_obstacle_N35_publisher, rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Predicted_track_35_publisher);
#include "labels.hpp"
#include "ticksUtils.hpp"
#include "channelSendUtils.hpp"

// Runnable UKF_36----
void run_UKF_36(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Potential_obstacle_N36_publisher, rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Predicted_track_36_publisher);
#include "labels.hpp"
#include "ticksUtils.hpp"
#include "channelSendUtils.hpp"

// Runnable UKF_37----
void run_UKF_37(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Potential_obstacle_N37_publisher, rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Predicted_track_37_publisher);
#include "labels.hpp"
#include "ticksUtils.hpp"
#include "channelSendUtils.hpp"

// Runnable UKF_38----
void run_UKF_38(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Potential_obstacle_N38_publisher, rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Predicted_track_38_publisher);
#include "labels.hpp"
#include "ticksUtils.hpp"
#include "channelSendUtils.hpp"

// Runnable UKF_39----
void run_UKF_39(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Potential_obstacle_N39_publisher, rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Predicted_track_39_publisher);
#include "labels.hpp"
#include "ticksUtils.hpp"
#include "channelSendUtils.hpp"

// Runnable UKF_40----
void run_UKF_40(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Potential_obstacle_N40_publisher, rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Predicted_track_40_publisher);
#include "labels.hpp"
#include "ticksUtils.hpp"
#include "channelSendUtils.hpp"

// Runnable UKF_41----
void run_UKF_41(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Potential_obstacle_N41_publisher, rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Predicted_track_41_publisher);
#include "labels.hpp"
#include "ticksUtils.hpp"
#include "channelSendUtils.hpp"

// Runnable UKF_42----
void run_UKF_42(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Potential_obstacle_N42_publisher, rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Predicted_track_42_publisher);
#include "labels.hpp"
#include "ticksUtils.hpp"
#include "channelSendUtils.hpp"

// Runnable UKF_43----
void run_UKF_43(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Potential_obstacle_N43_publisher, rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Predicted_track_43_publisher);
#include "labels.hpp"
#include "ticksUtils.hpp"
#include "channelSendUtils.hpp"

// Runnable UKF_44----
void run_UKF_44(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Potential_obstacle_N44_publisher, rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Predicted_track_44_publisher);
#include "labels.hpp"
#include "ticksUtils.hpp"
#include "channelSendUtils.hpp"

// Runnable UKF_45----
void run_UKF_45(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Potential_obstacle_N45_publisher, rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Predicted_track_45_publisher);
#include "labels.hpp"
#include "ticksUtils.hpp"
#include "channelSendUtils.hpp"

// Runnable UKF_46----
void run_UKF_46(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Potential_obstacle_N46_publisher, rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Predicted_track_46_publisher);
#include "labels.hpp"
#include "ticksUtils.hpp"
#include "channelSendUtils.hpp"

// Runnable UKF_47----
void run_UKF_47(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Potential_obstacle_N47_publisher, rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Predicted_track_47_publisher);
#include "labels.hpp"
#include "ticksUtils.hpp"
#include "channelSendUtils.hpp"

// Runnable UKF_48----
void run_UKF_48(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Potential_obstacle_N48_publisher, rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Predicted_track_48_publisher);
#include "labels.hpp"
#include "ticksUtils.hpp"
#include "channelSendUtils.hpp"

// Runnable UKF_49----
void run_UKF_49(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Potential_obstacle_N49_publisher, rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Predicted_track_49_publisher);
#include "labels.hpp"
#include "ticksUtils.hpp"
#include "channelSendUtils.hpp"

// Runnable UKF_50----
void run_UKF_50(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Potential_obstacle_N50_publisher, rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Predicted_track_50_publisher);
#include "labels.hpp"
#include "ticksUtils.hpp"
#include "channelSendUtils.hpp"

// Runnable UKF_51----
void run_UKF_51(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Potential_obstacle_N51_publisher, rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Predicted_track_51_publisher);
#include "labels.hpp"
#include "ticksUtils.hpp"
#include "channelSendUtils.hpp"

// Runnable UKF_52----
void run_UKF_52(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Potential_obstacle_N52_publisher, rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Predicted_track_52_publisher);
#include "labels.hpp"
#include "ticksUtils.hpp"
#include "channelSendUtils.hpp"

// Runnable UKF_53----
void run_UKF_53(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Potential_obstacle_N53_publisher, rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Predicted_track_53_publisher);
#include "labels.hpp"
#include "ticksUtils.hpp"
#include "channelSendUtils.hpp"

// Runnable UKF_54----
void run_UKF_54(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Potential_obstacle_N54_publisher, rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Predicted_track_54_publisher);
#include "labels.hpp"
#include "ticksUtils.hpp"
#include "channelSendUtils.hpp"

// Runnable UKF_55----
void run_UKF_55(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Potential_obstacle_N55_publisher, rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Predicted_track_55_publisher);
#include "labels.hpp"
#include "ticksUtils.hpp"
#include "channelSendUtils.hpp"

// Runnable UKF_56----
void run_UKF_56(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Potential_obstacle_N56_publisher, rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Predicted_track_56_publisher);
#include "labels.hpp"
#include "ticksUtils.hpp"
#include "channelSendUtils.hpp"

// Runnable UKF_57----
void run_UKF_57(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Potential_obstacle_N57_publisher, rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Predicted_track_57_publisher);
#include "labels.hpp"
#include "ticksUtils.hpp"
#include "channelSendUtils.hpp"

// Runnable UKF_58----
void run_UKF_58(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Potential_obstacle_N58_publisher, rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Predicted_track_58_publisher);
#include "labels.hpp"
#include "ticksUtils.hpp"
#include "channelSendUtils.hpp"

// Runnable UKF_59----
void run_UKF_59(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Potential_obstacle_N59_publisher, rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Predicted_track_59_publisher);
#include "ticksUtils.hpp"
#include "labels.hpp"

// Runnable Collision_checking----
void run_Collision_checking();
