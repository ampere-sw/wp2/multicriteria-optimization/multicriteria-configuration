// This code is auto-generated

#pragma once 
#include <string>

#include "rclcpp/rclcpp.hpp"
#include "std_msgs/msg/string.hpp"


void publish_to_RADAR_objects_in_VBF_coord(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher);


void publish_to_CAMERA_frame(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher);


void publish_to_LiDAR_objects_in_VBF_coord(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher);


void publish_to_Bounding_box_in_VBF_coord(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher);


void publish_to_Tracked_object_label_N(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher);


void publish_to_Potential_obstacle_N00(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher);


void publish_to_Predicted_track_00(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher);


void publish_to_Potential_obstacle_N01(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher);


void publish_to_Predicted_track_01(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher);


void publish_to_Potential_obstacle_N02(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher);


void publish_to_Predicted_track_02(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher);


void publish_to_Potential_obstacle_N03(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher);


void publish_to_Predicted_track_03(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher);


void publish_to_Potential_obstacle_N04(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher);


void publish_to_Predicted_track_04(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher);


void publish_to_Potential_obstacle_N05(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher);


void publish_to_Predicted_track_05(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher);


void publish_to_Potential_obstacle_N06(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher);


void publish_to_Predicted_track_06(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher);


void publish_to_Potential_obstacle_N07(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher);


void publish_to_Predicted_track_07(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher);


void publish_to_Potential_obstacle_N08(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher);


void publish_to_Predicted_track_08(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher);


void publish_to_Potential_obstacle_N09(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher);


void publish_to_Predicted_track_09(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher);


void publish_to_Potential_obstacle_N10(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher);


void publish_to_Predicted_track_10(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher);


void publish_to_Potential_obstacle_N11(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher);


void publish_to_Predicted_track_11(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher);


void publish_to_Potential_obstacle_N12(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher);


void publish_to_Predicted_track_12(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher);


void publish_to_Potential_obstacle_N13(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher);


void publish_to_Predicted_track_13(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher);


void publish_to_Potential_obstacle_N14(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher);


void publish_to_Predicted_track_14(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher);


void publish_to_Potential_obstacle_N15(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher);


void publish_to_Predicted_track_15(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher);


void publish_to_Potential_obstacle_N16(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher);


void publish_to_Predicted_track_16(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher);


void publish_to_Potential_obstacle_N17(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher);


void publish_to_Predicted_track_17(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher);


void publish_to_Potential_obstacle_N18(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher);


void publish_to_Predicted_track_18(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher);


void publish_to_Potential_obstacle_N19(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher);


void publish_to_Predicted_track_19(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher);


void publish_to_Potential_obstacle_N20(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher);


void publish_to_Predicted_track_20(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher);


void publish_to_Potential_obstacle_N21(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher);


void publish_to_Predicted_track_21(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher);


void publish_to_Potential_obstacle_N22(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher);


void publish_to_Predicted_track_22(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher);


void publish_to_Potential_obstacle_N23(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher);


void publish_to_Predicted_track_23(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher);


void publish_to_Potential_obstacle_N24(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher);


void publish_to_Predicted_track_24(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher);


void publish_to_Potential_obstacle_N25(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher);


void publish_to_Predicted_track_25(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher);


void publish_to_Potential_obstacle_N26(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher);


void publish_to_Predicted_track_26(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher);


void publish_to_Potential_obstacle_N27(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher);


void publish_to_Predicted_track_27(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher);


void publish_to_Potential_obstacle_N28(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher);


void publish_to_Predicted_track_28(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher);


void publish_to_Potential_obstacle_N29(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher);


void publish_to_Predicted_track_29(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher);


void publish_to_Potential_obstacle_N30(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher);


void publish_to_Predicted_track_30(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher);


void publish_to_Potential_obstacle_N31(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher);


void publish_to_Predicted_track_31(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher);


void publish_to_Potential_obstacle_N32(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher);


void publish_to_Predicted_track_32(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher);


void publish_to_Potential_obstacle_N33(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher);


void publish_to_Predicted_track_33(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher);


void publish_to_Potential_obstacle_N34(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher);


void publish_to_Predicted_track_34(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher);


void publish_to_Potential_obstacle_N35(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher);


void publish_to_Predicted_track_35(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher);


void publish_to_Potential_obstacle_N36(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher);


void publish_to_Predicted_track_36(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher);


void publish_to_Potential_obstacle_N37(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher);


void publish_to_Predicted_track_37(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher);


void publish_to_Potential_obstacle_N38(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher);


void publish_to_Predicted_track_38(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher);


void publish_to_Potential_obstacle_N39(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher);


void publish_to_Predicted_track_39(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher);


void publish_to_Potential_obstacle_N40(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher);


void publish_to_Predicted_track_40(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher);


void publish_to_Potential_obstacle_N41(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher);


void publish_to_Predicted_track_41(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher);


void publish_to_Potential_obstacle_N42(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher);


void publish_to_Predicted_track_42(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher);


void publish_to_Potential_obstacle_N43(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher);


void publish_to_Predicted_track_43(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher);


void publish_to_Potential_obstacle_N44(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher);


void publish_to_Predicted_track_44(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher);


void publish_to_Potential_obstacle_N45(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher);


void publish_to_Predicted_track_45(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher);


void publish_to_Potential_obstacle_N46(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher);


void publish_to_Predicted_track_46(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher);


void publish_to_Potential_obstacle_N47(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher);


void publish_to_Predicted_track_47(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher);


void publish_to_Potential_obstacle_N48(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher);


void publish_to_Predicted_track_48(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher);


void publish_to_Potential_obstacle_N49(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher);


void publish_to_Predicted_track_49(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher);


void publish_to_Potential_obstacle_N50(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher);


void publish_to_Predicted_track_50(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher);


void publish_to_Potential_obstacle_N51(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher);


void publish_to_Predicted_track_51(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher);


void publish_to_Potential_obstacle_N52(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher);


void publish_to_Predicted_track_52(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher);


void publish_to_Potential_obstacle_N53(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher);


void publish_to_Predicted_track_53(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher);


void publish_to_Potential_obstacle_N54(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher);


void publish_to_Predicted_track_54(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher);


void publish_to_Potential_obstacle_N55(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher);


void publish_to_Predicted_track_55(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher);


void publish_to_Potential_obstacle_N56(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher);


void publish_to_Predicted_track_56(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher);


void publish_to_Potential_obstacle_N57(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher);


void publish_to_Predicted_track_57(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher);


void publish_to_Potential_obstacle_N58(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher);


void publish_to_Predicted_track_58(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher);


void publish_to_Potential_obstacle_N59(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher);


void publish_to_Predicted_track_59(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher);

