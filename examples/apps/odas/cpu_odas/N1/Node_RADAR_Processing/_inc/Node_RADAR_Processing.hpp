// This code is auto-generated

#include <chrono>
#include <memory>

#include "rclcpp/rclcpp.hpp"
#include "std_msgs/msg/string.hpp"

#include "runnables.hpp"

using namespace std::chrono_literals;
using std::placeholders::_1;
#define CONSOLE_ENABLED

#if defined(_OPENMP)
int check_Node_RADAR_Processing(int *original, int *replicated){ return 1;}
#endif
class Node_RADAR_Processing : public rclcpp::Node
{
	private:
		rclcpp::TimerBase::SharedPtr timer_RADAR_Processing_92_ms_;
		rclcpp::Publisher<std_msgs::msg::String>::SharedPtr RADAR_objects_in_VBF_coord_publisher;

	public:
		Node_RADAR_Processing()
		: Node("node_radar_processing")
		{
			timer_RADAR_Processing_92_ms_ = this->create_wall_timer(
					92ms, std::bind(&Node_RADAR_Processing::timer_RADAR_Processing_92_ms_callback, this));
			RADAR_objects_in_VBF_coord_publisher = this->create_publisher<std_msgs::msg::String>("radar_objects_in_vbf_coord", 10);
		}
	void timer_RADAR_Processing_92_ms_callback() {
	#ifdef CONSOLE_ENABLED
		std::cout << "Timer_RADAR_Processing_92_ms_callback (92ms)" << std::endl;
	#endif
		extern int RADAR_points[1];
		extern int RADAR_points_in_VBF_coord[1];
		int dummy;
		
		 #if defined(_OPENMP)
		 #pragma omp taskgraph tdg_type(static) nowait
		#endif
		{
		#if defined(_OPENMP)
		 #pragma omp task depend(out:RADAR_points) replicated(2, dummy, check_Node_RADAR_Processing)
		#elif defined(_OMPSS)
		 #pragma oss task depend(out:RADAR_points) replicated(2, dummy, check_Node_RADAR_Processing)
		#endif
		run_CAN_msgs_management();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:RADAR_points) depend(out:RADAR_points_in_VBF_coord) replicated(2, dummy, check_Node_RADAR_Processing)
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:RADAR_points) depend(out:RADAR_points_in_VBF_coord) replicated(2, dummy, check_Node_RADAR_Processing)
		#endif
		run_RADAR_spatial_synchronization();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:RADAR_points_in_VBF_coord) replicated(2, dummy, check_Node_RADAR_Processing)
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:RADAR_points_in_VBF_coord) replicated(2, dummy, check_Node_RADAR_Processing)
		#endif
		run_RADAR_object_detection(RADAR_objects_in_VBF_coord_publisher);
		}
		#pragma omp taskwait;
	}
	
	
};
