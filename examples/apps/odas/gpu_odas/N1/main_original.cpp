#include "Node_CAMERA_Processing.hpp"
#include "Node_CCM.hpp"
#include "Node_LiDAR_Processing.hpp"
#include "Node_Offloaded_CAMERA_Processing.hpp"
#include "Node_RADAR_Processing.hpp"
#include "Node_Sensor_Fusion.hpp"
#include "Node_UKFs.hpp"

int main(int argc, char *argv[])
{
	std::vector<std::thread> threads;

	setvbuf(stdout, NULL, _IONBF, BUFSIZ);
	rclcpp::init(argc, argv);

//EXECUTOR 1

rclcpp::executors::SingleThreadedExecutor executor1;
auto node1 = std::make_shared<Node_CAMERA_Processing>();
executor1.add_node(node1);

auto spin_executor1 = [&executor1]()
{
	executor1.spin();
};
threads.emplace_back(std::thread(spin_executor1));

//EXECUTOR 2

rclcpp::executors::SingleThreadedExecutor executor2;
auto node2 = std::make_shared<Node_CCM>();
executor2.add_node(node2);

auto spin_executor2 = [&executor2]()
{
	#pragma omp parallel 
	#pragma omp single
	executor2.spin();
};
threads.emplace_back(std::thread(spin_executor2));

//EXECUTOR 3

rclcpp::executors::SingleThreadedExecutor executor3;
auto node3 = std::make_shared<Node_LiDAR_Processing>();
executor3.add_node(node3);

auto spin_executor3 = [&executor3]()
{
	#pragma omp parallel 
	#pragma omp single
	executor3.spin();
};
threads.emplace_back(std::thread(spin_executor3));

//EXECUTOR 4

rclcpp::executors::SingleThreadedExecutor executor4;
auto node4 = std::make_shared<Node_Offloaded_CAMERA_Processing>();
executor4.add_node(node4);

auto spin_executor4 = [&executor4]()
{
	executor4.spin();
};
threads.emplace_back(std::thread(spin_executor4));

//EXECUTOR 5

rclcpp::executors::SingleThreadedExecutor executor5;
auto node5 = std::make_shared<Node_RADAR_Processing>();
executor5.add_node(node5);

auto spin_executor5 = [&executor5]()
{
	#pragma omp parallel 
	#pragma omp single
	executor5.spin();
};
threads.emplace_back(std::thread(spin_executor5));

//EXECUTOR 6

rclcpp::executors::SingleThreadedExecutor executor6;
auto node6 = std::make_shared<Node_Sensor_Fusion>();
executor6.add_node(node6);

auto spin_executor6 = [&executor6]()
{
	#pragma omp parallel 
	#pragma omp single
	executor6.spin();
};
threads.emplace_back(std::thread(spin_executor6));

//EXECUTOR 7

rclcpp::executors::SingleThreadedExecutor executor7;
auto node7 = std::make_shared<Node_UKFs>();
executor7.add_node(node7);

auto spin_executor7 = [&executor7]()
{
	#pragma omp parallel 
	#pragma omp single
	executor7.spin();
};
threads.emplace_back(std::thread(spin_executor7));

	//WAITING OF THREADS

	for (auto& th : threads)
		th.join();

	rclcpp::shutdown();

	return 0;
}
