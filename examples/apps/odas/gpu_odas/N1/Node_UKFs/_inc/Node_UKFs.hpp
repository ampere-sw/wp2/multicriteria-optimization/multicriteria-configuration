// This code is auto-generated

#include <chrono>
#include <memory>

#include "rclcpp/rclcpp.hpp"
#include "std_msgs/msg/string.hpp"

#include "runnables.hpp"

using namespace std::chrono_literals;
using std::placeholders::_1;
#define CONSOLE_ENABLED

#if defined(_OPENMP)
int check_Node_UKFs(int *original, int *replicated){ return 1;}
#endif
class Node_UKFs : public rclcpp::Node
{
	private:
		rclcpp::Subscription<std_msgs::msg::String>::SharedPtr Tracked_object_label_N_subscription_;
		rclcpp::Publisher<std_msgs::msg::String>::SharedPtr Potential_obstacle_N00_publisher;
		rclcpp::Publisher<std_msgs::msg::String>::SharedPtr Predicted_track_00_publisher;
		rclcpp::Publisher<std_msgs::msg::String>::SharedPtr Potential_obstacle_N01_publisher;
		rclcpp::Publisher<std_msgs::msg::String>::SharedPtr Predicted_track_01_publisher;
		rclcpp::Publisher<std_msgs::msg::String>::SharedPtr Potential_obstacle_N02_publisher;
		rclcpp::Publisher<std_msgs::msg::String>::SharedPtr Predicted_track_02_publisher;
		rclcpp::Publisher<std_msgs::msg::String>::SharedPtr Potential_obstacle_N03_publisher;
		rclcpp::Publisher<std_msgs::msg::String>::SharedPtr Predicted_track_03_publisher;
		rclcpp::Publisher<std_msgs::msg::String>::SharedPtr Potential_obstacle_N04_publisher;
		rclcpp::Publisher<std_msgs::msg::String>::SharedPtr Predicted_track_04_publisher;
		rclcpp::Publisher<std_msgs::msg::String>::SharedPtr Potential_obstacle_N05_publisher;
		rclcpp::Publisher<std_msgs::msg::String>::SharedPtr Predicted_track_05_publisher;
		rclcpp::Publisher<std_msgs::msg::String>::SharedPtr Potential_obstacle_N06_publisher;
		rclcpp::Publisher<std_msgs::msg::String>::SharedPtr Predicted_track_06_publisher;
		rclcpp::Publisher<std_msgs::msg::String>::SharedPtr Potential_obstacle_N07_publisher;
		rclcpp::Publisher<std_msgs::msg::String>::SharedPtr Predicted_track_07_publisher;
		rclcpp::Publisher<std_msgs::msg::String>::SharedPtr Potential_obstacle_N08_publisher;
		rclcpp::Publisher<std_msgs::msg::String>::SharedPtr Predicted_track_08_publisher;
		rclcpp::Publisher<std_msgs::msg::String>::SharedPtr Potential_obstacle_N09_publisher;
		rclcpp::Publisher<std_msgs::msg::String>::SharedPtr Predicted_track_09_publisher;
		rclcpp::Publisher<std_msgs::msg::String>::SharedPtr Potential_obstacle_N10_publisher;
		rclcpp::Publisher<std_msgs::msg::String>::SharedPtr Predicted_track_10_publisher;
		rclcpp::Publisher<std_msgs::msg::String>::SharedPtr Potential_obstacle_N11_publisher;
		rclcpp::Publisher<std_msgs::msg::String>::SharedPtr Predicted_track_11_publisher;
		rclcpp::Publisher<std_msgs::msg::String>::SharedPtr Potential_obstacle_N12_publisher;
		rclcpp::Publisher<std_msgs::msg::String>::SharedPtr Predicted_track_12_publisher;
		rclcpp::Publisher<std_msgs::msg::String>::SharedPtr Potential_obstacle_N13_publisher;
		rclcpp::Publisher<std_msgs::msg::String>::SharedPtr Predicted_track_13_publisher;
		rclcpp::Publisher<std_msgs::msg::String>::SharedPtr Potential_obstacle_N14_publisher;
		rclcpp::Publisher<std_msgs::msg::String>::SharedPtr Predicted_track_14_publisher;
		rclcpp::Publisher<std_msgs::msg::String>::SharedPtr Potential_obstacle_N15_publisher;
		rclcpp::Publisher<std_msgs::msg::String>::SharedPtr Predicted_track_15_publisher;
		rclcpp::Publisher<std_msgs::msg::String>::SharedPtr Potential_obstacle_N16_publisher;
		rclcpp::Publisher<std_msgs::msg::String>::SharedPtr Predicted_track_16_publisher;
		rclcpp::Publisher<std_msgs::msg::String>::SharedPtr Potential_obstacle_N17_publisher;
		rclcpp::Publisher<std_msgs::msg::String>::SharedPtr Predicted_track_17_publisher;
		rclcpp::Publisher<std_msgs::msg::String>::SharedPtr Potential_obstacle_N18_publisher;
		rclcpp::Publisher<std_msgs::msg::String>::SharedPtr Predicted_track_18_publisher;
		rclcpp::Publisher<std_msgs::msg::String>::SharedPtr Potential_obstacle_N19_publisher;
		rclcpp::Publisher<std_msgs::msg::String>::SharedPtr Predicted_track_19_publisher;
		rclcpp::Publisher<std_msgs::msg::String>::SharedPtr Potential_obstacle_N20_publisher;
		rclcpp::Publisher<std_msgs::msg::String>::SharedPtr Predicted_track_20_publisher;
		rclcpp::Publisher<std_msgs::msg::String>::SharedPtr Potential_obstacle_N21_publisher;
		rclcpp::Publisher<std_msgs::msg::String>::SharedPtr Predicted_track_21_publisher;
		rclcpp::Publisher<std_msgs::msg::String>::SharedPtr Potential_obstacle_N22_publisher;
		rclcpp::Publisher<std_msgs::msg::String>::SharedPtr Predicted_track_22_publisher;
		rclcpp::Publisher<std_msgs::msg::String>::SharedPtr Potential_obstacle_N23_publisher;
		rclcpp::Publisher<std_msgs::msg::String>::SharedPtr Predicted_track_23_publisher;
		rclcpp::Publisher<std_msgs::msg::String>::SharedPtr Potential_obstacle_N24_publisher;
		rclcpp::Publisher<std_msgs::msg::String>::SharedPtr Predicted_track_24_publisher;
		rclcpp::Publisher<std_msgs::msg::String>::SharedPtr Potential_obstacle_N25_publisher;
		rclcpp::Publisher<std_msgs::msg::String>::SharedPtr Predicted_track_25_publisher;
		rclcpp::Publisher<std_msgs::msg::String>::SharedPtr Potential_obstacle_N26_publisher;
		rclcpp::Publisher<std_msgs::msg::String>::SharedPtr Predicted_track_26_publisher;
		rclcpp::Publisher<std_msgs::msg::String>::SharedPtr Potential_obstacle_N27_publisher;
		rclcpp::Publisher<std_msgs::msg::String>::SharedPtr Predicted_track_27_publisher;
		rclcpp::Publisher<std_msgs::msg::String>::SharedPtr Potential_obstacle_N28_publisher;
		rclcpp::Publisher<std_msgs::msg::String>::SharedPtr Predicted_track_28_publisher;
		rclcpp::Publisher<std_msgs::msg::String>::SharedPtr Potential_obstacle_N29_publisher;
		rclcpp::Publisher<std_msgs::msg::String>::SharedPtr Predicted_track_29_publisher;
		rclcpp::Publisher<std_msgs::msg::String>::SharedPtr Potential_obstacle_N30_publisher;
		rclcpp::Publisher<std_msgs::msg::String>::SharedPtr Predicted_track_30_publisher;
		rclcpp::Publisher<std_msgs::msg::String>::SharedPtr Potential_obstacle_N31_publisher;
		rclcpp::Publisher<std_msgs::msg::String>::SharedPtr Predicted_track_31_publisher;
		rclcpp::Publisher<std_msgs::msg::String>::SharedPtr Potential_obstacle_N32_publisher;
		rclcpp::Publisher<std_msgs::msg::String>::SharedPtr Predicted_track_32_publisher;
		rclcpp::Publisher<std_msgs::msg::String>::SharedPtr Potential_obstacle_N33_publisher;
		rclcpp::Publisher<std_msgs::msg::String>::SharedPtr Predicted_track_33_publisher;
		rclcpp::Publisher<std_msgs::msg::String>::SharedPtr Potential_obstacle_N34_publisher;
		rclcpp::Publisher<std_msgs::msg::String>::SharedPtr Predicted_track_34_publisher;
		rclcpp::Publisher<std_msgs::msg::String>::SharedPtr Potential_obstacle_N35_publisher;
		rclcpp::Publisher<std_msgs::msg::String>::SharedPtr Predicted_track_35_publisher;
		rclcpp::Publisher<std_msgs::msg::String>::SharedPtr Potential_obstacle_N36_publisher;
		rclcpp::Publisher<std_msgs::msg::String>::SharedPtr Predicted_track_36_publisher;
		rclcpp::Publisher<std_msgs::msg::String>::SharedPtr Potential_obstacle_N37_publisher;
		rclcpp::Publisher<std_msgs::msg::String>::SharedPtr Predicted_track_37_publisher;
		rclcpp::Publisher<std_msgs::msg::String>::SharedPtr Potential_obstacle_N38_publisher;
		rclcpp::Publisher<std_msgs::msg::String>::SharedPtr Predicted_track_38_publisher;
		rclcpp::Publisher<std_msgs::msg::String>::SharedPtr Potential_obstacle_N39_publisher;
		rclcpp::Publisher<std_msgs::msg::String>::SharedPtr Predicted_track_39_publisher;
		rclcpp::Publisher<std_msgs::msg::String>::SharedPtr Potential_obstacle_N40_publisher;
		rclcpp::Publisher<std_msgs::msg::String>::SharedPtr Predicted_track_40_publisher;
		rclcpp::Publisher<std_msgs::msg::String>::SharedPtr Potential_obstacle_N41_publisher;
		rclcpp::Publisher<std_msgs::msg::String>::SharedPtr Predicted_track_41_publisher;
		rclcpp::Publisher<std_msgs::msg::String>::SharedPtr Potential_obstacle_N42_publisher;
		rclcpp::Publisher<std_msgs::msg::String>::SharedPtr Predicted_track_42_publisher;
		rclcpp::Publisher<std_msgs::msg::String>::SharedPtr Potential_obstacle_N43_publisher;
		rclcpp::Publisher<std_msgs::msg::String>::SharedPtr Predicted_track_43_publisher;
		rclcpp::Publisher<std_msgs::msg::String>::SharedPtr Potential_obstacle_N44_publisher;
		rclcpp::Publisher<std_msgs::msg::String>::SharedPtr Predicted_track_44_publisher;
		rclcpp::Publisher<std_msgs::msg::String>::SharedPtr Potential_obstacle_N45_publisher;
		rclcpp::Publisher<std_msgs::msg::String>::SharedPtr Predicted_track_45_publisher;
		rclcpp::Publisher<std_msgs::msg::String>::SharedPtr Potential_obstacle_N46_publisher;
		rclcpp::Publisher<std_msgs::msg::String>::SharedPtr Predicted_track_46_publisher;
		rclcpp::Publisher<std_msgs::msg::String>::SharedPtr Potential_obstacle_N47_publisher;
		rclcpp::Publisher<std_msgs::msg::String>::SharedPtr Predicted_track_47_publisher;
		rclcpp::Publisher<std_msgs::msg::String>::SharedPtr Potential_obstacle_N48_publisher;
		rclcpp::Publisher<std_msgs::msg::String>::SharedPtr Predicted_track_48_publisher;
		rclcpp::Publisher<std_msgs::msg::String>::SharedPtr Potential_obstacle_N49_publisher;
		rclcpp::Publisher<std_msgs::msg::String>::SharedPtr Predicted_track_49_publisher;
		rclcpp::Publisher<std_msgs::msg::String>::SharedPtr Potential_obstacle_N50_publisher;
		rclcpp::Publisher<std_msgs::msg::String>::SharedPtr Predicted_track_50_publisher;
		rclcpp::Publisher<std_msgs::msg::String>::SharedPtr Potential_obstacle_N51_publisher;
		rclcpp::Publisher<std_msgs::msg::String>::SharedPtr Predicted_track_51_publisher;
		rclcpp::Publisher<std_msgs::msg::String>::SharedPtr Potential_obstacle_N52_publisher;
		rclcpp::Publisher<std_msgs::msg::String>::SharedPtr Predicted_track_52_publisher;
		rclcpp::Publisher<std_msgs::msg::String>::SharedPtr Potential_obstacle_N53_publisher;
		rclcpp::Publisher<std_msgs::msg::String>::SharedPtr Predicted_track_53_publisher;
		rclcpp::Publisher<std_msgs::msg::String>::SharedPtr Potential_obstacle_N54_publisher;
		rclcpp::Publisher<std_msgs::msg::String>::SharedPtr Predicted_track_54_publisher;
		rclcpp::Publisher<std_msgs::msg::String>::SharedPtr Potential_obstacle_N55_publisher;
		rclcpp::Publisher<std_msgs::msg::String>::SharedPtr Predicted_track_55_publisher;
		rclcpp::Publisher<std_msgs::msg::String>::SharedPtr Potential_obstacle_N56_publisher;
		rclcpp::Publisher<std_msgs::msg::String>::SharedPtr Predicted_track_56_publisher;
		rclcpp::Publisher<std_msgs::msg::String>::SharedPtr Potential_obstacle_N57_publisher;
		rclcpp::Publisher<std_msgs::msg::String>::SharedPtr Predicted_track_57_publisher;
		rclcpp::Publisher<std_msgs::msg::String>::SharedPtr Potential_obstacle_N58_publisher;
		rclcpp::Publisher<std_msgs::msg::String>::SharedPtr Predicted_track_58_publisher;
		rclcpp::Publisher<std_msgs::msg::String>::SharedPtr Potential_obstacle_N59_publisher;
		rclcpp::Publisher<std_msgs::msg::String>::SharedPtr Predicted_track_59_publisher;

	public:
		Node_UKFs()
		: Node("node_ukfs")
		{
			Tracked_object_label_N_subscription_ = this->create_subscription<std_msgs::msg::String>(
				"tracked_object_label_n", rclcpp::QoS(10), std::bind(&Node_UKFs::Tracked_object_label_N_subscription_callback, this, _1));
			Potential_obstacle_N00_publisher = this->create_publisher<std_msgs::msg::String>("potential_obstacle_n00", 10);
			Predicted_track_00_publisher = this->create_publisher<std_msgs::msg::String>("predicted_track_00", 10);
			Potential_obstacle_N01_publisher = this->create_publisher<std_msgs::msg::String>("potential_obstacle_n01", 10);
			Predicted_track_01_publisher = this->create_publisher<std_msgs::msg::String>("predicted_track_01", 10);
			Potential_obstacle_N02_publisher = this->create_publisher<std_msgs::msg::String>("potential_obstacle_n02", 10);
			Predicted_track_02_publisher = this->create_publisher<std_msgs::msg::String>("predicted_track_02", 10);
			Potential_obstacle_N03_publisher = this->create_publisher<std_msgs::msg::String>("potential_obstacle_n03", 10);
			Predicted_track_03_publisher = this->create_publisher<std_msgs::msg::String>("predicted_track_03", 10);
			Potential_obstacle_N04_publisher = this->create_publisher<std_msgs::msg::String>("potential_obstacle_n04", 10);
			Predicted_track_04_publisher = this->create_publisher<std_msgs::msg::String>("predicted_track_04", 10);
			Potential_obstacle_N05_publisher = this->create_publisher<std_msgs::msg::String>("potential_obstacle_n05", 10);
			Predicted_track_05_publisher = this->create_publisher<std_msgs::msg::String>("predicted_track_05", 10);
			Potential_obstacle_N06_publisher = this->create_publisher<std_msgs::msg::String>("potential_obstacle_n06", 10);
			Predicted_track_06_publisher = this->create_publisher<std_msgs::msg::String>("predicted_track_06", 10);
			Potential_obstacle_N07_publisher = this->create_publisher<std_msgs::msg::String>("potential_obstacle_n07", 10);
			Predicted_track_07_publisher = this->create_publisher<std_msgs::msg::String>("predicted_track_07", 10);
			Potential_obstacle_N08_publisher = this->create_publisher<std_msgs::msg::String>("potential_obstacle_n08", 10);
			Predicted_track_08_publisher = this->create_publisher<std_msgs::msg::String>("predicted_track_08", 10);
			Potential_obstacle_N09_publisher = this->create_publisher<std_msgs::msg::String>("potential_obstacle_n09", 10);
			Predicted_track_09_publisher = this->create_publisher<std_msgs::msg::String>("predicted_track_09", 10);
			Potential_obstacle_N10_publisher = this->create_publisher<std_msgs::msg::String>("potential_obstacle_n10", 10);
			Predicted_track_10_publisher = this->create_publisher<std_msgs::msg::String>("predicted_track_10", 10);
			Potential_obstacle_N11_publisher = this->create_publisher<std_msgs::msg::String>("potential_obstacle_n11", 10);
			Predicted_track_11_publisher = this->create_publisher<std_msgs::msg::String>("predicted_track_11", 10);
			Potential_obstacle_N12_publisher = this->create_publisher<std_msgs::msg::String>("potential_obstacle_n12", 10);
			Predicted_track_12_publisher = this->create_publisher<std_msgs::msg::String>("predicted_track_12", 10);
			Potential_obstacle_N13_publisher = this->create_publisher<std_msgs::msg::String>("potential_obstacle_n13", 10);
			Predicted_track_13_publisher = this->create_publisher<std_msgs::msg::String>("predicted_track_13", 10);
			Potential_obstacle_N14_publisher = this->create_publisher<std_msgs::msg::String>("potential_obstacle_n14", 10);
			Predicted_track_14_publisher = this->create_publisher<std_msgs::msg::String>("predicted_track_14", 10);
			Potential_obstacle_N15_publisher = this->create_publisher<std_msgs::msg::String>("potential_obstacle_n15", 10);
			Predicted_track_15_publisher = this->create_publisher<std_msgs::msg::String>("predicted_track_15", 10);
			Potential_obstacle_N16_publisher = this->create_publisher<std_msgs::msg::String>("potential_obstacle_n16", 10);
			Predicted_track_16_publisher = this->create_publisher<std_msgs::msg::String>("predicted_track_16", 10);
			Potential_obstacle_N17_publisher = this->create_publisher<std_msgs::msg::String>("potential_obstacle_n17", 10);
			Predicted_track_17_publisher = this->create_publisher<std_msgs::msg::String>("predicted_track_17", 10);
			Potential_obstacle_N18_publisher = this->create_publisher<std_msgs::msg::String>("potential_obstacle_n18", 10);
			Predicted_track_18_publisher = this->create_publisher<std_msgs::msg::String>("predicted_track_18", 10);
			Potential_obstacle_N19_publisher = this->create_publisher<std_msgs::msg::String>("potential_obstacle_n19", 10);
			Predicted_track_19_publisher = this->create_publisher<std_msgs::msg::String>("predicted_track_19", 10);
			Potential_obstacle_N20_publisher = this->create_publisher<std_msgs::msg::String>("potential_obstacle_n20", 10);
			Predicted_track_20_publisher = this->create_publisher<std_msgs::msg::String>("predicted_track_20", 10);
			Potential_obstacle_N21_publisher = this->create_publisher<std_msgs::msg::String>("potential_obstacle_n21", 10);
			Predicted_track_21_publisher = this->create_publisher<std_msgs::msg::String>("predicted_track_21", 10);
			Potential_obstacle_N22_publisher = this->create_publisher<std_msgs::msg::String>("potential_obstacle_n22", 10);
			Predicted_track_22_publisher = this->create_publisher<std_msgs::msg::String>("predicted_track_22", 10);
			Potential_obstacle_N23_publisher = this->create_publisher<std_msgs::msg::String>("potential_obstacle_n23", 10);
			Predicted_track_23_publisher = this->create_publisher<std_msgs::msg::String>("predicted_track_23", 10);
			Potential_obstacle_N24_publisher = this->create_publisher<std_msgs::msg::String>("potential_obstacle_n24", 10);
			Predicted_track_24_publisher = this->create_publisher<std_msgs::msg::String>("predicted_track_24", 10);
			Potential_obstacle_N25_publisher = this->create_publisher<std_msgs::msg::String>("potential_obstacle_n25", 10);
			Predicted_track_25_publisher = this->create_publisher<std_msgs::msg::String>("predicted_track_25", 10);
			Potential_obstacle_N26_publisher = this->create_publisher<std_msgs::msg::String>("potential_obstacle_n26", 10);
			Predicted_track_26_publisher = this->create_publisher<std_msgs::msg::String>("predicted_track_26", 10);
			Potential_obstacle_N27_publisher = this->create_publisher<std_msgs::msg::String>("potential_obstacle_n27", 10);
			Predicted_track_27_publisher = this->create_publisher<std_msgs::msg::String>("predicted_track_27", 10);
			Potential_obstacle_N28_publisher = this->create_publisher<std_msgs::msg::String>("potential_obstacle_n28", 10);
			Predicted_track_28_publisher = this->create_publisher<std_msgs::msg::String>("predicted_track_28", 10);
			Potential_obstacle_N29_publisher = this->create_publisher<std_msgs::msg::String>("potential_obstacle_n29", 10);
			Predicted_track_29_publisher = this->create_publisher<std_msgs::msg::String>("predicted_track_29", 10);
			Potential_obstacle_N30_publisher = this->create_publisher<std_msgs::msg::String>("potential_obstacle_n30", 10);
			Predicted_track_30_publisher = this->create_publisher<std_msgs::msg::String>("predicted_track_30", 10);
			Potential_obstacle_N31_publisher = this->create_publisher<std_msgs::msg::String>("potential_obstacle_n31", 10);
			Predicted_track_31_publisher = this->create_publisher<std_msgs::msg::String>("predicted_track_31", 10);
			Potential_obstacle_N32_publisher = this->create_publisher<std_msgs::msg::String>("potential_obstacle_n32", 10);
			Predicted_track_32_publisher = this->create_publisher<std_msgs::msg::String>("predicted_track_32", 10);
			Potential_obstacle_N33_publisher = this->create_publisher<std_msgs::msg::String>("potential_obstacle_n33", 10);
			Predicted_track_33_publisher = this->create_publisher<std_msgs::msg::String>("predicted_track_33", 10);
			Potential_obstacle_N34_publisher = this->create_publisher<std_msgs::msg::String>("potential_obstacle_n34", 10);
			Predicted_track_34_publisher = this->create_publisher<std_msgs::msg::String>("predicted_track_34", 10);
			Potential_obstacle_N35_publisher = this->create_publisher<std_msgs::msg::String>("potential_obstacle_n35", 10);
			Predicted_track_35_publisher = this->create_publisher<std_msgs::msg::String>("predicted_track_35", 10);
			Potential_obstacle_N36_publisher = this->create_publisher<std_msgs::msg::String>("potential_obstacle_n36", 10);
			Predicted_track_36_publisher = this->create_publisher<std_msgs::msg::String>("predicted_track_36", 10);
			Potential_obstacle_N37_publisher = this->create_publisher<std_msgs::msg::String>("potential_obstacle_n37", 10);
			Predicted_track_37_publisher = this->create_publisher<std_msgs::msg::String>("predicted_track_37", 10);
			Potential_obstacle_N38_publisher = this->create_publisher<std_msgs::msg::String>("potential_obstacle_n38", 10);
			Predicted_track_38_publisher = this->create_publisher<std_msgs::msg::String>("predicted_track_38", 10);
			Potential_obstacle_N39_publisher = this->create_publisher<std_msgs::msg::String>("potential_obstacle_n39", 10);
			Predicted_track_39_publisher = this->create_publisher<std_msgs::msg::String>("predicted_track_39", 10);
			Potential_obstacle_N40_publisher = this->create_publisher<std_msgs::msg::String>("potential_obstacle_n40", 10);
			Predicted_track_40_publisher = this->create_publisher<std_msgs::msg::String>("predicted_track_40", 10);
			Potential_obstacle_N41_publisher = this->create_publisher<std_msgs::msg::String>("potential_obstacle_n41", 10);
			Predicted_track_41_publisher = this->create_publisher<std_msgs::msg::String>("predicted_track_41", 10);
			Potential_obstacle_N42_publisher = this->create_publisher<std_msgs::msg::String>("potential_obstacle_n42", 10);
			Predicted_track_42_publisher = this->create_publisher<std_msgs::msg::String>("predicted_track_42", 10);
			Potential_obstacle_N43_publisher = this->create_publisher<std_msgs::msg::String>("potential_obstacle_n43", 10);
			Predicted_track_43_publisher = this->create_publisher<std_msgs::msg::String>("predicted_track_43", 10);
			Potential_obstacle_N44_publisher = this->create_publisher<std_msgs::msg::String>("potential_obstacle_n44", 10);
			Predicted_track_44_publisher = this->create_publisher<std_msgs::msg::String>("predicted_track_44", 10);
			Potential_obstacle_N45_publisher = this->create_publisher<std_msgs::msg::String>("potential_obstacle_n45", 10);
			Predicted_track_45_publisher = this->create_publisher<std_msgs::msg::String>("predicted_track_45", 10);
			Potential_obstacle_N46_publisher = this->create_publisher<std_msgs::msg::String>("potential_obstacle_n46", 10);
			Predicted_track_46_publisher = this->create_publisher<std_msgs::msg::String>("predicted_track_46", 10);
			Potential_obstacle_N47_publisher = this->create_publisher<std_msgs::msg::String>("potential_obstacle_n47", 10);
			Predicted_track_47_publisher = this->create_publisher<std_msgs::msg::String>("predicted_track_47", 10);
			Potential_obstacle_N48_publisher = this->create_publisher<std_msgs::msg::String>("potential_obstacle_n48", 10);
			Predicted_track_48_publisher = this->create_publisher<std_msgs::msg::String>("predicted_track_48", 10);
			Potential_obstacle_N49_publisher = this->create_publisher<std_msgs::msg::String>("potential_obstacle_n49", 10);
			Predicted_track_49_publisher = this->create_publisher<std_msgs::msg::String>("predicted_track_49", 10);
			Potential_obstacle_N50_publisher = this->create_publisher<std_msgs::msg::String>("potential_obstacle_n50", 10);
			Predicted_track_50_publisher = this->create_publisher<std_msgs::msg::String>("predicted_track_50", 10);
			Potential_obstacle_N51_publisher = this->create_publisher<std_msgs::msg::String>("potential_obstacle_n51", 10);
			Predicted_track_51_publisher = this->create_publisher<std_msgs::msg::String>("predicted_track_51", 10);
			Potential_obstacle_N52_publisher = this->create_publisher<std_msgs::msg::String>("potential_obstacle_n52", 10);
			Predicted_track_52_publisher = this->create_publisher<std_msgs::msg::String>("predicted_track_52", 10);
			Potential_obstacle_N53_publisher = this->create_publisher<std_msgs::msg::String>("potential_obstacle_n53", 10);
			Predicted_track_53_publisher = this->create_publisher<std_msgs::msg::String>("predicted_track_53", 10);
			Potential_obstacle_N54_publisher = this->create_publisher<std_msgs::msg::String>("potential_obstacle_n54", 10);
			Predicted_track_54_publisher = this->create_publisher<std_msgs::msg::String>("predicted_track_54", 10);
			Potential_obstacle_N55_publisher = this->create_publisher<std_msgs::msg::String>("potential_obstacle_n55", 10);
			Predicted_track_55_publisher = this->create_publisher<std_msgs::msg::String>("predicted_track_55", 10);
			Potential_obstacle_N56_publisher = this->create_publisher<std_msgs::msg::String>("potential_obstacle_n56", 10);
			Predicted_track_56_publisher = this->create_publisher<std_msgs::msg::String>("predicted_track_56", 10);
			Potential_obstacle_N57_publisher = this->create_publisher<std_msgs::msg::String>("potential_obstacle_n57", 10);
			Predicted_track_57_publisher = this->create_publisher<std_msgs::msg::String>("predicted_track_57", 10);
			Potential_obstacle_N58_publisher = this->create_publisher<std_msgs::msg::String>("potential_obstacle_n58", 10);
			Predicted_track_58_publisher = this->create_publisher<std_msgs::msg::String>("predicted_track_58", 10);
			Potential_obstacle_N59_publisher = this->create_publisher<std_msgs::msg::String>("potential_obstacle_n59", 10);
			Predicted_track_59_publisher = this->create_publisher<std_msgs::msg::String>("predicted_track_59", 10);
		}
	void Tracked_object_label_N_subscription_callback(const std_msgs::msg::String::SharedPtr msg) {
	#ifdef CONSOLE_ENABLED
		std::cout << "ROS2: subscription callback " << msg->data << std::endl ;
	#endif
		extern int UKF_index_29[1];
		extern int UKF_index_10[1];
		extern int UKF_index_23[1];
		extern int UKF_index_45[1];
		extern int UKF_index_24[1];
		extern int UKF_index_16[1];
		extern int UKF_index_09[1];
		extern int UKF_index_20[1];
		extern int UKF_index_42[1];
		extern int UKF_index_54[1];
		extern int UKF_index_18[1];
		extern int UKF_index_11[1];
		extern int UKF_index_49[1];
		extern int UKF_index_59[1];
		extern int UKF_index_34[1];
		extern int UKF_index_55[1];
		extern int UKF_index_52[1];
		extern int UKF_index_53[1];
		extern int UKF_index_28[1];
		extern int UKF_index_36[1];
		extern int UKF_index_39[1];
		extern int UKF_index_48[1];
		extern int UKF_index_22[1];
		extern int UKF_index_26[1];
		extern int UKF_index_32[1];
		extern int UKF_index_40[1];
		extern int UKF_index_47[1];
		extern int UKF_index_44[1];
		extern int UKF_index_14[1];
		extern int UKF_index_19[1];
		extern int UKF_index_15[1];
		extern int UKF_index_33[1];
		extern int UKF_index_38[1];
		extern int UKF_index_58[1];
		extern int UKF_index_41[1];
		extern int UKF_index_50[1];
		extern int UKF_index_03[1];
		extern int UKF_index_57[1];
		extern int UKF_index_13[1];
		extern int UKF_index_05[1];
		extern int UKF_index_51[1];
		extern int UKF_index_37[1];
		extern int UKF_index_46[1];
		extern int UKF_index_01[1];
		extern int UKF_index_31[1];
		extern int UKF_index_02[1];
		extern int UKF_index_00[1];
		extern int UKF_index_17[1];
		extern int UKF_index_08[1];
		extern int UKF_index_25[1];
		extern int UKF_index_56[1];
		extern int UKF_index_21[1];
		extern int UKF_index_04[1];
		extern int UKF_index_35[1];
		extern int UKF_index_43[1];
		extern int UKF_index_27[1];
		extern int UKF_index_12[1];
		extern int UKF_index_07[1];
		extern int UKF_index_06[1];
		extern int UKF_index_30[1];
		int dummy;
		
		 #if defined(_OPENMP)
		 #pragma omp taskgraph tdg_type(static) nowait
		#endif
		{
		#if defined(_OPENMP)
		 #pragma omp task depend(out:UKF_index_30,UKF_index_06,UKF_index_07,UKF_index_12,UKF_index_27,UKF_index_43,UKF_index_35,UKF_index_04,UKF_index_21,UKF_index_56,UKF_index_25,UKF_index_08,UKF_index_17,UKF_index_00,UKF_index_02,UKF_index_31,UKF_index_01,UKF_index_46,UKF_index_37,UKF_index_51,UKF_index_05,UKF_index_13,UKF_index_57,UKF_index_03,UKF_index_50,UKF_index_41,UKF_index_58,UKF_index_38,UKF_index_33,UKF_index_15,UKF_index_19,UKF_index_14,UKF_index_44,UKF_index_47,UKF_index_40,UKF_index_32,UKF_index_26,UKF_index_22,UKF_index_48,UKF_index_39,UKF_index_36,UKF_index_28,UKF_index_53,UKF_index_52,UKF_index_55,UKF_index_34,UKF_index_59,UKF_index_49,UKF_index_11,UKF_index_18,UKF_index_54,UKF_index_42,UKF_index_20,UKF_index_09,UKF_index_16,UKF_index_24,UKF_index_45,UKF_index_23,UKF_index_10,UKF_index_29) replicated(2, dummy, check_Node_UKFs)
		#elif defined(_OMPSS)
		 #pragma oss task depend(out:UKF_index_30,UKF_index_06,UKF_index_07,UKF_index_12,UKF_index_27,UKF_index_43,UKF_index_35,UKF_index_04,UKF_index_21,UKF_index_56,UKF_index_25,UKF_index_08,UKF_index_17,UKF_index_00,UKF_index_02,UKF_index_31,UKF_index_01,UKF_index_46,UKF_index_37,UKF_index_51,UKF_index_05,UKF_index_13,UKF_index_57,UKF_index_03,UKF_index_50,UKF_index_41,UKF_index_58,UKF_index_38,UKF_index_33,UKF_index_15,UKF_index_19,UKF_index_14,UKF_index_44,UKF_index_47,UKF_index_40,UKF_index_32,UKF_index_26,UKF_index_22,UKF_index_48,UKF_index_39,UKF_index_36,UKF_index_28,UKF_index_53,UKF_index_52,UKF_index_55,UKF_index_34,UKF_index_59,UKF_index_49,UKF_index_11,UKF_index_18,UKF_index_54,UKF_index_42,UKF_index_20,UKF_index_09,UKF_index_16,UKF_index_24,UKF_index_45,UKF_index_23,UKF_index_10,UKF_index_29) replicated(2, dummy, check_Node_UKFs)
		#endif
		run_UKF_management();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:UKF_index_00) replicated(2, dummy, check_Node_UKFs)
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:UKF_index_00) replicated(2, dummy, check_Node_UKFs)
		#endif
		run_UKF_00(Potential_obstacle_N00_publisher, Predicted_track_00_publisher);
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:UKF_index_01) replicated(2, dummy, check_Node_UKFs)
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:UKF_index_01) replicated(2, dummy, check_Node_UKFs)
		#endif
		run_UKF_01(Potential_obstacle_N01_publisher, Predicted_track_01_publisher);
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:UKF_index_02) replicated(2, dummy, check_Node_UKFs)
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:UKF_index_02) replicated(2, dummy, check_Node_UKFs)
		#endif
		run_UKF_02(Potential_obstacle_N02_publisher, Predicted_track_02_publisher);
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:UKF_index_03) replicated(2, dummy, check_Node_UKFs)
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:UKF_index_03) replicated(2, dummy, check_Node_UKFs)
		#endif
		run_UKF_03(Potential_obstacle_N03_publisher, Predicted_track_03_publisher);
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:UKF_index_04) replicated(2, dummy, check_Node_UKFs)
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:UKF_index_04) replicated(2, dummy, check_Node_UKFs)
		#endif
		run_UKF_04(Potential_obstacle_N04_publisher, Predicted_track_04_publisher);
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:UKF_index_05) replicated(2, dummy, check_Node_UKFs)
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:UKF_index_05) replicated(2, dummy, check_Node_UKFs)
		#endif
		run_UKF_05(Potential_obstacle_N05_publisher, Predicted_track_05_publisher);
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:UKF_index_06) replicated(2, dummy, check_Node_UKFs)
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:UKF_index_06) replicated(2, dummy, check_Node_UKFs)
		#endif
		run_UKF_06(Potential_obstacle_N06_publisher, Predicted_track_06_publisher);
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:UKF_index_07) replicated(2, dummy, check_Node_UKFs)
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:UKF_index_07) replicated(2, dummy, check_Node_UKFs)
		#endif
		run_UKF_07(Potential_obstacle_N07_publisher, Predicted_track_07_publisher);
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:UKF_index_08) replicated(2, dummy, check_Node_UKFs)
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:UKF_index_08) replicated(2, dummy, check_Node_UKFs)
		#endif
		run_UKF_08(Potential_obstacle_N08_publisher, Predicted_track_08_publisher);
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:UKF_index_09) replicated(2, dummy, check_Node_UKFs)
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:UKF_index_09) replicated(2, dummy, check_Node_UKFs)
		#endif
		run_UKF_09(Potential_obstacle_N09_publisher, Predicted_track_09_publisher);
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:UKF_index_10) replicated(2, dummy, check_Node_UKFs)
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:UKF_index_10) replicated(2, dummy, check_Node_UKFs)
		#endif
		run_UKF_10(Potential_obstacle_N10_publisher, Predicted_track_10_publisher);
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:UKF_index_11) replicated(2, dummy, check_Node_UKFs)
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:UKF_index_11) replicated(2, dummy, check_Node_UKFs)
		#endif
		run_UKF_11(Potential_obstacle_N11_publisher, Predicted_track_11_publisher);
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:UKF_index_12) replicated(2, dummy, check_Node_UKFs)
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:UKF_index_12) replicated(2, dummy, check_Node_UKFs)
		#endif
		run_UKF_12(Potential_obstacle_N12_publisher, Predicted_track_12_publisher);
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:UKF_index_13) replicated(2, dummy, check_Node_UKFs)
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:UKF_index_13) replicated(2, dummy, check_Node_UKFs)
		#endif
		run_UKF_13(Potential_obstacle_N13_publisher, Predicted_track_13_publisher);
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:UKF_index_14) replicated(2, dummy, check_Node_UKFs)
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:UKF_index_14) replicated(2, dummy, check_Node_UKFs)
		#endif
		run_UKF_14(Potential_obstacle_N14_publisher, Predicted_track_14_publisher);
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:UKF_index_15) replicated(2, dummy, check_Node_UKFs)
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:UKF_index_15) replicated(2, dummy, check_Node_UKFs)
		#endif
		run_UKF_15(Potential_obstacle_N15_publisher, Predicted_track_15_publisher);
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:UKF_index_16) replicated(2, dummy, check_Node_UKFs)
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:UKF_index_16) replicated(2, dummy, check_Node_UKFs)
		#endif
		run_UKF_16(Potential_obstacle_N16_publisher, Predicted_track_16_publisher);
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:UKF_index_17) replicated(2, dummy, check_Node_UKFs)
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:UKF_index_17) replicated(2, dummy, check_Node_UKFs)
		#endif
		run_UKF_17(Potential_obstacle_N17_publisher, Predicted_track_17_publisher);
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:UKF_index_18) replicated(2, dummy, check_Node_UKFs)
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:UKF_index_18) replicated(2, dummy, check_Node_UKFs)
		#endif
		run_UKF_18(Potential_obstacle_N18_publisher, Predicted_track_18_publisher);
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:UKF_index_19) replicated(2, dummy, check_Node_UKFs)
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:UKF_index_19) replicated(2, dummy, check_Node_UKFs)
		#endif
		run_UKF_19(Potential_obstacle_N19_publisher, Predicted_track_19_publisher);
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:UKF_index_20) replicated(2, dummy, check_Node_UKFs)
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:UKF_index_20) replicated(2, dummy, check_Node_UKFs)
		#endif
		run_UKF_20(Potential_obstacle_N20_publisher, Predicted_track_20_publisher);
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:UKF_index_21) replicated(2, dummy, check_Node_UKFs)
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:UKF_index_21) replicated(2, dummy, check_Node_UKFs)
		#endif
		run_UKF_21(Potential_obstacle_N21_publisher, Predicted_track_21_publisher);
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:UKF_index_22) replicated(2, dummy, check_Node_UKFs)
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:UKF_index_22) replicated(2, dummy, check_Node_UKFs)
		#endif
		run_UKF_22(Potential_obstacle_N22_publisher, Predicted_track_22_publisher);
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:UKF_index_23) replicated(2, dummy, check_Node_UKFs)
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:UKF_index_23) replicated(2, dummy, check_Node_UKFs)
		#endif
		run_UKF_23(Potential_obstacle_N23_publisher, Predicted_track_23_publisher);
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:UKF_index_24) replicated(2, dummy, check_Node_UKFs)
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:UKF_index_24) replicated(2, dummy, check_Node_UKFs)
		#endif
		run_UKF_24(Potential_obstacle_N24_publisher, Predicted_track_24_publisher);
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:UKF_index_25) replicated(2, dummy, check_Node_UKFs)
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:UKF_index_25) replicated(2, dummy, check_Node_UKFs)
		#endif
		run_UKF_25(Potential_obstacle_N25_publisher, Predicted_track_25_publisher);
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:UKF_index_26) replicated(2, dummy, check_Node_UKFs)
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:UKF_index_26) replicated(2, dummy, check_Node_UKFs)
		#endif
		run_UKF_26(Potential_obstacle_N26_publisher, Predicted_track_26_publisher);
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:UKF_index_27) replicated(2, dummy, check_Node_UKFs)
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:UKF_index_27) replicated(2, dummy, check_Node_UKFs)
		#endif
		run_UKF_27(Potential_obstacle_N27_publisher, Predicted_track_27_publisher);
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:UKF_index_28) replicated(2, dummy, check_Node_UKFs)
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:UKF_index_28) replicated(2, dummy, check_Node_UKFs)
		#endif
		run_UKF_28(Potential_obstacle_N28_publisher, Predicted_track_28_publisher);
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:UKF_index_29) replicated(2, dummy, check_Node_UKFs)
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:UKF_index_29) replicated(2, dummy, check_Node_UKFs)
		#endif
		run_UKF_29(Potential_obstacle_N29_publisher, Predicted_track_29_publisher);
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:UKF_index_30) replicated(2, dummy, check_Node_UKFs)
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:UKF_index_30) replicated(2, dummy, check_Node_UKFs)
		#endif
		run_UKF_30(Potential_obstacle_N30_publisher, Predicted_track_30_publisher);
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:UKF_index_31) replicated(2, dummy, check_Node_UKFs)
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:UKF_index_31) replicated(2, dummy, check_Node_UKFs)
		#endif
		run_UKF_31(Potential_obstacle_N31_publisher, Predicted_track_31_publisher);
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:UKF_index_32) replicated(2, dummy, check_Node_UKFs)
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:UKF_index_32) replicated(2, dummy, check_Node_UKFs)
		#endif
		run_UKF_32(Potential_obstacle_N32_publisher, Predicted_track_32_publisher);
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:UKF_index_33) replicated(2, dummy, check_Node_UKFs)
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:UKF_index_33) replicated(2, dummy, check_Node_UKFs)
		#endif
		run_UKF_33(Potential_obstacle_N33_publisher, Predicted_track_33_publisher);
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:UKF_index_34) replicated(2, dummy, check_Node_UKFs)
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:UKF_index_34) replicated(2, dummy, check_Node_UKFs)
		#endif
		run_UKF_34(Potential_obstacle_N34_publisher, Predicted_track_34_publisher);
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:UKF_index_35) replicated(2, dummy, check_Node_UKFs)
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:UKF_index_35) replicated(2, dummy, check_Node_UKFs)
		#endif
		run_UKF_35(Potential_obstacle_N35_publisher, Predicted_track_35_publisher);
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:UKF_index_36) replicated(2, dummy, check_Node_UKFs)
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:UKF_index_36) replicated(2, dummy, check_Node_UKFs)
		#endif
		run_UKF_36(Potential_obstacle_N36_publisher, Predicted_track_36_publisher);
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:UKF_index_37) replicated(2, dummy, check_Node_UKFs)
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:UKF_index_37) replicated(2, dummy, check_Node_UKFs)
		#endif
		run_UKF_37(Potential_obstacle_N37_publisher, Predicted_track_37_publisher);
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:UKF_index_38) replicated(2, dummy, check_Node_UKFs)
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:UKF_index_38) replicated(2, dummy, check_Node_UKFs)
		#endif
		run_UKF_38(Potential_obstacle_N38_publisher, Predicted_track_38_publisher);
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:UKF_index_39) replicated(2, dummy, check_Node_UKFs)
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:UKF_index_39) replicated(2, dummy, check_Node_UKFs)
		#endif
		run_UKF_39(Potential_obstacle_N39_publisher, Predicted_track_39_publisher);
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:UKF_index_40) replicated(2, dummy, check_Node_UKFs)
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:UKF_index_40) replicated(2, dummy, check_Node_UKFs)
		#endif
		run_UKF_40(Potential_obstacle_N40_publisher, Predicted_track_40_publisher);
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:UKF_index_41) replicated(2, dummy, check_Node_UKFs)
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:UKF_index_41) replicated(2, dummy, check_Node_UKFs)
		#endif
		run_UKF_41(Potential_obstacle_N41_publisher, Predicted_track_41_publisher);
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:UKF_index_42) replicated(2, dummy, check_Node_UKFs)
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:UKF_index_42) replicated(2, dummy, check_Node_UKFs)
		#endif
		run_UKF_42(Potential_obstacle_N42_publisher, Predicted_track_42_publisher);
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:UKF_index_43) replicated(2, dummy, check_Node_UKFs)
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:UKF_index_43) replicated(2, dummy, check_Node_UKFs)
		#endif
		run_UKF_43(Potential_obstacle_N43_publisher, Predicted_track_43_publisher);
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:UKF_index_44) replicated(2, dummy, check_Node_UKFs)
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:UKF_index_44) replicated(2, dummy, check_Node_UKFs)
		#endif
		run_UKF_44(Potential_obstacle_N44_publisher, Predicted_track_44_publisher);
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:UKF_index_45) replicated(2, dummy, check_Node_UKFs)
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:UKF_index_45) replicated(2, dummy, check_Node_UKFs)
		#endif
		run_UKF_45(Potential_obstacle_N45_publisher, Predicted_track_45_publisher);
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:UKF_index_46) replicated(2, dummy, check_Node_UKFs)
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:UKF_index_46) replicated(2, dummy, check_Node_UKFs)
		#endif
		run_UKF_46(Potential_obstacle_N46_publisher, Predicted_track_46_publisher);
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:UKF_index_47) replicated(2, dummy, check_Node_UKFs)
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:UKF_index_47) replicated(2, dummy, check_Node_UKFs)
		#endif
		run_UKF_47(Potential_obstacle_N47_publisher, Predicted_track_47_publisher);
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:UKF_index_48) replicated(2, dummy, check_Node_UKFs)
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:UKF_index_48) replicated(2, dummy, check_Node_UKFs)
		#endif
		run_UKF_48(Potential_obstacle_N48_publisher, Predicted_track_48_publisher);
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:UKF_index_49) replicated(2, dummy, check_Node_UKFs)
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:UKF_index_49) replicated(2, dummy, check_Node_UKFs)
		#endif
		run_UKF_49(Potential_obstacle_N49_publisher, Predicted_track_49_publisher);
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:UKF_index_50) replicated(2, dummy, check_Node_UKFs)
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:UKF_index_50) replicated(2, dummy, check_Node_UKFs)
		#endif
		run_UKF_50(Potential_obstacle_N50_publisher, Predicted_track_50_publisher);
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:UKF_index_51) replicated(2, dummy, check_Node_UKFs)
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:UKF_index_51) replicated(2, dummy, check_Node_UKFs)
		#endif
		run_UKF_51(Potential_obstacle_N51_publisher, Predicted_track_51_publisher);
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:UKF_index_52) replicated(2, dummy, check_Node_UKFs)
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:UKF_index_52) replicated(2, dummy, check_Node_UKFs)
		#endif
		run_UKF_52(Potential_obstacle_N52_publisher, Predicted_track_52_publisher);
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:UKF_index_53) replicated(2, dummy, check_Node_UKFs)
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:UKF_index_53) replicated(2, dummy, check_Node_UKFs)
		#endif
		run_UKF_53(Potential_obstacle_N53_publisher, Predicted_track_53_publisher);
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:UKF_index_54) replicated(2, dummy, check_Node_UKFs)
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:UKF_index_54) replicated(2, dummy, check_Node_UKFs)
		#endif
		run_UKF_54(Potential_obstacle_N54_publisher, Predicted_track_54_publisher);
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:UKF_index_55) replicated(2, dummy, check_Node_UKFs)
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:UKF_index_55) replicated(2, dummy, check_Node_UKFs)
		#endif
		run_UKF_55(Potential_obstacle_N55_publisher, Predicted_track_55_publisher);
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:UKF_index_56) replicated(2, dummy, check_Node_UKFs)
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:UKF_index_56) replicated(2, dummy, check_Node_UKFs)
		#endif
		run_UKF_56(Potential_obstacle_N56_publisher, Predicted_track_56_publisher);
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:UKF_index_57) replicated(2, dummy, check_Node_UKFs)
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:UKF_index_57) replicated(2, dummy, check_Node_UKFs)
		#endif
		run_UKF_57(Potential_obstacle_N57_publisher, Predicted_track_57_publisher);
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:UKF_index_58) replicated(2, dummy, check_Node_UKFs)
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:UKF_index_58) replicated(2, dummy, check_Node_UKFs)
		#endif
		run_UKF_58(Potential_obstacle_N58_publisher, Predicted_track_58_publisher);
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:UKF_index_59) replicated(2, dummy, check_Node_UKFs)
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:UKF_index_59) replicated(2, dummy, check_Node_UKFs)
		#endif
		run_UKF_59(Potential_obstacle_N59_publisher, Predicted_track_59_publisher);
		}
		#pragma omp taskwait;
	}
	
	
};
