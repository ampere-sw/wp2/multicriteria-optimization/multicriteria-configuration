// This code is auto-generated

#include <chrono>
#include <memory>

#include "rclcpp/rclcpp.hpp"
#include "std_msgs/msg/string.hpp"

#include "runnables.hpp"

using namespace std::chrono_literals;
using std::placeholders::_1;
#define CONSOLE_ENABLED

#if defined(_OPENMP)
int check_Node_CCM(int *original, int *replicated){ return 1;}
#endif
class Node_CCM : public rclcpp::Node
{
	private:
		rclcpp::Subscription<std_msgs::msg::String>::SharedPtr Potential_obstacle_N00_subscription_;
		rclcpp::Subscription<std_msgs::msg::String>::SharedPtr Potential_obstacle_N03_subscription_;
		rclcpp::Subscription<std_msgs::msg::String>::SharedPtr Potential_obstacle_N06_subscription_;
		rclcpp::Subscription<std_msgs::msg::String>::SharedPtr Potential_obstacle_N08_subscription_;
		rclcpp::Subscription<std_msgs::msg::String>::SharedPtr Potential_obstacle_N02_subscription_;
		rclcpp::Subscription<std_msgs::msg::String>::SharedPtr Potential_obstacle_N13_subscription_;
		rclcpp::Subscription<std_msgs::msg::String>::SharedPtr Potential_obstacle_N04_subscription_;
		rclcpp::Subscription<std_msgs::msg::String>::SharedPtr Potential_obstacle_N11_subscription_;
		rclcpp::Subscription<std_msgs::msg::String>::SharedPtr Potential_obstacle_N07_subscription_;
		rclcpp::Subscription<std_msgs::msg::String>::SharedPtr Potential_obstacle_N05_subscription_;
		rclcpp::Subscription<std_msgs::msg::String>::SharedPtr Potential_obstacle_N01_subscription_;
		rclcpp::Subscription<std_msgs::msg::String>::SharedPtr Potential_obstacle_N15_subscription_;
		rclcpp::Subscription<std_msgs::msg::String>::SharedPtr Potential_obstacle_N10_subscription_;
		rclcpp::Subscription<std_msgs::msg::String>::SharedPtr Potential_obstacle_N14_subscription_;
		rclcpp::Subscription<std_msgs::msg::String>::SharedPtr Potential_obstacle_N09_subscription_;
		rclcpp::Subscription<std_msgs::msg::String>::SharedPtr Potential_obstacle_N12_subscription_;
		rclcpp::Subscription<std_msgs::msg::String>::SharedPtr Potential_obstacle_N37_subscription_;
		rclcpp::Subscription<std_msgs::msg::String>::SharedPtr Potential_obstacle_N48_subscription_;
		rclcpp::Subscription<std_msgs::msg::String>::SharedPtr Potential_obstacle_N30_subscription_;
		rclcpp::Subscription<std_msgs::msg::String>::SharedPtr Potential_obstacle_N19_subscription_;
		rclcpp::Subscription<std_msgs::msg::String>::SharedPtr Potential_obstacle_N24_subscription_;
		rclcpp::Subscription<std_msgs::msg::String>::SharedPtr Potential_obstacle_N29_subscription_;
		rclcpp::Subscription<std_msgs::msg::String>::SharedPtr Potential_obstacle_N50_subscription_;
		rclcpp::Subscription<std_msgs::msg::String>::SharedPtr Potential_obstacle_N54_subscription_;
		rclcpp::Subscription<std_msgs::msg::String>::SharedPtr Potential_obstacle_N32_subscription_;
		rclcpp::Subscription<std_msgs::msg::String>::SharedPtr Potential_obstacle_N23_subscription_;
		rclcpp::Subscription<std_msgs::msg::String>::SharedPtr Potential_obstacle_N34_subscription_;
		rclcpp::Subscription<std_msgs::msg::String>::SharedPtr Potential_obstacle_N58_subscription_;
		rclcpp::Subscription<std_msgs::msg::String>::SharedPtr Potential_obstacle_N18_subscription_;
		rclcpp::Subscription<std_msgs::msg::String>::SharedPtr Potential_obstacle_N46_subscription_;
		rclcpp::Subscription<std_msgs::msg::String>::SharedPtr Potential_obstacle_N26_subscription_;
		rclcpp::Subscription<std_msgs::msg::String>::SharedPtr Potential_obstacle_N55_subscription_;
		rclcpp::Subscription<std_msgs::msg::String>::SharedPtr Potential_obstacle_N17_subscription_;
		rclcpp::Subscription<std_msgs::msg::String>::SharedPtr Potential_obstacle_N47_subscription_;
		rclcpp::Subscription<std_msgs::msg::String>::SharedPtr Potential_obstacle_N21_subscription_;
		rclcpp::Subscription<std_msgs::msg::String>::SharedPtr Potential_obstacle_N31_subscription_;
		rclcpp::Subscription<std_msgs::msg::String>::SharedPtr Potential_obstacle_N56_subscription_;
		rclcpp::Subscription<std_msgs::msg::String>::SharedPtr Potential_obstacle_N35_subscription_;
		rclcpp::Subscription<std_msgs::msg::String>::SharedPtr Potential_obstacle_N33_subscription_;
		rclcpp::Subscription<std_msgs::msg::String>::SharedPtr Potential_obstacle_N22_subscription_;
		rclcpp::Subscription<std_msgs::msg::String>::SharedPtr Potential_obstacle_N25_subscription_;
		rclcpp::Subscription<std_msgs::msg::String>::SharedPtr Potential_obstacle_N39_subscription_;
		rclcpp::Subscription<std_msgs::msg::String>::SharedPtr Potential_obstacle_N51_subscription_;
		rclcpp::Subscription<std_msgs::msg::String>::SharedPtr Potential_obstacle_N16_subscription_;
		rclcpp::Subscription<std_msgs::msg::String>::SharedPtr Potential_obstacle_N59_subscription_;
		rclcpp::Subscription<std_msgs::msg::String>::SharedPtr Potential_obstacle_N20_subscription_;
		rclcpp::Subscription<std_msgs::msg::String>::SharedPtr Potential_obstacle_N45_subscription_;
		rclcpp::Subscription<std_msgs::msg::String>::SharedPtr Potential_obstacle_N53_subscription_;
		rclcpp::Subscription<std_msgs::msg::String>::SharedPtr Potential_obstacle_N41_subscription_;
		rclcpp::Subscription<std_msgs::msg::String>::SharedPtr Potential_obstacle_N36_subscription_;
		rclcpp::Subscription<std_msgs::msg::String>::SharedPtr Potential_obstacle_N49_subscription_;
		rclcpp::Subscription<std_msgs::msg::String>::SharedPtr Potential_obstacle_N28_subscription_;
		rclcpp::Subscription<std_msgs::msg::String>::SharedPtr Potential_obstacle_N27_subscription_;
		rclcpp::Subscription<std_msgs::msg::String>::SharedPtr Potential_obstacle_N52_subscription_;
		rclcpp::Subscription<std_msgs::msg::String>::SharedPtr Potential_obstacle_N38_subscription_;
		rclcpp::Subscription<std_msgs::msg::String>::SharedPtr Potential_obstacle_N42_subscription_;
		rclcpp::Subscription<std_msgs::msg::String>::SharedPtr Potential_obstacle_N57_subscription_;
		rclcpp::Subscription<std_msgs::msg::String>::SharedPtr Potential_obstacle_N44_subscription_;
		rclcpp::Subscription<std_msgs::msg::String>::SharedPtr Potential_obstacle_N43_subscription_;
		rclcpp::Subscription<std_msgs::msg::String>::SharedPtr Potential_obstacle_N40_subscription_;

	public:
		Node_CCM()
		: Node("node_ccm")
		{
			Potential_obstacle_N00_subscription_ = this->create_subscription<std_msgs::msg::String>(
				"potential_obstacle_n00", rclcpp::QoS(10), std::bind(&Node_CCM::Potential_obstacle_N00_subscription_callback, this, _1));
			Potential_obstacle_N03_subscription_ = this->create_subscription<std_msgs::msg::String>(
				"potential_obstacle_n03", rclcpp::QoS(10), std::bind(&Node_CCM::Potential_obstacle_N03_subscription_callback, this, _1));
			Potential_obstacle_N06_subscription_ = this->create_subscription<std_msgs::msg::String>(
				"potential_obstacle_n06", rclcpp::QoS(10), std::bind(&Node_CCM::Potential_obstacle_N06_subscription_callback, this, _1));
			Potential_obstacle_N08_subscription_ = this->create_subscription<std_msgs::msg::String>(
				"potential_obstacle_n08", rclcpp::QoS(10), std::bind(&Node_CCM::Potential_obstacle_N08_subscription_callback, this, _1));
			Potential_obstacle_N02_subscription_ = this->create_subscription<std_msgs::msg::String>(
				"potential_obstacle_n02", rclcpp::QoS(10), std::bind(&Node_CCM::Potential_obstacle_N02_subscription_callback, this, _1));
			Potential_obstacle_N13_subscription_ = this->create_subscription<std_msgs::msg::String>(
				"potential_obstacle_n13", rclcpp::QoS(10), std::bind(&Node_CCM::Potential_obstacle_N13_subscription_callback, this, _1));
			Potential_obstacle_N04_subscription_ = this->create_subscription<std_msgs::msg::String>(
				"potential_obstacle_n04", rclcpp::QoS(10), std::bind(&Node_CCM::Potential_obstacle_N04_subscription_callback, this, _1));
			Potential_obstacle_N11_subscription_ = this->create_subscription<std_msgs::msg::String>(
				"potential_obstacle_n11", rclcpp::QoS(10), std::bind(&Node_CCM::Potential_obstacle_N11_subscription_callback, this, _1));
			Potential_obstacle_N07_subscription_ = this->create_subscription<std_msgs::msg::String>(
				"potential_obstacle_n07", rclcpp::QoS(10), std::bind(&Node_CCM::Potential_obstacle_N07_subscription_callback, this, _1));
			Potential_obstacle_N05_subscription_ = this->create_subscription<std_msgs::msg::String>(
				"potential_obstacle_n05", rclcpp::QoS(10), std::bind(&Node_CCM::Potential_obstacle_N05_subscription_callback, this, _1));
			Potential_obstacle_N01_subscription_ = this->create_subscription<std_msgs::msg::String>(
				"potential_obstacle_n01", rclcpp::QoS(10), std::bind(&Node_CCM::Potential_obstacle_N01_subscription_callback, this, _1));
			Potential_obstacle_N15_subscription_ = this->create_subscription<std_msgs::msg::String>(
				"potential_obstacle_n15", rclcpp::QoS(10), std::bind(&Node_CCM::Potential_obstacle_N15_subscription_callback, this, _1));
			Potential_obstacle_N10_subscription_ = this->create_subscription<std_msgs::msg::String>(
				"potential_obstacle_n10", rclcpp::QoS(10), std::bind(&Node_CCM::Potential_obstacle_N10_subscription_callback, this, _1));
			Potential_obstacle_N14_subscription_ = this->create_subscription<std_msgs::msg::String>(
				"potential_obstacle_n14", rclcpp::QoS(10), std::bind(&Node_CCM::Potential_obstacle_N14_subscription_callback, this, _1));
			Potential_obstacle_N09_subscription_ = this->create_subscription<std_msgs::msg::String>(
				"potential_obstacle_n09", rclcpp::QoS(10), std::bind(&Node_CCM::Potential_obstacle_N09_subscription_callback, this, _1));
			Potential_obstacle_N12_subscription_ = this->create_subscription<std_msgs::msg::String>(
				"potential_obstacle_n12", rclcpp::QoS(10), std::bind(&Node_CCM::Potential_obstacle_N12_subscription_callback, this, _1));
			Potential_obstacle_N37_subscription_ = this->create_subscription<std_msgs::msg::String>(
				"potential_obstacle_n37", rclcpp::QoS(10), std::bind(&Node_CCM::Potential_obstacle_N37_subscription_callback, this, _1));
			Potential_obstacle_N48_subscription_ = this->create_subscription<std_msgs::msg::String>(
				"potential_obstacle_n48", rclcpp::QoS(10), std::bind(&Node_CCM::Potential_obstacle_N48_subscription_callback, this, _1));
			Potential_obstacle_N30_subscription_ = this->create_subscription<std_msgs::msg::String>(
				"potential_obstacle_n30", rclcpp::QoS(10), std::bind(&Node_CCM::Potential_obstacle_N30_subscription_callback, this, _1));
			Potential_obstacle_N19_subscription_ = this->create_subscription<std_msgs::msg::String>(
				"potential_obstacle_n19", rclcpp::QoS(10), std::bind(&Node_CCM::Potential_obstacle_N19_subscription_callback, this, _1));
			Potential_obstacle_N24_subscription_ = this->create_subscription<std_msgs::msg::String>(
				"potential_obstacle_n24", rclcpp::QoS(10), std::bind(&Node_CCM::Potential_obstacle_N24_subscription_callback, this, _1));
			Potential_obstacle_N29_subscription_ = this->create_subscription<std_msgs::msg::String>(
				"potential_obstacle_n29", rclcpp::QoS(10), std::bind(&Node_CCM::Potential_obstacle_N29_subscription_callback, this, _1));
			Potential_obstacle_N50_subscription_ = this->create_subscription<std_msgs::msg::String>(
				"potential_obstacle_n50", rclcpp::QoS(10), std::bind(&Node_CCM::Potential_obstacle_N50_subscription_callback, this, _1));
			Potential_obstacle_N54_subscription_ = this->create_subscription<std_msgs::msg::String>(
				"potential_obstacle_n54", rclcpp::QoS(10), std::bind(&Node_CCM::Potential_obstacle_N54_subscription_callback, this, _1));
			Potential_obstacle_N32_subscription_ = this->create_subscription<std_msgs::msg::String>(
				"potential_obstacle_n32", rclcpp::QoS(10), std::bind(&Node_CCM::Potential_obstacle_N32_subscription_callback, this, _1));
			Potential_obstacle_N23_subscription_ = this->create_subscription<std_msgs::msg::String>(
				"potential_obstacle_n23", rclcpp::QoS(10), std::bind(&Node_CCM::Potential_obstacle_N23_subscription_callback, this, _1));
			Potential_obstacle_N34_subscription_ = this->create_subscription<std_msgs::msg::String>(
				"potential_obstacle_n34", rclcpp::QoS(10), std::bind(&Node_CCM::Potential_obstacle_N34_subscription_callback, this, _1));
			Potential_obstacle_N58_subscription_ = this->create_subscription<std_msgs::msg::String>(
				"potential_obstacle_n58", rclcpp::QoS(10), std::bind(&Node_CCM::Potential_obstacle_N58_subscription_callback, this, _1));
			Potential_obstacle_N18_subscription_ = this->create_subscription<std_msgs::msg::String>(
				"potential_obstacle_n18", rclcpp::QoS(10), std::bind(&Node_CCM::Potential_obstacle_N18_subscription_callback, this, _1));
			Potential_obstacle_N46_subscription_ = this->create_subscription<std_msgs::msg::String>(
				"potential_obstacle_n46", rclcpp::QoS(10), std::bind(&Node_CCM::Potential_obstacle_N46_subscription_callback, this, _1));
			Potential_obstacle_N26_subscription_ = this->create_subscription<std_msgs::msg::String>(
				"potential_obstacle_n26", rclcpp::QoS(10), std::bind(&Node_CCM::Potential_obstacle_N26_subscription_callback, this, _1));
			Potential_obstacle_N55_subscription_ = this->create_subscription<std_msgs::msg::String>(
				"potential_obstacle_n55", rclcpp::QoS(10), std::bind(&Node_CCM::Potential_obstacle_N55_subscription_callback, this, _1));
			Potential_obstacle_N17_subscription_ = this->create_subscription<std_msgs::msg::String>(
				"potential_obstacle_n17", rclcpp::QoS(10), std::bind(&Node_CCM::Potential_obstacle_N17_subscription_callback, this, _1));
			Potential_obstacle_N47_subscription_ = this->create_subscription<std_msgs::msg::String>(
				"potential_obstacle_n47", rclcpp::QoS(10), std::bind(&Node_CCM::Potential_obstacle_N47_subscription_callback, this, _1));
			Potential_obstacle_N21_subscription_ = this->create_subscription<std_msgs::msg::String>(
				"potential_obstacle_n21", rclcpp::QoS(10), std::bind(&Node_CCM::Potential_obstacle_N21_subscription_callback, this, _1));
			Potential_obstacle_N31_subscription_ = this->create_subscription<std_msgs::msg::String>(
				"potential_obstacle_n31", rclcpp::QoS(10), std::bind(&Node_CCM::Potential_obstacle_N31_subscription_callback, this, _1));
			Potential_obstacle_N56_subscription_ = this->create_subscription<std_msgs::msg::String>(
				"potential_obstacle_n56", rclcpp::QoS(10), std::bind(&Node_CCM::Potential_obstacle_N56_subscription_callback, this, _1));
			Potential_obstacle_N35_subscription_ = this->create_subscription<std_msgs::msg::String>(
				"potential_obstacle_n35", rclcpp::QoS(10), std::bind(&Node_CCM::Potential_obstacle_N35_subscription_callback, this, _1));
			Potential_obstacle_N33_subscription_ = this->create_subscription<std_msgs::msg::String>(
				"potential_obstacle_n33", rclcpp::QoS(10), std::bind(&Node_CCM::Potential_obstacle_N33_subscription_callback, this, _1));
			Potential_obstacle_N22_subscription_ = this->create_subscription<std_msgs::msg::String>(
				"potential_obstacle_n22", rclcpp::QoS(10), std::bind(&Node_CCM::Potential_obstacle_N22_subscription_callback, this, _1));
			Potential_obstacle_N25_subscription_ = this->create_subscription<std_msgs::msg::String>(
				"potential_obstacle_n25", rclcpp::QoS(10), std::bind(&Node_CCM::Potential_obstacle_N25_subscription_callback, this, _1));
			Potential_obstacle_N39_subscription_ = this->create_subscription<std_msgs::msg::String>(
				"potential_obstacle_n39", rclcpp::QoS(10), std::bind(&Node_CCM::Potential_obstacle_N39_subscription_callback, this, _1));
			Potential_obstacle_N51_subscription_ = this->create_subscription<std_msgs::msg::String>(
				"potential_obstacle_n51", rclcpp::QoS(10), std::bind(&Node_CCM::Potential_obstacle_N51_subscription_callback, this, _1));
			Potential_obstacle_N16_subscription_ = this->create_subscription<std_msgs::msg::String>(
				"potential_obstacle_n16", rclcpp::QoS(10), std::bind(&Node_CCM::Potential_obstacle_N16_subscription_callback, this, _1));
			Potential_obstacle_N59_subscription_ = this->create_subscription<std_msgs::msg::String>(
				"potential_obstacle_n59", rclcpp::QoS(10), std::bind(&Node_CCM::Potential_obstacle_N59_subscription_callback, this, _1));
			Potential_obstacle_N20_subscription_ = this->create_subscription<std_msgs::msg::String>(
				"potential_obstacle_n20", rclcpp::QoS(10), std::bind(&Node_CCM::Potential_obstacle_N20_subscription_callback, this, _1));
			Potential_obstacle_N45_subscription_ = this->create_subscription<std_msgs::msg::String>(
				"potential_obstacle_n45", rclcpp::QoS(10), std::bind(&Node_CCM::Potential_obstacle_N45_subscription_callback, this, _1));
			Potential_obstacle_N53_subscription_ = this->create_subscription<std_msgs::msg::String>(
				"potential_obstacle_n53", rclcpp::QoS(10), std::bind(&Node_CCM::Potential_obstacle_N53_subscription_callback, this, _1));
			Potential_obstacle_N41_subscription_ = this->create_subscription<std_msgs::msg::String>(
				"potential_obstacle_n41", rclcpp::QoS(10), std::bind(&Node_CCM::Potential_obstacle_N41_subscription_callback, this, _1));
			Potential_obstacle_N36_subscription_ = this->create_subscription<std_msgs::msg::String>(
				"potential_obstacle_n36", rclcpp::QoS(10), std::bind(&Node_CCM::Potential_obstacle_N36_subscription_callback, this, _1));
			Potential_obstacle_N49_subscription_ = this->create_subscription<std_msgs::msg::String>(
				"potential_obstacle_n49", rclcpp::QoS(10), std::bind(&Node_CCM::Potential_obstacle_N49_subscription_callback, this, _1));
			Potential_obstacle_N28_subscription_ = this->create_subscription<std_msgs::msg::String>(
				"potential_obstacle_n28", rclcpp::QoS(10), std::bind(&Node_CCM::Potential_obstacle_N28_subscription_callback, this, _1));
			Potential_obstacle_N27_subscription_ = this->create_subscription<std_msgs::msg::String>(
				"potential_obstacle_n27", rclcpp::QoS(10), std::bind(&Node_CCM::Potential_obstacle_N27_subscription_callback, this, _1));
			Potential_obstacle_N52_subscription_ = this->create_subscription<std_msgs::msg::String>(
				"potential_obstacle_n52", rclcpp::QoS(10), std::bind(&Node_CCM::Potential_obstacle_N52_subscription_callback, this, _1));
			Potential_obstacle_N38_subscription_ = this->create_subscription<std_msgs::msg::String>(
				"potential_obstacle_n38", rclcpp::QoS(10), std::bind(&Node_CCM::Potential_obstacle_N38_subscription_callback, this, _1));
			Potential_obstacle_N42_subscription_ = this->create_subscription<std_msgs::msg::String>(
				"potential_obstacle_n42", rclcpp::QoS(10), std::bind(&Node_CCM::Potential_obstacle_N42_subscription_callback, this, _1));
			Potential_obstacle_N57_subscription_ = this->create_subscription<std_msgs::msg::String>(
				"potential_obstacle_n57", rclcpp::QoS(10), std::bind(&Node_CCM::Potential_obstacle_N57_subscription_callback, this, _1));
			Potential_obstacle_N44_subscription_ = this->create_subscription<std_msgs::msg::String>(
				"potential_obstacle_n44", rclcpp::QoS(10), std::bind(&Node_CCM::Potential_obstacle_N44_subscription_callback, this, _1));
			Potential_obstacle_N43_subscription_ = this->create_subscription<std_msgs::msg::String>(
				"potential_obstacle_n43", rclcpp::QoS(10), std::bind(&Node_CCM::Potential_obstacle_N43_subscription_callback, this, _1));
			Potential_obstacle_N40_subscription_ = this->create_subscription<std_msgs::msg::String>(
				"potential_obstacle_n40", rclcpp::QoS(10), std::bind(&Node_CCM::Potential_obstacle_N40_subscription_callback, this, _1));
		}
	void Potential_obstacle_N00_subscription_callback(const std_msgs::msg::String::SharedPtr msg) {
	#ifdef CONSOLE_ENABLED
		std::cout << "ROS2: subscription callback " << msg->data << std::endl ;
	#endif
		int dummy;
		
		#if defined(_OPENMP)
		#pragma omp taskgraph tdg_type(static) nowait
		#endif
		{
		#if defined(_OPENMP)
		 #pragma omp task replicated(2, dummy, check_Node_CCM)
		#elif defined(_OMPSS)
		 #pragma oss task replicated(2, dummy, check_Node_CCM)
		#endif
		run_Collision_checking();
		}
		#pragma omp taskwait;
	}
	
	void Potential_obstacle_N03_subscription_callback(const std_msgs::msg::String::SharedPtr msg) {
	#ifdef CONSOLE_ENABLED
		std::cout << "ROS2: subscription callback " << msg->data << std::endl ;
	#endif
		int dummy;
		
		 #if defined(_OPENMP)
		 // #pragma omp taskgraph tdg_type(static) nowait
		#endif
		{
		#if defined(_OPENMP)
		 #pragma omp task replicated(2, dummy, check_Node_CCM)
		#elif defined(_OMPSS)
		 #pragma oss task replicated(2, dummy, check_Node_CCM)
		#endif
		run_Collision_checking();
		}
		#pragma omp taskwait;
	}
	
	void Potential_obstacle_N06_subscription_callback(const std_msgs::msg::String::SharedPtr msg) {
	#ifdef CONSOLE_ENABLED
		std::cout << "ROS2: subscription callback " << msg->data << std::endl ;
	#endif
		int dummy;
		
		 #if defined(_OPENMP)
		 // #pragma omp taskgraph tdg_type(static) nowait
		#endif
		{
		#if defined(_OPENMP)
		 #pragma omp task replicated(2, dummy, check_Node_CCM)
		#elif defined(_OMPSS)
		 #pragma oss task replicated(2, dummy, check_Node_CCM)
		#endif
		run_Collision_checking();
		}
		#pragma omp taskwait;
	}
	
	void Potential_obstacle_N08_subscription_callback(const std_msgs::msg::String::SharedPtr msg) {
	#ifdef CONSOLE_ENABLED
		std::cout << "ROS2: subscription callback " << msg->data << std::endl ;
	#endif
		int dummy;
		
		 #if defined(_OPENMP)
		 // #pragma omp taskgraph tdg_type(static) nowait
		#endif
		{
		#if defined(_OPENMP)
		 #pragma omp task replicated(2, dummy, check_Node_CCM)
		#elif defined(_OMPSS)
		 #pragma oss task replicated(2, dummy, check_Node_CCM)
		#endif
		run_Collision_checking();
		}
		#pragma omp taskwait;
	}
	
	void Potential_obstacle_N02_subscription_callback(const std_msgs::msg::String::SharedPtr msg) {
	#ifdef CONSOLE_ENABLED
		std::cout << "ROS2: subscription callback " << msg->data << std::endl ;
	#endif
		int dummy;
		
		 #if defined(_OPENMP)
		 // #pragma omp taskgraph tdg_type(static) nowait
		#endif
		{
		#if defined(_OPENMP)
		 #pragma omp task replicated(2, dummy, check_Node_CCM)
		#elif defined(_OMPSS)
		 #pragma oss task replicated(2, dummy, check_Node_CCM)
		#endif
		run_Collision_checking();
		}
		#pragma omp taskwait;
	}
	
	void Potential_obstacle_N13_subscription_callback(const std_msgs::msg::String::SharedPtr msg) {
	#ifdef CONSOLE_ENABLED
		std::cout << "ROS2: subscription callback " << msg->data << std::endl ;
	#endif
		int dummy;
		
		 #if defined(_OPENMP)
		 // #pragma omp taskgraph tdg_type(static) nowait
		#endif
		{
		#if defined(_OPENMP)
		 #pragma omp task replicated(2, dummy, check_Node_CCM)
		#elif defined(_OMPSS)
		 #pragma oss task replicated(2, dummy, check_Node_CCM)
		#endif
		run_Collision_checking();
		}
		#pragma omp taskwait;
	}
	
	void Potential_obstacle_N04_subscription_callback(const std_msgs::msg::String::SharedPtr msg) {
	#ifdef CONSOLE_ENABLED
		std::cout << "ROS2: subscription callback " << msg->data << std::endl ;
	#endif
		int dummy;
		
		 #if defined(_OPENMP)
		 // #pragma omp taskgraph tdg_type(static) nowait
		#endif
		{
		#if defined(_OPENMP)
		 #pragma omp task replicated(2, dummy, check_Node_CCM)
		#elif defined(_OMPSS)
		 #pragma oss task replicated(2, dummy, check_Node_CCM)
		#endif
		run_Collision_checking();
		}
		#pragma omp taskwait;
	}
	
	void Potential_obstacle_N11_subscription_callback(const std_msgs::msg::String::SharedPtr msg) {
	#ifdef CONSOLE_ENABLED
		std::cout << "ROS2: subscription callback " << msg->data << std::endl ;
	#endif
		int dummy;
		
		 #if defined(_OPENMP)
		 // #pragma omp taskgraph tdg_type(static) nowait
		#endif
		{
		#if defined(_OPENMP)
		 #pragma omp task replicated(2, dummy, check_Node_CCM)
		#elif defined(_OMPSS)
		 #pragma oss task replicated(2, dummy, check_Node_CCM)
		#endif
		run_Collision_checking();
		}
		#pragma omp taskwait;
	}
	
	void Potential_obstacle_N07_subscription_callback(const std_msgs::msg::String::SharedPtr msg) {
	#ifdef CONSOLE_ENABLED
		std::cout << "ROS2: subscription callback " << msg->data << std::endl ;
	#endif
		int dummy;
		
		 #if defined(_OPENMP)
		 // #pragma omp taskgraph tdg_type(static) nowait
		#endif
		{
		#if defined(_OPENMP)
		 #pragma omp task replicated(2, dummy, check_Node_CCM)
		#elif defined(_OMPSS)
		 #pragma oss task replicated(2, dummy, check_Node_CCM)
		#endif
		run_Collision_checking();
		}
		#pragma omp taskwait;
	}
	
	void Potential_obstacle_N05_subscription_callback(const std_msgs::msg::String::SharedPtr msg) {
	#ifdef CONSOLE_ENABLED
		std::cout << "ROS2: subscription callback " << msg->data << std::endl ;
	#endif
		int dummy;
		
		 #if defined(_OPENMP)
		 // #pragma omp taskgraph tdg_type(static) nowait
		#endif
		{
		#if defined(_OPENMP)
		 #pragma omp task replicated(2, dummy, check_Node_CCM)
		#elif defined(_OMPSS)
		 #pragma oss task replicated(2, dummy, check_Node_CCM)
		#endif
		run_Collision_checking();
		}
		#pragma omp taskwait;
	}
	
	void Potential_obstacle_N01_subscription_callback(const std_msgs::msg::String::SharedPtr msg) {
	#ifdef CONSOLE_ENABLED
		std::cout << "ROS2: subscription callback " << msg->data << std::endl ;
	#endif
		int dummy;
		
		 #if defined(_OPENMP)
		 // #pragma omp taskgraph tdg_type(static) nowait
		#endif
		{
		#if defined(_OPENMP)
		 #pragma omp task replicated(2, dummy, check_Node_CCM)
		#elif defined(_OMPSS)
		 #pragma oss task replicated(2, dummy, check_Node_CCM)
		#endif
		run_Collision_checking();
		}
		#pragma omp taskwait;
	}
	
	void Potential_obstacle_N15_subscription_callback(const std_msgs::msg::String::SharedPtr msg) {
	#ifdef CONSOLE_ENABLED
		std::cout << "ROS2: subscription callback " << msg->data << std::endl ;
	#endif
		int dummy;
		
		 #if defined(_OPENMP)
		 // #pragma omp taskgraph tdg_type(static) nowait
		#endif
		{
		#if defined(_OPENMP)
		 #pragma omp task replicated(2, dummy, check_Node_CCM)
		#elif defined(_OMPSS)
		 #pragma oss task replicated(2, dummy, check_Node_CCM)
		#endif
		run_Collision_checking();
		}
		#pragma omp taskwait;
	}
	
	void Potential_obstacle_N10_subscription_callback(const std_msgs::msg::String::SharedPtr msg) {
	#ifdef CONSOLE_ENABLED
		std::cout << "ROS2: subscription callback " << msg->data << std::endl ;
	#endif
		int dummy;
		
		 #if defined(_OPENMP)
		 // #pragma omp taskgraph tdg_type(static) nowait
		#endif
		{
		#if defined(_OPENMP)
		 #pragma omp task replicated(2, dummy, check_Node_CCM)
		#elif defined(_OMPSS)
		 #pragma oss task replicated(2, dummy, check_Node_CCM)
		#endif
		run_Collision_checking();
		}
		#pragma omp taskwait;
	}
	
	void Potential_obstacle_N14_subscription_callback(const std_msgs::msg::String::SharedPtr msg) {
	#ifdef CONSOLE_ENABLED
		std::cout << "ROS2: subscription callback " << msg->data << std::endl ;
	#endif
		int dummy;
		
		 #if defined(_OPENMP)
		 // #pragma omp taskgraph tdg_type(static) nowait
		#endif
		{
		#if defined(_OPENMP)
		 #pragma omp task replicated(2, dummy, check_Node_CCM)
		#elif defined(_OMPSS)
		 #pragma oss task replicated(2, dummy, check_Node_CCM)
		#endif
		run_Collision_checking();
		}
		#pragma omp taskwait;
	}
	
	void Potential_obstacle_N09_subscription_callback(const std_msgs::msg::String::SharedPtr msg) {
	#ifdef CONSOLE_ENABLED
		std::cout << "ROS2: subscription callback " << msg->data << std::endl ;
	#endif
		int dummy;
		
		 #if defined(_OPENMP)
		 // #pragma omp taskgraph tdg_type(static) nowait
		#endif
		{
		#if defined(_OPENMP)
		 #pragma omp task replicated(2, dummy, check_Node_CCM)
		#elif defined(_OMPSS)
		 #pragma oss task replicated(2, dummy, check_Node_CCM)
		#endif
		run_Collision_checking();
		}
		#pragma omp taskwait;
	}
	
	void Potential_obstacle_N12_subscription_callback(const std_msgs::msg::String::SharedPtr msg) {
	#ifdef CONSOLE_ENABLED
		std::cout << "ROS2: subscription callback " << msg->data << std::endl ;
	#endif
		int dummy;
		
		 #if defined(_OPENMP)
		 // #pragma omp taskgraph tdg_type(static) nowait
		#endif
		{
		#if defined(_OPENMP)
		 #pragma omp task replicated(2, dummy, check_Node_CCM)
		#elif defined(_OMPSS)
		 #pragma oss task replicated(2, dummy, check_Node_CCM)
		#endif
		run_Collision_checking();
		}
		#pragma omp taskwait;
	}
	
	void Potential_obstacle_N37_subscription_callback(const std_msgs::msg::String::SharedPtr msg) {
	#ifdef CONSOLE_ENABLED
		std::cout << "ROS2: subscription callback " << msg->data << std::endl ;
	#endif
		int dummy;
		
		 #if defined(_OPENMP)
		 // #pragma omp taskgraph tdg_type(static) nowait
		#endif
		{
		#if defined(_OPENMP)
		 #pragma omp task replicated(2, dummy, check_Node_CCM)
		#elif defined(_OMPSS)
		 #pragma oss task replicated(2, dummy, check_Node_CCM)
		#endif
		run_Collision_checking();
		}
		#pragma omp taskwait;
	}
	
	void Potential_obstacle_N48_subscription_callback(const std_msgs::msg::String::SharedPtr msg) {
	#ifdef CONSOLE_ENABLED
		std::cout << "ROS2: subscription callback " << msg->data << std::endl ;
	#endif
		int dummy;
		
		 #if defined(_OPENMP)
		 // #pragma omp taskgraph tdg_type(static) nowait
		#endif
		{
		#if defined(_OPENMP)
		 #pragma omp task replicated(2, dummy, check_Node_CCM)
		#elif defined(_OMPSS)
		 #pragma oss task replicated(2, dummy, check_Node_CCM)
		#endif
		run_Collision_checking();
		}
		#pragma omp taskwait;
	}
	
	void Potential_obstacle_N30_subscription_callback(const std_msgs::msg::String::SharedPtr msg) {
	#ifdef CONSOLE_ENABLED
		std::cout << "ROS2: subscription callback " << msg->data << std::endl ;
	#endif
		int dummy;
		
		 #if defined(_OPENMP)
		 // #pragma omp taskgraph tdg_type(static) nowait
		#endif
		{
		#if defined(_OPENMP)
		 #pragma omp task replicated(2, dummy, check_Node_CCM)
		#elif defined(_OMPSS)
		 #pragma oss task replicated(2, dummy, check_Node_CCM)
		#endif
		run_Collision_checking();
		}
		#pragma omp taskwait;
	}
	
	void Potential_obstacle_N19_subscription_callback(const std_msgs::msg::String::SharedPtr msg) {
	#ifdef CONSOLE_ENABLED
		std::cout << "ROS2: subscription callback " << msg->data << std::endl ;
	#endif
		int dummy;
		
		 #if defined(_OPENMP)
		 // #pragma omp taskgraph tdg_type(static) nowait
		#endif
		{
		#if defined(_OPENMP)
		 #pragma omp task replicated(2, dummy, check_Node_CCM)
		#elif defined(_OMPSS)
		 #pragma oss task replicated(2, dummy, check_Node_CCM)
		#endif
		run_Collision_checking();
		}
		#pragma omp taskwait;
	}
	
	void Potential_obstacle_N24_subscription_callback(const std_msgs::msg::String::SharedPtr msg) {
	#ifdef CONSOLE_ENABLED
		std::cout << "ROS2: subscription callback " << msg->data << std::endl ;
	#endif
		int dummy;
		
		 #if defined(_OPENMP)
		 // #pragma omp taskgraph tdg_type(static) nowait
		#endif
		{
		#if defined(_OPENMP)
		 #pragma omp task replicated(2, dummy, check_Node_CCM)
		#elif defined(_OMPSS)
		 #pragma oss task replicated(2, dummy, check_Node_CCM)
		#endif
		run_Collision_checking();
		}
		#pragma omp taskwait;
	}
	
	void Potential_obstacle_N29_subscription_callback(const std_msgs::msg::String::SharedPtr msg) {
	#ifdef CONSOLE_ENABLED
		std::cout << "ROS2: subscription callback " << msg->data << std::endl ;
	#endif
		int dummy;
		
		 #if defined(_OPENMP)
		 // #pragma omp taskgraph tdg_type(static) nowait
		#endif
		{
		#if defined(_OPENMP)
		 #pragma omp task replicated(2, dummy, check_Node_CCM)
		#elif defined(_OMPSS)
		 #pragma oss task replicated(2, dummy, check_Node_CCM)
		#endif
		run_Collision_checking();
		}
		#pragma omp taskwait;
	}
	
	void Potential_obstacle_N50_subscription_callback(const std_msgs::msg::String::SharedPtr msg) {
	#ifdef CONSOLE_ENABLED
		std::cout << "ROS2: subscription callback " << msg->data << std::endl ;
	#endif
		int dummy;
		
		 #if defined(_OPENMP)
		 // #pragma omp taskgraph tdg_type(static) nowait
		#endif
		{
		#if defined(_OPENMP)
		 #pragma omp task replicated(2, dummy, check_Node_CCM)
		#elif defined(_OMPSS)
		 #pragma oss task replicated(2, dummy, check_Node_CCM)
		#endif
		run_Collision_checking();
		}
		#pragma omp taskwait;
	}
	
	void Potential_obstacle_N54_subscription_callback(const std_msgs::msg::String::SharedPtr msg) {
	#ifdef CONSOLE_ENABLED
		std::cout << "ROS2: subscription callback " << msg->data << std::endl ;
	#endif
		int dummy;
		
		 #if defined(_OPENMP)
		 // #pragma omp taskgraph tdg_type(static) nowait
		#endif
		{
		#if defined(_OPENMP)
		 #pragma omp task replicated(2, dummy, check_Node_CCM)
		#elif defined(_OMPSS)
		 #pragma oss task replicated(2, dummy, check_Node_CCM)
		#endif
		run_Collision_checking();
		}
		#pragma omp taskwait;
	}
	
	void Potential_obstacle_N32_subscription_callback(const std_msgs::msg::String::SharedPtr msg) {
	#ifdef CONSOLE_ENABLED
		std::cout << "ROS2: subscription callback " << msg->data << std::endl ;
	#endif
		int dummy;
		
		 #if defined(_OPENMP)
		 // #pragma omp taskgraph tdg_type(static) nowait
		#endif
		{
		#if defined(_OPENMP)
		 #pragma omp task replicated(2, dummy, check_Node_CCM)
		#elif defined(_OMPSS)
		 #pragma oss task replicated(2, dummy, check_Node_CCM)
		#endif
		run_Collision_checking();
		}
		#pragma omp taskwait;
	}
	
	void Potential_obstacle_N23_subscription_callback(const std_msgs::msg::String::SharedPtr msg) {
	#ifdef CONSOLE_ENABLED
		std::cout << "ROS2: subscription callback " << msg->data << std::endl ;
	#endif
		int dummy;
		
		 #if defined(_OPENMP)
		 // #pragma omp taskgraph tdg_type(static) nowait
		#endif
		{
		#if defined(_OPENMP)
		 #pragma omp task replicated(2, dummy, check_Node_CCM)
		#elif defined(_OMPSS)
		 #pragma oss task replicated(2, dummy, check_Node_CCM)
		#endif
		run_Collision_checking();
		}
		#pragma omp taskwait;
	}
	
	void Potential_obstacle_N34_subscription_callback(const std_msgs::msg::String::SharedPtr msg) {
	#ifdef CONSOLE_ENABLED
		std::cout << "ROS2: subscription callback " << msg->data << std::endl ;
	#endif
		int dummy;
		
		 #if defined(_OPENMP)
		 // #pragma omp taskgraph tdg_type(static) nowait
		#endif
		{
		#if defined(_OPENMP)
		 #pragma omp task replicated(2, dummy, check_Node_CCM)
		#elif defined(_OMPSS)
		 #pragma oss task replicated(2, dummy, check_Node_CCM)
		#endif
		run_Collision_checking();
		}
		#pragma omp taskwait;
	}
	
	void Potential_obstacle_N58_subscription_callback(const std_msgs::msg::String::SharedPtr msg) {
	#ifdef CONSOLE_ENABLED
		std::cout << "ROS2: subscription callback " << msg->data << std::endl ;
	#endif
		int dummy;
		
		 #if defined(_OPENMP)
		 // #pragma omp taskgraph tdg_type(static) nowait
		#endif
		{
		#if defined(_OPENMP)
		 #pragma omp task replicated(2, dummy, check_Node_CCM)
		#elif defined(_OMPSS)
		 #pragma oss task replicated(2, dummy, check_Node_CCM)
		#endif
		run_Collision_checking();
		}
		#pragma omp taskwait;
	}
	
	void Potential_obstacle_N18_subscription_callback(const std_msgs::msg::String::SharedPtr msg) {
	#ifdef CONSOLE_ENABLED
		std::cout << "ROS2: subscription callback " << msg->data << std::endl ;
	#endif
		int dummy;
		
		 #if defined(_OPENMP)
		 // #pragma omp taskgraph tdg_type(static) nowait
		#endif
		{
		#if defined(_OPENMP)
		 #pragma omp task replicated(2, dummy, check_Node_CCM)
		#elif defined(_OMPSS)
		 #pragma oss task replicated(2, dummy, check_Node_CCM)
		#endif
		run_Collision_checking();
		}
		#pragma omp taskwait;
	}
	
	void Potential_obstacle_N46_subscription_callback(const std_msgs::msg::String::SharedPtr msg) {
	#ifdef CONSOLE_ENABLED
		std::cout << "ROS2: subscription callback " << msg->data << std::endl ;
	#endif
		int dummy;
		
		 #if defined(_OPENMP)
		 // #pragma omp taskgraph tdg_type(static) nowait
		#endif
		{
		#if defined(_OPENMP)
		 #pragma omp task replicated(2, dummy, check_Node_CCM)
		#elif defined(_OMPSS)
		 #pragma oss task replicated(2, dummy, check_Node_CCM)
		#endif
		run_Collision_checking();
		}
		#pragma omp taskwait;
	}
	
	void Potential_obstacle_N26_subscription_callback(const std_msgs::msg::String::SharedPtr msg) {
	#ifdef CONSOLE_ENABLED
		std::cout << "ROS2: subscription callback " << msg->data << std::endl ;
	#endif
		int dummy;
		
		 #if defined(_OPENMP)
		 // #pragma omp taskgraph tdg_type(static) nowait
		#endif
		{
		#if defined(_OPENMP)
		 #pragma omp task replicated(2, dummy, check_Node_CCM)
		#elif defined(_OMPSS)
		 #pragma oss task replicated(2, dummy, check_Node_CCM)
		#endif
		run_Collision_checking();
		}
		#pragma omp taskwait;
	}
	
	void Potential_obstacle_N55_subscription_callback(const std_msgs::msg::String::SharedPtr msg) {
	#ifdef CONSOLE_ENABLED
		std::cout << "ROS2: subscription callback " << msg->data << std::endl ;
	#endif
		int dummy;
		
		 #if defined(_OPENMP)
		 // #pragma omp taskgraph tdg_type(static) nowait
		#endif
		{
		#if defined(_OPENMP)
		 #pragma omp task replicated(2, dummy, check_Node_CCM)
		#elif defined(_OMPSS)
		 #pragma oss task replicated(2, dummy, check_Node_CCM)
		#endif
		run_Collision_checking();
		}
		#pragma omp taskwait;
	}
	
	void Potential_obstacle_N17_subscription_callback(const std_msgs::msg::String::SharedPtr msg) {
	#ifdef CONSOLE_ENABLED
		std::cout << "ROS2: subscription callback " << msg->data << std::endl ;
	#endif
		int dummy;
		
		 #if defined(_OPENMP)
		 // #pragma omp taskgraph tdg_type(static) nowait
		#endif
		{
		#if defined(_OPENMP)
		 #pragma omp task replicated(2, dummy, check_Node_CCM)
		#elif defined(_OMPSS)
		 #pragma oss task replicated(2, dummy, check_Node_CCM)
		#endif
		run_Collision_checking();
		}
		#pragma omp taskwait;
	}
	
	void Potential_obstacle_N47_subscription_callback(const std_msgs::msg::String::SharedPtr msg) {
	#ifdef CONSOLE_ENABLED
		std::cout << "ROS2: subscription callback " << msg->data << std::endl ;
	#endif
		int dummy;
		
		 #if defined(_OPENMP)
		 // #pragma omp taskgraph tdg_type(static) nowait
		#endif
		{
		#if defined(_OPENMP)
		 #pragma omp task replicated(2, dummy, check_Node_CCM)
		#elif defined(_OMPSS)
		 #pragma oss task replicated(2, dummy, check_Node_CCM)
		#endif
		run_Collision_checking();
		}
		#pragma omp taskwait;
	}
	
	void Potential_obstacle_N21_subscription_callback(const std_msgs::msg::String::SharedPtr msg) {
	#ifdef CONSOLE_ENABLED
		std::cout << "ROS2: subscription callback " << msg->data << std::endl ;
	#endif
		int dummy;
		
		 #if defined(_OPENMP)
		 // #pragma omp taskgraph tdg_type(static) nowait
		#endif
		{
		#if defined(_OPENMP)
		 #pragma omp task replicated(2, dummy, check_Node_CCM)
		#elif defined(_OMPSS)
		 #pragma oss task replicated(2, dummy, check_Node_CCM)
		#endif
		run_Collision_checking();
		}
		#pragma omp taskwait;
	}
	
	void Potential_obstacle_N31_subscription_callback(const std_msgs::msg::String::SharedPtr msg) {
	#ifdef CONSOLE_ENABLED
		std::cout << "ROS2: subscription callback " << msg->data << std::endl ;
	#endif
		int dummy;
		
		 #if defined(_OPENMP)
		 // #pragma omp taskgraph tdg_type(static) nowait
		#endif
		{
		#if defined(_OPENMP)
		 #pragma omp task replicated(2, dummy, check_Node_CCM)
		#elif defined(_OMPSS)
		 #pragma oss task replicated(2, dummy, check_Node_CCM)
		#endif
		run_Collision_checking();
		}
		#pragma omp taskwait;
	}
	
	void Potential_obstacle_N56_subscription_callback(const std_msgs::msg::String::SharedPtr msg) {
	#ifdef CONSOLE_ENABLED
		std::cout << "ROS2: subscription callback " << msg->data << std::endl ;
	#endif
		int dummy;
		
		 #if defined(_OPENMP)
		 // #pragma omp taskgraph tdg_type(static) nowait
		#endif
		{
		#if defined(_OPENMP)
		 #pragma omp task replicated(2, dummy, check_Node_CCM)
		#elif defined(_OMPSS)
		 #pragma oss task replicated(2, dummy, check_Node_CCM)
		#endif
		run_Collision_checking();
		}
		#pragma omp taskwait;
	}
	
	void Potential_obstacle_N35_subscription_callback(const std_msgs::msg::String::SharedPtr msg) {
	#ifdef CONSOLE_ENABLED
		std::cout << "ROS2: subscription callback " << msg->data << std::endl ;
	#endif
		int dummy;
		
		 #if defined(_OPENMP)
		 // #pragma omp taskgraph tdg_type(static) nowait
		#endif
		{
		#if defined(_OPENMP)
		 #pragma omp task replicated(2, dummy, check_Node_CCM)
		#elif defined(_OMPSS)
		 #pragma oss task replicated(2, dummy, check_Node_CCM)
		#endif
		run_Collision_checking();
		}
		#pragma omp taskwait;
	}
	
	void Potential_obstacle_N33_subscription_callback(const std_msgs::msg::String::SharedPtr msg) {
	#ifdef CONSOLE_ENABLED
		std::cout << "ROS2: subscription callback " << msg->data << std::endl ;
	#endif
		int dummy;
		
		 #if defined(_OPENMP)
		 // #pragma omp taskgraph tdg_type(static) nowait
		#endif
		{
		#if defined(_OPENMP)
		 #pragma omp task replicated(2, dummy, check_Node_CCM)
		#elif defined(_OMPSS)
		 #pragma oss task replicated(2, dummy, check_Node_CCM)
		#endif
		run_Collision_checking();
		}
		#pragma omp taskwait;
	}
	
	void Potential_obstacle_N22_subscription_callback(const std_msgs::msg::String::SharedPtr msg) {
	#ifdef CONSOLE_ENABLED
		std::cout << "ROS2: subscription callback " << msg->data << std::endl ;
	#endif
		int dummy;
		
		 #if defined(_OPENMP)
		 // #pragma omp taskgraph tdg_type(static) nowait
		#endif
		{
		#if defined(_OPENMP)
		 #pragma omp task replicated(2, dummy, check_Node_CCM)
		#elif defined(_OMPSS)
		 #pragma oss task replicated(2, dummy, check_Node_CCM)
		#endif
		run_Collision_checking();
		}
		#pragma omp taskwait;
	}
	
	void Potential_obstacle_N25_subscription_callback(const std_msgs::msg::String::SharedPtr msg) {
	#ifdef CONSOLE_ENABLED
		std::cout << "ROS2: subscription callback " << msg->data << std::endl ;
	#endif
		int dummy;
		
		 #if defined(_OPENMP)
		 // #pragma omp taskgraph tdg_type(static) nowait
		#endif
		{
		#if defined(_OPENMP)
		 #pragma omp task replicated(2, dummy, check_Node_CCM)
		#elif defined(_OMPSS)
		 #pragma oss task replicated(2, dummy, check_Node_CCM)
		#endif
		run_Collision_checking();
		}
		#pragma omp taskwait;
	}
	
	void Potential_obstacle_N39_subscription_callback(const std_msgs::msg::String::SharedPtr msg) {
	#ifdef CONSOLE_ENABLED
		std::cout << "ROS2: subscription callback " << msg->data << std::endl ;
	#endif
		int dummy;
		
		 #if defined(_OPENMP)
		 // #pragma omp taskgraph tdg_type(static) nowait
		#endif
		{
		#if defined(_OPENMP)
		 #pragma omp task replicated(2, dummy, check_Node_CCM)
		#elif defined(_OMPSS)
		 #pragma oss task replicated(2, dummy, check_Node_CCM)
		#endif
		run_Collision_checking();
		}
		#pragma omp taskwait;
	}
	
	void Potential_obstacle_N51_subscription_callback(const std_msgs::msg::String::SharedPtr msg) {
	#ifdef CONSOLE_ENABLED
		std::cout << "ROS2: subscription callback " << msg->data << std::endl ;
	#endif
		int dummy;
		
		 #if defined(_OPENMP)
		 // #pragma omp taskgraph tdg_type(static) nowait
		#endif
		{
		#if defined(_OPENMP)
		 #pragma omp task replicated(2, dummy, check_Node_CCM)
		#elif defined(_OMPSS)
		 #pragma oss task replicated(2, dummy, check_Node_CCM)
		#endif
		run_Collision_checking();
		}
		#pragma omp taskwait;
	}
	
	void Potential_obstacle_N16_subscription_callback(const std_msgs::msg::String::SharedPtr msg) {
	#ifdef CONSOLE_ENABLED
		std::cout << "ROS2: subscription callback " << msg->data << std::endl ;
	#endif
		int dummy;
		
		 #if defined(_OPENMP)
		 // #pragma omp taskgraph tdg_type(static) nowait
		#endif
		{
		#if defined(_OPENMP)
		 #pragma omp task replicated(2, dummy, check_Node_CCM)
		#elif defined(_OMPSS)
		 #pragma oss task replicated(2, dummy, check_Node_CCM)
		#endif
		run_Collision_checking();
		}
		#pragma omp taskwait;
	}
	
	void Potential_obstacle_N59_subscription_callback(const std_msgs::msg::String::SharedPtr msg) {
	#ifdef CONSOLE_ENABLED
		std::cout << "ROS2: subscription callback " << msg->data << std::endl ;
	#endif
		int dummy;
		
		 #if defined(_OPENMP)
		 // #pragma omp taskgraph tdg_type(static) nowait
		#endif
		{
		#if defined(_OPENMP)
		 #pragma omp task replicated(2, dummy, check_Node_CCM)
		#elif defined(_OMPSS)
		 #pragma oss task replicated(2, dummy, check_Node_CCM)
		#endif
		run_Collision_checking();
		}
		#pragma omp taskwait;
	}
	
	void Potential_obstacle_N20_subscription_callback(const std_msgs::msg::String::SharedPtr msg) {
	#ifdef CONSOLE_ENABLED
		std::cout << "ROS2: subscription callback " << msg->data << std::endl ;
	#endif
		int dummy;
		
		 #if defined(_OPENMP)
		 // #pragma omp taskgraph tdg_type(static) nowait
		#endif
		{
		#if defined(_OPENMP)
		 #pragma omp task replicated(2, dummy, check_Node_CCM)
		#elif defined(_OMPSS)
		 #pragma oss task replicated(2, dummy, check_Node_CCM)
		#endif
		run_Collision_checking();
		}
		#pragma omp taskwait;
	}
	
	void Potential_obstacle_N45_subscription_callback(const std_msgs::msg::String::SharedPtr msg) {
	#ifdef CONSOLE_ENABLED
		std::cout << "ROS2: subscription callback " << msg->data << std::endl ;
	#endif
		int dummy;
		
		 #if defined(_OPENMP)
		 // #pragma omp taskgraph tdg_type(static) nowait
		#endif
		{
		#if defined(_OPENMP)
		 #pragma omp task replicated(2, dummy, check_Node_CCM)
		#elif defined(_OMPSS)
		 #pragma oss task replicated(2, dummy, check_Node_CCM)
		#endif
		run_Collision_checking();
		}
		#pragma omp taskwait;
	}
	
	void Potential_obstacle_N53_subscription_callback(const std_msgs::msg::String::SharedPtr msg) {
	#ifdef CONSOLE_ENABLED
		std::cout << "ROS2: subscription callback " << msg->data << std::endl ;
	#endif
		int dummy;
		
		 #if defined(_OPENMP)
		 // #pragma omp taskgraph tdg_type(static) nowait
		#endif
		{
		#if defined(_OPENMP)
		 #pragma omp task replicated(2, dummy, check_Node_CCM)
		#elif defined(_OMPSS)
		 #pragma oss task replicated(2, dummy, check_Node_CCM)
		#endif
		run_Collision_checking();
		}
		#pragma omp taskwait;
	}
	
	void Potential_obstacle_N41_subscription_callback(const std_msgs::msg::String::SharedPtr msg) {
	#ifdef CONSOLE_ENABLED
		std::cout << "ROS2: subscription callback " << msg->data << std::endl ;
	#endif
		int dummy;
		
		 #if defined(_OPENMP)
		 // #pragma omp taskgraph tdg_type(static) nowait
		#endif
		{
		#if defined(_OPENMP)
		 #pragma omp task replicated(2, dummy, check_Node_CCM)
		#elif defined(_OMPSS)
		 #pragma oss task replicated(2, dummy, check_Node_CCM)
		#endif
		run_Collision_checking();
		}
		#pragma omp taskwait;
	}
	
	void Potential_obstacle_N36_subscription_callback(const std_msgs::msg::String::SharedPtr msg) {
	#ifdef CONSOLE_ENABLED
		std::cout << "ROS2: subscription callback " << msg->data << std::endl ;
	#endif
		int dummy;
		
		 #if defined(_OPENMP)
		 // #pragma omp taskgraph tdg_type(static) nowait
		#endif
		{
		#if defined(_OPENMP)
		 #pragma omp task replicated(2, dummy, check_Node_CCM)
		#elif defined(_OMPSS)
		 #pragma oss task replicated(2, dummy, check_Node_CCM)
		#endif
		run_Collision_checking();
		}
		#pragma omp taskwait;
	}
	
	void Potential_obstacle_N49_subscription_callback(const std_msgs::msg::String::SharedPtr msg) {
	#ifdef CONSOLE_ENABLED
		std::cout << "ROS2: subscription callback " << msg->data << std::endl ;
	#endif
		int dummy;
		
		 #if defined(_OPENMP)
		 // #pragma omp taskgraph tdg_type(static) nowait
		#endif
		{
		#if defined(_OPENMP)
		 #pragma omp task replicated(2, dummy, check_Node_CCM)
		#elif defined(_OMPSS)
		 #pragma oss task replicated(2, dummy, check_Node_CCM)
		#endif
		run_Collision_checking();
		}
		#pragma omp taskwait;
	}
	
	void Potential_obstacle_N28_subscription_callback(const std_msgs::msg::String::SharedPtr msg) {
	#ifdef CONSOLE_ENABLED
		std::cout << "ROS2: subscription callback " << msg->data << std::endl ;
	#endif
		int dummy;
		
		 #if defined(_OPENMP)
		 // #pragma omp taskgraph tdg_type(static) nowait
		#endif
		{
		#if defined(_OPENMP)
		 #pragma omp task replicated(2, dummy, check_Node_CCM)
		#elif defined(_OMPSS)
		 #pragma oss task replicated(2, dummy, check_Node_CCM)
		#endif
		run_Collision_checking();
		}
		#pragma omp taskwait;
	}
	
	void Potential_obstacle_N27_subscription_callback(const std_msgs::msg::String::SharedPtr msg) {
	#ifdef CONSOLE_ENABLED
		std::cout << "ROS2: subscription callback " << msg->data << std::endl ;
	#endif
		int dummy;
		
		 #if defined(_OPENMP)
		 // #pragma omp taskgraph tdg_type(static) nowait
		#endif
		{
		#if defined(_OPENMP)
		 #pragma omp task replicated(2, dummy, check_Node_CCM)
		#elif defined(_OMPSS)
		 #pragma oss task replicated(2, dummy, check_Node_CCM)
		#endif
		run_Collision_checking();
		}
		#pragma omp taskwait;
	}
	
	void Potential_obstacle_N52_subscription_callback(const std_msgs::msg::String::SharedPtr msg) {
	#ifdef CONSOLE_ENABLED
		std::cout << "ROS2: subscription callback " << msg->data << std::endl ;
	#endif
		int dummy;
		
		 #if defined(_OPENMP)
		 // #pragma omp taskgraph tdg_type(static) nowait
		#endif
		{
		#if defined(_OPENMP)
		 #pragma omp task replicated(2, dummy, check_Node_CCM)
		#elif defined(_OMPSS)
		 #pragma oss task replicated(2, dummy, check_Node_CCM)
		#endif
		run_Collision_checking();
		}
		#pragma omp taskwait;
	}
	
	void Potential_obstacle_N38_subscription_callback(const std_msgs::msg::String::SharedPtr msg) {
	#ifdef CONSOLE_ENABLED
		std::cout << "ROS2: subscription callback " << msg->data << std::endl ;
	#endif
		int dummy;
		
		 #if defined(_OPENMP)
		 // #pragma omp taskgraph tdg_type(static) nowait
		#endif
		{
		#if defined(_OPENMP)
		 #pragma omp task replicated(2, dummy, check_Node_CCM)
		#elif defined(_OMPSS)
		 #pragma oss task replicated(2, dummy, check_Node_CCM)
		#endif
		run_Collision_checking();
		}
		#pragma omp taskwait;
	}
	
	void Potential_obstacle_N42_subscription_callback(const std_msgs::msg::String::SharedPtr msg) {
	#ifdef CONSOLE_ENABLED
		std::cout << "ROS2: subscription callback " << msg->data << std::endl ;
	#endif
		int dummy;
		
		 #if defined(_OPENMP)
		 // #pragma omp taskgraph tdg_type(static) nowait
		#endif
		{
		#if defined(_OPENMP)
		 #pragma omp task replicated(2, dummy, check_Node_CCM)
		#elif defined(_OMPSS)
		 #pragma oss task replicated(2, dummy, check_Node_CCM)
		#endif
		run_Collision_checking();
		}
		#pragma omp taskwait;
	}
	
	void Potential_obstacle_N57_subscription_callback(const std_msgs::msg::String::SharedPtr msg) {
	#ifdef CONSOLE_ENABLED
		std::cout << "ROS2: subscription callback " << msg->data << std::endl ;
	#endif
		int dummy;
		
		 #if defined(_OPENMP)
		 // #pragma omp taskgraph tdg_type(static) nowait
		#endif
		{
		#if defined(_OPENMP)
		 #pragma omp task replicated(2, dummy, check_Node_CCM)
		#elif defined(_OMPSS)
		 #pragma oss task replicated(2, dummy, check_Node_CCM)
		#endif
		run_Collision_checking();
		}
		#pragma omp taskwait;
	}
	
	void Potential_obstacle_N44_subscription_callback(const std_msgs::msg::String::SharedPtr msg) {
	#ifdef CONSOLE_ENABLED
		std::cout << "ROS2: subscription callback " << msg->data << std::endl ;
	#endif
		int dummy;
		
		 #if defined(_OPENMP)
		 // #pragma omp taskgraph tdg_type(static) nowait
		#endif
		{
		#if defined(_OPENMP)
		 #pragma omp task replicated(2, dummy, check_Node_CCM)
		#elif defined(_OMPSS)
		 #pragma oss task replicated(2, dummy, check_Node_CCM)
		#endif
		run_Collision_checking();
		}
		#pragma omp taskwait;
	}
	
	void Potential_obstacle_N43_subscription_callback(const std_msgs::msg::String::SharedPtr msg) {
	#ifdef CONSOLE_ENABLED
		std::cout << "ROS2: subscription callback " << msg->data << std::endl ;
	#endif
		int dummy;
		
		 #if defined(_OPENMP)
		 // #pragma omp taskgraph tdg_type(static) nowait
		#endif
		{
		#if defined(_OPENMP)
		 #pragma omp task replicated(2, dummy, check_Node_CCM)
		#elif defined(_OMPSS)
		 #pragma oss task replicated(2, dummy, check_Node_CCM)
		#endif
		run_Collision_checking();
		}
		#pragma omp taskwait;
	}
	
	void Potential_obstacle_N40_subscription_callback(const std_msgs::msg::String::SharedPtr msg) {
	#ifdef CONSOLE_ENABLED
		std::cout << "ROS2: subscription callback " << msg->data << std::endl ;
	#endif
		int dummy;
		
		 #if defined(_OPENMP)
		 // #pragma omp taskgraph tdg_type(static) nowait
		#endif
		{
		#if defined(_OPENMP)
		 #pragma omp task replicated(2, dummy, check_Node_CCM)
		#elif defined(_OMPSS)
		 #pragma oss task replicated(2, dummy, check_Node_CCM)
		#endif
		run_Collision_checking();
		}
		#pragma omp taskwait;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
};
