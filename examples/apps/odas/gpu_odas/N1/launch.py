from launch import LaunchDescription
from launch_ros.actions import Node

def generate_launch_description():
    return LaunchDescription([
		Node(
			package='amalthea_ros2_model',
			node_namespace='',
			node_executable='Node_CAMERA_Processing',
		),
		Node(
			package='amalthea_ros2_model',
			node_namespace='',
			node_executable='Node_CCM',
		),
		Node(
			package='amalthea_ros2_model',
			node_namespace='',
			node_executable='Node_LiDAR_Processing',
		),
		Node(
			package='amalthea_ros2_model',
			node_namespace='',
			node_executable='Node_Offloaded_CAMERA_Processing',
		),
		Node(
			package='amalthea_ros2_model',
			node_namespace='',
			node_executable='Node_RADAR_Processing',
		),
		Node(
			package='amalthea_ros2_model',
			node_namespace='',
			node_executable='Node_Sensor_Fusion',
		),
		Node(
			package='amalthea_ros2_model',
			node_namespace='',
			node_executable='Node_UKFs',
		),
	])
