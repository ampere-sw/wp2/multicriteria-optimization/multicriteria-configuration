#include "Node_CAMERA_Processing.hpp"
#include "Node_CCM.hpp"
#include "Node_LiDAR_Processing.hpp"
#include "Node_Offloaded_CAMERA_Processing.hpp"
#include "Node_RADAR_Processing.hpp"
#include "Node_Sensor_Fusion.hpp"
#include "Node_UKFs.hpp"
#include <extrae.h>
int main(int argc, char *argv[])
{
	std::vector<std::thread> threads;

	setvbuf(stdout, NULL, _IONBF, BUFSIZ);
	rclcpp::init(argc, argv);

	auto node1 = new Node_CAMERA_Processing();
	auto node2 = new Node_CCM();
	auto node3 = new Node_LiDAR_Processing();
	auto node4 = new Node_Offloaded_CAMERA_Processing();
	auto node5 = new Node_RADAR_Processing();
	auto node6 = new Node_Sensor_Fusion();
	auto node7 = new Node_UKFs();
	

	// auto message = node1->create_publisher<std_msgs::msg::String>("DUMMY", 10);
	auto message2 = std_msgs::msg::String();
	message2.data ="DUMMY";

	auto message = make_shared<std_msgs::msg::String>(message2);
	Extrae_init();
	int NUM_ITER = 10;
	for (int i = 0; i < NUM_ITER; i++){
		#pragma omp parallel
		#pragma omp single
		node1->timer_CAMERA_Processing_40_ms_callback();

		#pragma omp parallel
		#pragma omp single
		node2->Potential_obstacle_N00_subscription_callback(message);

		#pragma omp parallel
		#pragma omp single
		node3->timer_LiDAR_Processing_100_ms_callback();

		#pragma omp parallel
		#pragma omp single
		node4->CAMERA_frame_subscription_callback(message);

		#pragma omp parallel
		#pragma omp single
		node5->timer_RADAR_Processing_92_ms_callback();

		#pragma omp parallel
		#pragma omp single
		node6->RADAR_objects_in_VBF_coord_subscription_callback(message);

		#pragma omp parallel
		#pragma omp single
		node7->Tracked_object_label_N_subscription_callback(message);
	}
	
	rclcpp::shutdown();
	Extrae_fini();
	return 0;
}
