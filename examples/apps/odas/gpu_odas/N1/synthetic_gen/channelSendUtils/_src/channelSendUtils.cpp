// This code is auto-generated

#include "channelSendUtils.hpp"


void publish_to_RADAR_objects_in_VBF_coord(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher) {
	
	auto message = std_msgs::msg::String();
	message.data ="RADAR_objects_in_VBF_coord";
	//message.data ="A";
	publisher->publish(message);
	std::cout << "ROS2: Publishing message " << message.data << std::endl;
}

void publish_to_CAMERA_frame(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher) {
	
	auto message = std_msgs::msg::String();
	message.data ="CAMERA_frame";
	//message.data ="A";
	publisher->publish(message);
	std::cout << "ROS2: Publishing message " << message.data << std::endl;
}

void publish_to_LiDAR_objects_in_VBF_coord(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher) {
	
	auto message = std_msgs::msg::String();
	message.data ="LiDAR_objects_in_VBF_coord";
	//message.data ="A";
	publisher->publish(message);
	std::cout << "ROS2: Publishing message " << message.data << std::endl;
}

void publish_to_Bounding_box_in_VBF_coord(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher) {
	
	auto message = std_msgs::msg::String();
	message.data ="Bounding_box_in_VBF_coord";
	//message.data ="A";
	publisher->publish(message);
	std::cout << "ROS2: Publishing message " << message.data << std::endl;
}

void publish_to_Tracked_object_label_N(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher) {
	
	auto message = std_msgs::msg::String();
	message.data ="Tracked_object_label_N";
	//message.data ="AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	publisher->publish(message);
	std::cout << "ROS2: Publishing message " << message.data << std::endl;
}

void publish_to_Potential_obstacle_N00(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher) {
	
	auto message = std_msgs::msg::String();
	message.data ="Potential_obstacle_N00";
	//message.data ="A";
	publisher->publish(message);
	std::cout << "ROS2: Publishing message " << message.data << std::endl;
}

void publish_to_Predicted_track_00(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher) {
	
	auto message = std_msgs::msg::String();
	message.data ="Predicted_track_00";
	//message.data ="AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
	publisher->publish(message);
	std::cout << "ROS2: Publishing message " << message.data << std::endl;
}

void publish_to_Potential_obstacle_N01(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher) {
	
	auto message = std_msgs::msg::String();
	message.data ="Potential_obstacle_N01";
	//message.data ="A";
	publisher->publish(message);
	std::cout << "ROS2: Publishing message " << message.data << std::endl;
}

void publish_to_Predicted_track_01(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher) {
	
	auto message = std_msgs::msg::String();
	message.data ="Predicted_track_01";
	//message.data ="A";
	publisher->publish(message);
	std::cout << "ROS2: Publishing message " << message.data << std::endl;
}

void publish_to_Potential_obstacle_N02(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher) {
	
	auto message = std_msgs::msg::String();
	message.data ="Potential_obstacle_N02";
	//message.data ="A";
	publisher->publish(message);
	std::cout << "ROS2: Publishing message " << message.data << std::endl;
}

void publish_to_Predicted_track_02(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher) {
	
	auto message = std_msgs::msg::String();
	message.data ="Predicted_track_02";
	//message.data ="A";
	publisher->publish(message);
	std::cout << "ROS2: Publishing message " << message.data << std::endl;
}

void publish_to_Potential_obstacle_N03(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher) {
	
	auto message = std_msgs::msg::String();
	message.data ="Potential_obstacle_N03";
	//message.data ="A";
	publisher->publish(message);
	std::cout << "ROS2: Publishing message " << message.data << std::endl;
}

void publish_to_Predicted_track_03(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher) {
	
	auto message = std_msgs::msg::String();
	message.data ="Predicted_track_03";
	//message.data ="A";
	publisher->publish(message);
	std::cout << "ROS2: Publishing message " << message.data << std::endl;
}

void publish_to_Potential_obstacle_N04(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher) {
	
	auto message = std_msgs::msg::String();
	message.data ="Potential_obstacle_N04";
	//message.data ="A";
	publisher->publish(message);
	std::cout << "ROS2: Publishing message " << message.data << std::endl;
}

void publish_to_Predicted_track_04(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher) {
	
	auto message = std_msgs::msg::String();
	message.data ="Predicted_track_04";
	//message.data ="A";
	publisher->publish(message);
	std::cout << "ROS2: Publishing message " << message.data << std::endl;
}

void publish_to_Potential_obstacle_N05(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher) {
	
	auto message = std_msgs::msg::String();
	message.data ="Potential_obstacle_N05";
	//message.data ="A";
	publisher->publish(message);
	std::cout << "ROS2: Publishing message " << message.data << std::endl;
}

void publish_to_Predicted_track_05(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher) {
	
	auto message = std_msgs::msg::String();
	message.data ="Predicted_track_05";
	//message.data ="A";
	publisher->publish(message);
	std::cout << "ROS2: Publishing message " << message.data << std::endl;
}

void publish_to_Potential_obstacle_N06(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher) {
	
	auto message = std_msgs::msg::String();
	message.data ="Potential_obstacle_N06";
	//message.data ="A";
	publisher->publish(message);
	std::cout << "ROS2: Publishing message " << message.data << std::endl;
}

void publish_to_Predicted_track_06(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher) {
	
	auto message = std_msgs::msg::String();
	message.data ="Predicted_track_06";
	//message.data ="A";
	publisher->publish(message);
	std::cout << "ROS2: Publishing message " << message.data << std::endl;
}

void publish_to_Potential_obstacle_N07(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher) {
	
	auto message = std_msgs::msg::String();
	message.data ="Potential_obstacle_N07";
	//message.data ="A";
	publisher->publish(message);
	std::cout << "ROS2: Publishing message " << message.data << std::endl;
}

void publish_to_Predicted_track_07(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher) {
	
	auto message = std_msgs::msg::String();
	message.data ="Predicted_track_07";
	//message.data ="A";
	publisher->publish(message);
	std::cout << "ROS2: Publishing message " << message.data << std::endl;
}

void publish_to_Potential_obstacle_N08(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher) {
	
	auto message = std_msgs::msg::String();
	message.data ="Potential_obstacle_N08";
	//message.data ="A";
	publisher->publish(message);
	std::cout << "ROS2: Publishing message " << message.data << std::endl;
}

void publish_to_Predicted_track_08(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher) {
	
	auto message = std_msgs::msg::String();
	message.data ="Predicted_track_08";
	//message.data ="A";
	publisher->publish(message);
	std::cout << "ROS2: Publishing message " << message.data << std::endl;
}

void publish_to_Potential_obstacle_N09(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher) {
	
	auto message = std_msgs::msg::String();
	message.data ="Potential_obstacle_N09";
	//message.data ="A";
	publisher->publish(message);
	std::cout << "ROS2: Publishing message " << message.data << std::endl;
}

void publish_to_Predicted_track_09(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher) {
	
	auto message = std_msgs::msg::String();
	message.data ="Predicted_track_09";
	//message.data ="A";
	publisher->publish(message);
	std::cout << "ROS2: Publishing message " << message.data << std::endl;
}

void publish_to_Potential_obstacle_N10(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher) {
	
	auto message = std_msgs::msg::String();
	message.data ="Potential_obstacle_N10";
	//message.data ="A";
	publisher->publish(message);
	std::cout << "ROS2: Publishing message " << message.data << std::endl;
}

void publish_to_Predicted_track_10(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher) {
	
	auto message = std_msgs::msg::String();
	message.data ="Predicted_track_10";
	//message.data ="A";
	publisher->publish(message);
	std::cout << "ROS2: Publishing message " << message.data << std::endl;
}

void publish_to_Potential_obstacle_N11(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher) {
	
	auto message = std_msgs::msg::String();
	message.data ="Potential_obstacle_N11";
	//message.data ="A";
	publisher->publish(message);
	std::cout << "ROS2: Publishing message " << message.data << std::endl;
}

void publish_to_Predicted_track_11(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher) {
	
	auto message = std_msgs::msg::String();
	message.data ="Predicted_track_11";
	//message.data ="A";
	publisher->publish(message);
	std::cout << "ROS2: Publishing message " << message.data << std::endl;
}

void publish_to_Potential_obstacle_N12(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher) {
	
	auto message = std_msgs::msg::String();
	message.data ="Potential_obstacle_N12";
	//message.data ="A";
	publisher->publish(message);
	std::cout << "ROS2: Publishing message " << message.data << std::endl;
}

void publish_to_Predicted_track_12(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher) {
	
	auto message = std_msgs::msg::String();
	message.data ="Predicted_track_12";
	//message.data ="A";
	publisher->publish(message);
	std::cout << "ROS2: Publishing message " << message.data << std::endl;
}

void publish_to_Potential_obstacle_N13(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher) {
	
	auto message = std_msgs::msg::String();
	message.data ="Potential_obstacle_N13";
	//message.data ="A";
	publisher->publish(message);
	std::cout << "ROS2: Publishing message " << message.data << std::endl;
}

void publish_to_Predicted_track_13(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher) {
	
	auto message = std_msgs::msg::String();
	message.data ="Predicted_track_13";
	//message.data ="A";
	publisher->publish(message);
	std::cout << "ROS2: Publishing message " << message.data << std::endl;
}

void publish_to_Potential_obstacle_N14(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher) {
	
	auto message = std_msgs::msg::String();
	message.data ="Potential_obstacle_N14";
	//message.data ="A";
	publisher->publish(message);
	std::cout << "ROS2: Publishing message " << message.data << std::endl;
}

void publish_to_Predicted_track_14(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher) {
	
	auto message = std_msgs::msg::String();
	message.data ="Predicted_track_14";
	//message.data ="A";
	publisher->publish(message);
	std::cout << "ROS2: Publishing message " << message.data << std::endl;
}

void publish_to_Potential_obstacle_N15(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher) {
	
	auto message = std_msgs::msg::String();
	message.data ="Potential_obstacle_N15";
	//message.data ="A";
	publisher->publish(message);
	std::cout << "ROS2: Publishing message " << message.data << std::endl;
}

void publish_to_Predicted_track_15(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher) {
	
	auto message = std_msgs::msg::String();
	message.data ="Predicted_track_15";
	//message.data ="A";
	publisher->publish(message);
	std::cout << "ROS2: Publishing message " << message.data << std::endl;
}

void publish_to_Potential_obstacle_N16(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher) {
	
	auto message = std_msgs::msg::String();
	message.data ="Potential_obstacle_N16";
	//message.data ="A";
	publisher->publish(message);
	std::cout << "ROS2: Publishing message " << message.data << std::endl;
}

void publish_to_Predicted_track_16(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher) {
	
	auto message = std_msgs::msg::String();
	message.data ="Predicted_track_16";
	//message.data ="A";
	publisher->publish(message);
	std::cout << "ROS2: Publishing message " << message.data << std::endl;
}

void publish_to_Potential_obstacle_N17(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher) {
	
	auto message = std_msgs::msg::String();
	message.data ="Potential_obstacle_N17";
	//message.data ="A";
	publisher->publish(message);
	std::cout << "ROS2: Publishing message " << message.data << std::endl;
}

void publish_to_Predicted_track_17(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher) {
	
	auto message = std_msgs::msg::String();
	message.data ="Predicted_track_17";
	//message.data ="A";
	publisher->publish(message);
	std::cout << "ROS2: Publishing message " << message.data << std::endl;
}

void publish_to_Potential_obstacle_N18(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher) {
	
	auto message = std_msgs::msg::String();
	message.data ="Potential_obstacle_N18";
	//message.data ="A";
	publisher->publish(message);
	std::cout << "ROS2: Publishing message " << message.data << std::endl;
}

void publish_to_Predicted_track_18(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher) {
	
	auto message = std_msgs::msg::String();
	message.data ="Predicted_track_18";
	//message.data ="A";
	publisher->publish(message);
	std::cout << "ROS2: Publishing message " << message.data << std::endl;
}

void publish_to_Potential_obstacle_N19(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher) {
	
	auto message = std_msgs::msg::String();
	message.data ="Potential_obstacle_N19";
	//message.data ="A";
	publisher->publish(message);
	std::cout << "ROS2: Publishing message " << message.data << std::endl;
}

void publish_to_Predicted_track_19(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher) {
	
	auto message = std_msgs::msg::String();
	message.data ="Predicted_track_19";
	//message.data ="A";
	publisher->publish(message);
	std::cout << "ROS2: Publishing message " << message.data << std::endl;
}

void publish_to_Potential_obstacle_N20(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher) {
	
	auto message = std_msgs::msg::String();
	message.data ="Potential_obstacle_N20";
	//message.data ="A";
	publisher->publish(message);
	std::cout << "ROS2: Publishing message " << message.data << std::endl;
}

void publish_to_Predicted_track_20(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher) {
	
	auto message = std_msgs::msg::String();
	message.data ="Predicted_track_20";
	//message.data ="A";
	publisher->publish(message);
	std::cout << "ROS2: Publishing message " << message.data << std::endl;
}

void publish_to_Potential_obstacle_N21(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher) {
	
	auto message = std_msgs::msg::String();
	message.data ="Potential_obstacle_N21";
	//message.data ="A";
	publisher->publish(message);
	std::cout << "ROS2: Publishing message " << message.data << std::endl;
}

void publish_to_Predicted_track_21(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher) {
	
	auto message = std_msgs::msg::String();
	message.data ="Predicted_track_21";
	//message.data ="A";
	publisher->publish(message);
	std::cout << "ROS2: Publishing message " << message.data << std::endl;
}

void publish_to_Potential_obstacle_N22(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher) {
	
	auto message = std_msgs::msg::String();
	message.data ="Potential_obstacle_N22";
	//message.data ="A";
	publisher->publish(message);
	std::cout << "ROS2: Publishing message " << message.data << std::endl;
}

void publish_to_Predicted_track_22(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher) {
	
	auto message = std_msgs::msg::String();
	message.data ="Predicted_track_22";
	//message.data ="A";
	publisher->publish(message);
	std::cout << "ROS2: Publishing message " << message.data << std::endl;
}

void publish_to_Potential_obstacle_N23(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher) {
	
	auto message = std_msgs::msg::String();
	message.data ="Potential_obstacle_N23";
	//message.data ="A";
	publisher->publish(message);
	std::cout << "ROS2: Publishing message " << message.data << std::endl;
}

void publish_to_Predicted_track_23(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher) {
	
	auto message = std_msgs::msg::String();
	message.data ="Predicted_track_23";
	//message.data ="A";
	publisher->publish(message);
	std::cout << "ROS2: Publishing message " << message.data << std::endl;
}

void publish_to_Potential_obstacle_N24(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher) {
	
	auto message = std_msgs::msg::String();
	message.data ="Potential_obstacle_N24";
	//message.data ="A";
	publisher->publish(message);
	std::cout << "ROS2: Publishing message " << message.data << std::endl;
}

void publish_to_Predicted_track_24(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher) {
	
	auto message = std_msgs::msg::String();
	message.data ="Predicted_track_24";
	//message.data ="A";
	publisher->publish(message);
	std::cout << "ROS2: Publishing message " << message.data << std::endl;
}

void publish_to_Potential_obstacle_N25(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher) {
	
	auto message = std_msgs::msg::String();
	message.data ="Potential_obstacle_N25";
	//message.data ="A";
	publisher->publish(message);
	std::cout << "ROS2: Publishing message " << message.data << std::endl;
}

void publish_to_Predicted_track_25(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher) {
	
	auto message = std_msgs::msg::String();
	message.data ="Predicted_track_25";
	//message.data ="A";
	publisher->publish(message);
	std::cout << "ROS2: Publishing message " << message.data << std::endl;
}

void publish_to_Potential_obstacle_N26(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher) {
	
	auto message = std_msgs::msg::String();
	message.data ="Potential_obstacle_N26";
	//message.data ="A";
	publisher->publish(message);
	std::cout << "ROS2: Publishing message " << message.data << std::endl;
}

void publish_to_Predicted_track_26(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher) {
	
	auto message = std_msgs::msg::String();
	message.data ="Predicted_track_26";
	//message.data ="A";
	publisher->publish(message);
	std::cout << "ROS2: Publishing message " << message.data << std::endl;
}

void publish_to_Potential_obstacle_N27(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher) {
	
	auto message = std_msgs::msg::String();
	message.data ="Potential_obstacle_N27";
	//message.data ="A";
	publisher->publish(message);
	std::cout << "ROS2: Publishing message " << message.data << std::endl;
}

void publish_to_Predicted_track_27(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher) {
	
	auto message = std_msgs::msg::String();
	message.data ="Predicted_track_27";
	//message.data ="A";
	publisher->publish(message);
	std::cout << "ROS2: Publishing message " << message.data << std::endl;
}

void publish_to_Potential_obstacle_N28(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher) {
	
	auto message = std_msgs::msg::String();
	message.data ="Potential_obstacle_N28";
	//message.data ="A";
	publisher->publish(message);
	std::cout << "ROS2: Publishing message " << message.data << std::endl;
}

void publish_to_Predicted_track_28(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher) {
	
	auto message = std_msgs::msg::String();
	message.data ="Predicted_track_28";
	//message.data ="A";
	publisher->publish(message);
	std::cout << "ROS2: Publishing message " << message.data << std::endl;
}

void publish_to_Potential_obstacle_N29(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher) {
	
	auto message = std_msgs::msg::String();
	message.data ="Potential_obstacle_N29";
	//message.data ="A";
	publisher->publish(message);
	std::cout << "ROS2: Publishing message " << message.data << std::endl;
}

void publish_to_Predicted_track_29(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher) {
	
	auto message = std_msgs::msg::String();
	message.data ="Predicted_track_29";
	//message.data ="A";
	publisher->publish(message);
	std::cout << "ROS2: Publishing message " << message.data << std::endl;
}

void publish_to_Potential_obstacle_N30(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher) {
	
	auto message = std_msgs::msg::String();
	message.data ="Potential_obstacle_N30";
	//message.data ="A";
	publisher->publish(message);
	std::cout << "ROS2: Publishing message " << message.data << std::endl;
}

void publish_to_Predicted_track_30(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher) {
	
	auto message = std_msgs::msg::String();
	message.data ="Predicted_track_30";
	//message.data ="A";
	publisher->publish(message);
	std::cout << "ROS2: Publishing message " << message.data << std::endl;
}

void publish_to_Potential_obstacle_N31(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher) {
	
	auto message = std_msgs::msg::String();
	message.data ="Potential_obstacle_N31";
	//message.data ="A";
	publisher->publish(message);
	std::cout << "ROS2: Publishing message " << message.data << std::endl;
}

void publish_to_Predicted_track_31(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher) {
	
	auto message = std_msgs::msg::String();
	message.data ="Predicted_track_31";
	//message.data ="A";
	publisher->publish(message);
	std::cout << "ROS2: Publishing message " << message.data << std::endl;
}

void publish_to_Potential_obstacle_N32(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher) {
	
	auto message = std_msgs::msg::String();
	message.data ="Potential_obstacle_N32";
	//message.data ="A";
	publisher->publish(message);
	std::cout << "ROS2: Publishing message " << message.data << std::endl;
}

void publish_to_Predicted_track_32(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher) {
	
	auto message = std_msgs::msg::String();
	message.data ="Predicted_track_32";
	//message.data ="A";
	publisher->publish(message);
	std::cout << "ROS2: Publishing message " << message.data << std::endl;
}

void publish_to_Potential_obstacle_N33(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher) {
	
	auto message = std_msgs::msg::String();
	message.data ="Potential_obstacle_N33";
	//message.data ="A";
	publisher->publish(message);
	std::cout << "ROS2: Publishing message " << message.data << std::endl;
}

void publish_to_Predicted_track_33(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher) {
	
	auto message = std_msgs::msg::String();
	message.data ="Predicted_track_33";
	//message.data ="A";
	publisher->publish(message);
	std::cout << "ROS2: Publishing message " << message.data << std::endl;
}

void publish_to_Potential_obstacle_N34(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher) {
	
	auto message = std_msgs::msg::String();
	message.data ="Potential_obstacle_N34";
	//message.data ="A";
	publisher->publish(message);
	std::cout << "ROS2: Publishing message " << message.data << std::endl;
}

void publish_to_Predicted_track_34(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher) {
	
	auto message = std_msgs::msg::String();
	message.data ="Predicted_track_34";
	//message.data ="A";
	publisher->publish(message);
	std::cout << "ROS2: Publishing message " << message.data << std::endl;
}

void publish_to_Potential_obstacle_N35(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher) {
	
	auto message = std_msgs::msg::String();
	message.data ="Potential_obstacle_N35";
	//message.data ="A";
	publisher->publish(message);
	std::cout << "ROS2: Publishing message " << message.data << std::endl;
}

void publish_to_Predicted_track_35(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher) {
	
	auto message = std_msgs::msg::String();
	message.data ="Predicted_track_35";
	//message.data ="A";
	publisher->publish(message);
	std::cout << "ROS2: Publishing message " << message.data << std::endl;
}

void publish_to_Potential_obstacle_N36(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher) {
	
	auto message = std_msgs::msg::String();
	message.data ="Potential_obstacle_N36";
	//message.data ="A";
	publisher->publish(message);
	std::cout << "ROS2: Publishing message " << message.data << std::endl;
}

void publish_to_Predicted_track_36(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher) {
	
	auto message = std_msgs::msg::String();
	message.data ="Predicted_track_36";
	//message.data ="A";
	publisher->publish(message);
	std::cout << "ROS2: Publishing message " << message.data << std::endl;
}

void publish_to_Potential_obstacle_N37(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher) {
	
	auto message = std_msgs::msg::String();
	message.data ="Potential_obstacle_N37";
	//message.data ="A";
	publisher->publish(message);
	std::cout << "ROS2: Publishing message " << message.data << std::endl;
}

void publish_to_Predicted_track_37(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher) {
	
	auto message = std_msgs::msg::String();
	message.data ="Predicted_track_37";
	//message.data ="A";
	publisher->publish(message);
	std::cout << "ROS2: Publishing message " << message.data << std::endl;
}

void publish_to_Potential_obstacle_N38(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher) {
	
	auto message = std_msgs::msg::String();
	message.data ="Potential_obstacle_N38";
	//message.data ="A";
	publisher->publish(message);
	std::cout << "ROS2: Publishing message " << message.data << std::endl;
}

void publish_to_Predicted_track_38(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher) {
	
	auto message = std_msgs::msg::String();
	message.data ="Predicted_track_38";
	//message.data ="A";
	publisher->publish(message);
	std::cout << "ROS2: Publishing message " << message.data << std::endl;
}

void publish_to_Potential_obstacle_N39(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher) {
	
	auto message = std_msgs::msg::String();
	message.data ="Potential_obstacle_N39";
	//message.data ="A";
	publisher->publish(message);
	std::cout << "ROS2: Publishing message " << message.data << std::endl;
}

void publish_to_Predicted_track_39(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher) {
	
	auto message = std_msgs::msg::String();
	message.data ="Predicted_track_39";
	//message.data ="A";
	publisher->publish(message);
	std::cout << "ROS2: Publishing message " << message.data << std::endl;
}

void publish_to_Potential_obstacle_N40(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher) {
	
	auto message = std_msgs::msg::String();
	message.data ="Potential_obstacle_N40";
	//message.data ="A";
	publisher->publish(message);
	std::cout << "ROS2: Publishing message " << message.data << std::endl;
}

void publish_to_Predicted_track_40(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher) {
	
	auto message = std_msgs::msg::String();
	message.data ="Predicted_track_40";
	//message.data ="A";
	publisher->publish(message);
	std::cout << "ROS2: Publishing message " << message.data << std::endl;
}

void publish_to_Potential_obstacle_N41(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher) {
	
	auto message = std_msgs::msg::String();
	message.data ="Potential_obstacle_N41";
	//message.data ="A";
	publisher->publish(message);
	std::cout << "ROS2: Publishing message " << message.data << std::endl;
}

void publish_to_Predicted_track_41(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher) {
	
	auto message = std_msgs::msg::String();
	message.data ="Predicted_track_41";
	//message.data ="A";
	publisher->publish(message);
	std::cout << "ROS2: Publishing message " << message.data << std::endl;
}

void publish_to_Potential_obstacle_N42(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher) {
	
	auto message = std_msgs::msg::String();
	message.data ="Potential_obstacle_N42";
	//message.data ="A";
	publisher->publish(message);
	std::cout << "ROS2: Publishing message " << message.data << std::endl;
}

void publish_to_Predicted_track_42(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher) {
	
	auto message = std_msgs::msg::String();
	message.data ="Predicted_track_42";
	//message.data ="A";
	publisher->publish(message);
	std::cout << "ROS2: Publishing message " << message.data << std::endl;
}

void publish_to_Potential_obstacle_N43(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher) {
	
	auto message = std_msgs::msg::String();
	message.data ="Potential_obstacle_N43";
	//message.data ="A";
	publisher->publish(message);
	std::cout << "ROS2: Publishing message " << message.data << std::endl;
}

void publish_to_Predicted_track_43(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher) {
	
	auto message = std_msgs::msg::String();
	message.data ="Predicted_track_43";
	//message.data ="A";
	publisher->publish(message);
	std::cout << "ROS2: Publishing message " << message.data << std::endl;
}

void publish_to_Potential_obstacle_N44(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher) {
	
	auto message = std_msgs::msg::String();
	message.data ="Potential_obstacle_N44";
	//message.data ="A";
	publisher->publish(message);
	std::cout << "ROS2: Publishing message " << message.data << std::endl;
}

void publish_to_Predicted_track_44(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher) {
	
	auto message = std_msgs::msg::String();
	message.data ="Predicted_track_44";
	//message.data ="A";
	publisher->publish(message);
	std::cout << "ROS2: Publishing message " << message.data << std::endl;
}

void publish_to_Potential_obstacle_N45(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher) {
	
	auto message = std_msgs::msg::String();
	message.data ="Potential_obstacle_N45";
	//message.data ="A";
	publisher->publish(message);
	std::cout << "ROS2: Publishing message " << message.data << std::endl;
}

void publish_to_Predicted_track_45(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher) {
	
	auto message = std_msgs::msg::String();
	message.data ="Predicted_track_45";
	//message.data ="A";
	publisher->publish(message);
	std::cout << "ROS2: Publishing message " << message.data << std::endl;
}

void publish_to_Potential_obstacle_N46(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher) {
	
	auto message = std_msgs::msg::String();
	message.data ="Potential_obstacle_N46";
	//message.data ="A";
	publisher->publish(message);
	std::cout << "ROS2: Publishing message " << message.data << std::endl;
}

void publish_to_Predicted_track_46(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher) {
	
	auto message = std_msgs::msg::String();
	message.data ="Predicted_track_46";
	//message.data ="A";
	publisher->publish(message);
	std::cout << "ROS2: Publishing message " << message.data << std::endl;
}

void publish_to_Potential_obstacle_N47(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher) {
	
	auto message = std_msgs::msg::String();
	message.data ="Potential_obstacle_N47";
	//message.data ="A";
	publisher->publish(message);
	std::cout << "ROS2: Publishing message " << message.data << std::endl;
}

void publish_to_Predicted_track_47(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher) {
	
	auto message = std_msgs::msg::String();
	message.data ="Predicted_track_47";
	//message.data ="A";
	publisher->publish(message);
	std::cout << "ROS2: Publishing message " << message.data << std::endl;
}

void publish_to_Potential_obstacle_N48(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher) {
	
	auto message = std_msgs::msg::String();
	message.data ="Potential_obstacle_N48";
	//message.data ="A";
	publisher->publish(message);
	std::cout << "ROS2: Publishing message " << message.data << std::endl;
}

void publish_to_Predicted_track_48(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher) {
	
	auto message = std_msgs::msg::String();
	message.data ="Predicted_track_48";
	//message.data ="A";
	publisher->publish(message);
	std::cout << "ROS2: Publishing message " << message.data << std::endl;
}

void publish_to_Potential_obstacle_N49(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher) {
	
	auto message = std_msgs::msg::String();
	message.data ="Potential_obstacle_N49";
	//message.data ="A";
	publisher->publish(message);
	std::cout << "ROS2: Publishing message " << message.data << std::endl;
}

void publish_to_Predicted_track_49(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher) {
	
	auto message = std_msgs::msg::String();
	message.data ="Predicted_track_49";
	//message.data ="A";
	publisher->publish(message);
	std::cout << "ROS2: Publishing message " << message.data << std::endl;
}

void publish_to_Potential_obstacle_N50(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher) {
	
	auto message = std_msgs::msg::String();
	message.data ="Potential_obstacle_N50";
	//message.data ="A";
	publisher->publish(message);
	std::cout << "ROS2: Publishing message " << message.data << std::endl;
}

void publish_to_Predicted_track_50(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher) {
	
	auto message = std_msgs::msg::String();
	message.data ="Predicted_track_50";
	//message.data ="A";
	publisher->publish(message);
	std::cout << "ROS2: Publishing message " << message.data << std::endl;
}

void publish_to_Potential_obstacle_N51(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher) {
	
	auto message = std_msgs::msg::String();
	message.data ="Potential_obstacle_N51";
	//message.data ="A";
	publisher->publish(message);
	std::cout << "ROS2: Publishing message " << message.data << std::endl;
}

void publish_to_Predicted_track_51(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher) {
	
	auto message = std_msgs::msg::String();
	message.data ="Predicted_track_51";
	//message.data ="A";
	publisher->publish(message);
	std::cout << "ROS2: Publishing message " << message.data << std::endl;
}

void publish_to_Potential_obstacle_N52(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher) {
	
	auto message = std_msgs::msg::String();
	message.data ="Potential_obstacle_N52";
	//message.data ="A";
	publisher->publish(message);
	std::cout << "ROS2: Publishing message " << message.data << std::endl;
}

void publish_to_Predicted_track_52(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher) {
	
	auto message = std_msgs::msg::String();
	message.data ="Predicted_track_52";
	//message.data ="A";
	publisher->publish(message);
	std::cout << "ROS2: Publishing message " << message.data << std::endl;
}

void publish_to_Potential_obstacle_N53(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher) {
	
	auto message = std_msgs::msg::String();
	message.data ="Potential_obstacle_N53";
	//message.data ="A";
	publisher->publish(message);
	std::cout << "ROS2: Publishing message " << message.data << std::endl;
}

void publish_to_Predicted_track_53(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher) {
	
	auto message = std_msgs::msg::String();
	message.data ="Predicted_track_53";
	//message.data ="A";
	publisher->publish(message);
	std::cout << "ROS2: Publishing message " << message.data << std::endl;
}

void publish_to_Potential_obstacle_N54(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher) {
	
	auto message = std_msgs::msg::String();
	message.data ="Potential_obstacle_N54";
	//message.data ="A";
	publisher->publish(message);
	std::cout << "ROS2: Publishing message " << message.data << std::endl;
}

void publish_to_Predicted_track_54(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher) {
	
	auto message = std_msgs::msg::String();
	message.data ="Predicted_track_54";
	//message.data ="A";
	publisher->publish(message);
	std::cout << "ROS2: Publishing message " << message.data << std::endl;
}

void publish_to_Potential_obstacle_N55(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher) {
	
	auto message = std_msgs::msg::String();
	message.data ="Potential_obstacle_N55";
	//message.data ="A";
	publisher->publish(message);
	std::cout << "ROS2: Publishing message " << message.data << std::endl;
}

void publish_to_Predicted_track_55(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher) {
	
	auto message = std_msgs::msg::String();
	message.data ="Predicted_track_55";
	//message.data ="A";
	publisher->publish(message);
	std::cout << "ROS2: Publishing message " << message.data << std::endl;
}

void publish_to_Potential_obstacle_N56(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher) {
	
	auto message = std_msgs::msg::String();
	message.data ="Potential_obstacle_N56";
	//message.data ="A";
	publisher->publish(message);
	std::cout << "ROS2: Publishing message " << message.data << std::endl;
}

void publish_to_Predicted_track_56(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher) {
	
	auto message = std_msgs::msg::String();
	message.data ="Predicted_track_56";
	//message.data ="A";
	publisher->publish(message);
	std::cout << "ROS2: Publishing message " << message.data << std::endl;
}

void publish_to_Potential_obstacle_N57(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher) {
	
	auto message = std_msgs::msg::String();
	message.data ="Potential_obstacle_N57";
	//message.data ="A";
	publisher->publish(message);
	std::cout << "ROS2: Publishing message " << message.data << std::endl;
}

void publish_to_Predicted_track_57(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher) {
	
	auto message = std_msgs::msg::String();
	message.data ="Predicted_track_57";
	//message.data ="A";
	publisher->publish(message);
	std::cout << "ROS2: Publishing message " << message.data << std::endl;
}

void publish_to_Potential_obstacle_N58(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher) {
	
	auto message = std_msgs::msg::String();
	message.data ="Potential_obstacle_N58";
	//message.data ="A";
	publisher->publish(message);
	std::cout << "ROS2: Publishing message " << message.data << std::endl;
}

void publish_to_Predicted_track_58(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher) {
	
	auto message = std_msgs::msg::String();
	message.data ="Predicted_track_58";
	//message.data ="A";
	publisher->publish(message);
	std::cout << "ROS2: Publishing message " << message.data << std::endl;
}

void publish_to_Potential_obstacle_N59(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher) {
	
	auto message = std_msgs::msg::String();
	message.data ="Potential_obstacle_N59";
	//message.data ="A";
	publisher->publish(message);
	std::cout << "ROS2: Publishing message " << message.data << std::endl;
}

void publish_to_Predicted_track_59(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher) {
	
	auto message = std_msgs::msg::String();
	message.data ="Predicted_track_59";
	//message.data ="A";
	publisher->publish(message);
	std::cout << "ROS2: Publishing message " << message.data << std::endl;
}
