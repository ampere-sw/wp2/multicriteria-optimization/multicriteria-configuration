// This code is auto-generated

#include "runnables.hpp"

// Runnable CAN_msgs_management ----
void run_CAN_msgs_management(){
	executeTicks_DiscreteValueConstant(9040000);
	write_RADAR_points(1);
}


// Runnable RADAR_spatial_synchronization ----
void run_RADAR_spatial_synchronization(){
	read_RADAR_points(1);
	executeTicks_DiscreteValueConstant(18080000);
	write_RADAR_points_in_VBF_coord(1);
}


// Runnable RADAR_object_detection ----
void run_RADAR_object_detection(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& RADAR_objects_in_VBF_coord_publisher){
	read_RADAR_points_in_VBF_coord(1);
	executeTicks_DiscreteValueConstant(47460000);
	// publish_to_RADAR_objects_in_VBF_coord(RADAR_objects_in_VBF_coord_publisher);
}


// Runnable ETH_CAMERA_raw_data_management ----
void run_ETH_CAMERA_raw_data_management(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& CAMERA_frame_publisher){
	executeTicks_DiscreteValueConstant(11300000);
	// publish_to_CAMERA_frame(CAMERA_frame_publisher);
}


// Runnable ETH_LiDAR_msgs_management ----
void run_ETH_LiDAR_msgs_management(){
	executeTicks_DiscreteValueConstant(20340000);
	write_ThreeD_point_cloud(1);
}


// Runnable LiDAR_spatial_synchronization ----
void run_LiDAR_spatial_synchronization(){
	read_ThreeD_point_cloud(1);
	executeTicks_DiscreteValueConstant(20340000);
	write_LiDAR_point_cloud_in_VBF_coord(1);
}


// Runnable LiDAR_object_detection ----
void run_LiDAR_object_detection(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& LiDAR_objects_in_VBF_coord_publisher){
	read_LiDAR_point_cloud_in_VBF_coord(1);
	executeTicks_DiscreteValueConstant(65540000);
	// publish_to_LiDAR_objects_in_VBF_coord(LiDAR_objects_in_VBF_coord_publisher);
}


// Runnable CAMERA_object_detection ----
void run_CAMERA_object_detection(){
}

void run_CAMERA_object_detection_GPU(){
		//ChannelReceiveTry;
		read_CAMERA_frame_sub_label(1);
		executeGPUTicksConstant(40680000);
		write_CAMERA_bounding_box_lists(1);
}


// Runnable Homography_transformation ----
void run_Homography_transformation(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Bounding_box_in_VBF_coord_publisher){
}

void run_Homography_transformation_GPU(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Bounding_box_in_VBF_coord_publisher){
		read_CAMERA_bounding_box_lists(1);
		executeGPUTicksConstant(24860000);
		// publish_to_Bounding_box_in_VBF_coord(Bounding_box_in_VBF_coord_publisher);
}


// Runnable Time_synchronization ----
void run_Time_synchronization(){
	//ChannelReceiveTry;
	read_Bounding_box_in_VBF_coord_sub_label(1);
	//ChannelReceiveTry;
	read_RADAR_objects_in_VBF_coord_sub_label(1);
	//ChannelReceiveTry;
	read_LiDAR_objects_in_VBF_coord_sub_label(1);
	executeTicks_DiscreteValueConstant(2260000);
	write_CAMERA_post_time_synch(1);
	write_RADAR_post_time_synch(1);
	write_LiDAR_post_time_synch(1);
}


// Runnable Data_association ----
void run_Data_association(){
	//ChannelReceiveTry;
	read_Predicted_track_48_sub_label(1);
	//ChannelReceiveTry;
	read_Predicted_track_59_sub_label(1);
	//ChannelReceiveTry;
	read_Predicted_track_58_sub_label(1);
	//ChannelReceiveTry;
	read_Predicted_track_57_sub_label(1);
	//ChannelReceiveTry;
	read_Predicted_track_56_sub_label(1);
	//ChannelReceiveTry;
	read_Predicted_track_55_sub_label(1);
	//ChannelReceiveTry;
	read_Predicted_track_54_sub_label(1);
	//ChannelReceiveTry;
	read_Predicted_track_53_sub_label(1);
	//ChannelReceiveTry;
	read_Predicted_track_50_sub_label(1);
	//ChannelReceiveTry;
	read_Predicted_track_52_sub_label(1);
	//ChannelReceiveTry;
	read_Predicted_track_49_sub_label(1);
	//ChannelReceiveTry;
	read_Predicted_track_51_sub_label(1);
	//ChannelReceiveTry;
	read_Predicted_track_33_sub_label(1);
	//ChannelReceiveTry;
	read_Predicted_track_47_sub_label(1);
	//ChannelReceiveTry;
	read_Predicted_track_46_sub_label(1);
	//ChannelReceiveTry;
	read_Predicted_track_45_sub_label(1);
	//ChannelReceiveTry;
	read_Predicted_track_44_sub_label(1);
	//ChannelReceiveTry;
	read_Predicted_track_43_sub_label(1);
	//ChannelReceiveTry;
	read_Predicted_track_42_sub_label(1);
	//ChannelReceiveTry;
	read_Predicted_track_41_sub_label(1);
	//ChannelReceiveTry;
	read_Predicted_track_40_sub_label(1);
	//ChannelReceiveTry;
	read_Predicted_track_36_sub_label(1);
	//ChannelReceiveTry;
	read_Predicted_track_38_sub_label(1);
	//ChannelReceiveTry;
	read_Predicted_track_35_sub_label(1);
	//ChannelReceiveTry;
	read_Predicted_track_34_sub_label(1);
	//ChannelReceiveTry;
	read_Predicted_track_39_sub_label(1);
	//ChannelReceiveTry;
	read_Predicted_track_37_sub_label(1);
	//ChannelReceiveTry;
	read_Predicted_track_32_sub_label(1);
	//ChannelReceiveTry;
	read_Predicted_track_17_sub_label(1);
	//ChannelReceiveTry;
	read_Predicted_track_31_sub_label(1);
	//ChannelReceiveTry;
	read_Predicted_track_30_sub_label(1);
	//ChannelReceiveTry;
	read_Predicted_track_29_sub_label(1);
	//ChannelReceiveTry;
	read_Predicted_track_28_sub_label(1);
	//ChannelReceiveTry;
	read_Predicted_track_27_sub_label(1);
	//ChannelReceiveTry;
	read_Predicted_track_26_sub_label(1);
	//ChannelReceiveTry;
	read_Predicted_track_25_sub_label(1);
	//ChannelReceiveTry;
	read_Predicted_track_24_sub_label(1);
	//ChannelReceiveTry;
	read_Predicted_track_20_sub_label(1);
	//ChannelReceiveTry;
	read_Predicted_track_22_sub_label(1);
	//ChannelReceiveTry;
	read_Predicted_track_19_sub_label(1);
	//ChannelReceiveTry;
	read_Predicted_track_18_sub_label(1);
	//ChannelReceiveTry;
	read_Predicted_track_23_sub_label(1);
	//ChannelReceiveTry;
	read_Predicted_track_21_sub_label(1);
	//ChannelReceiveTry;
	read_Predicted_track_16_sub_label(1);
	//ChannelReceiveTry;
	read_Predicted_track_01_sub_label(1);
	//ChannelReceiveTry;
	read_Predicted_track_15_sub_label(1);
	//ChannelReceiveTry;
	read_Predicted_track_14_sub_label(1);
	//ChannelReceiveTry;
	read_Predicted_track_13_sub_label(1);
	//ChannelReceiveTry;
	read_Predicted_track_12_sub_label(1);
	//ChannelReceiveTry;
	read_Predicted_track_11_sub_label(1);
	//ChannelReceiveTry;
	read_Predicted_track_10_sub_label(1);
	//ChannelReceiveTry;
	read_Predicted_track_09_sub_label(1);
	//ChannelReceiveTry;
	read_Predicted_track_08_sub_label(1);
	//ChannelReceiveTry;
	read_Predicted_track_04_sub_label(1);
	//ChannelReceiveTry;
	read_Predicted_track_06_sub_label(1);
	//ChannelReceiveTry;
	read_Predicted_track_03_sub_label(1);
	//ChannelReceiveTry;
	read_Predicted_track_02_sub_label(1);
	//ChannelReceiveTry;
	read_Predicted_track_07_sub_label(1);
	//ChannelReceiveTry;
	read_Predicted_track_05_sub_label(1);
	//ChannelReceiveTry;
	read_Predicted_track_00_sub_label(80);
	read_CAMERA_post_time_synch(1);
	read_RADAR_post_time_synch(1);
	read_LiDAR_post_time_synch(1);
	executeTicks_DiscreteValueConstant(67800000);
	write_Track_objects(1);
}


// Runnable Track_management ----
void run_Track_management(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Tracked_object_label_N_publisher){
	read_Track_objects(1);
	executeTicks_DiscreteValueConstant(13560000);
	// publish_to_Tracked_object_label_N(Tracked_object_label_N_publisher);
}


// Runnable UKF_management ----
void run_UKF_management(){
	//ChannelReceiveTry;
	read_Tracked_object_label_N_sub_label(80);
	executeTicks_DiscreteValueConstant(6780000);
	write_UKF_index_00(1);
	write_UKF_index_08(1);
	write_UKF_index_15(1);
	write_UKF_index_01(1);
	write_UKF_index_02(1);
	write_UKF_index_03(1);
	write_UKF_index_04(1);
	write_UKF_index_05(1);
	write_UKF_index_06(1);
	write_UKF_index_07(1);
	write_UKF_index_09(1);
	write_UKF_index_10(1);
	write_UKF_index_11(1);
	write_UKF_index_12(1);
	write_UKF_index_13(1);
	write_UKF_index_14(1);
	write_UKF_index_16(1);
	write_UKF_index_24(1);
	write_UKF_index_31(1);
	write_UKF_index_17(1);
	write_UKF_index_18(1);
	write_UKF_index_19(1);
	write_UKF_index_20(1);
	write_UKF_index_21(1);
	write_UKF_index_22(1);
	write_UKF_index_23(1);
	write_UKF_index_25(1);
	write_UKF_index_26(1);
	write_UKF_index_27(1);
	write_UKF_index_28(1);
	write_UKF_index_29(1);
	write_UKF_index_30(1);
	write_UKF_index_32(1);
	write_UKF_index_40(1);
	write_UKF_index_47(1);
	write_UKF_index_33(1);
	write_UKF_index_34(1);
	write_UKF_index_35(1);
	write_UKF_index_36(1);
	write_UKF_index_37(1);
	write_UKF_index_38(1);
	write_UKF_index_39(1);
	write_UKF_index_41(1);
	write_UKF_index_42(1);
	write_UKF_index_43(1);
	write_UKF_index_44(1);
	write_UKF_index_45(1);
	write_UKF_index_46(1);
	write_UKF_index_59(1);
	write_UKF_index_48(1);
	write_UKF_index_49(1);
	write_UKF_index_50(1);
	write_UKF_index_51(1);
	write_UKF_index_52(1);
	write_UKF_index_53(1);
	write_UKF_index_54(1);
	write_UKF_index_55(1);
	write_UKF_index_56(1);
	write_UKF_index_57(1);
	write_UKF_index_58(1);
}


// Runnable UKF_00 ----
void run_UKF_00(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Potential_obstacle_N00_publisher, rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Predicted_track_00_publisher){
	read_UKF_index_00(1);
	executeTicks_DiscreteValueConstant(20340000);
	// publish_to_Potential_obstacle_N00(Potential_obstacle_N00_publisher);
	// publish_to_Predicted_track_00(Predicted_track_00_publisher);
}


// Runnable UKF_01 ----
void run_UKF_01(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Potential_obstacle_N01_publisher, rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Predicted_track_01_publisher){
	read_UKF_index_01(1);
	executeTicks_DiscreteValueConstant(20340000);
	// publish_to_Potential_obstacle_N01(Potential_obstacle_N01_publisher);
	// publish_to_Predicted_track_01(Predicted_track_01_publisher);
}


// Runnable UKF_02 ----
void run_UKF_02(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Potential_obstacle_N02_publisher, rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Predicted_track_02_publisher){
	read_UKF_index_02(1);
	executeTicks_DiscreteValueConstant(20340000);
	// publish_to_Potential_obstacle_N02(Potential_obstacle_N02_publisher);
	// publish_to_Predicted_track_02(Predicted_track_02_publisher);
}


// Runnable UKF_03 ----
void run_UKF_03(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Potential_obstacle_N03_publisher, rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Predicted_track_03_publisher){
	read_UKF_index_03(1);
	executeTicks_DiscreteValueConstant(20340000);
	// publish_to_Potential_obstacle_N03(Potential_obstacle_N03_publisher);
	// publish_to_Predicted_track_03(Predicted_track_03_publisher);
}


// Runnable UKF_04 ----
void run_UKF_04(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Potential_obstacle_N04_publisher, rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Predicted_track_04_publisher){
	read_UKF_index_04(1);
	executeTicks_DiscreteValueConstant(20340000);
	// publish_to_Potential_obstacle_N04(Potential_obstacle_N04_publisher);
	// publish_to_Predicted_track_04(Predicted_track_04_publisher);
}


// Runnable UKF_05 ----
void run_UKF_05(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Potential_obstacle_N05_publisher, rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Predicted_track_05_publisher){
	read_UKF_index_05(1);
	executeTicks_DiscreteValueConstant(20340000);
	// publish_to_Potential_obstacle_N05(Potential_obstacle_N05_publisher);
	// publish_to_Predicted_track_05(Predicted_track_05_publisher);
}


// Runnable UKF_06 ----
void run_UKF_06(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Potential_obstacle_N06_publisher, rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Predicted_track_06_publisher){
	read_UKF_index_06(1);
	executeTicks_DiscreteValueConstant(20340000);
	// publish_to_Potential_obstacle_N06(Potential_obstacle_N06_publisher);
	// publish_to_Predicted_track_06(Predicted_track_06_publisher);
}


// Runnable UKF_07 ----
void run_UKF_07(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Potential_obstacle_N07_publisher, rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Predicted_track_07_publisher){
	read_UKF_index_07(1);
	executeTicks_DiscreteValueConstant(20340000);
	// publish_to_Potential_obstacle_N07(Potential_obstacle_N07_publisher);
	// publish_to_Predicted_track_07(Predicted_track_07_publisher);
}


// Runnable UKF_08 ----
void run_UKF_08(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Potential_obstacle_N08_publisher, rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Predicted_track_08_publisher){
	read_UKF_index_08(1);
	executeTicks_DiscreteValueConstant(20340000);
	// publish_to_Potential_obstacle_N08(Potential_obstacle_N08_publisher);
	// publish_to_Predicted_track_08(Predicted_track_08_publisher);
}


// Runnable UKF_09 ----
void run_UKF_09(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Potential_obstacle_N09_publisher, rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Predicted_track_09_publisher){
	read_UKF_index_09(1);
	executeTicks_DiscreteValueConstant(20340000);
	// publish_to_Potential_obstacle_N09(Potential_obstacle_N09_publisher);
	// publish_to_Predicted_track_09(Predicted_track_09_publisher);
}


// Runnable UKF_10 ----
void run_UKF_10(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Potential_obstacle_N10_publisher, rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Predicted_track_10_publisher){
	read_UKF_index_10(1);
	executeTicks_DiscreteValueConstant(20340000);
	// publish_to_Potential_obstacle_N10(Potential_obstacle_N10_publisher);
	// publish_to_Predicted_track_10(Predicted_track_10_publisher);
}


// Runnable UKF_11 ----
void run_UKF_11(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Potential_obstacle_N11_publisher, rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Predicted_track_11_publisher){
	read_UKF_index_11(1);
	executeTicks_DiscreteValueConstant(20340000);
	// publish_to_Potential_obstacle_N11(Potential_obstacle_N11_publisher);
	// publish_to_Predicted_track_11(Predicted_track_11_publisher);
}


// Runnable UKF_12 ----
void run_UKF_12(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Potential_obstacle_N12_publisher, rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Predicted_track_12_publisher){
	read_UKF_index_12(1);
	executeTicks_DiscreteValueConstant(20340000);
	// publish_to_Potential_obstacle_N12(Potential_obstacle_N12_publisher);
	// publish_to_Predicted_track_12(Predicted_track_12_publisher);
}


// Runnable UKF_13 ----
void run_UKF_13(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Potential_obstacle_N13_publisher, rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Predicted_track_13_publisher){
	read_UKF_index_13(1);
	executeTicks_DiscreteValueConstant(20340000);
	// publish_to_Potential_obstacle_N13(Potential_obstacle_N13_publisher);
	// publish_to_Predicted_track_13(Predicted_track_13_publisher);
}


// Runnable UKF_14 ----
void run_UKF_14(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Potential_obstacle_N14_publisher, rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Predicted_track_14_publisher){
	read_UKF_index_14(1);
	executeTicks_DiscreteValueConstant(20340000);
	// publish_to_Potential_obstacle_N14(Potential_obstacle_N14_publisher);
	// publish_to_Predicted_track_14(Predicted_track_14_publisher);
}


// Runnable UKF_15 ----
void run_UKF_15(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Potential_obstacle_N15_publisher, rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Predicted_track_15_publisher){
	read_UKF_index_15(1);
	executeTicks_DiscreteValueConstant(20340000);
	// publish_to_Potential_obstacle_N15(Potential_obstacle_N15_publisher);
	// publish_to_Predicted_track_15(Predicted_track_15_publisher);
}


// Runnable UKF_16 ----
void run_UKF_16(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Potential_obstacle_N16_publisher, rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Predicted_track_16_publisher){
	read_UKF_index_16(1);
	executeTicks_DiscreteValueConstant(20340000);
	// publish_to_Potential_obstacle_N16(Potential_obstacle_N16_publisher);
	// publish_to_Predicted_track_16(Predicted_track_16_publisher);
}


// Runnable UKF_17 ----
void run_UKF_17(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Potential_obstacle_N17_publisher, rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Predicted_track_17_publisher){
	read_UKF_index_17(1);
	executeTicks_DiscreteValueConstant(20340000);
	// publish_to_Potential_obstacle_N17(Potential_obstacle_N17_publisher);
	// publish_to_Predicted_track_17(Predicted_track_17_publisher);
}


// Runnable UKF_18 ----
void run_UKF_18(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Potential_obstacle_N18_publisher, rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Predicted_track_18_publisher){
	read_UKF_index_18(1);
	executeTicks_DiscreteValueConstant(20340000);
	// publish_to_Potential_obstacle_N18(Potential_obstacle_N18_publisher);
	// publish_to_Predicted_track_18(Predicted_track_18_publisher);
}


// Runnable UKF_19 ----
void run_UKF_19(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Potential_obstacle_N19_publisher, rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Predicted_track_19_publisher){
	read_UKF_index_19(1);
	executeTicks_DiscreteValueConstant(20340000);
	// publish_to_Potential_obstacle_N19(Potential_obstacle_N19_publisher);
	// publish_to_Predicted_track_19(Predicted_track_19_publisher);
}


// Runnable UKF_20 ----
void run_UKF_20(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Potential_obstacle_N20_publisher, rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Predicted_track_20_publisher){
	read_UKF_index_20(1);
	executeTicks_DiscreteValueConstant(20340000);
	// publish_to_Potential_obstacle_N20(Potential_obstacle_N20_publisher);
	// publish_to_Predicted_track_20(Predicted_track_20_publisher);
}


// Runnable UKF_21 ----
void run_UKF_21(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Potential_obstacle_N21_publisher, rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Predicted_track_21_publisher){
	read_UKF_index_21(1);
	executeTicks_DiscreteValueConstant(20340000);
	// publish_to_Potential_obstacle_N21(Potential_obstacle_N21_publisher);
	// publish_to_Predicted_track_21(Predicted_track_21_publisher);
}


// Runnable UKF_22 ----
void run_UKF_22(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Potential_obstacle_N22_publisher, rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Predicted_track_22_publisher){
	read_UKF_index_22(1);
	executeTicks_DiscreteValueConstant(20340000);
	// publish_to_Potential_obstacle_N22(Potential_obstacle_N22_publisher);
	// publish_to_Predicted_track_22(Predicted_track_22_publisher);
}


// Runnable UKF_23 ----
void run_UKF_23(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Potential_obstacle_N23_publisher, rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Predicted_track_23_publisher){
	read_UKF_index_23(1);
	executeTicks_DiscreteValueConstant(20340000);
	// publish_to_Potential_obstacle_N23(Potential_obstacle_N23_publisher);
	// publish_to_Predicted_track_23(Predicted_track_23_publisher);
}


// Runnable UKF_24 ----
void run_UKF_24(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Potential_obstacle_N24_publisher, rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Predicted_track_24_publisher){
	read_UKF_index_24(1);
	executeTicks_DiscreteValueConstant(20340000);
	// publish_to_Potential_obstacle_N24(Potential_obstacle_N24_publisher);
	// publish_to_Predicted_track_24(Predicted_track_24_publisher);
}


// Runnable UKF_25 ----
void run_UKF_25(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Potential_obstacle_N25_publisher, rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Predicted_track_25_publisher){
	read_UKF_index_25(1);
	executeTicks_DiscreteValueConstant(20340000);
	// publish_to_Potential_obstacle_N25(Potential_obstacle_N25_publisher);
	// publish_to_Predicted_track_25(Predicted_track_25_publisher);
}


// Runnable UKF_26 ----
void run_UKF_26(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Potential_obstacle_N26_publisher, rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Predicted_track_26_publisher){
	read_UKF_index_26(1);
	executeTicks_DiscreteValueConstant(20340000);
	// publish_to_Potential_obstacle_N26(Potential_obstacle_N26_publisher);
	// publish_to_Predicted_track_26(Predicted_track_26_publisher);
}


// Runnable UKF_27 ----
void run_UKF_27(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Potential_obstacle_N27_publisher, rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Predicted_track_27_publisher){
	read_UKF_index_27(1);
	executeTicks_DiscreteValueConstant(20340000);
	// publish_to_Potential_obstacle_N27(Potential_obstacle_N27_publisher);
	// publish_to_Predicted_track_27(Predicted_track_27_publisher);
}


// Runnable UKF_28 ----
void run_UKF_28(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Potential_obstacle_N28_publisher, rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Predicted_track_28_publisher){
	read_UKF_index_28(1);
	executeTicks_DiscreteValueConstant(20340000);
	// publish_to_Potential_obstacle_N28(Potential_obstacle_N28_publisher);
	// publish_to_Predicted_track_28(Predicted_track_28_publisher);
}


// Runnable UKF_29 ----
void run_UKF_29(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Potential_obstacle_N29_publisher, rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Predicted_track_29_publisher){
	read_UKF_index_29(1);
	executeTicks_DiscreteValueConstant(20340000);
	// publish_to_Potential_obstacle_N29(Potential_obstacle_N29_publisher);
	// publish_to_Predicted_track_29(Predicted_track_29_publisher);
}


// Runnable UKF_30 ----
void run_UKF_30(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Potential_obstacle_N30_publisher, rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Predicted_track_30_publisher){
	read_UKF_index_30(1);
	executeTicks_DiscreteValueConstant(20340000);
	// publish_to_Potential_obstacle_N30(Potential_obstacle_N30_publisher);
	// publish_to_Predicted_track_30(Predicted_track_30_publisher);
}


// Runnable UKF_31 ----
void run_UKF_31(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Potential_obstacle_N31_publisher, rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Predicted_track_31_publisher){
	read_UKF_index_31(1);
	executeTicks_DiscreteValueConstant(20340000);
	// publish_to_Potential_obstacle_N31(Potential_obstacle_N31_publisher);
	// publish_to_Predicted_track_31(Predicted_track_31_publisher);
}


// Runnable UKF_32 ----
void run_UKF_32(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Potential_obstacle_N32_publisher, rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Predicted_track_32_publisher){
	read_UKF_index_32(1);
	executeTicks_DiscreteValueConstant(20340000);
	// publish_to_Potential_obstacle_N32(Potential_obstacle_N32_publisher);
	// publish_to_Predicted_track_32(Predicted_track_32_publisher);
}


// Runnable UKF_33 ----
void run_UKF_33(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Potential_obstacle_N33_publisher, rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Predicted_track_33_publisher){
	read_UKF_index_33(1);
	executeTicks_DiscreteValueConstant(20340000);
	// publish_to_Potential_obstacle_N33(Potential_obstacle_N33_publisher);
	// publish_to_Predicted_track_33(Predicted_track_33_publisher);
}


// Runnable UKF_34 ----
void run_UKF_34(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Potential_obstacle_N34_publisher, rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Predicted_track_34_publisher){
	read_UKF_index_34(1);
	executeTicks_DiscreteValueConstant(20340000);
	// publish_to_Potential_obstacle_N34(Potential_obstacle_N34_publisher);
	// publish_to_Predicted_track_34(Predicted_track_34_publisher);
}


// Runnable UKF_35 ----
void run_UKF_35(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Potential_obstacle_N35_publisher, rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Predicted_track_35_publisher){
	read_UKF_index_35(1);
	executeTicks_DiscreteValueConstant(20340000);
	// publish_to_Potential_obstacle_N35(Potential_obstacle_N35_publisher);
	// publish_to_Predicted_track_35(Predicted_track_35_publisher);
}


// Runnable UKF_36 ----
void run_UKF_36(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Potential_obstacle_N36_publisher, rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Predicted_track_36_publisher){
	read_UKF_index_36(1);
	executeTicks_DiscreteValueConstant(20340000);
	// publish_to_Potential_obstacle_N36(Potential_obstacle_N36_publisher);
	// publish_to_Predicted_track_36(Predicted_track_36_publisher);
}


// Runnable UKF_37 ----
void run_UKF_37(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Potential_obstacle_N37_publisher, rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Predicted_track_37_publisher){
	read_UKF_index_37(1);
	executeTicks_DiscreteValueConstant(20340000);
	// publish_to_Potential_obstacle_N37(Potential_obstacle_N37_publisher);
	// publish_to_Predicted_track_37(Predicted_track_37_publisher);
}


// Runnable UKF_38 ----
void run_UKF_38(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Potential_obstacle_N38_publisher, rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Predicted_track_38_publisher){
	read_UKF_index_38(1);
	executeTicks_DiscreteValueConstant(20340000);
	// publish_to_Potential_obstacle_N38(Potential_obstacle_N38_publisher);
	// publish_to_Predicted_track_38(Predicted_track_38_publisher);
}


// Runnable UKF_39 ----
void run_UKF_39(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Potential_obstacle_N39_publisher, rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Predicted_track_39_publisher){
	read_UKF_index_39(1);
	executeTicks_DiscreteValueConstant(20340000);
	// publish_to_Potential_obstacle_N39(Potential_obstacle_N39_publisher);
	// publish_to_Predicted_track_39(Predicted_track_39_publisher);
}


// Runnable UKF_40 ----
void run_UKF_40(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Potential_obstacle_N40_publisher, rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Predicted_track_40_publisher){
	read_UKF_index_40(1);
	executeTicks_DiscreteValueConstant(20340000);
	// publish_to_Potential_obstacle_N40(Potential_obstacle_N40_publisher);
	// publish_to_Predicted_track_40(Predicted_track_40_publisher);
}


// Runnable UKF_41 ----
void run_UKF_41(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Potential_obstacle_N41_publisher, rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Predicted_track_41_publisher){
	read_UKF_index_41(1);
	executeTicks_DiscreteValueConstant(20340000);
	// publish_to_Potential_obstacle_N41(Potential_obstacle_N41_publisher);
	// publish_to_Predicted_track_41(Predicted_track_41_publisher);
}


// Runnable UKF_42 ----
void run_UKF_42(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Potential_obstacle_N42_publisher, rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Predicted_track_42_publisher){
	read_UKF_index_42(1);
	executeTicks_DiscreteValueConstant(20340000);
	// publish_to_Potential_obstacle_N42(Potential_obstacle_N42_publisher);
	// publish_to_Predicted_track_42(Predicted_track_42_publisher);
}


// Runnable UKF_43 ----
void run_UKF_43(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Potential_obstacle_N43_publisher, rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Predicted_track_43_publisher){
	read_UKF_index_43(1);
	executeTicks_DiscreteValueConstant(20340000);
	// publish_to_Potential_obstacle_N43(Potential_obstacle_N43_publisher);
	// publish_to_Predicted_track_43(Predicted_track_43_publisher);
}


// Runnable UKF_44 ----
void run_UKF_44(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Potential_obstacle_N44_publisher, rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Predicted_track_44_publisher){
	read_UKF_index_44(1);
	executeTicks_DiscreteValueConstant(20340000);
	// publish_to_Potential_obstacle_N44(Potential_obstacle_N44_publisher);
	// publish_to_Predicted_track_44(Predicted_track_44_publisher);
}


// Runnable UKF_45 ----
void run_UKF_45(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Potential_obstacle_N45_publisher, rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Predicted_track_45_publisher){
	read_UKF_index_45(1);
	executeTicks_DiscreteValueConstant(20340000);
	// publish_to_Potential_obstacle_N45(Potential_obstacle_N45_publisher);
	// publish_to_Predicted_track_45(Predicted_track_45_publisher);
}


// Runnable UKF_46 ----
void run_UKF_46(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Potential_obstacle_N46_publisher, rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Predicted_track_46_publisher){
	read_UKF_index_46(1);
	executeTicks_DiscreteValueConstant(20340000);
	// publish_to_Potential_obstacle_N46(Potential_obstacle_N46_publisher);
	// publish_to_Predicted_track_46(Predicted_track_46_publisher);
}


// Runnable UKF_47 ----
void run_UKF_47(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Potential_obstacle_N47_publisher, rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Predicted_track_47_publisher){
	read_UKF_index_47(1);
	executeTicks_DiscreteValueConstant(20340000);
	// publish_to_Potential_obstacle_N47(Potential_obstacle_N47_publisher);
	// publish_to_Predicted_track_47(Predicted_track_47_publisher);
}


// Runnable UKF_48 ----
void run_UKF_48(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Potential_obstacle_N48_publisher, rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Predicted_track_48_publisher){
	read_UKF_index_48(1);
	executeTicks_DiscreteValueConstant(20340000);
	// publish_to_Potential_obstacle_N48(Potential_obstacle_N48_publisher);
	// publish_to_Predicted_track_48(Predicted_track_48_publisher);
}


// Runnable UKF_49 ----
void run_UKF_49(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Potential_obstacle_N49_publisher, rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Predicted_track_49_publisher){
	read_UKF_index_49(1);
	executeTicks_DiscreteValueConstant(20340000);
	// publish_to_Potential_obstacle_N49(Potential_obstacle_N49_publisher);
	// publish_to_Predicted_track_49(Predicted_track_49_publisher);
}


// Runnable UKF_50 ----
void run_UKF_50(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Potential_obstacle_N50_publisher, rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Predicted_track_50_publisher){
	read_UKF_index_50(1);
	executeTicks_DiscreteValueConstant(20340000);
	// publish_to_Potential_obstacle_N50(Potential_obstacle_N50_publisher);
	// publish_to_Predicted_track_50(Predicted_track_50_publisher);
}


// Runnable UKF_51 ----
void run_UKF_51(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Potential_obstacle_N51_publisher, rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Predicted_track_51_publisher){
	read_UKF_index_51(1);
	executeTicks_DiscreteValueConstant(20340000);
	// publish_to_Potential_obstacle_N51(Potential_obstacle_N51_publisher);
	// publish_to_Predicted_track_51(Predicted_track_51_publisher);
}


// Runnable UKF_52 ----
void run_UKF_52(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Potential_obstacle_N52_publisher, rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Predicted_track_52_publisher){
	read_UKF_index_52(1);
	executeTicks_DiscreteValueConstant(20340000);
	// publish_to_Potential_obstacle_N52(Potential_obstacle_N52_publisher);
	// publish_to_Predicted_track_52(Predicted_track_52_publisher);
}


// Runnable UKF_53 ----
void run_UKF_53(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Potential_obstacle_N53_publisher, rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Predicted_track_53_publisher){
	read_UKF_index_53(1);
	executeTicks_DiscreteValueConstant(20340000);
	// publish_to_Potential_obstacle_N53(Potential_obstacle_N53_publisher);
	// publish_to_Predicted_track_53(Predicted_track_53_publisher);
}


// Runnable UKF_54 ----
void run_UKF_54(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Potential_obstacle_N54_publisher, rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Predicted_track_54_publisher){
	read_UKF_index_54(1);
	executeTicks_DiscreteValueConstant(20340000);
	// publish_to_Potential_obstacle_N54(Potential_obstacle_N54_publisher);
	// publish_to_Predicted_track_54(Predicted_track_54_publisher);
}


// Runnable UKF_55 ----
void run_UKF_55(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Potential_obstacle_N55_publisher, rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Predicted_track_55_publisher){
	read_UKF_index_55(1);
	executeTicks_DiscreteValueConstant(20340000);
	// publish_to_Potential_obstacle_N55(Potential_obstacle_N55_publisher);
	// publish_to_Predicted_track_55(Predicted_track_55_publisher);
}


// Runnable UKF_56 ----
void run_UKF_56(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Potential_obstacle_N56_publisher, rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Predicted_track_56_publisher){
	read_UKF_index_56(1);
	executeTicks_DiscreteValueConstant(20340000);
	// publish_to_Potential_obstacle_N56(Potential_obstacle_N56_publisher);
	// publish_to_Predicted_track_56(Predicted_track_56_publisher);
}


// Runnable UKF_57 ----
void run_UKF_57(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Potential_obstacle_N57_publisher, rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Predicted_track_57_publisher){
	read_UKF_index_57(1);
	executeTicks_DiscreteValueConstant(20340000);
	// publish_to_Potential_obstacle_N57(Potential_obstacle_N57_publisher);
	// publish_to_Predicted_track_57(Predicted_track_57_publisher);
}


// Runnable UKF_58 ----
void run_UKF_58(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Potential_obstacle_N58_publisher, rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Predicted_track_58_publisher){
	read_UKF_index_58(1);
	executeTicks_DiscreteValueConstant(20340000);
	// publish_to_Potential_obstacle_N58(Potential_obstacle_N58_publisher);
	// publish_to_Predicted_track_58(Predicted_track_58_publisher);
}


// Runnable UKF_59 ----
void run_UKF_59(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Potential_obstacle_N59_publisher, rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& Predicted_track_59_publisher){
	read_UKF_index_59(1);
	executeTicks_DiscreteValueConstant(20340000);
	// publish_to_Potential_obstacle_N59(Potential_obstacle_N59_publisher);
	// publish_to_Predicted_track_59(Predicted_track_59_publisher);
}


// Runnable Collision_checking ----
void run_Collision_checking(){
	//ChannelReceiveTry;
	read_Potential_obstacle_N58_sub_label(1);
	//ChannelReceiveTry;
	read_Potential_obstacle_N57_sub_label(1);
	//ChannelReceiveTry;
	read_Potential_obstacle_N56_sub_label(1);
	//ChannelReceiveTry;
	read_Potential_obstacle_N55_sub_label(1);
	//ChannelReceiveTry;
	read_Potential_obstacle_N50_sub_label(1);
	//ChannelReceiveTry;
	read_Potential_obstacle_N49_sub_label(1);
	//ChannelReceiveTry;
	read_Potential_obstacle_N48_sub_label(1);
	//ChannelReceiveTry;
	read_Potential_obstacle_N54_sub_label(1);
	//ChannelReceiveTry;
	read_Potential_obstacle_N53_sub_label(1);
	//ChannelReceiveTry;
	read_Potential_obstacle_N52_sub_label(1);
	//ChannelReceiveTry;
	read_Potential_obstacle_N51_sub_label(1);
	//ChannelReceiveTry;
	read_Potential_obstacle_N59_sub_label(1);
	//ChannelReceiveTry;
	read_Potential_obstacle_N46_sub_label(1);
	//ChannelReceiveTry;
	read_Potential_obstacle_N45_sub_label(1);
	//ChannelReceiveTry;
	read_Potential_obstacle_N44_sub_label(1);
	//ChannelReceiveTry;
	read_Potential_obstacle_N43_sub_label(1);
	//ChannelReceiveTry;
	read_Potential_obstacle_N36_sub_label(1);
	//ChannelReceiveTry;
	read_Potential_obstacle_N35_sub_label(1);
	//ChannelReceiveTry;
	read_Potential_obstacle_N34_sub_label(1);
	//ChannelReceiveTry;
	read_Potential_obstacle_N33_sub_label(1);
	//ChannelReceiveTry;
	read_Potential_obstacle_N39_sub_label(1);
	//ChannelReceiveTry;
	read_Potential_obstacle_N42_sub_label(1);
	//ChannelReceiveTry;
	read_Potential_obstacle_N41_sub_label(1);
	//ChannelReceiveTry;
	read_Potential_obstacle_N38_sub_label(1);
	//ChannelReceiveTry;
	read_Potential_obstacle_N37_sub_label(1);
	//ChannelReceiveTry;
	read_Potential_obstacle_N47_sub_label(1);
	//ChannelReceiveTry;
	read_Potential_obstacle_N32_sub_label(1);
	//ChannelReceiveTry;
	read_Potential_obstacle_N40_sub_label(1);
	//ChannelReceiveTry;
	read_Potential_obstacle_N30_sub_label(1);
	//ChannelReceiveTry;
	read_Potential_obstacle_N29_sub_label(1);
	//ChannelReceiveTry;
	read_Potential_obstacle_N28_sub_label(1);
	//ChannelReceiveTry;
	read_Potential_obstacle_N27_sub_label(1);
	//ChannelReceiveTry;
	read_Potential_obstacle_N20_sub_label(1);
	//ChannelReceiveTry;
	read_Potential_obstacle_N19_sub_label(1);
	//ChannelReceiveTry;
	read_Potential_obstacle_N18_sub_label(1);
	//ChannelReceiveTry;
	read_Potential_obstacle_N17_sub_label(1);
	//ChannelReceiveTry;
	read_Potential_obstacle_N23_sub_label(1);
	//ChannelReceiveTry;
	read_Potential_obstacle_N26_sub_label(1);
	//ChannelReceiveTry;
	read_Potential_obstacle_N25_sub_label(1);
	//ChannelReceiveTry;
	read_Potential_obstacle_N22_sub_label(1);
	//ChannelReceiveTry;
	read_Potential_obstacle_N21_sub_label(1);
	//ChannelReceiveTry;
	read_Potential_obstacle_N31_sub_label(1);
	//ChannelReceiveTry;
	read_Potential_obstacle_N16_sub_label(1);
	//ChannelReceiveTry;
	read_Potential_obstacle_N24_sub_label(1);
	//ChannelReceiveTry;
	read_Potential_obstacle_N14_sub_label(1);
	//ChannelReceiveTry;
	read_Potential_obstacle_N13_sub_label(1);
	//ChannelReceiveTry;
	read_Potential_obstacle_N12_sub_label(1);
	//ChannelReceiveTry;
	read_Potential_obstacle_N11_sub_label(1);
	//ChannelReceiveTry;
	read_Potential_obstacle_N04_sub_label(1);
	//ChannelReceiveTry;
	read_Potential_obstacle_N03_sub_label(1);
	//ChannelReceiveTry;
	read_Potential_obstacle_N02_sub_label(1);
	//ChannelReceiveTry;
	read_Potential_obstacle_N01_sub_label(1);
	//ChannelReceiveTry;
	read_Potential_obstacle_N07_sub_label(1);
	//ChannelReceiveTry;
	read_Potential_obstacle_N10_sub_label(1);
	//ChannelReceiveTry;
	read_Potential_obstacle_N09_sub_label(1);
	//ChannelReceiveTry;
	read_Potential_obstacle_N06_sub_label(1);
	//ChannelReceiveTry;
	read_Potential_obstacle_N05_sub_label(1);
	//ChannelReceiveTry;
	read_Potential_obstacle_N15_sub_label(1);
	//ChannelReceiveTry;
	read_Potential_obstacle_N00_sub_label(1);
	//ChannelReceiveTry;
	read_Potential_obstacle_N08_sub_label(1);
	executeTicks_DiscreteValueConstant(27120000);
}

