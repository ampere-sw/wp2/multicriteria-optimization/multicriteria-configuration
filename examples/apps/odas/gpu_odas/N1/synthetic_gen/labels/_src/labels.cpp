// This code is auto-generated

#include "labels.hpp"

#include <stdlib.h>
int RADAR_points[0];

static bool isIinitialized_RADAR_points = false;
void initialize_RADAR_points() {
	if (!isIinitialized_RADAR_points){
		int i;
		for (i=0; i < 0; i++){
			RADAR_points[i] = i+1;
		}
		isIinitialized_RADAR_points = true;
	}
}

void read_RADAR_points(int labelAccessStatistics) {
	int numberOfBytes = 1;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(RADAR_points) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = RADAR_points[i];
			a = RADAR_points[i+1];
			a = RADAR_points[i+2];
			a = RADAR_points[i+3];
			a = RADAR_points[i+4];
			a = RADAR_points[i+5];
			a = RADAR_points[i+6];
			a = RADAR_points[i+7];
			a = RADAR_points[i+8];
			a = RADAR_points[i+9];
		}
	
		for(;i<arraysize;i++){
			a = RADAR_points[i];
		}

		(void)a;
	}
}

void write_RADAR_points(int labelAccessStatistics) {
	int numberOfBytes = 1;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(RADAR_points) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			RADAR_points[i]   = 0x800A;
			RADAR_points[i+1] = 0xAFFE;
			RADAR_points[i+2] = 0xAFFE;
			RADAR_points[i+3] = 0xAFFE;
			RADAR_points[i+4] = 0xAFFE;
			RADAR_points[i+5] = 0xAFFE;
			RADAR_points[i+6] = 0xAFFE;
			RADAR_points[i+7] = 0xAFFE;
			RADAR_points[i+8] = 0xAFFE;
			RADAR_points[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				RADAR_points[i]=0xAFFE;
		}
	}
}

int RADAR_points_in_VBF_coord[0];

static bool isIinitialized_RADAR_points_in_VBF_coord = false;
void initialize_RADAR_points_in_VBF_coord() {
	if (!isIinitialized_RADAR_points_in_VBF_coord){
		int i;
		for (i=0; i < 0; i++){
			RADAR_points_in_VBF_coord[i] = i+1;
		}
		isIinitialized_RADAR_points_in_VBF_coord = true;
	}
}

void read_RADAR_points_in_VBF_coord(int labelAccessStatistics) {
	int numberOfBytes = 1;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(RADAR_points_in_VBF_coord) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = RADAR_points_in_VBF_coord[i];
			a = RADAR_points_in_VBF_coord[i+1];
			a = RADAR_points_in_VBF_coord[i+2];
			a = RADAR_points_in_VBF_coord[i+3];
			a = RADAR_points_in_VBF_coord[i+4];
			a = RADAR_points_in_VBF_coord[i+5];
			a = RADAR_points_in_VBF_coord[i+6];
			a = RADAR_points_in_VBF_coord[i+7];
			a = RADAR_points_in_VBF_coord[i+8];
			a = RADAR_points_in_VBF_coord[i+9];
		}
	
		for(;i<arraysize;i++){
			a = RADAR_points_in_VBF_coord[i];
		}

		(void)a;
	}
}

void write_RADAR_points_in_VBF_coord(int labelAccessStatistics) {
	int numberOfBytes = 1;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(RADAR_points_in_VBF_coord) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			RADAR_points_in_VBF_coord[i]   = 0x800A;
			RADAR_points_in_VBF_coord[i+1] = 0xAFFE;
			RADAR_points_in_VBF_coord[i+2] = 0xAFFE;
			RADAR_points_in_VBF_coord[i+3] = 0xAFFE;
			RADAR_points_in_VBF_coord[i+4] = 0xAFFE;
			RADAR_points_in_VBF_coord[i+5] = 0xAFFE;
			RADAR_points_in_VBF_coord[i+6] = 0xAFFE;
			RADAR_points_in_VBF_coord[i+7] = 0xAFFE;
			RADAR_points_in_VBF_coord[i+8] = 0xAFFE;
			RADAR_points_in_VBF_coord[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				RADAR_points_in_VBF_coord[i]=0xAFFE;
		}
	}
}

int ThreeD_point_cloud[0];

static bool isIinitialized_ThreeD_point_cloud = false;
void initialize_ThreeD_point_cloud() {
	if (!isIinitialized_ThreeD_point_cloud){
		int i;
		for (i=0; i < 0; i++){
			ThreeD_point_cloud[i] = i+1;
		}
		isIinitialized_ThreeD_point_cloud = true;
	}
}

void read_ThreeD_point_cloud(int labelAccessStatistics) {
	int numberOfBytes = 1;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(ThreeD_point_cloud) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = ThreeD_point_cloud[i];
			a = ThreeD_point_cloud[i+1];
			a = ThreeD_point_cloud[i+2];
			a = ThreeD_point_cloud[i+3];
			a = ThreeD_point_cloud[i+4];
			a = ThreeD_point_cloud[i+5];
			a = ThreeD_point_cloud[i+6];
			a = ThreeD_point_cloud[i+7];
			a = ThreeD_point_cloud[i+8];
			a = ThreeD_point_cloud[i+9];
		}
	
		for(;i<arraysize;i++){
			a = ThreeD_point_cloud[i];
		}

		(void)a;
	}
}

void write_ThreeD_point_cloud(int labelAccessStatistics) {
	int numberOfBytes = 1;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(ThreeD_point_cloud) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			ThreeD_point_cloud[i]   = 0x800A;
			ThreeD_point_cloud[i+1] = 0xAFFE;
			ThreeD_point_cloud[i+2] = 0xAFFE;
			ThreeD_point_cloud[i+3] = 0xAFFE;
			ThreeD_point_cloud[i+4] = 0xAFFE;
			ThreeD_point_cloud[i+5] = 0xAFFE;
			ThreeD_point_cloud[i+6] = 0xAFFE;
			ThreeD_point_cloud[i+7] = 0xAFFE;
			ThreeD_point_cloud[i+8] = 0xAFFE;
			ThreeD_point_cloud[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				ThreeD_point_cloud[i]=0xAFFE;
		}
	}
}

int LiDAR_point_cloud_in_VBF_coord[0];

static bool isIinitialized_LiDAR_point_cloud_in_VBF_coord = false;
void initialize_LiDAR_point_cloud_in_VBF_coord() {
	if (!isIinitialized_LiDAR_point_cloud_in_VBF_coord){
		int i;
		for (i=0; i < 0; i++){
			LiDAR_point_cloud_in_VBF_coord[i] = i+1;
		}
		isIinitialized_LiDAR_point_cloud_in_VBF_coord = true;
	}
}

void read_LiDAR_point_cloud_in_VBF_coord(int labelAccessStatistics) {
	int numberOfBytes = 1;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(LiDAR_point_cloud_in_VBF_coord) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = LiDAR_point_cloud_in_VBF_coord[i];
			a = LiDAR_point_cloud_in_VBF_coord[i+1];
			a = LiDAR_point_cloud_in_VBF_coord[i+2];
			a = LiDAR_point_cloud_in_VBF_coord[i+3];
			a = LiDAR_point_cloud_in_VBF_coord[i+4];
			a = LiDAR_point_cloud_in_VBF_coord[i+5];
			a = LiDAR_point_cloud_in_VBF_coord[i+6];
			a = LiDAR_point_cloud_in_VBF_coord[i+7];
			a = LiDAR_point_cloud_in_VBF_coord[i+8];
			a = LiDAR_point_cloud_in_VBF_coord[i+9];
		}
	
		for(;i<arraysize;i++){
			a = LiDAR_point_cloud_in_VBF_coord[i];
		}

		(void)a;
	}
}

void write_LiDAR_point_cloud_in_VBF_coord(int labelAccessStatistics) {
	int numberOfBytes = 1;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(LiDAR_point_cloud_in_VBF_coord) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			LiDAR_point_cloud_in_VBF_coord[i]   = 0x800A;
			LiDAR_point_cloud_in_VBF_coord[i+1] = 0xAFFE;
			LiDAR_point_cloud_in_VBF_coord[i+2] = 0xAFFE;
			LiDAR_point_cloud_in_VBF_coord[i+3] = 0xAFFE;
			LiDAR_point_cloud_in_VBF_coord[i+4] = 0xAFFE;
			LiDAR_point_cloud_in_VBF_coord[i+5] = 0xAFFE;
			LiDAR_point_cloud_in_VBF_coord[i+6] = 0xAFFE;
			LiDAR_point_cloud_in_VBF_coord[i+7] = 0xAFFE;
			LiDAR_point_cloud_in_VBF_coord[i+8] = 0xAFFE;
			LiDAR_point_cloud_in_VBF_coord[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				LiDAR_point_cloud_in_VBF_coord[i]=0xAFFE;
		}
	}
}

int CAMERA_frame_sub_label[0];

static bool isIinitialized_CAMERA_frame_sub_label = false;
void initialize_CAMERA_frame_sub_label() {
	if (!isIinitialized_CAMERA_frame_sub_label){
		int i;
		for (i=0; i < 0; i++){
			CAMERA_frame_sub_label[i] = i+1;
		}
		isIinitialized_CAMERA_frame_sub_label = true;
	}
}

void read_CAMERA_frame_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(CAMERA_frame_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = CAMERA_frame_sub_label[i];
			a = CAMERA_frame_sub_label[i+1];
			a = CAMERA_frame_sub_label[i+2];
			a = CAMERA_frame_sub_label[i+3];
			a = CAMERA_frame_sub_label[i+4];
			a = CAMERA_frame_sub_label[i+5];
			a = CAMERA_frame_sub_label[i+6];
			a = CAMERA_frame_sub_label[i+7];
			a = CAMERA_frame_sub_label[i+8];
			a = CAMERA_frame_sub_label[i+9];
		}
	
		for(;i<arraysize;i++){
			a = CAMERA_frame_sub_label[i];
		}

		(void)a;
	}
}

void write_CAMERA_frame_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(CAMERA_frame_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			CAMERA_frame_sub_label[i]   = 0x800A;
			CAMERA_frame_sub_label[i+1] = 0xAFFE;
			CAMERA_frame_sub_label[i+2] = 0xAFFE;
			CAMERA_frame_sub_label[i+3] = 0xAFFE;
			CAMERA_frame_sub_label[i+4] = 0xAFFE;
			CAMERA_frame_sub_label[i+5] = 0xAFFE;
			CAMERA_frame_sub_label[i+6] = 0xAFFE;
			CAMERA_frame_sub_label[i+7] = 0xAFFE;
			CAMERA_frame_sub_label[i+8] = 0xAFFE;
			CAMERA_frame_sub_label[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				CAMERA_frame_sub_label[i]=0xAFFE;
		}
	}
}

int CAMERA_bounding_box_lists[0];

static bool isIinitialized_CAMERA_bounding_box_lists = false;
void initialize_CAMERA_bounding_box_lists() {
	if (!isIinitialized_CAMERA_bounding_box_lists){
		int i;
		for (i=0; i < 0; i++){
			CAMERA_bounding_box_lists[i] = i+1;
		}
		isIinitialized_CAMERA_bounding_box_lists = true;
	}
}

void read_CAMERA_bounding_box_lists(int labelAccessStatistics) {
	int numberOfBytes = 1;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(CAMERA_bounding_box_lists) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = CAMERA_bounding_box_lists[i];
			a = CAMERA_bounding_box_lists[i+1];
			a = CAMERA_bounding_box_lists[i+2];
			a = CAMERA_bounding_box_lists[i+3];
			a = CAMERA_bounding_box_lists[i+4];
			a = CAMERA_bounding_box_lists[i+5];
			a = CAMERA_bounding_box_lists[i+6];
			a = CAMERA_bounding_box_lists[i+7];
			a = CAMERA_bounding_box_lists[i+8];
			a = CAMERA_bounding_box_lists[i+9];
		}
	
		for(;i<arraysize;i++){
			a = CAMERA_bounding_box_lists[i];
		}

		(void)a;
	}
}

void write_CAMERA_bounding_box_lists(int labelAccessStatistics) {
	int numberOfBytes = 1;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(CAMERA_bounding_box_lists) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			CAMERA_bounding_box_lists[i]   = 0x800A;
			CAMERA_bounding_box_lists[i+1] = 0xAFFE;
			CAMERA_bounding_box_lists[i+2] = 0xAFFE;
			CAMERA_bounding_box_lists[i+3] = 0xAFFE;
			CAMERA_bounding_box_lists[i+4] = 0xAFFE;
			CAMERA_bounding_box_lists[i+5] = 0xAFFE;
			CAMERA_bounding_box_lists[i+6] = 0xAFFE;
			CAMERA_bounding_box_lists[i+7] = 0xAFFE;
			CAMERA_bounding_box_lists[i+8] = 0xAFFE;
			CAMERA_bounding_box_lists[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				CAMERA_bounding_box_lists[i]=0xAFFE;
		}
	}
}

int Bounding_box_in_VBF_coord_sub_label[0];

static bool isIinitialized_Bounding_box_in_VBF_coord_sub_label = false;
void initialize_Bounding_box_in_VBF_coord_sub_label() {
	if (!isIinitialized_Bounding_box_in_VBF_coord_sub_label){
		int i;
		for (i=0; i < 0; i++){
			Bounding_box_in_VBF_coord_sub_label[i] = i+1;
		}
		isIinitialized_Bounding_box_in_VBF_coord_sub_label = true;
	}
}

void read_Bounding_box_in_VBF_coord_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(Bounding_box_in_VBF_coord_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = Bounding_box_in_VBF_coord_sub_label[i];
			a = Bounding_box_in_VBF_coord_sub_label[i+1];
			a = Bounding_box_in_VBF_coord_sub_label[i+2];
			a = Bounding_box_in_VBF_coord_sub_label[i+3];
			a = Bounding_box_in_VBF_coord_sub_label[i+4];
			a = Bounding_box_in_VBF_coord_sub_label[i+5];
			a = Bounding_box_in_VBF_coord_sub_label[i+6];
			a = Bounding_box_in_VBF_coord_sub_label[i+7];
			a = Bounding_box_in_VBF_coord_sub_label[i+8];
			a = Bounding_box_in_VBF_coord_sub_label[i+9];
		}
	
		for(;i<arraysize;i++){
			a = Bounding_box_in_VBF_coord_sub_label[i];
		}

		(void)a;
	}
}

void write_Bounding_box_in_VBF_coord_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(Bounding_box_in_VBF_coord_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			Bounding_box_in_VBF_coord_sub_label[i]   = 0x800A;
			Bounding_box_in_VBF_coord_sub_label[i+1] = 0xAFFE;
			Bounding_box_in_VBF_coord_sub_label[i+2] = 0xAFFE;
			Bounding_box_in_VBF_coord_sub_label[i+3] = 0xAFFE;
			Bounding_box_in_VBF_coord_sub_label[i+4] = 0xAFFE;
			Bounding_box_in_VBF_coord_sub_label[i+5] = 0xAFFE;
			Bounding_box_in_VBF_coord_sub_label[i+6] = 0xAFFE;
			Bounding_box_in_VBF_coord_sub_label[i+7] = 0xAFFE;
			Bounding_box_in_VBF_coord_sub_label[i+8] = 0xAFFE;
			Bounding_box_in_VBF_coord_sub_label[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				Bounding_box_in_VBF_coord_sub_label[i]=0xAFFE;
		}
	}
}

int RADAR_objects_in_VBF_coord_sub_label[0];

static bool isIinitialized_RADAR_objects_in_VBF_coord_sub_label = false;
void initialize_RADAR_objects_in_VBF_coord_sub_label() {
	if (!isIinitialized_RADAR_objects_in_VBF_coord_sub_label){
		int i;
		for (i=0; i < 0; i++){
			RADAR_objects_in_VBF_coord_sub_label[i] = i+1;
		}
		isIinitialized_RADAR_objects_in_VBF_coord_sub_label = true;
	}
}

void read_RADAR_objects_in_VBF_coord_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(RADAR_objects_in_VBF_coord_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = RADAR_objects_in_VBF_coord_sub_label[i];
			a = RADAR_objects_in_VBF_coord_sub_label[i+1];
			a = RADAR_objects_in_VBF_coord_sub_label[i+2];
			a = RADAR_objects_in_VBF_coord_sub_label[i+3];
			a = RADAR_objects_in_VBF_coord_sub_label[i+4];
			a = RADAR_objects_in_VBF_coord_sub_label[i+5];
			a = RADAR_objects_in_VBF_coord_sub_label[i+6];
			a = RADAR_objects_in_VBF_coord_sub_label[i+7];
			a = RADAR_objects_in_VBF_coord_sub_label[i+8];
			a = RADAR_objects_in_VBF_coord_sub_label[i+9];
		}
	
		for(;i<arraysize;i++){
			a = RADAR_objects_in_VBF_coord_sub_label[i];
		}

		(void)a;
	}
}

void write_RADAR_objects_in_VBF_coord_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(RADAR_objects_in_VBF_coord_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			RADAR_objects_in_VBF_coord_sub_label[i]   = 0x800A;
			RADAR_objects_in_VBF_coord_sub_label[i+1] = 0xAFFE;
			RADAR_objects_in_VBF_coord_sub_label[i+2] = 0xAFFE;
			RADAR_objects_in_VBF_coord_sub_label[i+3] = 0xAFFE;
			RADAR_objects_in_VBF_coord_sub_label[i+4] = 0xAFFE;
			RADAR_objects_in_VBF_coord_sub_label[i+5] = 0xAFFE;
			RADAR_objects_in_VBF_coord_sub_label[i+6] = 0xAFFE;
			RADAR_objects_in_VBF_coord_sub_label[i+7] = 0xAFFE;
			RADAR_objects_in_VBF_coord_sub_label[i+8] = 0xAFFE;
			RADAR_objects_in_VBF_coord_sub_label[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				RADAR_objects_in_VBF_coord_sub_label[i]=0xAFFE;
		}
	}
}

int LiDAR_objects_in_VBF_coord_sub_label[0];

static bool isIinitialized_LiDAR_objects_in_VBF_coord_sub_label = false;
void initialize_LiDAR_objects_in_VBF_coord_sub_label() {
	if (!isIinitialized_LiDAR_objects_in_VBF_coord_sub_label){
		int i;
		for (i=0; i < 0; i++){
			LiDAR_objects_in_VBF_coord_sub_label[i] = i+1;
		}
		isIinitialized_LiDAR_objects_in_VBF_coord_sub_label = true;
	}
}

void read_LiDAR_objects_in_VBF_coord_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(LiDAR_objects_in_VBF_coord_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = LiDAR_objects_in_VBF_coord_sub_label[i];
			a = LiDAR_objects_in_VBF_coord_sub_label[i+1];
			a = LiDAR_objects_in_VBF_coord_sub_label[i+2];
			a = LiDAR_objects_in_VBF_coord_sub_label[i+3];
			a = LiDAR_objects_in_VBF_coord_sub_label[i+4];
			a = LiDAR_objects_in_VBF_coord_sub_label[i+5];
			a = LiDAR_objects_in_VBF_coord_sub_label[i+6];
			a = LiDAR_objects_in_VBF_coord_sub_label[i+7];
			a = LiDAR_objects_in_VBF_coord_sub_label[i+8];
			a = LiDAR_objects_in_VBF_coord_sub_label[i+9];
		}
	
		for(;i<arraysize;i++){
			a = LiDAR_objects_in_VBF_coord_sub_label[i];
		}

		(void)a;
	}
}

void write_LiDAR_objects_in_VBF_coord_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(LiDAR_objects_in_VBF_coord_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			LiDAR_objects_in_VBF_coord_sub_label[i]   = 0x800A;
			LiDAR_objects_in_VBF_coord_sub_label[i+1] = 0xAFFE;
			LiDAR_objects_in_VBF_coord_sub_label[i+2] = 0xAFFE;
			LiDAR_objects_in_VBF_coord_sub_label[i+3] = 0xAFFE;
			LiDAR_objects_in_VBF_coord_sub_label[i+4] = 0xAFFE;
			LiDAR_objects_in_VBF_coord_sub_label[i+5] = 0xAFFE;
			LiDAR_objects_in_VBF_coord_sub_label[i+6] = 0xAFFE;
			LiDAR_objects_in_VBF_coord_sub_label[i+7] = 0xAFFE;
			LiDAR_objects_in_VBF_coord_sub_label[i+8] = 0xAFFE;
			LiDAR_objects_in_VBF_coord_sub_label[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				LiDAR_objects_in_VBF_coord_sub_label[i]=0xAFFE;
		}
	}
}

int CAMERA_post_time_synch[0];

static bool isIinitialized_CAMERA_post_time_synch = false;
void initialize_CAMERA_post_time_synch() {
	if (!isIinitialized_CAMERA_post_time_synch){
		int i;
		for (i=0; i < 0; i++){
			CAMERA_post_time_synch[i] = i+1;
		}
		isIinitialized_CAMERA_post_time_synch = true;
	}
}

void read_CAMERA_post_time_synch(int labelAccessStatistics) {
	int numberOfBytes = 1;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(CAMERA_post_time_synch) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = CAMERA_post_time_synch[i];
			a = CAMERA_post_time_synch[i+1];
			a = CAMERA_post_time_synch[i+2];
			a = CAMERA_post_time_synch[i+3];
			a = CAMERA_post_time_synch[i+4];
			a = CAMERA_post_time_synch[i+5];
			a = CAMERA_post_time_synch[i+6];
			a = CAMERA_post_time_synch[i+7];
			a = CAMERA_post_time_synch[i+8];
			a = CAMERA_post_time_synch[i+9];
		}
	
		for(;i<arraysize;i++){
			a = CAMERA_post_time_synch[i];
		}

		(void)a;
	}
}

void write_CAMERA_post_time_synch(int labelAccessStatistics) {
	int numberOfBytes = 1;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(CAMERA_post_time_synch) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			CAMERA_post_time_synch[i]   = 0x800A;
			CAMERA_post_time_synch[i+1] = 0xAFFE;
			CAMERA_post_time_synch[i+2] = 0xAFFE;
			CAMERA_post_time_synch[i+3] = 0xAFFE;
			CAMERA_post_time_synch[i+4] = 0xAFFE;
			CAMERA_post_time_synch[i+5] = 0xAFFE;
			CAMERA_post_time_synch[i+6] = 0xAFFE;
			CAMERA_post_time_synch[i+7] = 0xAFFE;
			CAMERA_post_time_synch[i+8] = 0xAFFE;
			CAMERA_post_time_synch[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				CAMERA_post_time_synch[i]=0xAFFE;
		}
	}
}

int RADAR_post_time_synch[0];

static bool isIinitialized_RADAR_post_time_synch = false;
void initialize_RADAR_post_time_synch() {
	if (!isIinitialized_RADAR_post_time_synch){
		int i;
		for (i=0; i < 0; i++){
			RADAR_post_time_synch[i] = i+1;
		}
		isIinitialized_RADAR_post_time_synch = true;
	}
}

void read_RADAR_post_time_synch(int labelAccessStatistics) {
	int numberOfBytes = 1;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(RADAR_post_time_synch) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = RADAR_post_time_synch[i];
			a = RADAR_post_time_synch[i+1];
			a = RADAR_post_time_synch[i+2];
			a = RADAR_post_time_synch[i+3];
			a = RADAR_post_time_synch[i+4];
			a = RADAR_post_time_synch[i+5];
			a = RADAR_post_time_synch[i+6];
			a = RADAR_post_time_synch[i+7];
			a = RADAR_post_time_synch[i+8];
			a = RADAR_post_time_synch[i+9];
		}
	
		for(;i<arraysize;i++){
			a = RADAR_post_time_synch[i];
		}

		(void)a;
	}
}

void write_RADAR_post_time_synch(int labelAccessStatistics) {
	int numberOfBytes = 1;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(RADAR_post_time_synch) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			RADAR_post_time_synch[i]   = 0x800A;
			RADAR_post_time_synch[i+1] = 0xAFFE;
			RADAR_post_time_synch[i+2] = 0xAFFE;
			RADAR_post_time_synch[i+3] = 0xAFFE;
			RADAR_post_time_synch[i+4] = 0xAFFE;
			RADAR_post_time_synch[i+5] = 0xAFFE;
			RADAR_post_time_synch[i+6] = 0xAFFE;
			RADAR_post_time_synch[i+7] = 0xAFFE;
			RADAR_post_time_synch[i+8] = 0xAFFE;
			RADAR_post_time_synch[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				RADAR_post_time_synch[i]=0xAFFE;
		}
	}
}

int LiDAR_post_time_synch[0];

static bool isIinitialized_LiDAR_post_time_synch = false;
void initialize_LiDAR_post_time_synch() {
	if (!isIinitialized_LiDAR_post_time_synch){
		int i;
		for (i=0; i < 0; i++){
			LiDAR_post_time_synch[i] = i+1;
		}
		isIinitialized_LiDAR_post_time_synch = true;
	}
}

void read_LiDAR_post_time_synch(int labelAccessStatistics) {
	int numberOfBytes = 1;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(LiDAR_post_time_synch) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = LiDAR_post_time_synch[i];
			a = LiDAR_post_time_synch[i+1];
			a = LiDAR_post_time_synch[i+2];
			a = LiDAR_post_time_synch[i+3];
			a = LiDAR_post_time_synch[i+4];
			a = LiDAR_post_time_synch[i+5];
			a = LiDAR_post_time_synch[i+6];
			a = LiDAR_post_time_synch[i+7];
			a = LiDAR_post_time_synch[i+8];
			a = LiDAR_post_time_synch[i+9];
		}
	
		for(;i<arraysize;i++){
			a = LiDAR_post_time_synch[i];
		}

		(void)a;
	}
}

void write_LiDAR_post_time_synch(int labelAccessStatistics) {
	int numberOfBytes = 1;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(LiDAR_post_time_synch) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			LiDAR_post_time_synch[i]   = 0x800A;
			LiDAR_post_time_synch[i+1] = 0xAFFE;
			LiDAR_post_time_synch[i+2] = 0xAFFE;
			LiDAR_post_time_synch[i+3] = 0xAFFE;
			LiDAR_post_time_synch[i+4] = 0xAFFE;
			LiDAR_post_time_synch[i+5] = 0xAFFE;
			LiDAR_post_time_synch[i+6] = 0xAFFE;
			LiDAR_post_time_synch[i+7] = 0xAFFE;
			LiDAR_post_time_synch[i+8] = 0xAFFE;
			LiDAR_post_time_synch[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				LiDAR_post_time_synch[i]=0xAFFE;
		}
	}
}

int Predicted_track_48_sub_label[0];

static bool isIinitialized_Predicted_track_48_sub_label = false;
void initialize_Predicted_track_48_sub_label() {
	if (!isIinitialized_Predicted_track_48_sub_label){
		int i;
		for (i=0; i < 0; i++){
			Predicted_track_48_sub_label[i] = i+1;
		}
		isIinitialized_Predicted_track_48_sub_label = true;
	}
}

void read_Predicted_track_48_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(Predicted_track_48_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = Predicted_track_48_sub_label[i];
			a = Predicted_track_48_sub_label[i+1];
			a = Predicted_track_48_sub_label[i+2];
			a = Predicted_track_48_sub_label[i+3];
			a = Predicted_track_48_sub_label[i+4];
			a = Predicted_track_48_sub_label[i+5];
			a = Predicted_track_48_sub_label[i+6];
			a = Predicted_track_48_sub_label[i+7];
			a = Predicted_track_48_sub_label[i+8];
			a = Predicted_track_48_sub_label[i+9];
		}
	
		for(;i<arraysize;i++){
			a = Predicted_track_48_sub_label[i];
		}

		(void)a;
	}
}

void write_Predicted_track_48_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(Predicted_track_48_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			Predicted_track_48_sub_label[i]   = 0x800A;
			Predicted_track_48_sub_label[i+1] = 0xAFFE;
			Predicted_track_48_sub_label[i+2] = 0xAFFE;
			Predicted_track_48_sub_label[i+3] = 0xAFFE;
			Predicted_track_48_sub_label[i+4] = 0xAFFE;
			Predicted_track_48_sub_label[i+5] = 0xAFFE;
			Predicted_track_48_sub_label[i+6] = 0xAFFE;
			Predicted_track_48_sub_label[i+7] = 0xAFFE;
			Predicted_track_48_sub_label[i+8] = 0xAFFE;
			Predicted_track_48_sub_label[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				Predicted_track_48_sub_label[i]=0xAFFE;
		}
	}
}

int Predicted_track_59_sub_label[0];

static bool isIinitialized_Predicted_track_59_sub_label = false;
void initialize_Predicted_track_59_sub_label() {
	if (!isIinitialized_Predicted_track_59_sub_label){
		int i;
		for (i=0; i < 0; i++){
			Predicted_track_59_sub_label[i] = i+1;
		}
		isIinitialized_Predicted_track_59_sub_label = true;
	}
}

void read_Predicted_track_59_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(Predicted_track_59_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = Predicted_track_59_sub_label[i];
			a = Predicted_track_59_sub_label[i+1];
			a = Predicted_track_59_sub_label[i+2];
			a = Predicted_track_59_sub_label[i+3];
			a = Predicted_track_59_sub_label[i+4];
			a = Predicted_track_59_sub_label[i+5];
			a = Predicted_track_59_sub_label[i+6];
			a = Predicted_track_59_sub_label[i+7];
			a = Predicted_track_59_sub_label[i+8];
			a = Predicted_track_59_sub_label[i+9];
		}
	
		for(;i<arraysize;i++){
			a = Predicted_track_59_sub_label[i];
		}

		(void)a;
	}
}

void write_Predicted_track_59_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(Predicted_track_59_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			Predicted_track_59_sub_label[i]   = 0x800A;
			Predicted_track_59_sub_label[i+1] = 0xAFFE;
			Predicted_track_59_sub_label[i+2] = 0xAFFE;
			Predicted_track_59_sub_label[i+3] = 0xAFFE;
			Predicted_track_59_sub_label[i+4] = 0xAFFE;
			Predicted_track_59_sub_label[i+5] = 0xAFFE;
			Predicted_track_59_sub_label[i+6] = 0xAFFE;
			Predicted_track_59_sub_label[i+7] = 0xAFFE;
			Predicted_track_59_sub_label[i+8] = 0xAFFE;
			Predicted_track_59_sub_label[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				Predicted_track_59_sub_label[i]=0xAFFE;
		}
	}
}

int Predicted_track_58_sub_label[0];

static bool isIinitialized_Predicted_track_58_sub_label = false;
void initialize_Predicted_track_58_sub_label() {
	if (!isIinitialized_Predicted_track_58_sub_label){
		int i;
		for (i=0; i < 0; i++){
			Predicted_track_58_sub_label[i] = i+1;
		}
		isIinitialized_Predicted_track_58_sub_label = true;
	}
}

void read_Predicted_track_58_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(Predicted_track_58_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = Predicted_track_58_sub_label[i];
			a = Predicted_track_58_sub_label[i+1];
			a = Predicted_track_58_sub_label[i+2];
			a = Predicted_track_58_sub_label[i+3];
			a = Predicted_track_58_sub_label[i+4];
			a = Predicted_track_58_sub_label[i+5];
			a = Predicted_track_58_sub_label[i+6];
			a = Predicted_track_58_sub_label[i+7];
			a = Predicted_track_58_sub_label[i+8];
			a = Predicted_track_58_sub_label[i+9];
		}
	
		for(;i<arraysize;i++){
			a = Predicted_track_58_sub_label[i];
		}

		(void)a;
	}
}

void write_Predicted_track_58_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(Predicted_track_58_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			Predicted_track_58_sub_label[i]   = 0x800A;
			Predicted_track_58_sub_label[i+1] = 0xAFFE;
			Predicted_track_58_sub_label[i+2] = 0xAFFE;
			Predicted_track_58_sub_label[i+3] = 0xAFFE;
			Predicted_track_58_sub_label[i+4] = 0xAFFE;
			Predicted_track_58_sub_label[i+5] = 0xAFFE;
			Predicted_track_58_sub_label[i+6] = 0xAFFE;
			Predicted_track_58_sub_label[i+7] = 0xAFFE;
			Predicted_track_58_sub_label[i+8] = 0xAFFE;
			Predicted_track_58_sub_label[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				Predicted_track_58_sub_label[i]=0xAFFE;
		}
	}
}

int Predicted_track_57_sub_label[0];

static bool isIinitialized_Predicted_track_57_sub_label = false;
void initialize_Predicted_track_57_sub_label() {
	if (!isIinitialized_Predicted_track_57_sub_label){
		int i;
		for (i=0; i < 0; i++){
			Predicted_track_57_sub_label[i] = i+1;
		}
		isIinitialized_Predicted_track_57_sub_label = true;
	}
}

void read_Predicted_track_57_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(Predicted_track_57_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = Predicted_track_57_sub_label[i];
			a = Predicted_track_57_sub_label[i+1];
			a = Predicted_track_57_sub_label[i+2];
			a = Predicted_track_57_sub_label[i+3];
			a = Predicted_track_57_sub_label[i+4];
			a = Predicted_track_57_sub_label[i+5];
			a = Predicted_track_57_sub_label[i+6];
			a = Predicted_track_57_sub_label[i+7];
			a = Predicted_track_57_sub_label[i+8];
			a = Predicted_track_57_sub_label[i+9];
		}
	
		for(;i<arraysize;i++){
			a = Predicted_track_57_sub_label[i];
		}

		(void)a;
	}
}

void write_Predicted_track_57_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(Predicted_track_57_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			Predicted_track_57_sub_label[i]   = 0x800A;
			Predicted_track_57_sub_label[i+1] = 0xAFFE;
			Predicted_track_57_sub_label[i+2] = 0xAFFE;
			Predicted_track_57_sub_label[i+3] = 0xAFFE;
			Predicted_track_57_sub_label[i+4] = 0xAFFE;
			Predicted_track_57_sub_label[i+5] = 0xAFFE;
			Predicted_track_57_sub_label[i+6] = 0xAFFE;
			Predicted_track_57_sub_label[i+7] = 0xAFFE;
			Predicted_track_57_sub_label[i+8] = 0xAFFE;
			Predicted_track_57_sub_label[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				Predicted_track_57_sub_label[i]=0xAFFE;
		}
	}
}

int Predicted_track_56_sub_label[0];

static bool isIinitialized_Predicted_track_56_sub_label = false;
void initialize_Predicted_track_56_sub_label() {
	if (!isIinitialized_Predicted_track_56_sub_label){
		int i;
		for (i=0; i < 0; i++){
			Predicted_track_56_sub_label[i] = i+1;
		}
		isIinitialized_Predicted_track_56_sub_label = true;
	}
}

void read_Predicted_track_56_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(Predicted_track_56_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = Predicted_track_56_sub_label[i];
			a = Predicted_track_56_sub_label[i+1];
			a = Predicted_track_56_sub_label[i+2];
			a = Predicted_track_56_sub_label[i+3];
			a = Predicted_track_56_sub_label[i+4];
			a = Predicted_track_56_sub_label[i+5];
			a = Predicted_track_56_sub_label[i+6];
			a = Predicted_track_56_sub_label[i+7];
			a = Predicted_track_56_sub_label[i+8];
			a = Predicted_track_56_sub_label[i+9];
		}
	
		for(;i<arraysize;i++){
			a = Predicted_track_56_sub_label[i];
		}

		(void)a;
	}
}

void write_Predicted_track_56_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(Predicted_track_56_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			Predicted_track_56_sub_label[i]   = 0x800A;
			Predicted_track_56_sub_label[i+1] = 0xAFFE;
			Predicted_track_56_sub_label[i+2] = 0xAFFE;
			Predicted_track_56_sub_label[i+3] = 0xAFFE;
			Predicted_track_56_sub_label[i+4] = 0xAFFE;
			Predicted_track_56_sub_label[i+5] = 0xAFFE;
			Predicted_track_56_sub_label[i+6] = 0xAFFE;
			Predicted_track_56_sub_label[i+7] = 0xAFFE;
			Predicted_track_56_sub_label[i+8] = 0xAFFE;
			Predicted_track_56_sub_label[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				Predicted_track_56_sub_label[i]=0xAFFE;
		}
	}
}

int Predicted_track_55_sub_label[0];

static bool isIinitialized_Predicted_track_55_sub_label = false;
void initialize_Predicted_track_55_sub_label() {
	if (!isIinitialized_Predicted_track_55_sub_label){
		int i;
		for (i=0; i < 0; i++){
			Predicted_track_55_sub_label[i] = i+1;
		}
		isIinitialized_Predicted_track_55_sub_label = true;
	}
}

void read_Predicted_track_55_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(Predicted_track_55_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = Predicted_track_55_sub_label[i];
			a = Predicted_track_55_sub_label[i+1];
			a = Predicted_track_55_sub_label[i+2];
			a = Predicted_track_55_sub_label[i+3];
			a = Predicted_track_55_sub_label[i+4];
			a = Predicted_track_55_sub_label[i+5];
			a = Predicted_track_55_sub_label[i+6];
			a = Predicted_track_55_sub_label[i+7];
			a = Predicted_track_55_sub_label[i+8];
			a = Predicted_track_55_sub_label[i+9];
		}
	
		for(;i<arraysize;i++){
			a = Predicted_track_55_sub_label[i];
		}

		(void)a;
	}
}

void write_Predicted_track_55_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(Predicted_track_55_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			Predicted_track_55_sub_label[i]   = 0x800A;
			Predicted_track_55_sub_label[i+1] = 0xAFFE;
			Predicted_track_55_sub_label[i+2] = 0xAFFE;
			Predicted_track_55_sub_label[i+3] = 0xAFFE;
			Predicted_track_55_sub_label[i+4] = 0xAFFE;
			Predicted_track_55_sub_label[i+5] = 0xAFFE;
			Predicted_track_55_sub_label[i+6] = 0xAFFE;
			Predicted_track_55_sub_label[i+7] = 0xAFFE;
			Predicted_track_55_sub_label[i+8] = 0xAFFE;
			Predicted_track_55_sub_label[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				Predicted_track_55_sub_label[i]=0xAFFE;
		}
	}
}

int Predicted_track_54_sub_label[0];

static bool isIinitialized_Predicted_track_54_sub_label = false;
void initialize_Predicted_track_54_sub_label() {
	if (!isIinitialized_Predicted_track_54_sub_label){
		int i;
		for (i=0; i < 0; i++){
			Predicted_track_54_sub_label[i] = i+1;
		}
		isIinitialized_Predicted_track_54_sub_label = true;
	}
}

void read_Predicted_track_54_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(Predicted_track_54_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = Predicted_track_54_sub_label[i];
			a = Predicted_track_54_sub_label[i+1];
			a = Predicted_track_54_sub_label[i+2];
			a = Predicted_track_54_sub_label[i+3];
			a = Predicted_track_54_sub_label[i+4];
			a = Predicted_track_54_sub_label[i+5];
			a = Predicted_track_54_sub_label[i+6];
			a = Predicted_track_54_sub_label[i+7];
			a = Predicted_track_54_sub_label[i+8];
			a = Predicted_track_54_sub_label[i+9];
		}
	
		for(;i<arraysize;i++){
			a = Predicted_track_54_sub_label[i];
		}

		(void)a;
	}
}

void write_Predicted_track_54_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(Predicted_track_54_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			Predicted_track_54_sub_label[i]   = 0x800A;
			Predicted_track_54_sub_label[i+1] = 0xAFFE;
			Predicted_track_54_sub_label[i+2] = 0xAFFE;
			Predicted_track_54_sub_label[i+3] = 0xAFFE;
			Predicted_track_54_sub_label[i+4] = 0xAFFE;
			Predicted_track_54_sub_label[i+5] = 0xAFFE;
			Predicted_track_54_sub_label[i+6] = 0xAFFE;
			Predicted_track_54_sub_label[i+7] = 0xAFFE;
			Predicted_track_54_sub_label[i+8] = 0xAFFE;
			Predicted_track_54_sub_label[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				Predicted_track_54_sub_label[i]=0xAFFE;
		}
	}
}

int Predicted_track_53_sub_label[0];

static bool isIinitialized_Predicted_track_53_sub_label = false;
void initialize_Predicted_track_53_sub_label() {
	if (!isIinitialized_Predicted_track_53_sub_label){
		int i;
		for (i=0; i < 0; i++){
			Predicted_track_53_sub_label[i] = i+1;
		}
		isIinitialized_Predicted_track_53_sub_label = true;
	}
}

void read_Predicted_track_53_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(Predicted_track_53_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = Predicted_track_53_sub_label[i];
			a = Predicted_track_53_sub_label[i+1];
			a = Predicted_track_53_sub_label[i+2];
			a = Predicted_track_53_sub_label[i+3];
			a = Predicted_track_53_sub_label[i+4];
			a = Predicted_track_53_sub_label[i+5];
			a = Predicted_track_53_sub_label[i+6];
			a = Predicted_track_53_sub_label[i+7];
			a = Predicted_track_53_sub_label[i+8];
			a = Predicted_track_53_sub_label[i+9];
		}
	
		for(;i<arraysize;i++){
			a = Predicted_track_53_sub_label[i];
		}

		(void)a;
	}
}

void write_Predicted_track_53_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(Predicted_track_53_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			Predicted_track_53_sub_label[i]   = 0x800A;
			Predicted_track_53_sub_label[i+1] = 0xAFFE;
			Predicted_track_53_sub_label[i+2] = 0xAFFE;
			Predicted_track_53_sub_label[i+3] = 0xAFFE;
			Predicted_track_53_sub_label[i+4] = 0xAFFE;
			Predicted_track_53_sub_label[i+5] = 0xAFFE;
			Predicted_track_53_sub_label[i+6] = 0xAFFE;
			Predicted_track_53_sub_label[i+7] = 0xAFFE;
			Predicted_track_53_sub_label[i+8] = 0xAFFE;
			Predicted_track_53_sub_label[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				Predicted_track_53_sub_label[i]=0xAFFE;
		}
	}
}

int Predicted_track_50_sub_label[0];

static bool isIinitialized_Predicted_track_50_sub_label = false;
void initialize_Predicted_track_50_sub_label() {
	if (!isIinitialized_Predicted_track_50_sub_label){
		int i;
		for (i=0; i < 0; i++){
			Predicted_track_50_sub_label[i] = i+1;
		}
		isIinitialized_Predicted_track_50_sub_label = true;
	}
}

void read_Predicted_track_50_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(Predicted_track_50_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = Predicted_track_50_sub_label[i];
			a = Predicted_track_50_sub_label[i+1];
			a = Predicted_track_50_sub_label[i+2];
			a = Predicted_track_50_sub_label[i+3];
			a = Predicted_track_50_sub_label[i+4];
			a = Predicted_track_50_sub_label[i+5];
			a = Predicted_track_50_sub_label[i+6];
			a = Predicted_track_50_sub_label[i+7];
			a = Predicted_track_50_sub_label[i+8];
			a = Predicted_track_50_sub_label[i+9];
		}
	
		for(;i<arraysize;i++){
			a = Predicted_track_50_sub_label[i];
		}

		(void)a;
	}
}

void write_Predicted_track_50_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(Predicted_track_50_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			Predicted_track_50_sub_label[i]   = 0x800A;
			Predicted_track_50_sub_label[i+1] = 0xAFFE;
			Predicted_track_50_sub_label[i+2] = 0xAFFE;
			Predicted_track_50_sub_label[i+3] = 0xAFFE;
			Predicted_track_50_sub_label[i+4] = 0xAFFE;
			Predicted_track_50_sub_label[i+5] = 0xAFFE;
			Predicted_track_50_sub_label[i+6] = 0xAFFE;
			Predicted_track_50_sub_label[i+7] = 0xAFFE;
			Predicted_track_50_sub_label[i+8] = 0xAFFE;
			Predicted_track_50_sub_label[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				Predicted_track_50_sub_label[i]=0xAFFE;
		}
	}
}

int Predicted_track_52_sub_label[0];

static bool isIinitialized_Predicted_track_52_sub_label = false;
void initialize_Predicted_track_52_sub_label() {
	if (!isIinitialized_Predicted_track_52_sub_label){
		int i;
		for (i=0; i < 0; i++){
			Predicted_track_52_sub_label[i] = i+1;
		}
		isIinitialized_Predicted_track_52_sub_label = true;
	}
}

void read_Predicted_track_52_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(Predicted_track_52_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = Predicted_track_52_sub_label[i];
			a = Predicted_track_52_sub_label[i+1];
			a = Predicted_track_52_sub_label[i+2];
			a = Predicted_track_52_sub_label[i+3];
			a = Predicted_track_52_sub_label[i+4];
			a = Predicted_track_52_sub_label[i+5];
			a = Predicted_track_52_sub_label[i+6];
			a = Predicted_track_52_sub_label[i+7];
			a = Predicted_track_52_sub_label[i+8];
			a = Predicted_track_52_sub_label[i+9];
		}
	
		for(;i<arraysize;i++){
			a = Predicted_track_52_sub_label[i];
		}

		(void)a;
	}
}

void write_Predicted_track_52_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(Predicted_track_52_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			Predicted_track_52_sub_label[i]   = 0x800A;
			Predicted_track_52_sub_label[i+1] = 0xAFFE;
			Predicted_track_52_sub_label[i+2] = 0xAFFE;
			Predicted_track_52_sub_label[i+3] = 0xAFFE;
			Predicted_track_52_sub_label[i+4] = 0xAFFE;
			Predicted_track_52_sub_label[i+5] = 0xAFFE;
			Predicted_track_52_sub_label[i+6] = 0xAFFE;
			Predicted_track_52_sub_label[i+7] = 0xAFFE;
			Predicted_track_52_sub_label[i+8] = 0xAFFE;
			Predicted_track_52_sub_label[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				Predicted_track_52_sub_label[i]=0xAFFE;
		}
	}
}

int Predicted_track_49_sub_label[0];

static bool isIinitialized_Predicted_track_49_sub_label = false;
void initialize_Predicted_track_49_sub_label() {
	if (!isIinitialized_Predicted_track_49_sub_label){
		int i;
		for (i=0; i < 0; i++){
			Predicted_track_49_sub_label[i] = i+1;
		}
		isIinitialized_Predicted_track_49_sub_label = true;
	}
}

void read_Predicted_track_49_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(Predicted_track_49_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = Predicted_track_49_sub_label[i];
			a = Predicted_track_49_sub_label[i+1];
			a = Predicted_track_49_sub_label[i+2];
			a = Predicted_track_49_sub_label[i+3];
			a = Predicted_track_49_sub_label[i+4];
			a = Predicted_track_49_sub_label[i+5];
			a = Predicted_track_49_sub_label[i+6];
			a = Predicted_track_49_sub_label[i+7];
			a = Predicted_track_49_sub_label[i+8];
			a = Predicted_track_49_sub_label[i+9];
		}
	
		for(;i<arraysize;i++){
			a = Predicted_track_49_sub_label[i];
		}

		(void)a;
	}
}

void write_Predicted_track_49_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(Predicted_track_49_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			Predicted_track_49_sub_label[i]   = 0x800A;
			Predicted_track_49_sub_label[i+1] = 0xAFFE;
			Predicted_track_49_sub_label[i+2] = 0xAFFE;
			Predicted_track_49_sub_label[i+3] = 0xAFFE;
			Predicted_track_49_sub_label[i+4] = 0xAFFE;
			Predicted_track_49_sub_label[i+5] = 0xAFFE;
			Predicted_track_49_sub_label[i+6] = 0xAFFE;
			Predicted_track_49_sub_label[i+7] = 0xAFFE;
			Predicted_track_49_sub_label[i+8] = 0xAFFE;
			Predicted_track_49_sub_label[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				Predicted_track_49_sub_label[i]=0xAFFE;
		}
	}
}

int Predicted_track_51_sub_label[0];

static bool isIinitialized_Predicted_track_51_sub_label = false;
void initialize_Predicted_track_51_sub_label() {
	if (!isIinitialized_Predicted_track_51_sub_label){
		int i;
		for (i=0; i < 0; i++){
			Predicted_track_51_sub_label[i] = i+1;
		}
		isIinitialized_Predicted_track_51_sub_label = true;
	}
}

void read_Predicted_track_51_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(Predicted_track_51_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = Predicted_track_51_sub_label[i];
			a = Predicted_track_51_sub_label[i+1];
			a = Predicted_track_51_sub_label[i+2];
			a = Predicted_track_51_sub_label[i+3];
			a = Predicted_track_51_sub_label[i+4];
			a = Predicted_track_51_sub_label[i+5];
			a = Predicted_track_51_sub_label[i+6];
			a = Predicted_track_51_sub_label[i+7];
			a = Predicted_track_51_sub_label[i+8];
			a = Predicted_track_51_sub_label[i+9];
		}
	
		for(;i<arraysize;i++){
			a = Predicted_track_51_sub_label[i];
		}

		(void)a;
	}
}

void write_Predicted_track_51_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(Predicted_track_51_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			Predicted_track_51_sub_label[i]   = 0x800A;
			Predicted_track_51_sub_label[i+1] = 0xAFFE;
			Predicted_track_51_sub_label[i+2] = 0xAFFE;
			Predicted_track_51_sub_label[i+3] = 0xAFFE;
			Predicted_track_51_sub_label[i+4] = 0xAFFE;
			Predicted_track_51_sub_label[i+5] = 0xAFFE;
			Predicted_track_51_sub_label[i+6] = 0xAFFE;
			Predicted_track_51_sub_label[i+7] = 0xAFFE;
			Predicted_track_51_sub_label[i+8] = 0xAFFE;
			Predicted_track_51_sub_label[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				Predicted_track_51_sub_label[i]=0xAFFE;
		}
	}
}

int Predicted_track_33_sub_label[0];

static bool isIinitialized_Predicted_track_33_sub_label = false;
void initialize_Predicted_track_33_sub_label() {
	if (!isIinitialized_Predicted_track_33_sub_label){
		int i;
		for (i=0; i < 0; i++){
			Predicted_track_33_sub_label[i] = i+1;
		}
		isIinitialized_Predicted_track_33_sub_label = true;
	}
}

void read_Predicted_track_33_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(Predicted_track_33_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = Predicted_track_33_sub_label[i];
			a = Predicted_track_33_sub_label[i+1];
			a = Predicted_track_33_sub_label[i+2];
			a = Predicted_track_33_sub_label[i+3];
			a = Predicted_track_33_sub_label[i+4];
			a = Predicted_track_33_sub_label[i+5];
			a = Predicted_track_33_sub_label[i+6];
			a = Predicted_track_33_sub_label[i+7];
			a = Predicted_track_33_sub_label[i+8];
			a = Predicted_track_33_sub_label[i+9];
		}
	
		for(;i<arraysize;i++){
			a = Predicted_track_33_sub_label[i];
		}

		(void)a;
	}
}

void write_Predicted_track_33_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(Predicted_track_33_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			Predicted_track_33_sub_label[i]   = 0x800A;
			Predicted_track_33_sub_label[i+1] = 0xAFFE;
			Predicted_track_33_sub_label[i+2] = 0xAFFE;
			Predicted_track_33_sub_label[i+3] = 0xAFFE;
			Predicted_track_33_sub_label[i+4] = 0xAFFE;
			Predicted_track_33_sub_label[i+5] = 0xAFFE;
			Predicted_track_33_sub_label[i+6] = 0xAFFE;
			Predicted_track_33_sub_label[i+7] = 0xAFFE;
			Predicted_track_33_sub_label[i+8] = 0xAFFE;
			Predicted_track_33_sub_label[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				Predicted_track_33_sub_label[i]=0xAFFE;
		}
	}
}

int Predicted_track_47_sub_label[0];

static bool isIinitialized_Predicted_track_47_sub_label = false;
void initialize_Predicted_track_47_sub_label() {
	if (!isIinitialized_Predicted_track_47_sub_label){
		int i;
		for (i=0; i < 0; i++){
			Predicted_track_47_sub_label[i] = i+1;
		}
		isIinitialized_Predicted_track_47_sub_label = true;
	}
}

void read_Predicted_track_47_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(Predicted_track_47_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = Predicted_track_47_sub_label[i];
			a = Predicted_track_47_sub_label[i+1];
			a = Predicted_track_47_sub_label[i+2];
			a = Predicted_track_47_sub_label[i+3];
			a = Predicted_track_47_sub_label[i+4];
			a = Predicted_track_47_sub_label[i+5];
			a = Predicted_track_47_sub_label[i+6];
			a = Predicted_track_47_sub_label[i+7];
			a = Predicted_track_47_sub_label[i+8];
			a = Predicted_track_47_sub_label[i+9];
		}
	
		for(;i<arraysize;i++){
			a = Predicted_track_47_sub_label[i];
		}

		(void)a;
	}
}

void write_Predicted_track_47_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(Predicted_track_47_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			Predicted_track_47_sub_label[i]   = 0x800A;
			Predicted_track_47_sub_label[i+1] = 0xAFFE;
			Predicted_track_47_sub_label[i+2] = 0xAFFE;
			Predicted_track_47_sub_label[i+3] = 0xAFFE;
			Predicted_track_47_sub_label[i+4] = 0xAFFE;
			Predicted_track_47_sub_label[i+5] = 0xAFFE;
			Predicted_track_47_sub_label[i+6] = 0xAFFE;
			Predicted_track_47_sub_label[i+7] = 0xAFFE;
			Predicted_track_47_sub_label[i+8] = 0xAFFE;
			Predicted_track_47_sub_label[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				Predicted_track_47_sub_label[i]=0xAFFE;
		}
	}
}

int Predicted_track_46_sub_label[0];

static bool isIinitialized_Predicted_track_46_sub_label = false;
void initialize_Predicted_track_46_sub_label() {
	if (!isIinitialized_Predicted_track_46_sub_label){
		int i;
		for (i=0; i < 0; i++){
			Predicted_track_46_sub_label[i] = i+1;
		}
		isIinitialized_Predicted_track_46_sub_label = true;
	}
}

void read_Predicted_track_46_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(Predicted_track_46_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = Predicted_track_46_sub_label[i];
			a = Predicted_track_46_sub_label[i+1];
			a = Predicted_track_46_sub_label[i+2];
			a = Predicted_track_46_sub_label[i+3];
			a = Predicted_track_46_sub_label[i+4];
			a = Predicted_track_46_sub_label[i+5];
			a = Predicted_track_46_sub_label[i+6];
			a = Predicted_track_46_sub_label[i+7];
			a = Predicted_track_46_sub_label[i+8];
			a = Predicted_track_46_sub_label[i+9];
		}
	
		for(;i<arraysize;i++){
			a = Predicted_track_46_sub_label[i];
		}

		(void)a;
	}
}

void write_Predicted_track_46_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(Predicted_track_46_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			Predicted_track_46_sub_label[i]   = 0x800A;
			Predicted_track_46_sub_label[i+1] = 0xAFFE;
			Predicted_track_46_sub_label[i+2] = 0xAFFE;
			Predicted_track_46_sub_label[i+3] = 0xAFFE;
			Predicted_track_46_sub_label[i+4] = 0xAFFE;
			Predicted_track_46_sub_label[i+5] = 0xAFFE;
			Predicted_track_46_sub_label[i+6] = 0xAFFE;
			Predicted_track_46_sub_label[i+7] = 0xAFFE;
			Predicted_track_46_sub_label[i+8] = 0xAFFE;
			Predicted_track_46_sub_label[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				Predicted_track_46_sub_label[i]=0xAFFE;
		}
	}
}

int Predicted_track_45_sub_label[0];

static bool isIinitialized_Predicted_track_45_sub_label = false;
void initialize_Predicted_track_45_sub_label() {
	if (!isIinitialized_Predicted_track_45_sub_label){
		int i;
		for (i=0; i < 0; i++){
			Predicted_track_45_sub_label[i] = i+1;
		}
		isIinitialized_Predicted_track_45_sub_label = true;
	}
}

void read_Predicted_track_45_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(Predicted_track_45_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = Predicted_track_45_sub_label[i];
			a = Predicted_track_45_sub_label[i+1];
			a = Predicted_track_45_sub_label[i+2];
			a = Predicted_track_45_sub_label[i+3];
			a = Predicted_track_45_sub_label[i+4];
			a = Predicted_track_45_sub_label[i+5];
			a = Predicted_track_45_sub_label[i+6];
			a = Predicted_track_45_sub_label[i+7];
			a = Predicted_track_45_sub_label[i+8];
			a = Predicted_track_45_sub_label[i+9];
		}
	
		for(;i<arraysize;i++){
			a = Predicted_track_45_sub_label[i];
		}

		(void)a;
	}
}

void write_Predicted_track_45_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(Predicted_track_45_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			Predicted_track_45_sub_label[i]   = 0x800A;
			Predicted_track_45_sub_label[i+1] = 0xAFFE;
			Predicted_track_45_sub_label[i+2] = 0xAFFE;
			Predicted_track_45_sub_label[i+3] = 0xAFFE;
			Predicted_track_45_sub_label[i+4] = 0xAFFE;
			Predicted_track_45_sub_label[i+5] = 0xAFFE;
			Predicted_track_45_sub_label[i+6] = 0xAFFE;
			Predicted_track_45_sub_label[i+7] = 0xAFFE;
			Predicted_track_45_sub_label[i+8] = 0xAFFE;
			Predicted_track_45_sub_label[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				Predicted_track_45_sub_label[i]=0xAFFE;
		}
	}
}

int Predicted_track_44_sub_label[0];

static bool isIinitialized_Predicted_track_44_sub_label = false;
void initialize_Predicted_track_44_sub_label() {
	if (!isIinitialized_Predicted_track_44_sub_label){
		int i;
		for (i=0; i < 0; i++){
			Predicted_track_44_sub_label[i] = i+1;
		}
		isIinitialized_Predicted_track_44_sub_label = true;
	}
}

void read_Predicted_track_44_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(Predicted_track_44_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = Predicted_track_44_sub_label[i];
			a = Predicted_track_44_sub_label[i+1];
			a = Predicted_track_44_sub_label[i+2];
			a = Predicted_track_44_sub_label[i+3];
			a = Predicted_track_44_sub_label[i+4];
			a = Predicted_track_44_sub_label[i+5];
			a = Predicted_track_44_sub_label[i+6];
			a = Predicted_track_44_sub_label[i+7];
			a = Predicted_track_44_sub_label[i+8];
			a = Predicted_track_44_sub_label[i+9];
		}
	
		for(;i<arraysize;i++){
			a = Predicted_track_44_sub_label[i];
		}

		(void)a;
	}
}

void write_Predicted_track_44_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(Predicted_track_44_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			Predicted_track_44_sub_label[i]   = 0x800A;
			Predicted_track_44_sub_label[i+1] = 0xAFFE;
			Predicted_track_44_sub_label[i+2] = 0xAFFE;
			Predicted_track_44_sub_label[i+3] = 0xAFFE;
			Predicted_track_44_sub_label[i+4] = 0xAFFE;
			Predicted_track_44_sub_label[i+5] = 0xAFFE;
			Predicted_track_44_sub_label[i+6] = 0xAFFE;
			Predicted_track_44_sub_label[i+7] = 0xAFFE;
			Predicted_track_44_sub_label[i+8] = 0xAFFE;
			Predicted_track_44_sub_label[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				Predicted_track_44_sub_label[i]=0xAFFE;
		}
	}
}

int Predicted_track_43_sub_label[0];

static bool isIinitialized_Predicted_track_43_sub_label = false;
void initialize_Predicted_track_43_sub_label() {
	if (!isIinitialized_Predicted_track_43_sub_label){
		int i;
		for (i=0; i < 0; i++){
			Predicted_track_43_sub_label[i] = i+1;
		}
		isIinitialized_Predicted_track_43_sub_label = true;
	}
}

void read_Predicted_track_43_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(Predicted_track_43_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = Predicted_track_43_sub_label[i];
			a = Predicted_track_43_sub_label[i+1];
			a = Predicted_track_43_sub_label[i+2];
			a = Predicted_track_43_sub_label[i+3];
			a = Predicted_track_43_sub_label[i+4];
			a = Predicted_track_43_sub_label[i+5];
			a = Predicted_track_43_sub_label[i+6];
			a = Predicted_track_43_sub_label[i+7];
			a = Predicted_track_43_sub_label[i+8];
			a = Predicted_track_43_sub_label[i+9];
		}
	
		for(;i<arraysize;i++){
			a = Predicted_track_43_sub_label[i];
		}

		(void)a;
	}
}

void write_Predicted_track_43_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(Predicted_track_43_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			Predicted_track_43_sub_label[i]   = 0x800A;
			Predicted_track_43_sub_label[i+1] = 0xAFFE;
			Predicted_track_43_sub_label[i+2] = 0xAFFE;
			Predicted_track_43_sub_label[i+3] = 0xAFFE;
			Predicted_track_43_sub_label[i+4] = 0xAFFE;
			Predicted_track_43_sub_label[i+5] = 0xAFFE;
			Predicted_track_43_sub_label[i+6] = 0xAFFE;
			Predicted_track_43_sub_label[i+7] = 0xAFFE;
			Predicted_track_43_sub_label[i+8] = 0xAFFE;
			Predicted_track_43_sub_label[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				Predicted_track_43_sub_label[i]=0xAFFE;
		}
	}
}

int Predicted_track_42_sub_label[0];

static bool isIinitialized_Predicted_track_42_sub_label = false;
void initialize_Predicted_track_42_sub_label() {
	if (!isIinitialized_Predicted_track_42_sub_label){
		int i;
		for (i=0; i < 0; i++){
			Predicted_track_42_sub_label[i] = i+1;
		}
		isIinitialized_Predicted_track_42_sub_label = true;
	}
}

void read_Predicted_track_42_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(Predicted_track_42_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = Predicted_track_42_sub_label[i];
			a = Predicted_track_42_sub_label[i+1];
			a = Predicted_track_42_sub_label[i+2];
			a = Predicted_track_42_sub_label[i+3];
			a = Predicted_track_42_sub_label[i+4];
			a = Predicted_track_42_sub_label[i+5];
			a = Predicted_track_42_sub_label[i+6];
			a = Predicted_track_42_sub_label[i+7];
			a = Predicted_track_42_sub_label[i+8];
			a = Predicted_track_42_sub_label[i+9];
		}
	
		for(;i<arraysize;i++){
			a = Predicted_track_42_sub_label[i];
		}

		(void)a;
	}
}

void write_Predicted_track_42_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(Predicted_track_42_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			Predicted_track_42_sub_label[i]   = 0x800A;
			Predicted_track_42_sub_label[i+1] = 0xAFFE;
			Predicted_track_42_sub_label[i+2] = 0xAFFE;
			Predicted_track_42_sub_label[i+3] = 0xAFFE;
			Predicted_track_42_sub_label[i+4] = 0xAFFE;
			Predicted_track_42_sub_label[i+5] = 0xAFFE;
			Predicted_track_42_sub_label[i+6] = 0xAFFE;
			Predicted_track_42_sub_label[i+7] = 0xAFFE;
			Predicted_track_42_sub_label[i+8] = 0xAFFE;
			Predicted_track_42_sub_label[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				Predicted_track_42_sub_label[i]=0xAFFE;
		}
	}
}

int Predicted_track_41_sub_label[0];

static bool isIinitialized_Predicted_track_41_sub_label = false;
void initialize_Predicted_track_41_sub_label() {
	if (!isIinitialized_Predicted_track_41_sub_label){
		int i;
		for (i=0; i < 0; i++){
			Predicted_track_41_sub_label[i] = i+1;
		}
		isIinitialized_Predicted_track_41_sub_label = true;
	}
}

void read_Predicted_track_41_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(Predicted_track_41_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = Predicted_track_41_sub_label[i];
			a = Predicted_track_41_sub_label[i+1];
			a = Predicted_track_41_sub_label[i+2];
			a = Predicted_track_41_sub_label[i+3];
			a = Predicted_track_41_sub_label[i+4];
			a = Predicted_track_41_sub_label[i+5];
			a = Predicted_track_41_sub_label[i+6];
			a = Predicted_track_41_sub_label[i+7];
			a = Predicted_track_41_sub_label[i+8];
			a = Predicted_track_41_sub_label[i+9];
		}
	
		for(;i<arraysize;i++){
			a = Predicted_track_41_sub_label[i];
		}

		(void)a;
	}
}

void write_Predicted_track_41_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(Predicted_track_41_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			Predicted_track_41_sub_label[i]   = 0x800A;
			Predicted_track_41_sub_label[i+1] = 0xAFFE;
			Predicted_track_41_sub_label[i+2] = 0xAFFE;
			Predicted_track_41_sub_label[i+3] = 0xAFFE;
			Predicted_track_41_sub_label[i+4] = 0xAFFE;
			Predicted_track_41_sub_label[i+5] = 0xAFFE;
			Predicted_track_41_sub_label[i+6] = 0xAFFE;
			Predicted_track_41_sub_label[i+7] = 0xAFFE;
			Predicted_track_41_sub_label[i+8] = 0xAFFE;
			Predicted_track_41_sub_label[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				Predicted_track_41_sub_label[i]=0xAFFE;
		}
	}
}

int Predicted_track_40_sub_label[0];

static bool isIinitialized_Predicted_track_40_sub_label = false;
void initialize_Predicted_track_40_sub_label() {
	if (!isIinitialized_Predicted_track_40_sub_label){
		int i;
		for (i=0; i < 0; i++){
			Predicted_track_40_sub_label[i] = i+1;
		}
		isIinitialized_Predicted_track_40_sub_label = true;
	}
}

void read_Predicted_track_40_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(Predicted_track_40_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = Predicted_track_40_sub_label[i];
			a = Predicted_track_40_sub_label[i+1];
			a = Predicted_track_40_sub_label[i+2];
			a = Predicted_track_40_sub_label[i+3];
			a = Predicted_track_40_sub_label[i+4];
			a = Predicted_track_40_sub_label[i+5];
			a = Predicted_track_40_sub_label[i+6];
			a = Predicted_track_40_sub_label[i+7];
			a = Predicted_track_40_sub_label[i+8];
			a = Predicted_track_40_sub_label[i+9];
		}
	
		for(;i<arraysize;i++){
			a = Predicted_track_40_sub_label[i];
		}

		(void)a;
	}
}

void write_Predicted_track_40_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(Predicted_track_40_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			Predicted_track_40_sub_label[i]   = 0x800A;
			Predicted_track_40_sub_label[i+1] = 0xAFFE;
			Predicted_track_40_sub_label[i+2] = 0xAFFE;
			Predicted_track_40_sub_label[i+3] = 0xAFFE;
			Predicted_track_40_sub_label[i+4] = 0xAFFE;
			Predicted_track_40_sub_label[i+5] = 0xAFFE;
			Predicted_track_40_sub_label[i+6] = 0xAFFE;
			Predicted_track_40_sub_label[i+7] = 0xAFFE;
			Predicted_track_40_sub_label[i+8] = 0xAFFE;
			Predicted_track_40_sub_label[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				Predicted_track_40_sub_label[i]=0xAFFE;
		}
	}
}

int Predicted_track_36_sub_label[0];

static bool isIinitialized_Predicted_track_36_sub_label = false;
void initialize_Predicted_track_36_sub_label() {
	if (!isIinitialized_Predicted_track_36_sub_label){
		int i;
		for (i=0; i < 0; i++){
			Predicted_track_36_sub_label[i] = i+1;
		}
		isIinitialized_Predicted_track_36_sub_label = true;
	}
}

void read_Predicted_track_36_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(Predicted_track_36_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = Predicted_track_36_sub_label[i];
			a = Predicted_track_36_sub_label[i+1];
			a = Predicted_track_36_sub_label[i+2];
			a = Predicted_track_36_sub_label[i+3];
			a = Predicted_track_36_sub_label[i+4];
			a = Predicted_track_36_sub_label[i+5];
			a = Predicted_track_36_sub_label[i+6];
			a = Predicted_track_36_sub_label[i+7];
			a = Predicted_track_36_sub_label[i+8];
			a = Predicted_track_36_sub_label[i+9];
		}
	
		for(;i<arraysize;i++){
			a = Predicted_track_36_sub_label[i];
		}

		(void)a;
	}
}

void write_Predicted_track_36_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(Predicted_track_36_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			Predicted_track_36_sub_label[i]   = 0x800A;
			Predicted_track_36_sub_label[i+1] = 0xAFFE;
			Predicted_track_36_sub_label[i+2] = 0xAFFE;
			Predicted_track_36_sub_label[i+3] = 0xAFFE;
			Predicted_track_36_sub_label[i+4] = 0xAFFE;
			Predicted_track_36_sub_label[i+5] = 0xAFFE;
			Predicted_track_36_sub_label[i+6] = 0xAFFE;
			Predicted_track_36_sub_label[i+7] = 0xAFFE;
			Predicted_track_36_sub_label[i+8] = 0xAFFE;
			Predicted_track_36_sub_label[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				Predicted_track_36_sub_label[i]=0xAFFE;
		}
	}
}

int Predicted_track_38_sub_label[0];

static bool isIinitialized_Predicted_track_38_sub_label = false;
void initialize_Predicted_track_38_sub_label() {
	if (!isIinitialized_Predicted_track_38_sub_label){
		int i;
		for (i=0; i < 0; i++){
			Predicted_track_38_sub_label[i] = i+1;
		}
		isIinitialized_Predicted_track_38_sub_label = true;
	}
}

void read_Predicted_track_38_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(Predicted_track_38_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = Predicted_track_38_sub_label[i];
			a = Predicted_track_38_sub_label[i+1];
			a = Predicted_track_38_sub_label[i+2];
			a = Predicted_track_38_sub_label[i+3];
			a = Predicted_track_38_sub_label[i+4];
			a = Predicted_track_38_sub_label[i+5];
			a = Predicted_track_38_sub_label[i+6];
			a = Predicted_track_38_sub_label[i+7];
			a = Predicted_track_38_sub_label[i+8];
			a = Predicted_track_38_sub_label[i+9];
		}
	
		for(;i<arraysize;i++){
			a = Predicted_track_38_sub_label[i];
		}

		(void)a;
	}
}

void write_Predicted_track_38_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(Predicted_track_38_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			Predicted_track_38_sub_label[i]   = 0x800A;
			Predicted_track_38_sub_label[i+1] = 0xAFFE;
			Predicted_track_38_sub_label[i+2] = 0xAFFE;
			Predicted_track_38_sub_label[i+3] = 0xAFFE;
			Predicted_track_38_sub_label[i+4] = 0xAFFE;
			Predicted_track_38_sub_label[i+5] = 0xAFFE;
			Predicted_track_38_sub_label[i+6] = 0xAFFE;
			Predicted_track_38_sub_label[i+7] = 0xAFFE;
			Predicted_track_38_sub_label[i+8] = 0xAFFE;
			Predicted_track_38_sub_label[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				Predicted_track_38_sub_label[i]=0xAFFE;
		}
	}
}

int Predicted_track_35_sub_label[0];

static bool isIinitialized_Predicted_track_35_sub_label = false;
void initialize_Predicted_track_35_sub_label() {
	if (!isIinitialized_Predicted_track_35_sub_label){
		int i;
		for (i=0; i < 0; i++){
			Predicted_track_35_sub_label[i] = i+1;
		}
		isIinitialized_Predicted_track_35_sub_label = true;
	}
}

void read_Predicted_track_35_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(Predicted_track_35_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = Predicted_track_35_sub_label[i];
			a = Predicted_track_35_sub_label[i+1];
			a = Predicted_track_35_sub_label[i+2];
			a = Predicted_track_35_sub_label[i+3];
			a = Predicted_track_35_sub_label[i+4];
			a = Predicted_track_35_sub_label[i+5];
			a = Predicted_track_35_sub_label[i+6];
			a = Predicted_track_35_sub_label[i+7];
			a = Predicted_track_35_sub_label[i+8];
			a = Predicted_track_35_sub_label[i+9];
		}
	
		for(;i<arraysize;i++){
			a = Predicted_track_35_sub_label[i];
		}

		(void)a;
	}
}

void write_Predicted_track_35_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(Predicted_track_35_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			Predicted_track_35_sub_label[i]   = 0x800A;
			Predicted_track_35_sub_label[i+1] = 0xAFFE;
			Predicted_track_35_sub_label[i+2] = 0xAFFE;
			Predicted_track_35_sub_label[i+3] = 0xAFFE;
			Predicted_track_35_sub_label[i+4] = 0xAFFE;
			Predicted_track_35_sub_label[i+5] = 0xAFFE;
			Predicted_track_35_sub_label[i+6] = 0xAFFE;
			Predicted_track_35_sub_label[i+7] = 0xAFFE;
			Predicted_track_35_sub_label[i+8] = 0xAFFE;
			Predicted_track_35_sub_label[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				Predicted_track_35_sub_label[i]=0xAFFE;
		}
	}
}

int Predicted_track_34_sub_label[0];

static bool isIinitialized_Predicted_track_34_sub_label = false;
void initialize_Predicted_track_34_sub_label() {
	if (!isIinitialized_Predicted_track_34_sub_label){
		int i;
		for (i=0; i < 0; i++){
			Predicted_track_34_sub_label[i] = i+1;
		}
		isIinitialized_Predicted_track_34_sub_label = true;
	}
}

void read_Predicted_track_34_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(Predicted_track_34_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = Predicted_track_34_sub_label[i];
			a = Predicted_track_34_sub_label[i+1];
			a = Predicted_track_34_sub_label[i+2];
			a = Predicted_track_34_sub_label[i+3];
			a = Predicted_track_34_sub_label[i+4];
			a = Predicted_track_34_sub_label[i+5];
			a = Predicted_track_34_sub_label[i+6];
			a = Predicted_track_34_sub_label[i+7];
			a = Predicted_track_34_sub_label[i+8];
			a = Predicted_track_34_sub_label[i+9];
		}
	
		for(;i<arraysize;i++){
			a = Predicted_track_34_sub_label[i];
		}

		(void)a;
	}
}

void write_Predicted_track_34_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(Predicted_track_34_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			Predicted_track_34_sub_label[i]   = 0x800A;
			Predicted_track_34_sub_label[i+1] = 0xAFFE;
			Predicted_track_34_sub_label[i+2] = 0xAFFE;
			Predicted_track_34_sub_label[i+3] = 0xAFFE;
			Predicted_track_34_sub_label[i+4] = 0xAFFE;
			Predicted_track_34_sub_label[i+5] = 0xAFFE;
			Predicted_track_34_sub_label[i+6] = 0xAFFE;
			Predicted_track_34_sub_label[i+7] = 0xAFFE;
			Predicted_track_34_sub_label[i+8] = 0xAFFE;
			Predicted_track_34_sub_label[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				Predicted_track_34_sub_label[i]=0xAFFE;
		}
	}
}

int Predicted_track_39_sub_label[0];

static bool isIinitialized_Predicted_track_39_sub_label = false;
void initialize_Predicted_track_39_sub_label() {
	if (!isIinitialized_Predicted_track_39_sub_label){
		int i;
		for (i=0; i < 0; i++){
			Predicted_track_39_sub_label[i] = i+1;
		}
		isIinitialized_Predicted_track_39_sub_label = true;
	}
}

void read_Predicted_track_39_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(Predicted_track_39_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = Predicted_track_39_sub_label[i];
			a = Predicted_track_39_sub_label[i+1];
			a = Predicted_track_39_sub_label[i+2];
			a = Predicted_track_39_sub_label[i+3];
			a = Predicted_track_39_sub_label[i+4];
			a = Predicted_track_39_sub_label[i+5];
			a = Predicted_track_39_sub_label[i+6];
			a = Predicted_track_39_sub_label[i+7];
			a = Predicted_track_39_sub_label[i+8];
			a = Predicted_track_39_sub_label[i+9];
		}
	
		for(;i<arraysize;i++){
			a = Predicted_track_39_sub_label[i];
		}

		(void)a;
	}
}

void write_Predicted_track_39_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(Predicted_track_39_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			Predicted_track_39_sub_label[i]   = 0x800A;
			Predicted_track_39_sub_label[i+1] = 0xAFFE;
			Predicted_track_39_sub_label[i+2] = 0xAFFE;
			Predicted_track_39_sub_label[i+3] = 0xAFFE;
			Predicted_track_39_sub_label[i+4] = 0xAFFE;
			Predicted_track_39_sub_label[i+5] = 0xAFFE;
			Predicted_track_39_sub_label[i+6] = 0xAFFE;
			Predicted_track_39_sub_label[i+7] = 0xAFFE;
			Predicted_track_39_sub_label[i+8] = 0xAFFE;
			Predicted_track_39_sub_label[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				Predicted_track_39_sub_label[i]=0xAFFE;
		}
	}
}

int Predicted_track_37_sub_label[0];

static bool isIinitialized_Predicted_track_37_sub_label = false;
void initialize_Predicted_track_37_sub_label() {
	if (!isIinitialized_Predicted_track_37_sub_label){
		int i;
		for (i=0; i < 0; i++){
			Predicted_track_37_sub_label[i] = i+1;
		}
		isIinitialized_Predicted_track_37_sub_label = true;
	}
}

void read_Predicted_track_37_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(Predicted_track_37_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = Predicted_track_37_sub_label[i];
			a = Predicted_track_37_sub_label[i+1];
			a = Predicted_track_37_sub_label[i+2];
			a = Predicted_track_37_sub_label[i+3];
			a = Predicted_track_37_sub_label[i+4];
			a = Predicted_track_37_sub_label[i+5];
			a = Predicted_track_37_sub_label[i+6];
			a = Predicted_track_37_sub_label[i+7];
			a = Predicted_track_37_sub_label[i+8];
			a = Predicted_track_37_sub_label[i+9];
		}
	
		for(;i<arraysize;i++){
			a = Predicted_track_37_sub_label[i];
		}

		(void)a;
	}
}

void write_Predicted_track_37_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(Predicted_track_37_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			Predicted_track_37_sub_label[i]   = 0x800A;
			Predicted_track_37_sub_label[i+1] = 0xAFFE;
			Predicted_track_37_sub_label[i+2] = 0xAFFE;
			Predicted_track_37_sub_label[i+3] = 0xAFFE;
			Predicted_track_37_sub_label[i+4] = 0xAFFE;
			Predicted_track_37_sub_label[i+5] = 0xAFFE;
			Predicted_track_37_sub_label[i+6] = 0xAFFE;
			Predicted_track_37_sub_label[i+7] = 0xAFFE;
			Predicted_track_37_sub_label[i+8] = 0xAFFE;
			Predicted_track_37_sub_label[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				Predicted_track_37_sub_label[i]=0xAFFE;
		}
	}
}

int Predicted_track_32_sub_label[0];

static bool isIinitialized_Predicted_track_32_sub_label = false;
void initialize_Predicted_track_32_sub_label() {
	if (!isIinitialized_Predicted_track_32_sub_label){
		int i;
		for (i=0; i < 0; i++){
			Predicted_track_32_sub_label[i] = i+1;
		}
		isIinitialized_Predicted_track_32_sub_label = true;
	}
}

void read_Predicted_track_32_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(Predicted_track_32_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = Predicted_track_32_sub_label[i];
			a = Predicted_track_32_sub_label[i+1];
			a = Predicted_track_32_sub_label[i+2];
			a = Predicted_track_32_sub_label[i+3];
			a = Predicted_track_32_sub_label[i+4];
			a = Predicted_track_32_sub_label[i+5];
			a = Predicted_track_32_sub_label[i+6];
			a = Predicted_track_32_sub_label[i+7];
			a = Predicted_track_32_sub_label[i+8];
			a = Predicted_track_32_sub_label[i+9];
		}
	
		for(;i<arraysize;i++){
			a = Predicted_track_32_sub_label[i];
		}

		(void)a;
	}
}

void write_Predicted_track_32_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(Predicted_track_32_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			Predicted_track_32_sub_label[i]   = 0x800A;
			Predicted_track_32_sub_label[i+1] = 0xAFFE;
			Predicted_track_32_sub_label[i+2] = 0xAFFE;
			Predicted_track_32_sub_label[i+3] = 0xAFFE;
			Predicted_track_32_sub_label[i+4] = 0xAFFE;
			Predicted_track_32_sub_label[i+5] = 0xAFFE;
			Predicted_track_32_sub_label[i+6] = 0xAFFE;
			Predicted_track_32_sub_label[i+7] = 0xAFFE;
			Predicted_track_32_sub_label[i+8] = 0xAFFE;
			Predicted_track_32_sub_label[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				Predicted_track_32_sub_label[i]=0xAFFE;
		}
	}
}

int Predicted_track_17_sub_label[0];

static bool isIinitialized_Predicted_track_17_sub_label = false;
void initialize_Predicted_track_17_sub_label() {
	if (!isIinitialized_Predicted_track_17_sub_label){
		int i;
		for (i=0; i < 0; i++){
			Predicted_track_17_sub_label[i] = i+1;
		}
		isIinitialized_Predicted_track_17_sub_label = true;
	}
}

void read_Predicted_track_17_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(Predicted_track_17_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = Predicted_track_17_sub_label[i];
			a = Predicted_track_17_sub_label[i+1];
			a = Predicted_track_17_sub_label[i+2];
			a = Predicted_track_17_sub_label[i+3];
			a = Predicted_track_17_sub_label[i+4];
			a = Predicted_track_17_sub_label[i+5];
			a = Predicted_track_17_sub_label[i+6];
			a = Predicted_track_17_sub_label[i+7];
			a = Predicted_track_17_sub_label[i+8];
			a = Predicted_track_17_sub_label[i+9];
		}
	
		for(;i<arraysize;i++){
			a = Predicted_track_17_sub_label[i];
		}

		(void)a;
	}
}

void write_Predicted_track_17_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(Predicted_track_17_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			Predicted_track_17_sub_label[i]   = 0x800A;
			Predicted_track_17_sub_label[i+1] = 0xAFFE;
			Predicted_track_17_sub_label[i+2] = 0xAFFE;
			Predicted_track_17_sub_label[i+3] = 0xAFFE;
			Predicted_track_17_sub_label[i+4] = 0xAFFE;
			Predicted_track_17_sub_label[i+5] = 0xAFFE;
			Predicted_track_17_sub_label[i+6] = 0xAFFE;
			Predicted_track_17_sub_label[i+7] = 0xAFFE;
			Predicted_track_17_sub_label[i+8] = 0xAFFE;
			Predicted_track_17_sub_label[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				Predicted_track_17_sub_label[i]=0xAFFE;
		}
	}
}

int Predicted_track_31_sub_label[0];

static bool isIinitialized_Predicted_track_31_sub_label = false;
void initialize_Predicted_track_31_sub_label() {
	if (!isIinitialized_Predicted_track_31_sub_label){
		int i;
		for (i=0; i < 0; i++){
			Predicted_track_31_sub_label[i] = i+1;
		}
		isIinitialized_Predicted_track_31_sub_label = true;
	}
}

void read_Predicted_track_31_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(Predicted_track_31_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = Predicted_track_31_sub_label[i];
			a = Predicted_track_31_sub_label[i+1];
			a = Predicted_track_31_sub_label[i+2];
			a = Predicted_track_31_sub_label[i+3];
			a = Predicted_track_31_sub_label[i+4];
			a = Predicted_track_31_sub_label[i+5];
			a = Predicted_track_31_sub_label[i+6];
			a = Predicted_track_31_sub_label[i+7];
			a = Predicted_track_31_sub_label[i+8];
			a = Predicted_track_31_sub_label[i+9];
		}
	
		for(;i<arraysize;i++){
			a = Predicted_track_31_sub_label[i];
		}

		(void)a;
	}
}

void write_Predicted_track_31_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(Predicted_track_31_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			Predicted_track_31_sub_label[i]   = 0x800A;
			Predicted_track_31_sub_label[i+1] = 0xAFFE;
			Predicted_track_31_sub_label[i+2] = 0xAFFE;
			Predicted_track_31_sub_label[i+3] = 0xAFFE;
			Predicted_track_31_sub_label[i+4] = 0xAFFE;
			Predicted_track_31_sub_label[i+5] = 0xAFFE;
			Predicted_track_31_sub_label[i+6] = 0xAFFE;
			Predicted_track_31_sub_label[i+7] = 0xAFFE;
			Predicted_track_31_sub_label[i+8] = 0xAFFE;
			Predicted_track_31_sub_label[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				Predicted_track_31_sub_label[i]=0xAFFE;
		}
	}
}

int Predicted_track_30_sub_label[0];

static bool isIinitialized_Predicted_track_30_sub_label = false;
void initialize_Predicted_track_30_sub_label() {
	if (!isIinitialized_Predicted_track_30_sub_label){
		int i;
		for (i=0; i < 0; i++){
			Predicted_track_30_sub_label[i] = i+1;
		}
		isIinitialized_Predicted_track_30_sub_label = true;
	}
}

void read_Predicted_track_30_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(Predicted_track_30_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = Predicted_track_30_sub_label[i];
			a = Predicted_track_30_sub_label[i+1];
			a = Predicted_track_30_sub_label[i+2];
			a = Predicted_track_30_sub_label[i+3];
			a = Predicted_track_30_sub_label[i+4];
			a = Predicted_track_30_sub_label[i+5];
			a = Predicted_track_30_sub_label[i+6];
			a = Predicted_track_30_sub_label[i+7];
			a = Predicted_track_30_sub_label[i+8];
			a = Predicted_track_30_sub_label[i+9];
		}
	
		for(;i<arraysize;i++){
			a = Predicted_track_30_sub_label[i];
		}

		(void)a;
	}
}

void write_Predicted_track_30_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(Predicted_track_30_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			Predicted_track_30_sub_label[i]   = 0x800A;
			Predicted_track_30_sub_label[i+1] = 0xAFFE;
			Predicted_track_30_sub_label[i+2] = 0xAFFE;
			Predicted_track_30_sub_label[i+3] = 0xAFFE;
			Predicted_track_30_sub_label[i+4] = 0xAFFE;
			Predicted_track_30_sub_label[i+5] = 0xAFFE;
			Predicted_track_30_sub_label[i+6] = 0xAFFE;
			Predicted_track_30_sub_label[i+7] = 0xAFFE;
			Predicted_track_30_sub_label[i+8] = 0xAFFE;
			Predicted_track_30_sub_label[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				Predicted_track_30_sub_label[i]=0xAFFE;
		}
	}
}

int Predicted_track_29_sub_label[0];

static bool isIinitialized_Predicted_track_29_sub_label = false;
void initialize_Predicted_track_29_sub_label() {
	if (!isIinitialized_Predicted_track_29_sub_label){
		int i;
		for (i=0; i < 0; i++){
			Predicted_track_29_sub_label[i] = i+1;
		}
		isIinitialized_Predicted_track_29_sub_label = true;
	}
}

void read_Predicted_track_29_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(Predicted_track_29_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = Predicted_track_29_sub_label[i];
			a = Predicted_track_29_sub_label[i+1];
			a = Predicted_track_29_sub_label[i+2];
			a = Predicted_track_29_sub_label[i+3];
			a = Predicted_track_29_sub_label[i+4];
			a = Predicted_track_29_sub_label[i+5];
			a = Predicted_track_29_sub_label[i+6];
			a = Predicted_track_29_sub_label[i+7];
			a = Predicted_track_29_sub_label[i+8];
			a = Predicted_track_29_sub_label[i+9];
		}
	
		for(;i<arraysize;i++){
			a = Predicted_track_29_sub_label[i];
		}

		(void)a;
	}
}

void write_Predicted_track_29_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(Predicted_track_29_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			Predicted_track_29_sub_label[i]   = 0x800A;
			Predicted_track_29_sub_label[i+1] = 0xAFFE;
			Predicted_track_29_sub_label[i+2] = 0xAFFE;
			Predicted_track_29_sub_label[i+3] = 0xAFFE;
			Predicted_track_29_sub_label[i+4] = 0xAFFE;
			Predicted_track_29_sub_label[i+5] = 0xAFFE;
			Predicted_track_29_sub_label[i+6] = 0xAFFE;
			Predicted_track_29_sub_label[i+7] = 0xAFFE;
			Predicted_track_29_sub_label[i+8] = 0xAFFE;
			Predicted_track_29_sub_label[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				Predicted_track_29_sub_label[i]=0xAFFE;
		}
	}
}

int Predicted_track_28_sub_label[0];

static bool isIinitialized_Predicted_track_28_sub_label = false;
void initialize_Predicted_track_28_sub_label() {
	if (!isIinitialized_Predicted_track_28_sub_label){
		int i;
		for (i=0; i < 0; i++){
			Predicted_track_28_sub_label[i] = i+1;
		}
		isIinitialized_Predicted_track_28_sub_label = true;
	}
}

void read_Predicted_track_28_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(Predicted_track_28_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = Predicted_track_28_sub_label[i];
			a = Predicted_track_28_sub_label[i+1];
			a = Predicted_track_28_sub_label[i+2];
			a = Predicted_track_28_sub_label[i+3];
			a = Predicted_track_28_sub_label[i+4];
			a = Predicted_track_28_sub_label[i+5];
			a = Predicted_track_28_sub_label[i+6];
			a = Predicted_track_28_sub_label[i+7];
			a = Predicted_track_28_sub_label[i+8];
			a = Predicted_track_28_sub_label[i+9];
		}
	
		for(;i<arraysize;i++){
			a = Predicted_track_28_sub_label[i];
		}

		(void)a;
	}
}

void write_Predicted_track_28_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(Predicted_track_28_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			Predicted_track_28_sub_label[i]   = 0x800A;
			Predicted_track_28_sub_label[i+1] = 0xAFFE;
			Predicted_track_28_sub_label[i+2] = 0xAFFE;
			Predicted_track_28_sub_label[i+3] = 0xAFFE;
			Predicted_track_28_sub_label[i+4] = 0xAFFE;
			Predicted_track_28_sub_label[i+5] = 0xAFFE;
			Predicted_track_28_sub_label[i+6] = 0xAFFE;
			Predicted_track_28_sub_label[i+7] = 0xAFFE;
			Predicted_track_28_sub_label[i+8] = 0xAFFE;
			Predicted_track_28_sub_label[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				Predicted_track_28_sub_label[i]=0xAFFE;
		}
	}
}

int Predicted_track_27_sub_label[0];

static bool isIinitialized_Predicted_track_27_sub_label = false;
void initialize_Predicted_track_27_sub_label() {
	if (!isIinitialized_Predicted_track_27_sub_label){
		int i;
		for (i=0; i < 0; i++){
			Predicted_track_27_sub_label[i] = i+1;
		}
		isIinitialized_Predicted_track_27_sub_label = true;
	}
}

void read_Predicted_track_27_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(Predicted_track_27_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = Predicted_track_27_sub_label[i];
			a = Predicted_track_27_sub_label[i+1];
			a = Predicted_track_27_sub_label[i+2];
			a = Predicted_track_27_sub_label[i+3];
			a = Predicted_track_27_sub_label[i+4];
			a = Predicted_track_27_sub_label[i+5];
			a = Predicted_track_27_sub_label[i+6];
			a = Predicted_track_27_sub_label[i+7];
			a = Predicted_track_27_sub_label[i+8];
			a = Predicted_track_27_sub_label[i+9];
		}
	
		for(;i<arraysize;i++){
			a = Predicted_track_27_sub_label[i];
		}

		(void)a;
	}
}

void write_Predicted_track_27_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(Predicted_track_27_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			Predicted_track_27_sub_label[i]   = 0x800A;
			Predicted_track_27_sub_label[i+1] = 0xAFFE;
			Predicted_track_27_sub_label[i+2] = 0xAFFE;
			Predicted_track_27_sub_label[i+3] = 0xAFFE;
			Predicted_track_27_sub_label[i+4] = 0xAFFE;
			Predicted_track_27_sub_label[i+5] = 0xAFFE;
			Predicted_track_27_sub_label[i+6] = 0xAFFE;
			Predicted_track_27_sub_label[i+7] = 0xAFFE;
			Predicted_track_27_sub_label[i+8] = 0xAFFE;
			Predicted_track_27_sub_label[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				Predicted_track_27_sub_label[i]=0xAFFE;
		}
	}
}

int Predicted_track_26_sub_label[0];

static bool isIinitialized_Predicted_track_26_sub_label = false;
void initialize_Predicted_track_26_sub_label() {
	if (!isIinitialized_Predicted_track_26_sub_label){
		int i;
		for (i=0; i < 0; i++){
			Predicted_track_26_sub_label[i] = i+1;
		}
		isIinitialized_Predicted_track_26_sub_label = true;
	}
}

void read_Predicted_track_26_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(Predicted_track_26_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = Predicted_track_26_sub_label[i];
			a = Predicted_track_26_sub_label[i+1];
			a = Predicted_track_26_sub_label[i+2];
			a = Predicted_track_26_sub_label[i+3];
			a = Predicted_track_26_sub_label[i+4];
			a = Predicted_track_26_sub_label[i+5];
			a = Predicted_track_26_sub_label[i+6];
			a = Predicted_track_26_sub_label[i+7];
			a = Predicted_track_26_sub_label[i+8];
			a = Predicted_track_26_sub_label[i+9];
		}
	
		for(;i<arraysize;i++){
			a = Predicted_track_26_sub_label[i];
		}

		(void)a;
	}
}

void write_Predicted_track_26_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(Predicted_track_26_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			Predicted_track_26_sub_label[i]   = 0x800A;
			Predicted_track_26_sub_label[i+1] = 0xAFFE;
			Predicted_track_26_sub_label[i+2] = 0xAFFE;
			Predicted_track_26_sub_label[i+3] = 0xAFFE;
			Predicted_track_26_sub_label[i+4] = 0xAFFE;
			Predicted_track_26_sub_label[i+5] = 0xAFFE;
			Predicted_track_26_sub_label[i+6] = 0xAFFE;
			Predicted_track_26_sub_label[i+7] = 0xAFFE;
			Predicted_track_26_sub_label[i+8] = 0xAFFE;
			Predicted_track_26_sub_label[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				Predicted_track_26_sub_label[i]=0xAFFE;
		}
	}
}

int Predicted_track_25_sub_label[0];

static bool isIinitialized_Predicted_track_25_sub_label = false;
void initialize_Predicted_track_25_sub_label() {
	if (!isIinitialized_Predicted_track_25_sub_label){
		int i;
		for (i=0; i < 0; i++){
			Predicted_track_25_sub_label[i] = i+1;
		}
		isIinitialized_Predicted_track_25_sub_label = true;
	}
}

void read_Predicted_track_25_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(Predicted_track_25_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = Predicted_track_25_sub_label[i];
			a = Predicted_track_25_sub_label[i+1];
			a = Predicted_track_25_sub_label[i+2];
			a = Predicted_track_25_sub_label[i+3];
			a = Predicted_track_25_sub_label[i+4];
			a = Predicted_track_25_sub_label[i+5];
			a = Predicted_track_25_sub_label[i+6];
			a = Predicted_track_25_sub_label[i+7];
			a = Predicted_track_25_sub_label[i+8];
			a = Predicted_track_25_sub_label[i+9];
		}
	
		for(;i<arraysize;i++){
			a = Predicted_track_25_sub_label[i];
		}

		(void)a;
	}
}

void write_Predicted_track_25_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(Predicted_track_25_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			Predicted_track_25_sub_label[i]   = 0x800A;
			Predicted_track_25_sub_label[i+1] = 0xAFFE;
			Predicted_track_25_sub_label[i+2] = 0xAFFE;
			Predicted_track_25_sub_label[i+3] = 0xAFFE;
			Predicted_track_25_sub_label[i+4] = 0xAFFE;
			Predicted_track_25_sub_label[i+5] = 0xAFFE;
			Predicted_track_25_sub_label[i+6] = 0xAFFE;
			Predicted_track_25_sub_label[i+7] = 0xAFFE;
			Predicted_track_25_sub_label[i+8] = 0xAFFE;
			Predicted_track_25_sub_label[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				Predicted_track_25_sub_label[i]=0xAFFE;
		}
	}
}

int Predicted_track_24_sub_label[0];

static bool isIinitialized_Predicted_track_24_sub_label = false;
void initialize_Predicted_track_24_sub_label() {
	if (!isIinitialized_Predicted_track_24_sub_label){
		int i;
		for (i=0; i < 0; i++){
			Predicted_track_24_sub_label[i] = i+1;
		}
		isIinitialized_Predicted_track_24_sub_label = true;
	}
}

void read_Predicted_track_24_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(Predicted_track_24_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = Predicted_track_24_sub_label[i];
			a = Predicted_track_24_sub_label[i+1];
			a = Predicted_track_24_sub_label[i+2];
			a = Predicted_track_24_sub_label[i+3];
			a = Predicted_track_24_sub_label[i+4];
			a = Predicted_track_24_sub_label[i+5];
			a = Predicted_track_24_sub_label[i+6];
			a = Predicted_track_24_sub_label[i+7];
			a = Predicted_track_24_sub_label[i+8];
			a = Predicted_track_24_sub_label[i+9];
		}
	
		for(;i<arraysize;i++){
			a = Predicted_track_24_sub_label[i];
		}

		(void)a;
	}
}

void write_Predicted_track_24_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(Predicted_track_24_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			Predicted_track_24_sub_label[i]   = 0x800A;
			Predicted_track_24_sub_label[i+1] = 0xAFFE;
			Predicted_track_24_sub_label[i+2] = 0xAFFE;
			Predicted_track_24_sub_label[i+3] = 0xAFFE;
			Predicted_track_24_sub_label[i+4] = 0xAFFE;
			Predicted_track_24_sub_label[i+5] = 0xAFFE;
			Predicted_track_24_sub_label[i+6] = 0xAFFE;
			Predicted_track_24_sub_label[i+7] = 0xAFFE;
			Predicted_track_24_sub_label[i+8] = 0xAFFE;
			Predicted_track_24_sub_label[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				Predicted_track_24_sub_label[i]=0xAFFE;
		}
	}
}

int Predicted_track_20_sub_label[0];

static bool isIinitialized_Predicted_track_20_sub_label = false;
void initialize_Predicted_track_20_sub_label() {
	if (!isIinitialized_Predicted_track_20_sub_label){
		int i;
		for (i=0; i < 0; i++){
			Predicted_track_20_sub_label[i] = i+1;
		}
		isIinitialized_Predicted_track_20_sub_label = true;
	}
}

void read_Predicted_track_20_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(Predicted_track_20_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = Predicted_track_20_sub_label[i];
			a = Predicted_track_20_sub_label[i+1];
			a = Predicted_track_20_sub_label[i+2];
			a = Predicted_track_20_sub_label[i+3];
			a = Predicted_track_20_sub_label[i+4];
			a = Predicted_track_20_sub_label[i+5];
			a = Predicted_track_20_sub_label[i+6];
			a = Predicted_track_20_sub_label[i+7];
			a = Predicted_track_20_sub_label[i+8];
			a = Predicted_track_20_sub_label[i+9];
		}
	
		for(;i<arraysize;i++){
			a = Predicted_track_20_sub_label[i];
		}

		(void)a;
	}
}

void write_Predicted_track_20_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(Predicted_track_20_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			Predicted_track_20_sub_label[i]   = 0x800A;
			Predicted_track_20_sub_label[i+1] = 0xAFFE;
			Predicted_track_20_sub_label[i+2] = 0xAFFE;
			Predicted_track_20_sub_label[i+3] = 0xAFFE;
			Predicted_track_20_sub_label[i+4] = 0xAFFE;
			Predicted_track_20_sub_label[i+5] = 0xAFFE;
			Predicted_track_20_sub_label[i+6] = 0xAFFE;
			Predicted_track_20_sub_label[i+7] = 0xAFFE;
			Predicted_track_20_sub_label[i+8] = 0xAFFE;
			Predicted_track_20_sub_label[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				Predicted_track_20_sub_label[i]=0xAFFE;
		}
	}
}

int Predicted_track_22_sub_label[0];

static bool isIinitialized_Predicted_track_22_sub_label = false;
void initialize_Predicted_track_22_sub_label() {
	if (!isIinitialized_Predicted_track_22_sub_label){
		int i;
		for (i=0; i < 0; i++){
			Predicted_track_22_sub_label[i] = i+1;
		}
		isIinitialized_Predicted_track_22_sub_label = true;
	}
}

void read_Predicted_track_22_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(Predicted_track_22_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = Predicted_track_22_sub_label[i];
			a = Predicted_track_22_sub_label[i+1];
			a = Predicted_track_22_sub_label[i+2];
			a = Predicted_track_22_sub_label[i+3];
			a = Predicted_track_22_sub_label[i+4];
			a = Predicted_track_22_sub_label[i+5];
			a = Predicted_track_22_sub_label[i+6];
			a = Predicted_track_22_sub_label[i+7];
			a = Predicted_track_22_sub_label[i+8];
			a = Predicted_track_22_sub_label[i+9];
		}
	
		for(;i<arraysize;i++){
			a = Predicted_track_22_sub_label[i];
		}

		(void)a;
	}
}

void write_Predicted_track_22_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(Predicted_track_22_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			Predicted_track_22_sub_label[i]   = 0x800A;
			Predicted_track_22_sub_label[i+1] = 0xAFFE;
			Predicted_track_22_sub_label[i+2] = 0xAFFE;
			Predicted_track_22_sub_label[i+3] = 0xAFFE;
			Predicted_track_22_sub_label[i+4] = 0xAFFE;
			Predicted_track_22_sub_label[i+5] = 0xAFFE;
			Predicted_track_22_sub_label[i+6] = 0xAFFE;
			Predicted_track_22_sub_label[i+7] = 0xAFFE;
			Predicted_track_22_sub_label[i+8] = 0xAFFE;
			Predicted_track_22_sub_label[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				Predicted_track_22_sub_label[i]=0xAFFE;
		}
	}
}

int Predicted_track_19_sub_label[0];

static bool isIinitialized_Predicted_track_19_sub_label = false;
void initialize_Predicted_track_19_sub_label() {
	if (!isIinitialized_Predicted_track_19_sub_label){
		int i;
		for (i=0; i < 0; i++){
			Predicted_track_19_sub_label[i] = i+1;
		}
		isIinitialized_Predicted_track_19_sub_label = true;
	}
}

void read_Predicted_track_19_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(Predicted_track_19_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = Predicted_track_19_sub_label[i];
			a = Predicted_track_19_sub_label[i+1];
			a = Predicted_track_19_sub_label[i+2];
			a = Predicted_track_19_sub_label[i+3];
			a = Predicted_track_19_sub_label[i+4];
			a = Predicted_track_19_sub_label[i+5];
			a = Predicted_track_19_sub_label[i+6];
			a = Predicted_track_19_sub_label[i+7];
			a = Predicted_track_19_sub_label[i+8];
			a = Predicted_track_19_sub_label[i+9];
		}
	
		for(;i<arraysize;i++){
			a = Predicted_track_19_sub_label[i];
		}

		(void)a;
	}
}

void write_Predicted_track_19_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(Predicted_track_19_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			Predicted_track_19_sub_label[i]   = 0x800A;
			Predicted_track_19_sub_label[i+1] = 0xAFFE;
			Predicted_track_19_sub_label[i+2] = 0xAFFE;
			Predicted_track_19_sub_label[i+3] = 0xAFFE;
			Predicted_track_19_sub_label[i+4] = 0xAFFE;
			Predicted_track_19_sub_label[i+5] = 0xAFFE;
			Predicted_track_19_sub_label[i+6] = 0xAFFE;
			Predicted_track_19_sub_label[i+7] = 0xAFFE;
			Predicted_track_19_sub_label[i+8] = 0xAFFE;
			Predicted_track_19_sub_label[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				Predicted_track_19_sub_label[i]=0xAFFE;
		}
	}
}

int Predicted_track_18_sub_label[0];

static bool isIinitialized_Predicted_track_18_sub_label = false;
void initialize_Predicted_track_18_sub_label() {
	if (!isIinitialized_Predicted_track_18_sub_label){
		int i;
		for (i=0; i < 0; i++){
			Predicted_track_18_sub_label[i] = i+1;
		}
		isIinitialized_Predicted_track_18_sub_label = true;
	}
}

void read_Predicted_track_18_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(Predicted_track_18_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = Predicted_track_18_sub_label[i];
			a = Predicted_track_18_sub_label[i+1];
			a = Predicted_track_18_sub_label[i+2];
			a = Predicted_track_18_sub_label[i+3];
			a = Predicted_track_18_sub_label[i+4];
			a = Predicted_track_18_sub_label[i+5];
			a = Predicted_track_18_sub_label[i+6];
			a = Predicted_track_18_sub_label[i+7];
			a = Predicted_track_18_sub_label[i+8];
			a = Predicted_track_18_sub_label[i+9];
		}
	
		for(;i<arraysize;i++){
			a = Predicted_track_18_sub_label[i];
		}

		(void)a;
	}
}

void write_Predicted_track_18_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(Predicted_track_18_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			Predicted_track_18_sub_label[i]   = 0x800A;
			Predicted_track_18_sub_label[i+1] = 0xAFFE;
			Predicted_track_18_sub_label[i+2] = 0xAFFE;
			Predicted_track_18_sub_label[i+3] = 0xAFFE;
			Predicted_track_18_sub_label[i+4] = 0xAFFE;
			Predicted_track_18_sub_label[i+5] = 0xAFFE;
			Predicted_track_18_sub_label[i+6] = 0xAFFE;
			Predicted_track_18_sub_label[i+7] = 0xAFFE;
			Predicted_track_18_sub_label[i+8] = 0xAFFE;
			Predicted_track_18_sub_label[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				Predicted_track_18_sub_label[i]=0xAFFE;
		}
	}
}

int Predicted_track_23_sub_label[0];

static bool isIinitialized_Predicted_track_23_sub_label = false;
void initialize_Predicted_track_23_sub_label() {
	if (!isIinitialized_Predicted_track_23_sub_label){
		int i;
		for (i=0; i < 0; i++){
			Predicted_track_23_sub_label[i] = i+1;
		}
		isIinitialized_Predicted_track_23_sub_label = true;
	}
}

void read_Predicted_track_23_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(Predicted_track_23_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = Predicted_track_23_sub_label[i];
			a = Predicted_track_23_sub_label[i+1];
			a = Predicted_track_23_sub_label[i+2];
			a = Predicted_track_23_sub_label[i+3];
			a = Predicted_track_23_sub_label[i+4];
			a = Predicted_track_23_sub_label[i+5];
			a = Predicted_track_23_sub_label[i+6];
			a = Predicted_track_23_sub_label[i+7];
			a = Predicted_track_23_sub_label[i+8];
			a = Predicted_track_23_sub_label[i+9];
		}
	
		for(;i<arraysize;i++){
			a = Predicted_track_23_sub_label[i];
		}

		(void)a;
	}
}

void write_Predicted_track_23_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(Predicted_track_23_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			Predicted_track_23_sub_label[i]   = 0x800A;
			Predicted_track_23_sub_label[i+1] = 0xAFFE;
			Predicted_track_23_sub_label[i+2] = 0xAFFE;
			Predicted_track_23_sub_label[i+3] = 0xAFFE;
			Predicted_track_23_sub_label[i+4] = 0xAFFE;
			Predicted_track_23_sub_label[i+5] = 0xAFFE;
			Predicted_track_23_sub_label[i+6] = 0xAFFE;
			Predicted_track_23_sub_label[i+7] = 0xAFFE;
			Predicted_track_23_sub_label[i+8] = 0xAFFE;
			Predicted_track_23_sub_label[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				Predicted_track_23_sub_label[i]=0xAFFE;
		}
	}
}

int Predicted_track_21_sub_label[0];

static bool isIinitialized_Predicted_track_21_sub_label = false;
void initialize_Predicted_track_21_sub_label() {
	if (!isIinitialized_Predicted_track_21_sub_label){
		int i;
		for (i=0; i < 0; i++){
			Predicted_track_21_sub_label[i] = i+1;
		}
		isIinitialized_Predicted_track_21_sub_label = true;
	}
}

void read_Predicted_track_21_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(Predicted_track_21_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = Predicted_track_21_sub_label[i];
			a = Predicted_track_21_sub_label[i+1];
			a = Predicted_track_21_sub_label[i+2];
			a = Predicted_track_21_sub_label[i+3];
			a = Predicted_track_21_sub_label[i+4];
			a = Predicted_track_21_sub_label[i+5];
			a = Predicted_track_21_sub_label[i+6];
			a = Predicted_track_21_sub_label[i+7];
			a = Predicted_track_21_sub_label[i+8];
			a = Predicted_track_21_sub_label[i+9];
		}
	
		for(;i<arraysize;i++){
			a = Predicted_track_21_sub_label[i];
		}

		(void)a;
	}
}

void write_Predicted_track_21_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(Predicted_track_21_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			Predicted_track_21_sub_label[i]   = 0x800A;
			Predicted_track_21_sub_label[i+1] = 0xAFFE;
			Predicted_track_21_sub_label[i+2] = 0xAFFE;
			Predicted_track_21_sub_label[i+3] = 0xAFFE;
			Predicted_track_21_sub_label[i+4] = 0xAFFE;
			Predicted_track_21_sub_label[i+5] = 0xAFFE;
			Predicted_track_21_sub_label[i+6] = 0xAFFE;
			Predicted_track_21_sub_label[i+7] = 0xAFFE;
			Predicted_track_21_sub_label[i+8] = 0xAFFE;
			Predicted_track_21_sub_label[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				Predicted_track_21_sub_label[i]=0xAFFE;
		}
	}
}

int Predicted_track_16_sub_label[0];

static bool isIinitialized_Predicted_track_16_sub_label = false;
void initialize_Predicted_track_16_sub_label() {
	if (!isIinitialized_Predicted_track_16_sub_label){
		int i;
		for (i=0; i < 0; i++){
			Predicted_track_16_sub_label[i] = i+1;
		}
		isIinitialized_Predicted_track_16_sub_label = true;
	}
}

void read_Predicted_track_16_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(Predicted_track_16_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = Predicted_track_16_sub_label[i];
			a = Predicted_track_16_sub_label[i+1];
			a = Predicted_track_16_sub_label[i+2];
			a = Predicted_track_16_sub_label[i+3];
			a = Predicted_track_16_sub_label[i+4];
			a = Predicted_track_16_sub_label[i+5];
			a = Predicted_track_16_sub_label[i+6];
			a = Predicted_track_16_sub_label[i+7];
			a = Predicted_track_16_sub_label[i+8];
			a = Predicted_track_16_sub_label[i+9];
		}
	
		for(;i<arraysize;i++){
			a = Predicted_track_16_sub_label[i];
		}

		(void)a;
	}
}

void write_Predicted_track_16_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(Predicted_track_16_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			Predicted_track_16_sub_label[i]   = 0x800A;
			Predicted_track_16_sub_label[i+1] = 0xAFFE;
			Predicted_track_16_sub_label[i+2] = 0xAFFE;
			Predicted_track_16_sub_label[i+3] = 0xAFFE;
			Predicted_track_16_sub_label[i+4] = 0xAFFE;
			Predicted_track_16_sub_label[i+5] = 0xAFFE;
			Predicted_track_16_sub_label[i+6] = 0xAFFE;
			Predicted_track_16_sub_label[i+7] = 0xAFFE;
			Predicted_track_16_sub_label[i+8] = 0xAFFE;
			Predicted_track_16_sub_label[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				Predicted_track_16_sub_label[i]=0xAFFE;
		}
	}
}

int Predicted_track_01_sub_label[0];

static bool isIinitialized_Predicted_track_01_sub_label = false;
void initialize_Predicted_track_01_sub_label() {
	if (!isIinitialized_Predicted_track_01_sub_label){
		int i;
		for (i=0; i < 0; i++){
			Predicted_track_01_sub_label[i] = i+1;
		}
		isIinitialized_Predicted_track_01_sub_label = true;
	}
}

void read_Predicted_track_01_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(Predicted_track_01_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = Predicted_track_01_sub_label[i];
			a = Predicted_track_01_sub_label[i+1];
			a = Predicted_track_01_sub_label[i+2];
			a = Predicted_track_01_sub_label[i+3];
			a = Predicted_track_01_sub_label[i+4];
			a = Predicted_track_01_sub_label[i+5];
			a = Predicted_track_01_sub_label[i+6];
			a = Predicted_track_01_sub_label[i+7];
			a = Predicted_track_01_sub_label[i+8];
			a = Predicted_track_01_sub_label[i+9];
		}
	
		for(;i<arraysize;i++){
			a = Predicted_track_01_sub_label[i];
		}

		(void)a;
	}
}

void write_Predicted_track_01_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(Predicted_track_01_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			Predicted_track_01_sub_label[i]   = 0x800A;
			Predicted_track_01_sub_label[i+1] = 0xAFFE;
			Predicted_track_01_sub_label[i+2] = 0xAFFE;
			Predicted_track_01_sub_label[i+3] = 0xAFFE;
			Predicted_track_01_sub_label[i+4] = 0xAFFE;
			Predicted_track_01_sub_label[i+5] = 0xAFFE;
			Predicted_track_01_sub_label[i+6] = 0xAFFE;
			Predicted_track_01_sub_label[i+7] = 0xAFFE;
			Predicted_track_01_sub_label[i+8] = 0xAFFE;
			Predicted_track_01_sub_label[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				Predicted_track_01_sub_label[i]=0xAFFE;
		}
	}
}

int Predicted_track_15_sub_label[0];

static bool isIinitialized_Predicted_track_15_sub_label = false;
void initialize_Predicted_track_15_sub_label() {
	if (!isIinitialized_Predicted_track_15_sub_label){
		int i;
		for (i=0; i < 0; i++){
			Predicted_track_15_sub_label[i] = i+1;
		}
		isIinitialized_Predicted_track_15_sub_label = true;
	}
}

void read_Predicted_track_15_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(Predicted_track_15_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = Predicted_track_15_sub_label[i];
			a = Predicted_track_15_sub_label[i+1];
			a = Predicted_track_15_sub_label[i+2];
			a = Predicted_track_15_sub_label[i+3];
			a = Predicted_track_15_sub_label[i+4];
			a = Predicted_track_15_sub_label[i+5];
			a = Predicted_track_15_sub_label[i+6];
			a = Predicted_track_15_sub_label[i+7];
			a = Predicted_track_15_sub_label[i+8];
			a = Predicted_track_15_sub_label[i+9];
		}
	
		for(;i<arraysize;i++){
			a = Predicted_track_15_sub_label[i];
		}

		(void)a;
	}
}

void write_Predicted_track_15_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(Predicted_track_15_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			Predicted_track_15_sub_label[i]   = 0x800A;
			Predicted_track_15_sub_label[i+1] = 0xAFFE;
			Predicted_track_15_sub_label[i+2] = 0xAFFE;
			Predicted_track_15_sub_label[i+3] = 0xAFFE;
			Predicted_track_15_sub_label[i+4] = 0xAFFE;
			Predicted_track_15_sub_label[i+5] = 0xAFFE;
			Predicted_track_15_sub_label[i+6] = 0xAFFE;
			Predicted_track_15_sub_label[i+7] = 0xAFFE;
			Predicted_track_15_sub_label[i+8] = 0xAFFE;
			Predicted_track_15_sub_label[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				Predicted_track_15_sub_label[i]=0xAFFE;
		}
	}
}

int Predicted_track_14_sub_label[0];

static bool isIinitialized_Predicted_track_14_sub_label = false;
void initialize_Predicted_track_14_sub_label() {
	if (!isIinitialized_Predicted_track_14_sub_label){
		int i;
		for (i=0; i < 0; i++){
			Predicted_track_14_sub_label[i] = i+1;
		}
		isIinitialized_Predicted_track_14_sub_label = true;
	}
}

void read_Predicted_track_14_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(Predicted_track_14_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = Predicted_track_14_sub_label[i];
			a = Predicted_track_14_sub_label[i+1];
			a = Predicted_track_14_sub_label[i+2];
			a = Predicted_track_14_sub_label[i+3];
			a = Predicted_track_14_sub_label[i+4];
			a = Predicted_track_14_sub_label[i+5];
			a = Predicted_track_14_sub_label[i+6];
			a = Predicted_track_14_sub_label[i+7];
			a = Predicted_track_14_sub_label[i+8];
			a = Predicted_track_14_sub_label[i+9];
		}
	
		for(;i<arraysize;i++){
			a = Predicted_track_14_sub_label[i];
		}

		(void)a;
	}
}

void write_Predicted_track_14_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(Predicted_track_14_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			Predicted_track_14_sub_label[i]   = 0x800A;
			Predicted_track_14_sub_label[i+1] = 0xAFFE;
			Predicted_track_14_sub_label[i+2] = 0xAFFE;
			Predicted_track_14_sub_label[i+3] = 0xAFFE;
			Predicted_track_14_sub_label[i+4] = 0xAFFE;
			Predicted_track_14_sub_label[i+5] = 0xAFFE;
			Predicted_track_14_sub_label[i+6] = 0xAFFE;
			Predicted_track_14_sub_label[i+7] = 0xAFFE;
			Predicted_track_14_sub_label[i+8] = 0xAFFE;
			Predicted_track_14_sub_label[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				Predicted_track_14_sub_label[i]=0xAFFE;
		}
	}
}

int Predicted_track_13_sub_label[0];

static bool isIinitialized_Predicted_track_13_sub_label = false;
void initialize_Predicted_track_13_sub_label() {
	if (!isIinitialized_Predicted_track_13_sub_label){
		int i;
		for (i=0; i < 0; i++){
			Predicted_track_13_sub_label[i] = i+1;
		}
		isIinitialized_Predicted_track_13_sub_label = true;
	}
}

void read_Predicted_track_13_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(Predicted_track_13_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = Predicted_track_13_sub_label[i];
			a = Predicted_track_13_sub_label[i+1];
			a = Predicted_track_13_sub_label[i+2];
			a = Predicted_track_13_sub_label[i+3];
			a = Predicted_track_13_sub_label[i+4];
			a = Predicted_track_13_sub_label[i+5];
			a = Predicted_track_13_sub_label[i+6];
			a = Predicted_track_13_sub_label[i+7];
			a = Predicted_track_13_sub_label[i+8];
			a = Predicted_track_13_sub_label[i+9];
		}
	
		for(;i<arraysize;i++){
			a = Predicted_track_13_sub_label[i];
		}

		(void)a;
	}
}

void write_Predicted_track_13_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(Predicted_track_13_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			Predicted_track_13_sub_label[i]   = 0x800A;
			Predicted_track_13_sub_label[i+1] = 0xAFFE;
			Predicted_track_13_sub_label[i+2] = 0xAFFE;
			Predicted_track_13_sub_label[i+3] = 0xAFFE;
			Predicted_track_13_sub_label[i+4] = 0xAFFE;
			Predicted_track_13_sub_label[i+5] = 0xAFFE;
			Predicted_track_13_sub_label[i+6] = 0xAFFE;
			Predicted_track_13_sub_label[i+7] = 0xAFFE;
			Predicted_track_13_sub_label[i+8] = 0xAFFE;
			Predicted_track_13_sub_label[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				Predicted_track_13_sub_label[i]=0xAFFE;
		}
	}
}

int Predicted_track_12_sub_label[0];

static bool isIinitialized_Predicted_track_12_sub_label = false;
void initialize_Predicted_track_12_sub_label() {
	if (!isIinitialized_Predicted_track_12_sub_label){
		int i;
		for (i=0; i < 0; i++){
			Predicted_track_12_sub_label[i] = i+1;
		}
		isIinitialized_Predicted_track_12_sub_label = true;
	}
}

void read_Predicted_track_12_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(Predicted_track_12_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = Predicted_track_12_sub_label[i];
			a = Predicted_track_12_sub_label[i+1];
			a = Predicted_track_12_sub_label[i+2];
			a = Predicted_track_12_sub_label[i+3];
			a = Predicted_track_12_sub_label[i+4];
			a = Predicted_track_12_sub_label[i+5];
			a = Predicted_track_12_sub_label[i+6];
			a = Predicted_track_12_sub_label[i+7];
			a = Predicted_track_12_sub_label[i+8];
			a = Predicted_track_12_sub_label[i+9];
		}
	
		for(;i<arraysize;i++){
			a = Predicted_track_12_sub_label[i];
		}

		(void)a;
	}
}

void write_Predicted_track_12_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(Predicted_track_12_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			Predicted_track_12_sub_label[i]   = 0x800A;
			Predicted_track_12_sub_label[i+1] = 0xAFFE;
			Predicted_track_12_sub_label[i+2] = 0xAFFE;
			Predicted_track_12_sub_label[i+3] = 0xAFFE;
			Predicted_track_12_sub_label[i+4] = 0xAFFE;
			Predicted_track_12_sub_label[i+5] = 0xAFFE;
			Predicted_track_12_sub_label[i+6] = 0xAFFE;
			Predicted_track_12_sub_label[i+7] = 0xAFFE;
			Predicted_track_12_sub_label[i+8] = 0xAFFE;
			Predicted_track_12_sub_label[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				Predicted_track_12_sub_label[i]=0xAFFE;
		}
	}
}

int Predicted_track_11_sub_label[0];

static bool isIinitialized_Predicted_track_11_sub_label = false;
void initialize_Predicted_track_11_sub_label() {
	if (!isIinitialized_Predicted_track_11_sub_label){
		int i;
		for (i=0; i < 0; i++){
			Predicted_track_11_sub_label[i] = i+1;
		}
		isIinitialized_Predicted_track_11_sub_label = true;
	}
}

void read_Predicted_track_11_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(Predicted_track_11_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = Predicted_track_11_sub_label[i];
			a = Predicted_track_11_sub_label[i+1];
			a = Predicted_track_11_sub_label[i+2];
			a = Predicted_track_11_sub_label[i+3];
			a = Predicted_track_11_sub_label[i+4];
			a = Predicted_track_11_sub_label[i+5];
			a = Predicted_track_11_sub_label[i+6];
			a = Predicted_track_11_sub_label[i+7];
			a = Predicted_track_11_sub_label[i+8];
			a = Predicted_track_11_sub_label[i+9];
		}
	
		for(;i<arraysize;i++){
			a = Predicted_track_11_sub_label[i];
		}

		(void)a;
	}
}

void write_Predicted_track_11_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(Predicted_track_11_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			Predicted_track_11_sub_label[i]   = 0x800A;
			Predicted_track_11_sub_label[i+1] = 0xAFFE;
			Predicted_track_11_sub_label[i+2] = 0xAFFE;
			Predicted_track_11_sub_label[i+3] = 0xAFFE;
			Predicted_track_11_sub_label[i+4] = 0xAFFE;
			Predicted_track_11_sub_label[i+5] = 0xAFFE;
			Predicted_track_11_sub_label[i+6] = 0xAFFE;
			Predicted_track_11_sub_label[i+7] = 0xAFFE;
			Predicted_track_11_sub_label[i+8] = 0xAFFE;
			Predicted_track_11_sub_label[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				Predicted_track_11_sub_label[i]=0xAFFE;
		}
	}
}

int Predicted_track_10_sub_label[0];

static bool isIinitialized_Predicted_track_10_sub_label = false;
void initialize_Predicted_track_10_sub_label() {
	if (!isIinitialized_Predicted_track_10_sub_label){
		int i;
		for (i=0; i < 0; i++){
			Predicted_track_10_sub_label[i] = i+1;
		}
		isIinitialized_Predicted_track_10_sub_label = true;
	}
}

void read_Predicted_track_10_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(Predicted_track_10_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = Predicted_track_10_sub_label[i];
			a = Predicted_track_10_sub_label[i+1];
			a = Predicted_track_10_sub_label[i+2];
			a = Predicted_track_10_sub_label[i+3];
			a = Predicted_track_10_sub_label[i+4];
			a = Predicted_track_10_sub_label[i+5];
			a = Predicted_track_10_sub_label[i+6];
			a = Predicted_track_10_sub_label[i+7];
			a = Predicted_track_10_sub_label[i+8];
			a = Predicted_track_10_sub_label[i+9];
		}
	
		for(;i<arraysize;i++){
			a = Predicted_track_10_sub_label[i];
		}

		(void)a;
	}
}

void write_Predicted_track_10_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(Predicted_track_10_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			Predicted_track_10_sub_label[i]   = 0x800A;
			Predicted_track_10_sub_label[i+1] = 0xAFFE;
			Predicted_track_10_sub_label[i+2] = 0xAFFE;
			Predicted_track_10_sub_label[i+3] = 0xAFFE;
			Predicted_track_10_sub_label[i+4] = 0xAFFE;
			Predicted_track_10_sub_label[i+5] = 0xAFFE;
			Predicted_track_10_sub_label[i+6] = 0xAFFE;
			Predicted_track_10_sub_label[i+7] = 0xAFFE;
			Predicted_track_10_sub_label[i+8] = 0xAFFE;
			Predicted_track_10_sub_label[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				Predicted_track_10_sub_label[i]=0xAFFE;
		}
	}
}

int Predicted_track_09_sub_label[0];

static bool isIinitialized_Predicted_track_09_sub_label = false;
void initialize_Predicted_track_09_sub_label() {
	if (!isIinitialized_Predicted_track_09_sub_label){
		int i;
		for (i=0; i < 0; i++){
			Predicted_track_09_sub_label[i] = i+1;
		}
		isIinitialized_Predicted_track_09_sub_label = true;
	}
}

void read_Predicted_track_09_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(Predicted_track_09_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = Predicted_track_09_sub_label[i];
			a = Predicted_track_09_sub_label[i+1];
			a = Predicted_track_09_sub_label[i+2];
			a = Predicted_track_09_sub_label[i+3];
			a = Predicted_track_09_sub_label[i+4];
			a = Predicted_track_09_sub_label[i+5];
			a = Predicted_track_09_sub_label[i+6];
			a = Predicted_track_09_sub_label[i+7];
			a = Predicted_track_09_sub_label[i+8];
			a = Predicted_track_09_sub_label[i+9];
		}
	
		for(;i<arraysize;i++){
			a = Predicted_track_09_sub_label[i];
		}

		(void)a;
	}
}

void write_Predicted_track_09_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(Predicted_track_09_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			Predicted_track_09_sub_label[i]   = 0x800A;
			Predicted_track_09_sub_label[i+1] = 0xAFFE;
			Predicted_track_09_sub_label[i+2] = 0xAFFE;
			Predicted_track_09_sub_label[i+3] = 0xAFFE;
			Predicted_track_09_sub_label[i+4] = 0xAFFE;
			Predicted_track_09_sub_label[i+5] = 0xAFFE;
			Predicted_track_09_sub_label[i+6] = 0xAFFE;
			Predicted_track_09_sub_label[i+7] = 0xAFFE;
			Predicted_track_09_sub_label[i+8] = 0xAFFE;
			Predicted_track_09_sub_label[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				Predicted_track_09_sub_label[i]=0xAFFE;
		}
	}
}

int Predicted_track_08_sub_label[0];

static bool isIinitialized_Predicted_track_08_sub_label = false;
void initialize_Predicted_track_08_sub_label() {
	if (!isIinitialized_Predicted_track_08_sub_label){
		int i;
		for (i=0; i < 0; i++){
			Predicted_track_08_sub_label[i] = i+1;
		}
		isIinitialized_Predicted_track_08_sub_label = true;
	}
}

void read_Predicted_track_08_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(Predicted_track_08_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = Predicted_track_08_sub_label[i];
			a = Predicted_track_08_sub_label[i+1];
			a = Predicted_track_08_sub_label[i+2];
			a = Predicted_track_08_sub_label[i+3];
			a = Predicted_track_08_sub_label[i+4];
			a = Predicted_track_08_sub_label[i+5];
			a = Predicted_track_08_sub_label[i+6];
			a = Predicted_track_08_sub_label[i+7];
			a = Predicted_track_08_sub_label[i+8];
			a = Predicted_track_08_sub_label[i+9];
		}
	
		for(;i<arraysize;i++){
			a = Predicted_track_08_sub_label[i];
		}

		(void)a;
	}
}

void write_Predicted_track_08_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(Predicted_track_08_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			Predicted_track_08_sub_label[i]   = 0x800A;
			Predicted_track_08_sub_label[i+1] = 0xAFFE;
			Predicted_track_08_sub_label[i+2] = 0xAFFE;
			Predicted_track_08_sub_label[i+3] = 0xAFFE;
			Predicted_track_08_sub_label[i+4] = 0xAFFE;
			Predicted_track_08_sub_label[i+5] = 0xAFFE;
			Predicted_track_08_sub_label[i+6] = 0xAFFE;
			Predicted_track_08_sub_label[i+7] = 0xAFFE;
			Predicted_track_08_sub_label[i+8] = 0xAFFE;
			Predicted_track_08_sub_label[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				Predicted_track_08_sub_label[i]=0xAFFE;
		}
	}
}

int Predicted_track_04_sub_label[0];

static bool isIinitialized_Predicted_track_04_sub_label = false;
void initialize_Predicted_track_04_sub_label() {
	if (!isIinitialized_Predicted_track_04_sub_label){
		int i;
		for (i=0; i < 0; i++){
			Predicted_track_04_sub_label[i] = i+1;
		}
		isIinitialized_Predicted_track_04_sub_label = true;
	}
}

void read_Predicted_track_04_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(Predicted_track_04_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = Predicted_track_04_sub_label[i];
			a = Predicted_track_04_sub_label[i+1];
			a = Predicted_track_04_sub_label[i+2];
			a = Predicted_track_04_sub_label[i+3];
			a = Predicted_track_04_sub_label[i+4];
			a = Predicted_track_04_sub_label[i+5];
			a = Predicted_track_04_sub_label[i+6];
			a = Predicted_track_04_sub_label[i+7];
			a = Predicted_track_04_sub_label[i+8];
			a = Predicted_track_04_sub_label[i+9];
		}
	
		for(;i<arraysize;i++){
			a = Predicted_track_04_sub_label[i];
		}

		(void)a;
	}
}

void write_Predicted_track_04_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(Predicted_track_04_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			Predicted_track_04_sub_label[i]   = 0x800A;
			Predicted_track_04_sub_label[i+1] = 0xAFFE;
			Predicted_track_04_sub_label[i+2] = 0xAFFE;
			Predicted_track_04_sub_label[i+3] = 0xAFFE;
			Predicted_track_04_sub_label[i+4] = 0xAFFE;
			Predicted_track_04_sub_label[i+5] = 0xAFFE;
			Predicted_track_04_sub_label[i+6] = 0xAFFE;
			Predicted_track_04_sub_label[i+7] = 0xAFFE;
			Predicted_track_04_sub_label[i+8] = 0xAFFE;
			Predicted_track_04_sub_label[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				Predicted_track_04_sub_label[i]=0xAFFE;
		}
	}
}

int Predicted_track_06_sub_label[0];

static bool isIinitialized_Predicted_track_06_sub_label = false;
void initialize_Predicted_track_06_sub_label() {
	if (!isIinitialized_Predicted_track_06_sub_label){
		int i;
		for (i=0; i < 0; i++){
			Predicted_track_06_sub_label[i] = i+1;
		}
		isIinitialized_Predicted_track_06_sub_label = true;
	}
}

void read_Predicted_track_06_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(Predicted_track_06_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = Predicted_track_06_sub_label[i];
			a = Predicted_track_06_sub_label[i+1];
			a = Predicted_track_06_sub_label[i+2];
			a = Predicted_track_06_sub_label[i+3];
			a = Predicted_track_06_sub_label[i+4];
			a = Predicted_track_06_sub_label[i+5];
			a = Predicted_track_06_sub_label[i+6];
			a = Predicted_track_06_sub_label[i+7];
			a = Predicted_track_06_sub_label[i+8];
			a = Predicted_track_06_sub_label[i+9];
		}
	
		for(;i<arraysize;i++){
			a = Predicted_track_06_sub_label[i];
		}

		(void)a;
	}
}

void write_Predicted_track_06_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(Predicted_track_06_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			Predicted_track_06_sub_label[i]   = 0x800A;
			Predicted_track_06_sub_label[i+1] = 0xAFFE;
			Predicted_track_06_sub_label[i+2] = 0xAFFE;
			Predicted_track_06_sub_label[i+3] = 0xAFFE;
			Predicted_track_06_sub_label[i+4] = 0xAFFE;
			Predicted_track_06_sub_label[i+5] = 0xAFFE;
			Predicted_track_06_sub_label[i+6] = 0xAFFE;
			Predicted_track_06_sub_label[i+7] = 0xAFFE;
			Predicted_track_06_sub_label[i+8] = 0xAFFE;
			Predicted_track_06_sub_label[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				Predicted_track_06_sub_label[i]=0xAFFE;
		}
	}
}

int Predicted_track_03_sub_label[0];

static bool isIinitialized_Predicted_track_03_sub_label = false;
void initialize_Predicted_track_03_sub_label() {
	if (!isIinitialized_Predicted_track_03_sub_label){
		int i;
		for (i=0; i < 0; i++){
			Predicted_track_03_sub_label[i] = i+1;
		}
		isIinitialized_Predicted_track_03_sub_label = true;
	}
}

void read_Predicted_track_03_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(Predicted_track_03_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = Predicted_track_03_sub_label[i];
			a = Predicted_track_03_sub_label[i+1];
			a = Predicted_track_03_sub_label[i+2];
			a = Predicted_track_03_sub_label[i+3];
			a = Predicted_track_03_sub_label[i+4];
			a = Predicted_track_03_sub_label[i+5];
			a = Predicted_track_03_sub_label[i+6];
			a = Predicted_track_03_sub_label[i+7];
			a = Predicted_track_03_sub_label[i+8];
			a = Predicted_track_03_sub_label[i+9];
		}
	
		for(;i<arraysize;i++){
			a = Predicted_track_03_sub_label[i];
		}

		(void)a;
	}
}

void write_Predicted_track_03_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(Predicted_track_03_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			Predicted_track_03_sub_label[i]   = 0x800A;
			Predicted_track_03_sub_label[i+1] = 0xAFFE;
			Predicted_track_03_sub_label[i+2] = 0xAFFE;
			Predicted_track_03_sub_label[i+3] = 0xAFFE;
			Predicted_track_03_sub_label[i+4] = 0xAFFE;
			Predicted_track_03_sub_label[i+5] = 0xAFFE;
			Predicted_track_03_sub_label[i+6] = 0xAFFE;
			Predicted_track_03_sub_label[i+7] = 0xAFFE;
			Predicted_track_03_sub_label[i+8] = 0xAFFE;
			Predicted_track_03_sub_label[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				Predicted_track_03_sub_label[i]=0xAFFE;
		}
	}
}

int Predicted_track_02_sub_label[0];

static bool isIinitialized_Predicted_track_02_sub_label = false;
void initialize_Predicted_track_02_sub_label() {
	if (!isIinitialized_Predicted_track_02_sub_label){
		int i;
		for (i=0; i < 0; i++){
			Predicted_track_02_sub_label[i] = i+1;
		}
		isIinitialized_Predicted_track_02_sub_label = true;
	}
}

void read_Predicted_track_02_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(Predicted_track_02_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = Predicted_track_02_sub_label[i];
			a = Predicted_track_02_sub_label[i+1];
			a = Predicted_track_02_sub_label[i+2];
			a = Predicted_track_02_sub_label[i+3];
			a = Predicted_track_02_sub_label[i+4];
			a = Predicted_track_02_sub_label[i+5];
			a = Predicted_track_02_sub_label[i+6];
			a = Predicted_track_02_sub_label[i+7];
			a = Predicted_track_02_sub_label[i+8];
			a = Predicted_track_02_sub_label[i+9];
		}
	
		for(;i<arraysize;i++){
			a = Predicted_track_02_sub_label[i];
		}

		(void)a;
	}
}

void write_Predicted_track_02_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(Predicted_track_02_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			Predicted_track_02_sub_label[i]   = 0x800A;
			Predicted_track_02_sub_label[i+1] = 0xAFFE;
			Predicted_track_02_sub_label[i+2] = 0xAFFE;
			Predicted_track_02_sub_label[i+3] = 0xAFFE;
			Predicted_track_02_sub_label[i+4] = 0xAFFE;
			Predicted_track_02_sub_label[i+5] = 0xAFFE;
			Predicted_track_02_sub_label[i+6] = 0xAFFE;
			Predicted_track_02_sub_label[i+7] = 0xAFFE;
			Predicted_track_02_sub_label[i+8] = 0xAFFE;
			Predicted_track_02_sub_label[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				Predicted_track_02_sub_label[i]=0xAFFE;
		}
	}
}

int Predicted_track_07_sub_label[0];

static bool isIinitialized_Predicted_track_07_sub_label = false;
void initialize_Predicted_track_07_sub_label() {
	if (!isIinitialized_Predicted_track_07_sub_label){
		int i;
		for (i=0; i < 0; i++){
			Predicted_track_07_sub_label[i] = i+1;
		}
		isIinitialized_Predicted_track_07_sub_label = true;
	}
}

void read_Predicted_track_07_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(Predicted_track_07_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = Predicted_track_07_sub_label[i];
			a = Predicted_track_07_sub_label[i+1];
			a = Predicted_track_07_sub_label[i+2];
			a = Predicted_track_07_sub_label[i+3];
			a = Predicted_track_07_sub_label[i+4];
			a = Predicted_track_07_sub_label[i+5];
			a = Predicted_track_07_sub_label[i+6];
			a = Predicted_track_07_sub_label[i+7];
			a = Predicted_track_07_sub_label[i+8];
			a = Predicted_track_07_sub_label[i+9];
		}
	
		for(;i<arraysize;i++){
			a = Predicted_track_07_sub_label[i];
		}

		(void)a;
	}
}

void write_Predicted_track_07_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(Predicted_track_07_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			Predicted_track_07_sub_label[i]   = 0x800A;
			Predicted_track_07_sub_label[i+1] = 0xAFFE;
			Predicted_track_07_sub_label[i+2] = 0xAFFE;
			Predicted_track_07_sub_label[i+3] = 0xAFFE;
			Predicted_track_07_sub_label[i+4] = 0xAFFE;
			Predicted_track_07_sub_label[i+5] = 0xAFFE;
			Predicted_track_07_sub_label[i+6] = 0xAFFE;
			Predicted_track_07_sub_label[i+7] = 0xAFFE;
			Predicted_track_07_sub_label[i+8] = 0xAFFE;
			Predicted_track_07_sub_label[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				Predicted_track_07_sub_label[i]=0xAFFE;
		}
	}
}

int Predicted_track_05_sub_label[0];

static bool isIinitialized_Predicted_track_05_sub_label = false;
void initialize_Predicted_track_05_sub_label() {
	if (!isIinitialized_Predicted_track_05_sub_label){
		int i;
		for (i=0; i < 0; i++){
			Predicted_track_05_sub_label[i] = i+1;
		}
		isIinitialized_Predicted_track_05_sub_label = true;
	}
}

void read_Predicted_track_05_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(Predicted_track_05_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = Predicted_track_05_sub_label[i];
			a = Predicted_track_05_sub_label[i+1];
			a = Predicted_track_05_sub_label[i+2];
			a = Predicted_track_05_sub_label[i+3];
			a = Predicted_track_05_sub_label[i+4];
			a = Predicted_track_05_sub_label[i+5];
			a = Predicted_track_05_sub_label[i+6];
			a = Predicted_track_05_sub_label[i+7];
			a = Predicted_track_05_sub_label[i+8];
			a = Predicted_track_05_sub_label[i+9];
		}
	
		for(;i<arraysize;i++){
			a = Predicted_track_05_sub_label[i];
		}

		(void)a;
	}
}

void write_Predicted_track_05_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(Predicted_track_05_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			Predicted_track_05_sub_label[i]   = 0x800A;
			Predicted_track_05_sub_label[i+1] = 0xAFFE;
			Predicted_track_05_sub_label[i+2] = 0xAFFE;
			Predicted_track_05_sub_label[i+3] = 0xAFFE;
			Predicted_track_05_sub_label[i+4] = 0xAFFE;
			Predicted_track_05_sub_label[i+5] = 0xAFFE;
			Predicted_track_05_sub_label[i+6] = 0xAFFE;
			Predicted_track_05_sub_label[i+7] = 0xAFFE;
			Predicted_track_05_sub_label[i+8] = 0xAFFE;
			Predicted_track_05_sub_label[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				Predicted_track_05_sub_label[i]=0xAFFE;
		}
	}
}

int Predicted_track_00_sub_label[0];

static bool isIinitialized_Predicted_track_00_sub_label = false;
void initialize_Predicted_track_00_sub_label() {
	if (!isIinitialized_Predicted_track_00_sub_label){
		int i;
		for (i=0; i < 0; i++){
			Predicted_track_00_sub_label[i] = i+1;
		}
		isIinitialized_Predicted_track_00_sub_label = true;
	}
}

void read_Predicted_track_00_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(Predicted_track_00_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = Predicted_track_00_sub_label[i];
			a = Predicted_track_00_sub_label[i+1];
			a = Predicted_track_00_sub_label[i+2];
			a = Predicted_track_00_sub_label[i+3];
			a = Predicted_track_00_sub_label[i+4];
			a = Predicted_track_00_sub_label[i+5];
			a = Predicted_track_00_sub_label[i+6];
			a = Predicted_track_00_sub_label[i+7];
			a = Predicted_track_00_sub_label[i+8];
			a = Predicted_track_00_sub_label[i+9];
		}
	
		for(;i<arraysize;i++){
			a = Predicted_track_00_sub_label[i];
		}

		(void)a;
	}
}

void write_Predicted_track_00_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(Predicted_track_00_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			Predicted_track_00_sub_label[i]   = 0x800A;
			Predicted_track_00_sub_label[i+1] = 0xAFFE;
			Predicted_track_00_sub_label[i+2] = 0xAFFE;
			Predicted_track_00_sub_label[i+3] = 0xAFFE;
			Predicted_track_00_sub_label[i+4] = 0xAFFE;
			Predicted_track_00_sub_label[i+5] = 0xAFFE;
			Predicted_track_00_sub_label[i+6] = 0xAFFE;
			Predicted_track_00_sub_label[i+7] = 0xAFFE;
			Predicted_track_00_sub_label[i+8] = 0xAFFE;
			Predicted_track_00_sub_label[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				Predicted_track_00_sub_label[i]=0xAFFE;
		}
	}
}

int Track_objects[0];

static bool isIinitialized_Track_objects = false;
void initialize_Track_objects() {
	if (!isIinitialized_Track_objects){
		int i;
		for (i=0; i < 0; i++){
			Track_objects[i] = i+1;
		}
		isIinitialized_Track_objects = true;
	}
}

void read_Track_objects(int labelAccessStatistics) {
	int numberOfBytes = 1;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(Track_objects) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = Track_objects[i];
			a = Track_objects[i+1];
			a = Track_objects[i+2];
			a = Track_objects[i+3];
			a = Track_objects[i+4];
			a = Track_objects[i+5];
			a = Track_objects[i+6];
			a = Track_objects[i+7];
			a = Track_objects[i+8];
			a = Track_objects[i+9];
		}
	
		for(;i<arraysize;i++){
			a = Track_objects[i];
		}

		(void)a;
	}
}

void write_Track_objects(int labelAccessStatistics) {
	int numberOfBytes = 1;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(Track_objects) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			Track_objects[i]   = 0x800A;
			Track_objects[i+1] = 0xAFFE;
			Track_objects[i+2] = 0xAFFE;
			Track_objects[i+3] = 0xAFFE;
			Track_objects[i+4] = 0xAFFE;
			Track_objects[i+5] = 0xAFFE;
			Track_objects[i+6] = 0xAFFE;
			Track_objects[i+7] = 0xAFFE;
			Track_objects[i+8] = 0xAFFE;
			Track_objects[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				Track_objects[i]=0xAFFE;
		}
	}
}

int Tracked_object_label_N_sub_label[0];

static bool isIinitialized_Tracked_object_label_N_sub_label = false;
void initialize_Tracked_object_label_N_sub_label() {
	if (!isIinitialized_Tracked_object_label_N_sub_label){
		int i;
		for (i=0; i < 0; i++){
			Tracked_object_label_N_sub_label[i] = i+1;
		}
		isIinitialized_Tracked_object_label_N_sub_label = true;
	}
}

void read_Tracked_object_label_N_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(Tracked_object_label_N_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = Tracked_object_label_N_sub_label[i];
			a = Tracked_object_label_N_sub_label[i+1];
			a = Tracked_object_label_N_sub_label[i+2];
			a = Tracked_object_label_N_sub_label[i+3];
			a = Tracked_object_label_N_sub_label[i+4];
			a = Tracked_object_label_N_sub_label[i+5];
			a = Tracked_object_label_N_sub_label[i+6];
			a = Tracked_object_label_N_sub_label[i+7];
			a = Tracked_object_label_N_sub_label[i+8];
			a = Tracked_object_label_N_sub_label[i+9];
		}
	
		for(;i<arraysize;i++){
			a = Tracked_object_label_N_sub_label[i];
		}

		(void)a;
	}
}

void write_Tracked_object_label_N_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(Tracked_object_label_N_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			Tracked_object_label_N_sub_label[i]   = 0x800A;
			Tracked_object_label_N_sub_label[i+1] = 0xAFFE;
			Tracked_object_label_N_sub_label[i+2] = 0xAFFE;
			Tracked_object_label_N_sub_label[i+3] = 0xAFFE;
			Tracked_object_label_N_sub_label[i+4] = 0xAFFE;
			Tracked_object_label_N_sub_label[i+5] = 0xAFFE;
			Tracked_object_label_N_sub_label[i+6] = 0xAFFE;
			Tracked_object_label_N_sub_label[i+7] = 0xAFFE;
			Tracked_object_label_N_sub_label[i+8] = 0xAFFE;
			Tracked_object_label_N_sub_label[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				Tracked_object_label_N_sub_label[i]=0xAFFE;
		}
	}
}

int UKF_index_00[0];

static bool isIinitialized_UKF_index_00 = false;
void initialize_UKF_index_00() {
	if (!isIinitialized_UKF_index_00){
		int i;
		for (i=0; i < 0; i++){
			UKF_index_00[i] = i+1;
		}
		isIinitialized_UKF_index_00 = true;
	}
}

void read_UKF_index_00(int labelAccessStatistics) {
	int numberOfBytes = 1;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(UKF_index_00) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = UKF_index_00[i];
			a = UKF_index_00[i+1];
			a = UKF_index_00[i+2];
			a = UKF_index_00[i+3];
			a = UKF_index_00[i+4];
			a = UKF_index_00[i+5];
			a = UKF_index_00[i+6];
			a = UKF_index_00[i+7];
			a = UKF_index_00[i+8];
			a = UKF_index_00[i+9];
		}
	
		for(;i<arraysize;i++){
			a = UKF_index_00[i];
		}

		(void)a;
	}
}

void write_UKF_index_00(int labelAccessStatistics) {
	int numberOfBytes = 1;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(UKF_index_00) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			UKF_index_00[i]   = 0x800A;
			UKF_index_00[i+1] = 0xAFFE;
			UKF_index_00[i+2] = 0xAFFE;
			UKF_index_00[i+3] = 0xAFFE;
			UKF_index_00[i+4] = 0xAFFE;
			UKF_index_00[i+5] = 0xAFFE;
			UKF_index_00[i+6] = 0xAFFE;
			UKF_index_00[i+7] = 0xAFFE;
			UKF_index_00[i+8] = 0xAFFE;
			UKF_index_00[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				UKF_index_00[i]=0xAFFE;
		}
	}
}

int UKF_index_08[0];

static bool isIinitialized_UKF_index_08 = false;
void initialize_UKF_index_08() {
	if (!isIinitialized_UKF_index_08){
		int i;
		for (i=0; i < 0; i++){
			UKF_index_08[i] = i+1;
		}
		isIinitialized_UKF_index_08 = true;
	}
}

void read_UKF_index_08(int labelAccessStatistics) {
	int numberOfBytes = 1;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(UKF_index_08) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = UKF_index_08[i];
			a = UKF_index_08[i+1];
			a = UKF_index_08[i+2];
			a = UKF_index_08[i+3];
			a = UKF_index_08[i+4];
			a = UKF_index_08[i+5];
			a = UKF_index_08[i+6];
			a = UKF_index_08[i+7];
			a = UKF_index_08[i+8];
			a = UKF_index_08[i+9];
		}
	
		for(;i<arraysize;i++){
			a = UKF_index_08[i];
		}

		(void)a;
	}
}

void write_UKF_index_08(int labelAccessStatistics) {
	int numberOfBytes = 1;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(UKF_index_08) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			UKF_index_08[i]   = 0x800A;
			UKF_index_08[i+1] = 0xAFFE;
			UKF_index_08[i+2] = 0xAFFE;
			UKF_index_08[i+3] = 0xAFFE;
			UKF_index_08[i+4] = 0xAFFE;
			UKF_index_08[i+5] = 0xAFFE;
			UKF_index_08[i+6] = 0xAFFE;
			UKF_index_08[i+7] = 0xAFFE;
			UKF_index_08[i+8] = 0xAFFE;
			UKF_index_08[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				UKF_index_08[i]=0xAFFE;
		}
	}
}

int UKF_index_15[0];

static bool isIinitialized_UKF_index_15 = false;
void initialize_UKF_index_15() {
	if (!isIinitialized_UKF_index_15){
		int i;
		for (i=0; i < 0; i++){
			UKF_index_15[i] = i+1;
		}
		isIinitialized_UKF_index_15 = true;
	}
}

void read_UKF_index_15(int labelAccessStatistics) {
	int numberOfBytes = 1;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(UKF_index_15) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = UKF_index_15[i];
			a = UKF_index_15[i+1];
			a = UKF_index_15[i+2];
			a = UKF_index_15[i+3];
			a = UKF_index_15[i+4];
			a = UKF_index_15[i+5];
			a = UKF_index_15[i+6];
			a = UKF_index_15[i+7];
			a = UKF_index_15[i+8];
			a = UKF_index_15[i+9];
		}
	
		for(;i<arraysize;i++){
			a = UKF_index_15[i];
		}

		(void)a;
	}
}

void write_UKF_index_15(int labelAccessStatistics) {
	int numberOfBytes = 1;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(UKF_index_15) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			UKF_index_15[i]   = 0x800A;
			UKF_index_15[i+1] = 0xAFFE;
			UKF_index_15[i+2] = 0xAFFE;
			UKF_index_15[i+3] = 0xAFFE;
			UKF_index_15[i+4] = 0xAFFE;
			UKF_index_15[i+5] = 0xAFFE;
			UKF_index_15[i+6] = 0xAFFE;
			UKF_index_15[i+7] = 0xAFFE;
			UKF_index_15[i+8] = 0xAFFE;
			UKF_index_15[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				UKF_index_15[i]=0xAFFE;
		}
	}
}

int UKF_index_01[0];

static bool isIinitialized_UKF_index_01 = false;
void initialize_UKF_index_01() {
	if (!isIinitialized_UKF_index_01){
		int i;
		for (i=0; i < 0; i++){
			UKF_index_01[i] = i+1;
		}
		isIinitialized_UKF_index_01 = true;
	}
}

void read_UKF_index_01(int labelAccessStatistics) {
	int numberOfBytes = 1;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(UKF_index_01) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = UKF_index_01[i];
			a = UKF_index_01[i+1];
			a = UKF_index_01[i+2];
			a = UKF_index_01[i+3];
			a = UKF_index_01[i+4];
			a = UKF_index_01[i+5];
			a = UKF_index_01[i+6];
			a = UKF_index_01[i+7];
			a = UKF_index_01[i+8];
			a = UKF_index_01[i+9];
		}
	
		for(;i<arraysize;i++){
			a = UKF_index_01[i];
		}

		(void)a;
	}
}

void write_UKF_index_01(int labelAccessStatistics) {
	int numberOfBytes = 1;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(UKF_index_01) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			UKF_index_01[i]   = 0x800A;
			UKF_index_01[i+1] = 0xAFFE;
			UKF_index_01[i+2] = 0xAFFE;
			UKF_index_01[i+3] = 0xAFFE;
			UKF_index_01[i+4] = 0xAFFE;
			UKF_index_01[i+5] = 0xAFFE;
			UKF_index_01[i+6] = 0xAFFE;
			UKF_index_01[i+7] = 0xAFFE;
			UKF_index_01[i+8] = 0xAFFE;
			UKF_index_01[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				UKF_index_01[i]=0xAFFE;
		}
	}
}

int UKF_index_02[0];

static bool isIinitialized_UKF_index_02 = false;
void initialize_UKF_index_02() {
	if (!isIinitialized_UKF_index_02){
		int i;
		for (i=0; i < 0; i++){
			UKF_index_02[i] = i+1;
		}
		isIinitialized_UKF_index_02 = true;
	}
}

void read_UKF_index_02(int labelAccessStatistics) {
	int numberOfBytes = 1;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(UKF_index_02) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = UKF_index_02[i];
			a = UKF_index_02[i+1];
			a = UKF_index_02[i+2];
			a = UKF_index_02[i+3];
			a = UKF_index_02[i+4];
			a = UKF_index_02[i+5];
			a = UKF_index_02[i+6];
			a = UKF_index_02[i+7];
			a = UKF_index_02[i+8];
			a = UKF_index_02[i+9];
		}
	
		for(;i<arraysize;i++){
			a = UKF_index_02[i];
		}

		(void)a;
	}
}

void write_UKF_index_02(int labelAccessStatistics) {
	int numberOfBytes = 1;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(UKF_index_02) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			UKF_index_02[i]   = 0x800A;
			UKF_index_02[i+1] = 0xAFFE;
			UKF_index_02[i+2] = 0xAFFE;
			UKF_index_02[i+3] = 0xAFFE;
			UKF_index_02[i+4] = 0xAFFE;
			UKF_index_02[i+5] = 0xAFFE;
			UKF_index_02[i+6] = 0xAFFE;
			UKF_index_02[i+7] = 0xAFFE;
			UKF_index_02[i+8] = 0xAFFE;
			UKF_index_02[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				UKF_index_02[i]=0xAFFE;
		}
	}
}

int UKF_index_03[0];

static bool isIinitialized_UKF_index_03 = false;
void initialize_UKF_index_03() {
	if (!isIinitialized_UKF_index_03){
		int i;
		for (i=0; i < 0; i++){
			UKF_index_03[i] = i+1;
		}
		isIinitialized_UKF_index_03 = true;
	}
}

void read_UKF_index_03(int labelAccessStatistics) {
	int numberOfBytes = 1;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(UKF_index_03) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = UKF_index_03[i];
			a = UKF_index_03[i+1];
			a = UKF_index_03[i+2];
			a = UKF_index_03[i+3];
			a = UKF_index_03[i+4];
			a = UKF_index_03[i+5];
			a = UKF_index_03[i+6];
			a = UKF_index_03[i+7];
			a = UKF_index_03[i+8];
			a = UKF_index_03[i+9];
		}
	
		for(;i<arraysize;i++){
			a = UKF_index_03[i];
		}

		(void)a;
	}
}

void write_UKF_index_03(int labelAccessStatistics) {
	int numberOfBytes = 1;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(UKF_index_03) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			UKF_index_03[i]   = 0x800A;
			UKF_index_03[i+1] = 0xAFFE;
			UKF_index_03[i+2] = 0xAFFE;
			UKF_index_03[i+3] = 0xAFFE;
			UKF_index_03[i+4] = 0xAFFE;
			UKF_index_03[i+5] = 0xAFFE;
			UKF_index_03[i+6] = 0xAFFE;
			UKF_index_03[i+7] = 0xAFFE;
			UKF_index_03[i+8] = 0xAFFE;
			UKF_index_03[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				UKF_index_03[i]=0xAFFE;
		}
	}
}

int UKF_index_04[0];

static bool isIinitialized_UKF_index_04 = false;
void initialize_UKF_index_04() {
	if (!isIinitialized_UKF_index_04){
		int i;
		for (i=0; i < 0; i++){
			UKF_index_04[i] = i+1;
		}
		isIinitialized_UKF_index_04 = true;
	}
}

void read_UKF_index_04(int labelAccessStatistics) {
	int numberOfBytes = 1;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(UKF_index_04) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = UKF_index_04[i];
			a = UKF_index_04[i+1];
			a = UKF_index_04[i+2];
			a = UKF_index_04[i+3];
			a = UKF_index_04[i+4];
			a = UKF_index_04[i+5];
			a = UKF_index_04[i+6];
			a = UKF_index_04[i+7];
			a = UKF_index_04[i+8];
			a = UKF_index_04[i+9];
		}
	
		for(;i<arraysize;i++){
			a = UKF_index_04[i];
		}

		(void)a;
	}
}

void write_UKF_index_04(int labelAccessStatistics) {
	int numberOfBytes = 1;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(UKF_index_04) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			UKF_index_04[i]   = 0x800A;
			UKF_index_04[i+1] = 0xAFFE;
			UKF_index_04[i+2] = 0xAFFE;
			UKF_index_04[i+3] = 0xAFFE;
			UKF_index_04[i+4] = 0xAFFE;
			UKF_index_04[i+5] = 0xAFFE;
			UKF_index_04[i+6] = 0xAFFE;
			UKF_index_04[i+7] = 0xAFFE;
			UKF_index_04[i+8] = 0xAFFE;
			UKF_index_04[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				UKF_index_04[i]=0xAFFE;
		}
	}
}

int UKF_index_05[0];

static bool isIinitialized_UKF_index_05 = false;
void initialize_UKF_index_05() {
	if (!isIinitialized_UKF_index_05){
		int i;
		for (i=0; i < 0; i++){
			UKF_index_05[i] = i+1;
		}
		isIinitialized_UKF_index_05 = true;
	}
}

void read_UKF_index_05(int labelAccessStatistics) {
	int numberOfBytes = 1;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(UKF_index_05) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = UKF_index_05[i];
			a = UKF_index_05[i+1];
			a = UKF_index_05[i+2];
			a = UKF_index_05[i+3];
			a = UKF_index_05[i+4];
			a = UKF_index_05[i+5];
			a = UKF_index_05[i+6];
			a = UKF_index_05[i+7];
			a = UKF_index_05[i+8];
			a = UKF_index_05[i+9];
		}
	
		for(;i<arraysize;i++){
			a = UKF_index_05[i];
		}

		(void)a;
	}
}

void write_UKF_index_05(int labelAccessStatistics) {
	int numberOfBytes = 1;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(UKF_index_05) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			UKF_index_05[i]   = 0x800A;
			UKF_index_05[i+1] = 0xAFFE;
			UKF_index_05[i+2] = 0xAFFE;
			UKF_index_05[i+3] = 0xAFFE;
			UKF_index_05[i+4] = 0xAFFE;
			UKF_index_05[i+5] = 0xAFFE;
			UKF_index_05[i+6] = 0xAFFE;
			UKF_index_05[i+7] = 0xAFFE;
			UKF_index_05[i+8] = 0xAFFE;
			UKF_index_05[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				UKF_index_05[i]=0xAFFE;
		}
	}
}

int UKF_index_06[0];

static bool isIinitialized_UKF_index_06 = false;
void initialize_UKF_index_06() {
	if (!isIinitialized_UKF_index_06){
		int i;
		for (i=0; i < 0; i++){
			UKF_index_06[i] = i+1;
		}
		isIinitialized_UKF_index_06 = true;
	}
}

void read_UKF_index_06(int labelAccessStatistics) {
	int numberOfBytes = 1;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(UKF_index_06) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = UKF_index_06[i];
			a = UKF_index_06[i+1];
			a = UKF_index_06[i+2];
			a = UKF_index_06[i+3];
			a = UKF_index_06[i+4];
			a = UKF_index_06[i+5];
			a = UKF_index_06[i+6];
			a = UKF_index_06[i+7];
			a = UKF_index_06[i+8];
			a = UKF_index_06[i+9];
		}
	
		for(;i<arraysize;i++){
			a = UKF_index_06[i];
		}

		(void)a;
	}
}

void write_UKF_index_06(int labelAccessStatistics) {
	int numberOfBytes = 1;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(UKF_index_06) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			UKF_index_06[i]   = 0x800A;
			UKF_index_06[i+1] = 0xAFFE;
			UKF_index_06[i+2] = 0xAFFE;
			UKF_index_06[i+3] = 0xAFFE;
			UKF_index_06[i+4] = 0xAFFE;
			UKF_index_06[i+5] = 0xAFFE;
			UKF_index_06[i+6] = 0xAFFE;
			UKF_index_06[i+7] = 0xAFFE;
			UKF_index_06[i+8] = 0xAFFE;
			UKF_index_06[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				UKF_index_06[i]=0xAFFE;
		}
	}
}

int UKF_index_07[0];

static bool isIinitialized_UKF_index_07 = false;
void initialize_UKF_index_07() {
	if (!isIinitialized_UKF_index_07){
		int i;
		for (i=0; i < 0; i++){
			UKF_index_07[i] = i+1;
		}
		isIinitialized_UKF_index_07 = true;
	}
}

void read_UKF_index_07(int labelAccessStatistics) {
	int numberOfBytes = 1;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(UKF_index_07) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = UKF_index_07[i];
			a = UKF_index_07[i+1];
			a = UKF_index_07[i+2];
			a = UKF_index_07[i+3];
			a = UKF_index_07[i+4];
			a = UKF_index_07[i+5];
			a = UKF_index_07[i+6];
			a = UKF_index_07[i+7];
			a = UKF_index_07[i+8];
			a = UKF_index_07[i+9];
		}
	
		for(;i<arraysize;i++){
			a = UKF_index_07[i];
		}

		(void)a;
	}
}

void write_UKF_index_07(int labelAccessStatistics) {
	int numberOfBytes = 1;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(UKF_index_07) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			UKF_index_07[i]   = 0x800A;
			UKF_index_07[i+1] = 0xAFFE;
			UKF_index_07[i+2] = 0xAFFE;
			UKF_index_07[i+3] = 0xAFFE;
			UKF_index_07[i+4] = 0xAFFE;
			UKF_index_07[i+5] = 0xAFFE;
			UKF_index_07[i+6] = 0xAFFE;
			UKF_index_07[i+7] = 0xAFFE;
			UKF_index_07[i+8] = 0xAFFE;
			UKF_index_07[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				UKF_index_07[i]=0xAFFE;
		}
	}
}

int UKF_index_09[0];

static bool isIinitialized_UKF_index_09 = false;
void initialize_UKF_index_09() {
	if (!isIinitialized_UKF_index_09){
		int i;
		for (i=0; i < 0; i++){
			UKF_index_09[i] = i+1;
		}
		isIinitialized_UKF_index_09 = true;
	}
}

void read_UKF_index_09(int labelAccessStatistics) {
	int numberOfBytes = 1;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(UKF_index_09) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = UKF_index_09[i];
			a = UKF_index_09[i+1];
			a = UKF_index_09[i+2];
			a = UKF_index_09[i+3];
			a = UKF_index_09[i+4];
			a = UKF_index_09[i+5];
			a = UKF_index_09[i+6];
			a = UKF_index_09[i+7];
			a = UKF_index_09[i+8];
			a = UKF_index_09[i+9];
		}
	
		for(;i<arraysize;i++){
			a = UKF_index_09[i];
		}

		(void)a;
	}
}

void write_UKF_index_09(int labelAccessStatistics) {
	int numberOfBytes = 1;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(UKF_index_09) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			UKF_index_09[i]   = 0x800A;
			UKF_index_09[i+1] = 0xAFFE;
			UKF_index_09[i+2] = 0xAFFE;
			UKF_index_09[i+3] = 0xAFFE;
			UKF_index_09[i+4] = 0xAFFE;
			UKF_index_09[i+5] = 0xAFFE;
			UKF_index_09[i+6] = 0xAFFE;
			UKF_index_09[i+7] = 0xAFFE;
			UKF_index_09[i+8] = 0xAFFE;
			UKF_index_09[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				UKF_index_09[i]=0xAFFE;
		}
	}
}

int UKF_index_10[0];

static bool isIinitialized_UKF_index_10 = false;
void initialize_UKF_index_10() {
	if (!isIinitialized_UKF_index_10){
		int i;
		for (i=0; i < 0; i++){
			UKF_index_10[i] = i+1;
		}
		isIinitialized_UKF_index_10 = true;
	}
}

void read_UKF_index_10(int labelAccessStatistics) {
	int numberOfBytes = 1;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(UKF_index_10) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = UKF_index_10[i];
			a = UKF_index_10[i+1];
			a = UKF_index_10[i+2];
			a = UKF_index_10[i+3];
			a = UKF_index_10[i+4];
			a = UKF_index_10[i+5];
			a = UKF_index_10[i+6];
			a = UKF_index_10[i+7];
			a = UKF_index_10[i+8];
			a = UKF_index_10[i+9];
		}
	
		for(;i<arraysize;i++){
			a = UKF_index_10[i];
		}

		(void)a;
	}
}

void write_UKF_index_10(int labelAccessStatistics) {
	int numberOfBytes = 1;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(UKF_index_10) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			UKF_index_10[i]   = 0x800A;
			UKF_index_10[i+1] = 0xAFFE;
			UKF_index_10[i+2] = 0xAFFE;
			UKF_index_10[i+3] = 0xAFFE;
			UKF_index_10[i+4] = 0xAFFE;
			UKF_index_10[i+5] = 0xAFFE;
			UKF_index_10[i+6] = 0xAFFE;
			UKF_index_10[i+7] = 0xAFFE;
			UKF_index_10[i+8] = 0xAFFE;
			UKF_index_10[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				UKF_index_10[i]=0xAFFE;
		}
	}
}

int UKF_index_11[0];

static bool isIinitialized_UKF_index_11 = false;
void initialize_UKF_index_11() {
	if (!isIinitialized_UKF_index_11){
		int i;
		for (i=0; i < 0; i++){
			UKF_index_11[i] = i+1;
		}
		isIinitialized_UKF_index_11 = true;
	}
}

void read_UKF_index_11(int labelAccessStatistics) {
	int numberOfBytes = 1;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(UKF_index_11) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = UKF_index_11[i];
			a = UKF_index_11[i+1];
			a = UKF_index_11[i+2];
			a = UKF_index_11[i+3];
			a = UKF_index_11[i+4];
			a = UKF_index_11[i+5];
			a = UKF_index_11[i+6];
			a = UKF_index_11[i+7];
			a = UKF_index_11[i+8];
			a = UKF_index_11[i+9];
		}
	
		for(;i<arraysize;i++){
			a = UKF_index_11[i];
		}

		(void)a;
	}
}

void write_UKF_index_11(int labelAccessStatistics) {
	int numberOfBytes = 1;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(UKF_index_11) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			UKF_index_11[i]   = 0x800A;
			UKF_index_11[i+1] = 0xAFFE;
			UKF_index_11[i+2] = 0xAFFE;
			UKF_index_11[i+3] = 0xAFFE;
			UKF_index_11[i+4] = 0xAFFE;
			UKF_index_11[i+5] = 0xAFFE;
			UKF_index_11[i+6] = 0xAFFE;
			UKF_index_11[i+7] = 0xAFFE;
			UKF_index_11[i+8] = 0xAFFE;
			UKF_index_11[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				UKF_index_11[i]=0xAFFE;
		}
	}
}

int UKF_index_12[0];

static bool isIinitialized_UKF_index_12 = false;
void initialize_UKF_index_12() {
	if (!isIinitialized_UKF_index_12){
		int i;
		for (i=0; i < 0; i++){
			UKF_index_12[i] = i+1;
		}
		isIinitialized_UKF_index_12 = true;
	}
}

void read_UKF_index_12(int labelAccessStatistics) {
	int numberOfBytes = 1;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(UKF_index_12) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = UKF_index_12[i];
			a = UKF_index_12[i+1];
			a = UKF_index_12[i+2];
			a = UKF_index_12[i+3];
			a = UKF_index_12[i+4];
			a = UKF_index_12[i+5];
			a = UKF_index_12[i+6];
			a = UKF_index_12[i+7];
			a = UKF_index_12[i+8];
			a = UKF_index_12[i+9];
		}
	
		for(;i<arraysize;i++){
			a = UKF_index_12[i];
		}

		(void)a;
	}
}

void write_UKF_index_12(int labelAccessStatistics) {
	int numberOfBytes = 1;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(UKF_index_12) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			UKF_index_12[i]   = 0x800A;
			UKF_index_12[i+1] = 0xAFFE;
			UKF_index_12[i+2] = 0xAFFE;
			UKF_index_12[i+3] = 0xAFFE;
			UKF_index_12[i+4] = 0xAFFE;
			UKF_index_12[i+5] = 0xAFFE;
			UKF_index_12[i+6] = 0xAFFE;
			UKF_index_12[i+7] = 0xAFFE;
			UKF_index_12[i+8] = 0xAFFE;
			UKF_index_12[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				UKF_index_12[i]=0xAFFE;
		}
	}
}

int UKF_index_13[0];

static bool isIinitialized_UKF_index_13 = false;
void initialize_UKF_index_13() {
	if (!isIinitialized_UKF_index_13){
		int i;
		for (i=0; i < 0; i++){
			UKF_index_13[i] = i+1;
		}
		isIinitialized_UKF_index_13 = true;
	}
}

void read_UKF_index_13(int labelAccessStatistics) {
	int numberOfBytes = 1;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(UKF_index_13) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = UKF_index_13[i];
			a = UKF_index_13[i+1];
			a = UKF_index_13[i+2];
			a = UKF_index_13[i+3];
			a = UKF_index_13[i+4];
			a = UKF_index_13[i+5];
			a = UKF_index_13[i+6];
			a = UKF_index_13[i+7];
			a = UKF_index_13[i+8];
			a = UKF_index_13[i+9];
		}
	
		for(;i<arraysize;i++){
			a = UKF_index_13[i];
		}

		(void)a;
	}
}

void write_UKF_index_13(int labelAccessStatistics) {
	int numberOfBytes = 1;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(UKF_index_13) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			UKF_index_13[i]   = 0x800A;
			UKF_index_13[i+1] = 0xAFFE;
			UKF_index_13[i+2] = 0xAFFE;
			UKF_index_13[i+3] = 0xAFFE;
			UKF_index_13[i+4] = 0xAFFE;
			UKF_index_13[i+5] = 0xAFFE;
			UKF_index_13[i+6] = 0xAFFE;
			UKF_index_13[i+7] = 0xAFFE;
			UKF_index_13[i+8] = 0xAFFE;
			UKF_index_13[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				UKF_index_13[i]=0xAFFE;
		}
	}
}

int UKF_index_14[0];

static bool isIinitialized_UKF_index_14 = false;
void initialize_UKF_index_14() {
	if (!isIinitialized_UKF_index_14){
		int i;
		for (i=0; i < 0; i++){
			UKF_index_14[i] = i+1;
		}
		isIinitialized_UKF_index_14 = true;
	}
}

void read_UKF_index_14(int labelAccessStatistics) {
	int numberOfBytes = 1;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(UKF_index_14) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = UKF_index_14[i];
			a = UKF_index_14[i+1];
			a = UKF_index_14[i+2];
			a = UKF_index_14[i+3];
			a = UKF_index_14[i+4];
			a = UKF_index_14[i+5];
			a = UKF_index_14[i+6];
			a = UKF_index_14[i+7];
			a = UKF_index_14[i+8];
			a = UKF_index_14[i+9];
		}
	
		for(;i<arraysize;i++){
			a = UKF_index_14[i];
		}

		(void)a;
	}
}

void write_UKF_index_14(int labelAccessStatistics) {
	int numberOfBytes = 1;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(UKF_index_14) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			UKF_index_14[i]   = 0x800A;
			UKF_index_14[i+1] = 0xAFFE;
			UKF_index_14[i+2] = 0xAFFE;
			UKF_index_14[i+3] = 0xAFFE;
			UKF_index_14[i+4] = 0xAFFE;
			UKF_index_14[i+5] = 0xAFFE;
			UKF_index_14[i+6] = 0xAFFE;
			UKF_index_14[i+7] = 0xAFFE;
			UKF_index_14[i+8] = 0xAFFE;
			UKF_index_14[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				UKF_index_14[i]=0xAFFE;
		}
	}
}

int UKF_index_16[0];

static bool isIinitialized_UKF_index_16 = false;
void initialize_UKF_index_16() {
	if (!isIinitialized_UKF_index_16){
		int i;
		for (i=0; i < 0; i++){
			UKF_index_16[i] = i+1;
		}
		isIinitialized_UKF_index_16 = true;
	}
}

void read_UKF_index_16(int labelAccessStatistics) {
	int numberOfBytes = 1;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(UKF_index_16) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = UKF_index_16[i];
			a = UKF_index_16[i+1];
			a = UKF_index_16[i+2];
			a = UKF_index_16[i+3];
			a = UKF_index_16[i+4];
			a = UKF_index_16[i+5];
			a = UKF_index_16[i+6];
			a = UKF_index_16[i+7];
			a = UKF_index_16[i+8];
			a = UKF_index_16[i+9];
		}
	
		for(;i<arraysize;i++){
			a = UKF_index_16[i];
		}

		(void)a;
	}
}

void write_UKF_index_16(int labelAccessStatistics) {
	int numberOfBytes = 1;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(UKF_index_16) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			UKF_index_16[i]   = 0x800A;
			UKF_index_16[i+1] = 0xAFFE;
			UKF_index_16[i+2] = 0xAFFE;
			UKF_index_16[i+3] = 0xAFFE;
			UKF_index_16[i+4] = 0xAFFE;
			UKF_index_16[i+5] = 0xAFFE;
			UKF_index_16[i+6] = 0xAFFE;
			UKF_index_16[i+7] = 0xAFFE;
			UKF_index_16[i+8] = 0xAFFE;
			UKF_index_16[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				UKF_index_16[i]=0xAFFE;
		}
	}
}

int UKF_index_24[0];

static bool isIinitialized_UKF_index_24 = false;
void initialize_UKF_index_24() {
	if (!isIinitialized_UKF_index_24){
		int i;
		for (i=0; i < 0; i++){
			UKF_index_24[i] = i+1;
		}
		isIinitialized_UKF_index_24 = true;
	}
}

void read_UKF_index_24(int labelAccessStatistics) {
	int numberOfBytes = 1;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(UKF_index_24) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = UKF_index_24[i];
			a = UKF_index_24[i+1];
			a = UKF_index_24[i+2];
			a = UKF_index_24[i+3];
			a = UKF_index_24[i+4];
			a = UKF_index_24[i+5];
			a = UKF_index_24[i+6];
			a = UKF_index_24[i+7];
			a = UKF_index_24[i+8];
			a = UKF_index_24[i+9];
		}
	
		for(;i<arraysize;i++){
			a = UKF_index_24[i];
		}

		(void)a;
	}
}

void write_UKF_index_24(int labelAccessStatistics) {
	int numberOfBytes = 1;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(UKF_index_24) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			UKF_index_24[i]   = 0x800A;
			UKF_index_24[i+1] = 0xAFFE;
			UKF_index_24[i+2] = 0xAFFE;
			UKF_index_24[i+3] = 0xAFFE;
			UKF_index_24[i+4] = 0xAFFE;
			UKF_index_24[i+5] = 0xAFFE;
			UKF_index_24[i+6] = 0xAFFE;
			UKF_index_24[i+7] = 0xAFFE;
			UKF_index_24[i+8] = 0xAFFE;
			UKF_index_24[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				UKF_index_24[i]=0xAFFE;
		}
	}
}

int UKF_index_31[0];

static bool isIinitialized_UKF_index_31 = false;
void initialize_UKF_index_31() {
	if (!isIinitialized_UKF_index_31){
		int i;
		for (i=0; i < 0; i++){
			UKF_index_31[i] = i+1;
		}
		isIinitialized_UKF_index_31 = true;
	}
}

void read_UKF_index_31(int labelAccessStatistics) {
	int numberOfBytes = 1;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(UKF_index_31) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = UKF_index_31[i];
			a = UKF_index_31[i+1];
			a = UKF_index_31[i+2];
			a = UKF_index_31[i+3];
			a = UKF_index_31[i+4];
			a = UKF_index_31[i+5];
			a = UKF_index_31[i+6];
			a = UKF_index_31[i+7];
			a = UKF_index_31[i+8];
			a = UKF_index_31[i+9];
		}
	
		for(;i<arraysize;i++){
			a = UKF_index_31[i];
		}

		(void)a;
	}
}

void write_UKF_index_31(int labelAccessStatistics) {
	int numberOfBytes = 1;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(UKF_index_31) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			UKF_index_31[i]   = 0x800A;
			UKF_index_31[i+1] = 0xAFFE;
			UKF_index_31[i+2] = 0xAFFE;
			UKF_index_31[i+3] = 0xAFFE;
			UKF_index_31[i+4] = 0xAFFE;
			UKF_index_31[i+5] = 0xAFFE;
			UKF_index_31[i+6] = 0xAFFE;
			UKF_index_31[i+7] = 0xAFFE;
			UKF_index_31[i+8] = 0xAFFE;
			UKF_index_31[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				UKF_index_31[i]=0xAFFE;
		}
	}
}

int UKF_index_17[0];

static bool isIinitialized_UKF_index_17 = false;
void initialize_UKF_index_17() {
	if (!isIinitialized_UKF_index_17){
		int i;
		for (i=0; i < 0; i++){
			UKF_index_17[i] = i+1;
		}
		isIinitialized_UKF_index_17 = true;
	}
}

void read_UKF_index_17(int labelAccessStatistics) {
	int numberOfBytes = 1;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(UKF_index_17) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = UKF_index_17[i];
			a = UKF_index_17[i+1];
			a = UKF_index_17[i+2];
			a = UKF_index_17[i+3];
			a = UKF_index_17[i+4];
			a = UKF_index_17[i+5];
			a = UKF_index_17[i+6];
			a = UKF_index_17[i+7];
			a = UKF_index_17[i+8];
			a = UKF_index_17[i+9];
		}
	
		for(;i<arraysize;i++){
			a = UKF_index_17[i];
		}

		(void)a;
	}
}

void write_UKF_index_17(int labelAccessStatistics) {
	int numberOfBytes = 1;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(UKF_index_17) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			UKF_index_17[i]   = 0x800A;
			UKF_index_17[i+1] = 0xAFFE;
			UKF_index_17[i+2] = 0xAFFE;
			UKF_index_17[i+3] = 0xAFFE;
			UKF_index_17[i+4] = 0xAFFE;
			UKF_index_17[i+5] = 0xAFFE;
			UKF_index_17[i+6] = 0xAFFE;
			UKF_index_17[i+7] = 0xAFFE;
			UKF_index_17[i+8] = 0xAFFE;
			UKF_index_17[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				UKF_index_17[i]=0xAFFE;
		}
	}
}

int UKF_index_18[0];

static bool isIinitialized_UKF_index_18 = false;
void initialize_UKF_index_18() {
	if (!isIinitialized_UKF_index_18){
		int i;
		for (i=0; i < 0; i++){
			UKF_index_18[i] = i+1;
		}
		isIinitialized_UKF_index_18 = true;
	}
}

void read_UKF_index_18(int labelAccessStatistics) {
	int numberOfBytes = 1;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(UKF_index_18) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = UKF_index_18[i];
			a = UKF_index_18[i+1];
			a = UKF_index_18[i+2];
			a = UKF_index_18[i+3];
			a = UKF_index_18[i+4];
			a = UKF_index_18[i+5];
			a = UKF_index_18[i+6];
			a = UKF_index_18[i+7];
			a = UKF_index_18[i+8];
			a = UKF_index_18[i+9];
		}
	
		for(;i<arraysize;i++){
			a = UKF_index_18[i];
		}

		(void)a;
	}
}

void write_UKF_index_18(int labelAccessStatistics) {
	int numberOfBytes = 1;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(UKF_index_18) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			UKF_index_18[i]   = 0x800A;
			UKF_index_18[i+1] = 0xAFFE;
			UKF_index_18[i+2] = 0xAFFE;
			UKF_index_18[i+3] = 0xAFFE;
			UKF_index_18[i+4] = 0xAFFE;
			UKF_index_18[i+5] = 0xAFFE;
			UKF_index_18[i+6] = 0xAFFE;
			UKF_index_18[i+7] = 0xAFFE;
			UKF_index_18[i+8] = 0xAFFE;
			UKF_index_18[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				UKF_index_18[i]=0xAFFE;
		}
	}
}

int UKF_index_19[0];

static bool isIinitialized_UKF_index_19 = false;
void initialize_UKF_index_19() {
	if (!isIinitialized_UKF_index_19){
		int i;
		for (i=0; i < 0; i++){
			UKF_index_19[i] = i+1;
		}
		isIinitialized_UKF_index_19 = true;
	}
}

void read_UKF_index_19(int labelAccessStatistics) {
	int numberOfBytes = 1;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(UKF_index_19) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = UKF_index_19[i];
			a = UKF_index_19[i+1];
			a = UKF_index_19[i+2];
			a = UKF_index_19[i+3];
			a = UKF_index_19[i+4];
			a = UKF_index_19[i+5];
			a = UKF_index_19[i+6];
			a = UKF_index_19[i+7];
			a = UKF_index_19[i+8];
			a = UKF_index_19[i+9];
		}
	
		for(;i<arraysize;i++){
			a = UKF_index_19[i];
		}

		(void)a;
	}
}

void write_UKF_index_19(int labelAccessStatistics) {
	int numberOfBytes = 1;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(UKF_index_19) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			UKF_index_19[i]   = 0x800A;
			UKF_index_19[i+1] = 0xAFFE;
			UKF_index_19[i+2] = 0xAFFE;
			UKF_index_19[i+3] = 0xAFFE;
			UKF_index_19[i+4] = 0xAFFE;
			UKF_index_19[i+5] = 0xAFFE;
			UKF_index_19[i+6] = 0xAFFE;
			UKF_index_19[i+7] = 0xAFFE;
			UKF_index_19[i+8] = 0xAFFE;
			UKF_index_19[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				UKF_index_19[i]=0xAFFE;
		}
	}
}

int UKF_index_20[0];

static bool isIinitialized_UKF_index_20 = false;
void initialize_UKF_index_20() {
	if (!isIinitialized_UKF_index_20){
		int i;
		for (i=0; i < 0; i++){
			UKF_index_20[i] = i+1;
		}
		isIinitialized_UKF_index_20 = true;
	}
}

void read_UKF_index_20(int labelAccessStatistics) {
	int numberOfBytes = 1;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(UKF_index_20) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = UKF_index_20[i];
			a = UKF_index_20[i+1];
			a = UKF_index_20[i+2];
			a = UKF_index_20[i+3];
			a = UKF_index_20[i+4];
			a = UKF_index_20[i+5];
			a = UKF_index_20[i+6];
			a = UKF_index_20[i+7];
			a = UKF_index_20[i+8];
			a = UKF_index_20[i+9];
		}
	
		for(;i<arraysize;i++){
			a = UKF_index_20[i];
		}

		(void)a;
	}
}

void write_UKF_index_20(int labelAccessStatistics) {
	int numberOfBytes = 1;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(UKF_index_20) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			UKF_index_20[i]   = 0x800A;
			UKF_index_20[i+1] = 0xAFFE;
			UKF_index_20[i+2] = 0xAFFE;
			UKF_index_20[i+3] = 0xAFFE;
			UKF_index_20[i+4] = 0xAFFE;
			UKF_index_20[i+5] = 0xAFFE;
			UKF_index_20[i+6] = 0xAFFE;
			UKF_index_20[i+7] = 0xAFFE;
			UKF_index_20[i+8] = 0xAFFE;
			UKF_index_20[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				UKF_index_20[i]=0xAFFE;
		}
	}
}

int UKF_index_21[0];

static bool isIinitialized_UKF_index_21 = false;
void initialize_UKF_index_21() {
	if (!isIinitialized_UKF_index_21){
		int i;
		for (i=0; i < 0; i++){
			UKF_index_21[i] = i+1;
		}
		isIinitialized_UKF_index_21 = true;
	}
}

void read_UKF_index_21(int labelAccessStatistics) {
	int numberOfBytes = 1;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(UKF_index_21) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = UKF_index_21[i];
			a = UKF_index_21[i+1];
			a = UKF_index_21[i+2];
			a = UKF_index_21[i+3];
			a = UKF_index_21[i+4];
			a = UKF_index_21[i+5];
			a = UKF_index_21[i+6];
			a = UKF_index_21[i+7];
			a = UKF_index_21[i+8];
			a = UKF_index_21[i+9];
		}
	
		for(;i<arraysize;i++){
			a = UKF_index_21[i];
		}

		(void)a;
	}
}

void write_UKF_index_21(int labelAccessStatistics) {
	int numberOfBytes = 1;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(UKF_index_21) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			UKF_index_21[i]   = 0x800A;
			UKF_index_21[i+1] = 0xAFFE;
			UKF_index_21[i+2] = 0xAFFE;
			UKF_index_21[i+3] = 0xAFFE;
			UKF_index_21[i+4] = 0xAFFE;
			UKF_index_21[i+5] = 0xAFFE;
			UKF_index_21[i+6] = 0xAFFE;
			UKF_index_21[i+7] = 0xAFFE;
			UKF_index_21[i+8] = 0xAFFE;
			UKF_index_21[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				UKF_index_21[i]=0xAFFE;
		}
	}
}

int UKF_index_22[0];

static bool isIinitialized_UKF_index_22 = false;
void initialize_UKF_index_22() {
	if (!isIinitialized_UKF_index_22){
		int i;
		for (i=0; i < 0; i++){
			UKF_index_22[i] = i+1;
		}
		isIinitialized_UKF_index_22 = true;
	}
}

void read_UKF_index_22(int labelAccessStatistics) {
	int numberOfBytes = 1;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(UKF_index_22) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = UKF_index_22[i];
			a = UKF_index_22[i+1];
			a = UKF_index_22[i+2];
			a = UKF_index_22[i+3];
			a = UKF_index_22[i+4];
			a = UKF_index_22[i+5];
			a = UKF_index_22[i+6];
			a = UKF_index_22[i+7];
			a = UKF_index_22[i+8];
			a = UKF_index_22[i+9];
		}
	
		for(;i<arraysize;i++){
			a = UKF_index_22[i];
		}

		(void)a;
	}
}

void write_UKF_index_22(int labelAccessStatistics) {
	int numberOfBytes = 1;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(UKF_index_22) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			UKF_index_22[i]   = 0x800A;
			UKF_index_22[i+1] = 0xAFFE;
			UKF_index_22[i+2] = 0xAFFE;
			UKF_index_22[i+3] = 0xAFFE;
			UKF_index_22[i+4] = 0xAFFE;
			UKF_index_22[i+5] = 0xAFFE;
			UKF_index_22[i+6] = 0xAFFE;
			UKF_index_22[i+7] = 0xAFFE;
			UKF_index_22[i+8] = 0xAFFE;
			UKF_index_22[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				UKF_index_22[i]=0xAFFE;
		}
	}
}

int UKF_index_23[0];

static bool isIinitialized_UKF_index_23 = false;
void initialize_UKF_index_23() {
	if (!isIinitialized_UKF_index_23){
		int i;
		for (i=0; i < 0; i++){
			UKF_index_23[i] = i+1;
		}
		isIinitialized_UKF_index_23 = true;
	}
}

void read_UKF_index_23(int labelAccessStatistics) {
	int numberOfBytes = 1;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(UKF_index_23) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = UKF_index_23[i];
			a = UKF_index_23[i+1];
			a = UKF_index_23[i+2];
			a = UKF_index_23[i+3];
			a = UKF_index_23[i+4];
			a = UKF_index_23[i+5];
			a = UKF_index_23[i+6];
			a = UKF_index_23[i+7];
			a = UKF_index_23[i+8];
			a = UKF_index_23[i+9];
		}
	
		for(;i<arraysize;i++){
			a = UKF_index_23[i];
		}

		(void)a;
	}
}

void write_UKF_index_23(int labelAccessStatistics) {
	int numberOfBytes = 1;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(UKF_index_23) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			UKF_index_23[i]   = 0x800A;
			UKF_index_23[i+1] = 0xAFFE;
			UKF_index_23[i+2] = 0xAFFE;
			UKF_index_23[i+3] = 0xAFFE;
			UKF_index_23[i+4] = 0xAFFE;
			UKF_index_23[i+5] = 0xAFFE;
			UKF_index_23[i+6] = 0xAFFE;
			UKF_index_23[i+7] = 0xAFFE;
			UKF_index_23[i+8] = 0xAFFE;
			UKF_index_23[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				UKF_index_23[i]=0xAFFE;
		}
	}
}

int UKF_index_25[0];

static bool isIinitialized_UKF_index_25 = false;
void initialize_UKF_index_25() {
	if (!isIinitialized_UKF_index_25){
		int i;
		for (i=0; i < 0; i++){
			UKF_index_25[i] = i+1;
		}
		isIinitialized_UKF_index_25 = true;
	}
}

void read_UKF_index_25(int labelAccessStatistics) {
	int numberOfBytes = 1;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(UKF_index_25) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = UKF_index_25[i];
			a = UKF_index_25[i+1];
			a = UKF_index_25[i+2];
			a = UKF_index_25[i+3];
			a = UKF_index_25[i+4];
			a = UKF_index_25[i+5];
			a = UKF_index_25[i+6];
			a = UKF_index_25[i+7];
			a = UKF_index_25[i+8];
			a = UKF_index_25[i+9];
		}
	
		for(;i<arraysize;i++){
			a = UKF_index_25[i];
		}

		(void)a;
	}
}

void write_UKF_index_25(int labelAccessStatistics) {
	int numberOfBytes = 1;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(UKF_index_25) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			UKF_index_25[i]   = 0x800A;
			UKF_index_25[i+1] = 0xAFFE;
			UKF_index_25[i+2] = 0xAFFE;
			UKF_index_25[i+3] = 0xAFFE;
			UKF_index_25[i+4] = 0xAFFE;
			UKF_index_25[i+5] = 0xAFFE;
			UKF_index_25[i+6] = 0xAFFE;
			UKF_index_25[i+7] = 0xAFFE;
			UKF_index_25[i+8] = 0xAFFE;
			UKF_index_25[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				UKF_index_25[i]=0xAFFE;
		}
	}
}

int UKF_index_26[0];

static bool isIinitialized_UKF_index_26 = false;
void initialize_UKF_index_26() {
	if (!isIinitialized_UKF_index_26){
		int i;
		for (i=0; i < 0; i++){
			UKF_index_26[i] = i+1;
		}
		isIinitialized_UKF_index_26 = true;
	}
}

void read_UKF_index_26(int labelAccessStatistics) {
	int numberOfBytes = 1;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(UKF_index_26) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = UKF_index_26[i];
			a = UKF_index_26[i+1];
			a = UKF_index_26[i+2];
			a = UKF_index_26[i+3];
			a = UKF_index_26[i+4];
			a = UKF_index_26[i+5];
			a = UKF_index_26[i+6];
			a = UKF_index_26[i+7];
			a = UKF_index_26[i+8];
			a = UKF_index_26[i+9];
		}
	
		for(;i<arraysize;i++){
			a = UKF_index_26[i];
		}

		(void)a;
	}
}

void write_UKF_index_26(int labelAccessStatistics) {
	int numberOfBytes = 1;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(UKF_index_26) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			UKF_index_26[i]   = 0x800A;
			UKF_index_26[i+1] = 0xAFFE;
			UKF_index_26[i+2] = 0xAFFE;
			UKF_index_26[i+3] = 0xAFFE;
			UKF_index_26[i+4] = 0xAFFE;
			UKF_index_26[i+5] = 0xAFFE;
			UKF_index_26[i+6] = 0xAFFE;
			UKF_index_26[i+7] = 0xAFFE;
			UKF_index_26[i+8] = 0xAFFE;
			UKF_index_26[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				UKF_index_26[i]=0xAFFE;
		}
	}
}

int UKF_index_27[0];

static bool isIinitialized_UKF_index_27 = false;
void initialize_UKF_index_27() {
	if (!isIinitialized_UKF_index_27){
		int i;
		for (i=0; i < 0; i++){
			UKF_index_27[i] = i+1;
		}
		isIinitialized_UKF_index_27 = true;
	}
}

void read_UKF_index_27(int labelAccessStatistics) {
	int numberOfBytes = 1;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(UKF_index_27) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = UKF_index_27[i];
			a = UKF_index_27[i+1];
			a = UKF_index_27[i+2];
			a = UKF_index_27[i+3];
			a = UKF_index_27[i+4];
			a = UKF_index_27[i+5];
			a = UKF_index_27[i+6];
			a = UKF_index_27[i+7];
			a = UKF_index_27[i+8];
			a = UKF_index_27[i+9];
		}
	
		for(;i<arraysize;i++){
			a = UKF_index_27[i];
		}

		(void)a;
	}
}

void write_UKF_index_27(int labelAccessStatistics) {
	int numberOfBytes = 1;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(UKF_index_27) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			UKF_index_27[i]   = 0x800A;
			UKF_index_27[i+1] = 0xAFFE;
			UKF_index_27[i+2] = 0xAFFE;
			UKF_index_27[i+3] = 0xAFFE;
			UKF_index_27[i+4] = 0xAFFE;
			UKF_index_27[i+5] = 0xAFFE;
			UKF_index_27[i+6] = 0xAFFE;
			UKF_index_27[i+7] = 0xAFFE;
			UKF_index_27[i+8] = 0xAFFE;
			UKF_index_27[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				UKF_index_27[i]=0xAFFE;
		}
	}
}

int UKF_index_28[0];

static bool isIinitialized_UKF_index_28 = false;
void initialize_UKF_index_28() {
	if (!isIinitialized_UKF_index_28){
		int i;
		for (i=0; i < 0; i++){
			UKF_index_28[i] = i+1;
		}
		isIinitialized_UKF_index_28 = true;
	}
}

void read_UKF_index_28(int labelAccessStatistics) {
	int numberOfBytes = 1;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(UKF_index_28) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = UKF_index_28[i];
			a = UKF_index_28[i+1];
			a = UKF_index_28[i+2];
			a = UKF_index_28[i+3];
			a = UKF_index_28[i+4];
			a = UKF_index_28[i+5];
			a = UKF_index_28[i+6];
			a = UKF_index_28[i+7];
			a = UKF_index_28[i+8];
			a = UKF_index_28[i+9];
		}
	
		for(;i<arraysize;i++){
			a = UKF_index_28[i];
		}

		(void)a;
	}
}

void write_UKF_index_28(int labelAccessStatistics) {
	int numberOfBytes = 1;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(UKF_index_28) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			UKF_index_28[i]   = 0x800A;
			UKF_index_28[i+1] = 0xAFFE;
			UKF_index_28[i+2] = 0xAFFE;
			UKF_index_28[i+3] = 0xAFFE;
			UKF_index_28[i+4] = 0xAFFE;
			UKF_index_28[i+5] = 0xAFFE;
			UKF_index_28[i+6] = 0xAFFE;
			UKF_index_28[i+7] = 0xAFFE;
			UKF_index_28[i+8] = 0xAFFE;
			UKF_index_28[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				UKF_index_28[i]=0xAFFE;
		}
	}
}

int UKF_index_29[0];

static bool isIinitialized_UKF_index_29 = false;
void initialize_UKF_index_29() {
	if (!isIinitialized_UKF_index_29){
		int i;
		for (i=0; i < 0; i++){
			UKF_index_29[i] = i+1;
		}
		isIinitialized_UKF_index_29 = true;
	}
}

void read_UKF_index_29(int labelAccessStatistics) {
	int numberOfBytes = 1;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(UKF_index_29) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = UKF_index_29[i];
			a = UKF_index_29[i+1];
			a = UKF_index_29[i+2];
			a = UKF_index_29[i+3];
			a = UKF_index_29[i+4];
			a = UKF_index_29[i+5];
			a = UKF_index_29[i+6];
			a = UKF_index_29[i+7];
			a = UKF_index_29[i+8];
			a = UKF_index_29[i+9];
		}
	
		for(;i<arraysize;i++){
			a = UKF_index_29[i];
		}

		(void)a;
	}
}

void write_UKF_index_29(int labelAccessStatistics) {
	int numberOfBytes = 1;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(UKF_index_29) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			UKF_index_29[i]   = 0x800A;
			UKF_index_29[i+1] = 0xAFFE;
			UKF_index_29[i+2] = 0xAFFE;
			UKF_index_29[i+3] = 0xAFFE;
			UKF_index_29[i+4] = 0xAFFE;
			UKF_index_29[i+5] = 0xAFFE;
			UKF_index_29[i+6] = 0xAFFE;
			UKF_index_29[i+7] = 0xAFFE;
			UKF_index_29[i+8] = 0xAFFE;
			UKF_index_29[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				UKF_index_29[i]=0xAFFE;
		}
	}
}

int UKF_index_30[0];

static bool isIinitialized_UKF_index_30 = false;
void initialize_UKF_index_30() {
	if (!isIinitialized_UKF_index_30){
		int i;
		for (i=0; i < 0; i++){
			UKF_index_30[i] = i+1;
		}
		isIinitialized_UKF_index_30 = true;
	}
}

void read_UKF_index_30(int labelAccessStatistics) {
	int numberOfBytes = 1;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(UKF_index_30) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = UKF_index_30[i];
			a = UKF_index_30[i+1];
			a = UKF_index_30[i+2];
			a = UKF_index_30[i+3];
			a = UKF_index_30[i+4];
			a = UKF_index_30[i+5];
			a = UKF_index_30[i+6];
			a = UKF_index_30[i+7];
			a = UKF_index_30[i+8];
			a = UKF_index_30[i+9];
		}
	
		for(;i<arraysize;i++){
			a = UKF_index_30[i];
		}

		(void)a;
	}
}

void write_UKF_index_30(int labelAccessStatistics) {
	int numberOfBytes = 1;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(UKF_index_30) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			UKF_index_30[i]   = 0x800A;
			UKF_index_30[i+1] = 0xAFFE;
			UKF_index_30[i+2] = 0xAFFE;
			UKF_index_30[i+3] = 0xAFFE;
			UKF_index_30[i+4] = 0xAFFE;
			UKF_index_30[i+5] = 0xAFFE;
			UKF_index_30[i+6] = 0xAFFE;
			UKF_index_30[i+7] = 0xAFFE;
			UKF_index_30[i+8] = 0xAFFE;
			UKF_index_30[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				UKF_index_30[i]=0xAFFE;
		}
	}
}

int UKF_index_32[0];

static bool isIinitialized_UKF_index_32 = false;
void initialize_UKF_index_32() {
	if (!isIinitialized_UKF_index_32){
		int i;
		for (i=0; i < 0; i++){
			UKF_index_32[i] = i+1;
		}
		isIinitialized_UKF_index_32 = true;
	}
}

void read_UKF_index_32(int labelAccessStatistics) {
	int numberOfBytes = 1;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(UKF_index_32) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = UKF_index_32[i];
			a = UKF_index_32[i+1];
			a = UKF_index_32[i+2];
			a = UKF_index_32[i+3];
			a = UKF_index_32[i+4];
			a = UKF_index_32[i+5];
			a = UKF_index_32[i+6];
			a = UKF_index_32[i+7];
			a = UKF_index_32[i+8];
			a = UKF_index_32[i+9];
		}
	
		for(;i<arraysize;i++){
			a = UKF_index_32[i];
		}

		(void)a;
	}
}

void write_UKF_index_32(int labelAccessStatistics) {
	int numberOfBytes = 1;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(UKF_index_32) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			UKF_index_32[i]   = 0x800A;
			UKF_index_32[i+1] = 0xAFFE;
			UKF_index_32[i+2] = 0xAFFE;
			UKF_index_32[i+3] = 0xAFFE;
			UKF_index_32[i+4] = 0xAFFE;
			UKF_index_32[i+5] = 0xAFFE;
			UKF_index_32[i+6] = 0xAFFE;
			UKF_index_32[i+7] = 0xAFFE;
			UKF_index_32[i+8] = 0xAFFE;
			UKF_index_32[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				UKF_index_32[i]=0xAFFE;
		}
	}
}

int UKF_index_40[0];

static bool isIinitialized_UKF_index_40 = false;
void initialize_UKF_index_40() {
	if (!isIinitialized_UKF_index_40){
		int i;
		for (i=0; i < 0; i++){
			UKF_index_40[i] = i+1;
		}
		isIinitialized_UKF_index_40 = true;
	}
}

void read_UKF_index_40(int labelAccessStatistics) {
	int numberOfBytes = 1;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(UKF_index_40) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = UKF_index_40[i];
			a = UKF_index_40[i+1];
			a = UKF_index_40[i+2];
			a = UKF_index_40[i+3];
			a = UKF_index_40[i+4];
			a = UKF_index_40[i+5];
			a = UKF_index_40[i+6];
			a = UKF_index_40[i+7];
			a = UKF_index_40[i+8];
			a = UKF_index_40[i+9];
		}
	
		for(;i<arraysize;i++){
			a = UKF_index_40[i];
		}

		(void)a;
	}
}

void write_UKF_index_40(int labelAccessStatistics) {
	int numberOfBytes = 1;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(UKF_index_40) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			UKF_index_40[i]   = 0x800A;
			UKF_index_40[i+1] = 0xAFFE;
			UKF_index_40[i+2] = 0xAFFE;
			UKF_index_40[i+3] = 0xAFFE;
			UKF_index_40[i+4] = 0xAFFE;
			UKF_index_40[i+5] = 0xAFFE;
			UKF_index_40[i+6] = 0xAFFE;
			UKF_index_40[i+7] = 0xAFFE;
			UKF_index_40[i+8] = 0xAFFE;
			UKF_index_40[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				UKF_index_40[i]=0xAFFE;
		}
	}
}

int UKF_index_47[0];

static bool isIinitialized_UKF_index_47 = false;
void initialize_UKF_index_47() {
	if (!isIinitialized_UKF_index_47){
		int i;
		for (i=0; i < 0; i++){
			UKF_index_47[i] = i+1;
		}
		isIinitialized_UKF_index_47 = true;
	}
}

void read_UKF_index_47(int labelAccessStatistics) {
	int numberOfBytes = 1;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(UKF_index_47) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = UKF_index_47[i];
			a = UKF_index_47[i+1];
			a = UKF_index_47[i+2];
			a = UKF_index_47[i+3];
			a = UKF_index_47[i+4];
			a = UKF_index_47[i+5];
			a = UKF_index_47[i+6];
			a = UKF_index_47[i+7];
			a = UKF_index_47[i+8];
			a = UKF_index_47[i+9];
		}
	
		for(;i<arraysize;i++){
			a = UKF_index_47[i];
		}

		(void)a;
	}
}

void write_UKF_index_47(int labelAccessStatistics) {
	int numberOfBytes = 1;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(UKF_index_47) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			UKF_index_47[i]   = 0x800A;
			UKF_index_47[i+1] = 0xAFFE;
			UKF_index_47[i+2] = 0xAFFE;
			UKF_index_47[i+3] = 0xAFFE;
			UKF_index_47[i+4] = 0xAFFE;
			UKF_index_47[i+5] = 0xAFFE;
			UKF_index_47[i+6] = 0xAFFE;
			UKF_index_47[i+7] = 0xAFFE;
			UKF_index_47[i+8] = 0xAFFE;
			UKF_index_47[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				UKF_index_47[i]=0xAFFE;
		}
	}
}

int UKF_index_33[0];

static bool isIinitialized_UKF_index_33 = false;
void initialize_UKF_index_33() {
	if (!isIinitialized_UKF_index_33){
		int i;
		for (i=0; i < 0; i++){
			UKF_index_33[i] = i+1;
		}
		isIinitialized_UKF_index_33 = true;
	}
}

void read_UKF_index_33(int labelAccessStatistics) {
	int numberOfBytes = 1;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(UKF_index_33) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = UKF_index_33[i];
			a = UKF_index_33[i+1];
			a = UKF_index_33[i+2];
			a = UKF_index_33[i+3];
			a = UKF_index_33[i+4];
			a = UKF_index_33[i+5];
			a = UKF_index_33[i+6];
			a = UKF_index_33[i+7];
			a = UKF_index_33[i+8];
			a = UKF_index_33[i+9];
		}
	
		for(;i<arraysize;i++){
			a = UKF_index_33[i];
		}

		(void)a;
	}
}

void write_UKF_index_33(int labelAccessStatistics) {
	int numberOfBytes = 1;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(UKF_index_33) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			UKF_index_33[i]   = 0x800A;
			UKF_index_33[i+1] = 0xAFFE;
			UKF_index_33[i+2] = 0xAFFE;
			UKF_index_33[i+3] = 0xAFFE;
			UKF_index_33[i+4] = 0xAFFE;
			UKF_index_33[i+5] = 0xAFFE;
			UKF_index_33[i+6] = 0xAFFE;
			UKF_index_33[i+7] = 0xAFFE;
			UKF_index_33[i+8] = 0xAFFE;
			UKF_index_33[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				UKF_index_33[i]=0xAFFE;
		}
	}
}

int UKF_index_34[0];

static bool isIinitialized_UKF_index_34 = false;
void initialize_UKF_index_34() {
	if (!isIinitialized_UKF_index_34){
		int i;
		for (i=0; i < 0; i++){
			UKF_index_34[i] = i+1;
		}
		isIinitialized_UKF_index_34 = true;
	}
}

void read_UKF_index_34(int labelAccessStatistics) {
	int numberOfBytes = 1;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(UKF_index_34) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = UKF_index_34[i];
			a = UKF_index_34[i+1];
			a = UKF_index_34[i+2];
			a = UKF_index_34[i+3];
			a = UKF_index_34[i+4];
			a = UKF_index_34[i+5];
			a = UKF_index_34[i+6];
			a = UKF_index_34[i+7];
			a = UKF_index_34[i+8];
			a = UKF_index_34[i+9];
		}
	
		for(;i<arraysize;i++){
			a = UKF_index_34[i];
		}

		(void)a;
	}
}

void write_UKF_index_34(int labelAccessStatistics) {
	int numberOfBytes = 1;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(UKF_index_34) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			UKF_index_34[i]   = 0x800A;
			UKF_index_34[i+1] = 0xAFFE;
			UKF_index_34[i+2] = 0xAFFE;
			UKF_index_34[i+3] = 0xAFFE;
			UKF_index_34[i+4] = 0xAFFE;
			UKF_index_34[i+5] = 0xAFFE;
			UKF_index_34[i+6] = 0xAFFE;
			UKF_index_34[i+7] = 0xAFFE;
			UKF_index_34[i+8] = 0xAFFE;
			UKF_index_34[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				UKF_index_34[i]=0xAFFE;
		}
	}
}

int UKF_index_35[0];

static bool isIinitialized_UKF_index_35 = false;
void initialize_UKF_index_35() {
	if (!isIinitialized_UKF_index_35){
		int i;
		for (i=0; i < 0; i++){
			UKF_index_35[i] = i+1;
		}
		isIinitialized_UKF_index_35 = true;
	}
}

void read_UKF_index_35(int labelAccessStatistics) {
	int numberOfBytes = 1;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(UKF_index_35) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = UKF_index_35[i];
			a = UKF_index_35[i+1];
			a = UKF_index_35[i+2];
			a = UKF_index_35[i+3];
			a = UKF_index_35[i+4];
			a = UKF_index_35[i+5];
			a = UKF_index_35[i+6];
			a = UKF_index_35[i+7];
			a = UKF_index_35[i+8];
			a = UKF_index_35[i+9];
		}
	
		for(;i<arraysize;i++){
			a = UKF_index_35[i];
		}

		(void)a;
	}
}

void write_UKF_index_35(int labelAccessStatistics) {
	int numberOfBytes = 1;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(UKF_index_35) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			UKF_index_35[i]   = 0x800A;
			UKF_index_35[i+1] = 0xAFFE;
			UKF_index_35[i+2] = 0xAFFE;
			UKF_index_35[i+3] = 0xAFFE;
			UKF_index_35[i+4] = 0xAFFE;
			UKF_index_35[i+5] = 0xAFFE;
			UKF_index_35[i+6] = 0xAFFE;
			UKF_index_35[i+7] = 0xAFFE;
			UKF_index_35[i+8] = 0xAFFE;
			UKF_index_35[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				UKF_index_35[i]=0xAFFE;
		}
	}
}

int UKF_index_36[0];

static bool isIinitialized_UKF_index_36 = false;
void initialize_UKF_index_36() {
	if (!isIinitialized_UKF_index_36){
		int i;
		for (i=0; i < 0; i++){
			UKF_index_36[i] = i+1;
		}
		isIinitialized_UKF_index_36 = true;
	}
}

void read_UKF_index_36(int labelAccessStatistics) {
	int numberOfBytes = 1;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(UKF_index_36) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = UKF_index_36[i];
			a = UKF_index_36[i+1];
			a = UKF_index_36[i+2];
			a = UKF_index_36[i+3];
			a = UKF_index_36[i+4];
			a = UKF_index_36[i+5];
			a = UKF_index_36[i+6];
			a = UKF_index_36[i+7];
			a = UKF_index_36[i+8];
			a = UKF_index_36[i+9];
		}
	
		for(;i<arraysize;i++){
			a = UKF_index_36[i];
		}

		(void)a;
	}
}

void write_UKF_index_36(int labelAccessStatistics) {
	int numberOfBytes = 1;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(UKF_index_36) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			UKF_index_36[i]   = 0x800A;
			UKF_index_36[i+1] = 0xAFFE;
			UKF_index_36[i+2] = 0xAFFE;
			UKF_index_36[i+3] = 0xAFFE;
			UKF_index_36[i+4] = 0xAFFE;
			UKF_index_36[i+5] = 0xAFFE;
			UKF_index_36[i+6] = 0xAFFE;
			UKF_index_36[i+7] = 0xAFFE;
			UKF_index_36[i+8] = 0xAFFE;
			UKF_index_36[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				UKF_index_36[i]=0xAFFE;
		}
	}
}

int UKF_index_37[0];

static bool isIinitialized_UKF_index_37 = false;
void initialize_UKF_index_37() {
	if (!isIinitialized_UKF_index_37){
		int i;
		for (i=0; i < 0; i++){
			UKF_index_37[i] = i+1;
		}
		isIinitialized_UKF_index_37 = true;
	}
}

void read_UKF_index_37(int labelAccessStatistics) {
	int numberOfBytes = 1;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(UKF_index_37) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = UKF_index_37[i];
			a = UKF_index_37[i+1];
			a = UKF_index_37[i+2];
			a = UKF_index_37[i+3];
			a = UKF_index_37[i+4];
			a = UKF_index_37[i+5];
			a = UKF_index_37[i+6];
			a = UKF_index_37[i+7];
			a = UKF_index_37[i+8];
			a = UKF_index_37[i+9];
		}
	
		for(;i<arraysize;i++){
			a = UKF_index_37[i];
		}

		(void)a;
	}
}

void write_UKF_index_37(int labelAccessStatistics) {
	int numberOfBytes = 1;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(UKF_index_37) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			UKF_index_37[i]   = 0x800A;
			UKF_index_37[i+1] = 0xAFFE;
			UKF_index_37[i+2] = 0xAFFE;
			UKF_index_37[i+3] = 0xAFFE;
			UKF_index_37[i+4] = 0xAFFE;
			UKF_index_37[i+5] = 0xAFFE;
			UKF_index_37[i+6] = 0xAFFE;
			UKF_index_37[i+7] = 0xAFFE;
			UKF_index_37[i+8] = 0xAFFE;
			UKF_index_37[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				UKF_index_37[i]=0xAFFE;
		}
	}
}

int UKF_index_38[0];

static bool isIinitialized_UKF_index_38 = false;
void initialize_UKF_index_38() {
	if (!isIinitialized_UKF_index_38){
		int i;
		for (i=0; i < 0; i++){
			UKF_index_38[i] = i+1;
		}
		isIinitialized_UKF_index_38 = true;
	}
}

void read_UKF_index_38(int labelAccessStatistics) {
	int numberOfBytes = 1;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(UKF_index_38) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = UKF_index_38[i];
			a = UKF_index_38[i+1];
			a = UKF_index_38[i+2];
			a = UKF_index_38[i+3];
			a = UKF_index_38[i+4];
			a = UKF_index_38[i+5];
			a = UKF_index_38[i+6];
			a = UKF_index_38[i+7];
			a = UKF_index_38[i+8];
			a = UKF_index_38[i+9];
		}
	
		for(;i<arraysize;i++){
			a = UKF_index_38[i];
		}

		(void)a;
	}
}

void write_UKF_index_38(int labelAccessStatistics) {
	int numberOfBytes = 1;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(UKF_index_38) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			UKF_index_38[i]   = 0x800A;
			UKF_index_38[i+1] = 0xAFFE;
			UKF_index_38[i+2] = 0xAFFE;
			UKF_index_38[i+3] = 0xAFFE;
			UKF_index_38[i+4] = 0xAFFE;
			UKF_index_38[i+5] = 0xAFFE;
			UKF_index_38[i+6] = 0xAFFE;
			UKF_index_38[i+7] = 0xAFFE;
			UKF_index_38[i+8] = 0xAFFE;
			UKF_index_38[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				UKF_index_38[i]=0xAFFE;
		}
	}
}

int UKF_index_39[0];

static bool isIinitialized_UKF_index_39 = false;
void initialize_UKF_index_39() {
	if (!isIinitialized_UKF_index_39){
		int i;
		for (i=0; i < 0; i++){
			UKF_index_39[i] = i+1;
		}
		isIinitialized_UKF_index_39 = true;
	}
}

void read_UKF_index_39(int labelAccessStatistics) {
	int numberOfBytes = 1;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(UKF_index_39) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = UKF_index_39[i];
			a = UKF_index_39[i+1];
			a = UKF_index_39[i+2];
			a = UKF_index_39[i+3];
			a = UKF_index_39[i+4];
			a = UKF_index_39[i+5];
			a = UKF_index_39[i+6];
			a = UKF_index_39[i+7];
			a = UKF_index_39[i+8];
			a = UKF_index_39[i+9];
		}
	
		for(;i<arraysize;i++){
			a = UKF_index_39[i];
		}

		(void)a;
	}
}

void write_UKF_index_39(int labelAccessStatistics) {
	int numberOfBytes = 1;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(UKF_index_39) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			UKF_index_39[i]   = 0x800A;
			UKF_index_39[i+1] = 0xAFFE;
			UKF_index_39[i+2] = 0xAFFE;
			UKF_index_39[i+3] = 0xAFFE;
			UKF_index_39[i+4] = 0xAFFE;
			UKF_index_39[i+5] = 0xAFFE;
			UKF_index_39[i+6] = 0xAFFE;
			UKF_index_39[i+7] = 0xAFFE;
			UKF_index_39[i+8] = 0xAFFE;
			UKF_index_39[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				UKF_index_39[i]=0xAFFE;
		}
	}
}

int UKF_index_41[0];

static bool isIinitialized_UKF_index_41 = false;
void initialize_UKF_index_41() {
	if (!isIinitialized_UKF_index_41){
		int i;
		for (i=0; i < 0; i++){
			UKF_index_41[i] = i+1;
		}
		isIinitialized_UKF_index_41 = true;
	}
}

void read_UKF_index_41(int labelAccessStatistics) {
	int numberOfBytes = 1;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(UKF_index_41) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = UKF_index_41[i];
			a = UKF_index_41[i+1];
			a = UKF_index_41[i+2];
			a = UKF_index_41[i+3];
			a = UKF_index_41[i+4];
			a = UKF_index_41[i+5];
			a = UKF_index_41[i+6];
			a = UKF_index_41[i+7];
			a = UKF_index_41[i+8];
			a = UKF_index_41[i+9];
		}
	
		for(;i<arraysize;i++){
			a = UKF_index_41[i];
		}

		(void)a;
	}
}

void write_UKF_index_41(int labelAccessStatistics) {
	int numberOfBytes = 1;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(UKF_index_41) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			UKF_index_41[i]   = 0x800A;
			UKF_index_41[i+1] = 0xAFFE;
			UKF_index_41[i+2] = 0xAFFE;
			UKF_index_41[i+3] = 0xAFFE;
			UKF_index_41[i+4] = 0xAFFE;
			UKF_index_41[i+5] = 0xAFFE;
			UKF_index_41[i+6] = 0xAFFE;
			UKF_index_41[i+7] = 0xAFFE;
			UKF_index_41[i+8] = 0xAFFE;
			UKF_index_41[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				UKF_index_41[i]=0xAFFE;
		}
	}
}

int UKF_index_42[0];

static bool isIinitialized_UKF_index_42 = false;
void initialize_UKF_index_42() {
	if (!isIinitialized_UKF_index_42){
		int i;
		for (i=0; i < 0; i++){
			UKF_index_42[i] = i+1;
		}
		isIinitialized_UKF_index_42 = true;
	}
}

void read_UKF_index_42(int labelAccessStatistics) {
	int numberOfBytes = 1;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(UKF_index_42) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = UKF_index_42[i];
			a = UKF_index_42[i+1];
			a = UKF_index_42[i+2];
			a = UKF_index_42[i+3];
			a = UKF_index_42[i+4];
			a = UKF_index_42[i+5];
			a = UKF_index_42[i+6];
			a = UKF_index_42[i+7];
			a = UKF_index_42[i+8];
			a = UKF_index_42[i+9];
		}
	
		for(;i<arraysize;i++){
			a = UKF_index_42[i];
		}

		(void)a;
	}
}

void write_UKF_index_42(int labelAccessStatistics) {
	int numberOfBytes = 1;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(UKF_index_42) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			UKF_index_42[i]   = 0x800A;
			UKF_index_42[i+1] = 0xAFFE;
			UKF_index_42[i+2] = 0xAFFE;
			UKF_index_42[i+3] = 0xAFFE;
			UKF_index_42[i+4] = 0xAFFE;
			UKF_index_42[i+5] = 0xAFFE;
			UKF_index_42[i+6] = 0xAFFE;
			UKF_index_42[i+7] = 0xAFFE;
			UKF_index_42[i+8] = 0xAFFE;
			UKF_index_42[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				UKF_index_42[i]=0xAFFE;
		}
	}
}

int UKF_index_43[0];

static bool isIinitialized_UKF_index_43 = false;
void initialize_UKF_index_43() {
	if (!isIinitialized_UKF_index_43){
		int i;
		for (i=0; i < 0; i++){
			UKF_index_43[i] = i+1;
		}
		isIinitialized_UKF_index_43 = true;
	}
}

void read_UKF_index_43(int labelAccessStatistics) {
	int numberOfBytes = 1;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(UKF_index_43) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = UKF_index_43[i];
			a = UKF_index_43[i+1];
			a = UKF_index_43[i+2];
			a = UKF_index_43[i+3];
			a = UKF_index_43[i+4];
			a = UKF_index_43[i+5];
			a = UKF_index_43[i+6];
			a = UKF_index_43[i+7];
			a = UKF_index_43[i+8];
			a = UKF_index_43[i+9];
		}
	
		for(;i<arraysize;i++){
			a = UKF_index_43[i];
		}

		(void)a;
	}
}

void write_UKF_index_43(int labelAccessStatistics) {
	int numberOfBytes = 1;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(UKF_index_43) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			UKF_index_43[i]   = 0x800A;
			UKF_index_43[i+1] = 0xAFFE;
			UKF_index_43[i+2] = 0xAFFE;
			UKF_index_43[i+3] = 0xAFFE;
			UKF_index_43[i+4] = 0xAFFE;
			UKF_index_43[i+5] = 0xAFFE;
			UKF_index_43[i+6] = 0xAFFE;
			UKF_index_43[i+7] = 0xAFFE;
			UKF_index_43[i+8] = 0xAFFE;
			UKF_index_43[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				UKF_index_43[i]=0xAFFE;
		}
	}
}

int UKF_index_44[0];

static bool isIinitialized_UKF_index_44 = false;
void initialize_UKF_index_44() {
	if (!isIinitialized_UKF_index_44){
		int i;
		for (i=0; i < 0; i++){
			UKF_index_44[i] = i+1;
		}
		isIinitialized_UKF_index_44 = true;
	}
}

void read_UKF_index_44(int labelAccessStatistics) {
	int numberOfBytes = 1;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(UKF_index_44) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = UKF_index_44[i];
			a = UKF_index_44[i+1];
			a = UKF_index_44[i+2];
			a = UKF_index_44[i+3];
			a = UKF_index_44[i+4];
			a = UKF_index_44[i+5];
			a = UKF_index_44[i+6];
			a = UKF_index_44[i+7];
			a = UKF_index_44[i+8];
			a = UKF_index_44[i+9];
		}
	
		for(;i<arraysize;i++){
			a = UKF_index_44[i];
		}

		(void)a;
	}
}

void write_UKF_index_44(int labelAccessStatistics) {
	int numberOfBytes = 1;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(UKF_index_44) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			UKF_index_44[i]   = 0x800A;
			UKF_index_44[i+1] = 0xAFFE;
			UKF_index_44[i+2] = 0xAFFE;
			UKF_index_44[i+3] = 0xAFFE;
			UKF_index_44[i+4] = 0xAFFE;
			UKF_index_44[i+5] = 0xAFFE;
			UKF_index_44[i+6] = 0xAFFE;
			UKF_index_44[i+7] = 0xAFFE;
			UKF_index_44[i+8] = 0xAFFE;
			UKF_index_44[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				UKF_index_44[i]=0xAFFE;
		}
	}
}

int UKF_index_45[0];

static bool isIinitialized_UKF_index_45 = false;
void initialize_UKF_index_45() {
	if (!isIinitialized_UKF_index_45){
		int i;
		for (i=0; i < 0; i++){
			UKF_index_45[i] = i+1;
		}
		isIinitialized_UKF_index_45 = true;
	}
}

void read_UKF_index_45(int labelAccessStatistics) {
	int numberOfBytes = 1;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(UKF_index_45) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = UKF_index_45[i];
			a = UKF_index_45[i+1];
			a = UKF_index_45[i+2];
			a = UKF_index_45[i+3];
			a = UKF_index_45[i+4];
			a = UKF_index_45[i+5];
			a = UKF_index_45[i+6];
			a = UKF_index_45[i+7];
			a = UKF_index_45[i+8];
			a = UKF_index_45[i+9];
		}
	
		for(;i<arraysize;i++){
			a = UKF_index_45[i];
		}

		(void)a;
	}
}

void write_UKF_index_45(int labelAccessStatistics) {
	int numberOfBytes = 1;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(UKF_index_45) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			UKF_index_45[i]   = 0x800A;
			UKF_index_45[i+1] = 0xAFFE;
			UKF_index_45[i+2] = 0xAFFE;
			UKF_index_45[i+3] = 0xAFFE;
			UKF_index_45[i+4] = 0xAFFE;
			UKF_index_45[i+5] = 0xAFFE;
			UKF_index_45[i+6] = 0xAFFE;
			UKF_index_45[i+7] = 0xAFFE;
			UKF_index_45[i+8] = 0xAFFE;
			UKF_index_45[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				UKF_index_45[i]=0xAFFE;
		}
	}
}

int UKF_index_46[0];

static bool isIinitialized_UKF_index_46 = false;
void initialize_UKF_index_46() {
	if (!isIinitialized_UKF_index_46){
		int i;
		for (i=0; i < 0; i++){
			UKF_index_46[i] = i+1;
		}
		isIinitialized_UKF_index_46 = true;
	}
}

void read_UKF_index_46(int labelAccessStatistics) {
	int numberOfBytes = 1;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(UKF_index_46) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = UKF_index_46[i];
			a = UKF_index_46[i+1];
			a = UKF_index_46[i+2];
			a = UKF_index_46[i+3];
			a = UKF_index_46[i+4];
			a = UKF_index_46[i+5];
			a = UKF_index_46[i+6];
			a = UKF_index_46[i+7];
			a = UKF_index_46[i+8];
			a = UKF_index_46[i+9];
		}
	
		for(;i<arraysize;i++){
			a = UKF_index_46[i];
		}

		(void)a;
	}
}

void write_UKF_index_46(int labelAccessStatistics) {
	int numberOfBytes = 1;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(UKF_index_46) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			UKF_index_46[i]   = 0x800A;
			UKF_index_46[i+1] = 0xAFFE;
			UKF_index_46[i+2] = 0xAFFE;
			UKF_index_46[i+3] = 0xAFFE;
			UKF_index_46[i+4] = 0xAFFE;
			UKF_index_46[i+5] = 0xAFFE;
			UKF_index_46[i+6] = 0xAFFE;
			UKF_index_46[i+7] = 0xAFFE;
			UKF_index_46[i+8] = 0xAFFE;
			UKF_index_46[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				UKF_index_46[i]=0xAFFE;
		}
	}
}

int UKF_index_59[0];

static bool isIinitialized_UKF_index_59 = false;
void initialize_UKF_index_59() {
	if (!isIinitialized_UKF_index_59){
		int i;
		for (i=0; i < 0; i++){
			UKF_index_59[i] = i+1;
		}
		isIinitialized_UKF_index_59 = true;
	}
}

void read_UKF_index_59(int labelAccessStatistics) {
	int numberOfBytes = 1;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(UKF_index_59) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = UKF_index_59[i];
			a = UKF_index_59[i+1];
			a = UKF_index_59[i+2];
			a = UKF_index_59[i+3];
			a = UKF_index_59[i+4];
			a = UKF_index_59[i+5];
			a = UKF_index_59[i+6];
			a = UKF_index_59[i+7];
			a = UKF_index_59[i+8];
			a = UKF_index_59[i+9];
		}
	
		for(;i<arraysize;i++){
			a = UKF_index_59[i];
		}

		(void)a;
	}
}

void write_UKF_index_59(int labelAccessStatistics) {
	int numberOfBytes = 1;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(UKF_index_59) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			UKF_index_59[i]   = 0x800A;
			UKF_index_59[i+1] = 0xAFFE;
			UKF_index_59[i+2] = 0xAFFE;
			UKF_index_59[i+3] = 0xAFFE;
			UKF_index_59[i+4] = 0xAFFE;
			UKF_index_59[i+5] = 0xAFFE;
			UKF_index_59[i+6] = 0xAFFE;
			UKF_index_59[i+7] = 0xAFFE;
			UKF_index_59[i+8] = 0xAFFE;
			UKF_index_59[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				UKF_index_59[i]=0xAFFE;
		}
	}
}

int UKF_index_48[0];

static bool isIinitialized_UKF_index_48 = false;
void initialize_UKF_index_48() {
	if (!isIinitialized_UKF_index_48){
		int i;
		for (i=0; i < 0; i++){
			UKF_index_48[i] = i+1;
		}
		isIinitialized_UKF_index_48 = true;
	}
}

void read_UKF_index_48(int labelAccessStatistics) {
	int numberOfBytes = 1;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(UKF_index_48) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = UKF_index_48[i];
			a = UKF_index_48[i+1];
			a = UKF_index_48[i+2];
			a = UKF_index_48[i+3];
			a = UKF_index_48[i+4];
			a = UKF_index_48[i+5];
			a = UKF_index_48[i+6];
			a = UKF_index_48[i+7];
			a = UKF_index_48[i+8];
			a = UKF_index_48[i+9];
		}
	
		for(;i<arraysize;i++){
			a = UKF_index_48[i];
		}

		(void)a;
	}
}

void write_UKF_index_48(int labelAccessStatistics) {
	int numberOfBytes = 1;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(UKF_index_48) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			UKF_index_48[i]   = 0x800A;
			UKF_index_48[i+1] = 0xAFFE;
			UKF_index_48[i+2] = 0xAFFE;
			UKF_index_48[i+3] = 0xAFFE;
			UKF_index_48[i+4] = 0xAFFE;
			UKF_index_48[i+5] = 0xAFFE;
			UKF_index_48[i+6] = 0xAFFE;
			UKF_index_48[i+7] = 0xAFFE;
			UKF_index_48[i+8] = 0xAFFE;
			UKF_index_48[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				UKF_index_48[i]=0xAFFE;
		}
	}
}

int UKF_index_49[0];

static bool isIinitialized_UKF_index_49 = false;
void initialize_UKF_index_49() {
	if (!isIinitialized_UKF_index_49){
		int i;
		for (i=0; i < 0; i++){
			UKF_index_49[i] = i+1;
		}
		isIinitialized_UKF_index_49 = true;
	}
}

void read_UKF_index_49(int labelAccessStatistics) {
	int numberOfBytes = 1;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(UKF_index_49) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = UKF_index_49[i];
			a = UKF_index_49[i+1];
			a = UKF_index_49[i+2];
			a = UKF_index_49[i+3];
			a = UKF_index_49[i+4];
			a = UKF_index_49[i+5];
			a = UKF_index_49[i+6];
			a = UKF_index_49[i+7];
			a = UKF_index_49[i+8];
			a = UKF_index_49[i+9];
		}
	
		for(;i<arraysize;i++){
			a = UKF_index_49[i];
		}

		(void)a;
	}
}

void write_UKF_index_49(int labelAccessStatistics) {
	int numberOfBytes = 1;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(UKF_index_49) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			UKF_index_49[i]   = 0x800A;
			UKF_index_49[i+1] = 0xAFFE;
			UKF_index_49[i+2] = 0xAFFE;
			UKF_index_49[i+3] = 0xAFFE;
			UKF_index_49[i+4] = 0xAFFE;
			UKF_index_49[i+5] = 0xAFFE;
			UKF_index_49[i+6] = 0xAFFE;
			UKF_index_49[i+7] = 0xAFFE;
			UKF_index_49[i+8] = 0xAFFE;
			UKF_index_49[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				UKF_index_49[i]=0xAFFE;
		}
	}
}

int UKF_index_50[0];

static bool isIinitialized_UKF_index_50 = false;
void initialize_UKF_index_50() {
	if (!isIinitialized_UKF_index_50){
		int i;
		for (i=0; i < 0; i++){
			UKF_index_50[i] = i+1;
		}
		isIinitialized_UKF_index_50 = true;
	}
}

void read_UKF_index_50(int labelAccessStatistics) {
	int numberOfBytes = 1;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(UKF_index_50) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = UKF_index_50[i];
			a = UKF_index_50[i+1];
			a = UKF_index_50[i+2];
			a = UKF_index_50[i+3];
			a = UKF_index_50[i+4];
			a = UKF_index_50[i+5];
			a = UKF_index_50[i+6];
			a = UKF_index_50[i+7];
			a = UKF_index_50[i+8];
			a = UKF_index_50[i+9];
		}
	
		for(;i<arraysize;i++){
			a = UKF_index_50[i];
		}

		(void)a;
	}
}

void write_UKF_index_50(int labelAccessStatistics) {
	int numberOfBytes = 1;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(UKF_index_50) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			UKF_index_50[i]   = 0x800A;
			UKF_index_50[i+1] = 0xAFFE;
			UKF_index_50[i+2] = 0xAFFE;
			UKF_index_50[i+3] = 0xAFFE;
			UKF_index_50[i+4] = 0xAFFE;
			UKF_index_50[i+5] = 0xAFFE;
			UKF_index_50[i+6] = 0xAFFE;
			UKF_index_50[i+7] = 0xAFFE;
			UKF_index_50[i+8] = 0xAFFE;
			UKF_index_50[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				UKF_index_50[i]=0xAFFE;
		}
	}
}

int UKF_index_51[0];

static bool isIinitialized_UKF_index_51 = false;
void initialize_UKF_index_51() {
	if (!isIinitialized_UKF_index_51){
		int i;
		for (i=0; i < 0; i++){
			UKF_index_51[i] = i+1;
		}
		isIinitialized_UKF_index_51 = true;
	}
}

void read_UKF_index_51(int labelAccessStatistics) {
	int numberOfBytes = 1;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(UKF_index_51) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = UKF_index_51[i];
			a = UKF_index_51[i+1];
			a = UKF_index_51[i+2];
			a = UKF_index_51[i+3];
			a = UKF_index_51[i+4];
			a = UKF_index_51[i+5];
			a = UKF_index_51[i+6];
			a = UKF_index_51[i+7];
			a = UKF_index_51[i+8];
			a = UKF_index_51[i+9];
		}
	
		for(;i<arraysize;i++){
			a = UKF_index_51[i];
		}

		(void)a;
	}
}

void write_UKF_index_51(int labelAccessStatistics) {
	int numberOfBytes = 1;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(UKF_index_51) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			UKF_index_51[i]   = 0x800A;
			UKF_index_51[i+1] = 0xAFFE;
			UKF_index_51[i+2] = 0xAFFE;
			UKF_index_51[i+3] = 0xAFFE;
			UKF_index_51[i+4] = 0xAFFE;
			UKF_index_51[i+5] = 0xAFFE;
			UKF_index_51[i+6] = 0xAFFE;
			UKF_index_51[i+7] = 0xAFFE;
			UKF_index_51[i+8] = 0xAFFE;
			UKF_index_51[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				UKF_index_51[i]=0xAFFE;
		}
	}
}

int UKF_index_52[0];

static bool isIinitialized_UKF_index_52 = false;
void initialize_UKF_index_52() {
	if (!isIinitialized_UKF_index_52){
		int i;
		for (i=0; i < 0; i++){
			UKF_index_52[i] = i+1;
		}
		isIinitialized_UKF_index_52 = true;
	}
}

void read_UKF_index_52(int labelAccessStatistics) {
	int numberOfBytes = 1;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(UKF_index_52) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = UKF_index_52[i];
			a = UKF_index_52[i+1];
			a = UKF_index_52[i+2];
			a = UKF_index_52[i+3];
			a = UKF_index_52[i+4];
			a = UKF_index_52[i+5];
			a = UKF_index_52[i+6];
			a = UKF_index_52[i+7];
			a = UKF_index_52[i+8];
			a = UKF_index_52[i+9];
		}
	
		for(;i<arraysize;i++){
			a = UKF_index_52[i];
		}

		(void)a;
	}
}

void write_UKF_index_52(int labelAccessStatistics) {
	int numberOfBytes = 1;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(UKF_index_52) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			UKF_index_52[i]   = 0x800A;
			UKF_index_52[i+1] = 0xAFFE;
			UKF_index_52[i+2] = 0xAFFE;
			UKF_index_52[i+3] = 0xAFFE;
			UKF_index_52[i+4] = 0xAFFE;
			UKF_index_52[i+5] = 0xAFFE;
			UKF_index_52[i+6] = 0xAFFE;
			UKF_index_52[i+7] = 0xAFFE;
			UKF_index_52[i+8] = 0xAFFE;
			UKF_index_52[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				UKF_index_52[i]=0xAFFE;
		}
	}
}

int UKF_index_53[0];

static bool isIinitialized_UKF_index_53 = false;
void initialize_UKF_index_53() {
	if (!isIinitialized_UKF_index_53){
		int i;
		for (i=0; i < 0; i++){
			UKF_index_53[i] = i+1;
		}
		isIinitialized_UKF_index_53 = true;
	}
}

void read_UKF_index_53(int labelAccessStatistics) {
	int numberOfBytes = 1;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(UKF_index_53) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = UKF_index_53[i];
			a = UKF_index_53[i+1];
			a = UKF_index_53[i+2];
			a = UKF_index_53[i+3];
			a = UKF_index_53[i+4];
			a = UKF_index_53[i+5];
			a = UKF_index_53[i+6];
			a = UKF_index_53[i+7];
			a = UKF_index_53[i+8];
			a = UKF_index_53[i+9];
		}
	
		for(;i<arraysize;i++){
			a = UKF_index_53[i];
		}

		(void)a;
	}
}

void write_UKF_index_53(int labelAccessStatistics) {
	int numberOfBytes = 1;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(UKF_index_53) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			UKF_index_53[i]   = 0x800A;
			UKF_index_53[i+1] = 0xAFFE;
			UKF_index_53[i+2] = 0xAFFE;
			UKF_index_53[i+3] = 0xAFFE;
			UKF_index_53[i+4] = 0xAFFE;
			UKF_index_53[i+5] = 0xAFFE;
			UKF_index_53[i+6] = 0xAFFE;
			UKF_index_53[i+7] = 0xAFFE;
			UKF_index_53[i+8] = 0xAFFE;
			UKF_index_53[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				UKF_index_53[i]=0xAFFE;
		}
	}
}

int UKF_index_54[0];

static bool isIinitialized_UKF_index_54 = false;
void initialize_UKF_index_54() {
	if (!isIinitialized_UKF_index_54){
		int i;
		for (i=0; i < 0; i++){
			UKF_index_54[i] = i+1;
		}
		isIinitialized_UKF_index_54 = true;
	}
}

void read_UKF_index_54(int labelAccessStatistics) {
	int numberOfBytes = 1;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(UKF_index_54) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = UKF_index_54[i];
			a = UKF_index_54[i+1];
			a = UKF_index_54[i+2];
			a = UKF_index_54[i+3];
			a = UKF_index_54[i+4];
			a = UKF_index_54[i+5];
			a = UKF_index_54[i+6];
			a = UKF_index_54[i+7];
			a = UKF_index_54[i+8];
			a = UKF_index_54[i+9];
		}
	
		for(;i<arraysize;i++){
			a = UKF_index_54[i];
		}

		(void)a;
	}
}

void write_UKF_index_54(int labelAccessStatistics) {
	int numberOfBytes = 1;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(UKF_index_54) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			UKF_index_54[i]   = 0x800A;
			UKF_index_54[i+1] = 0xAFFE;
			UKF_index_54[i+2] = 0xAFFE;
			UKF_index_54[i+3] = 0xAFFE;
			UKF_index_54[i+4] = 0xAFFE;
			UKF_index_54[i+5] = 0xAFFE;
			UKF_index_54[i+6] = 0xAFFE;
			UKF_index_54[i+7] = 0xAFFE;
			UKF_index_54[i+8] = 0xAFFE;
			UKF_index_54[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				UKF_index_54[i]=0xAFFE;
		}
	}
}

int UKF_index_55[0];

static bool isIinitialized_UKF_index_55 = false;
void initialize_UKF_index_55() {
	if (!isIinitialized_UKF_index_55){
		int i;
		for (i=0; i < 0; i++){
			UKF_index_55[i] = i+1;
		}
		isIinitialized_UKF_index_55 = true;
	}
}

void read_UKF_index_55(int labelAccessStatistics) {
	int numberOfBytes = 1;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(UKF_index_55) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = UKF_index_55[i];
			a = UKF_index_55[i+1];
			a = UKF_index_55[i+2];
			a = UKF_index_55[i+3];
			a = UKF_index_55[i+4];
			a = UKF_index_55[i+5];
			a = UKF_index_55[i+6];
			a = UKF_index_55[i+7];
			a = UKF_index_55[i+8];
			a = UKF_index_55[i+9];
		}
	
		for(;i<arraysize;i++){
			a = UKF_index_55[i];
		}

		(void)a;
	}
}

void write_UKF_index_55(int labelAccessStatistics) {
	int numberOfBytes = 1;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(UKF_index_55) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			UKF_index_55[i]   = 0x800A;
			UKF_index_55[i+1] = 0xAFFE;
			UKF_index_55[i+2] = 0xAFFE;
			UKF_index_55[i+3] = 0xAFFE;
			UKF_index_55[i+4] = 0xAFFE;
			UKF_index_55[i+5] = 0xAFFE;
			UKF_index_55[i+6] = 0xAFFE;
			UKF_index_55[i+7] = 0xAFFE;
			UKF_index_55[i+8] = 0xAFFE;
			UKF_index_55[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				UKF_index_55[i]=0xAFFE;
		}
	}
}

int UKF_index_56[0];

static bool isIinitialized_UKF_index_56 = false;
void initialize_UKF_index_56() {
	if (!isIinitialized_UKF_index_56){
		int i;
		for (i=0; i < 0; i++){
			UKF_index_56[i] = i+1;
		}
		isIinitialized_UKF_index_56 = true;
	}
}

void read_UKF_index_56(int labelAccessStatistics) {
	int numberOfBytes = 1;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(UKF_index_56) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = UKF_index_56[i];
			a = UKF_index_56[i+1];
			a = UKF_index_56[i+2];
			a = UKF_index_56[i+3];
			a = UKF_index_56[i+4];
			a = UKF_index_56[i+5];
			a = UKF_index_56[i+6];
			a = UKF_index_56[i+7];
			a = UKF_index_56[i+8];
			a = UKF_index_56[i+9];
		}
	
		for(;i<arraysize;i++){
			a = UKF_index_56[i];
		}

		(void)a;
	}
}

void write_UKF_index_56(int labelAccessStatistics) {
	int numberOfBytes = 1;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(UKF_index_56) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			UKF_index_56[i]   = 0x800A;
			UKF_index_56[i+1] = 0xAFFE;
			UKF_index_56[i+2] = 0xAFFE;
			UKF_index_56[i+3] = 0xAFFE;
			UKF_index_56[i+4] = 0xAFFE;
			UKF_index_56[i+5] = 0xAFFE;
			UKF_index_56[i+6] = 0xAFFE;
			UKF_index_56[i+7] = 0xAFFE;
			UKF_index_56[i+8] = 0xAFFE;
			UKF_index_56[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				UKF_index_56[i]=0xAFFE;
		}
	}
}

int UKF_index_57[0];

static bool isIinitialized_UKF_index_57 = false;
void initialize_UKF_index_57() {
	if (!isIinitialized_UKF_index_57){
		int i;
		for (i=0; i < 0; i++){
			UKF_index_57[i] = i+1;
		}
		isIinitialized_UKF_index_57 = true;
	}
}

void read_UKF_index_57(int labelAccessStatistics) {
	int numberOfBytes = 1;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(UKF_index_57) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = UKF_index_57[i];
			a = UKF_index_57[i+1];
			a = UKF_index_57[i+2];
			a = UKF_index_57[i+3];
			a = UKF_index_57[i+4];
			a = UKF_index_57[i+5];
			a = UKF_index_57[i+6];
			a = UKF_index_57[i+7];
			a = UKF_index_57[i+8];
			a = UKF_index_57[i+9];
		}
	
		for(;i<arraysize;i++){
			a = UKF_index_57[i];
		}

		(void)a;
	}
}

void write_UKF_index_57(int labelAccessStatistics) {
	int numberOfBytes = 1;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(UKF_index_57) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			UKF_index_57[i]   = 0x800A;
			UKF_index_57[i+1] = 0xAFFE;
			UKF_index_57[i+2] = 0xAFFE;
			UKF_index_57[i+3] = 0xAFFE;
			UKF_index_57[i+4] = 0xAFFE;
			UKF_index_57[i+5] = 0xAFFE;
			UKF_index_57[i+6] = 0xAFFE;
			UKF_index_57[i+7] = 0xAFFE;
			UKF_index_57[i+8] = 0xAFFE;
			UKF_index_57[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				UKF_index_57[i]=0xAFFE;
		}
	}
}

int UKF_index_58[0];

static bool isIinitialized_UKF_index_58 = false;
void initialize_UKF_index_58() {
	if (!isIinitialized_UKF_index_58){
		int i;
		for (i=0; i < 0; i++){
			UKF_index_58[i] = i+1;
		}
		isIinitialized_UKF_index_58 = true;
	}
}

void read_UKF_index_58(int labelAccessStatistics) {
	int numberOfBytes = 1;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(UKF_index_58) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = UKF_index_58[i];
			a = UKF_index_58[i+1];
			a = UKF_index_58[i+2];
			a = UKF_index_58[i+3];
			a = UKF_index_58[i+4];
			a = UKF_index_58[i+5];
			a = UKF_index_58[i+6];
			a = UKF_index_58[i+7];
			a = UKF_index_58[i+8];
			a = UKF_index_58[i+9];
		}
	
		for(;i<arraysize;i++){
			a = UKF_index_58[i];
		}

		(void)a;
	}
}

void write_UKF_index_58(int labelAccessStatistics) {
	int numberOfBytes = 1;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(UKF_index_58) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			UKF_index_58[i]   = 0x800A;
			UKF_index_58[i+1] = 0xAFFE;
			UKF_index_58[i+2] = 0xAFFE;
			UKF_index_58[i+3] = 0xAFFE;
			UKF_index_58[i+4] = 0xAFFE;
			UKF_index_58[i+5] = 0xAFFE;
			UKF_index_58[i+6] = 0xAFFE;
			UKF_index_58[i+7] = 0xAFFE;
			UKF_index_58[i+8] = 0xAFFE;
			UKF_index_58[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				UKF_index_58[i]=0xAFFE;
		}
	}
}

int Potential_obstacle_N58_sub_label[0];

static bool isIinitialized_Potential_obstacle_N58_sub_label = false;
void initialize_Potential_obstacle_N58_sub_label() {
	if (!isIinitialized_Potential_obstacle_N58_sub_label){
		int i;
		for (i=0; i < 0; i++){
			Potential_obstacle_N58_sub_label[i] = i+1;
		}
		isIinitialized_Potential_obstacle_N58_sub_label = true;
	}
}

void read_Potential_obstacle_N58_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(Potential_obstacle_N58_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = Potential_obstacle_N58_sub_label[i];
			a = Potential_obstacle_N58_sub_label[i+1];
			a = Potential_obstacle_N58_sub_label[i+2];
			a = Potential_obstacle_N58_sub_label[i+3];
			a = Potential_obstacle_N58_sub_label[i+4];
			a = Potential_obstacle_N58_sub_label[i+5];
			a = Potential_obstacle_N58_sub_label[i+6];
			a = Potential_obstacle_N58_sub_label[i+7];
			a = Potential_obstacle_N58_sub_label[i+8];
			a = Potential_obstacle_N58_sub_label[i+9];
		}
	
		for(;i<arraysize;i++){
			a = Potential_obstacle_N58_sub_label[i];
		}

		(void)a;
	}
}

void write_Potential_obstacle_N58_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(Potential_obstacle_N58_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			Potential_obstacle_N58_sub_label[i]   = 0x800A;
			Potential_obstacle_N58_sub_label[i+1] = 0xAFFE;
			Potential_obstacle_N58_sub_label[i+2] = 0xAFFE;
			Potential_obstacle_N58_sub_label[i+3] = 0xAFFE;
			Potential_obstacle_N58_sub_label[i+4] = 0xAFFE;
			Potential_obstacle_N58_sub_label[i+5] = 0xAFFE;
			Potential_obstacle_N58_sub_label[i+6] = 0xAFFE;
			Potential_obstacle_N58_sub_label[i+7] = 0xAFFE;
			Potential_obstacle_N58_sub_label[i+8] = 0xAFFE;
			Potential_obstacle_N58_sub_label[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				Potential_obstacle_N58_sub_label[i]=0xAFFE;
		}
	}
}

int Potential_obstacle_N57_sub_label[0];

static bool isIinitialized_Potential_obstacle_N57_sub_label = false;
void initialize_Potential_obstacle_N57_sub_label() {
	if (!isIinitialized_Potential_obstacle_N57_sub_label){
		int i;
		for (i=0; i < 0; i++){
			Potential_obstacle_N57_sub_label[i] = i+1;
		}
		isIinitialized_Potential_obstacle_N57_sub_label = true;
	}
}

void read_Potential_obstacle_N57_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(Potential_obstacle_N57_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = Potential_obstacle_N57_sub_label[i];
			a = Potential_obstacle_N57_sub_label[i+1];
			a = Potential_obstacle_N57_sub_label[i+2];
			a = Potential_obstacle_N57_sub_label[i+3];
			a = Potential_obstacle_N57_sub_label[i+4];
			a = Potential_obstacle_N57_sub_label[i+5];
			a = Potential_obstacle_N57_sub_label[i+6];
			a = Potential_obstacle_N57_sub_label[i+7];
			a = Potential_obstacle_N57_sub_label[i+8];
			a = Potential_obstacle_N57_sub_label[i+9];
		}
	
		for(;i<arraysize;i++){
			a = Potential_obstacle_N57_sub_label[i];
		}

		(void)a;
	}
}

void write_Potential_obstacle_N57_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(Potential_obstacle_N57_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			Potential_obstacle_N57_sub_label[i]   = 0x800A;
			Potential_obstacle_N57_sub_label[i+1] = 0xAFFE;
			Potential_obstacle_N57_sub_label[i+2] = 0xAFFE;
			Potential_obstacle_N57_sub_label[i+3] = 0xAFFE;
			Potential_obstacle_N57_sub_label[i+4] = 0xAFFE;
			Potential_obstacle_N57_sub_label[i+5] = 0xAFFE;
			Potential_obstacle_N57_sub_label[i+6] = 0xAFFE;
			Potential_obstacle_N57_sub_label[i+7] = 0xAFFE;
			Potential_obstacle_N57_sub_label[i+8] = 0xAFFE;
			Potential_obstacle_N57_sub_label[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				Potential_obstacle_N57_sub_label[i]=0xAFFE;
		}
	}
}

int Potential_obstacle_N56_sub_label[0];

static bool isIinitialized_Potential_obstacle_N56_sub_label = false;
void initialize_Potential_obstacle_N56_sub_label() {
	if (!isIinitialized_Potential_obstacle_N56_sub_label){
		int i;
		for (i=0; i < 0; i++){
			Potential_obstacle_N56_sub_label[i] = i+1;
		}
		isIinitialized_Potential_obstacle_N56_sub_label = true;
	}
}

void read_Potential_obstacle_N56_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(Potential_obstacle_N56_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = Potential_obstacle_N56_sub_label[i];
			a = Potential_obstacle_N56_sub_label[i+1];
			a = Potential_obstacle_N56_sub_label[i+2];
			a = Potential_obstacle_N56_sub_label[i+3];
			a = Potential_obstacle_N56_sub_label[i+4];
			a = Potential_obstacle_N56_sub_label[i+5];
			a = Potential_obstacle_N56_sub_label[i+6];
			a = Potential_obstacle_N56_sub_label[i+7];
			a = Potential_obstacle_N56_sub_label[i+8];
			a = Potential_obstacle_N56_sub_label[i+9];
		}
	
		for(;i<arraysize;i++){
			a = Potential_obstacle_N56_sub_label[i];
		}

		(void)a;
	}
}

void write_Potential_obstacle_N56_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(Potential_obstacle_N56_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			Potential_obstacle_N56_sub_label[i]   = 0x800A;
			Potential_obstacle_N56_sub_label[i+1] = 0xAFFE;
			Potential_obstacle_N56_sub_label[i+2] = 0xAFFE;
			Potential_obstacle_N56_sub_label[i+3] = 0xAFFE;
			Potential_obstacle_N56_sub_label[i+4] = 0xAFFE;
			Potential_obstacle_N56_sub_label[i+5] = 0xAFFE;
			Potential_obstacle_N56_sub_label[i+6] = 0xAFFE;
			Potential_obstacle_N56_sub_label[i+7] = 0xAFFE;
			Potential_obstacle_N56_sub_label[i+8] = 0xAFFE;
			Potential_obstacle_N56_sub_label[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				Potential_obstacle_N56_sub_label[i]=0xAFFE;
		}
	}
}

int Potential_obstacle_N55_sub_label[0];

static bool isIinitialized_Potential_obstacle_N55_sub_label = false;
void initialize_Potential_obstacle_N55_sub_label() {
	if (!isIinitialized_Potential_obstacle_N55_sub_label){
		int i;
		for (i=0; i < 0; i++){
			Potential_obstacle_N55_sub_label[i] = i+1;
		}
		isIinitialized_Potential_obstacle_N55_sub_label = true;
	}
}

void read_Potential_obstacle_N55_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(Potential_obstacle_N55_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = Potential_obstacle_N55_sub_label[i];
			a = Potential_obstacle_N55_sub_label[i+1];
			a = Potential_obstacle_N55_sub_label[i+2];
			a = Potential_obstacle_N55_sub_label[i+3];
			a = Potential_obstacle_N55_sub_label[i+4];
			a = Potential_obstacle_N55_sub_label[i+5];
			a = Potential_obstacle_N55_sub_label[i+6];
			a = Potential_obstacle_N55_sub_label[i+7];
			a = Potential_obstacle_N55_sub_label[i+8];
			a = Potential_obstacle_N55_sub_label[i+9];
		}
	
		for(;i<arraysize;i++){
			a = Potential_obstacle_N55_sub_label[i];
		}

		(void)a;
	}
}

void write_Potential_obstacle_N55_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(Potential_obstacle_N55_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			Potential_obstacle_N55_sub_label[i]   = 0x800A;
			Potential_obstacle_N55_sub_label[i+1] = 0xAFFE;
			Potential_obstacle_N55_sub_label[i+2] = 0xAFFE;
			Potential_obstacle_N55_sub_label[i+3] = 0xAFFE;
			Potential_obstacle_N55_sub_label[i+4] = 0xAFFE;
			Potential_obstacle_N55_sub_label[i+5] = 0xAFFE;
			Potential_obstacle_N55_sub_label[i+6] = 0xAFFE;
			Potential_obstacle_N55_sub_label[i+7] = 0xAFFE;
			Potential_obstacle_N55_sub_label[i+8] = 0xAFFE;
			Potential_obstacle_N55_sub_label[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				Potential_obstacle_N55_sub_label[i]=0xAFFE;
		}
	}
}

int Potential_obstacle_N50_sub_label[0];

static bool isIinitialized_Potential_obstacle_N50_sub_label = false;
void initialize_Potential_obstacle_N50_sub_label() {
	if (!isIinitialized_Potential_obstacle_N50_sub_label){
		int i;
		for (i=0; i < 0; i++){
			Potential_obstacle_N50_sub_label[i] = i+1;
		}
		isIinitialized_Potential_obstacle_N50_sub_label = true;
	}
}

void read_Potential_obstacle_N50_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(Potential_obstacle_N50_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = Potential_obstacle_N50_sub_label[i];
			a = Potential_obstacle_N50_sub_label[i+1];
			a = Potential_obstacle_N50_sub_label[i+2];
			a = Potential_obstacle_N50_sub_label[i+3];
			a = Potential_obstacle_N50_sub_label[i+4];
			a = Potential_obstacle_N50_sub_label[i+5];
			a = Potential_obstacle_N50_sub_label[i+6];
			a = Potential_obstacle_N50_sub_label[i+7];
			a = Potential_obstacle_N50_sub_label[i+8];
			a = Potential_obstacle_N50_sub_label[i+9];
		}
	
		for(;i<arraysize;i++){
			a = Potential_obstacle_N50_sub_label[i];
		}

		(void)a;
	}
}

void write_Potential_obstacle_N50_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(Potential_obstacle_N50_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			Potential_obstacle_N50_sub_label[i]   = 0x800A;
			Potential_obstacle_N50_sub_label[i+1] = 0xAFFE;
			Potential_obstacle_N50_sub_label[i+2] = 0xAFFE;
			Potential_obstacle_N50_sub_label[i+3] = 0xAFFE;
			Potential_obstacle_N50_sub_label[i+4] = 0xAFFE;
			Potential_obstacle_N50_sub_label[i+5] = 0xAFFE;
			Potential_obstacle_N50_sub_label[i+6] = 0xAFFE;
			Potential_obstacle_N50_sub_label[i+7] = 0xAFFE;
			Potential_obstacle_N50_sub_label[i+8] = 0xAFFE;
			Potential_obstacle_N50_sub_label[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				Potential_obstacle_N50_sub_label[i]=0xAFFE;
		}
	}
}

int Potential_obstacle_N49_sub_label[0];

static bool isIinitialized_Potential_obstacle_N49_sub_label = false;
void initialize_Potential_obstacle_N49_sub_label() {
	if (!isIinitialized_Potential_obstacle_N49_sub_label){
		int i;
		for (i=0; i < 0; i++){
			Potential_obstacle_N49_sub_label[i] = i+1;
		}
		isIinitialized_Potential_obstacle_N49_sub_label = true;
	}
}

void read_Potential_obstacle_N49_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(Potential_obstacle_N49_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = Potential_obstacle_N49_sub_label[i];
			a = Potential_obstacle_N49_sub_label[i+1];
			a = Potential_obstacle_N49_sub_label[i+2];
			a = Potential_obstacle_N49_sub_label[i+3];
			a = Potential_obstacle_N49_sub_label[i+4];
			a = Potential_obstacle_N49_sub_label[i+5];
			a = Potential_obstacle_N49_sub_label[i+6];
			a = Potential_obstacle_N49_sub_label[i+7];
			a = Potential_obstacle_N49_sub_label[i+8];
			a = Potential_obstacle_N49_sub_label[i+9];
		}
	
		for(;i<arraysize;i++){
			a = Potential_obstacle_N49_sub_label[i];
		}

		(void)a;
	}
}

void write_Potential_obstacle_N49_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(Potential_obstacle_N49_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			Potential_obstacle_N49_sub_label[i]   = 0x800A;
			Potential_obstacle_N49_sub_label[i+1] = 0xAFFE;
			Potential_obstacle_N49_sub_label[i+2] = 0xAFFE;
			Potential_obstacle_N49_sub_label[i+3] = 0xAFFE;
			Potential_obstacle_N49_sub_label[i+4] = 0xAFFE;
			Potential_obstacle_N49_sub_label[i+5] = 0xAFFE;
			Potential_obstacle_N49_sub_label[i+6] = 0xAFFE;
			Potential_obstacle_N49_sub_label[i+7] = 0xAFFE;
			Potential_obstacle_N49_sub_label[i+8] = 0xAFFE;
			Potential_obstacle_N49_sub_label[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				Potential_obstacle_N49_sub_label[i]=0xAFFE;
		}
	}
}

int Potential_obstacle_N48_sub_label[0];

static bool isIinitialized_Potential_obstacle_N48_sub_label = false;
void initialize_Potential_obstacle_N48_sub_label() {
	if (!isIinitialized_Potential_obstacle_N48_sub_label){
		int i;
		for (i=0; i < 0; i++){
			Potential_obstacle_N48_sub_label[i] = i+1;
		}
		isIinitialized_Potential_obstacle_N48_sub_label = true;
	}
}

void read_Potential_obstacle_N48_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(Potential_obstacle_N48_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = Potential_obstacle_N48_sub_label[i];
			a = Potential_obstacle_N48_sub_label[i+1];
			a = Potential_obstacle_N48_sub_label[i+2];
			a = Potential_obstacle_N48_sub_label[i+3];
			a = Potential_obstacle_N48_sub_label[i+4];
			a = Potential_obstacle_N48_sub_label[i+5];
			a = Potential_obstacle_N48_sub_label[i+6];
			a = Potential_obstacle_N48_sub_label[i+7];
			a = Potential_obstacle_N48_sub_label[i+8];
			a = Potential_obstacle_N48_sub_label[i+9];
		}
	
		for(;i<arraysize;i++){
			a = Potential_obstacle_N48_sub_label[i];
		}

		(void)a;
	}
}

void write_Potential_obstacle_N48_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(Potential_obstacle_N48_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			Potential_obstacle_N48_sub_label[i]   = 0x800A;
			Potential_obstacle_N48_sub_label[i+1] = 0xAFFE;
			Potential_obstacle_N48_sub_label[i+2] = 0xAFFE;
			Potential_obstacle_N48_sub_label[i+3] = 0xAFFE;
			Potential_obstacle_N48_sub_label[i+4] = 0xAFFE;
			Potential_obstacle_N48_sub_label[i+5] = 0xAFFE;
			Potential_obstacle_N48_sub_label[i+6] = 0xAFFE;
			Potential_obstacle_N48_sub_label[i+7] = 0xAFFE;
			Potential_obstacle_N48_sub_label[i+8] = 0xAFFE;
			Potential_obstacle_N48_sub_label[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				Potential_obstacle_N48_sub_label[i]=0xAFFE;
		}
	}
}

int Potential_obstacle_N54_sub_label[0];

static bool isIinitialized_Potential_obstacle_N54_sub_label = false;
void initialize_Potential_obstacle_N54_sub_label() {
	if (!isIinitialized_Potential_obstacle_N54_sub_label){
		int i;
		for (i=0; i < 0; i++){
			Potential_obstacle_N54_sub_label[i] = i+1;
		}
		isIinitialized_Potential_obstacle_N54_sub_label = true;
	}
}

void read_Potential_obstacle_N54_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(Potential_obstacle_N54_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = Potential_obstacle_N54_sub_label[i];
			a = Potential_obstacle_N54_sub_label[i+1];
			a = Potential_obstacle_N54_sub_label[i+2];
			a = Potential_obstacle_N54_sub_label[i+3];
			a = Potential_obstacle_N54_sub_label[i+4];
			a = Potential_obstacle_N54_sub_label[i+5];
			a = Potential_obstacle_N54_sub_label[i+6];
			a = Potential_obstacle_N54_sub_label[i+7];
			a = Potential_obstacle_N54_sub_label[i+8];
			a = Potential_obstacle_N54_sub_label[i+9];
		}
	
		for(;i<arraysize;i++){
			a = Potential_obstacle_N54_sub_label[i];
		}

		(void)a;
	}
}

void write_Potential_obstacle_N54_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(Potential_obstacle_N54_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			Potential_obstacle_N54_sub_label[i]   = 0x800A;
			Potential_obstacle_N54_sub_label[i+1] = 0xAFFE;
			Potential_obstacle_N54_sub_label[i+2] = 0xAFFE;
			Potential_obstacle_N54_sub_label[i+3] = 0xAFFE;
			Potential_obstacle_N54_sub_label[i+4] = 0xAFFE;
			Potential_obstacle_N54_sub_label[i+5] = 0xAFFE;
			Potential_obstacle_N54_sub_label[i+6] = 0xAFFE;
			Potential_obstacle_N54_sub_label[i+7] = 0xAFFE;
			Potential_obstacle_N54_sub_label[i+8] = 0xAFFE;
			Potential_obstacle_N54_sub_label[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				Potential_obstacle_N54_sub_label[i]=0xAFFE;
		}
	}
}

int Potential_obstacle_N53_sub_label[0];

static bool isIinitialized_Potential_obstacle_N53_sub_label = false;
void initialize_Potential_obstacle_N53_sub_label() {
	if (!isIinitialized_Potential_obstacle_N53_sub_label){
		int i;
		for (i=0; i < 0; i++){
			Potential_obstacle_N53_sub_label[i] = i+1;
		}
		isIinitialized_Potential_obstacle_N53_sub_label = true;
	}
}

void read_Potential_obstacle_N53_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(Potential_obstacle_N53_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = Potential_obstacle_N53_sub_label[i];
			a = Potential_obstacle_N53_sub_label[i+1];
			a = Potential_obstacle_N53_sub_label[i+2];
			a = Potential_obstacle_N53_sub_label[i+3];
			a = Potential_obstacle_N53_sub_label[i+4];
			a = Potential_obstacle_N53_sub_label[i+5];
			a = Potential_obstacle_N53_sub_label[i+6];
			a = Potential_obstacle_N53_sub_label[i+7];
			a = Potential_obstacle_N53_sub_label[i+8];
			a = Potential_obstacle_N53_sub_label[i+9];
		}
	
		for(;i<arraysize;i++){
			a = Potential_obstacle_N53_sub_label[i];
		}

		(void)a;
	}
}

void write_Potential_obstacle_N53_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(Potential_obstacle_N53_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			Potential_obstacle_N53_sub_label[i]   = 0x800A;
			Potential_obstacle_N53_sub_label[i+1] = 0xAFFE;
			Potential_obstacle_N53_sub_label[i+2] = 0xAFFE;
			Potential_obstacle_N53_sub_label[i+3] = 0xAFFE;
			Potential_obstacle_N53_sub_label[i+4] = 0xAFFE;
			Potential_obstacle_N53_sub_label[i+5] = 0xAFFE;
			Potential_obstacle_N53_sub_label[i+6] = 0xAFFE;
			Potential_obstacle_N53_sub_label[i+7] = 0xAFFE;
			Potential_obstacle_N53_sub_label[i+8] = 0xAFFE;
			Potential_obstacle_N53_sub_label[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				Potential_obstacle_N53_sub_label[i]=0xAFFE;
		}
	}
}

int Potential_obstacle_N52_sub_label[0];

static bool isIinitialized_Potential_obstacle_N52_sub_label = false;
void initialize_Potential_obstacle_N52_sub_label() {
	if (!isIinitialized_Potential_obstacle_N52_sub_label){
		int i;
		for (i=0; i < 0; i++){
			Potential_obstacle_N52_sub_label[i] = i+1;
		}
		isIinitialized_Potential_obstacle_N52_sub_label = true;
	}
}

void read_Potential_obstacle_N52_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(Potential_obstacle_N52_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = Potential_obstacle_N52_sub_label[i];
			a = Potential_obstacle_N52_sub_label[i+1];
			a = Potential_obstacle_N52_sub_label[i+2];
			a = Potential_obstacle_N52_sub_label[i+3];
			a = Potential_obstacle_N52_sub_label[i+4];
			a = Potential_obstacle_N52_sub_label[i+5];
			a = Potential_obstacle_N52_sub_label[i+6];
			a = Potential_obstacle_N52_sub_label[i+7];
			a = Potential_obstacle_N52_sub_label[i+8];
			a = Potential_obstacle_N52_sub_label[i+9];
		}
	
		for(;i<arraysize;i++){
			a = Potential_obstacle_N52_sub_label[i];
		}

		(void)a;
	}
}

void write_Potential_obstacle_N52_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(Potential_obstacle_N52_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			Potential_obstacle_N52_sub_label[i]   = 0x800A;
			Potential_obstacle_N52_sub_label[i+1] = 0xAFFE;
			Potential_obstacle_N52_sub_label[i+2] = 0xAFFE;
			Potential_obstacle_N52_sub_label[i+3] = 0xAFFE;
			Potential_obstacle_N52_sub_label[i+4] = 0xAFFE;
			Potential_obstacle_N52_sub_label[i+5] = 0xAFFE;
			Potential_obstacle_N52_sub_label[i+6] = 0xAFFE;
			Potential_obstacle_N52_sub_label[i+7] = 0xAFFE;
			Potential_obstacle_N52_sub_label[i+8] = 0xAFFE;
			Potential_obstacle_N52_sub_label[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				Potential_obstacle_N52_sub_label[i]=0xAFFE;
		}
	}
}

int Potential_obstacle_N51_sub_label[0];

static bool isIinitialized_Potential_obstacle_N51_sub_label = false;
void initialize_Potential_obstacle_N51_sub_label() {
	if (!isIinitialized_Potential_obstacle_N51_sub_label){
		int i;
		for (i=0; i < 0; i++){
			Potential_obstacle_N51_sub_label[i] = i+1;
		}
		isIinitialized_Potential_obstacle_N51_sub_label = true;
	}
}

void read_Potential_obstacle_N51_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(Potential_obstacle_N51_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = Potential_obstacle_N51_sub_label[i];
			a = Potential_obstacle_N51_sub_label[i+1];
			a = Potential_obstacle_N51_sub_label[i+2];
			a = Potential_obstacle_N51_sub_label[i+3];
			a = Potential_obstacle_N51_sub_label[i+4];
			a = Potential_obstacle_N51_sub_label[i+5];
			a = Potential_obstacle_N51_sub_label[i+6];
			a = Potential_obstacle_N51_sub_label[i+7];
			a = Potential_obstacle_N51_sub_label[i+8];
			a = Potential_obstacle_N51_sub_label[i+9];
		}
	
		for(;i<arraysize;i++){
			a = Potential_obstacle_N51_sub_label[i];
		}

		(void)a;
	}
}

void write_Potential_obstacle_N51_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(Potential_obstacle_N51_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			Potential_obstacle_N51_sub_label[i]   = 0x800A;
			Potential_obstacle_N51_sub_label[i+1] = 0xAFFE;
			Potential_obstacle_N51_sub_label[i+2] = 0xAFFE;
			Potential_obstacle_N51_sub_label[i+3] = 0xAFFE;
			Potential_obstacle_N51_sub_label[i+4] = 0xAFFE;
			Potential_obstacle_N51_sub_label[i+5] = 0xAFFE;
			Potential_obstacle_N51_sub_label[i+6] = 0xAFFE;
			Potential_obstacle_N51_sub_label[i+7] = 0xAFFE;
			Potential_obstacle_N51_sub_label[i+8] = 0xAFFE;
			Potential_obstacle_N51_sub_label[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				Potential_obstacle_N51_sub_label[i]=0xAFFE;
		}
	}
}

int Potential_obstacle_N59_sub_label[0];

static bool isIinitialized_Potential_obstacle_N59_sub_label = false;
void initialize_Potential_obstacle_N59_sub_label() {
	if (!isIinitialized_Potential_obstacle_N59_sub_label){
		int i;
		for (i=0; i < 0; i++){
			Potential_obstacle_N59_sub_label[i] = i+1;
		}
		isIinitialized_Potential_obstacle_N59_sub_label = true;
	}
}

void read_Potential_obstacle_N59_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(Potential_obstacle_N59_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = Potential_obstacle_N59_sub_label[i];
			a = Potential_obstacle_N59_sub_label[i+1];
			a = Potential_obstacle_N59_sub_label[i+2];
			a = Potential_obstacle_N59_sub_label[i+3];
			a = Potential_obstacle_N59_sub_label[i+4];
			a = Potential_obstacle_N59_sub_label[i+5];
			a = Potential_obstacle_N59_sub_label[i+6];
			a = Potential_obstacle_N59_sub_label[i+7];
			a = Potential_obstacle_N59_sub_label[i+8];
			a = Potential_obstacle_N59_sub_label[i+9];
		}
	
		for(;i<arraysize;i++){
			a = Potential_obstacle_N59_sub_label[i];
		}

		(void)a;
	}
}

void write_Potential_obstacle_N59_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(Potential_obstacle_N59_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			Potential_obstacle_N59_sub_label[i]   = 0x800A;
			Potential_obstacle_N59_sub_label[i+1] = 0xAFFE;
			Potential_obstacle_N59_sub_label[i+2] = 0xAFFE;
			Potential_obstacle_N59_sub_label[i+3] = 0xAFFE;
			Potential_obstacle_N59_sub_label[i+4] = 0xAFFE;
			Potential_obstacle_N59_sub_label[i+5] = 0xAFFE;
			Potential_obstacle_N59_sub_label[i+6] = 0xAFFE;
			Potential_obstacle_N59_sub_label[i+7] = 0xAFFE;
			Potential_obstacle_N59_sub_label[i+8] = 0xAFFE;
			Potential_obstacle_N59_sub_label[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				Potential_obstacle_N59_sub_label[i]=0xAFFE;
		}
	}
}

int Potential_obstacle_N46_sub_label[0];

static bool isIinitialized_Potential_obstacle_N46_sub_label = false;
void initialize_Potential_obstacle_N46_sub_label() {
	if (!isIinitialized_Potential_obstacle_N46_sub_label){
		int i;
		for (i=0; i < 0; i++){
			Potential_obstacle_N46_sub_label[i] = i+1;
		}
		isIinitialized_Potential_obstacle_N46_sub_label = true;
	}
}

void read_Potential_obstacle_N46_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(Potential_obstacle_N46_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = Potential_obstacle_N46_sub_label[i];
			a = Potential_obstacle_N46_sub_label[i+1];
			a = Potential_obstacle_N46_sub_label[i+2];
			a = Potential_obstacle_N46_sub_label[i+3];
			a = Potential_obstacle_N46_sub_label[i+4];
			a = Potential_obstacle_N46_sub_label[i+5];
			a = Potential_obstacle_N46_sub_label[i+6];
			a = Potential_obstacle_N46_sub_label[i+7];
			a = Potential_obstacle_N46_sub_label[i+8];
			a = Potential_obstacle_N46_sub_label[i+9];
		}
	
		for(;i<arraysize;i++){
			a = Potential_obstacle_N46_sub_label[i];
		}

		(void)a;
	}
}

void write_Potential_obstacle_N46_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(Potential_obstacle_N46_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			Potential_obstacle_N46_sub_label[i]   = 0x800A;
			Potential_obstacle_N46_sub_label[i+1] = 0xAFFE;
			Potential_obstacle_N46_sub_label[i+2] = 0xAFFE;
			Potential_obstacle_N46_sub_label[i+3] = 0xAFFE;
			Potential_obstacle_N46_sub_label[i+4] = 0xAFFE;
			Potential_obstacle_N46_sub_label[i+5] = 0xAFFE;
			Potential_obstacle_N46_sub_label[i+6] = 0xAFFE;
			Potential_obstacle_N46_sub_label[i+7] = 0xAFFE;
			Potential_obstacle_N46_sub_label[i+8] = 0xAFFE;
			Potential_obstacle_N46_sub_label[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				Potential_obstacle_N46_sub_label[i]=0xAFFE;
		}
	}
}

int Potential_obstacle_N45_sub_label[0];

static bool isIinitialized_Potential_obstacle_N45_sub_label = false;
void initialize_Potential_obstacle_N45_sub_label() {
	if (!isIinitialized_Potential_obstacle_N45_sub_label){
		int i;
		for (i=0; i < 0; i++){
			Potential_obstacle_N45_sub_label[i] = i+1;
		}
		isIinitialized_Potential_obstacle_N45_sub_label = true;
	}
}

void read_Potential_obstacle_N45_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(Potential_obstacle_N45_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = Potential_obstacle_N45_sub_label[i];
			a = Potential_obstacle_N45_sub_label[i+1];
			a = Potential_obstacle_N45_sub_label[i+2];
			a = Potential_obstacle_N45_sub_label[i+3];
			a = Potential_obstacle_N45_sub_label[i+4];
			a = Potential_obstacle_N45_sub_label[i+5];
			a = Potential_obstacle_N45_sub_label[i+6];
			a = Potential_obstacle_N45_sub_label[i+7];
			a = Potential_obstacle_N45_sub_label[i+8];
			a = Potential_obstacle_N45_sub_label[i+9];
		}
	
		for(;i<arraysize;i++){
			a = Potential_obstacle_N45_sub_label[i];
		}

		(void)a;
	}
}

void write_Potential_obstacle_N45_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(Potential_obstacle_N45_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			Potential_obstacle_N45_sub_label[i]   = 0x800A;
			Potential_obstacle_N45_sub_label[i+1] = 0xAFFE;
			Potential_obstacle_N45_sub_label[i+2] = 0xAFFE;
			Potential_obstacle_N45_sub_label[i+3] = 0xAFFE;
			Potential_obstacle_N45_sub_label[i+4] = 0xAFFE;
			Potential_obstacle_N45_sub_label[i+5] = 0xAFFE;
			Potential_obstacle_N45_sub_label[i+6] = 0xAFFE;
			Potential_obstacle_N45_sub_label[i+7] = 0xAFFE;
			Potential_obstacle_N45_sub_label[i+8] = 0xAFFE;
			Potential_obstacle_N45_sub_label[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				Potential_obstacle_N45_sub_label[i]=0xAFFE;
		}
	}
}

int Potential_obstacle_N44_sub_label[0];

static bool isIinitialized_Potential_obstacle_N44_sub_label = false;
void initialize_Potential_obstacle_N44_sub_label() {
	if (!isIinitialized_Potential_obstacle_N44_sub_label){
		int i;
		for (i=0; i < 0; i++){
			Potential_obstacle_N44_sub_label[i] = i+1;
		}
		isIinitialized_Potential_obstacle_N44_sub_label = true;
	}
}

void read_Potential_obstacle_N44_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(Potential_obstacle_N44_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = Potential_obstacle_N44_sub_label[i];
			a = Potential_obstacle_N44_sub_label[i+1];
			a = Potential_obstacle_N44_sub_label[i+2];
			a = Potential_obstacle_N44_sub_label[i+3];
			a = Potential_obstacle_N44_sub_label[i+4];
			a = Potential_obstacle_N44_sub_label[i+5];
			a = Potential_obstacle_N44_sub_label[i+6];
			a = Potential_obstacle_N44_sub_label[i+7];
			a = Potential_obstacle_N44_sub_label[i+8];
			a = Potential_obstacle_N44_sub_label[i+9];
		}
	
		for(;i<arraysize;i++){
			a = Potential_obstacle_N44_sub_label[i];
		}

		(void)a;
	}
}

void write_Potential_obstacle_N44_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(Potential_obstacle_N44_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			Potential_obstacle_N44_sub_label[i]   = 0x800A;
			Potential_obstacle_N44_sub_label[i+1] = 0xAFFE;
			Potential_obstacle_N44_sub_label[i+2] = 0xAFFE;
			Potential_obstacle_N44_sub_label[i+3] = 0xAFFE;
			Potential_obstacle_N44_sub_label[i+4] = 0xAFFE;
			Potential_obstacle_N44_sub_label[i+5] = 0xAFFE;
			Potential_obstacle_N44_sub_label[i+6] = 0xAFFE;
			Potential_obstacle_N44_sub_label[i+7] = 0xAFFE;
			Potential_obstacle_N44_sub_label[i+8] = 0xAFFE;
			Potential_obstacle_N44_sub_label[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				Potential_obstacle_N44_sub_label[i]=0xAFFE;
		}
	}
}

int Potential_obstacle_N43_sub_label[0];

static bool isIinitialized_Potential_obstacle_N43_sub_label = false;
void initialize_Potential_obstacle_N43_sub_label() {
	if (!isIinitialized_Potential_obstacle_N43_sub_label){
		int i;
		for (i=0; i < 0; i++){
			Potential_obstacle_N43_sub_label[i] = i+1;
		}
		isIinitialized_Potential_obstacle_N43_sub_label = true;
	}
}

void read_Potential_obstacle_N43_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(Potential_obstacle_N43_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = Potential_obstacle_N43_sub_label[i];
			a = Potential_obstacle_N43_sub_label[i+1];
			a = Potential_obstacle_N43_sub_label[i+2];
			a = Potential_obstacle_N43_sub_label[i+3];
			a = Potential_obstacle_N43_sub_label[i+4];
			a = Potential_obstacle_N43_sub_label[i+5];
			a = Potential_obstacle_N43_sub_label[i+6];
			a = Potential_obstacle_N43_sub_label[i+7];
			a = Potential_obstacle_N43_sub_label[i+8];
			a = Potential_obstacle_N43_sub_label[i+9];
		}
	
		for(;i<arraysize;i++){
			a = Potential_obstacle_N43_sub_label[i];
		}

		(void)a;
	}
}

void write_Potential_obstacle_N43_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(Potential_obstacle_N43_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			Potential_obstacle_N43_sub_label[i]   = 0x800A;
			Potential_obstacle_N43_sub_label[i+1] = 0xAFFE;
			Potential_obstacle_N43_sub_label[i+2] = 0xAFFE;
			Potential_obstacle_N43_sub_label[i+3] = 0xAFFE;
			Potential_obstacle_N43_sub_label[i+4] = 0xAFFE;
			Potential_obstacle_N43_sub_label[i+5] = 0xAFFE;
			Potential_obstacle_N43_sub_label[i+6] = 0xAFFE;
			Potential_obstacle_N43_sub_label[i+7] = 0xAFFE;
			Potential_obstacle_N43_sub_label[i+8] = 0xAFFE;
			Potential_obstacle_N43_sub_label[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				Potential_obstacle_N43_sub_label[i]=0xAFFE;
		}
	}
}

int Potential_obstacle_N36_sub_label[0];

static bool isIinitialized_Potential_obstacle_N36_sub_label = false;
void initialize_Potential_obstacle_N36_sub_label() {
	if (!isIinitialized_Potential_obstacle_N36_sub_label){
		int i;
		for (i=0; i < 0; i++){
			Potential_obstacle_N36_sub_label[i] = i+1;
		}
		isIinitialized_Potential_obstacle_N36_sub_label = true;
	}
}

void read_Potential_obstacle_N36_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(Potential_obstacle_N36_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = Potential_obstacle_N36_sub_label[i];
			a = Potential_obstacle_N36_sub_label[i+1];
			a = Potential_obstacle_N36_sub_label[i+2];
			a = Potential_obstacle_N36_sub_label[i+3];
			a = Potential_obstacle_N36_sub_label[i+4];
			a = Potential_obstacle_N36_sub_label[i+5];
			a = Potential_obstacle_N36_sub_label[i+6];
			a = Potential_obstacle_N36_sub_label[i+7];
			a = Potential_obstacle_N36_sub_label[i+8];
			a = Potential_obstacle_N36_sub_label[i+9];
		}
	
		for(;i<arraysize;i++){
			a = Potential_obstacle_N36_sub_label[i];
		}

		(void)a;
	}
}

void write_Potential_obstacle_N36_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(Potential_obstacle_N36_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			Potential_obstacle_N36_sub_label[i]   = 0x800A;
			Potential_obstacle_N36_sub_label[i+1] = 0xAFFE;
			Potential_obstacle_N36_sub_label[i+2] = 0xAFFE;
			Potential_obstacle_N36_sub_label[i+3] = 0xAFFE;
			Potential_obstacle_N36_sub_label[i+4] = 0xAFFE;
			Potential_obstacle_N36_sub_label[i+5] = 0xAFFE;
			Potential_obstacle_N36_sub_label[i+6] = 0xAFFE;
			Potential_obstacle_N36_sub_label[i+7] = 0xAFFE;
			Potential_obstacle_N36_sub_label[i+8] = 0xAFFE;
			Potential_obstacle_N36_sub_label[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				Potential_obstacle_N36_sub_label[i]=0xAFFE;
		}
	}
}

int Potential_obstacle_N35_sub_label[0];

static bool isIinitialized_Potential_obstacle_N35_sub_label = false;
void initialize_Potential_obstacle_N35_sub_label() {
	if (!isIinitialized_Potential_obstacle_N35_sub_label){
		int i;
		for (i=0; i < 0; i++){
			Potential_obstacle_N35_sub_label[i] = i+1;
		}
		isIinitialized_Potential_obstacle_N35_sub_label = true;
	}
}

void read_Potential_obstacle_N35_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(Potential_obstacle_N35_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = Potential_obstacle_N35_sub_label[i];
			a = Potential_obstacle_N35_sub_label[i+1];
			a = Potential_obstacle_N35_sub_label[i+2];
			a = Potential_obstacle_N35_sub_label[i+3];
			a = Potential_obstacle_N35_sub_label[i+4];
			a = Potential_obstacle_N35_sub_label[i+5];
			a = Potential_obstacle_N35_sub_label[i+6];
			a = Potential_obstacle_N35_sub_label[i+7];
			a = Potential_obstacle_N35_sub_label[i+8];
			a = Potential_obstacle_N35_sub_label[i+9];
		}
	
		for(;i<arraysize;i++){
			a = Potential_obstacle_N35_sub_label[i];
		}

		(void)a;
	}
}

void write_Potential_obstacle_N35_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(Potential_obstacle_N35_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			Potential_obstacle_N35_sub_label[i]   = 0x800A;
			Potential_obstacle_N35_sub_label[i+1] = 0xAFFE;
			Potential_obstacle_N35_sub_label[i+2] = 0xAFFE;
			Potential_obstacle_N35_sub_label[i+3] = 0xAFFE;
			Potential_obstacle_N35_sub_label[i+4] = 0xAFFE;
			Potential_obstacle_N35_sub_label[i+5] = 0xAFFE;
			Potential_obstacle_N35_sub_label[i+6] = 0xAFFE;
			Potential_obstacle_N35_sub_label[i+7] = 0xAFFE;
			Potential_obstacle_N35_sub_label[i+8] = 0xAFFE;
			Potential_obstacle_N35_sub_label[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				Potential_obstacle_N35_sub_label[i]=0xAFFE;
		}
	}
}

int Potential_obstacle_N34_sub_label[0];

static bool isIinitialized_Potential_obstacle_N34_sub_label = false;
void initialize_Potential_obstacle_N34_sub_label() {
	if (!isIinitialized_Potential_obstacle_N34_sub_label){
		int i;
		for (i=0; i < 0; i++){
			Potential_obstacle_N34_sub_label[i] = i+1;
		}
		isIinitialized_Potential_obstacle_N34_sub_label = true;
	}
}

void read_Potential_obstacle_N34_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(Potential_obstacle_N34_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = Potential_obstacle_N34_sub_label[i];
			a = Potential_obstacle_N34_sub_label[i+1];
			a = Potential_obstacle_N34_sub_label[i+2];
			a = Potential_obstacle_N34_sub_label[i+3];
			a = Potential_obstacle_N34_sub_label[i+4];
			a = Potential_obstacle_N34_sub_label[i+5];
			a = Potential_obstacle_N34_sub_label[i+6];
			a = Potential_obstacle_N34_sub_label[i+7];
			a = Potential_obstacle_N34_sub_label[i+8];
			a = Potential_obstacle_N34_sub_label[i+9];
		}
	
		for(;i<arraysize;i++){
			a = Potential_obstacle_N34_sub_label[i];
		}

		(void)a;
	}
}

void write_Potential_obstacle_N34_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(Potential_obstacle_N34_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			Potential_obstacle_N34_sub_label[i]   = 0x800A;
			Potential_obstacle_N34_sub_label[i+1] = 0xAFFE;
			Potential_obstacle_N34_sub_label[i+2] = 0xAFFE;
			Potential_obstacle_N34_sub_label[i+3] = 0xAFFE;
			Potential_obstacle_N34_sub_label[i+4] = 0xAFFE;
			Potential_obstacle_N34_sub_label[i+5] = 0xAFFE;
			Potential_obstacle_N34_sub_label[i+6] = 0xAFFE;
			Potential_obstacle_N34_sub_label[i+7] = 0xAFFE;
			Potential_obstacle_N34_sub_label[i+8] = 0xAFFE;
			Potential_obstacle_N34_sub_label[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				Potential_obstacle_N34_sub_label[i]=0xAFFE;
		}
	}
}

int Potential_obstacle_N33_sub_label[0];

static bool isIinitialized_Potential_obstacle_N33_sub_label = false;
void initialize_Potential_obstacle_N33_sub_label() {
	if (!isIinitialized_Potential_obstacle_N33_sub_label){
		int i;
		for (i=0; i < 0; i++){
			Potential_obstacle_N33_sub_label[i] = i+1;
		}
		isIinitialized_Potential_obstacle_N33_sub_label = true;
	}
}

void read_Potential_obstacle_N33_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(Potential_obstacle_N33_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = Potential_obstacle_N33_sub_label[i];
			a = Potential_obstacle_N33_sub_label[i+1];
			a = Potential_obstacle_N33_sub_label[i+2];
			a = Potential_obstacle_N33_sub_label[i+3];
			a = Potential_obstacle_N33_sub_label[i+4];
			a = Potential_obstacle_N33_sub_label[i+5];
			a = Potential_obstacle_N33_sub_label[i+6];
			a = Potential_obstacle_N33_sub_label[i+7];
			a = Potential_obstacle_N33_sub_label[i+8];
			a = Potential_obstacle_N33_sub_label[i+9];
		}
	
		for(;i<arraysize;i++){
			a = Potential_obstacle_N33_sub_label[i];
		}

		(void)a;
	}
}

void write_Potential_obstacle_N33_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(Potential_obstacle_N33_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			Potential_obstacle_N33_sub_label[i]   = 0x800A;
			Potential_obstacle_N33_sub_label[i+1] = 0xAFFE;
			Potential_obstacle_N33_sub_label[i+2] = 0xAFFE;
			Potential_obstacle_N33_sub_label[i+3] = 0xAFFE;
			Potential_obstacle_N33_sub_label[i+4] = 0xAFFE;
			Potential_obstacle_N33_sub_label[i+5] = 0xAFFE;
			Potential_obstacle_N33_sub_label[i+6] = 0xAFFE;
			Potential_obstacle_N33_sub_label[i+7] = 0xAFFE;
			Potential_obstacle_N33_sub_label[i+8] = 0xAFFE;
			Potential_obstacle_N33_sub_label[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				Potential_obstacle_N33_sub_label[i]=0xAFFE;
		}
	}
}

int Potential_obstacle_N39_sub_label[0];

static bool isIinitialized_Potential_obstacle_N39_sub_label = false;
void initialize_Potential_obstacle_N39_sub_label() {
	if (!isIinitialized_Potential_obstacle_N39_sub_label){
		int i;
		for (i=0; i < 0; i++){
			Potential_obstacle_N39_sub_label[i] = i+1;
		}
		isIinitialized_Potential_obstacle_N39_sub_label = true;
	}
}

void read_Potential_obstacle_N39_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(Potential_obstacle_N39_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = Potential_obstacle_N39_sub_label[i];
			a = Potential_obstacle_N39_sub_label[i+1];
			a = Potential_obstacle_N39_sub_label[i+2];
			a = Potential_obstacle_N39_sub_label[i+3];
			a = Potential_obstacle_N39_sub_label[i+4];
			a = Potential_obstacle_N39_sub_label[i+5];
			a = Potential_obstacle_N39_sub_label[i+6];
			a = Potential_obstacle_N39_sub_label[i+7];
			a = Potential_obstacle_N39_sub_label[i+8];
			a = Potential_obstacle_N39_sub_label[i+9];
		}
	
		for(;i<arraysize;i++){
			a = Potential_obstacle_N39_sub_label[i];
		}

		(void)a;
	}
}

void write_Potential_obstacle_N39_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(Potential_obstacle_N39_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			Potential_obstacle_N39_sub_label[i]   = 0x800A;
			Potential_obstacle_N39_sub_label[i+1] = 0xAFFE;
			Potential_obstacle_N39_sub_label[i+2] = 0xAFFE;
			Potential_obstacle_N39_sub_label[i+3] = 0xAFFE;
			Potential_obstacle_N39_sub_label[i+4] = 0xAFFE;
			Potential_obstacle_N39_sub_label[i+5] = 0xAFFE;
			Potential_obstacle_N39_sub_label[i+6] = 0xAFFE;
			Potential_obstacle_N39_sub_label[i+7] = 0xAFFE;
			Potential_obstacle_N39_sub_label[i+8] = 0xAFFE;
			Potential_obstacle_N39_sub_label[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				Potential_obstacle_N39_sub_label[i]=0xAFFE;
		}
	}
}

int Potential_obstacle_N42_sub_label[0];

static bool isIinitialized_Potential_obstacle_N42_sub_label = false;
void initialize_Potential_obstacle_N42_sub_label() {
	if (!isIinitialized_Potential_obstacle_N42_sub_label){
		int i;
		for (i=0; i < 0; i++){
			Potential_obstacle_N42_sub_label[i] = i+1;
		}
		isIinitialized_Potential_obstacle_N42_sub_label = true;
	}
}

void read_Potential_obstacle_N42_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(Potential_obstacle_N42_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = Potential_obstacle_N42_sub_label[i];
			a = Potential_obstacle_N42_sub_label[i+1];
			a = Potential_obstacle_N42_sub_label[i+2];
			a = Potential_obstacle_N42_sub_label[i+3];
			a = Potential_obstacle_N42_sub_label[i+4];
			a = Potential_obstacle_N42_sub_label[i+5];
			a = Potential_obstacle_N42_sub_label[i+6];
			a = Potential_obstacle_N42_sub_label[i+7];
			a = Potential_obstacle_N42_sub_label[i+8];
			a = Potential_obstacle_N42_sub_label[i+9];
		}
	
		for(;i<arraysize;i++){
			a = Potential_obstacle_N42_sub_label[i];
		}

		(void)a;
	}
}

void write_Potential_obstacle_N42_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(Potential_obstacle_N42_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			Potential_obstacle_N42_sub_label[i]   = 0x800A;
			Potential_obstacle_N42_sub_label[i+1] = 0xAFFE;
			Potential_obstacle_N42_sub_label[i+2] = 0xAFFE;
			Potential_obstacle_N42_sub_label[i+3] = 0xAFFE;
			Potential_obstacle_N42_sub_label[i+4] = 0xAFFE;
			Potential_obstacle_N42_sub_label[i+5] = 0xAFFE;
			Potential_obstacle_N42_sub_label[i+6] = 0xAFFE;
			Potential_obstacle_N42_sub_label[i+7] = 0xAFFE;
			Potential_obstacle_N42_sub_label[i+8] = 0xAFFE;
			Potential_obstacle_N42_sub_label[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				Potential_obstacle_N42_sub_label[i]=0xAFFE;
		}
	}
}

int Potential_obstacle_N41_sub_label[0];

static bool isIinitialized_Potential_obstacle_N41_sub_label = false;
void initialize_Potential_obstacle_N41_sub_label() {
	if (!isIinitialized_Potential_obstacle_N41_sub_label){
		int i;
		for (i=0; i < 0; i++){
			Potential_obstacle_N41_sub_label[i] = i+1;
		}
		isIinitialized_Potential_obstacle_N41_sub_label = true;
	}
}

void read_Potential_obstacle_N41_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(Potential_obstacle_N41_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = Potential_obstacle_N41_sub_label[i];
			a = Potential_obstacle_N41_sub_label[i+1];
			a = Potential_obstacle_N41_sub_label[i+2];
			a = Potential_obstacle_N41_sub_label[i+3];
			a = Potential_obstacle_N41_sub_label[i+4];
			a = Potential_obstacle_N41_sub_label[i+5];
			a = Potential_obstacle_N41_sub_label[i+6];
			a = Potential_obstacle_N41_sub_label[i+7];
			a = Potential_obstacle_N41_sub_label[i+8];
			a = Potential_obstacle_N41_sub_label[i+9];
		}
	
		for(;i<arraysize;i++){
			a = Potential_obstacle_N41_sub_label[i];
		}

		(void)a;
	}
}

void write_Potential_obstacle_N41_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(Potential_obstacle_N41_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			Potential_obstacle_N41_sub_label[i]   = 0x800A;
			Potential_obstacle_N41_sub_label[i+1] = 0xAFFE;
			Potential_obstacle_N41_sub_label[i+2] = 0xAFFE;
			Potential_obstacle_N41_sub_label[i+3] = 0xAFFE;
			Potential_obstacle_N41_sub_label[i+4] = 0xAFFE;
			Potential_obstacle_N41_sub_label[i+5] = 0xAFFE;
			Potential_obstacle_N41_sub_label[i+6] = 0xAFFE;
			Potential_obstacle_N41_sub_label[i+7] = 0xAFFE;
			Potential_obstacle_N41_sub_label[i+8] = 0xAFFE;
			Potential_obstacle_N41_sub_label[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				Potential_obstacle_N41_sub_label[i]=0xAFFE;
		}
	}
}

int Potential_obstacle_N38_sub_label[0];

static bool isIinitialized_Potential_obstacle_N38_sub_label = false;
void initialize_Potential_obstacle_N38_sub_label() {
	if (!isIinitialized_Potential_obstacle_N38_sub_label){
		int i;
		for (i=0; i < 0; i++){
			Potential_obstacle_N38_sub_label[i] = i+1;
		}
		isIinitialized_Potential_obstacle_N38_sub_label = true;
	}
}

void read_Potential_obstacle_N38_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(Potential_obstacle_N38_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = Potential_obstacle_N38_sub_label[i];
			a = Potential_obstacle_N38_sub_label[i+1];
			a = Potential_obstacle_N38_sub_label[i+2];
			a = Potential_obstacle_N38_sub_label[i+3];
			a = Potential_obstacle_N38_sub_label[i+4];
			a = Potential_obstacle_N38_sub_label[i+5];
			a = Potential_obstacle_N38_sub_label[i+6];
			a = Potential_obstacle_N38_sub_label[i+7];
			a = Potential_obstacle_N38_sub_label[i+8];
			a = Potential_obstacle_N38_sub_label[i+9];
		}
	
		for(;i<arraysize;i++){
			a = Potential_obstacle_N38_sub_label[i];
		}

		(void)a;
	}
}

void write_Potential_obstacle_N38_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(Potential_obstacle_N38_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			Potential_obstacle_N38_sub_label[i]   = 0x800A;
			Potential_obstacle_N38_sub_label[i+1] = 0xAFFE;
			Potential_obstacle_N38_sub_label[i+2] = 0xAFFE;
			Potential_obstacle_N38_sub_label[i+3] = 0xAFFE;
			Potential_obstacle_N38_sub_label[i+4] = 0xAFFE;
			Potential_obstacle_N38_sub_label[i+5] = 0xAFFE;
			Potential_obstacle_N38_sub_label[i+6] = 0xAFFE;
			Potential_obstacle_N38_sub_label[i+7] = 0xAFFE;
			Potential_obstacle_N38_sub_label[i+8] = 0xAFFE;
			Potential_obstacle_N38_sub_label[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				Potential_obstacle_N38_sub_label[i]=0xAFFE;
		}
	}
}

int Potential_obstacle_N37_sub_label[0];

static bool isIinitialized_Potential_obstacle_N37_sub_label = false;
void initialize_Potential_obstacle_N37_sub_label() {
	if (!isIinitialized_Potential_obstacle_N37_sub_label){
		int i;
		for (i=0; i < 0; i++){
			Potential_obstacle_N37_sub_label[i] = i+1;
		}
		isIinitialized_Potential_obstacle_N37_sub_label = true;
	}
}

void read_Potential_obstacle_N37_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(Potential_obstacle_N37_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = Potential_obstacle_N37_sub_label[i];
			a = Potential_obstacle_N37_sub_label[i+1];
			a = Potential_obstacle_N37_sub_label[i+2];
			a = Potential_obstacle_N37_sub_label[i+3];
			a = Potential_obstacle_N37_sub_label[i+4];
			a = Potential_obstacle_N37_sub_label[i+5];
			a = Potential_obstacle_N37_sub_label[i+6];
			a = Potential_obstacle_N37_sub_label[i+7];
			a = Potential_obstacle_N37_sub_label[i+8];
			a = Potential_obstacle_N37_sub_label[i+9];
		}
	
		for(;i<arraysize;i++){
			a = Potential_obstacle_N37_sub_label[i];
		}

		(void)a;
	}
}

void write_Potential_obstacle_N37_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(Potential_obstacle_N37_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			Potential_obstacle_N37_sub_label[i]   = 0x800A;
			Potential_obstacle_N37_sub_label[i+1] = 0xAFFE;
			Potential_obstacle_N37_sub_label[i+2] = 0xAFFE;
			Potential_obstacle_N37_sub_label[i+3] = 0xAFFE;
			Potential_obstacle_N37_sub_label[i+4] = 0xAFFE;
			Potential_obstacle_N37_sub_label[i+5] = 0xAFFE;
			Potential_obstacle_N37_sub_label[i+6] = 0xAFFE;
			Potential_obstacle_N37_sub_label[i+7] = 0xAFFE;
			Potential_obstacle_N37_sub_label[i+8] = 0xAFFE;
			Potential_obstacle_N37_sub_label[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				Potential_obstacle_N37_sub_label[i]=0xAFFE;
		}
	}
}

int Potential_obstacle_N47_sub_label[0];

static bool isIinitialized_Potential_obstacle_N47_sub_label = false;
void initialize_Potential_obstacle_N47_sub_label() {
	if (!isIinitialized_Potential_obstacle_N47_sub_label){
		int i;
		for (i=0; i < 0; i++){
			Potential_obstacle_N47_sub_label[i] = i+1;
		}
		isIinitialized_Potential_obstacle_N47_sub_label = true;
	}
}

void read_Potential_obstacle_N47_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(Potential_obstacle_N47_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = Potential_obstacle_N47_sub_label[i];
			a = Potential_obstacle_N47_sub_label[i+1];
			a = Potential_obstacle_N47_sub_label[i+2];
			a = Potential_obstacle_N47_sub_label[i+3];
			a = Potential_obstacle_N47_sub_label[i+4];
			a = Potential_obstacle_N47_sub_label[i+5];
			a = Potential_obstacle_N47_sub_label[i+6];
			a = Potential_obstacle_N47_sub_label[i+7];
			a = Potential_obstacle_N47_sub_label[i+8];
			a = Potential_obstacle_N47_sub_label[i+9];
		}
	
		for(;i<arraysize;i++){
			a = Potential_obstacle_N47_sub_label[i];
		}

		(void)a;
	}
}

void write_Potential_obstacle_N47_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(Potential_obstacle_N47_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			Potential_obstacle_N47_sub_label[i]   = 0x800A;
			Potential_obstacle_N47_sub_label[i+1] = 0xAFFE;
			Potential_obstacle_N47_sub_label[i+2] = 0xAFFE;
			Potential_obstacle_N47_sub_label[i+3] = 0xAFFE;
			Potential_obstacle_N47_sub_label[i+4] = 0xAFFE;
			Potential_obstacle_N47_sub_label[i+5] = 0xAFFE;
			Potential_obstacle_N47_sub_label[i+6] = 0xAFFE;
			Potential_obstacle_N47_sub_label[i+7] = 0xAFFE;
			Potential_obstacle_N47_sub_label[i+8] = 0xAFFE;
			Potential_obstacle_N47_sub_label[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				Potential_obstacle_N47_sub_label[i]=0xAFFE;
		}
	}
}

int Potential_obstacle_N32_sub_label[0];

static bool isIinitialized_Potential_obstacle_N32_sub_label = false;
void initialize_Potential_obstacle_N32_sub_label() {
	if (!isIinitialized_Potential_obstacle_N32_sub_label){
		int i;
		for (i=0; i < 0; i++){
			Potential_obstacle_N32_sub_label[i] = i+1;
		}
		isIinitialized_Potential_obstacle_N32_sub_label = true;
	}
}

void read_Potential_obstacle_N32_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(Potential_obstacle_N32_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = Potential_obstacle_N32_sub_label[i];
			a = Potential_obstacle_N32_sub_label[i+1];
			a = Potential_obstacle_N32_sub_label[i+2];
			a = Potential_obstacle_N32_sub_label[i+3];
			a = Potential_obstacle_N32_sub_label[i+4];
			a = Potential_obstacle_N32_sub_label[i+5];
			a = Potential_obstacle_N32_sub_label[i+6];
			a = Potential_obstacle_N32_sub_label[i+7];
			a = Potential_obstacle_N32_sub_label[i+8];
			a = Potential_obstacle_N32_sub_label[i+9];
		}
	
		for(;i<arraysize;i++){
			a = Potential_obstacle_N32_sub_label[i];
		}

		(void)a;
	}
}

void write_Potential_obstacle_N32_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(Potential_obstacle_N32_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			Potential_obstacle_N32_sub_label[i]   = 0x800A;
			Potential_obstacle_N32_sub_label[i+1] = 0xAFFE;
			Potential_obstacle_N32_sub_label[i+2] = 0xAFFE;
			Potential_obstacle_N32_sub_label[i+3] = 0xAFFE;
			Potential_obstacle_N32_sub_label[i+4] = 0xAFFE;
			Potential_obstacle_N32_sub_label[i+5] = 0xAFFE;
			Potential_obstacle_N32_sub_label[i+6] = 0xAFFE;
			Potential_obstacle_N32_sub_label[i+7] = 0xAFFE;
			Potential_obstacle_N32_sub_label[i+8] = 0xAFFE;
			Potential_obstacle_N32_sub_label[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				Potential_obstacle_N32_sub_label[i]=0xAFFE;
		}
	}
}

int Potential_obstacle_N40_sub_label[0];

static bool isIinitialized_Potential_obstacle_N40_sub_label = false;
void initialize_Potential_obstacle_N40_sub_label() {
	if (!isIinitialized_Potential_obstacle_N40_sub_label){
		int i;
		for (i=0; i < 0; i++){
			Potential_obstacle_N40_sub_label[i] = i+1;
		}
		isIinitialized_Potential_obstacle_N40_sub_label = true;
	}
}

void read_Potential_obstacle_N40_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(Potential_obstacle_N40_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = Potential_obstacle_N40_sub_label[i];
			a = Potential_obstacle_N40_sub_label[i+1];
			a = Potential_obstacle_N40_sub_label[i+2];
			a = Potential_obstacle_N40_sub_label[i+3];
			a = Potential_obstacle_N40_sub_label[i+4];
			a = Potential_obstacle_N40_sub_label[i+5];
			a = Potential_obstacle_N40_sub_label[i+6];
			a = Potential_obstacle_N40_sub_label[i+7];
			a = Potential_obstacle_N40_sub_label[i+8];
			a = Potential_obstacle_N40_sub_label[i+9];
		}
	
		for(;i<arraysize;i++){
			a = Potential_obstacle_N40_sub_label[i];
		}

		(void)a;
	}
}

void write_Potential_obstacle_N40_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(Potential_obstacle_N40_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			Potential_obstacle_N40_sub_label[i]   = 0x800A;
			Potential_obstacle_N40_sub_label[i+1] = 0xAFFE;
			Potential_obstacle_N40_sub_label[i+2] = 0xAFFE;
			Potential_obstacle_N40_sub_label[i+3] = 0xAFFE;
			Potential_obstacle_N40_sub_label[i+4] = 0xAFFE;
			Potential_obstacle_N40_sub_label[i+5] = 0xAFFE;
			Potential_obstacle_N40_sub_label[i+6] = 0xAFFE;
			Potential_obstacle_N40_sub_label[i+7] = 0xAFFE;
			Potential_obstacle_N40_sub_label[i+8] = 0xAFFE;
			Potential_obstacle_N40_sub_label[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				Potential_obstacle_N40_sub_label[i]=0xAFFE;
		}
	}
}

int Potential_obstacle_N30_sub_label[0];

static bool isIinitialized_Potential_obstacle_N30_sub_label = false;
void initialize_Potential_obstacle_N30_sub_label() {
	if (!isIinitialized_Potential_obstacle_N30_sub_label){
		int i;
		for (i=0; i < 0; i++){
			Potential_obstacle_N30_sub_label[i] = i+1;
		}
		isIinitialized_Potential_obstacle_N30_sub_label = true;
	}
}

void read_Potential_obstacle_N30_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(Potential_obstacle_N30_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = Potential_obstacle_N30_sub_label[i];
			a = Potential_obstacle_N30_sub_label[i+1];
			a = Potential_obstacle_N30_sub_label[i+2];
			a = Potential_obstacle_N30_sub_label[i+3];
			a = Potential_obstacle_N30_sub_label[i+4];
			a = Potential_obstacle_N30_sub_label[i+5];
			a = Potential_obstacle_N30_sub_label[i+6];
			a = Potential_obstacle_N30_sub_label[i+7];
			a = Potential_obstacle_N30_sub_label[i+8];
			a = Potential_obstacle_N30_sub_label[i+9];
		}
	
		for(;i<arraysize;i++){
			a = Potential_obstacle_N30_sub_label[i];
		}

		(void)a;
	}
}

void write_Potential_obstacle_N30_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(Potential_obstacle_N30_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			Potential_obstacle_N30_sub_label[i]   = 0x800A;
			Potential_obstacle_N30_sub_label[i+1] = 0xAFFE;
			Potential_obstacle_N30_sub_label[i+2] = 0xAFFE;
			Potential_obstacle_N30_sub_label[i+3] = 0xAFFE;
			Potential_obstacle_N30_sub_label[i+4] = 0xAFFE;
			Potential_obstacle_N30_sub_label[i+5] = 0xAFFE;
			Potential_obstacle_N30_sub_label[i+6] = 0xAFFE;
			Potential_obstacle_N30_sub_label[i+7] = 0xAFFE;
			Potential_obstacle_N30_sub_label[i+8] = 0xAFFE;
			Potential_obstacle_N30_sub_label[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				Potential_obstacle_N30_sub_label[i]=0xAFFE;
		}
	}
}

int Potential_obstacle_N29_sub_label[0];

static bool isIinitialized_Potential_obstacle_N29_sub_label = false;
void initialize_Potential_obstacle_N29_sub_label() {
	if (!isIinitialized_Potential_obstacle_N29_sub_label){
		int i;
		for (i=0; i < 0; i++){
			Potential_obstacle_N29_sub_label[i] = i+1;
		}
		isIinitialized_Potential_obstacle_N29_sub_label = true;
	}
}

void read_Potential_obstacle_N29_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(Potential_obstacle_N29_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = Potential_obstacle_N29_sub_label[i];
			a = Potential_obstacle_N29_sub_label[i+1];
			a = Potential_obstacle_N29_sub_label[i+2];
			a = Potential_obstacle_N29_sub_label[i+3];
			a = Potential_obstacle_N29_sub_label[i+4];
			a = Potential_obstacle_N29_sub_label[i+5];
			a = Potential_obstacle_N29_sub_label[i+6];
			a = Potential_obstacle_N29_sub_label[i+7];
			a = Potential_obstacle_N29_sub_label[i+8];
			a = Potential_obstacle_N29_sub_label[i+9];
		}
	
		for(;i<arraysize;i++){
			a = Potential_obstacle_N29_sub_label[i];
		}

		(void)a;
	}
}

void write_Potential_obstacle_N29_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(Potential_obstacle_N29_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			Potential_obstacle_N29_sub_label[i]   = 0x800A;
			Potential_obstacle_N29_sub_label[i+1] = 0xAFFE;
			Potential_obstacle_N29_sub_label[i+2] = 0xAFFE;
			Potential_obstacle_N29_sub_label[i+3] = 0xAFFE;
			Potential_obstacle_N29_sub_label[i+4] = 0xAFFE;
			Potential_obstacle_N29_sub_label[i+5] = 0xAFFE;
			Potential_obstacle_N29_sub_label[i+6] = 0xAFFE;
			Potential_obstacle_N29_sub_label[i+7] = 0xAFFE;
			Potential_obstacle_N29_sub_label[i+8] = 0xAFFE;
			Potential_obstacle_N29_sub_label[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				Potential_obstacle_N29_sub_label[i]=0xAFFE;
		}
	}
}

int Potential_obstacle_N28_sub_label[0];

static bool isIinitialized_Potential_obstacle_N28_sub_label = false;
void initialize_Potential_obstacle_N28_sub_label() {
	if (!isIinitialized_Potential_obstacle_N28_sub_label){
		int i;
		for (i=0; i < 0; i++){
			Potential_obstacle_N28_sub_label[i] = i+1;
		}
		isIinitialized_Potential_obstacle_N28_sub_label = true;
	}
}

void read_Potential_obstacle_N28_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(Potential_obstacle_N28_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = Potential_obstacle_N28_sub_label[i];
			a = Potential_obstacle_N28_sub_label[i+1];
			a = Potential_obstacle_N28_sub_label[i+2];
			a = Potential_obstacle_N28_sub_label[i+3];
			a = Potential_obstacle_N28_sub_label[i+4];
			a = Potential_obstacle_N28_sub_label[i+5];
			a = Potential_obstacle_N28_sub_label[i+6];
			a = Potential_obstacle_N28_sub_label[i+7];
			a = Potential_obstacle_N28_sub_label[i+8];
			a = Potential_obstacle_N28_sub_label[i+9];
		}
	
		for(;i<arraysize;i++){
			a = Potential_obstacle_N28_sub_label[i];
		}

		(void)a;
	}
}

void write_Potential_obstacle_N28_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(Potential_obstacle_N28_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			Potential_obstacle_N28_sub_label[i]   = 0x800A;
			Potential_obstacle_N28_sub_label[i+1] = 0xAFFE;
			Potential_obstacle_N28_sub_label[i+2] = 0xAFFE;
			Potential_obstacle_N28_sub_label[i+3] = 0xAFFE;
			Potential_obstacle_N28_sub_label[i+4] = 0xAFFE;
			Potential_obstacle_N28_sub_label[i+5] = 0xAFFE;
			Potential_obstacle_N28_sub_label[i+6] = 0xAFFE;
			Potential_obstacle_N28_sub_label[i+7] = 0xAFFE;
			Potential_obstacle_N28_sub_label[i+8] = 0xAFFE;
			Potential_obstacle_N28_sub_label[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				Potential_obstacle_N28_sub_label[i]=0xAFFE;
		}
	}
}

int Potential_obstacle_N27_sub_label[0];

static bool isIinitialized_Potential_obstacle_N27_sub_label = false;
void initialize_Potential_obstacle_N27_sub_label() {
	if (!isIinitialized_Potential_obstacle_N27_sub_label){
		int i;
		for (i=0; i < 0; i++){
			Potential_obstacle_N27_sub_label[i] = i+1;
		}
		isIinitialized_Potential_obstacle_N27_sub_label = true;
	}
}

void read_Potential_obstacle_N27_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(Potential_obstacle_N27_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = Potential_obstacle_N27_sub_label[i];
			a = Potential_obstacle_N27_sub_label[i+1];
			a = Potential_obstacle_N27_sub_label[i+2];
			a = Potential_obstacle_N27_sub_label[i+3];
			a = Potential_obstacle_N27_sub_label[i+4];
			a = Potential_obstacle_N27_sub_label[i+5];
			a = Potential_obstacle_N27_sub_label[i+6];
			a = Potential_obstacle_N27_sub_label[i+7];
			a = Potential_obstacle_N27_sub_label[i+8];
			a = Potential_obstacle_N27_sub_label[i+9];
		}
	
		for(;i<arraysize;i++){
			a = Potential_obstacle_N27_sub_label[i];
		}

		(void)a;
	}
}

void write_Potential_obstacle_N27_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(Potential_obstacle_N27_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			Potential_obstacle_N27_sub_label[i]   = 0x800A;
			Potential_obstacle_N27_sub_label[i+1] = 0xAFFE;
			Potential_obstacle_N27_sub_label[i+2] = 0xAFFE;
			Potential_obstacle_N27_sub_label[i+3] = 0xAFFE;
			Potential_obstacle_N27_sub_label[i+4] = 0xAFFE;
			Potential_obstacle_N27_sub_label[i+5] = 0xAFFE;
			Potential_obstacle_N27_sub_label[i+6] = 0xAFFE;
			Potential_obstacle_N27_sub_label[i+7] = 0xAFFE;
			Potential_obstacle_N27_sub_label[i+8] = 0xAFFE;
			Potential_obstacle_N27_sub_label[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				Potential_obstacle_N27_sub_label[i]=0xAFFE;
		}
	}
}

int Potential_obstacle_N20_sub_label[0];

static bool isIinitialized_Potential_obstacle_N20_sub_label = false;
void initialize_Potential_obstacle_N20_sub_label() {
	if (!isIinitialized_Potential_obstacle_N20_sub_label){
		int i;
		for (i=0; i < 0; i++){
			Potential_obstacle_N20_sub_label[i] = i+1;
		}
		isIinitialized_Potential_obstacle_N20_sub_label = true;
	}
}

void read_Potential_obstacle_N20_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(Potential_obstacle_N20_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = Potential_obstacle_N20_sub_label[i];
			a = Potential_obstacle_N20_sub_label[i+1];
			a = Potential_obstacle_N20_sub_label[i+2];
			a = Potential_obstacle_N20_sub_label[i+3];
			a = Potential_obstacle_N20_sub_label[i+4];
			a = Potential_obstacle_N20_sub_label[i+5];
			a = Potential_obstacle_N20_sub_label[i+6];
			a = Potential_obstacle_N20_sub_label[i+7];
			a = Potential_obstacle_N20_sub_label[i+8];
			a = Potential_obstacle_N20_sub_label[i+9];
		}
	
		for(;i<arraysize;i++){
			a = Potential_obstacle_N20_sub_label[i];
		}

		(void)a;
	}
}

void write_Potential_obstacle_N20_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(Potential_obstacle_N20_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			Potential_obstacle_N20_sub_label[i]   = 0x800A;
			Potential_obstacle_N20_sub_label[i+1] = 0xAFFE;
			Potential_obstacle_N20_sub_label[i+2] = 0xAFFE;
			Potential_obstacle_N20_sub_label[i+3] = 0xAFFE;
			Potential_obstacle_N20_sub_label[i+4] = 0xAFFE;
			Potential_obstacle_N20_sub_label[i+5] = 0xAFFE;
			Potential_obstacle_N20_sub_label[i+6] = 0xAFFE;
			Potential_obstacle_N20_sub_label[i+7] = 0xAFFE;
			Potential_obstacle_N20_sub_label[i+8] = 0xAFFE;
			Potential_obstacle_N20_sub_label[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				Potential_obstacle_N20_sub_label[i]=0xAFFE;
		}
	}
}

int Potential_obstacle_N19_sub_label[0];

static bool isIinitialized_Potential_obstacle_N19_sub_label = false;
void initialize_Potential_obstacle_N19_sub_label() {
	if (!isIinitialized_Potential_obstacle_N19_sub_label){
		int i;
		for (i=0; i < 0; i++){
			Potential_obstacle_N19_sub_label[i] = i+1;
		}
		isIinitialized_Potential_obstacle_N19_sub_label = true;
	}
}

void read_Potential_obstacle_N19_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(Potential_obstacle_N19_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = Potential_obstacle_N19_sub_label[i];
			a = Potential_obstacle_N19_sub_label[i+1];
			a = Potential_obstacle_N19_sub_label[i+2];
			a = Potential_obstacle_N19_sub_label[i+3];
			a = Potential_obstacle_N19_sub_label[i+4];
			a = Potential_obstacle_N19_sub_label[i+5];
			a = Potential_obstacle_N19_sub_label[i+6];
			a = Potential_obstacle_N19_sub_label[i+7];
			a = Potential_obstacle_N19_sub_label[i+8];
			a = Potential_obstacle_N19_sub_label[i+9];
		}
	
		for(;i<arraysize;i++){
			a = Potential_obstacle_N19_sub_label[i];
		}

		(void)a;
	}
}

void write_Potential_obstacle_N19_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(Potential_obstacle_N19_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			Potential_obstacle_N19_sub_label[i]   = 0x800A;
			Potential_obstacle_N19_sub_label[i+1] = 0xAFFE;
			Potential_obstacle_N19_sub_label[i+2] = 0xAFFE;
			Potential_obstacle_N19_sub_label[i+3] = 0xAFFE;
			Potential_obstacle_N19_sub_label[i+4] = 0xAFFE;
			Potential_obstacle_N19_sub_label[i+5] = 0xAFFE;
			Potential_obstacle_N19_sub_label[i+6] = 0xAFFE;
			Potential_obstacle_N19_sub_label[i+7] = 0xAFFE;
			Potential_obstacle_N19_sub_label[i+8] = 0xAFFE;
			Potential_obstacle_N19_sub_label[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				Potential_obstacle_N19_sub_label[i]=0xAFFE;
		}
	}
}

int Potential_obstacle_N18_sub_label[0];

static bool isIinitialized_Potential_obstacle_N18_sub_label = false;
void initialize_Potential_obstacle_N18_sub_label() {
	if (!isIinitialized_Potential_obstacle_N18_sub_label){
		int i;
		for (i=0; i < 0; i++){
			Potential_obstacle_N18_sub_label[i] = i+1;
		}
		isIinitialized_Potential_obstacle_N18_sub_label = true;
	}
}

void read_Potential_obstacle_N18_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(Potential_obstacle_N18_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = Potential_obstacle_N18_sub_label[i];
			a = Potential_obstacle_N18_sub_label[i+1];
			a = Potential_obstacle_N18_sub_label[i+2];
			a = Potential_obstacle_N18_sub_label[i+3];
			a = Potential_obstacle_N18_sub_label[i+4];
			a = Potential_obstacle_N18_sub_label[i+5];
			a = Potential_obstacle_N18_sub_label[i+6];
			a = Potential_obstacle_N18_sub_label[i+7];
			a = Potential_obstacle_N18_sub_label[i+8];
			a = Potential_obstacle_N18_sub_label[i+9];
		}
	
		for(;i<arraysize;i++){
			a = Potential_obstacle_N18_sub_label[i];
		}

		(void)a;
	}
}

void write_Potential_obstacle_N18_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(Potential_obstacle_N18_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			Potential_obstacle_N18_sub_label[i]   = 0x800A;
			Potential_obstacle_N18_sub_label[i+1] = 0xAFFE;
			Potential_obstacle_N18_sub_label[i+2] = 0xAFFE;
			Potential_obstacle_N18_sub_label[i+3] = 0xAFFE;
			Potential_obstacle_N18_sub_label[i+4] = 0xAFFE;
			Potential_obstacle_N18_sub_label[i+5] = 0xAFFE;
			Potential_obstacle_N18_sub_label[i+6] = 0xAFFE;
			Potential_obstacle_N18_sub_label[i+7] = 0xAFFE;
			Potential_obstacle_N18_sub_label[i+8] = 0xAFFE;
			Potential_obstacle_N18_sub_label[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				Potential_obstacle_N18_sub_label[i]=0xAFFE;
		}
	}
}

int Potential_obstacle_N17_sub_label[0];

static bool isIinitialized_Potential_obstacle_N17_sub_label = false;
void initialize_Potential_obstacle_N17_sub_label() {
	if (!isIinitialized_Potential_obstacle_N17_sub_label){
		int i;
		for (i=0; i < 0; i++){
			Potential_obstacle_N17_sub_label[i] = i+1;
		}
		isIinitialized_Potential_obstacle_N17_sub_label = true;
	}
}

void read_Potential_obstacle_N17_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(Potential_obstacle_N17_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = Potential_obstacle_N17_sub_label[i];
			a = Potential_obstacle_N17_sub_label[i+1];
			a = Potential_obstacle_N17_sub_label[i+2];
			a = Potential_obstacle_N17_sub_label[i+3];
			a = Potential_obstacle_N17_sub_label[i+4];
			a = Potential_obstacle_N17_sub_label[i+5];
			a = Potential_obstacle_N17_sub_label[i+6];
			a = Potential_obstacle_N17_sub_label[i+7];
			a = Potential_obstacle_N17_sub_label[i+8];
			a = Potential_obstacle_N17_sub_label[i+9];
		}
	
		for(;i<arraysize;i++){
			a = Potential_obstacle_N17_sub_label[i];
		}

		(void)a;
	}
}

void write_Potential_obstacle_N17_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(Potential_obstacle_N17_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			Potential_obstacle_N17_sub_label[i]   = 0x800A;
			Potential_obstacle_N17_sub_label[i+1] = 0xAFFE;
			Potential_obstacle_N17_sub_label[i+2] = 0xAFFE;
			Potential_obstacle_N17_sub_label[i+3] = 0xAFFE;
			Potential_obstacle_N17_sub_label[i+4] = 0xAFFE;
			Potential_obstacle_N17_sub_label[i+5] = 0xAFFE;
			Potential_obstacle_N17_sub_label[i+6] = 0xAFFE;
			Potential_obstacle_N17_sub_label[i+7] = 0xAFFE;
			Potential_obstacle_N17_sub_label[i+8] = 0xAFFE;
			Potential_obstacle_N17_sub_label[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				Potential_obstacle_N17_sub_label[i]=0xAFFE;
		}
	}
}

int Potential_obstacle_N23_sub_label[0];

static bool isIinitialized_Potential_obstacle_N23_sub_label = false;
void initialize_Potential_obstacle_N23_sub_label() {
	if (!isIinitialized_Potential_obstacle_N23_sub_label){
		int i;
		for (i=0; i < 0; i++){
			Potential_obstacle_N23_sub_label[i] = i+1;
		}
		isIinitialized_Potential_obstacle_N23_sub_label = true;
	}
}

void read_Potential_obstacle_N23_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(Potential_obstacle_N23_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = Potential_obstacle_N23_sub_label[i];
			a = Potential_obstacle_N23_sub_label[i+1];
			a = Potential_obstacle_N23_sub_label[i+2];
			a = Potential_obstacle_N23_sub_label[i+3];
			a = Potential_obstacle_N23_sub_label[i+4];
			a = Potential_obstacle_N23_sub_label[i+5];
			a = Potential_obstacle_N23_sub_label[i+6];
			a = Potential_obstacle_N23_sub_label[i+7];
			a = Potential_obstacle_N23_sub_label[i+8];
			a = Potential_obstacle_N23_sub_label[i+9];
		}
	
		for(;i<arraysize;i++){
			a = Potential_obstacle_N23_sub_label[i];
		}

		(void)a;
	}
}

void write_Potential_obstacle_N23_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(Potential_obstacle_N23_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			Potential_obstacle_N23_sub_label[i]   = 0x800A;
			Potential_obstacle_N23_sub_label[i+1] = 0xAFFE;
			Potential_obstacle_N23_sub_label[i+2] = 0xAFFE;
			Potential_obstacle_N23_sub_label[i+3] = 0xAFFE;
			Potential_obstacle_N23_sub_label[i+4] = 0xAFFE;
			Potential_obstacle_N23_sub_label[i+5] = 0xAFFE;
			Potential_obstacle_N23_sub_label[i+6] = 0xAFFE;
			Potential_obstacle_N23_sub_label[i+7] = 0xAFFE;
			Potential_obstacle_N23_sub_label[i+8] = 0xAFFE;
			Potential_obstacle_N23_sub_label[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				Potential_obstacle_N23_sub_label[i]=0xAFFE;
		}
	}
}

int Potential_obstacle_N26_sub_label[0];

static bool isIinitialized_Potential_obstacle_N26_sub_label = false;
void initialize_Potential_obstacle_N26_sub_label() {
	if (!isIinitialized_Potential_obstacle_N26_sub_label){
		int i;
		for (i=0; i < 0; i++){
			Potential_obstacle_N26_sub_label[i] = i+1;
		}
		isIinitialized_Potential_obstacle_N26_sub_label = true;
	}
}

void read_Potential_obstacle_N26_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(Potential_obstacle_N26_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = Potential_obstacle_N26_sub_label[i];
			a = Potential_obstacle_N26_sub_label[i+1];
			a = Potential_obstacle_N26_sub_label[i+2];
			a = Potential_obstacle_N26_sub_label[i+3];
			a = Potential_obstacle_N26_sub_label[i+4];
			a = Potential_obstacle_N26_sub_label[i+5];
			a = Potential_obstacle_N26_sub_label[i+6];
			a = Potential_obstacle_N26_sub_label[i+7];
			a = Potential_obstacle_N26_sub_label[i+8];
			a = Potential_obstacle_N26_sub_label[i+9];
		}
	
		for(;i<arraysize;i++){
			a = Potential_obstacle_N26_sub_label[i];
		}

		(void)a;
	}
}

void write_Potential_obstacle_N26_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(Potential_obstacle_N26_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			Potential_obstacle_N26_sub_label[i]   = 0x800A;
			Potential_obstacle_N26_sub_label[i+1] = 0xAFFE;
			Potential_obstacle_N26_sub_label[i+2] = 0xAFFE;
			Potential_obstacle_N26_sub_label[i+3] = 0xAFFE;
			Potential_obstacle_N26_sub_label[i+4] = 0xAFFE;
			Potential_obstacle_N26_sub_label[i+5] = 0xAFFE;
			Potential_obstacle_N26_sub_label[i+6] = 0xAFFE;
			Potential_obstacle_N26_sub_label[i+7] = 0xAFFE;
			Potential_obstacle_N26_sub_label[i+8] = 0xAFFE;
			Potential_obstacle_N26_sub_label[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				Potential_obstacle_N26_sub_label[i]=0xAFFE;
		}
	}
}

int Potential_obstacle_N25_sub_label[0];

static bool isIinitialized_Potential_obstacle_N25_sub_label = false;
void initialize_Potential_obstacle_N25_sub_label() {
	if (!isIinitialized_Potential_obstacle_N25_sub_label){
		int i;
		for (i=0; i < 0; i++){
			Potential_obstacle_N25_sub_label[i] = i+1;
		}
		isIinitialized_Potential_obstacle_N25_sub_label = true;
	}
}

void read_Potential_obstacle_N25_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(Potential_obstacle_N25_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = Potential_obstacle_N25_sub_label[i];
			a = Potential_obstacle_N25_sub_label[i+1];
			a = Potential_obstacle_N25_sub_label[i+2];
			a = Potential_obstacle_N25_sub_label[i+3];
			a = Potential_obstacle_N25_sub_label[i+4];
			a = Potential_obstacle_N25_sub_label[i+5];
			a = Potential_obstacle_N25_sub_label[i+6];
			a = Potential_obstacle_N25_sub_label[i+7];
			a = Potential_obstacle_N25_sub_label[i+8];
			a = Potential_obstacle_N25_sub_label[i+9];
		}
	
		for(;i<arraysize;i++){
			a = Potential_obstacle_N25_sub_label[i];
		}

		(void)a;
	}
}

void write_Potential_obstacle_N25_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(Potential_obstacle_N25_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			Potential_obstacle_N25_sub_label[i]   = 0x800A;
			Potential_obstacle_N25_sub_label[i+1] = 0xAFFE;
			Potential_obstacle_N25_sub_label[i+2] = 0xAFFE;
			Potential_obstacle_N25_sub_label[i+3] = 0xAFFE;
			Potential_obstacle_N25_sub_label[i+4] = 0xAFFE;
			Potential_obstacle_N25_sub_label[i+5] = 0xAFFE;
			Potential_obstacle_N25_sub_label[i+6] = 0xAFFE;
			Potential_obstacle_N25_sub_label[i+7] = 0xAFFE;
			Potential_obstacle_N25_sub_label[i+8] = 0xAFFE;
			Potential_obstacle_N25_sub_label[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				Potential_obstacle_N25_sub_label[i]=0xAFFE;
		}
	}
}

int Potential_obstacle_N22_sub_label[0];

static bool isIinitialized_Potential_obstacle_N22_sub_label = false;
void initialize_Potential_obstacle_N22_sub_label() {
	if (!isIinitialized_Potential_obstacle_N22_sub_label){
		int i;
		for (i=0; i < 0; i++){
			Potential_obstacle_N22_sub_label[i] = i+1;
		}
		isIinitialized_Potential_obstacle_N22_sub_label = true;
	}
}

void read_Potential_obstacle_N22_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(Potential_obstacle_N22_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = Potential_obstacle_N22_sub_label[i];
			a = Potential_obstacle_N22_sub_label[i+1];
			a = Potential_obstacle_N22_sub_label[i+2];
			a = Potential_obstacle_N22_sub_label[i+3];
			a = Potential_obstacle_N22_sub_label[i+4];
			a = Potential_obstacle_N22_sub_label[i+5];
			a = Potential_obstacle_N22_sub_label[i+6];
			a = Potential_obstacle_N22_sub_label[i+7];
			a = Potential_obstacle_N22_sub_label[i+8];
			a = Potential_obstacle_N22_sub_label[i+9];
		}
	
		for(;i<arraysize;i++){
			a = Potential_obstacle_N22_sub_label[i];
		}

		(void)a;
	}
}

void write_Potential_obstacle_N22_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(Potential_obstacle_N22_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			Potential_obstacle_N22_sub_label[i]   = 0x800A;
			Potential_obstacle_N22_sub_label[i+1] = 0xAFFE;
			Potential_obstacle_N22_sub_label[i+2] = 0xAFFE;
			Potential_obstacle_N22_sub_label[i+3] = 0xAFFE;
			Potential_obstacle_N22_sub_label[i+4] = 0xAFFE;
			Potential_obstacle_N22_sub_label[i+5] = 0xAFFE;
			Potential_obstacle_N22_sub_label[i+6] = 0xAFFE;
			Potential_obstacle_N22_sub_label[i+7] = 0xAFFE;
			Potential_obstacle_N22_sub_label[i+8] = 0xAFFE;
			Potential_obstacle_N22_sub_label[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				Potential_obstacle_N22_sub_label[i]=0xAFFE;
		}
	}
}

int Potential_obstacle_N21_sub_label[0];

static bool isIinitialized_Potential_obstacle_N21_sub_label = false;
void initialize_Potential_obstacle_N21_sub_label() {
	if (!isIinitialized_Potential_obstacle_N21_sub_label){
		int i;
		for (i=0; i < 0; i++){
			Potential_obstacle_N21_sub_label[i] = i+1;
		}
		isIinitialized_Potential_obstacle_N21_sub_label = true;
	}
}

void read_Potential_obstacle_N21_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(Potential_obstacle_N21_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = Potential_obstacle_N21_sub_label[i];
			a = Potential_obstacle_N21_sub_label[i+1];
			a = Potential_obstacle_N21_sub_label[i+2];
			a = Potential_obstacle_N21_sub_label[i+3];
			a = Potential_obstacle_N21_sub_label[i+4];
			a = Potential_obstacle_N21_sub_label[i+5];
			a = Potential_obstacle_N21_sub_label[i+6];
			a = Potential_obstacle_N21_sub_label[i+7];
			a = Potential_obstacle_N21_sub_label[i+8];
			a = Potential_obstacle_N21_sub_label[i+9];
		}
	
		for(;i<arraysize;i++){
			a = Potential_obstacle_N21_sub_label[i];
		}

		(void)a;
	}
}

void write_Potential_obstacle_N21_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(Potential_obstacle_N21_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			Potential_obstacle_N21_sub_label[i]   = 0x800A;
			Potential_obstacle_N21_sub_label[i+1] = 0xAFFE;
			Potential_obstacle_N21_sub_label[i+2] = 0xAFFE;
			Potential_obstacle_N21_sub_label[i+3] = 0xAFFE;
			Potential_obstacle_N21_sub_label[i+4] = 0xAFFE;
			Potential_obstacle_N21_sub_label[i+5] = 0xAFFE;
			Potential_obstacle_N21_sub_label[i+6] = 0xAFFE;
			Potential_obstacle_N21_sub_label[i+7] = 0xAFFE;
			Potential_obstacle_N21_sub_label[i+8] = 0xAFFE;
			Potential_obstacle_N21_sub_label[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				Potential_obstacle_N21_sub_label[i]=0xAFFE;
		}
	}
}

int Potential_obstacle_N31_sub_label[0];

static bool isIinitialized_Potential_obstacle_N31_sub_label = false;
void initialize_Potential_obstacle_N31_sub_label() {
	if (!isIinitialized_Potential_obstacle_N31_sub_label){
		int i;
		for (i=0; i < 0; i++){
			Potential_obstacle_N31_sub_label[i] = i+1;
		}
		isIinitialized_Potential_obstacle_N31_sub_label = true;
	}
}

void read_Potential_obstacle_N31_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(Potential_obstacle_N31_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = Potential_obstacle_N31_sub_label[i];
			a = Potential_obstacle_N31_sub_label[i+1];
			a = Potential_obstacle_N31_sub_label[i+2];
			a = Potential_obstacle_N31_sub_label[i+3];
			a = Potential_obstacle_N31_sub_label[i+4];
			a = Potential_obstacle_N31_sub_label[i+5];
			a = Potential_obstacle_N31_sub_label[i+6];
			a = Potential_obstacle_N31_sub_label[i+7];
			a = Potential_obstacle_N31_sub_label[i+8];
			a = Potential_obstacle_N31_sub_label[i+9];
		}
	
		for(;i<arraysize;i++){
			a = Potential_obstacle_N31_sub_label[i];
		}

		(void)a;
	}
}

void write_Potential_obstacle_N31_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(Potential_obstacle_N31_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			Potential_obstacle_N31_sub_label[i]   = 0x800A;
			Potential_obstacle_N31_sub_label[i+1] = 0xAFFE;
			Potential_obstacle_N31_sub_label[i+2] = 0xAFFE;
			Potential_obstacle_N31_sub_label[i+3] = 0xAFFE;
			Potential_obstacle_N31_sub_label[i+4] = 0xAFFE;
			Potential_obstacle_N31_sub_label[i+5] = 0xAFFE;
			Potential_obstacle_N31_sub_label[i+6] = 0xAFFE;
			Potential_obstacle_N31_sub_label[i+7] = 0xAFFE;
			Potential_obstacle_N31_sub_label[i+8] = 0xAFFE;
			Potential_obstacle_N31_sub_label[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				Potential_obstacle_N31_sub_label[i]=0xAFFE;
		}
	}
}

int Potential_obstacle_N16_sub_label[0];

static bool isIinitialized_Potential_obstacle_N16_sub_label = false;
void initialize_Potential_obstacle_N16_sub_label() {
	if (!isIinitialized_Potential_obstacle_N16_sub_label){
		int i;
		for (i=0; i < 0; i++){
			Potential_obstacle_N16_sub_label[i] = i+1;
		}
		isIinitialized_Potential_obstacle_N16_sub_label = true;
	}
}

void read_Potential_obstacle_N16_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(Potential_obstacle_N16_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = Potential_obstacle_N16_sub_label[i];
			a = Potential_obstacle_N16_sub_label[i+1];
			a = Potential_obstacle_N16_sub_label[i+2];
			a = Potential_obstacle_N16_sub_label[i+3];
			a = Potential_obstacle_N16_sub_label[i+4];
			a = Potential_obstacle_N16_sub_label[i+5];
			a = Potential_obstacle_N16_sub_label[i+6];
			a = Potential_obstacle_N16_sub_label[i+7];
			a = Potential_obstacle_N16_sub_label[i+8];
			a = Potential_obstacle_N16_sub_label[i+9];
		}
	
		for(;i<arraysize;i++){
			a = Potential_obstacle_N16_sub_label[i];
		}

		(void)a;
	}
}

void write_Potential_obstacle_N16_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(Potential_obstacle_N16_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			Potential_obstacle_N16_sub_label[i]   = 0x800A;
			Potential_obstacle_N16_sub_label[i+1] = 0xAFFE;
			Potential_obstacle_N16_sub_label[i+2] = 0xAFFE;
			Potential_obstacle_N16_sub_label[i+3] = 0xAFFE;
			Potential_obstacle_N16_sub_label[i+4] = 0xAFFE;
			Potential_obstacle_N16_sub_label[i+5] = 0xAFFE;
			Potential_obstacle_N16_sub_label[i+6] = 0xAFFE;
			Potential_obstacle_N16_sub_label[i+7] = 0xAFFE;
			Potential_obstacle_N16_sub_label[i+8] = 0xAFFE;
			Potential_obstacle_N16_sub_label[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				Potential_obstacle_N16_sub_label[i]=0xAFFE;
		}
	}
}

int Potential_obstacle_N24_sub_label[0];

static bool isIinitialized_Potential_obstacle_N24_sub_label = false;
void initialize_Potential_obstacle_N24_sub_label() {
	if (!isIinitialized_Potential_obstacle_N24_sub_label){
		int i;
		for (i=0; i < 0; i++){
			Potential_obstacle_N24_sub_label[i] = i+1;
		}
		isIinitialized_Potential_obstacle_N24_sub_label = true;
	}
}

void read_Potential_obstacle_N24_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(Potential_obstacle_N24_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = Potential_obstacle_N24_sub_label[i];
			a = Potential_obstacle_N24_sub_label[i+1];
			a = Potential_obstacle_N24_sub_label[i+2];
			a = Potential_obstacle_N24_sub_label[i+3];
			a = Potential_obstacle_N24_sub_label[i+4];
			a = Potential_obstacle_N24_sub_label[i+5];
			a = Potential_obstacle_N24_sub_label[i+6];
			a = Potential_obstacle_N24_sub_label[i+7];
			a = Potential_obstacle_N24_sub_label[i+8];
			a = Potential_obstacle_N24_sub_label[i+9];
		}
	
		for(;i<arraysize;i++){
			a = Potential_obstacle_N24_sub_label[i];
		}

		(void)a;
	}
}

void write_Potential_obstacle_N24_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(Potential_obstacle_N24_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			Potential_obstacle_N24_sub_label[i]   = 0x800A;
			Potential_obstacle_N24_sub_label[i+1] = 0xAFFE;
			Potential_obstacle_N24_sub_label[i+2] = 0xAFFE;
			Potential_obstacle_N24_sub_label[i+3] = 0xAFFE;
			Potential_obstacle_N24_sub_label[i+4] = 0xAFFE;
			Potential_obstacle_N24_sub_label[i+5] = 0xAFFE;
			Potential_obstacle_N24_sub_label[i+6] = 0xAFFE;
			Potential_obstacle_N24_sub_label[i+7] = 0xAFFE;
			Potential_obstacle_N24_sub_label[i+8] = 0xAFFE;
			Potential_obstacle_N24_sub_label[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				Potential_obstacle_N24_sub_label[i]=0xAFFE;
		}
	}
}

int Potential_obstacle_N14_sub_label[0];

static bool isIinitialized_Potential_obstacle_N14_sub_label = false;
void initialize_Potential_obstacle_N14_sub_label() {
	if (!isIinitialized_Potential_obstacle_N14_sub_label){
		int i;
		for (i=0; i < 0; i++){
			Potential_obstacle_N14_sub_label[i] = i+1;
		}
		isIinitialized_Potential_obstacle_N14_sub_label = true;
	}
}

void read_Potential_obstacle_N14_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(Potential_obstacle_N14_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = Potential_obstacle_N14_sub_label[i];
			a = Potential_obstacle_N14_sub_label[i+1];
			a = Potential_obstacle_N14_sub_label[i+2];
			a = Potential_obstacle_N14_sub_label[i+3];
			a = Potential_obstacle_N14_sub_label[i+4];
			a = Potential_obstacle_N14_sub_label[i+5];
			a = Potential_obstacle_N14_sub_label[i+6];
			a = Potential_obstacle_N14_sub_label[i+7];
			a = Potential_obstacle_N14_sub_label[i+8];
			a = Potential_obstacle_N14_sub_label[i+9];
		}
	
		for(;i<arraysize;i++){
			a = Potential_obstacle_N14_sub_label[i];
		}

		(void)a;
	}
}

void write_Potential_obstacle_N14_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(Potential_obstacle_N14_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			Potential_obstacle_N14_sub_label[i]   = 0x800A;
			Potential_obstacle_N14_sub_label[i+1] = 0xAFFE;
			Potential_obstacle_N14_sub_label[i+2] = 0xAFFE;
			Potential_obstacle_N14_sub_label[i+3] = 0xAFFE;
			Potential_obstacle_N14_sub_label[i+4] = 0xAFFE;
			Potential_obstacle_N14_sub_label[i+5] = 0xAFFE;
			Potential_obstacle_N14_sub_label[i+6] = 0xAFFE;
			Potential_obstacle_N14_sub_label[i+7] = 0xAFFE;
			Potential_obstacle_N14_sub_label[i+8] = 0xAFFE;
			Potential_obstacle_N14_sub_label[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				Potential_obstacle_N14_sub_label[i]=0xAFFE;
		}
	}
}

int Potential_obstacle_N13_sub_label[0];

static bool isIinitialized_Potential_obstacle_N13_sub_label = false;
void initialize_Potential_obstacle_N13_sub_label() {
	if (!isIinitialized_Potential_obstacle_N13_sub_label){
		int i;
		for (i=0; i < 0; i++){
			Potential_obstacle_N13_sub_label[i] = i+1;
		}
		isIinitialized_Potential_obstacle_N13_sub_label = true;
	}
}

void read_Potential_obstacle_N13_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(Potential_obstacle_N13_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = Potential_obstacle_N13_sub_label[i];
			a = Potential_obstacle_N13_sub_label[i+1];
			a = Potential_obstacle_N13_sub_label[i+2];
			a = Potential_obstacle_N13_sub_label[i+3];
			a = Potential_obstacle_N13_sub_label[i+4];
			a = Potential_obstacle_N13_sub_label[i+5];
			a = Potential_obstacle_N13_sub_label[i+6];
			a = Potential_obstacle_N13_sub_label[i+7];
			a = Potential_obstacle_N13_sub_label[i+8];
			a = Potential_obstacle_N13_sub_label[i+9];
		}
	
		for(;i<arraysize;i++){
			a = Potential_obstacle_N13_sub_label[i];
		}

		(void)a;
	}
}

void write_Potential_obstacle_N13_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(Potential_obstacle_N13_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			Potential_obstacle_N13_sub_label[i]   = 0x800A;
			Potential_obstacle_N13_sub_label[i+1] = 0xAFFE;
			Potential_obstacle_N13_sub_label[i+2] = 0xAFFE;
			Potential_obstacle_N13_sub_label[i+3] = 0xAFFE;
			Potential_obstacle_N13_sub_label[i+4] = 0xAFFE;
			Potential_obstacle_N13_sub_label[i+5] = 0xAFFE;
			Potential_obstacle_N13_sub_label[i+6] = 0xAFFE;
			Potential_obstacle_N13_sub_label[i+7] = 0xAFFE;
			Potential_obstacle_N13_sub_label[i+8] = 0xAFFE;
			Potential_obstacle_N13_sub_label[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				Potential_obstacle_N13_sub_label[i]=0xAFFE;
		}
	}
}

int Potential_obstacle_N12_sub_label[0];

static bool isIinitialized_Potential_obstacle_N12_sub_label = false;
void initialize_Potential_obstacle_N12_sub_label() {
	if (!isIinitialized_Potential_obstacle_N12_sub_label){
		int i;
		for (i=0; i < 0; i++){
			Potential_obstacle_N12_sub_label[i] = i+1;
		}
		isIinitialized_Potential_obstacle_N12_sub_label = true;
	}
}

void read_Potential_obstacle_N12_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(Potential_obstacle_N12_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = Potential_obstacle_N12_sub_label[i];
			a = Potential_obstacle_N12_sub_label[i+1];
			a = Potential_obstacle_N12_sub_label[i+2];
			a = Potential_obstacle_N12_sub_label[i+3];
			a = Potential_obstacle_N12_sub_label[i+4];
			a = Potential_obstacle_N12_sub_label[i+5];
			a = Potential_obstacle_N12_sub_label[i+6];
			a = Potential_obstacle_N12_sub_label[i+7];
			a = Potential_obstacle_N12_sub_label[i+8];
			a = Potential_obstacle_N12_sub_label[i+9];
		}
	
		for(;i<arraysize;i++){
			a = Potential_obstacle_N12_sub_label[i];
		}

		(void)a;
	}
}

void write_Potential_obstacle_N12_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(Potential_obstacle_N12_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			Potential_obstacle_N12_sub_label[i]   = 0x800A;
			Potential_obstacle_N12_sub_label[i+1] = 0xAFFE;
			Potential_obstacle_N12_sub_label[i+2] = 0xAFFE;
			Potential_obstacle_N12_sub_label[i+3] = 0xAFFE;
			Potential_obstacle_N12_sub_label[i+4] = 0xAFFE;
			Potential_obstacle_N12_sub_label[i+5] = 0xAFFE;
			Potential_obstacle_N12_sub_label[i+6] = 0xAFFE;
			Potential_obstacle_N12_sub_label[i+7] = 0xAFFE;
			Potential_obstacle_N12_sub_label[i+8] = 0xAFFE;
			Potential_obstacle_N12_sub_label[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				Potential_obstacle_N12_sub_label[i]=0xAFFE;
		}
	}
}

int Potential_obstacle_N11_sub_label[0];

static bool isIinitialized_Potential_obstacle_N11_sub_label = false;
void initialize_Potential_obstacle_N11_sub_label() {
	if (!isIinitialized_Potential_obstacle_N11_sub_label){
		int i;
		for (i=0; i < 0; i++){
			Potential_obstacle_N11_sub_label[i] = i+1;
		}
		isIinitialized_Potential_obstacle_N11_sub_label = true;
	}
}

void read_Potential_obstacle_N11_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(Potential_obstacle_N11_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = Potential_obstacle_N11_sub_label[i];
			a = Potential_obstacle_N11_sub_label[i+1];
			a = Potential_obstacle_N11_sub_label[i+2];
			a = Potential_obstacle_N11_sub_label[i+3];
			a = Potential_obstacle_N11_sub_label[i+4];
			a = Potential_obstacle_N11_sub_label[i+5];
			a = Potential_obstacle_N11_sub_label[i+6];
			a = Potential_obstacle_N11_sub_label[i+7];
			a = Potential_obstacle_N11_sub_label[i+8];
			a = Potential_obstacle_N11_sub_label[i+9];
		}
	
		for(;i<arraysize;i++){
			a = Potential_obstacle_N11_sub_label[i];
		}

		(void)a;
	}
}

void write_Potential_obstacle_N11_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(Potential_obstacle_N11_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			Potential_obstacle_N11_sub_label[i]   = 0x800A;
			Potential_obstacle_N11_sub_label[i+1] = 0xAFFE;
			Potential_obstacle_N11_sub_label[i+2] = 0xAFFE;
			Potential_obstacle_N11_sub_label[i+3] = 0xAFFE;
			Potential_obstacle_N11_sub_label[i+4] = 0xAFFE;
			Potential_obstacle_N11_sub_label[i+5] = 0xAFFE;
			Potential_obstacle_N11_sub_label[i+6] = 0xAFFE;
			Potential_obstacle_N11_sub_label[i+7] = 0xAFFE;
			Potential_obstacle_N11_sub_label[i+8] = 0xAFFE;
			Potential_obstacle_N11_sub_label[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				Potential_obstacle_N11_sub_label[i]=0xAFFE;
		}
	}
}

int Potential_obstacle_N04_sub_label[0];

static bool isIinitialized_Potential_obstacle_N04_sub_label = false;
void initialize_Potential_obstacle_N04_sub_label() {
	if (!isIinitialized_Potential_obstacle_N04_sub_label){
		int i;
		for (i=0; i < 0; i++){
			Potential_obstacle_N04_sub_label[i] = i+1;
		}
		isIinitialized_Potential_obstacle_N04_sub_label = true;
	}
}

void read_Potential_obstacle_N04_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(Potential_obstacle_N04_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = Potential_obstacle_N04_sub_label[i];
			a = Potential_obstacle_N04_sub_label[i+1];
			a = Potential_obstacle_N04_sub_label[i+2];
			a = Potential_obstacle_N04_sub_label[i+3];
			a = Potential_obstacle_N04_sub_label[i+4];
			a = Potential_obstacle_N04_sub_label[i+5];
			a = Potential_obstacle_N04_sub_label[i+6];
			a = Potential_obstacle_N04_sub_label[i+7];
			a = Potential_obstacle_N04_sub_label[i+8];
			a = Potential_obstacle_N04_sub_label[i+9];
		}
	
		for(;i<arraysize;i++){
			a = Potential_obstacle_N04_sub_label[i];
		}

		(void)a;
	}
}

void write_Potential_obstacle_N04_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(Potential_obstacle_N04_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			Potential_obstacle_N04_sub_label[i]   = 0x800A;
			Potential_obstacle_N04_sub_label[i+1] = 0xAFFE;
			Potential_obstacle_N04_sub_label[i+2] = 0xAFFE;
			Potential_obstacle_N04_sub_label[i+3] = 0xAFFE;
			Potential_obstacle_N04_sub_label[i+4] = 0xAFFE;
			Potential_obstacle_N04_sub_label[i+5] = 0xAFFE;
			Potential_obstacle_N04_sub_label[i+6] = 0xAFFE;
			Potential_obstacle_N04_sub_label[i+7] = 0xAFFE;
			Potential_obstacle_N04_sub_label[i+8] = 0xAFFE;
			Potential_obstacle_N04_sub_label[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				Potential_obstacle_N04_sub_label[i]=0xAFFE;
		}
	}
}

int Potential_obstacle_N03_sub_label[0];

static bool isIinitialized_Potential_obstacle_N03_sub_label = false;
void initialize_Potential_obstacle_N03_sub_label() {
	if (!isIinitialized_Potential_obstacle_N03_sub_label){
		int i;
		for (i=0; i < 0; i++){
			Potential_obstacle_N03_sub_label[i] = i+1;
		}
		isIinitialized_Potential_obstacle_N03_sub_label = true;
	}
}

void read_Potential_obstacle_N03_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(Potential_obstacle_N03_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = Potential_obstacle_N03_sub_label[i];
			a = Potential_obstacle_N03_sub_label[i+1];
			a = Potential_obstacle_N03_sub_label[i+2];
			a = Potential_obstacle_N03_sub_label[i+3];
			a = Potential_obstacle_N03_sub_label[i+4];
			a = Potential_obstacle_N03_sub_label[i+5];
			a = Potential_obstacle_N03_sub_label[i+6];
			a = Potential_obstacle_N03_sub_label[i+7];
			a = Potential_obstacle_N03_sub_label[i+8];
			a = Potential_obstacle_N03_sub_label[i+9];
		}
	
		for(;i<arraysize;i++){
			a = Potential_obstacle_N03_sub_label[i];
		}

		(void)a;
	}
}

void write_Potential_obstacle_N03_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(Potential_obstacle_N03_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			Potential_obstacle_N03_sub_label[i]   = 0x800A;
			Potential_obstacle_N03_sub_label[i+1] = 0xAFFE;
			Potential_obstacle_N03_sub_label[i+2] = 0xAFFE;
			Potential_obstacle_N03_sub_label[i+3] = 0xAFFE;
			Potential_obstacle_N03_sub_label[i+4] = 0xAFFE;
			Potential_obstacle_N03_sub_label[i+5] = 0xAFFE;
			Potential_obstacle_N03_sub_label[i+6] = 0xAFFE;
			Potential_obstacle_N03_sub_label[i+7] = 0xAFFE;
			Potential_obstacle_N03_sub_label[i+8] = 0xAFFE;
			Potential_obstacle_N03_sub_label[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				Potential_obstacle_N03_sub_label[i]=0xAFFE;
		}
	}
}

int Potential_obstacle_N02_sub_label[0];

static bool isIinitialized_Potential_obstacle_N02_sub_label = false;
void initialize_Potential_obstacle_N02_sub_label() {
	if (!isIinitialized_Potential_obstacle_N02_sub_label){
		int i;
		for (i=0; i < 0; i++){
			Potential_obstacle_N02_sub_label[i] = i+1;
		}
		isIinitialized_Potential_obstacle_N02_sub_label = true;
	}
}

void read_Potential_obstacle_N02_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(Potential_obstacle_N02_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = Potential_obstacle_N02_sub_label[i];
			a = Potential_obstacle_N02_sub_label[i+1];
			a = Potential_obstacle_N02_sub_label[i+2];
			a = Potential_obstacle_N02_sub_label[i+3];
			a = Potential_obstacle_N02_sub_label[i+4];
			a = Potential_obstacle_N02_sub_label[i+5];
			a = Potential_obstacle_N02_sub_label[i+6];
			a = Potential_obstacle_N02_sub_label[i+7];
			a = Potential_obstacle_N02_sub_label[i+8];
			a = Potential_obstacle_N02_sub_label[i+9];
		}
	
		for(;i<arraysize;i++){
			a = Potential_obstacle_N02_sub_label[i];
		}

		(void)a;
	}
}

void write_Potential_obstacle_N02_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(Potential_obstacle_N02_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			Potential_obstacle_N02_sub_label[i]   = 0x800A;
			Potential_obstacle_N02_sub_label[i+1] = 0xAFFE;
			Potential_obstacle_N02_sub_label[i+2] = 0xAFFE;
			Potential_obstacle_N02_sub_label[i+3] = 0xAFFE;
			Potential_obstacle_N02_sub_label[i+4] = 0xAFFE;
			Potential_obstacle_N02_sub_label[i+5] = 0xAFFE;
			Potential_obstacle_N02_sub_label[i+6] = 0xAFFE;
			Potential_obstacle_N02_sub_label[i+7] = 0xAFFE;
			Potential_obstacle_N02_sub_label[i+8] = 0xAFFE;
			Potential_obstacle_N02_sub_label[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				Potential_obstacle_N02_sub_label[i]=0xAFFE;
		}
	}
}

int Potential_obstacle_N01_sub_label[0];

static bool isIinitialized_Potential_obstacle_N01_sub_label = false;
void initialize_Potential_obstacle_N01_sub_label() {
	if (!isIinitialized_Potential_obstacle_N01_sub_label){
		int i;
		for (i=0; i < 0; i++){
			Potential_obstacle_N01_sub_label[i] = i+1;
		}
		isIinitialized_Potential_obstacle_N01_sub_label = true;
	}
}

void read_Potential_obstacle_N01_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(Potential_obstacle_N01_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = Potential_obstacle_N01_sub_label[i];
			a = Potential_obstacle_N01_sub_label[i+1];
			a = Potential_obstacle_N01_sub_label[i+2];
			a = Potential_obstacle_N01_sub_label[i+3];
			a = Potential_obstacle_N01_sub_label[i+4];
			a = Potential_obstacle_N01_sub_label[i+5];
			a = Potential_obstacle_N01_sub_label[i+6];
			a = Potential_obstacle_N01_sub_label[i+7];
			a = Potential_obstacle_N01_sub_label[i+8];
			a = Potential_obstacle_N01_sub_label[i+9];
		}
	
		for(;i<arraysize;i++){
			a = Potential_obstacle_N01_sub_label[i];
		}

		(void)a;
	}
}

void write_Potential_obstacle_N01_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(Potential_obstacle_N01_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			Potential_obstacle_N01_sub_label[i]   = 0x800A;
			Potential_obstacle_N01_sub_label[i+1] = 0xAFFE;
			Potential_obstacle_N01_sub_label[i+2] = 0xAFFE;
			Potential_obstacle_N01_sub_label[i+3] = 0xAFFE;
			Potential_obstacle_N01_sub_label[i+4] = 0xAFFE;
			Potential_obstacle_N01_sub_label[i+5] = 0xAFFE;
			Potential_obstacle_N01_sub_label[i+6] = 0xAFFE;
			Potential_obstacle_N01_sub_label[i+7] = 0xAFFE;
			Potential_obstacle_N01_sub_label[i+8] = 0xAFFE;
			Potential_obstacle_N01_sub_label[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				Potential_obstacle_N01_sub_label[i]=0xAFFE;
		}
	}
}

int Potential_obstacle_N07_sub_label[0];

static bool isIinitialized_Potential_obstacle_N07_sub_label = false;
void initialize_Potential_obstacle_N07_sub_label() {
	if (!isIinitialized_Potential_obstacle_N07_sub_label){
		int i;
		for (i=0; i < 0; i++){
			Potential_obstacle_N07_sub_label[i] = i+1;
		}
		isIinitialized_Potential_obstacle_N07_sub_label = true;
	}
}

void read_Potential_obstacle_N07_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(Potential_obstacle_N07_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = Potential_obstacle_N07_sub_label[i];
			a = Potential_obstacle_N07_sub_label[i+1];
			a = Potential_obstacle_N07_sub_label[i+2];
			a = Potential_obstacle_N07_sub_label[i+3];
			a = Potential_obstacle_N07_sub_label[i+4];
			a = Potential_obstacle_N07_sub_label[i+5];
			a = Potential_obstacle_N07_sub_label[i+6];
			a = Potential_obstacle_N07_sub_label[i+7];
			a = Potential_obstacle_N07_sub_label[i+8];
			a = Potential_obstacle_N07_sub_label[i+9];
		}
	
		for(;i<arraysize;i++){
			a = Potential_obstacle_N07_sub_label[i];
		}

		(void)a;
	}
}

void write_Potential_obstacle_N07_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(Potential_obstacle_N07_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			Potential_obstacle_N07_sub_label[i]   = 0x800A;
			Potential_obstacle_N07_sub_label[i+1] = 0xAFFE;
			Potential_obstacle_N07_sub_label[i+2] = 0xAFFE;
			Potential_obstacle_N07_sub_label[i+3] = 0xAFFE;
			Potential_obstacle_N07_sub_label[i+4] = 0xAFFE;
			Potential_obstacle_N07_sub_label[i+5] = 0xAFFE;
			Potential_obstacle_N07_sub_label[i+6] = 0xAFFE;
			Potential_obstacle_N07_sub_label[i+7] = 0xAFFE;
			Potential_obstacle_N07_sub_label[i+8] = 0xAFFE;
			Potential_obstacle_N07_sub_label[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				Potential_obstacle_N07_sub_label[i]=0xAFFE;
		}
	}
}

int Potential_obstacle_N10_sub_label[0];

static bool isIinitialized_Potential_obstacle_N10_sub_label = false;
void initialize_Potential_obstacle_N10_sub_label() {
	if (!isIinitialized_Potential_obstacle_N10_sub_label){
		int i;
		for (i=0; i < 0; i++){
			Potential_obstacle_N10_sub_label[i] = i+1;
		}
		isIinitialized_Potential_obstacle_N10_sub_label = true;
	}
}

void read_Potential_obstacle_N10_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(Potential_obstacle_N10_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = Potential_obstacle_N10_sub_label[i];
			a = Potential_obstacle_N10_sub_label[i+1];
			a = Potential_obstacle_N10_sub_label[i+2];
			a = Potential_obstacle_N10_sub_label[i+3];
			a = Potential_obstacle_N10_sub_label[i+4];
			a = Potential_obstacle_N10_sub_label[i+5];
			a = Potential_obstacle_N10_sub_label[i+6];
			a = Potential_obstacle_N10_sub_label[i+7];
			a = Potential_obstacle_N10_sub_label[i+8];
			a = Potential_obstacle_N10_sub_label[i+9];
		}
	
		for(;i<arraysize;i++){
			a = Potential_obstacle_N10_sub_label[i];
		}

		(void)a;
	}
}

void write_Potential_obstacle_N10_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(Potential_obstacle_N10_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			Potential_obstacle_N10_sub_label[i]   = 0x800A;
			Potential_obstacle_N10_sub_label[i+1] = 0xAFFE;
			Potential_obstacle_N10_sub_label[i+2] = 0xAFFE;
			Potential_obstacle_N10_sub_label[i+3] = 0xAFFE;
			Potential_obstacle_N10_sub_label[i+4] = 0xAFFE;
			Potential_obstacle_N10_sub_label[i+5] = 0xAFFE;
			Potential_obstacle_N10_sub_label[i+6] = 0xAFFE;
			Potential_obstacle_N10_sub_label[i+7] = 0xAFFE;
			Potential_obstacle_N10_sub_label[i+8] = 0xAFFE;
			Potential_obstacle_N10_sub_label[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				Potential_obstacle_N10_sub_label[i]=0xAFFE;
		}
	}
}

int Potential_obstacle_N09_sub_label[0];

static bool isIinitialized_Potential_obstacle_N09_sub_label = false;
void initialize_Potential_obstacle_N09_sub_label() {
	if (!isIinitialized_Potential_obstacle_N09_sub_label){
		int i;
		for (i=0; i < 0; i++){
			Potential_obstacle_N09_sub_label[i] = i+1;
		}
		isIinitialized_Potential_obstacle_N09_sub_label = true;
	}
}

void read_Potential_obstacle_N09_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(Potential_obstacle_N09_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = Potential_obstacle_N09_sub_label[i];
			a = Potential_obstacle_N09_sub_label[i+1];
			a = Potential_obstacle_N09_sub_label[i+2];
			a = Potential_obstacle_N09_sub_label[i+3];
			a = Potential_obstacle_N09_sub_label[i+4];
			a = Potential_obstacle_N09_sub_label[i+5];
			a = Potential_obstacle_N09_sub_label[i+6];
			a = Potential_obstacle_N09_sub_label[i+7];
			a = Potential_obstacle_N09_sub_label[i+8];
			a = Potential_obstacle_N09_sub_label[i+9];
		}
	
		for(;i<arraysize;i++){
			a = Potential_obstacle_N09_sub_label[i];
		}

		(void)a;
	}
}

void write_Potential_obstacle_N09_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(Potential_obstacle_N09_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			Potential_obstacle_N09_sub_label[i]   = 0x800A;
			Potential_obstacle_N09_sub_label[i+1] = 0xAFFE;
			Potential_obstacle_N09_sub_label[i+2] = 0xAFFE;
			Potential_obstacle_N09_sub_label[i+3] = 0xAFFE;
			Potential_obstacle_N09_sub_label[i+4] = 0xAFFE;
			Potential_obstacle_N09_sub_label[i+5] = 0xAFFE;
			Potential_obstacle_N09_sub_label[i+6] = 0xAFFE;
			Potential_obstacle_N09_sub_label[i+7] = 0xAFFE;
			Potential_obstacle_N09_sub_label[i+8] = 0xAFFE;
			Potential_obstacle_N09_sub_label[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				Potential_obstacle_N09_sub_label[i]=0xAFFE;
		}
	}
}

int Potential_obstacle_N06_sub_label[0];

static bool isIinitialized_Potential_obstacle_N06_sub_label = false;
void initialize_Potential_obstacle_N06_sub_label() {
	if (!isIinitialized_Potential_obstacle_N06_sub_label){
		int i;
		for (i=0; i < 0; i++){
			Potential_obstacle_N06_sub_label[i] = i+1;
		}
		isIinitialized_Potential_obstacle_N06_sub_label = true;
	}
}

void read_Potential_obstacle_N06_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(Potential_obstacle_N06_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = Potential_obstacle_N06_sub_label[i];
			a = Potential_obstacle_N06_sub_label[i+1];
			a = Potential_obstacle_N06_sub_label[i+2];
			a = Potential_obstacle_N06_sub_label[i+3];
			a = Potential_obstacle_N06_sub_label[i+4];
			a = Potential_obstacle_N06_sub_label[i+5];
			a = Potential_obstacle_N06_sub_label[i+6];
			a = Potential_obstacle_N06_sub_label[i+7];
			a = Potential_obstacle_N06_sub_label[i+8];
			a = Potential_obstacle_N06_sub_label[i+9];
		}
	
		for(;i<arraysize;i++){
			a = Potential_obstacle_N06_sub_label[i];
		}

		(void)a;
	}
}

void write_Potential_obstacle_N06_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(Potential_obstacle_N06_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			Potential_obstacle_N06_sub_label[i]   = 0x800A;
			Potential_obstacle_N06_sub_label[i+1] = 0xAFFE;
			Potential_obstacle_N06_sub_label[i+2] = 0xAFFE;
			Potential_obstacle_N06_sub_label[i+3] = 0xAFFE;
			Potential_obstacle_N06_sub_label[i+4] = 0xAFFE;
			Potential_obstacle_N06_sub_label[i+5] = 0xAFFE;
			Potential_obstacle_N06_sub_label[i+6] = 0xAFFE;
			Potential_obstacle_N06_sub_label[i+7] = 0xAFFE;
			Potential_obstacle_N06_sub_label[i+8] = 0xAFFE;
			Potential_obstacle_N06_sub_label[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				Potential_obstacle_N06_sub_label[i]=0xAFFE;
		}
	}
}

int Potential_obstacle_N05_sub_label[0];

static bool isIinitialized_Potential_obstacle_N05_sub_label = false;
void initialize_Potential_obstacle_N05_sub_label() {
	if (!isIinitialized_Potential_obstacle_N05_sub_label){
		int i;
		for (i=0; i < 0; i++){
			Potential_obstacle_N05_sub_label[i] = i+1;
		}
		isIinitialized_Potential_obstacle_N05_sub_label = true;
	}
}

void read_Potential_obstacle_N05_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(Potential_obstacle_N05_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = Potential_obstacle_N05_sub_label[i];
			a = Potential_obstacle_N05_sub_label[i+1];
			a = Potential_obstacle_N05_sub_label[i+2];
			a = Potential_obstacle_N05_sub_label[i+3];
			a = Potential_obstacle_N05_sub_label[i+4];
			a = Potential_obstacle_N05_sub_label[i+5];
			a = Potential_obstacle_N05_sub_label[i+6];
			a = Potential_obstacle_N05_sub_label[i+7];
			a = Potential_obstacle_N05_sub_label[i+8];
			a = Potential_obstacle_N05_sub_label[i+9];
		}
	
		for(;i<arraysize;i++){
			a = Potential_obstacle_N05_sub_label[i];
		}

		(void)a;
	}
}

void write_Potential_obstacle_N05_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(Potential_obstacle_N05_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			Potential_obstacle_N05_sub_label[i]   = 0x800A;
			Potential_obstacle_N05_sub_label[i+1] = 0xAFFE;
			Potential_obstacle_N05_sub_label[i+2] = 0xAFFE;
			Potential_obstacle_N05_sub_label[i+3] = 0xAFFE;
			Potential_obstacle_N05_sub_label[i+4] = 0xAFFE;
			Potential_obstacle_N05_sub_label[i+5] = 0xAFFE;
			Potential_obstacle_N05_sub_label[i+6] = 0xAFFE;
			Potential_obstacle_N05_sub_label[i+7] = 0xAFFE;
			Potential_obstacle_N05_sub_label[i+8] = 0xAFFE;
			Potential_obstacle_N05_sub_label[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				Potential_obstacle_N05_sub_label[i]=0xAFFE;
		}
	}
}

int Potential_obstacle_N15_sub_label[0];

static bool isIinitialized_Potential_obstacle_N15_sub_label = false;
void initialize_Potential_obstacle_N15_sub_label() {
	if (!isIinitialized_Potential_obstacle_N15_sub_label){
		int i;
		for (i=0; i < 0; i++){
			Potential_obstacle_N15_sub_label[i] = i+1;
		}
		isIinitialized_Potential_obstacle_N15_sub_label = true;
	}
}

void read_Potential_obstacle_N15_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(Potential_obstacle_N15_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = Potential_obstacle_N15_sub_label[i];
			a = Potential_obstacle_N15_sub_label[i+1];
			a = Potential_obstacle_N15_sub_label[i+2];
			a = Potential_obstacle_N15_sub_label[i+3];
			a = Potential_obstacle_N15_sub_label[i+4];
			a = Potential_obstacle_N15_sub_label[i+5];
			a = Potential_obstacle_N15_sub_label[i+6];
			a = Potential_obstacle_N15_sub_label[i+7];
			a = Potential_obstacle_N15_sub_label[i+8];
			a = Potential_obstacle_N15_sub_label[i+9];
		}
	
		for(;i<arraysize;i++){
			a = Potential_obstacle_N15_sub_label[i];
		}

		(void)a;
	}
}

void write_Potential_obstacle_N15_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(Potential_obstacle_N15_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			Potential_obstacle_N15_sub_label[i]   = 0x800A;
			Potential_obstacle_N15_sub_label[i+1] = 0xAFFE;
			Potential_obstacle_N15_sub_label[i+2] = 0xAFFE;
			Potential_obstacle_N15_sub_label[i+3] = 0xAFFE;
			Potential_obstacle_N15_sub_label[i+4] = 0xAFFE;
			Potential_obstacle_N15_sub_label[i+5] = 0xAFFE;
			Potential_obstacle_N15_sub_label[i+6] = 0xAFFE;
			Potential_obstacle_N15_sub_label[i+7] = 0xAFFE;
			Potential_obstacle_N15_sub_label[i+8] = 0xAFFE;
			Potential_obstacle_N15_sub_label[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				Potential_obstacle_N15_sub_label[i]=0xAFFE;
		}
	}
}

int Potential_obstacle_N00_sub_label[0];

static bool isIinitialized_Potential_obstacle_N00_sub_label = false;
void initialize_Potential_obstacle_N00_sub_label() {
	if (!isIinitialized_Potential_obstacle_N00_sub_label){
		int i;
		for (i=0; i < 0; i++){
			Potential_obstacle_N00_sub_label[i] = i+1;
		}
		isIinitialized_Potential_obstacle_N00_sub_label = true;
	}
}

void read_Potential_obstacle_N00_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(Potential_obstacle_N00_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = Potential_obstacle_N00_sub_label[i];
			a = Potential_obstacle_N00_sub_label[i+1];
			a = Potential_obstacle_N00_sub_label[i+2];
			a = Potential_obstacle_N00_sub_label[i+3];
			a = Potential_obstacle_N00_sub_label[i+4];
			a = Potential_obstacle_N00_sub_label[i+5];
			a = Potential_obstacle_N00_sub_label[i+6];
			a = Potential_obstacle_N00_sub_label[i+7];
			a = Potential_obstacle_N00_sub_label[i+8];
			a = Potential_obstacle_N00_sub_label[i+9];
		}
	
		for(;i<arraysize;i++){
			a = Potential_obstacle_N00_sub_label[i];
		}

		(void)a;
	}
}

void write_Potential_obstacle_N00_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(Potential_obstacle_N00_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			Potential_obstacle_N00_sub_label[i]   = 0x800A;
			Potential_obstacle_N00_sub_label[i+1] = 0xAFFE;
			Potential_obstacle_N00_sub_label[i+2] = 0xAFFE;
			Potential_obstacle_N00_sub_label[i+3] = 0xAFFE;
			Potential_obstacle_N00_sub_label[i+4] = 0xAFFE;
			Potential_obstacle_N00_sub_label[i+5] = 0xAFFE;
			Potential_obstacle_N00_sub_label[i+6] = 0xAFFE;
			Potential_obstacle_N00_sub_label[i+7] = 0xAFFE;
			Potential_obstacle_N00_sub_label[i+8] = 0xAFFE;
			Potential_obstacle_N00_sub_label[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				Potential_obstacle_N00_sub_label[i]=0xAFFE;
		}
	}
}

int Potential_obstacle_N08_sub_label[0];

static bool isIinitialized_Potential_obstacle_N08_sub_label = false;
void initialize_Potential_obstacle_N08_sub_label() {
	if (!isIinitialized_Potential_obstacle_N08_sub_label){
		int i;
		for (i=0; i < 0; i++){
			Potential_obstacle_N08_sub_label[i] = i+1;
		}
		isIinitialized_Potential_obstacle_N08_sub_label = true;
	}
}

void read_Potential_obstacle_N08_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(Potential_obstacle_N08_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = Potential_obstacle_N08_sub_label[i];
			a = Potential_obstacle_N08_sub_label[i+1];
			a = Potential_obstacle_N08_sub_label[i+2];
			a = Potential_obstacle_N08_sub_label[i+3];
			a = Potential_obstacle_N08_sub_label[i+4];
			a = Potential_obstacle_N08_sub_label[i+5];
			a = Potential_obstacle_N08_sub_label[i+6];
			a = Potential_obstacle_N08_sub_label[i+7];
			a = Potential_obstacle_N08_sub_label[i+8];
			a = Potential_obstacle_N08_sub_label[i+9];
		}
	
		for(;i<arraysize;i++){
			a = Potential_obstacle_N08_sub_label[i];
		}

		(void)a;
	}
}

void write_Potential_obstacle_N08_sub_label(int labelAccessStatistics) {
	int numberOfBytes = 0;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(Potential_obstacle_N08_sub_label) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			Potential_obstacle_N08_sub_label[i]   = 0x800A;
			Potential_obstacle_N08_sub_label[i+1] = 0xAFFE;
			Potential_obstacle_N08_sub_label[i+2] = 0xAFFE;
			Potential_obstacle_N08_sub_label[i+3] = 0xAFFE;
			Potential_obstacle_N08_sub_label[i+4] = 0xAFFE;
			Potential_obstacle_N08_sub_label[i+5] = 0xAFFE;
			Potential_obstacle_N08_sub_label[i+6] = 0xAFFE;
			Potential_obstacle_N08_sub_label[i+7] = 0xAFFE;
			Potential_obstacle_N08_sub_label[i+8] = 0xAFFE;
			Potential_obstacle_N08_sub_label[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				Potential_obstacle_N08_sub_label[i]=0xAFFE;
		}
	}
}

