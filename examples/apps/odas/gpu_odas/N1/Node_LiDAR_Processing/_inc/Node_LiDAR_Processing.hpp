// This code is auto-generated

#include <chrono>
#include <memory>

#include "rclcpp/rclcpp.hpp"
#include "std_msgs/msg/string.hpp"

#include "runnables.hpp"

using namespace std::chrono_literals;
using std::placeholders::_1;
#define CONSOLE_ENABLED

#if defined(_OPENMP)
int check_Node_LiDAR_Processing(int *original, int *replicated){ return 1;}
#endif
class Node_LiDAR_Processing : public rclcpp::Node
{
	private:
		rclcpp::TimerBase::SharedPtr timer_LiDAR_Processing_100_ms_;
		rclcpp::Publisher<std_msgs::msg::String>::SharedPtr LiDAR_objects_in_VBF_coord_publisher;

	public:
		Node_LiDAR_Processing()
		: Node("node_lidar_processing")
		{
			timer_LiDAR_Processing_100_ms_ = this->create_wall_timer(
					100ms, std::bind(&Node_LiDAR_Processing::timer_LiDAR_Processing_100_ms_callback, this));
			LiDAR_objects_in_VBF_coord_publisher = this->create_publisher<std_msgs::msg::String>("lidar_objects_in_vbf_coord", 10);
		}
	void timer_LiDAR_Processing_100_ms_callback() {
	#ifdef CONSOLE_ENABLED
		std::cout << "Timer_LiDAR_Processing_100_ms_callback (100ms)" << std::endl;
	#endif
		extern int LiDAR_point_cloud_in_VBF_coord[1];
		extern int ThreeD_point_cloud[1];
		int dummy;
		
		 #if defined(_OPENMP)
		 #pragma omp taskgraph tdg_type(static) nowait
		#endif
		{
		#if defined(_OPENMP)
		 #pragma omp task depend(out:ThreeD_point_cloud) replicated(2, dummy, check_Node_LiDAR_Processing)
		#elif defined(_OMPSS)
		 #pragma oss task depend(out:ThreeD_point_cloud) replicated(2, dummy, check_Node_LiDAR_Processing)
		#endif
		run_ETH_LiDAR_msgs_management();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:ThreeD_point_cloud) depend(out:LiDAR_point_cloud_in_VBF_coord) replicated(2, dummy, check_Node_LiDAR_Processing)
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:ThreeD_point_cloud) depend(out:LiDAR_point_cloud_in_VBF_coord) replicated(2, dummy, check_Node_LiDAR_Processing)
		#endif
		run_LiDAR_spatial_synchronization();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:LiDAR_point_cloud_in_VBF_coord) replicated(2, dummy, check_Node_LiDAR_Processing)
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:LiDAR_point_cloud_in_VBF_coord) replicated(2, dummy, check_Node_LiDAR_Processing)
		#endif
		run_LiDAR_object_detection(LiDAR_objects_in_VBF_coord_publisher);
		}
		#pragma omp taskwait;
	}
	
	
};
