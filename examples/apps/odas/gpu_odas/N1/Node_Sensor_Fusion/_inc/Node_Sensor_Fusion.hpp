// This code is auto-generated

#include <chrono>
#include <memory>

#include "rclcpp/rclcpp.hpp"
#include "std_msgs/msg/string.hpp"

#include "runnables.hpp"

using namespace std::chrono_literals;
using std::placeholders::_1;
#define CONSOLE_ENABLED

#if defined(_OPENMP)
int check_Node_Sensor_Fusion(int *original, int *replicated){ return 1;}
#endif
class Node_Sensor_Fusion : public rclcpp::Node
{
	private:
		rclcpp::Subscription<std_msgs::msg::String>::SharedPtr RADAR_objects_in_VBF_coord_subscription_;
		rclcpp::Subscription<std_msgs::msg::String>::SharedPtr LiDAR_objects_in_VBF_coord_subscription_;
		rclcpp::Subscription<std_msgs::msg::String>::SharedPtr Bounding_box_in_VBF_coord_subscription_;
		rclcpp::Publisher<std_msgs::msg::String>::SharedPtr Tracked_object_label_N_publisher;

	public:
		Node_Sensor_Fusion()
		: Node("node_sensor_fusion")
		{
			RADAR_objects_in_VBF_coord_subscription_ = this->create_subscription<std_msgs::msg::String>(
				"radar_objects_in_vbf_coord", rclcpp::QoS(10), std::bind(&Node_Sensor_Fusion::RADAR_objects_in_VBF_coord_subscription_callback, this, _1));
			LiDAR_objects_in_VBF_coord_subscription_ = this->create_subscription<std_msgs::msg::String>(
				"lidar_objects_in_vbf_coord", rclcpp::QoS(10), std::bind(&Node_Sensor_Fusion::LiDAR_objects_in_VBF_coord_subscription_callback, this, _1));
			Bounding_box_in_VBF_coord_subscription_ = this->create_subscription<std_msgs::msg::String>(
				"bounding_box_in_vbf_coord", rclcpp::QoS(10), std::bind(&Node_Sensor_Fusion::Bounding_box_in_VBF_coord_subscription_callback, this, _1));
			Tracked_object_label_N_publisher = this->create_publisher<std_msgs::msg::String>("tracked_object_label_n", 10);
		}
	void RADAR_objects_in_VBF_coord_subscription_callback(const std_msgs::msg::String::SharedPtr msg) {
	#ifdef CONSOLE_ENABLED
		std::cout << "ROS2: subscription callback " << msg->data << std::endl ;
	#endif
		extern int RADAR_post_time_synch[1];
		extern int CAMERA_post_time_synch[1];
		extern int LiDAR_post_time_synch[1];
		extern int Track_objects[1];
		int dummy;
		
		#if defined(_OPENMP)
		#pragma omp taskgraph tdg_type(static) nowait
		#endif
		{
		#if defined(_OPENMP)
		 #pragma omp task depend(out:LiDAR_post_time_synch,CAMERA_post_time_synch,RADAR_post_time_synch) replicated(2, dummy, check_Node_Sensor_Fusion)
		#elif defined(_OMPSS)
		 #pragma oss task depend(out:LiDAR_post_time_synch,CAMERA_post_time_synch,RADAR_post_time_synch) replicated(2, dummy, check_Node_Sensor_Fusion)
		#endif
		run_Time_synchronization();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:LiDAR_post_time_synch,CAMERA_post_time_synch,RADAR_post_time_synch) depend(out:Track_objects) replicated(2, dummy, check_Node_Sensor_Fusion)
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:LiDAR_post_time_synch,CAMERA_post_time_synch,RADAR_post_time_synch) depend(out:Track_objects) replicated(2, dummy, check_Node_Sensor_Fusion)
		#endif
		run_Data_association();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:Track_objects) replicated(2, dummy, check_Node_Sensor_Fusion)
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:Track_objects) replicated(2, dummy, check_Node_Sensor_Fusion)
		#endif
		run_Track_management(Tracked_object_label_N_publisher);
		}
		#pragma omp taskwait;
	}
	
	void LiDAR_objects_in_VBF_coord_subscription_callback(const std_msgs::msg::String::SharedPtr msg) {
	#ifdef CONSOLE_ENABLED
		std::cout << "ROS2: subscription callback " << msg->data << std::endl ;
	#endif
		extern int RADAR_post_time_synch[1];
		extern int CAMERA_post_time_synch[1];
		extern int LiDAR_post_time_synch[1];
		extern int Track_objects[1];
		int dummy;
		
		 #if defined(_OPENMP)
		 // #pragma omp taskgraph tdg_type(static) nowait
		#endif
		{
		#if defined(_OPENMP)
		 #pragma omp task depend(out:LiDAR_post_time_synch,CAMERA_post_time_synch,RADAR_post_time_synch) replicated(2, dummy, check_Node_Sensor_Fusion)
		#elif defined(_OMPSS)
		 #pragma oss task depend(out:LiDAR_post_time_synch,CAMERA_post_time_synch,RADAR_post_time_synch) replicated(2, dummy, check_Node_Sensor_Fusion)
		#endif
		run_Time_synchronization();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:LiDAR_post_time_synch,CAMERA_post_time_synch,RADAR_post_time_synch) depend(out:Track_objects) replicated(2, dummy, check_Node_Sensor_Fusion)
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:LiDAR_post_time_synch,CAMERA_post_time_synch,RADAR_post_time_synch) depend(out:Track_objects) replicated(2, dummy, check_Node_Sensor_Fusion)
		#endif
		run_Data_association();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:Track_objects) replicated(2, dummy, check_Node_Sensor_Fusion)
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:Track_objects) replicated(2, dummy, check_Node_Sensor_Fusion)
		#endif
		run_Track_management(Tracked_object_label_N_publisher);
		}
		#pragma omp taskwait;
	}
	
	void Bounding_box_in_VBF_coord_subscription_callback(const std_msgs::msg::String::SharedPtr msg) {
	#ifdef CONSOLE_ENABLED
		std::cout << "ROS2: subscription callback " << msg->data << std::endl ;
	#endif
		extern int RADAR_post_time_synch[1];
		extern int CAMERA_post_time_synch[1];
		extern int LiDAR_post_time_synch[1];
		extern int Track_objects[1];
		int dummy;
		
		 #if defined(_OPENMP)
		 // #pragma omp taskgraph tdg_type(static) nowait
		#endif
		{
		#if defined(_OPENMP)
		 #pragma omp task depend(out:LiDAR_post_time_synch,CAMERA_post_time_synch,RADAR_post_time_synch) replicated(2, dummy, check_Node_Sensor_Fusion)
		#elif defined(_OMPSS)
		 #pragma oss task depend(out:LiDAR_post_time_synch,CAMERA_post_time_synch,RADAR_post_time_synch) replicated(2, dummy, check_Node_Sensor_Fusion)
		#endif
		run_Time_synchronization();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:LiDAR_post_time_synch,CAMERA_post_time_synch,RADAR_post_time_synch) depend(out:Track_objects) replicated(2, dummy, check_Node_Sensor_Fusion)
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:LiDAR_post_time_synch,CAMERA_post_time_synch,RADAR_post_time_synch) depend(out:Track_objects) replicated(2, dummy, check_Node_Sensor_Fusion)
		#endif
		run_Data_association();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:Track_objects) replicated(2, dummy, check_Node_Sensor_Fusion)
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:Track_objects) replicated(2, dummy, check_Node_Sensor_Fusion)
		#endif
		run_Track_management(Tracked_object_label_N_publisher);
		}
		#pragma omp taskwait;
	}
	
	
	
	
};
