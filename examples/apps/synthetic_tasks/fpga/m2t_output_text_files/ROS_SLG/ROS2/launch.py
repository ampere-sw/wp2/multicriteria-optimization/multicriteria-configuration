from launch import LaunchDescription
from launch_ros.actions import Node

def generate_launch_description():
    return LaunchDescription([
		Node(
			package='amalthea_ros2_model',
			node_namespace='',
			node_executable='Node_Task1',
		),
		Node(
			package='amalthea_ros2_model',
			node_namespace='',
			node_executable='Node_Task2',
		),
		Node(
			package='amalthea_ros2_model',
			node_namespace='',
			node_executable='Node_Task3',
		),
	])
