#!/bin/bash

delete_build_contents(){
    rm -rf $1/m2t_output_text_files/ROS_SLG/build
    rm -rf $1/m2t_output_text_files/ROS_SLG/install
}

if [ $# -eq 0 ]
  then
    delete_build_contents cpu
    delete_build_contents gpu
    delete_build_contents fpga
  else
    delete_build_contents $1
fi