#!/bin/bash
SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )


replace_contents(){
    cp ${SCRIPT_DIR}/CMakeLists.txt $1/m2t_output_text_files/ROS_SLG/ROS2
    cp ${SCRIPT_DIR}/main.cpp $1/m2t_output_text_files/ROS_SLG/ROS2
}

# cp ${SCRIPT_DIR}/CMakeLists.txt $1/m2t_output_text_files/ROS_SLG/ROS2
# cp ${SCRIPT_DIR}/main.cpp $1/m2t_output_text_files/ROS_SLG/ROS2
if [ $# -eq 0 ]
  then
    replace_contents cpu
    replace_contents gpu
    replace_contents fpga
  else
    replace_contents $1
fi