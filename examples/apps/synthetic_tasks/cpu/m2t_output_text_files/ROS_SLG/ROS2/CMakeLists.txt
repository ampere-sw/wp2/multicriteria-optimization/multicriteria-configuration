# CMakeLists for Nodes
cmake_minimum_required(VERSION 3.5)
project(amalthea_ros2_model)

# Default to C++14
if(NOT CMAKE_CXX_STANDARD)
	set(CMAKE_CXX_STANDARD 17)
endif()

if(CMAKE_COMPILER_IS_GNUCXX OR CMAKE_CXX_COMPILER_ID MATCHES "Clang")
	add_compile_options(-Wall -Wextra -Wpedantic)
endif()

# Adding the external code's include directories
include_directories($ENV{EXTRAE_HOME}/include)
link_directories($ENV{EXTRAE_HOME}/lib)
# Adding the libraries created out of the external code

find_package(ament_cmake REQUIRED)
find_package(rclcpp REQUIRED)
find_package(std_msgs REQUIRED)
find_package(OpenMP)

if(OPENMP_FOUND)
    set (CMAKE_C_FLAGS "${CMAKE_C_FLAGS} ${OpenMP_C_FLAGS} -fopenmp-taskgraph -static-tdg")
    set (CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${OpenMP_CXX_FLAGS} -fopenmp-taskgraph -static-tdg")
    set (CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} ${OpenMP_EXE_LINKER_FLAGS}")
endif()
find_package(trigger_task2_service REQUIRED)

add_library(INTERPROCESSTRIGGER_UTIL STATIC
	  synthetic_gen/interProcessTriggerUtils/_src/interProcessTriggerUtils.cpp
)
ament_target_dependencies(INTERPROCESSTRIGGER_UTIL rclcpp std_msgs trigger_task2_service)
target_include_directories(INTERPROCESSTRIGGER_UTIL
	PUBLIC synthetic_gen/interProcessTriggerUtils/_inc
)

add_subdirectory (synthetic_gen/labels)
add_subdirectory (synthetic_gen/ticksUtils)

# RUNNABLES_LIB ################################################################

####
add_library(RUNNABLES_LIB STATIC
	synthetic_gen/runnables/_src/runnables.cpp
)

target_include_directories(RUNNABLES_LIB
	PUBLIC synthetic_gen/runnables/_inc
)	

target_include_directories(RUNNABLES_LIB
	PUBLIC 
	synthetic_gen/ticksUtils/_inc
	synthetic_gen/labels/_inc
	synthetic_gen/interProcessTriggerUtils/_inc
)
target_link_libraries(RUNNABLES_LIB
	PRIVATE LABELS_LIB TICKS_UTILS INTERPROCESSTRIGGER_UTIL
)


# ********** Node Node_Task1 **********

add_library(Node_Task1_LIB STATIC
	Node_Task1/_inc/Node_Task1.hpp
)

target_include_directories(Node_Task1_LIB
	PUBLIC
	synthetic_gen/runnables/_inc
	synthetic_gen/ticksUtils/_inc
	synthetic_gen/labels/_inc
	synthetic_gen/interProcessTriggerUtils/_inc
)

target_link_libraries(Node_Task1_LIB 
	PRIVATE  RUNNABLES_LIB LABELS_LIB TICKS_UTILS INTERPROCESSTRIGGER_UTIL
)


# ********** Node Node_Task2 **********

add_library(Node_Task2_LIB STATIC
	Node_Task2/_inc/Node_Task2.hpp
)

target_include_directories(Node_Task2_LIB
	PUBLIC
	synthetic_gen/runnables/_inc
	synthetic_gen/ticksUtils/_inc
	synthetic_gen/labels/_inc
	synthetic_gen/interProcessTriggerUtils/_inc
)

target_link_libraries(Node_Task2_LIB 
	PRIVATE  RUNNABLES_LIB LABELS_LIB TICKS_UTILS INTERPROCESSTRIGGER_UTIL
)


# ********** Node Node_Task3 **********

add_library(Node_Task3_LIB STATIC
	Node_Task3/_inc/Node_Task3.hpp
)

target_include_directories(Node_Task3_LIB
	PUBLIC
	synthetic_gen/runnables/_inc
	synthetic_gen/ticksUtils/_inc
	synthetic_gen/labels/_inc
	synthetic_gen/interProcessTriggerUtils/_inc
)

target_link_libraries(Node_Task3_LIB 
	PRIVATE  RUNNABLES_LIB LABELS_LIB TICKS_UTILS INTERPROCESSTRIGGER_UTIL
)

add_library(MAIN_OBJ OBJECT main.cpp)
add_custom_command(
    COMMAND echo ""
    OUTPUT main_tdg.cpp
    DEPENDS main.cpp
)
target_include_directories(MAIN_OBJ
	PUBLIC
	Node_Task1/_inc
	Node_Task2/_inc
	Node_Task3/_inc
	$ENV{EXTRAE_HOME}/include
)

target_link_libraries(MAIN_OBJ Node_Task1_LIB Node_Task2_LIB Node_Task3_LIB omptrace)
ament_target_dependencies(MAIN_OBJ rclcpp std_msgs trigger_task2_service)

add_executable(MAIN_BIN main.cpp)

target_include_directories(MAIN_BIN
	PUBLIC
	Node_Task1/_inc
	Node_Task2/_inc
	Node_Task3/_inc
	$ENV{EXTRAE_HOME}/include
)

target_link_libraries(MAIN_BIN Node_Task1_LIB Node_Task2_LIB Node_Task3_LIB omptrace)
ament_target_dependencies(MAIN_BIN rclcpp std_msgs trigger_task2_service)

target_sources(MAIN_BIN PRIVATE main_tdg.cpp)
add_dependencies(MAIN_BIN MAIN_OBJ)

install(TARGETS
	# Node_Task1
	# Node_Task2
	# Node_Task3
	MAIN_BIN
	DESTINATION lib/${PROJECT_NAME}
)

ament_package()

