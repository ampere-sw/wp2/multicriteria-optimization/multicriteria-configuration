// This code is auto-generated

#include <chrono>
#include <memory>

#include "rclcpp/rclcpp.hpp"
#include "std_msgs/msg/string.hpp"

#include "runnables.hpp"

using namespace std::chrono_literals;
using std::placeholders::_1;
#define CONSOLE_ENABLED

#if defined(_OPENMP)
int check_Node_Task3(int *original, int *replicated){ return 1;}
#endif
class Node_Task3 : public rclcpp::Node
{
	private:
		rclcpp::TimerBase::SharedPtr timer_another_stimulus_;

	public:
		Node_Task3()
		: Node("node_task3")
		{
			timer_another_stimulus_ = this->create_wall_timer(
					200ms, std::bind(&Node_Task3::timer_another_stimulus_callback, this));
		}
	void timer_another_stimulus_callback() {
	#ifdef CONSOLE_ENABLED
		std::cout << "Timer_another_stimulus_callback (200ms)" << std::endl;
	#endif
		extern int Image3[15625];
		extern int ResultsE[15625];
		extern int ResultsF[15625];
		int dummy;
		
		 #if defined(_OPENMP)
		 #pragma omp taskgraph tdg_type(static) nowait
		#endif
		{
		#if defined(_OPENMP)
		 #pragma omp task depend(out:Image3) replicated(2, dummy, check_Node_Task3)
		#elif defined(_OMPSS)
		 #pragma oss task depend(out:Image3) replicated(2, dummy, check_Node_Task3)
		#endif
		run_read_image3();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(inout:Image3)
		#elif defined(_OMPSS)
		 #pragma oss task depend(inout:Image3)
		#endif
		run_convert_image3();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:Image3) depend(out:ResultsE)
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:Image3) depend(out:ResultsE)
		#endif
		run_analysisE();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:Image3) depend(out:ResultsF)
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:Image3) depend(out:ResultsF)
		#endif
		run_analysisF();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:ResultsF,ResultsE)
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:ResultsF,ResultsE)
		#endif
		run_merge_results3();
		}
		#pragma omp taskwait;
	}
	
	
};
