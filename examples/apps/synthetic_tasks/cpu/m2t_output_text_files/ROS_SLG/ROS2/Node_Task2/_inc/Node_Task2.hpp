// This code is auto-generated

#include <chrono>
#include <memory>

#include "rclcpp/rclcpp.hpp"
#include "std_msgs/msg/string.hpp"

#include "runnables.hpp"
#include "trigger_task2_service/srv/trigger_task2_service.hpp"

using namespace std::chrono_literals;
using std::placeholders::_1;
#define CONSOLE_ENABLED

#if defined(_OPENMP)
int check_Node_Task2(int *original, int *replicated){ return 1;}
#endif
class Node_Task2 : public rclcpp::Node
{
	private:
		rclcpp::Service<trigger_task2_service::srv::TriggerTask2Service>::SharedPtr trigger_task2_service;

	public:
		Node_Task2()
		: Node("node_task2")
		{
			trigger_task2_service = this->create_service<trigger_task2_service::srv::TriggerTask2Service>(
				"trigger_task2_service", 
				std::bind(&Node_Task2::trigger_task2_service_callback, this, std::placeholders::_1, std::placeholders::_2));
		}
	
	void trigger_task2_service_callback(const std::shared_ptr<trigger_task2_service::srv::TriggerTask2Service::Request> request,
		std::shared_ptr<trigger_task2_service::srv::TriggerTask2Service::Response> response) {
			(void)request;
			(void)response;
	#ifdef CONSOLE_ENABLED
		std::cout << "Starting trigger_task2_service_callback" << std::endl;
	#endif
		extern int ResultsD[15625];
		extern int Image2[15625];
		extern int ResultsC[15625];
		int dummy;
		
		 #if defined(_OPENMP)
		 #pragma omp taskgraph tdg_type(static) nowait
		#endif
		{
		#if defined(_OPENMP)
		 #pragma omp task depend(out:Image2)
		#elif defined(_OMPSS)
		 #pragma oss task depend(out:Image2)
		#endif
		run_read_image2();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(inout:Image2)
		#elif defined(_OMPSS)
		 #pragma oss task depend(inout:Image2)
		#endif
		run_convert_image2();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:Image2) depend(out:ResultsC)
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:Image2) depend(out:ResultsC)
		#endif
		run_analysisC();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:Image2) depend(out:ResultsD)
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:Image2) depend(out:ResultsD)
		#endif
		run_analysisD();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:ResultsC,ResultsD)
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:ResultsC,ResultsD)
		#endif
		run_merge_results2();
		}
		#pragma omp taskwait;
	}
	
};
