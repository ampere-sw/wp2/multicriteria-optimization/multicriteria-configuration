// This code is auto-generated

#include "runnables.hpp"

// Runnable read_image ----
void run_read_image(){
	executeTicks_DiscreteValueConstant(400000000);
	write_Image(1);
}


// Runnable convert_image ----
void run_convert_image(){
	read_Image(1);
	executeTicks_DiscreteValueConstant(700000000);
	write_Image(1);
}


// Runnable analysisA ----
void run_analysisA_GPU(){
		read_Image(1);
		executeTicks_DiscreteValueConstant(20000002);
		write_ResultsA(1);
}

void run_analysisA(){
		read_Image(1);
		executeTicks_DiscreteValueConstant(1000000001);
		write_ResultsA(1);
}


// Runnable analysisB ----
void run_analysisB(){
		read_Image(1);
		executeTicks_DiscreteValueConstant(1000000000);
		write_ResultsB(1);
}

void run_analysisB_FPGA(){
		read_Image(1);
		executeTicks_DiscreteValueConstant(10000001);
		write_ResultsB(1);
}


// Runnable merge_results ----
void run_merge_results(rclcpp::Client<trigger_task2_service::srv::TriggerTask2Service>::SharedPtr& trigger_task2_client){
	read_ResultsA(1);
	read_ResultsB(1);
	executeTicks_DiscreteValueConstant(700000000);
	write_Image2(1);
	call_service_trigger_task2(trigger_task2_client);
}


// Runnable read_image2 ----
void run_read_image2(){
	executeTicks_DiscreteValueConstant(400000000);
	write_Image2(1);
}


// Runnable convert_image2 ----
void run_convert_image2(){
	read_Image2(1);
	executeTicks_DiscreteValueConstant(700000000);
	write_Image2(1);
}


// Runnable analysisC ----
void run_analysisC_GPU(){
		read_Image2(1);
		executeTicks_DiscreteValueConstant(20000002);
		write_ResultsC(1);
}

void run_analysisC(){
		read_Image2(1);
		executeTicks_DiscreteValueConstant(1000000001);
		write_ResultsC(1);
}


// Runnable analysisD ----
void run_analysisD(){
	read_Image2(1);
	executeTicks_DiscreteValueConstant(1000000000);
	write_ResultsD(1);
}


// Runnable merge_results2 ----
void run_merge_results2(){
	read_ResultsC(1);
	read_ResultsD(1);
	executeTicks_DiscreteValueConstant(700000000);
	write_Image3(1);
}


// Runnable read_image3 ----
void run_read_image3(){
	executeTicks_DiscreteValueConstant(400000000);
	write_Image3(1);
}


// Runnable convert_image3 ----
void run_convert_image3(){
	read_Image3(1);
	executeTicks_DiscreteValueConstant(700000000);
	write_Image3(1);
}


// Runnable analysisE ----
void run_analysisE_GPU(){
		read_Image3(1);
		executeTicks_DiscreteValueConstant(20000002);
		write_ResultsE(1);
}

void run_analysisE(){
		read_Image3(1);
		executeTicks_DiscreteValueConstant(1000000001);
		write_ResultsE(1);
}


// Runnable analysisF ----
void run_analysisF(){
	read_Image3(1);
	executeTicks_DiscreteValueConstant(1000000000);
	write_ResultsF(1);
}


// Runnable merge_results3 ----
void run_merge_results3(){
	read_ResultsE(1);
	read_ResultsF(1);
	executeTicks_DiscreteValueConstant(700000000);
}

