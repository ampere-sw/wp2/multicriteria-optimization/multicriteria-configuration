// This code is auto-generated

#pragma once 

#include "ticksUtils.hpp"
extern void executeGPUTicks(long long average,long long lowerBound, long long upperBound);

#include "labels.hpp"

// Runnable read_image----
void run_read_image();
#include "labels.hpp"
#include "ticksUtils.hpp"

// Runnable convert_image----
void run_convert_image();
#include "labels.hpp"
#include "ticksUtils.hpp"
void run_analysisA_GPU();
#pragma omp declare variant(run_analysisA_GPU) match(construct={target}) 

// Runnable analysisA----
void run_analysisA();
#include "labels.hpp"
#include "ticksUtils.hpp"
void run_analysisB_FPGA();
#pragma omp declare variant(run_analysisB_FPGA) match(construct={target}) 

// Runnable analysisB----
void run_analysisB();
#include "labels.hpp"
#include "ticksUtils.hpp"
#include "interProcessTriggerUtils.hpp"
#include "trigger_task2_service/srv/trigger_task2_service.hpp"

// Runnable merge_results----
void run_merge_results(rclcpp::Client<trigger_task2_service::srv::TriggerTask2Service>::SharedPtr& trigger_task2_client);
#include "ticksUtils.hpp"
#include "labels.hpp"

// Runnable read_image2----
void run_read_image2();
#include "labels.hpp"
#include "ticksUtils.hpp"

// Runnable convert_image2----
void run_convert_image2();
#include "labels.hpp"
#include "ticksUtils.hpp"
void run_analysisC_GPU();
#pragma omp declare variant(run_analysisC_GPU) match(construct={target}) 

// Runnable analysisC----
void run_analysisC();
#include "labels.hpp"
#include "ticksUtils.hpp"

// Runnable analysisD----
void run_analysisD();
#include "labels.hpp"
#include "ticksUtils.hpp"

// Runnable merge_results2----
void run_merge_results2();
#include "ticksUtils.hpp"
#include "labels.hpp"

// Runnable read_image3----
void run_read_image3();
#include "labels.hpp"
#include "ticksUtils.hpp"

// Runnable convert_image3----
void run_convert_image3();
#include "labels.hpp"
#include "ticksUtils.hpp"
void run_analysisE_GPU();
#pragma omp declare variant(run_analysisE_GPU) match(construct={target}) 

// Runnable analysisE----
void run_analysisE();
#include "labels.hpp"
#include "ticksUtils.hpp"

// Runnable analysisF----
void run_analysisF();
#include "labels.hpp"
#include "ticksUtils.hpp"

// Runnable merge_results3----
void run_merge_results3();
