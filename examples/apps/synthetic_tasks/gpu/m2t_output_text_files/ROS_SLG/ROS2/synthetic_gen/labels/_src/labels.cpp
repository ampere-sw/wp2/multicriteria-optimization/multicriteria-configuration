// This code is auto-generated

#include "labels.hpp"

#include <stdlib.h>
int Image[15625];

static bool isIinitialized_Image = false;
void initialize_Image() {
	if (!isIinitialized_Image){
		int i;
		for (i=0; i < 15625; i++){
			Image[i] = i+1;
		}
		isIinitialized_Image = true;
	}
}

void read_Image(int labelAccessStatistics) {
	int numberOfBytes = 62500;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(Image) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = Image[i];
			a = Image[i+1];
			a = Image[i+2];
			a = Image[i+3];
			a = Image[i+4];
			a = Image[i+5];
			a = Image[i+6];
			a = Image[i+7];
			a = Image[i+8];
			a = Image[i+9];
		}
	
		for(;i<arraysize;i++){
			a = Image[i];
		}

		(void)a;
	}
}

void write_Image(int labelAccessStatistics) {
	int numberOfBytes = 62500;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(Image) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			Image[i]   = 0x800A;
			Image[i+1] = 0xAFFE;
			Image[i+2] = 0xAFFE;
			Image[i+3] = 0xAFFE;
			Image[i+4] = 0xAFFE;
			Image[i+5] = 0xAFFE;
			Image[i+6] = 0xAFFE;
			Image[i+7] = 0xAFFE;
			Image[i+8] = 0xAFFE;
			Image[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				Image[i]=0xAFFE;
		}
	}
}

int ResultsA[15625];

static bool isIinitialized_ResultsA = false;
void initialize_ResultsA() {
	if (!isIinitialized_ResultsA){
		int i;
		for (i=0; i < 15625; i++){
			ResultsA[i] = i+1;
		}
		isIinitialized_ResultsA = true;
	}
}

void read_ResultsA(int labelAccessStatistics) {
	int numberOfBytes = 62500;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(ResultsA) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = ResultsA[i];
			a = ResultsA[i+1];
			a = ResultsA[i+2];
			a = ResultsA[i+3];
			a = ResultsA[i+4];
			a = ResultsA[i+5];
			a = ResultsA[i+6];
			a = ResultsA[i+7];
			a = ResultsA[i+8];
			a = ResultsA[i+9];
		}
	
		for(;i<arraysize;i++){
			a = ResultsA[i];
		}

		(void)a;
	}
}

void write_ResultsA(int labelAccessStatistics) {
	int numberOfBytes = 62500;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(ResultsA) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			ResultsA[i]   = 0x800A;
			ResultsA[i+1] = 0xAFFE;
			ResultsA[i+2] = 0xAFFE;
			ResultsA[i+3] = 0xAFFE;
			ResultsA[i+4] = 0xAFFE;
			ResultsA[i+5] = 0xAFFE;
			ResultsA[i+6] = 0xAFFE;
			ResultsA[i+7] = 0xAFFE;
			ResultsA[i+8] = 0xAFFE;
			ResultsA[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				ResultsA[i]=0xAFFE;
		}
	}
}

int ResultsB[15625];

static bool isIinitialized_ResultsB = false;
void initialize_ResultsB() {
	if (!isIinitialized_ResultsB){
		int i;
		for (i=0; i < 15625; i++){
			ResultsB[i] = i+1;
		}
		isIinitialized_ResultsB = true;
	}
}

void read_ResultsB(int labelAccessStatistics) {
	int numberOfBytes = 62500;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(ResultsB) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = ResultsB[i];
			a = ResultsB[i+1];
			a = ResultsB[i+2];
			a = ResultsB[i+3];
			a = ResultsB[i+4];
			a = ResultsB[i+5];
			a = ResultsB[i+6];
			a = ResultsB[i+7];
			a = ResultsB[i+8];
			a = ResultsB[i+9];
		}
	
		for(;i<arraysize;i++){
			a = ResultsB[i];
		}

		(void)a;
	}
}

void write_ResultsB(int labelAccessStatistics) {
	int numberOfBytes = 62500;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(ResultsB) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			ResultsB[i]   = 0x800A;
			ResultsB[i+1] = 0xAFFE;
			ResultsB[i+2] = 0xAFFE;
			ResultsB[i+3] = 0xAFFE;
			ResultsB[i+4] = 0xAFFE;
			ResultsB[i+5] = 0xAFFE;
			ResultsB[i+6] = 0xAFFE;
			ResultsB[i+7] = 0xAFFE;
			ResultsB[i+8] = 0xAFFE;
			ResultsB[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				ResultsB[i]=0xAFFE;
		}
	}
}

int Image2[15625];

static bool isIinitialized_Image2 = false;
void initialize_Image2() {
	if (!isIinitialized_Image2){
		int i;
		for (i=0; i < 15625; i++){
			Image2[i] = i+1;
		}
		isIinitialized_Image2 = true;
	}
}

void read_Image2(int labelAccessStatistics) {
	int numberOfBytes = 62500;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(Image2) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = Image2[i];
			a = Image2[i+1];
			a = Image2[i+2];
			a = Image2[i+3];
			a = Image2[i+4];
			a = Image2[i+5];
			a = Image2[i+6];
			a = Image2[i+7];
			a = Image2[i+8];
			a = Image2[i+9];
		}
	
		for(;i<arraysize;i++){
			a = Image2[i];
		}

		(void)a;
	}
}

void write_Image2(int labelAccessStatistics) {
	int numberOfBytes = 62500;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(Image2) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			Image2[i]   = 0x800A;
			Image2[i+1] = 0xAFFE;
			Image2[i+2] = 0xAFFE;
			Image2[i+3] = 0xAFFE;
			Image2[i+4] = 0xAFFE;
			Image2[i+5] = 0xAFFE;
			Image2[i+6] = 0xAFFE;
			Image2[i+7] = 0xAFFE;
			Image2[i+8] = 0xAFFE;
			Image2[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				Image2[i]=0xAFFE;
		}
	}
}

int ResultsC[15625];

static bool isIinitialized_ResultsC = false;
void initialize_ResultsC() {
	if (!isIinitialized_ResultsC){
		int i;
		for (i=0; i < 15625; i++){
			ResultsC[i] = i+1;
		}
		isIinitialized_ResultsC = true;
	}
}

void read_ResultsC(int labelAccessStatistics) {
	int numberOfBytes = 62500;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(ResultsC) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = ResultsC[i];
			a = ResultsC[i+1];
			a = ResultsC[i+2];
			a = ResultsC[i+3];
			a = ResultsC[i+4];
			a = ResultsC[i+5];
			a = ResultsC[i+6];
			a = ResultsC[i+7];
			a = ResultsC[i+8];
			a = ResultsC[i+9];
		}
	
		for(;i<arraysize;i++){
			a = ResultsC[i];
		}

		(void)a;
	}
}

void write_ResultsC(int labelAccessStatistics) {
	int numberOfBytes = 62500;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(ResultsC) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			ResultsC[i]   = 0x800A;
			ResultsC[i+1] = 0xAFFE;
			ResultsC[i+2] = 0xAFFE;
			ResultsC[i+3] = 0xAFFE;
			ResultsC[i+4] = 0xAFFE;
			ResultsC[i+5] = 0xAFFE;
			ResultsC[i+6] = 0xAFFE;
			ResultsC[i+7] = 0xAFFE;
			ResultsC[i+8] = 0xAFFE;
			ResultsC[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				ResultsC[i]=0xAFFE;
		}
	}
}

int ResultsD[15625];

static bool isIinitialized_ResultsD = false;
void initialize_ResultsD() {
	if (!isIinitialized_ResultsD){
		int i;
		for (i=0; i < 15625; i++){
			ResultsD[i] = i+1;
		}
		isIinitialized_ResultsD = true;
	}
}

void read_ResultsD(int labelAccessStatistics) {
	int numberOfBytes = 62500;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(ResultsD) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = ResultsD[i];
			a = ResultsD[i+1];
			a = ResultsD[i+2];
			a = ResultsD[i+3];
			a = ResultsD[i+4];
			a = ResultsD[i+5];
			a = ResultsD[i+6];
			a = ResultsD[i+7];
			a = ResultsD[i+8];
			a = ResultsD[i+9];
		}
	
		for(;i<arraysize;i++){
			a = ResultsD[i];
		}

		(void)a;
	}
}

void write_ResultsD(int labelAccessStatistics) {
	int numberOfBytes = 62500;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(ResultsD) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			ResultsD[i]   = 0x800A;
			ResultsD[i+1] = 0xAFFE;
			ResultsD[i+2] = 0xAFFE;
			ResultsD[i+3] = 0xAFFE;
			ResultsD[i+4] = 0xAFFE;
			ResultsD[i+5] = 0xAFFE;
			ResultsD[i+6] = 0xAFFE;
			ResultsD[i+7] = 0xAFFE;
			ResultsD[i+8] = 0xAFFE;
			ResultsD[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				ResultsD[i]=0xAFFE;
		}
	}
}

int Image3[15625];

static bool isIinitialized_Image3 = false;
void initialize_Image3() {
	if (!isIinitialized_Image3){
		int i;
		for (i=0; i < 15625; i++){
			Image3[i] = i+1;
		}
		isIinitialized_Image3 = true;
	}
}

void read_Image3(int labelAccessStatistics) {
	int numberOfBytes = 62500;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(Image3) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = Image3[i];
			a = Image3[i+1];
			a = Image3[i+2];
			a = Image3[i+3];
			a = Image3[i+4];
			a = Image3[i+5];
			a = Image3[i+6];
			a = Image3[i+7];
			a = Image3[i+8];
			a = Image3[i+9];
		}
	
		for(;i<arraysize;i++){
			a = Image3[i];
		}

		(void)a;
	}
}

void write_Image3(int labelAccessStatistics) {
	int numberOfBytes = 62500;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(Image3) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			Image3[i]   = 0x800A;
			Image3[i+1] = 0xAFFE;
			Image3[i+2] = 0xAFFE;
			Image3[i+3] = 0xAFFE;
			Image3[i+4] = 0xAFFE;
			Image3[i+5] = 0xAFFE;
			Image3[i+6] = 0xAFFE;
			Image3[i+7] = 0xAFFE;
			Image3[i+8] = 0xAFFE;
			Image3[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				Image3[i]=0xAFFE;
		}
	}
}

int ResultsE[15625];

static bool isIinitialized_ResultsE = false;
void initialize_ResultsE() {
	if (!isIinitialized_ResultsE){
		int i;
		for (i=0; i < 15625; i++){
			ResultsE[i] = i+1;
		}
		isIinitialized_ResultsE = true;
	}
}

void read_ResultsE(int labelAccessStatistics) {
	int numberOfBytes = 62500;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(ResultsE) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = ResultsE[i];
			a = ResultsE[i+1];
			a = ResultsE[i+2];
			a = ResultsE[i+3];
			a = ResultsE[i+4];
			a = ResultsE[i+5];
			a = ResultsE[i+6];
			a = ResultsE[i+7];
			a = ResultsE[i+8];
			a = ResultsE[i+9];
		}
	
		for(;i<arraysize;i++){
			a = ResultsE[i];
		}

		(void)a;
	}
}

void write_ResultsE(int labelAccessStatistics) {
	int numberOfBytes = 62500;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(ResultsE) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			ResultsE[i]   = 0x800A;
			ResultsE[i+1] = 0xAFFE;
			ResultsE[i+2] = 0xAFFE;
			ResultsE[i+3] = 0xAFFE;
			ResultsE[i+4] = 0xAFFE;
			ResultsE[i+5] = 0xAFFE;
			ResultsE[i+6] = 0xAFFE;
			ResultsE[i+7] = 0xAFFE;
			ResultsE[i+8] = 0xAFFE;
			ResultsE[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				ResultsE[i]=0xAFFE;
		}
	}
}

int ResultsF[15625];

static bool isIinitialized_ResultsF = false;
void initialize_ResultsF() {
	if (!isIinitialized_ResultsF){
		int i;
		for (i=0; i < 15625; i++){
			ResultsF[i] = i+1;
		}
		isIinitialized_ResultsF = true;
	}
}

void read_ResultsF(int labelAccessStatistics) {
	int numberOfBytes = 62500;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
	
		int arraysize = sizeof(ResultsF) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		int a = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {           //iteration with 10 reads
			a = ResultsF[i];
			a = ResultsF[i+1];
			a = ResultsF[i+2];
			a = ResultsF[i+3];
			a = ResultsF[i+4];
			a = ResultsF[i+5];
			a = ResultsF[i+6];
			a = ResultsF[i+7];
			a = ResultsF[i+8];
			a = ResultsF[i+9];
		}
	
		for(;i<arraysize;i++){
			a = ResultsF[i];
		}

		(void)a;
	}
}

void write_ResultsF(int labelAccessStatistics) {
	int numberOfBytes = 62500;
	int repeat;
	for (repeat = 0 ; repeat < labelAccessStatistics; repeat++){
		if(numberOfBytes < 4){
			numberOfBytes = 4;
		}
		int arraysize = sizeof(ResultsF) / 4;
		int leftOverElements=arraysize%10;
		int arraySizeWith10Multiples=arraysize-leftOverElements;
		
		int i = 0;
		for (i = 0; i < arraySizeWith10Multiples; i = i + 10) {
			ResultsF[i]   = 0x800A;
			ResultsF[i+1] = 0xAFFE;
			ResultsF[i+2] = 0xAFFE;
			ResultsF[i+3] = 0xAFFE;
			ResultsF[i+4] = 0xAFFE;
			ResultsF[i+5] = 0xAFFE;
			ResultsF[i+6] = 0xAFFE;
			ResultsF[i+7] = 0xAFFE;
			ResultsF[i+8] = 0xAFFE;
			ResultsF[i+9] = 0xAFFE;
		}
		for(;i<arraysize;i++){
				ResultsF[i]=0xAFFE;
		}
	}
}

