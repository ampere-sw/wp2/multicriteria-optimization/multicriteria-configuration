// This code is auto-generated

#include "interProcessTriggerUtils.hpp"

using namespace std::chrono_literals;

void call_service_trigger_task2(rclcpp::Client<trigger_task2_service::srv::TriggerTask2Service>::SharedPtr& client) {
	
	auto request = std::make_shared<trigger_task2_service::srv::TriggerTask2Service::Request>();
	
	while (!client->wait_for_service(50ms)) {
		if (!rclcpp::ok()) {
			RCLCPP_ERROR(rclcpp::get_logger("rclcpp"), "Interrupted while waiting for the service. Exiting.");
			exit;
		}
		RCLCPP_INFO(rclcpp::get_logger("rclcpp"), "service not available, waiting again...");
	}
	auto result = client->async_send_request(request);	
}
