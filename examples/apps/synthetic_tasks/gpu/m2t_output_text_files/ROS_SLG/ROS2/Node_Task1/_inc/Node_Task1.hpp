// This code is auto-generated

#include <chrono>
#include <memory>

#include "rclcpp/rclcpp.hpp"
#include "std_msgs/msg/string.hpp"

#include "runnables.hpp"

using namespace std::chrono_literals;
using std::placeholders::_1;
#define CONSOLE_ENABLED

#if defined(_OPENMP)
int check_Node_Task1(int *original, int *replicated){ return 1;}
#endif
class Node_Task1 : public rclcpp::Node
{
	private:
		rclcpp::TimerBase::SharedPtr timer_stepstimulus_;
		rclcpp::Client<trigger_task2_service::srv::TriggerTask2Service>::SharedPtr trigger_task2_client;

	public:
		Node_Task1()
		: Node("node_task1")
		{
			timer_stepstimulus_ = this->create_wall_timer(
					500ms, std::bind(&Node_Task1::timer_stepstimulus_callback, this));
			trigger_task2_client =  this->create_client<trigger_task2_service::srv::TriggerTask2Service>("trigger_task2_service");
		}
	void timer_stepstimulus_callback() {
	#ifdef CONSOLE_ENABLED
		std::cout << "Timer_stepstimulus_callback (500ms)" << std::endl;
	#endif
		extern int ResultsA[15625];
		extern int Image[15625];
		extern int ResultsB[15625];
		int dummy;
		
		 #if defined(_OPENMP)
		 #pragma omp taskgraph tdg_type(static) nowait
		#endif
		{
		#if defined(_OPENMP)
		 #pragma omp task depend(out:Image)
		#elif defined(_OMPSS)
		 #pragma oss task depend(out:Image)
		#endif
		run_read_image();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(inout:Image)
		#elif defined(_OMPSS)
		 #pragma oss task depend(inout:Image)
		#endif
		run_convert_image();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:Image) depend(out:ResultsA)
		 #pragma omp target  map(to:Image[0:15625]) map(from:ResultsA[0:15625])
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:Image) depend(out:ResultsA) map(to:Image[0:15625]) map(from:ResultsA[0:15625]) copy_deps
		#endif
		run_analysisA();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:Image) depend(out:ResultsB)
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:Image) depend(out:ResultsB)
		#endif
		run_analysisB();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:ResultsB,ResultsA) replicated(2, dummy, check_Node_Task1)
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:ResultsB,ResultsA) replicated(2, dummy, check_Node_Task1)
		#endif
		run_merge_results(trigger_task2_client);
		}
		#pragma omp taskwait;
	}
	
	
};
