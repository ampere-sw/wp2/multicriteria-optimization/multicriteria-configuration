#include "Node_Task1.hpp"
#include "Node_Task2.hpp"
#include "Node_Task3.hpp"

#include <extrae.h>
extern "C" {
	#include <cudamperf.h>
}
int main(int argc, char *argv[])
{
	cudamperf_start(argv[0]);
	setvbuf(stdout, NULL, _IONBF, BUFSIZ);
	rclcpp::init(argc, argv);
	
	Extrae_init();
	auto node1 = new Node_Task1();
	auto node2 = new Node_Task2();
	auto node3 = new Node_Task3();

	#pragma omp parallel 
	#pragma omp single
	{
		cudamperf_enter_omp_region("Task1");
		node1->timer_stepstimulus_callback();
		cudamperf_exit_omp_region();
	}

	#pragma omp parallel 
	#pragma omp single
	{
		cudamperf_enter_omp_region("Task2");
		node2->trigger_task2_service_callback(NULL,NULL);
		cudamperf_exit_omp_region();
	}

	#pragma omp parallel 
	#pragma omp single
	{
		cudamperf_enter_omp_region("Task3");
		node3->timer_another_stimulus_callback();
		cudamperf_exit_omp_region();
	}
	cudamperf_stop();
	rclcpp::shutdown();

	Extrae_fini();
	return 0;
}