# generated from
# rosidl_cmake/cmake/template/rosidl_cmake_export_typesupport_targets.cmake.in

set(_exported_typesupport_targets
  "__rosidl_typesupport_introspection_c:trigger_task2_service__rosidl_typesupport_introspection_c;__rosidl_typesupport_introspection_cpp:trigger_task2_service__rosidl_typesupport_introspection_cpp")

# populate trigger_task2_service_TARGETS_<suffix>
if(NOT _exported_typesupport_targets STREQUAL "")
  # loop over typesupport targets
  foreach(_tuple ${_exported_typesupport_targets})
    string(REPLACE ":" ";" _tuple "${_tuple}")
    list(GET _tuple 0 _suffix)
    list(GET _tuple 1 _target)

    set(_target "trigger_task2_service::${_target}")
    if(NOT TARGET "${_target}")
      # the exported target must exist
      message(WARNING "Package 'trigger_task2_service' exports the typesupport target '${_target}' which doesn't exist")
    else()
      list(APPEND trigger_task2_service_TARGETS${_suffix} "${_target}")
    endif()
  endforeach()
endif()
