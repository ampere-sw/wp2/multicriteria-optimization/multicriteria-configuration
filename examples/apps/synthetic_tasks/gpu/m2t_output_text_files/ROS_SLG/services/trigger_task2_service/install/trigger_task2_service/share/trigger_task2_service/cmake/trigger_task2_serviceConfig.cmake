# generated from ament/cmake/core/templates/nameConfig.cmake.in

# prevent multiple inclusion
if(_trigger_task2_service_CONFIG_INCLUDED)
  # ensure to keep the found flag the same
  if(NOT DEFINED trigger_task2_service_FOUND)
    # explicitly set it to FALSE, otherwise CMake will set it to TRUE
    set(trigger_task2_service_FOUND FALSE)
  elseif(NOT trigger_task2_service_FOUND)
    # use separate condition to avoid uninitialized variable warning
    set(trigger_task2_service_FOUND FALSE)
  endif()
  return()
endif()
set(_trigger_task2_service_CONFIG_INCLUDED TRUE)

# output package information
if(NOT trigger_task2_service_FIND_QUIETLY)
  message(STATUS "Found trigger_task2_service: 0.0.0 (${trigger_task2_service_DIR})")
endif()

# warn when using a deprecated package
if(NOT "" STREQUAL "")
  set(_msg "Package 'trigger_task2_service' is deprecated")
  # append custom deprecation text if available
  if(NOT "" STREQUAL "TRUE")
    set(_msg "${_msg} ()")
  endif()
  # optionally quiet the deprecation message
  if(NOT ${trigger_task2_service_DEPRECATED_QUIET})
    message(DEPRECATION "${_msg}")
  endif()
endif()

# flag package as ament-based to distinguish it after being find_package()-ed
set(trigger_task2_service_FOUND_AMENT_PACKAGE TRUE)

# include all config extra files
set(_extras "rosidl_cmake-extras.cmake;ament_cmake_export_dependencies-extras.cmake;ament_cmake_export_libraries-extras.cmake;ament_cmake_export_targets-extras.cmake;ament_cmake_export_include_directories-extras.cmake;rosidl_cmake_export_typesupport_libraries-extras.cmake;rosidl_cmake_export_typesupport_targets-extras.cmake")
foreach(_extra ${_extras})
  include("${trigger_task2_service_DIR}/${_extra}")
endforeach()
