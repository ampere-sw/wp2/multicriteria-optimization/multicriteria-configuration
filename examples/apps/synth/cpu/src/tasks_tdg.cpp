#include "tdg.hpp"
int tasks_kmp_tdg_outs_1[7] = {1, 2, 3, 3, 4, 5, 5};
struct kmp_node_info tasks_kmp_tdg_1[6] = {{ .static_id = 0, .task = NULL, .succesors = &tasks_kmp_tdg_outs_1[0], .nsuccessors = 2, .npredecessors_counter = {0}, .npredecessors = 0, .successors_size = 0, .static_thread = -1, .pragma_id = -1, .private_data = NULL, .shared_data = NULL, .parent_task = NULL, .next_waiting_tdg = NULL},
{ .static_id = 1, .task = NULL, .succesors = &tasks_kmp_tdg_outs_1[2], .nsuccessors = 1, .npredecessors_counter = {1}, .npredecessors = 1, .successors_size = 0, .static_thread = -1, .pragma_id = -1, .private_data = NULL, .shared_data = NULL, .parent_task = NULL, .next_waiting_tdg = NULL},
{ .static_id = 2, .task = NULL, .succesors = &tasks_kmp_tdg_outs_1[3], .nsuccessors = 2, .npredecessors_counter = {1}, .npredecessors = 1, .successors_size = 0, .static_thread = -1, .pragma_id = -1, .private_data = NULL, .shared_data = NULL, .parent_task = NULL, .next_waiting_tdg = NULL},
{ .static_id = 3, .task = NULL, .succesors = &tasks_kmp_tdg_outs_1[5], .nsuccessors = 1, .npredecessors_counter = {2}, .npredecessors = 2, .successors_size = 0, .static_thread = -1, .pragma_id = -1, .private_data = NULL, .shared_data = NULL, .parent_task = NULL, .next_waiting_tdg = NULL},
{ .static_id = 4, .task = NULL, .succesors = &tasks_kmp_tdg_outs_1[6], .nsuccessors = 1, .npredecessors_counter = {1}, .npredecessors = 1, .successors_size = 0, .static_thread = -1, .pragma_id = -1, .private_data = NULL, .shared_data = NULL, .parent_task = NULL, .next_waiting_tdg = NULL},
{ .static_id = 5, .task = NULL, .succesors = &tasks_kmp_tdg_outs_1[7], .nsuccessors = 0, .npredecessors_counter = {2}, .npredecessors = 2, .successors_size = 0, .static_thread = -1, .pragma_id = -1, .private_data = NULL, .shared_data = NULL, .parent_task = NULL, .next_waiting_tdg = NULL}};
int tasks_kmp_tdg_roots_1[1] = {0};
extern "C" void tasks_kmp_set_tdg___captured_stmt_(void *loc_ref, int gtid, void (*entry)(void *), void *args, int tdg_type, int if_cond, bool nowait, int num_preallocs)
{
  __kmpc_set_tdg(tasks_kmp_tdg_1, gtid, 1, 6, tasks_kmp_tdg_roots_1, 1);
  __kmpc_taskgraph(loc_ref, gtid, 1, entry, args, tdg_type, if_cond, nowait);
}
int tasks_kmp_tdg_outs_2[10] = {1, 2, 3, 5, 6, 4, 5, 7, 7, 7};
struct kmp_node_info tasks_kmp_tdg_2[8] = {{ .static_id = 0, .task = NULL, .succesors = &tasks_kmp_tdg_outs_2[0], .nsuccessors = 3, .npredecessors_counter = {0}, .npredecessors = 0, .successors_size = 0, .static_thread = -1, .pragma_id = -1, .private_data = NULL, .shared_data = NULL, .parent_task = NULL, .next_waiting_tdg = NULL},
{ .static_id = 1, .task = NULL, .succesors = &tasks_kmp_tdg_outs_2[3], .nsuccessors = 2, .npredecessors_counter = {1}, .npredecessors = 1, .successors_size = 0, .static_thread = -1, .pragma_id = -1, .private_data = NULL, .shared_data = NULL, .parent_task = NULL, .next_waiting_tdg = NULL},
{ .static_id = 2, .task = NULL, .succesors = &tasks_kmp_tdg_outs_2[5], .nsuccessors = 1, .npredecessors_counter = {1}, .npredecessors = 1, .successors_size = 0, .static_thread = -1, .pragma_id = -1, .private_data = NULL, .shared_data = NULL, .parent_task = NULL, .next_waiting_tdg = NULL},
{ .static_id = 3, .task = NULL, .succesors = &tasks_kmp_tdg_outs_2[6], .nsuccessors = 1, .npredecessors_counter = {1}, .npredecessors = 1, .successors_size = 0, .static_thread = -1, .pragma_id = -1, .private_data = NULL, .shared_data = NULL, .parent_task = NULL, .next_waiting_tdg = NULL},
{ .static_id = 4, .task = NULL, .succesors = &tasks_kmp_tdg_outs_2[7], .nsuccessors = 1, .npredecessors_counter = {1}, .npredecessors = 1, .successors_size = 0, .static_thread = -1, .pragma_id = -1, .private_data = NULL, .shared_data = NULL, .parent_task = NULL, .next_waiting_tdg = NULL},
{ .static_id = 5, .task = NULL, .succesors = &tasks_kmp_tdg_outs_2[8], .nsuccessors = 1, .npredecessors_counter = {2}, .npredecessors = 2, .successors_size = 0, .static_thread = -1, .pragma_id = -1, .private_data = NULL, .shared_data = NULL, .parent_task = NULL, .next_waiting_tdg = NULL},
{ .static_id = 6, .task = NULL, .succesors = &tasks_kmp_tdg_outs_2[9], .nsuccessors = 1, .npredecessors_counter = {1}, .npredecessors = 1, .successors_size = 0, .static_thread = -1, .pragma_id = -1, .private_data = NULL, .shared_data = NULL, .parent_task = NULL, .next_waiting_tdg = NULL},
{ .static_id = 7, .task = NULL, .succesors = &tasks_kmp_tdg_outs_2[10], .nsuccessors = 0, .npredecessors_counter = {3}, .npredecessors = 3, .successors_size = 0, .static_thread = -1, .pragma_id = -1, .private_data = NULL, .shared_data = NULL, .parent_task = NULL, .next_waiting_tdg = NULL}};
int tasks_kmp_tdg_roots_2[1] = {0};
extern "C" void tasks_kmp_set_tdg___captured_stmt_13(void *loc_ref, int gtid, void (*entry)(void *), void *args, int tdg_type, int if_cond, bool nowait, int num_preallocs)
{
  __kmpc_set_tdg(tasks_kmp_tdg_2, gtid, 2, 8, tasks_kmp_tdg_roots_2, 1);
  __kmpc_taskgraph(loc_ref, gtid, 2, entry, args, tdg_type, if_cond, nowait);
}
int tasks_kmp_tdg_outs_3[12] = {1, 2, 3, 4, 5, 6, 7, 7, 7, 7, 7, 7};
struct kmp_node_info tasks_kmp_tdg_3[8] = {{ .static_id = 0, .task = NULL, .succesors = &tasks_kmp_tdg_outs_3[0], .nsuccessors = 6, .npredecessors_counter = {0}, .npredecessors = 0, .successors_size = 0, .static_thread = -1, .pragma_id = -1, .private_data = NULL, .shared_data = NULL, .parent_task = NULL, .next_waiting_tdg = NULL},
{ .static_id = 1, .task = NULL, .succesors = &tasks_kmp_tdg_outs_3[6], .nsuccessors = 1, .npredecessors_counter = {1}, .npredecessors = 1, .successors_size = 0, .static_thread = -1, .pragma_id = -1, .private_data = NULL, .shared_data = NULL, .parent_task = NULL, .next_waiting_tdg = NULL},
{ .static_id = 2, .task = NULL, .succesors = &tasks_kmp_tdg_outs_3[7], .nsuccessors = 1, .npredecessors_counter = {1}, .npredecessors = 1, .successors_size = 0, .static_thread = -1, .pragma_id = -1, .private_data = NULL, .shared_data = NULL, .parent_task = NULL, .next_waiting_tdg = NULL},
{ .static_id = 3, .task = NULL, .succesors = &tasks_kmp_tdg_outs_3[8], .nsuccessors = 1, .npredecessors_counter = {1}, .npredecessors = 1, .successors_size = 0, .static_thread = -1, .pragma_id = -1, .private_data = NULL, .shared_data = NULL, .parent_task = NULL, .next_waiting_tdg = NULL},
{ .static_id = 4, .task = NULL, .succesors = &tasks_kmp_tdg_outs_3[9], .nsuccessors = 1, .npredecessors_counter = {1}, .npredecessors = 1, .successors_size = 0, .static_thread = -1, .pragma_id = -1, .private_data = NULL, .shared_data = NULL, .parent_task = NULL, .next_waiting_tdg = NULL},
{ .static_id = 5, .task = NULL, .succesors = &tasks_kmp_tdg_outs_3[10], .nsuccessors = 1, .npredecessors_counter = {1}, .npredecessors = 1, .successors_size = 0, .static_thread = -1, .pragma_id = -1, .private_data = NULL, .shared_data = NULL, .parent_task = NULL, .next_waiting_tdg = NULL},
{ .static_id = 6, .task = NULL, .succesors = &tasks_kmp_tdg_outs_3[11], .nsuccessors = 1, .npredecessors_counter = {1}, .npredecessors = 1, .successors_size = 0, .static_thread = -1, .pragma_id = -1, .private_data = NULL, .shared_data = NULL, .parent_task = NULL, .next_waiting_tdg = NULL},
{ .static_id = 7, .task = NULL, .succesors = &tasks_kmp_tdg_outs_3[12], .nsuccessors = 0, .npredecessors_counter = {6}, .npredecessors = 6, .successors_size = 0, .static_thread = -1, .pragma_id = -1, .private_data = NULL, .shared_data = NULL, .parent_task = NULL, .next_waiting_tdg = NULL}};
int tasks_kmp_tdg_roots_3[1] = {0};
extern "C" void tasks_kmp_set_tdg___captured_stmt_33(void *loc_ref, int gtid, void (*entry)(void *), void *args, int tdg_type, int if_cond, bool nowait, int num_preallocs)
{
  __kmpc_set_tdg(tasks_kmp_tdg_3, gtid, 3, 8, tasks_kmp_tdg_roots_3, 1);
  __kmpc_taskgraph(loc_ref, gtid, 3, entry, args, tdg_type, if_cond, nowait);
}