#include "common.h"
#include "tasks.h"


int main(){

    #ifdef EXTRAE
    Extrae_init();
    #endif

    #ifdef TASK_X
    task_x();
    #endif
    
    #ifdef TASK_Y
    task_y();
    #endif

    #ifdef TASK_Z
    task_z();
    #endif

    #ifdef EXTRAE
    Extrae_fini();
    #endif
}