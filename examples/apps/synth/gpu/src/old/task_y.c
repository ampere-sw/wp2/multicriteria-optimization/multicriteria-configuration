#include <stdio.h>
#include <math.h>
#include <omp.h>

#define BASE_TIME 10
#define TIME_UNIT 6

static void kernel_y(int a_time,int b_time,int c_time,int d_time,int e_time, int f_time, int g_time){

    int a,b,c,d,e,f,g;
    int threads[] = {-1,-1,-1,-1,-1,-1,-1,-1};
    #pragma omp task depend(out: a) shared(threads)
    {
        for(int i = 0; i < a_time*pow(10,TIME_UNIT);i++){
            asm ("nop");
            a+=i;
        }
        threads[0] = omp_get_thread_num();
    }

    #pragma omp task depend(in: a) depend(out: b) shared(threads)
    {
        for(int i = 0; i < b_time*pow(10,TIME_UNIT);i++){
            asm ("nop");
            b+=a+i;
        }
        threads[1] = omp_get_thread_num();
    }

    #pragma omp task depend(in: a) depend(out: c) shared(threads)
    {
        for(int i = 0; i < c_time*pow(10,TIME_UNIT);i++){
            asm ("nop");
            c+=a+i*2;
        }
        threads[2] = omp_get_thread_num();
    }

    #pragma omp task depend(in: a) depend(out: d) shared(threads) 
    {
        for(int i = 0; i < d_time*pow(10,TIME_UNIT);i++){
            asm ("nop");
            d+=a*a+i*2;
        }
        threads[3] = omp_get_thread_num();
    }

    #pragma omp task depend(in: c) depend(out: e) shared(threads) 
    {
        for(int i = 0; i < f_time*pow(10,TIME_UNIT);i++){
            asm ("nop");
            e+=c+i*2;
        }
        threads[4] = omp_get_thread_num();
    }

    #pragma omp task depend(in: b, d) depend(out: f) shared(threads) 
    {
        for(int i = 0; i < g_time*pow(10,TIME_UNIT);i++){
            asm ("nop");
            f+=d*b+i*2;
        }
        threads[5] = omp_get_thread_num();
    }

    #pragma omp task depend(in: b) depend(out: g) shared(threads) 
    {
        for(int i = 0; i < e_time*pow(10,TIME_UNIT);i++){
            asm ("nop");
            g+=b+i*2;
        }
        threads[6] = omp_get_thread_num();
    }

    #pragma omp task depend(in: e,f,g) shared(threads) 
    {
        threads[7] = omp_get_thread_num();
        printf("Done! Mapping: %d,%d,%d,%d,%d,%d,%d,%d\n",threads[0],threads[1],threads[2],threads[3],threads[4], threads[5],threads[6],threads[7]);
    }

}


void task_y(){
    int a_time = BASE_TIME+34;
    int b_time = BASE_TIME+15;
    int c_time = BASE_TIME+30;
    int d_time = BASE_TIME+48;
    int e_time = BASE_TIME+65;
    int f_time = BASE_TIME+20;
    int g_time = BASE_TIME+100;
   
    #pragma omp parallel
    #pragma omp single
    #ifdef TDG
    #pragma omp taskgraph tdg_type(static)
    #endif
    kernel_y(a_time,b_time,c_time,d_time,e_time, f_time, g_time);
    
}