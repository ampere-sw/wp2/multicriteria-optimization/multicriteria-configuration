// This code is auto-generated

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/ioctl.h>
#include <linux/perf_event.h>
#include <linux/hw_breakpoint.h>
#include <asm/unistd.h>
#include <inttypes.h>

#include <errno.h>

int instrument_start(pid_t pid, uint64_t event_list[], int total_events);
void instrument_stop(int fd, char* filename);
void instrument_read(int fd);
void instrument_reset(int fd);
void instrument_read_to_file(int fd, char* filename);
