// This code is auto-generated

#include "channelSendUtils.hpp"


void publish_to_recognizedspeedlimit(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher) {
	
	auto message = std_msgs::msg::String();
	message.data ="recognizedspeedlimit";
	//message.data ="AAAA";
	publisher->publish(message);
	std::cout << "ROS2: Publishing message " << message.data << std::endl;
}

void publish_to_pccspeedsetpoint(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher) {
	
	auto message = std_msgs::msg::String();
	message.data ="pccspeedsetpoint";
	//message.data ="AAAA";
	publisher->publish(message);
	std::cout << "ROS2: Publishing message " << message.data << std::endl;
}

void publish_to_operationsetpoint(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher) {
	
	auto message = std_msgs::msg::String();
	message.data ="operationsetpoint";
	//message.data ="AAAA";
	publisher->publish(message);
	std::cout << "ROS2: Publishing message " << message.data << std::endl;
}
