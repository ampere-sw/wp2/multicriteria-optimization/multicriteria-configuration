// This code is auto-generated

#include <chrono>
#include <memory>

#include "rclcpp/rclcpp.hpp"
#include "std_msgs/msg/string.hpp"

#include "runnables.hpp"
#include "trigger_perception_acc_service/srv/trigger_perception_acc_service.hpp"

using namespace std::chrono_literals;
using std::placeholders::_1;
#define CONSOLE_ENABLED

#if defined(_OPENMP)
int check_Node_perception_acc(int *original, int *replicated){ return 1;}
#endif
class Node_perception_acc : public rclcpp::Node
{
	private:
		rclcpp::Service<trigger_perception_acc_service::srv::TriggerPerceptionAccService>::SharedPtr trigger_perception_acc_service;
		rclcpp::Client<trigger_world_model_acc_service::srv::TriggerWorldModelAccService>::SharedPtr trigger_world_model_acc_client;

	public:
		Node_perception_acc()
		: Node("node_perception_acc")
		{
			trigger_perception_acc_service = this->create_service<trigger_perception_acc_service::srv::TriggerPerceptionAccService>(
				"trigger_perception_acc_service", 
				std::bind(&Node_perception_acc::trigger_perception_acc_service_callback, this, std::placeholders::_1, std::placeholders::_2));
			trigger_world_model_acc_client =  this->create_client<trigger_world_model_acc_service::srv::TriggerWorldModelAccService>("trigger_world_model_acc_service");
		}
	
	void trigger_perception_acc_service_callback(const std::shared_ptr<trigger_perception_acc_service::srv::TriggerPerceptionAccService::Request> request,
		std::shared_ptr<trigger_perception_acc_service::srv::TriggerPerceptionAccService::Response> response) {
			(void)request;
			(void)response;
	#ifdef CONSOLE_ENABLED
		std::cout << "Starting trigger_perception_acc_service_callback" << std::endl;
	#endif
		int dummy;
		
		 #if defined(_OPENMP)
		 #pragma omp taskgraph tdg_type(static) nowait
		#endif
		{
		#if defined(_OPENMP)
		 #pragma omp task
		#elif defined(_OMPSS)
		 #pragma oss task
		#endif
		run_run13();
		
		#if defined(_OPENMP)
		 #pragma omp task
		#elif defined(_OMPSS)
		 #pragma oss task
		#endif
		run_run14();
		
		#if defined(_OPENMP)
		 #pragma omp task
		#elif defined(_OMPSS)
		 #pragma oss task
		#endif
		run_run15();
		
		#if defined(_OPENMP)
		 #pragma omp task
		#elif defined(_OMPSS)
		 #pragma oss task
		#endif
		run_run16();
		
		#if defined(_OPENMP)
		 #pragma omp task
		#elif defined(_OMPSS)
		 #pragma oss task
		#endif
		run_run17();
		
		#if defined(_OPENMP)
		 #pragma omp task
		#elif defined(_OMPSS)
		 #pragma oss task
		#endif
		run_run18();
		
		#if defined(_OPENMP)
		 #pragma omp task
		#elif defined(_OMPSS)
		 #pragma oss task
		#endif
		run_run19();
		
		#if defined(_OPENMP)
		 #pragma omp task
		#elif defined(_OMPSS)
		 #pragma oss task
		#endif
		run_run20();
		
		#if defined(_OPENMP)
		 #pragma omp task
		#elif defined(_OMPSS)
		 #pragma oss task
		#endif
		run_run21();
		
		#if defined(_OPENMP)
		 #pragma omp task
		#elif defined(_OMPSS)
		 #pragma oss task
		#endif
		run_run22();
		
		#if defined(_OPENMP)
		 #pragma omp task
		#elif defined(_OMPSS)
		 #pragma oss task
		#endif
		run_run23();
		
		#if defined(_OPENMP)
		 #pragma omp task
		#elif defined(_OMPSS)
		 #pragma oss task
		#endif
		run_run24();
		
		#if defined(_OPENMP)
		 #pragma omp task
		#elif defined(_OMPSS)
		 #pragma oss task
		#endif
		run_run25();
		
		#if defined(_OPENMP)
		 #pragma omp task
		#elif defined(_OMPSS)
		 #pragma oss task
		#endif
		run_run26();
		
		#if defined(_OPENMP)
		 #pragma omp task
		#elif defined(_OMPSS)
		 #pragma oss task
		#endif
		run_run27();
		
		#if defined(_OPENMP)
		 #pragma omp task
		#elif defined(_OMPSS)
		 #pragma oss task
		#endif
		run_run28();
		
		#if defined(_OPENMP)
		 #pragma omp task
		#elif defined(_OMPSS)
		 #pragma oss task
		#endif
		run_run29();
		
		#if defined(_OPENMP)
		 #pragma omp task
		#elif defined(_OMPSS)
		 #pragma oss task
		#endif
		run_run30();
		
		#if defined(_OPENMP)
		 #pragma omp task
		#elif defined(_OMPSS)
		 #pragma oss task
		#endif
		run_run31();
		
		#if defined(_OPENMP)
		 #pragma omp task
		#elif defined(_OMPSS)
		 #pragma oss task
		#endif
		run_run32();
		
		#if defined(_OPENMP)
		 #pragma omp task
		#elif defined(_OMPSS)
		 #pragma oss task
		#endif
		run_run33();
		
		#if defined(_OPENMP)
		 #pragma omp task
		#elif defined(_OMPSS)
		 #pragma oss task
		#endif
		run_run34();
		
		#if defined(_OPENMP)
		 #pragma omp task
		#elif defined(_OMPSS)
		 #pragma oss task
		#endif
		run_run35();
		
		#if defined(_OPENMP)
		 #pragma omp task
		#elif defined(_OMPSS)
		 #pragma oss task
		#endif
		run_run36();
		
		#if defined(_OPENMP)
		 #pragma omp task
		#elif defined(_OMPSS)
		 #pragma oss task
		#endif
		run_run37();
		
		#if defined(_OPENMP)
		 #pragma omp task
		#elif defined(_OMPSS)
		 #pragma oss task
		#endif
		run_run38();
		
		#if defined(_OPENMP)
		 #pragma omp task
		#elif defined(_OMPSS)
		 #pragma oss task
		#endif
		run_run39();
		
		#if defined(_OPENMP)
		 #pragma omp task
		#elif defined(_OMPSS)
		 #pragma oss task
		#endif
		run_run40();
		
		#if defined(_OPENMP)
		 #pragma omp task
		#elif defined(_OMPSS)
		 #pragma oss task
		#endif
		run_run41(trigger_world_model_acc_client);
		}
		#pragma omp taskwait;
	}
	
};
