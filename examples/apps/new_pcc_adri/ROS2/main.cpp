#include "Node_classification_tsr.hpp"
#include "Node_cominput_tsr.hpp"
#include "Node_control_behavior_acc.hpp"
#include "Node_databroker_acc.hpp"
#include "Node_databroker_ecm.hpp"
#include "Node_databroker_pcc.hpp"
#include "Node_detection_tsr.hpp"
#include "Node_gaussian_filter_tsr.hpp"
#include "Node_input_acc.hpp"
#include "Node_isr_10_ecm.hpp"
#include "Node_isr_11_ecm.hpp"
#include "Node_isr_1_ecm.hpp"
#include "Node_isr_2_ecm.hpp"
#include "Node_isr_3_ecm.hpp"
#include "Node_isr_4_ecm.hpp"
#include "Node_isr_5_ecm.hpp"
#include "Node_isr_6_ecm.hpp"
#include "Node_isr_7_ecm.hpp"
#include "Node_isr_8_ecm.hpp"
#include "Node_isr_9_ecm.hpp"
#include "Node_output_acc.hpp"
#include "Node_output_tsr.hpp"
#include "Node_perception_acc.hpp"
#include "Node_prediction_pcc.hpp"
#include "Node_preprocessing_acc.hpp"
#include "Node_resizing_tsr.hpp"
#include "Node_segmentation_to_background_tsr.hpp"
#include "Node_segmentation_tsr.hpp"
#include "Node_task_1000ms_ecm.hpp"
#include "Node_task_100ms_ecm.hpp"
#include "Node_task_10ms_ecm.hpp"
#include "Node_task_1ms_ecm.hpp"
#include "Node_task_200ms_ecm.hpp"
#include "Node_task_20ms_ecm.hpp"
#include "Node_task_2ms_ecm.hpp"
#include "Node_task_50ms_ecm.hpp"
#include "Node_task_5ms_ecm.hpp"
#include "Node_trajectory_optimizer_pcc.hpp"
#include "Node_world_model_acc.hpp"

int main(int argc, char *argv[])
{
	std::vector<std::thread> threads;

	setvbuf(stdout, NULL, _IONBF, BUFSIZ);
	rclcpp::init(argc, argv);

//EXECUTOR 1

rclcpp::executors::SingleThreadedExecutor executor1;
auto node1 = std::make_shared<Node_classification_tsr>();
executor1.add_node(node1);

auto spin_executor1 = [&executor1]()
{
	executor1.spin();
};
threads.emplace_back(std::thread(spin_executor1));

//EXECUTOR 2

rclcpp::executors::SingleThreadedExecutor executor2;
auto node2 = std::make_shared<Node_cominput_tsr>();
executor2.add_node(node2);

auto spin_executor2 = [&executor2]()
{
	executor2.spin();
};
threads.emplace_back(std::thread(spin_executor2));

//EXECUTOR 3

rclcpp::executors::SingleThreadedExecutor executor3;
auto node3 = std::make_shared<Node_control_behavior_acc>();
executor3.add_node(node3);

auto spin_executor3 = [&executor3]()
{
	executor3.spin();
};
threads.emplace_back(std::thread(spin_executor3));

//EXECUTOR 4

rclcpp::executors::SingleThreadedExecutor executor4;
auto node4 = std::make_shared<Node_databroker_acc>();
executor4.add_node(node4);

auto spin_executor4 = [&executor4]()
{
	executor4.spin();
};
threads.emplace_back(std::thread(spin_executor4));

//EXECUTOR 5

rclcpp::executors::SingleThreadedExecutor executor5;
auto node5 = std::make_shared<Node_databroker_ecm>();
executor5.add_node(node5);

auto spin_executor5 = [&executor5]()
{
	executor5.spin();
};
threads.emplace_back(std::thread(spin_executor5));

//EXECUTOR 6

rclcpp::executors::SingleThreadedExecutor executor6;
auto node6 = std::make_shared<Node_databroker_pcc>();
executor6.add_node(node6);

auto spin_executor6 = [&executor6]()
{
	executor6.spin();
};
threads.emplace_back(std::thread(spin_executor6));

//EXECUTOR 7

rclcpp::executors::SingleThreadedExecutor executor7;
auto node7 = std::make_shared<Node_detection_tsr>();
executor7.add_node(node7);

auto spin_executor7 = [&executor7]()
{
	executor7.spin();
};
threads.emplace_back(std::thread(spin_executor7));

//EXECUTOR 8

rclcpp::executors::SingleThreadedExecutor executor8;
auto node8 = std::make_shared<Node_gaussian_filter_tsr>();
executor8.add_node(node8);

auto spin_executor8 = [&executor8]()
{
	executor8.spin();
};
threads.emplace_back(std::thread(spin_executor8));

//EXECUTOR 9

rclcpp::executors::SingleThreadedExecutor executor9;
auto node9 = std::make_shared<Node_input_acc>();
executor9.add_node(node9);

auto spin_executor9 = [&executor9]()
{
	#pragma omp parallel thread_sched(0,10000,10000,10000) thread_sched(1,10000,50,10) 
	#pragma omp single
	executor9.spin();
};
threads.emplace_back(std::thread(spin_executor9));

//EXECUTOR 10

rclcpp::executors::SingleThreadedExecutor executor10;
auto node10 = std::make_shared<Node_isr_10_ecm>();
executor10.add_node(node10);

auto spin_executor10 = [&executor10]()
{
	executor10.spin();
};
threads.emplace_back(std::thread(spin_executor10));

//EXECUTOR 11

rclcpp::executors::SingleThreadedExecutor executor11;
auto node11 = std::make_shared<Node_isr_11_ecm>();
executor11.add_node(node11);

auto spin_executor11 = [&executor11]()
{
	executor11.spin();
};
threads.emplace_back(std::thread(spin_executor11));

//EXECUTOR 12

rclcpp::executors::SingleThreadedExecutor executor12;
auto node12 = std::make_shared<Node_isr_1_ecm>();
executor12.add_node(node12);

auto spin_executor12 = [&executor12]()
{
	executor12.spin();
};
threads.emplace_back(std::thread(spin_executor12));

//EXECUTOR 13

rclcpp::executors::SingleThreadedExecutor executor13;
auto node13 = std::make_shared<Node_isr_2_ecm>();
executor13.add_node(node13);

auto spin_executor13 = [&executor13]()
{
	executor13.spin();
};
threads.emplace_back(std::thread(spin_executor13));

//EXECUTOR 14

rclcpp::executors::SingleThreadedExecutor executor14;
auto node14 = std::make_shared<Node_isr_3_ecm>();
executor14.add_node(node14);

auto spin_executor14 = [&executor14]()
{
	executor14.spin();
};
threads.emplace_back(std::thread(spin_executor14));

//EXECUTOR 15

rclcpp::executors::SingleThreadedExecutor executor15;
auto node15 = std::make_shared<Node_isr_4_ecm>();
executor15.add_node(node15);

auto spin_executor15 = [&executor15]()
{
	executor15.spin();
};
threads.emplace_back(std::thread(spin_executor15));

//EXECUTOR 16

rclcpp::executors::SingleThreadedExecutor executor16;
auto node16 = std::make_shared<Node_isr_5_ecm>();
executor16.add_node(node16);

auto spin_executor16 = [&executor16]()
{
	executor16.spin();
};
threads.emplace_back(std::thread(spin_executor16));

//EXECUTOR 17

rclcpp::executors::SingleThreadedExecutor executor17;
auto node17 = std::make_shared<Node_isr_6_ecm>();
executor17.add_node(node17);

auto spin_executor17 = [&executor17]()
{
	executor17.spin();
};
threads.emplace_back(std::thread(spin_executor17));

//EXECUTOR 18

rclcpp::executors::SingleThreadedExecutor executor18;
auto node18 = std::make_shared<Node_isr_7_ecm>();
executor18.add_node(node18);

auto spin_executor18 = [&executor18]()
{
	executor18.spin();
};
threads.emplace_back(std::thread(spin_executor18));

//EXECUTOR 19

rclcpp::executors::SingleThreadedExecutor executor19;
auto node19 = std::make_shared<Node_isr_8_ecm>();
executor19.add_node(node19);

auto spin_executor19 = [&executor19]()
{
	executor19.spin();
};
threads.emplace_back(std::thread(spin_executor19));

//EXECUTOR 20

rclcpp::executors::SingleThreadedExecutor executor20;
auto node20 = std::make_shared<Node_isr_9_ecm>();
executor20.add_node(node20);

auto spin_executor20 = [&executor20]()
{
	executor20.spin();
};
threads.emplace_back(std::thread(spin_executor20));

//EXECUTOR 21

rclcpp::executors::SingleThreadedExecutor executor21;
auto node21 = std::make_shared<Node_output_acc>();
executor21.add_node(node21);

auto spin_executor21 = [&executor21]()
{
	executor21.spin();
};
threads.emplace_back(std::thread(spin_executor21));

//EXECUTOR 22

rclcpp::executors::SingleThreadedExecutor executor22;
auto node22 = std::make_shared<Node_output_tsr>();
executor22.add_node(node22);

auto spin_executor22 = [&executor22]()
{
	executor22.spin();
};
threads.emplace_back(std::thread(spin_executor22));

//EXECUTOR 23

rclcpp::executors::SingleThreadedExecutor executor23;
auto node23 = std::make_shared<Node_perception_acc>();
executor23.add_node(node23);

auto spin_executor23 = [&executor23]()
{
	executor23.spin();
};
threads.emplace_back(std::thread(spin_executor23));

//EXECUTOR 24

rclcpp::executors::SingleThreadedExecutor executor24;
auto node24 = std::make_shared<Node_prediction_pcc>();
executor24.add_node(node24);

auto spin_executor24 = [&executor24]()
{
	executor24.spin();
};
threads.emplace_back(std::thread(spin_executor24));

//EXECUTOR 25

rclcpp::executors::SingleThreadedExecutor executor25;
auto node25 = std::make_shared<Node_preprocessing_acc>();
executor25.add_node(node25);

auto spin_executor25 = [&executor25]()
{
	executor25.spin();
};
threads.emplace_back(std::thread(spin_executor25));

//EXECUTOR 26

rclcpp::executors::SingleThreadedExecutor executor26;
auto node26 = std::make_shared<Node_resizing_tsr>();
executor26.add_node(node26);

auto spin_executor26 = [&executor26]()
{
	executor26.spin();
};
threads.emplace_back(std::thread(spin_executor26));

//EXECUTOR 27

rclcpp::executors::SingleThreadedExecutor executor27;
auto node27 = std::make_shared<Node_segmentation_to_background_tsr>();
executor27.add_node(node27);

auto spin_executor27 = [&executor27]()
{
	executor27.spin();
};
threads.emplace_back(std::thread(spin_executor27));

//EXECUTOR 28

rclcpp::executors::SingleThreadedExecutor executor28;
auto node28 = std::make_shared<Node_segmentation_tsr>();
executor28.add_node(node28);

auto spin_executor28 = [&executor28]()
{
	executor28.spin();
};
threads.emplace_back(std::thread(spin_executor28));

//EXECUTOR 29

rclcpp::executors::SingleThreadedExecutor executor29;
auto node29 = std::make_shared<Node_task_1000ms_ecm>();
executor29.add_node(node29);

auto spin_executor29 = [&executor29]()
{
	executor29.spin();
};
threads.emplace_back(std::thread(spin_executor29));

//EXECUTOR 30

rclcpp::executors::SingleThreadedExecutor executor30;
auto node30 = std::make_shared<Node_task_100ms_ecm>();
executor30.add_node(node30);

auto spin_executor30 = [&executor30]()
{
	executor30.spin();
};
threads.emplace_back(std::thread(spin_executor30));

//EXECUTOR 31

rclcpp::executors::SingleThreadedExecutor executor31;
auto node31 = std::make_shared<Node_task_10ms_ecm>();
executor31.add_node(node31);

auto spin_executor31 = [&executor31]()
{
	executor31.spin();
};
threads.emplace_back(std::thread(spin_executor31));

//EXECUTOR 32

rclcpp::executors::SingleThreadedExecutor executor32;
auto node32 = std::make_shared<Node_task_1ms_ecm>();
executor32.add_node(node32);

auto spin_executor32 = [&executor32]()
{
	executor32.spin();
};
threads.emplace_back(std::thread(spin_executor32));

//EXECUTOR 33

rclcpp::executors::SingleThreadedExecutor executor33;
auto node33 = std::make_shared<Node_task_200ms_ecm>();
executor33.add_node(node33);

auto spin_executor33 = [&executor33]()
{
	executor33.spin();
};
threads.emplace_back(std::thread(spin_executor33));

//EXECUTOR 34

rclcpp::executors::SingleThreadedExecutor executor34;
auto node34 = std::make_shared<Node_task_20ms_ecm>();
executor34.add_node(node34);

auto spin_executor34 = [&executor34]()
{
	executor34.spin();
};
threads.emplace_back(std::thread(spin_executor34));

//EXECUTOR 35

rclcpp::executors::SingleThreadedExecutor executor35;
auto node35 = std::make_shared<Node_task_2ms_ecm>();
executor35.add_node(node35);

auto spin_executor35 = [&executor35]()
{
	executor35.spin();
};
threads.emplace_back(std::thread(spin_executor35));

//EXECUTOR 36

rclcpp::executors::SingleThreadedExecutor executor36;
auto node36 = std::make_shared<Node_task_50ms_ecm>();
executor36.add_node(node36);

auto spin_executor36 = [&executor36]()
{
	executor36.spin();
};
threads.emplace_back(std::thread(spin_executor36));

//EXECUTOR 37

rclcpp::executors::SingleThreadedExecutor executor37;
auto node37 = std::make_shared<Node_task_5ms_ecm>();
executor37.add_node(node37);

auto spin_executor37 = [&executor37]()
{
	executor37.spin();
};
threads.emplace_back(std::thread(spin_executor37));

//EXECUTOR 38

rclcpp::executors::SingleThreadedExecutor executor38;
auto node38 = std::make_shared<Node_trajectory_optimizer_pcc>();
executor38.add_node(node38);

auto spin_executor38 = [&executor38]()
{
	executor38.spin();
};
threads.emplace_back(std::thread(spin_executor38));

//EXECUTOR 39

rclcpp::executors::SingleThreadedExecutor executor39;
auto node39 = std::make_shared<Node_world_model_acc>();
executor39.add_node(node39);

auto spin_executor39 = [&executor39]()
{
	executor39.spin();
};
threads.emplace_back(std::thread(spin_executor39));

	//WAITING OF THREADS

	for (auto& th : threads)
		th.join();

	rclcpp::shutdown();

	return 0;
}
