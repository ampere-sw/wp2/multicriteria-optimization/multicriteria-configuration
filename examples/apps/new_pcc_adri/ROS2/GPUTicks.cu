#define RATIO 0.65
__global__ void kernel(long long nclocks){
	long long start=clock64();
	while (clock64() < start+nclocks);
}
void executeGPUTicks(long long average,long long lowerBound, long long upperBound) {
   kernel<<<1,1>>>(average * RATIO);
   cudaDeviceSynchronize();
}
