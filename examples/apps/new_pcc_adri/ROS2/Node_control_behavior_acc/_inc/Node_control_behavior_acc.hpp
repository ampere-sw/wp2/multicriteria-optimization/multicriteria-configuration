// This code is auto-generated

#include <chrono>
#include <memory>

#include "rclcpp/rclcpp.hpp"
#include "std_msgs/msg/string.hpp"

#include "runnables.hpp"

using namespace std::chrono_literals;
using std::placeholders::_1;
#define CONSOLE_ENABLED

#if defined(_OPENMP)
int check_Node_control_behavior_acc(int *original, int *replicated){ return 1;}
#endif
class Node_control_behavior_acc : public rclcpp::Node
{
	private:
		rclcpp::TimerBase::SharedPtr timer_periodic_20ms_;

	public:
		Node_control_behavior_acc()
		: Node("node_control_behavior_acc")
		{
			timer_periodic_20ms_ = this->create_wall_timer(
					20ms, std::bind(&Node_control_behavior_acc::timer_periodic_20ms_callback, this));
		}
	void timer_periodic_20ms_callback() {
	#ifdef CONSOLE_ENABLED
		std::cout << "Timer_periodic_20ms_callback (20ms)" << std::endl;
	#endif
		run_run80();
		run_run81();
		run_run82();
		run_run83();
		run_run84();
		run_run85();
		run_run86();
		run_run87();
		run_run88();
		run_run89();
		run_run90();
		run_run91();
		run_run92();
		run_run93();
		run_run94();
		run_run95();
		run_run96();
		run_run97();
		run_run98();
		run_run80_div();
		run_run81_div();
		run_run82_div();
		run_run83_div();
		run_run84_div();
		run_run85_div();
		run_run86_div();
		run_run87_div();
		run_run88_div();
		run_run89_div();
		run_run90_div();
		run_run91_div();
		run_run92_div();
		run_run93_div();
		run_run94_div();
		run_run95_div();
		run_run96_div();
		run_run97_div();
		run_run98_div();
	}
	
	
};
