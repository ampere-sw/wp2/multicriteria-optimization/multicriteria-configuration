// This code is auto-generated

#include <chrono>
#include <memory>

#include "rclcpp/rclcpp.hpp"
#include "std_msgs/msg/string.hpp"

#include "runnables.hpp"

using namespace std::chrono_literals;
using std::placeholders::_1;
#define CONSOLE_ENABLED

#if defined(_OPENMP)
int check_Node_output_acc(int *original, int *replicated){ return 1;}
#endif
class Node_output_acc : public rclcpp::Node
{
	private:
		rclcpp::TimerBase::SharedPtr timer_5mstrigger_;

	public:
		Node_output_acc()
		: Node("node_output_acc")
		{
			timer_5mstrigger_ = this->create_wall_timer(
					5ms, std::bind(&Node_output_acc::timer_5mstrigger_callback, this));
		}
	void timer_5mstrigger_callback() {
	#ifdef CONSOLE_ENABLED
		std::cout << "Timer_5mstrigger_callback (5ms)" << std::endl;
	#endif
		run_run99();
		run_run100();
		run_run101();
	}
	
	
};
