// This code is auto-generated

#include <chrono>
#include <memory>

#include "rclcpp/rclcpp.hpp"
#include "std_msgs/msg/string.hpp"

#include "runnables.hpp"

using namespace std::chrono_literals;
using std::placeholders::_1;
#define CONSOLE_ENABLED

#if defined(_OPENMP)
int check_Node_isr_7_ecm(int *original, int *replicated){ return 1;}
#endif
class Node_isr_7_ecm : public rclcpp::Node
{
	private:

	public:
		Node_isr_7_ecm()
		: Node("node_isr_7_ecm")
		{
		}
	void timer_sporadic_4900us_5050us_isr_7_callback() {
	#ifdef CONSOLE_ENABLED
		std::cout << "Timer_sporadic_4900us_5050us_isr_7_callback (4900us)" << std::endl;
	#endif
		run_runnable_sporadic_4900us_5050us_0();
		run_runnable_sporadic_4900us_5050us_1();
		run_runnable_sporadic_4900us_5050us_2();
		run_runnable_sporadic_4900us_5050us_3();
		run_runnable_sporadic_4900us_5050us_4();
	}
	
	
};
