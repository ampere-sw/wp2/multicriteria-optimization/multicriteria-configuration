// This code is auto-generated

#include <chrono>
#include <memory>

#include "rclcpp/rclcpp.hpp"
#include "std_msgs/msg/string.hpp"

#include "runnables.hpp"

using namespace std::chrono_literals;
using std::placeholders::_1;
#define CONSOLE_ENABLED

#if defined(_OPENMP)
int check_Node_isr_8_ecm(int *original, int *replicated){ return 1;}
#endif
class Node_isr_8_ecm : public rclcpp::Node
{
	private:

	public:
		Node_isr_8_ecm()
		: Node("node_isr_8_ecm")
		{
		}
	void timer_sporadic_1700us_1800us_isr_8_callback() {
	#ifdef CONSOLE_ENABLED
		std::cout << "Timer_sporadic_1700us_1800us_isr_8_callback (1700us)" << std::endl;
	#endif
		run_runnable_sporadic_1700us_1800us_0();
		run_runnable_sporadic_1700us_1800us_1();
		run_runnable_sporadic_1700us_1800us_2();
		run_runnable_sporadic_1700us_1800us_3();
		run_runnable_sporadic_1700us_1800us_4();
		run_runnable_sporadic_1700us_1800us_5();
		run_runnable_sporadic_1700us_1800us_6();
	}
	
	
};
