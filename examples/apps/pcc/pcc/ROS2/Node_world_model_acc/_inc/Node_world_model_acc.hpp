// This code is auto-generated

#include <chrono>
#include <memory>

#include "rclcpp/rclcpp.hpp"
#include "std_msgs/msg/string.hpp"

#include "runnables.hpp"
#include "trigger_world_model_acc_service/srv/trigger_world_model_acc_service.hpp"

using namespace std::chrono_literals;
using std::placeholders::_1;
#define CONSOLE_ENABLED

#if defined(_OPENMP)
int check_Node_world_model_acc(int *original, int *replicated){ return 1;}
#endif
class Node_world_model_acc : public rclcpp::Node
{
	private:
		rclcpp::Service<trigger_world_model_acc_service::srv::TriggerWorldModelAccService>::SharedPtr trigger_world_model_acc_service;

	public:
		Node_world_model_acc()
		: Node("node_world_model_acc")
		{
			trigger_world_model_acc_service = this->create_service<trigger_world_model_acc_service::srv::TriggerWorldModelAccService>(
				"trigger_world_model_acc_service", 
				std::bind(&Node_world_model_acc::trigger_world_model_acc_service_callback, this, std::placeholders::_1, std::placeholders::_2));
		}
	
	void trigger_world_model_acc_service_callback(const std::shared_ptr<trigger_world_model_acc_service::srv::TriggerWorldModelAccService::Request> request,
		std::shared_ptr<trigger_world_model_acc_service::srv::TriggerWorldModelAccService::Response> response) {
			(void)request;
			(void)response;
	#ifdef CONSOLE_ENABLED
		std::cout << "Starting trigger_world_model_acc_service_callback" << std::endl;
	#endif
		extern int label201[1];
		extern int label422[1];
		extern int label340[2];
		extern int label271[2];
		extern int label160[1];
		extern int label409[2];
		extern int label294[2];
		extern int label147[1];
		extern int label229[1];
		extern int label150[1];
		extern int label316[1];
		extern int label309[2];
		extern int label272[1];
		extern int label413[2];
		extern int label282[2];
		extern int label61[1];
		extern int label298[1];
		extern int label400[1];
		extern int label107[301];
		extern int label295[2];
		extern int label168[1];
		extern int label372[2];
		extern int label336[1];
		extern int label347[2];
		extern int label194[1];
		extern int label291[1];
		extern int label166[1];
		extern int label89[1];
		extern int label211[1];
		extern int label164[1];
		extern int label205[1];
		extern int label368[1];
		extern int label180[1];
		extern int label44[1];
		extern int label299[1];
		extern int label440[1];
		extern int label397[1];
		extern int label382[19];
		extern int label155[1];
		extern int label274[17];
		extern int label330[2];
		extern int label388[2];
		extern int label55[1];
		extern int label302[2];
		extern int label223[1];
		extern int label374[1];
		extern int label119[1];
		extern int label136[1];
		extern int label103[1];
		extern int label323[2];
		extern int label431[20];
		extern int label88[1];
		extern int label359[1];
		extern int label396[2];
		extern int label99[316];
		extern int label250[1];
		extern int label185[1];
		extern int label200[1];
		extern int label140[275];
		extern int label90[306];
		extern int label296[2];
		extern int label206[1];
		extern int label161[1];
		extern int label54[285];
		extern int label385[2];
		extern int label209[1];
		extern int label192[1];
		extern int label394[2];
		extern int label398[1];
		extern int label221[283];
		extern int label418[1];
		extern int label371[1];
		extern int label307[1];
		extern int label311[2];
		extern int label389[1];
		extern int label360[1];
		extern int label352[2];
		extern int label189[1];
		extern int label292[1];
		extern int label401[2];
		extern int label363[1];
		extern int label369[2];
		extern int label245[1];
		extern int label332[1];
		extern int label134[1];
		extern int label184[1];
		extern int label364[1];
		extern int label228[1];
		extern int label308[2];
		extern int label306[2];
		extern int label342[2];
		extern int label367[1];
		extern int label116[238];
		extern int label325[1];
		extern int label373[1];
		extern int label275[2];
		extern int label326[2];
		extern int label381[1];
		extern int label428[2];
		extern int label108[1];
		extern int label214[1];
		extern int label416[1];
		extern int label430[2];
		extern int label419[2];
		extern int label305[2];
		extern int label355[2];
		extern int label191[245];
		extern int label246[254];
		extern int label411[1];
		extern int label348[1];
		extern int label320[2];
		extern int label259[1];
		extern int label424[2];
		extern int label204[301];
		extern int label58[1];
		extern int label321[2];
		extern int label319[2];
		extern int label101[1];
		extern int label327[1];
		extern int label379[2];
		extern int label265[1];
		extern int label281[1];
		extern int label277[2];
		extern int label362[2];
		extern int label420[1];
		extern int label48[1];
		extern int label122[1];
		extern int label434[1];
		extern int label328[2];
		extern int label289[1];
		extern int label174[313];
		extern int label199[1];
		extern int label142[1];
		extern int label138[1];
		extern int label357[1];
		extern int label350[23];
		extern int label406[1];
		extern int label380[1];
		extern int label344[2];
		extern int label391[2];
		extern int label278[1];
		extern int label264[317];
		extern int label139[1];
		extern int label429[2];
		extern int label343[1];
		extern int label286[2];
		extern int label279[2];
		extern int label242[1];
		extern int label222[1];
		extern int label104[1];
		extern int label273[2];
		extern int label399[1];
		extern int label276[2];
		extern int label124[246];
		extern int label370[1];
		extern int label314[1];
		extern int label356[1];
		extern int label353[1];
		extern int label63[281];
		extern int label410[2];
		extern int label266[1];
		extern int label256[1];
		extern int label255[219];
		extern int label384[1];
		extern int label224[1];
		extern int label300[1];
		extern int label287[2];
		extern int label195[1];
		extern int label167[1];
		extern int label177[1];
		extern int label412[2];
		extern int label375[2];
		extern int label238[1];
		extern int label366[2];
		extern int label131[233];
		extern int label225[1];
		extern int label335[2];
		extern int label404[2];
		extern int label196[269];
		extern int label390[2];
		extern int label94[1];
		extern int label121[1];
		extern int label329[1];
		extern int label346[1];
		extern int label260[1];
		extern int label72[231];
		extern int label156[269];
		extern int label407[1];
		extern int label230[277];
		extern int label34[1];
		extern int label433[1];
		extern int label152[1];
		extern int label354[1];
		extern int label337[2];
		extern int label414[1];
		extern int label349[2];
		extern int label253[1];
		extern int label351[2];
		extern int label358[1];
		extern int label69[1];
		extern int label439[1];
		extern int label432[2];
		extern int label392[16];
		extern int label312[1];
		extern int label341[1];
		extern int label47[1];
		extern int label304[2];
		extern int label93[1];
		extern int label345[2];
		extern int label331[2];
		extern int label402[1];
		extern int label322[1];
		extern int label315[1];
		extern int label284[2];
		extern int label113[1];
		extern int label283[1];
		extern int label181[1];
		extern int label426[18];
		extern int label436[19];
		extern int label212[279];
		extern int label237[274];
		extern int label66[1];
		extern int label423[2];
		extern int label421[1];
		extern int label115[1];
		extern int label438[1];
		extern int label268[2];
		extern int label310[1];
		extern int label293[1];
		extern int label376[2];
		extern int label208[1];
		extern int label318[2];
		extern int label425[1];
		extern int label144[1];
		extern int label173[1];
		extern int label383[1];
		extern int label415[2];
		extern int label148[270];
		extern int label338[2];
		extern int label50[1];
		extern int label178[1];
		extern int label378[1];
		extern int label81[308];
		extern int label183[229];
		extern int label159[1];
		extern int label377[1];
		extern int label405[1];
		extern int label324[2];
		extern int label217[1];
		extern int label36[1];
		extern int label227[1];
		extern int label262[1];
		extern int label117[1];
		extern int label49[297];
		extern int label313[1];
		extern int label40[312];
		extern int label361[2];
		extern int label297[2];
		extern int label290[2];
		extern int label303[1];
		extern int label427[1];
		extern int label65[1];
		extern int label386[2];
		extern int label165[271];
		extern int label441[19];
		extern int label239[1];
		extern int label317[1];
		extern int label78[1];
		extern int label334[2];
		extern int label267[2];
		extern int label417[2];
		extern int label435[2];
		extern int label280[1];
		extern int label408[2];
		extern int label393[2];
		extern int label288[1];
		extern int label285[1];
		extern int label190[1];
		extern int label301[1];
		extern int label269[20];
		extern int label365[2];
		extern int label333[1];
		extern int label213[1];
		extern int label387[1];
		extern int label339[1];
		extern int label437[2];
		extern int label403[2];
		extern int label395[2];
		extern int label270[2];
		extern int label127[1];
		int dummy;
		
		 #if defined(_OPENMP)
		 #pragma omp taskgraph tdg_type(static) nowait
		#endif
		{
		#if defined(_OPENMP)
		 #pragma omp task
		#elif defined(_OMPSS)
		 #pragma oss task
		#endif
		run_run42();
		
		#if defined(_OPENMP)
		 #pragma omp task
		#elif defined(_OMPSS)
		 #pragma oss task
		#endif
		run_run43();
		
		#if defined(_OPENMP)
		 #pragma omp task
		#elif defined(_OMPSS)
		 #pragma oss task
		#endif
		run_run44();
		
		#if defined(_OPENMP)
		 #pragma omp task
		#elif defined(_OMPSS)
		 #pragma oss task
		#endif
		run_run45();
		
		#if defined(_OPENMP)
		 #pragma omp task
		#elif defined(_OMPSS)
		 #pragma oss task
		#endif
		run_run46();
		
		#if defined(_OPENMP)
		 #pragma omp task
		#elif defined(_OMPSS)
		 #pragma oss task
		#endif
		run_run47();
		
		#if defined(_OPENMP)
		 #pragma omp task
		#elif defined(_OMPSS)
		 #pragma oss task
		#endif
		run_run48();
		
		#if defined(_OPENMP)
		 #pragma omp task
		#elif defined(_OMPSS)
		 #pragma oss task
		#endif
		run_run49();
		
		#if defined(_OPENMP)
		 #pragma omp task
		#elif defined(_OMPSS)
		 #pragma oss task
		#endif
		run_run50();
		
		#if defined(_OPENMP)
		 #pragma omp task
		#elif defined(_OMPSS)
		 #pragma oss task
		#endif
		run_run51();
		
		#if defined(_OPENMP)
		 #pragma omp task
		#elif defined(_OMPSS)
		 #pragma oss task
		#endif
		run_run52();
		
		#if defined(_OPENMP)
		 #pragma omp task
		#elif defined(_OMPSS)
		 #pragma oss task
		#endif
		run_run53();
		
		#if defined(_OPENMP)
		 #pragma omp task
		#elif defined(_OMPSS)
		 #pragma oss task
		#endif
		run_run54();
		
		#if defined(_OPENMP)
		 #pragma omp task
		#elif defined(_OMPSS)
		 #pragma oss task
		#endif
		run_run55();
		
		#if defined(_OPENMP)
		 #pragma omp task
		#elif defined(_OMPSS)
		 #pragma oss task
		#endif
		run_run56();
		
		#if defined(_OPENMP)
		 #pragma omp task
		#elif defined(_OMPSS)
		 #pragma oss task
		#endif
		run_run57();
		
		#if defined(_OPENMP)
		 #pragma omp task
		#elif defined(_OMPSS)
		 #pragma oss task
		#endif
		run_run58();
		
		#if defined(_OPENMP)
		 #pragma omp task
		#elif defined(_OMPSS)
		 #pragma oss task
		#endif
		run_run59();
		
		#if defined(_OPENMP)
		 #pragma omp task
		#elif defined(_OMPSS)
		 #pragma oss task
		#endif
		run_run60();
		
		#if defined(_OPENMP)
		 #pragma omp task
		#elif defined(_OMPSS)
		 #pragma oss task
		#endif
		run_run61();
		
		#if defined(_OPENMP)
		 #pragma omp task
		#elif defined(_OMPSS)
		 #pragma oss task
		#endif
		run_run62();
		
		#if defined(_OPENMP)
		 #pragma omp task
		#elif defined(_OMPSS)
		 #pragma oss task
		#endif
		run_run63();
		
		#if defined(_OPENMP)
		 #pragma omp task
		#elif defined(_OMPSS)
		 #pragma oss task
		#endif
		run_run64();
		
		#if defined(_OPENMP)
		 #pragma omp task
		#elif defined(_OMPSS)
		 #pragma oss task
		#endif
		run_run65();
		
		#if defined(_OPENMP)
		 #pragma omp task
		#elif defined(_OMPSS)
		 #pragma oss task
		#endif
		run_run66();
		
		#if defined(_OPENMP)
		 #pragma omp task
		#elif defined(_OMPSS)
		 #pragma oss task
		#endif
		run_run67();
		
		#if defined(_OPENMP)
		 #pragma omp task
		#elif defined(_OMPSS)
		 #pragma oss task
		#endif
		run_run68();
		
		#if defined(_OPENMP)
		 #pragma omp task
		#elif defined(_OMPSS)
		 #pragma oss task
		#endif
		run_run69();
		
		#if defined(_OPENMP)
		 #pragma omp task
		#elif defined(_OMPSS)
		 #pragma oss task
		#endif
		run_run70();
		
		#if defined(_OPENMP)
		 #pragma omp task
		#elif defined(_OMPSS)
		 #pragma oss task
		#endif
		run_run71();
		
		#if defined(_OPENMP)
		 #pragma omp task
		#elif defined(_OMPSS)
		 #pragma oss task
		#endif
		run_run72();
		
		#if defined(_OPENMP)
		 #pragma omp task
		#elif defined(_OMPSS)
		 #pragma oss task
		#endif
		run_run73();
		
		#if defined(_OPENMP)
		 #pragma omp task
		#elif defined(_OMPSS)
		 #pragma oss task
		#endif
		run_run74();
		
		#if defined(_OPENMP)
		 #pragma omp task
		#elif defined(_OMPSS)
		 #pragma oss task
		#endif
		run_run75();
		
		#if defined(_OPENMP)
		 #pragma omp task
		#elif defined(_OMPSS)
		 #pragma oss task
		#endif
		run_run76();
		
		#if defined(_OPENMP)
		 #pragma omp task
		#elif defined(_OMPSS)
		 #pragma oss task
		#endif
		run_run77();
		
		#if defined(_OPENMP)
		 #pragma omp task
		#elif defined(_OMPSS)
		 #pragma oss task
		#endif
		run_run78();
		
		#if defined(_OPENMP)
		 #pragma omp task
		#elif defined(_OMPSS)
		 #pragma oss task
		#endif
		run_run79();
		}
		#pragma omp taskwait;
	}
	
};
