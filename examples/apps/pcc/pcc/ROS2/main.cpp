#include "Node_classification_tsr.hpp"
#include "Node_cominput_tsr.hpp"
#include "Node_control_behavior_acc.hpp"
#include "Node_databroker_acc.hpp"
#include "Node_databroker_ecm.hpp"
#include "Node_databroker_pcc.hpp"
#include "Node_detection_tsr.hpp"
#include "Node_gaussian_filter_tsr.hpp"
#include "Node_input_acc.hpp"
#include "Node_isr_10_ecm.hpp"
#include "Node_isr_11_ecm.hpp"
#include "Node_isr_1_ecm.hpp"
#include "Node_isr_2_ecm.hpp"
#include "Node_isr_3_ecm.hpp"
#include "Node_isr_4_ecm.hpp"
#include "Node_isr_5_ecm.hpp"
#include "Node_isr_6_ecm.hpp"
#include "Node_isr_7_ecm.hpp"
#include "Node_isr_8_ecm.hpp"
#include "Node_isr_9_ecm.hpp"
#include "Node_output_acc.hpp"
#include "Node_output_tsr.hpp"
#include "Node_perception_acc.hpp"
#include "Node_prediction_pcc.hpp"
#include "Node_preprocessing_acc.hpp"
#include "Node_resizing_tsr.hpp"
#include "Node_segmentation_to_background_tsr.hpp"
#include "Node_segmentation_tsr.hpp"
#include "Node_task_1000ms_ecm.hpp"
#include "Node_task_100ms_ecm.hpp"
#include "Node_task_10ms_ecm.hpp"
#include "Node_task_1ms_ecm.hpp"
#include "Node_task_200ms_ecm.hpp"
#include "Node_task_20ms_ecm.hpp"
#include "Node_task_2ms_ecm.hpp"
#include "Node_task_50ms_ecm.hpp"
#include "Node_task_5ms_ecm.hpp"
#include "Node_trajectory_optimizer_pcc.hpp"
#include "Node_world_model_acc.hpp"
#include <extrae.h>

int main(int argc, char *argv[])
{
	std::vector<std::thread> threads;
	Extrae_init();

	setvbuf(stdout, NULL, _IONBF, BUFSIZ);
	rclcpp::init(argc, argv);

//  ---------------------------------
//  PCC - ARM
// Deadline: "Req Deadline EC_PCC_Component" 200ms
// Chain: EffectChainPCC_Component
//  - pcc_input: Task databrocker_pcc,
//  - pcc_prediction: Task prediction_pcc,
//  - pcc_trajectory_optimizer: pcc_trajectory_optimizer_pcc
//EXECUTOR 1
	auto databrocker = new Node_databroker_pcc();
	auto prediction = new Node_prediction_pcc();
	auto trajectory_optimizer = new Node_trajectory_optimizer_pcc();


	rclcpp::shutdown();

	return 0;
}
