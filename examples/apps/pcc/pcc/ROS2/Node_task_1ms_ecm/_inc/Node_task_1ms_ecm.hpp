// This code is auto-generated

#include <chrono>
#include <memory>

#include "rclcpp/rclcpp.hpp"
#include "std_msgs/msg/string.hpp"

#include "runnables.hpp"

using namespace std::chrono_literals;
using std::placeholders::_1;
#define CONSOLE_ENABLED

#if defined(_OPENMP)
int check_Node_task_1ms_ecm(int *original, int *replicated){ return 1;}
#endif
class Node_task_1ms_ecm : public rclcpp::Node
{
	private:
		rclcpp::TimerBase::SharedPtr timer_periodic_1ms_;

	public:
		Node_task_1ms_ecm()
		: Node("node_task_1ms_ecm")
		{
			timer_periodic_1ms_ = this->create_wall_timer(
					1ms, std::bind(&Node_task_1ms_ecm::timer_periodic_1ms_callback, this));
		}
	void timer_periodic_1ms_callback() {
	#ifdef CONSOLE_ENABLED
		std::cout << "Timer_periodic_1ms_callback (1ms)" << std::endl;
	#endif
		run_runnable_1ms_0();
		run_runnable_1ms_1();
		run_runnable_1ms_2();
		run_runnable_1ms_3();
		run_runnable_1ms_4();
		run_runnable_1ms_5();
		run_runnable_1ms_6();
		run_runnable_1ms_7();
		run_runnable_1ms_8();
		run_runnable_1ms_9();
		run_runnable_1ms_10();
		run_runnable_1ms_11();
		run_runnable_1ms_12();
		run_runnable_1ms_13();
		run_runnable_1ms_14();
		run_runnable_1ms_15();
		run_runnable_1ms_16();
		run_runnable_1ms_17();
		run_runnable_1ms_18();
		run_runnable_1ms_19();
		run_runnable_1ms_20();
		run_runnable_1ms_21();
		run_runnable_1ms_22();
		run_runnable_1ms_23();
		run_runnable_1ms_24();
		run_runnable_1ms_25();
		run_runnable_1ms_26();
		run_runnable_1ms_27();
		run_runnable_1ms_28();
		run_runnable_1ms_29();
		run_runnable_1ms_30();
		run_runnable_1ms_31();
		run_runnable_1ms_32();
		run_runnable_1ms_33();
		run_runnable_1ms_34();
		run_runnable_1ms_35();
		run_runnable_1ms_36();
		run_runnable_1ms_37();
		run_runnable_1ms_38();
		run_runnable_1ms_39();
		run_runnable_1ms_40();
		run_runnable_1ms_41();
	}
	
	
};
