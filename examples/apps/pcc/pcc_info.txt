ACC - FPGA
Deadline: "Req Deadline EC_ACC" 200ms
Chain: EffectChainAcc
 - acc_input : periodic 5ms
 - acc_preprocessing :  periodic 66ms
 - acc_perception : stimulus "trigger_perception_acc"
 - acc_world_model : stimulus "trigger_world_model_acc"
 - acc_control : periodic 20ms
 - acc_output : periodic 5ms
 
 input_acc
  - run 1 and run 2
 preprocessing_acc
  - run3 -> run12
  - spec in all
  
 perception_acc
  - run 13 -> 41
  - spec in all
 
 world_model_acc
  - run 42 -> 79
  - spec in all
  
 control_behavior_acc
  - run 80 -> 98
  - run 80_div -> 98_div ?
  - acc_databroker
 
 output_acc
  - run 99 -> 101
  
 ---------------------------------
 TSR - GPU
Deadline: "Req Deadline EC_TSR" 200ms
Chain: EffectChainTsr
 - tsr_input : periodic 5ms
 - tsr_resizing :  periodic 66ms
 - tsr_gaussian : stimulus "trigger_perception_acc"
 - tsr_segmentation : stimulus "trigger_world_model_acc"
 - tsr_segmentation_back : periodic 20ms
 - tsr_detect : periodic 5ms
 - tsr_classification : periodic 5ms
 - tsr_output : periodic 5ms
 
 ---------------------------------
 PCC - ARM
Deadline: "Req Deadline EC_PCC_Component" 200ms
Chain: EffectChainPCC_Component
 - pcc_input: Task databrocker_pcc,
 - pcc_prediction: Task prediction_pcc,
 - pcc_trajectory_optimizer: pcc_trajectory_optimizer_pcc












 ---------------------------------
 ECM - ARM
 