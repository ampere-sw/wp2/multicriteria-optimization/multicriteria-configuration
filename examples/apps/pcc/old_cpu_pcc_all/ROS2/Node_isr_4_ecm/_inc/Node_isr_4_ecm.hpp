// This code is auto-generated

#include <chrono>
#include <memory>

#include "rclcpp/rclcpp.hpp"
#include "std_msgs/msg/string.hpp"

#include "runnables.hpp"

using namespace std::chrono_literals;
using std::placeholders::_1;
#define CONSOLE_ENABLED

#if defined(_OPENMP)
int check_Node_isr_4_ecm(int *original, int *replicated){ return 1;}
#endif
class Node_isr_4_ecm : public rclcpp::Node
{
	private:

	public:
		Node_isr_4_ecm()
		: Node("node_isr_4_ecm")
		{
		}
	void timer_sporadic_1500us_1700us_isr_4_callback() {
	#ifdef CONSOLE_ENABLED
		std::cout << "Timer_sporadic_1500us_1700us_isr_4_callback (1500us)" << std::endl;
	#endif
		run_runnable_sporadic_1500us_1700us_0();
		run_runnable_sporadic_1500us_1700us_1();
		run_runnable_sporadic_1500us_1700us_2();
		run_runnable_sporadic_1500us_1700us_3();
		run_runnable_sporadic_1500us_1700us_4();
		run_runnable_sporadic_1500us_1700us_5();
		run_runnable_sporadic_1500us_1700us_6();
		run_runnable_sporadic_1500us_1700us_7();
	}
	
	
};
