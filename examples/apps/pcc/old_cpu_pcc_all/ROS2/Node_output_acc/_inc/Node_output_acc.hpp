// This code is auto-generated

#include <chrono>
#include <memory>

#include "rclcpp/rclcpp.hpp"
#include "std_msgs/msg/string.hpp"

#include "runnables.hpp"

using namespace std::chrono_literals;
using std::placeholders::_1;
#define CONSOLE_ENABLED

#if defined(_OPENMP)
int check_Node_output_acc(int *original, int *replicated){ return 1;}
#endif
class Node_output_acc : public rclcpp::Node
{
	private:
		rclcpp::TimerBase::SharedPtr timer_5mstrigger_;
		rclcpp::Publisher<std_msgs::msg::String>::SharedPtr torqueDemand_publisher;
		rclcpp::Publisher<std_msgs::msg::String>::SharedPtr speed_publisher;
		rclcpp::Publisher<std_msgs::msg::String>::SharedPtr userspeedsetpoint_publisher;

	public:
		Node_output_acc()
		: Node("node_output_acc")
		{
			timer_5mstrigger_ = this->create_wall_timer(
					5ms, std::bind(&Node_output_acc::timer_5mstrigger_callback, this));
			torqueDemand_publisher = this->create_publisher<std_msgs::msg::String>("torquedemand", 10);
			speed_publisher = this->create_publisher<std_msgs::msg::String>("speed", 10);
			userspeedsetpoint_publisher = this->create_publisher<std_msgs::msg::String>("userspeedsetpoint", 10);
		}
	void timer_5mstrigger_callback() {
	#ifdef CONSOLE_ENABLED
		std::cout << "Timer_5mstrigger_callback (5ms)" << std::endl;
	#endif
		run_run99(torqueDemand_publisher, speed_publisher, userspeedsetpoint_publisher);
		run_run100();
		run_run101();
	}
	
	
};
