// This code is auto-generated

#include <chrono>
#include <memory>

#include "rclcpp/rclcpp.hpp"
#include "std_msgs/msg/string.hpp"

#include "runnables.hpp"

using namespace std::chrono_literals;
using std::placeholders::_1;
#define CONSOLE_ENABLED

#if defined(_OPENMP)
int check_Node_databroker_pcc(int *original, int *replicated){ return 1;}
#endif
class Node_databroker_pcc : public rclcpp::Node
{
	private:
		rclcpp::Subscription<std_msgs::msg::String>::SharedPtr recognizedspeedlimit_subscription_;
		rclcpp::Subscription<std_msgs::msg::String>::SharedPtr userspeedsetpoint_subscription_;
		rclcpp::Subscription<std_msgs::msg::String>::SharedPtr speed_subscription_;

	public:
		Node_databroker_pcc()
		: Node("node_databroker_pcc")
		{
			recognizedspeedlimit_subscription_ = this->create_subscription<std_msgs::msg::String>(
				"recognizedspeedlimit", rclcpp::QoS(10), std::bind(&Node_databroker_pcc::recognizedspeedlimit_subscription_callback, this, _1));
			userspeedsetpoint_subscription_ = this->create_subscription<std_msgs::msg::String>(
				"userspeedsetpoint", rclcpp::QoS(10), std::bind(&Node_databroker_pcc::userspeedsetpoint_subscription_callback, this, _1));
			speed_subscription_ = this->create_subscription<std_msgs::msg::String>(
				"speed", rclcpp::QoS(10), std::bind(&Node_databroker_pcc::speed_subscription_callback, this, _1));
		}
	void recognizedspeedlimit_subscription_callback(const std_msgs::msg::String::SharedPtr msg) {
	#ifdef CONSOLE_ENABLED
		std::cout << "ROS2: subscription callback " << msg->data << std::endl ;
	#endif
		run_pcc_databroker();
	}
	
	void userspeedsetpoint_subscription_callback(const std_msgs::msg::String::SharedPtr msg) {
	#ifdef CONSOLE_ENABLED
		std::cout << "ROS2: subscription callback " << msg->data << std::endl ;
	#endif
		run_pcc_databroker();
	}
	
	void speed_subscription_callback(const std_msgs::msg::String::SharedPtr msg) {
	#ifdef CONSOLE_ENABLED
		std::cout << "ROS2: subscription callback " << msg->data << std::endl ;
	#endif
		run_pcc_databroker();
	}
	
	
	
	
};
