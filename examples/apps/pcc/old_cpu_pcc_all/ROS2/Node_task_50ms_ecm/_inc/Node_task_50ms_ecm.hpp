// This code is auto-generated

#include <chrono>
#include <memory>

#include "rclcpp/rclcpp.hpp"
#include "std_msgs/msg/string.hpp"

#include "runnables.hpp"

using namespace std::chrono_literals;
using std::placeholders::_1;
#define CONSOLE_ENABLED

#if defined(_OPENMP)
int check_Node_task_50ms_ecm(int *original, int *replicated){ return 1;}
#endif
class Node_task_50ms_ecm : public rclcpp::Node
{
	private:
		rclcpp::TimerBase::SharedPtr timer_periodic_50ms_;

	public:
		Node_task_50ms_ecm()
		: Node("node_task_50ms_ecm")
		{
			timer_periodic_50ms_ = this->create_wall_timer(
					50ms, std::bind(&Node_task_50ms_ecm::timer_periodic_50ms_callback, this));
		}
	void timer_periodic_50ms_callback() {
	#ifdef CONSOLE_ENABLED
		std::cout << "Timer_periodic_50ms_callback (50ms)" << std::endl;
	#endif
		run_runnable_50ms_0();
		run_runnable_50ms_1();
		run_runnable_50ms_2();
		run_runnable_50ms_3();
		run_runnable_50ms_4();
		run_runnable_50ms_5();
		run_runnable_50ms_6();
		run_runnable_50ms_7();
		run_runnable_50ms_8();
		run_runnable_50ms_9();
		run_runnable_50ms_10();
		run_runnable_50ms_11();
		run_runnable_50ms_12();
		run_runnable_50ms_13();
		run_runnable_50ms_14();
		run_runnable_50ms_15();
		run_runnable_50ms_16();
		run_runnable_50ms_17();
		run_runnable_50ms_18();
		run_runnable_50ms_19();
		run_runnable_50ms_20();
		run_runnable_50ms_21();
		run_runnable_50ms_22();
		run_runnable_50ms_23();
		run_runnable_50ms_24();
		run_runnable_50ms_25();
		run_runnable_50ms_26();
		run_runnable_50ms_27();
		run_runnable_50ms_28();
		run_runnable_50ms_29();
		run_runnable_50ms_30();
		run_runnable_50ms_31();
		run_runnable_50ms_32();
		run_runnable_50ms_33();
		run_runnable_50ms_34();
		run_runnable_50ms_35();
		run_runnable_50ms_36();
		run_runnable_50ms_37();
		run_runnable_50ms_38();
		run_runnable_50ms_39();
		run_runnable_50ms_40();
		run_runnable_50ms_41();
		run_runnable_50ms_42();
		run_runnable_50ms_43();
		run_runnable_50ms_44();
		run_runnable_50ms_45();
	}
	
	
};
