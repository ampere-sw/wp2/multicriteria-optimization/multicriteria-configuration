// This code is auto-generated

#include <chrono>
#include <memory>

#include "rclcpp/rclcpp.hpp"
#include "std_msgs/msg/string.hpp"

#include "runnables.hpp"

using namespace std::chrono_literals;
using std::placeholders::_1;
#define CONSOLE_ENABLED

#if defined(_OPENMP)
int check_Node_isr_2_ecm(int *original, int *replicated){ return 1;}
#endif
class Node_isr_2_ecm : public rclcpp::Node
{
	private:

	public:
		Node_isr_2_ecm()
		: Node("node_isr_2_ecm")
		{
		}
	void timer_sporadic_9500us_10500us_isr_2_callback() {
	#ifdef CONSOLE_ENABLED
		std::cout << "Timer_sporadic_9500us_10500us_isr_2_callback (9500us)" << std::endl;
	#endif
		run_runnable_sporadic_9500us_10500us_2();
		run_runnable_sporadic_9500us_10500us_6();
	}
	
	
};
