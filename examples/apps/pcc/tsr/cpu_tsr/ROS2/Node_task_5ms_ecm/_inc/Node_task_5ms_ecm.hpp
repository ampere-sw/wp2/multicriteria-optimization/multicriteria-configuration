// This code is auto-generated

#include <chrono>
#include <memory>

#include "rclcpp/rclcpp.hpp"
#include "std_msgs/msg/string.hpp"

#include "runnables.hpp"

using namespace std::chrono_literals;
using std::placeholders::_1;
#define CONSOLE_ENABLED

#if defined(_OPENMP)
int check_Node_task_5ms_ecm(int *original, int *replicated){ return 1;}
#endif
class Node_task_5ms_ecm : public rclcpp::Node
{
	private:
		rclcpp::TimerBase::SharedPtr timer_periodic_5ms_;

	public:
		Node_task_5ms_ecm()
		: Node("node_task_5ms_ecm")
		{
			timer_periodic_5ms_ = this->create_wall_timer(
					5ms, std::bind(&Node_task_5ms_ecm::timer_periodic_5ms_callback, this));
		}
	void timer_periodic_5ms_callback() {
	#ifdef CONSOLE_ENABLED
		std::cout << "Timer_periodic_5ms_callback (5ms)" << std::endl;
	#endif
		run_runnable_5ms_0();
		run_runnable_5ms_1();
		run_runnable_5ms_2();
		run_runnable_5ms_3();
		run_runnable_5ms_4();
		run_runnable_5ms_5();
		run_runnable_5ms_6();
		run_runnable_5ms_7();
		run_runnable_5ms_8();
		run_runnable_5ms_9();
		run_runnable_5ms_10();
		run_runnable_5ms_11();
		run_runnable_5ms_12();
		run_runnable_5ms_13();
		run_runnable_5ms_14();
		run_runnable_5ms_15();
		run_runnable_5ms_16();
		run_runnable_5ms_17();
		run_runnable_5ms_18();
		run_runnable_5ms_19();
		run_runnable_5ms_20();
		run_runnable_5ms_21();
		run_runnable_5ms_22();
	}
	
	
};
