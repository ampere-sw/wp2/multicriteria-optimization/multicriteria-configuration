// This code is auto-generated

#include <chrono>
#include <memory>

#include "rclcpp/rclcpp.hpp"
#include "std_msgs/msg/string.hpp"

#include "runnables.hpp"
#include "trigger_perception_acc_service/srv/trigger_perception_acc_service.hpp"

using namespace std::chrono_literals;
using std::placeholders::_1;
#define CONSOLE_ENABLED

#if defined(_OPENMP)
int check_Node_perception_acc(int *original, int *replicated){ return 1;}
#endif
class Node_perception_acc : public rclcpp::Node
{
	private:
		rclcpp::Service<trigger_perception_acc_service::srv::TriggerPerceptionAccService>::SharedPtr trigger_perception_acc_service;
		rclcpp::Client<trigger_world_model_acc_service::srv::TriggerWorldModelAccService>::SharedPtr trigger_world_model_acc_client;

	public:
		Node_perception_acc()
		: Node("node_perception_acc")
		{
			trigger_perception_acc_service = this->create_service<trigger_perception_acc_service::srv::TriggerPerceptionAccService>(
				"trigger_perception_acc_service", 
				std::bind(&Node_perception_acc::trigger_perception_acc_service_callback, this, std::placeholders::_1, std::placeholders::_2));
			trigger_world_model_acc_client =  this->create_client<trigger_world_model_acc_service::srv::TriggerWorldModelAccService>("trigger_world_model_acc_service");
		}
	
	void trigger_perception_acc_service_callback(const std::shared_ptr<trigger_perception_acc_service::srv::TriggerPerceptionAccService::Request> request,
		std::shared_ptr<trigger_perception_acc_service::srv::TriggerPerceptionAccService::Response> response) {
			(void)request;
			(void)response;
	#ifdef CONSOLE_ENABLED
		std::cout << "Starting trigger_perception_acc_service_callback" << std::endl;
	#endif
		extern int label253[1];
		extern int label239[1];
		extern int label152[1];
		extern int label69[1];
		extern int label199[1];
		extern int label173[1];
		extern int label217[1];
		extern int label159[1];
		extern int label44[1];
		extern int label117[1];
		extern int label208[1];
		extern int label213[1];
		extern int label259[1];
		extern int label201[1];
		extern int label222[1];
		extern int label262[1];
		extern int label127[1];
		extern int label160[1];
		extern int label103[1];
		extern int label48[1];
		extern int label121[1];
		extern int label227[1];
		extern int label119[1];
		extern int label181[1];
		extern int label58[1];
		extern int label54[285];
		extern int label229[1];
		extern int label195[1];
		extern int label228[1];
		extern int label40[312];
		extern int label36[1];
		extern int label131[233];
		extern int label177[1];
		extern int label115[1];
		extern int label166[1];
		extern int label164[1];
		extern int label134[1];
		extern int label108[1];
		extern int label211[1];
		extern int label89[1];
		extern int label47[1];
		extern int label136[1];
		extern int label156[269];
		extern int label138[1];
		extern int label184[1];
		extern int label55[1];
		extern int label225[1];
		extern int label101[1];
		extern int label34[1];
		extern int label238[1];
		extern int label209[1];
		extern int label142[1];
		extern int label192[1];
		extern int label260[1];
		extern int label65[1];
		extern int label190[1];
		extern int label206[1];
		extern int label94[1];
		extern int label191[245];
		extern int label124[246];
		extern int label250[1];
		int dummy;
		
		 #if defined(_OPENMP)
		 #pragma omp taskgraph tdg_type(static) nowait
		#endif
		{
		#if defined(_OPENMP)
		 #pragma omp task depend(out:label250,label94,label206,label190,label65,label260,label192,label142,label209,label238,label34,label101,label225,label184,label138,label136,label47,label89,label211,label134,label164,label166,label115,label177,label36,label228,label195,label229,label181,label119,label227,label121,label48,label160,label127,label262,label222,label201,label259,label213,label208,label117,label44,label159,label217,label173,label199,label69,label152,label239,label253)
		#elif defined(_OMPSS)
		 #pragma oss task depend(out:label250,label94,label206,label190,label65,label260,label192,label142,label209,label238,label34,label101,label225,label184,label138,label136,label47,label89,label211,label134,label164,label166,label115,label177,label36,label228,label195,label229,label181,label119,label227,label121,label48,label160,label127,label262,label222,label201,label259,label213,label208,label117,label44,label159,label217,label173,label199,label69,label152,label239,label253)
		#endif
		run_run13();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:label36,label34) depend(out:label40)
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:label36,label34) depend(out:label40)
		#endif
		run_run14();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:label44,label47) depend(out:label48)
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:label44,label47) depend(out:label48)
		#endif
		run_run15();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:label40,label48) depend(out:label54)
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:label40,label48) depend(out:label54)
		#endif
		run_run16();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(out:label55,label58)
		#elif defined(_OMPSS)
		 #pragma oss task depend(out:label55,label58)
		#endif
		run_run17();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:label65,label69)
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:label65,label69)
		#endif
		run_run18();
		
		#if defined(_OPENMP)
		 #pragma omp task
		#elif defined(_OMPSS)
		 #pragma oss task
		#endif
		run_run19();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:label89)
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:label89)
		#endif
		run_run20();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:label94)
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:label94)
		#endif
		run_run21();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:label54) depend(out:label103,label101)
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:label54) depend(out:label103,label101)
		#endif
		run_run22();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:label115) depend(out:label108)
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:label115) depend(out:label108)
		#endif
		run_run23();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:label119,label108,label117,label121) depend(out:label124)
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:label119,label108,label117,label121) depend(out:label124)
		#endif
		run_run24();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:label48,label103,label127) depend(out:label131)
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:label48,label103,label127) depend(out:label131)
		#endif
		run_run25();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:label138,label134,label136)
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:label138,label134,label136)
		#endif
		run_run26();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:label55,label142)
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:label55,label142)
		#endif
		run_run27();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:label152,label40) depend(out:label156)
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:label152,label40) depend(out:label156)
		#endif
		run_run28();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:label160,label159,label164)
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:label160,label159,label164)
		#endif
		run_run29();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:label166,label173)
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:label166,label173)
		#endif
		run_run30();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:label181,label177)
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:label181,label177)
		#endif
		run_run31();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:label184,label101,label190) depend(out:label191)
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:label184,label101,label190) depend(out:label191)
		#endif
		run_run32();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:label124,label131,label192,label156,label195,label54)
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:label124,label131,label192,label156,label195,label54)
		#endif
		run_run33();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:label201,label124,label199)
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:label201,label124,label199)
		#endif
		run_run34();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:label208,label191,label209,label206) depend(out:label211)
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:label208,label191,label209,label206) depend(out:label211)
		#endif
		run_run35();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:label213,label217)
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:label213,label217)
		#endif
		run_run36();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:label222,label225,label227,label228,label229)
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:label222,label225,label227,label228,label229)
		#endif
		run_run37();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:label211,label58)
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:label211,label58)
		#endif
		run_run38();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:label239,label238)
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:label239,label238)
		#endif
		run_run39();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:label250,label253)
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:label250,label253)
		#endif
		run_run40();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:label260,label259,label262)
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:label260,label259,label262)
		#endif
		run_run41(trigger_world_model_acc_client);
		}
		#pragma omp taskwait;
	}
	
};
