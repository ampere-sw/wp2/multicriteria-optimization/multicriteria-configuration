// This code is auto-generated

#include <chrono>
#include <memory>

#include "rclcpp/rclcpp.hpp"
#include "std_msgs/msg/string.hpp"

#include "runnables.hpp"

using namespace std::chrono_literals;
using std::placeholders::_1;
#define CONSOLE_ENABLED

#if defined(_OPENMP)
int check_Node_task_200ms_ecm(int *original, int *replicated){ return 1;}
#endif
class Node_task_200ms_ecm : public rclcpp::Node
{
	private:
		rclcpp::TimerBase::SharedPtr timer_periodic_200ms_;

	public:
		Node_task_200ms_ecm()
		: Node("node_task_200ms_ecm")
		{
			timer_periodic_200ms_ = this->create_wall_timer(
					200ms, std::bind(&Node_task_200ms_ecm::timer_periodic_200ms_callback, this));
		}
	void timer_periodic_200ms_callback() {
	#ifdef CONSOLE_ENABLED
		std::cout << "Timer_periodic_200ms_callback (200ms)" << std::endl;
	#endif
		run_runnable_200ms_0();
		run_runnable_200ms_1();
		run_runnable_200ms_2();
		run_runnable_200ms_3();
		run_runnable_200ms_4();
		run_runnable_200ms_5();
		run_runnable_200ms_6();
		run_runnable_200ms_7();
		run_runnable_200ms_8();
		run_runnable_200ms_9();
		run_runnable_200ms_10();
		run_runnable_200ms_11();
		run_runnable_200ms_12();
		run_runnable_200ms_13();
		run_runnable_200ms_14();
	}
	
	
};
