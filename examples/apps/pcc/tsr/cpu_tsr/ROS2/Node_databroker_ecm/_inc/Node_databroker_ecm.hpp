// This code is auto-generated

#include <chrono>
#include <memory>

#include "rclcpp/rclcpp.hpp"
#include "std_msgs/msg/string.hpp"

#include "runnables.hpp"

using namespace std::chrono_literals;
using std::placeholders::_1;
#define CONSOLE_ENABLED

#if defined(_OPENMP)
int check_Node_databroker_ecm(int *original, int *replicated){ return 1;}
#endif
class Node_databroker_ecm : public rclcpp::Node
{
	private:
		rclcpp::Subscription<std_msgs::msg::String>::SharedPtr operationsetpoint_subscription_;
		rclcpp::Subscription<std_msgs::msg::String>::SharedPtr tourqueDemand_subscription_;

	public:
		Node_databroker_ecm()
		: Node("node_databroker_ecm")
		{
			operationsetpoint_subscription_ = this->create_subscription<std_msgs::msg::String>(
				"operationsetpoint", rclcpp::QoS(10), std::bind(&Node_databroker_ecm::operationsetpoint_subscription_callback, this, _1));
			tourqueDemand_subscription_ = this->create_subscription<std_msgs::msg::String>(
				"tourquedemand", rclcpp::QoS(10), std::bind(&Node_databroker_ecm::tourqueDemand_subscription_callback, this, _1));
		}
	void operationsetpoint_subscription_callback(const std_msgs::msg::String::SharedPtr msg) {
	#ifdef CONSOLE_ENABLED
		std::cout << "ROS2: subscription callback " << msg->data << std::endl ;
	#endif
		run_ecm_databroker();
	}
	
	void tourqueDemand_subscription_callback(const std_msgs::msg::String::SharedPtr msg) {
	#ifdef CONSOLE_ENABLED
		std::cout << "ROS2: subscription callback " << msg->data << std::endl ;
	#endif
		run_ecm_databroker();
	}
	
	
	
};
