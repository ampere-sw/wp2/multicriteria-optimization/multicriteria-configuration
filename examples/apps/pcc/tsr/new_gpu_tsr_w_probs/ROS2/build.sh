#building service classification_service

cd services/classification_service
colcon build
. install/setup.bash
cd ../..

#building service detection_service

cd services/detection_service
colcon build
. install/setup.bash
cd ../..

#building service gausian_filter_service

cd services/gausian_filter_service
colcon build
. install/setup.bash
cd ../..

#building service segmentation_service

cd services/segmentation_service
colcon build
. install/setup.bash
cd ../..

#building service segmentation_to_bckground_service

cd services/segmentation_to_bckground_service
colcon build
. install/setup.bash
cd ../..

colcon build
. install/setup.bash
