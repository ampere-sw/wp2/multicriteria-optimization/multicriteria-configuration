// This code is auto-generated

#include <chrono>
#include <memory>

#include "rclcpp/rclcpp.hpp"
#include "std_msgs/msg/string.hpp"

#include "runnables.hpp"

using namespace std::chrono_literals;
using std::placeholders::_1;
#define CONSOLE_ENABLED

#if defined(_OPENMP)
int check_Node_cominput_tsr(int *original, int *replicated){ return 1;}
#endif
class Node_cominput_tsr : public rclcpp::Node
{
	private:
		rclcpp::TimerBase::SharedPtr timer_trigger_;

	public:
		Node_cominput_tsr()
		: Node("node_cominput_tsr")
		{
			timer_trigger_ = this->create_wall_timer(
					5ms, std::bind(&Node_cominput_tsr::timer_trigger_callback, this));
		}
	void timer_trigger_callback() {
	#ifdef CONSOLE_ENABLED
		std::cout << "Timer_trigger_callback (5ms)" << std::endl;
	#endif
		#if defined(_OPENMP)
		#pragma omp taskgraph tdg_type(static) nowait
		#endif
		{
		#if defined(_OPENMP)
		 #pragma omp task
		#elif defined(_OMPSS)
		 #pragma oss task
		#endif
		run_input();
		}
		#pragma omp taskwait;
	}
	
	
};
