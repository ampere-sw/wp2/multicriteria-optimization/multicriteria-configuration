#include "Node_cominput_tsr.hpp"
#include "Node_resizing_tsr.hpp"
#include "Node_gaussian_filter_tsr.hpp"
#include "Node_segmentation_tsr.hpp"
#include "Node_segmentation_to_background_tsr.hpp"
#include "Node_detection_tsr.hpp"
#include "Node_classification_tsr.hpp"
#include "Node_output_tsr.hpp"
#include <extrae.h>
extern "C" {
	#include <cudamperf.h>
}
int main(int argc, char *argv[])
{
	setvbuf(stdout, NULL, _IONBF, BUFSIZ);
	rclcpp::init(argc, argv);
	auto node1 = new Node_cominput_tsr();
	auto node2 = new Node_resizing_tsr();
	auto node3 = new Node_gaussian_filter_tsr();
	auto node4 = new Node_segmentation_tsr();
	auto node5 = new Node_segmentation_to_background_tsr();
	auto node6 = new Node_detection_tsr();
	auto node7 = new Node_classification_tsr();
	auto node8 = new Node_output_tsr();

	cudamperf_start();
	Extrae_init();
	int NUM_ITER = 2;

	for(int i = 0; i < NUM_ITER; i++){
		printf("IT %d\n",i);
		#pragma omp parallel 
		#pragma omp single
		node1->timer_trigger_callback();

		#pragma omp parallel 
		#pragma omp single
		node2->timer_preprocessing_callback();

		#pragma omp parallel 
		#pragma omp single
		node3->gausian_filter_service_callback(NULL,NULL);
		
		#pragma omp parallel 
		#pragma omp single
		node4->segmentation_service_callback(NULL,NULL);
		
		#pragma omp parallel 
		#pragma omp single
		node5->segmentation_to_bckground_service_callback(NULL,NULL);
		
		#pragma omp parallel 
		#pragma omp single
		node6->detection_service_callback(NULL,NULL);
			
		#pragma omp parallel 
		#pragma omp single
		node7->classification_service_callback(NULL,NULL);

		#pragma omp parallel 
		#pragma omp single
		node8->timer_output_callback();
	}

	rclcpp::shutdown();
	Extrae_fini();
	cudamperf_stop();
	return 0;
}
