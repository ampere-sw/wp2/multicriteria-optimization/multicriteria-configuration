// This code is auto-generated

#include <chrono>
#include <memory>

#include "rclcpp/rclcpp.hpp"
#include "std_msgs/msg/string.hpp"

#include "runnables.hpp"

using namespace std::chrono_literals;
using std::placeholders::_1;
#define CONSOLE_ENABLED

#if defined(_OPENMP)
int check_Node_output_acc(int *original, int *replicated){ return 1;}
#endif
class Node_output_acc : public rclcpp::Node
{
	private:
		rclcpp::TimerBase::SharedPtr timer_5mstrigger_;
		rclcpp::Publisher<std_msgs::msg::String>::SharedPtr torqueDemand_publisher;
		rclcpp::Publisher<std_msgs::msg::String>::SharedPtr speed_publisher;
		rclcpp::Publisher<std_msgs::msg::String>::SharedPtr userspeedsetpoint_publisher;

	public:
		Node_output_acc()
		: Node("node_output_acc")
		{
			timer_5mstrigger_ = this->create_wall_timer(
					5ms, std::bind(&Node_output_acc::timer_5mstrigger_callback, this));
			torqueDemand_publisher = this->create_publisher<std_msgs::msg::String>("torquedemand", 10);
			speed_publisher = this->create_publisher<std_msgs::msg::String>("speed", 10);
			userspeedsetpoint_publisher = this->create_publisher<std_msgs::msg::String>("userspeedsetpoint", 10);
		}
	void timer_5mstrigger_callback() {
	#ifdef CONSOLE_ENABLED
		std::cout << "Timer_5mstrigger_callback (5ms)" << std::endl;
	#endif
		extern int label460[1];
		extern int label469[1];
		extern int label485[1];
		extern int label480[1];
		extern int label521[1];
		extern int label484[1];
		extern int label516[1];
		extern int label449[1];
		extern int label453[1];
		extern int label530[1];
		extern int label527[1];
		extern int label472[1];
		extern int label496[1];
		extern int label515[1];
		extern int label502[1];
		extern int label501[1];
		extern int label497[1];
		extern int label477[1];
		extern int label444[1];
		extern int label510[1];
		extern int label500[1];
		extern int label464[1];
		extern int label487[1];
		extern int label528[1];
		extern int label511[1];
		extern int label474[1];
		extern int label490[1];
		extern int label525[1];
		extern int label462[1];
		extern int label509[1];
		extern int label517[1];
		extern int label446[1];
		extern int label452[1];
		extern int label492[1];
		extern int label486[1];
		extern int label465[1];
		extern int label531[1];
		extern int label529[1];
		extern int label478[1];
		extern int label482[1];
		extern int label506[1];
		extern int label450[1];
		extern int label455[1];
		extern int label466[1];
		extern int label461[1];
		extern int label526[1];
		extern int label523[1];
		int dummy;
		
		 #if defined(_OPENMP)
		 #pragma omp taskgraph tdg_type(static) nowait
		#endif
		{
		#if defined(_OPENMP)
		 #pragma omp task depend(out:label527,label530)
		#elif defined(_OMPSS)
		 #pragma oss task depend(out:label527,label530)
		#endif
		run_run99(torqueDemand_publisher, speed_publisher, userspeedsetpoint_publisher);
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:label527)
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:label527)
		#endif
		run_run100();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:label530)
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:label530)
		#endif
		run_run101();
		}
		#pragma omp taskwait;
	}
	
	
};
