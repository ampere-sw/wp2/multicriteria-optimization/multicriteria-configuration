// This code is auto-generated

#include <chrono>
#include <memory>

#include "rclcpp/rclcpp.hpp"
#include "std_msgs/msg/string.hpp"

#include "runnables.hpp"
#include "trigger_trajectory_optimizer_pcc_service/srv/trigger_trajectory_optimizer_pcc_service.hpp"

using namespace std::chrono_literals;
using std::placeholders::_1;
#define CONSOLE_ENABLED

#if defined(_OPENMP)
int check_Node_trajectory_optimizer_pcc(int *original, int *replicated){ return 1;}
#endif
class Node_trajectory_optimizer_pcc : public rclcpp::Node
{
	private:
		rclcpp::Service<trigger_trajectory_optimizer_pcc_service::srv::TriggerTrajectoryOptimizerPccService>::SharedPtr trigger_trajectory_optimizer_pcc_service;
		rclcpp::Publisher<std_msgs::msg::String>::SharedPtr pccspeedsetpoint_publisher;
		rclcpp::Publisher<std_msgs::msg::String>::SharedPtr operationsetpoint_publisher;

	public:
		Node_trajectory_optimizer_pcc()
		: Node("node_trajectory_optimizer_pcc")
		{
			trigger_trajectory_optimizer_pcc_service = this->create_service<trigger_trajectory_optimizer_pcc_service::srv::TriggerTrajectoryOptimizerPccService>(
				"trigger_trajectory_optimizer_pcc_service", 
				std::bind(&Node_trajectory_optimizer_pcc::trigger_trajectory_optimizer_pcc_service_callback, this, std::placeholders::_1, std::placeholders::_2));
			pccspeedsetpoint_publisher = this->create_publisher<std_msgs::msg::String>("pccspeedsetpoint", 10);
			operationsetpoint_publisher = this->create_publisher<std_msgs::msg::String>("operationsetpoint", 10);
		}
	
	void trigger_trajectory_optimizer_pcc_service_callback(const std::shared_ptr<trigger_trajectory_optimizer_pcc_service::srv::TriggerTrajectoryOptimizerPccService::Request> request,
		std::shared_ptr<trigger_trajectory_optimizer_pcc_service::srv::TriggerTrajectoryOptimizerPccService::Response> response) {
			(void)request;
			(void)response;
	#ifdef CONSOLE_ENABLED
		std::cout << "Starting trigger_trajectory_optimizer_pcc_service_callback" << std::endl;
	#endif
		run_runnable_66();
		run_runnable_52();
		run_runnable_43();
		run_runnable_114();
		run_runnable_47();
		run_runnable_61();
		run_runnable_65();
		run_runnable_57();
		run_runnable_59();
		run_runnable_58();
		run_runnable_67();
		run_runnable_117(pccspeedsetpoint_publisher, operationsetpoint_publisher);
	}
	
};
