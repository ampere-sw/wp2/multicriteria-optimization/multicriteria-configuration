// This code is auto-generated

#include <chrono>
#include <memory>

#include "rclcpp/rclcpp.hpp"
#include "std_msgs/msg/string.hpp"

#include "detection_service/srv/detection_service.hpp"
#include "runnables.hpp"

using namespace std::chrono_literals;
using std::placeholders::_1;
#define CONSOLE_ENABLED

#if defined(_OPENMP)
int check_Node_detection_tsr(int *original, int *replicated){ return 1;}
#endif
class Node_detection_tsr : public rclcpp::Node
{
	private:
		rclcpp::Service<detection_service::srv::DetectionService>::SharedPtr detection_service;
		rclcpp::Client<classification_service::srv::ClassificationService>::SharedPtr classification_client;

	public:
		Node_detection_tsr()
		: Node("node_detection_tsr")
		{
			detection_service = this->create_service<detection_service::srv::DetectionService>(
				"detection_service", 
				std::bind(&Node_detection_tsr::detection_service_callback, this, std::placeholders::_1, std::placeholders::_2));
			classification_client =  this->create_client<classification_service::srv::ClassificationService>("classification_service");
		}
	
	void detection_service_callback(const std::shared_ptr<detection_service::srv::DetectionService::Request> request,
		std::shared_ptr<detection_service::srv::DetectionService::Response> response) {
			(void)request;
			(void)response;
	#ifdef CONSOLE_ENABLED
		std::cout << "Starting detection_service_callback" << std::endl;
	#endif
		extern int detected2[20500];
		extern int detection_memory_access5[38925];
		extern int detected5[20500];
		extern int segmented_to_back[27500];
		extern int detected9[20500];
		extern int detection_memory_access4[38925];
		extern int detected8[20500];
		extern int detection_memory_access3[38925];
		extern int detected1[20500];
		extern int detected10[20500];
		extern int detection_memory_access[38925];
		extern int detected7[20500];
		extern int detected3[20500];
		extern int detected6[20500];
		extern int detection_memory_access2[38925];
		extern int detected4[20500];
		int dummy;
		
		 #if defined(_OPENMP)
		 #pragma omp taskgraph tdg_type(static) nowait
		#endif
		{
		#if defined(_OPENMP)
		 #pragma omp task
		 #pragma omp target  map(to:segmented_to_back[0:27500]) map(from:detected4[0:20500],detection_memory_access2[0:38925],detected6[0:20500],detected3[0:20500],detected7[0:20500],detection_memory_access[0:38925],detected10[0:20500],detected1[0:20500],detection_memory_access3[0:38925],detected8[0:20500],detection_memory_access4[0:38925],detected9[0:20500],detected5[0:20500],detection_memory_access5[0:38925],detected2[0:20500])
		#elif defined(_OMPSS)
		 #pragma oss task map(to:segmented_to_back[0:27500]) map(from:detected4[0:20500],detection_memory_access2[0:38925],detected6[0:20500],detected3[0:20500],detected7[0:20500],detection_memory_access[0:38925],detected10[0:20500],detected1[0:20500],detection_memory_access3[0:38925],detected8[0:20500],detection_memory_access4[0:38925],detected9[0:20500],detected5[0:20500],detection_memory_access5[0:38925],detected2[0:20500]) copy_deps
		#endif
		run_detection(classification_client);
		}
		#pragma omp taskwait;
	}
	
};
