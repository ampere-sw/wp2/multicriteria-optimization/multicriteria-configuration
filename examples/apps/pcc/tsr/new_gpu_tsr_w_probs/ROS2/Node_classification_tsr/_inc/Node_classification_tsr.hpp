// This code is auto-generated

#include <chrono>
#include <memory>

#include "rclcpp/rclcpp.hpp"
#include "std_msgs/msg/string.hpp"

#include "classification_service/srv/classification_service.hpp"
#include "runnables.hpp"

using namespace std::chrono_literals;
using std::placeholders::_1;
#define CONSOLE_ENABLED

#if defined(_OPENMP)
int check_Node_classification_tsr(int *original, int *replicated){ return 1;}
#endif
class Node_classification_tsr : public rclcpp::Node
{
	private:
		rclcpp::Service<classification_service::srv::ClassificationService>::SharedPtr classification_service;

	public:
		Node_classification_tsr()
		: Node("node_classification_tsr")
		{
			classification_service = this->create_service<classification_service::srv::ClassificationService>(
				"classification_service", 
				std::bind(&Node_classification_tsr::classification_service_callback, this, std::placeholders::_1, std::placeholders::_2));
		}
	
	void classification_service_callback(const std::shared_ptr<classification_service::srv::ClassificationService::Request> request,
		std::shared_ptr<classification_service::srv::ClassificationService::Response> response) {
			(void)request;
			(void)response;
	#ifdef CONSOLE_ENABLED
		std::cout << "Starting classification_service_callback" << std::endl;
	#endif
		extern int detected2[20500];
		extern int detected5[20500];
		extern int detected9[20500];
		extern int classified6[1];
		extern int classification_memory_access[816];
		extern int detected1[20500];
		extern int classified4[1];
		extern int classified2[1];
		extern int classification_memory_access4[816];
		extern int classified5[1];
		extern int detected3[20500];
		extern int classified10[1];
		extern int detected6[20500];
		extern int classified7[1];
		extern int detected4[20500];
		extern int classification_memory_access3[816];
		extern int classified9[1];
		extern int classification_memory_access2[816];
		extern int classified1[1];
		extern int detected8[20500];
		extern int classified8[1];
		extern int detected10[20500];
		extern int classified3[1];
		extern int classification_memory_access5[816];
		extern int detected7[20500];
		int dummy;
		
		 #if defined(_OPENMP)
		 #pragma omp taskgraph tdg_type(static) nowait
		#endif
		{
		#if defined(_OPENMP)
		 #pragma omp task
		 #pragma omp target  map(to:detected1[0:20500]) map(from:classified1[0:0],classification_memory_access4[0:816],classification_memory_access5[0:816],classification_memory_access2[0:816],classification_memory_access3[0:816],classification_memory_access[0:816])
		#elif defined(_OMPSS)
		 #pragma oss task map(to:detected1[0:20500]) map(from:classified1[0:0],classification_memory_access4[0:816],classification_memory_access5[0:816],classification_memory_access2[0:816],classification_memory_access3[0:816],classification_memory_access[0:816]) copy_deps
		#endif
		run_classification1();
		
		#if defined(_OPENMP)
		 #pragma omp task
		 #pragma omp target  map(to:detected2[0:20500]) map(from:classification_memory_access4[0:816],classification_memory_access5[0:816],classification_memory_access2[0:816],classified2[0:0],classification_memory_access[0:816],classification_memory_access3[0:816])
		#elif defined(_OMPSS)
		 #pragma oss task map(to:detected2[0:20500]) map(from:classification_memory_access4[0:816],classification_memory_access5[0:816],classification_memory_access2[0:816],classified2[0:0],classification_memory_access[0:816],classification_memory_access3[0:816]) copy_deps
		#endif
		run_classification2();
		
		#if defined(_OPENMP)
		 #pragma omp task
		 #pragma omp target  map(to:detected3[0:20500]) map(from:classification_memory_access4[0:816],classification_memory_access5[0:816],classification_memory_access2[0:816],classified3[0:0],classification_memory_access[0:816],classification_memory_access3[0:816])
		#elif defined(_OMPSS)
		 #pragma oss task map(to:detected3[0:20500]) map(from:classification_memory_access4[0:816],classification_memory_access5[0:816],classification_memory_access2[0:816],classified3[0:0],classification_memory_access[0:816],classification_memory_access3[0:816]) copy_deps
		#endif
		run_classification3();
		
		#if defined(_OPENMP)
		 #pragma omp task
		 #pragma omp target  map(to:detected4[0:20500]) map(from:classification_memory_access4[0:816],classification_memory_access5[0:816],classification_memory_access2[0:816],classified4[0:0],classification_memory_access[0:816],classification_memory_access3[0:816])
		#elif defined(_OMPSS)
		 #pragma oss task map(to:detected4[0:20500]) map(from:classification_memory_access4[0:816],classification_memory_access5[0:816],classification_memory_access2[0:816],classified4[0:0],classification_memory_access[0:816],classification_memory_access3[0:816]) copy_deps
		#endif
		run_classification4();
		
		#if defined(_OPENMP)
		 #pragma omp task
		 #pragma omp target  map(to:detected5[0:20500]) map(from:classified5[0:0],classification_memory_access4[0:816],classification_memory_access5[0:816],classification_memory_access2[0:816],classification_memory_access3[0:816],classification_memory_access[0:816])
		#elif defined(_OMPSS)
		 #pragma oss task map(to:detected5[0:20500]) map(from:classified5[0:0],classification_memory_access4[0:816],classification_memory_access5[0:816],classification_memory_access2[0:816],classification_memory_access3[0:816],classification_memory_access[0:816]) copy_deps
		#endif
		run_classification5();
		
		#if defined(_OPENMP)
		 #pragma omp task
		 #pragma omp target  map(to:detected6[0:20500]) map(from:classified6[0:0],classification_memory_access4[0:816],classification_memory_access2[0:816],classification_memory_access5[0:816],classification_memory_access3[0:816],classification_memory_access[0:816])
		#elif defined(_OMPSS)
		 #pragma oss task map(to:detected6[0:20500]) map(from:classified6[0:0],classification_memory_access4[0:816],classification_memory_access2[0:816],classification_memory_access5[0:816],classification_memory_access3[0:816],classification_memory_access[0:816]) copy_deps
		#endif
		run_classification6();
		
		#if defined(_OPENMP)
		 #pragma omp task
		 #pragma omp target  map(to:detected7[0:20500]) map(from:classified7[0:0],classification_memory_access4[0:816],classification_memory_access2[0:816],classification_memory_access5[0:816],classification_memory_access3[0:816],classification_memory_access[0:816])
		#elif defined(_OMPSS)
		 #pragma oss task map(to:detected7[0:20500]) map(from:classified7[0:0],classification_memory_access4[0:816],classification_memory_access2[0:816],classification_memory_access5[0:816],classification_memory_access3[0:816],classification_memory_access[0:816]) copy_deps
		#endif
		run_classification7();
		
		#if defined(_OPENMP)
		 #pragma omp task
		 #pragma omp target  map(to:detected8[0:20500]) map(from:classification_memory_access4[0:816],classification_memory_access5[0:816],classification_memory_access2[0:816],classification_memory_access3[0:816],classification_memory_access[0:816],classified8[0:0])
		#elif defined(_OMPSS)
		 #pragma oss task map(to:detected8[0:20500]) map(from:classification_memory_access4[0:816],classification_memory_access5[0:816],classification_memory_access2[0:816],classification_memory_access3[0:816],classification_memory_access[0:816],classified8[0:0]) copy_deps
		#endif
		run_classification8();
		
		#if defined(_OPENMP)
		 #pragma omp task
		 #pragma omp target  map(to:detected9[0:20500]) map(from:classification_memory_access4[0:816],classification_memory_access2[0:816],classification_memory_access5[0:816],classified9[0:0],classification_memory_access[0:816],classification_memory_access3[0:816])
		#elif defined(_OMPSS)
		 #pragma oss task map(to:detected9[0:20500]) map(from:classification_memory_access4[0:816],classification_memory_access2[0:816],classification_memory_access5[0:816],classified9[0:0],classification_memory_access[0:816],classification_memory_access3[0:816]) copy_deps
		#endif
		run_classification9();
		
		#if defined(_OPENMP)
		 #pragma omp task
		 #pragma omp target  map(to:detected10[0:20500]) map(from:classified10[0:0],classification_memory_access4[0:816],classification_memory_access2[0:816],classification_memory_access5[0:816],classification_memory_access[0:816],classification_memory_access3[0:816])
		#elif defined(_OMPSS)
		 #pragma oss task map(to:detected10[0:20500]) map(from:classified10[0:0],classification_memory_access4[0:816],classification_memory_access2[0:816],classification_memory_access5[0:816],classification_memory_access[0:816],classification_memory_access3[0:816]) copy_deps
		#endif
		run_classification10();
		}
		#pragma omp taskwait;
	}
	
};
