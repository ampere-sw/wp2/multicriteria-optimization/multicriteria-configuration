// This code is auto-generated

#include <chrono>
#include <memory>

#include "rclcpp/rclcpp.hpp"
#include "std_msgs/msg/string.hpp"

#include "runnables.hpp"

using namespace std::chrono_literals;
using std::placeholders::_1;
#define CONSOLE_ENABLED

#if defined(_OPENMP)
int check_Node_task_2ms_ecm(int *original, int *replicated){ return 1;}
#endif
class Node_task_2ms_ecm : public rclcpp::Node
{
	private:
		rclcpp::TimerBase::SharedPtr timer_periodic_2ms_;

	public:
		Node_task_2ms_ecm()
		: Node("node_task_2ms_ecm")
		{
			timer_periodic_2ms_ = this->create_wall_timer(
					2ms, std::bind(&Node_task_2ms_ecm::timer_periodic_2ms_callback, this));
		}
	void timer_periodic_2ms_callback() {
	#ifdef CONSOLE_ENABLED
		std::cout << "Timer_periodic_2ms_callback (2ms)" << std::endl;
	#endif
		run_runnable_2ms_0();
		run_runnable_2ms_1();
		run_runnable_2ms_2();
		run_runnable_2ms_3();
		run_runnable_2ms_4();
		run_runnable_2ms_5();
		run_runnable_2ms_6();
		run_runnable_2ms_7();
		run_runnable_2ms_8();
		run_runnable_2ms_9();
		run_runnable_2ms_10();
		run_runnable_2ms_11();
		run_runnable_2ms_12();
		run_runnable_2ms_13();
		run_runnable_2ms_14();
		run_runnable_2ms_15();
		run_runnable_2ms_16();
		run_runnable_2ms_17();
		run_runnable_2ms_18();
		run_runnable_2ms_19();
		run_runnable_2ms_20();
		run_runnable_2ms_21();
		run_runnable_2ms_22();
		run_runnable_2ms_23();
		run_runnable_2ms_24();
		run_runnable_2ms_25();
		run_runnable_2ms_26();
		run_runnable_2ms_27();
	}
	
	
};
