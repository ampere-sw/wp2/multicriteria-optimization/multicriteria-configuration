// This code is auto-generated

#include <chrono>
#include <memory>

#include "rclcpp/rclcpp.hpp"
#include "std_msgs/msg/string.hpp"

#include "runnables.hpp"

using namespace std::chrono_literals;
using std::placeholders::_1;
#define CONSOLE_ENABLED

#if defined(_OPENMP)
int check_Node_preprocessing_acc(int *original, int *replicated){ return 1;}
#endif
class Node_preprocessing_acc : public rclcpp::Node
{
	private:
		rclcpp::TimerBase::SharedPtr timer_66mstrigger_;
		rclcpp::Client<trigger_perception_acc_service::srv::TriggerPerceptionAccService>::SharedPtr trigger_perception_acc_client;

	public:
		Node_preprocessing_acc()
		: Node("node_preprocessing_acc")
		{
			timer_66mstrigger_ = this->create_wall_timer(
					66ms, std::bind(&Node_preprocessing_acc::timer_66mstrigger_callback, this));
			trigger_perception_acc_client =  this->create_client<trigger_perception_acc_service::srv::TriggerPerceptionAccService>("trigger_perception_acc_service");
		}
	void timer_66mstrigger_callback() {
	#ifdef CONSOLE_ENABLED
		std::cout << "Timer_66mstrigger_callback (66ms)" << std::endl;
	#endif
		extern int label6[1];
		extern int label12[1];
		extern int label15[1];
		extern int label21[1];
		extern int label20[1];
		extern int label9[1];
		extern int label23[1];
		int dummy;
		
		 #if defined(_OPENMP)
		 #pragma omp taskgraph tdg_type(static) nowait
		#endif
		{
		#if defined(_OPENMP)
		 #pragma omp task depend(out:label23,label9,label20,label21,label15,label12,label6)
		#elif defined(_OMPSS)
		 #pragma oss task depend(out:label23,label9,label20,label21,label15,label12,label6)
		#endif
		run_run3();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:label6)
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:label6)
		#endif
		run_run4();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:label9)
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:label9)
		#endif
		run_run5();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:label12)
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:label12)
		#endif
		run_run6();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:label15)
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:label15)
		#endif
		run_run7();
		
		#if defined(_OPENMP)
		 #pragma omp task
		#elif defined(_OMPSS)
		 #pragma oss task
		#endif
		run_run8();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:label20,label21)
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:label20,label21)
		#endif
		run_run9();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:label23)
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:label23)
		#endif
		run_run10();
		
		#if defined(_OPENMP)
		 #pragma omp task
		#elif defined(_OMPSS)
		 #pragma oss task
		#endif
		run_run11();
		
		#if defined(_OPENMP)
		 #pragma omp task
		#elif defined(_OMPSS)
		 #pragma oss task
		#endif
		run_run12(trigger_perception_acc_client);
		}
		#pragma omp taskwait;
	}
	
	
};
