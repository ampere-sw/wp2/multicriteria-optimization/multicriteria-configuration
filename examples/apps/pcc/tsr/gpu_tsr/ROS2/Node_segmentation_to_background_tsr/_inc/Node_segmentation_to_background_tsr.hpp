// This code is auto-generated

#include <chrono>
#include <memory>

#include "rclcpp/rclcpp.hpp"
#include "std_msgs/msg/string.hpp"

#include "runnables.hpp"
#include "segmentation_to_bckground_service/srv/segmentation_to_bckground_service.hpp"

using namespace std::chrono_literals;
using std::placeholders::_1;
#define CONSOLE_ENABLED

#if defined(_OPENMP)
int check_Node_segmentation_to_background_tsr(int *original, int *replicated){ return 1;}
#endif
class Node_segmentation_to_background_tsr : public rclcpp::Node
{
	private:
		rclcpp::Service<segmentation_to_bckground_service::srv::SegmentationToBckgroundService>::SharedPtr segmentation_to_bckground_service;
		rclcpp::Client<detection_service::srv::DetectionService>::SharedPtr detection_client;

	public:
		Node_segmentation_to_background_tsr()
		: Node("node_segmentation_to_background_tsr")
		{
			segmentation_to_bckground_service = this->create_service<segmentation_to_bckground_service::srv::SegmentationToBckgroundService>(
				"segmentation_to_bckground_service", 
				std::bind(&Node_segmentation_to_background_tsr::segmentation_to_bckground_service_callback, this, std::placeholders::_1, std::placeholders::_2));
			detection_client =  this->create_client<detection_service::srv::DetectionService>("detection_service");
		}
	
	void segmentation_to_bckground_service_callback(const std::shared_ptr<segmentation_to_bckground_service::srv::SegmentationToBckgroundService::Request> request,
		std::shared_ptr<segmentation_to_bckground_service::srv::SegmentationToBckgroundService::Response> response) {
			(void)request;
			(void)response;
	#ifdef CONSOLE_ENABLED
		std::cout << "Starting segmentation_to_bckground_service_callback" << std::endl;
	#endif
		int dummy;
		
		 #if defined(_OPENMP)
		 #pragma omp taskgraph tdg_type(static) nowait
		#endif
		{
		#if defined(_OPENMP)
		 #pragma omp task
		 #pragma omp target 
		#elif defined(_OMPSS)
		 #pragma oss task copy_deps
		#endif
		run_segm_to_background(detection_client);
		}
		#pragma omp taskwait;
	}
	
};
