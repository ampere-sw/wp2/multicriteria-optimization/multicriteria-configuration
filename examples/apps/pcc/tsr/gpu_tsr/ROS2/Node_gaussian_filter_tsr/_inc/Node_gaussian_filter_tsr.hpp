// This code is auto-generated

#include <chrono>
#include <memory>

#include "rclcpp/rclcpp.hpp"
#include "std_msgs/msg/string.hpp"

#include "gausian_filter_service/srv/gausian_filter_service.hpp"
#include "runnables.hpp"

using namespace std::chrono_literals;
using std::placeholders::_1;
#define CONSOLE_ENABLED

#if defined(_OPENMP)
int check_Node_gaussian_filter_tsr(int *original, int *replicated){ return 1;}
#endif
class Node_gaussian_filter_tsr : public rclcpp::Node
{
	private:
		rclcpp::Service<gausian_filter_service::srv::GausianFilterService>::SharedPtr gausian_filter_service;
		rclcpp::Client<segmentation_service::srv::SegmentationService>::SharedPtr segmentation_client;

	public:
		Node_gaussian_filter_tsr()
		: Node("node_gaussian_filter_tsr")
		{
			gausian_filter_service = this->create_service<gausian_filter_service::srv::GausianFilterService>(
				"gausian_filter_service", 
				std::bind(&Node_gaussian_filter_tsr::gausian_filter_service_callback, this, std::placeholders::_1, std::placeholders::_2));
			segmentation_client =  this->create_client<segmentation_service::srv::SegmentationService>("segmentation_service");
		}
	
	void gausian_filter_service_callback(const std::shared_ptr<gausian_filter_service::srv::GausianFilterService::Request> request,
		std::shared_ptr<gausian_filter_service::srv::GausianFilterService::Response> response) {
			(void)request;
			(void)response;
	#ifdef CONSOLE_ENABLED
		std::cout << "Starting gausian_filter_service_callback" << std::endl;
	#endif
		int dummy;
		
		 #if defined(_OPENMP)
		 #pragma omp taskgraph tdg_type(static) nowait
		#endif
		{
		#if defined(_OPENMP)
		 #pragma omp task
		 #pragma omp target 
		#elif defined(_OMPSS)
		 #pragma oss task copy_deps
		#endif
		run_gausian_filter(segmentation_client);
		}
		#pragma omp taskwait;
	}
	
};
