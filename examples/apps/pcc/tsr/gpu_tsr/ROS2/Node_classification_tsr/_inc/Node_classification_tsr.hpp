// This code is auto-generated

#include <chrono>
#include <memory>

#include "rclcpp/rclcpp.hpp"
#include "std_msgs/msg/string.hpp"

#include "classification_service/srv/classification_service.hpp"
#include "runnables.hpp"

using namespace std::chrono_literals;
using std::placeholders::_1;
#define CONSOLE_ENABLED

#if defined(_OPENMP)
int check_Node_classification_tsr(int *original, int *replicated){ return 1;}
#endif
class Node_classification_tsr : public rclcpp::Node
{
	private:
		rclcpp::Service<classification_service::srv::ClassificationService>::SharedPtr classification_service;

	public:
		Node_classification_tsr()
		: Node("node_classification_tsr")
		{
			classification_service = this->create_service<classification_service::srv::ClassificationService>(
				"classification_service", 
				std::bind(&Node_classification_tsr::classification_service_callback, this, std::placeholders::_1, std::placeholders::_2));
		}
	
	void classification_service_callback(const std::shared_ptr<classification_service::srv::ClassificationService::Request> request,
		std::shared_ptr<classification_service::srv::ClassificationService::Response> response) {
			(void)request;
			(void)response;
	#ifdef CONSOLE_ENABLED
		std::cout << "Starting classification_service_callback" << std::endl;
	#endif
		extern int classification_memory_access3[816];
		extern int classification_memory_access4[816];
		extern int classification_memory_access5[816];
		extern int classification_memory_access2[816];
		extern int classification_memory_access[816];
		int dummy;
		
		 #if defined(_OPENMP)
		 #pragma omp taskgraph tdg_type(static) nowait
		#endif
		{
		#if defined(_OPENMP)
		 #pragma omp task depend(out:classification_memory_access,classification_memory_access2,classification_memory_access5,classification_memory_access4,classification_memory_access3)
		 #pragma omp target  map(from:classification_memory_access[0:816],classification_memory_access2[0:816],classification_memory_access5[0:816],classification_memory_access4[0:816],classification_memory_access3[0:816])
		#elif defined(_OMPSS)
		 #pragma oss task depend(out:classification_memory_access,classification_memory_access2,classification_memory_access5,classification_memory_access4,classification_memory_access3) map(from:classification_memory_access[0:816],classification_memory_access2[0:816],classification_memory_access5[0:816],classification_memory_access4[0:816],classification_memory_access3[0:816]) copy_deps
		#endif
		run_classification1();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(out:classification_memory_access,classification_memory_access2,classification_memory_access5,classification_memory_access4,classification_memory_access3)
		 #pragma omp target  map(from:classification_memory_access[0:816],classification_memory_access2[0:816],classification_memory_access5[0:816],classification_memory_access4[0:816],classification_memory_access3[0:816])
		#elif defined(_OMPSS)
		 #pragma oss task depend(out:classification_memory_access,classification_memory_access2,classification_memory_access5,classification_memory_access4,classification_memory_access3) map(from:classification_memory_access[0:816],classification_memory_access2[0:816],classification_memory_access5[0:816],classification_memory_access4[0:816],classification_memory_access3[0:816]) copy_deps
		#endif
		run_classification2();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(out:classification_memory_access,classification_memory_access2,classification_memory_access5,classification_memory_access4,classification_memory_access3)
		 #pragma omp target  map(from:classification_memory_access[0:816],classification_memory_access2[0:816],classification_memory_access5[0:816],classification_memory_access4[0:816],classification_memory_access3[0:816])
		#elif defined(_OMPSS)
		 #pragma oss task depend(out:classification_memory_access,classification_memory_access2,classification_memory_access5,classification_memory_access4,classification_memory_access3) map(from:classification_memory_access[0:816],classification_memory_access2[0:816],classification_memory_access5[0:816],classification_memory_access4[0:816],classification_memory_access3[0:816]) copy_deps
		#endif
		run_classification3();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(out:classification_memory_access,classification_memory_access2,classification_memory_access5,classification_memory_access4,classification_memory_access3)
		 #pragma omp target  map(from:classification_memory_access[0:816],classification_memory_access2[0:816],classification_memory_access5[0:816],classification_memory_access4[0:816],classification_memory_access3[0:816])
		#elif defined(_OMPSS)
		 #pragma oss task depend(out:classification_memory_access,classification_memory_access2,classification_memory_access5,classification_memory_access4,classification_memory_access3) map(from:classification_memory_access[0:816],classification_memory_access2[0:816],classification_memory_access5[0:816],classification_memory_access4[0:816],classification_memory_access3[0:816]) copy_deps
		#endif
		run_classification4();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(out:classification_memory_access,classification_memory_access2,classification_memory_access5,classification_memory_access4,classification_memory_access3)
		 #pragma omp target  map(from:classification_memory_access[0:816],classification_memory_access2[0:816],classification_memory_access5[0:816],classification_memory_access4[0:816],classification_memory_access3[0:816])
		#elif defined(_OMPSS)
		 #pragma oss task depend(out:classification_memory_access,classification_memory_access2,classification_memory_access5,classification_memory_access4,classification_memory_access3) map(from:classification_memory_access[0:816],classification_memory_access2[0:816],classification_memory_access5[0:816],classification_memory_access4[0:816],classification_memory_access3[0:816]) copy_deps
		#endif
		run_classification5();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(out:classification_memory_access,classification_memory_access2,classification_memory_access5,classification_memory_access4,classification_memory_access3)
		 #pragma omp target  map(from:classification_memory_access[0:816],classification_memory_access2[0:816],classification_memory_access5[0:816],classification_memory_access4[0:816],classification_memory_access3[0:816])
		#elif defined(_OMPSS)
		 #pragma oss task depend(out:classification_memory_access,classification_memory_access2,classification_memory_access5,classification_memory_access4,classification_memory_access3) map(from:classification_memory_access[0:816],classification_memory_access2[0:816],classification_memory_access5[0:816],classification_memory_access4[0:816],classification_memory_access3[0:816]) copy_deps
		#endif
		run_classification6();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(out:classification_memory_access,classification_memory_access2,classification_memory_access5,classification_memory_access4,classification_memory_access3)
		 #pragma omp target  map(from:classification_memory_access[0:816],classification_memory_access2[0:816],classification_memory_access5[0:816],classification_memory_access4[0:816],classification_memory_access3[0:816])
		#elif defined(_OMPSS)
		 #pragma oss task depend(out:classification_memory_access,classification_memory_access2,classification_memory_access5,classification_memory_access4,classification_memory_access3) map(from:classification_memory_access[0:816],classification_memory_access2[0:816],classification_memory_access5[0:816],classification_memory_access4[0:816],classification_memory_access3[0:816]) copy_deps
		#endif
		run_classification7();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(out:classification_memory_access,classification_memory_access2,classification_memory_access5,classification_memory_access4,classification_memory_access3)
		 #pragma omp target  map(from:classification_memory_access[0:816],classification_memory_access2[0:816],classification_memory_access5[0:816],classification_memory_access4[0:816],classification_memory_access3[0:816])
		#elif defined(_OMPSS)
		 #pragma oss task depend(out:classification_memory_access,classification_memory_access2,classification_memory_access5,classification_memory_access4,classification_memory_access3) map(from:classification_memory_access[0:816],classification_memory_access2[0:816],classification_memory_access5[0:816],classification_memory_access4[0:816],classification_memory_access3[0:816]) copy_deps
		#endif
		run_classification8();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(out:classification_memory_access,classification_memory_access2,classification_memory_access5,classification_memory_access4,classification_memory_access3)
		 #pragma omp target  map(from:classification_memory_access[0:816],classification_memory_access2[0:816],classification_memory_access5[0:816],classification_memory_access4[0:816],classification_memory_access3[0:816])
		#elif defined(_OMPSS)
		 #pragma oss task depend(out:classification_memory_access,classification_memory_access2,classification_memory_access5,classification_memory_access4,classification_memory_access3) map(from:classification_memory_access[0:816],classification_memory_access2[0:816],classification_memory_access5[0:816],classification_memory_access4[0:816],classification_memory_access3[0:816]) copy_deps
		#endif
		run_classification9();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(out:classification_memory_access,classification_memory_access2,classification_memory_access5,classification_memory_access4,classification_memory_access3)
		 #pragma omp target  map(from:classification_memory_access[0:816],classification_memory_access2[0:816],classification_memory_access5[0:816],classification_memory_access4[0:816],classification_memory_access3[0:816])
		#elif defined(_OMPSS)
		 #pragma oss task depend(out:classification_memory_access,classification_memory_access2,classification_memory_access5,classification_memory_access4,classification_memory_access3) map(from:classification_memory_access[0:816],classification_memory_access2[0:816],classification_memory_access5[0:816],classification_memory_access4[0:816],classification_memory_access3[0:816]) copy_deps
		#endif
		run_classification10();
		}
		#pragma omp taskwait;
	}
	
};
