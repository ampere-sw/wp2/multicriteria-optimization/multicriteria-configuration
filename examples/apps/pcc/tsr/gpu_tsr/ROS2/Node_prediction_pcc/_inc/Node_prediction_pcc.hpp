// This code is auto-generated

#include <chrono>
#include <memory>

#include "rclcpp/rclcpp.hpp"
#include "std_msgs/msg/string.hpp"

#include "runnables.hpp"

using namespace std::chrono_literals;
using std::placeholders::_1;
#define CONSOLE_ENABLED

#if defined(_OPENMP)
int check_Node_prediction_pcc(int *original, int *replicated){ return 1;}
#endif
class Node_prediction_pcc : public rclcpp::Node
{
	private:
		rclcpp::TimerBase::SharedPtr timer_500mstrigger_;
		rclcpp::Client<trigger_trajectory_optimizer_pcc_service::srv::TriggerTrajectoryOptimizerPccService>::SharedPtr trigger_trajectory_optimizer_pcc_client;

	public:
		Node_prediction_pcc()
		: Node("node_prediction_pcc")
		{
			timer_500mstrigger_ = this->create_wall_timer(
					500ms, std::bind(&Node_prediction_pcc::timer_500mstrigger_callback, this));
			trigger_trajectory_optimizer_pcc_client =  this->create_client<trigger_trajectory_optimizer_pcc_service::srv::TriggerTrajectoryOptimizerPccService>("trigger_trajectory_optimizer_pcc_service");
		}
	void timer_500mstrigger_callback() {
	#ifdef CONSOLE_ENABLED
		std::cout << "Timer_500mstrigger_callback (500ms)" << std::endl;
	#endif
		run_runnable_60();
		run_runnable_45();
		run_runnable_38();
		run_runnable_56();
		run_runnable_35();
		run_runnable_44();
		run_runnable_5(trigger_trajectory_optimizer_pcc_client);
	}
	
	
};
