#include "Node_cominput_tsr.hpp"
#include "Node_resizing_tsr.hpp"
#include "Node_gaussian_filter_tsr.hpp"
#include "Node_segmentation_tsr.hpp"
#include "Node_segmentation_to_background_tsr.hpp"
#include "Node_detection_tsr.hpp"
#include "Node_classification_tsr.hpp"
#include "Node_output_tsr.hpp"
#include <extrae.h>
extern "C" {
	#include <cudamperf.h>
}
int main(int argc, char *argv[])
{
	cudamperf_start(argv[0]);
	setvbuf(stdout, NULL, _IONBF, BUFSIZ);
	rclcpp::init(argc, argv);
	auto node1 = new Node_cominput_tsr();
	auto node2 = new Node_resizing_tsr();
	auto node3 = new Node_gaussian_filter_tsr();
	auto node4 = new Node_segmentation_tsr();
	auto node5 = new Node_segmentation_to_background_tsr();
	auto node6 = new Node_detection_tsr();
	auto node7 = new Node_classification_tsr();
	auto node8 = new Node_output_tsr();

	Extrae_init();

	#pragma omp parallel 
	#pragma omp single
	{
		cudamperf_enter_omp_region("cominput_tsr");
    	node1->timer_trigger_callback();
        cudamperf_exit_omp_region();
	}

	#pragma omp parallel 
	#pragma omp single
	{
		cudamperf_enter_omp_region("resizing_tsr");
        node2->timer_preprocessing_callback();
        cudamperf_exit_omp_region();
	}
	#pragma omp parallel 
	#pragma omp single
	{
		cudamperf_enter_omp_region("gaussian_filter_tsr");
        node3->gausian_filter_service_callback(NULL,NULL);
        cudamperf_exit_omp_region();
	}
	#pragma omp parallel 
	#pragma omp single
	{
		cudamperf_enter_omp_region("segmentation_tsr");
        node4->segmentation_service_callback(NULL,NULL);
        cudamperf_exit_omp_region();
	}
	#pragma omp parallel 
	#pragma omp single
	{
		cudamperf_enter_omp_region("segmentation_to_background_tsr");
        node5->segmentation_to_bckground_service_callback(NULL,NULL);
        cudamperf_exit_omp_region();
	}
	#pragma omp parallel 
	#pragma omp single
	{
		cudamperf_enter_omp_region("detection_tsr");
        node6->detection_service_callback(NULL,NULL);
        cudamperf_exit_omp_region();
	}
	#pragma omp parallel 
	#pragma omp single
	{
		cudamperf_enter_omp_region("classification_tsr");
        node7->classification_service_callback(NULL,NULL);
        cudamperf_exit_omp_region();
	}

	#pragma omp parallel 
	#pragma omp single
	{
		cudamperf_enter_omp_region("output_tsr");
    	node8->timer_output_callback();
		cudamperf_exit_omp_region();
	}

	cudamperf_stop();
	rclcpp::shutdown();

	Extrae_fini();
	return 0;
}
