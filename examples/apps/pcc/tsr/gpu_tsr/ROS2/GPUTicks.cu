extern "C" {
	#include <cudamperf.h>
}
#include <extrae.h>

#define RATIO 0.1 
//0.65
__global__ void kernel(long long nclocks){
	long long start=clock64();
	while (clock64() < start+nclocks);
}

void executeGPUTicks(long long average,long long lowerBound, long long upperBound) {
   cudamperf_enter_context();
   // uint64_t timestamp = cudamperf_get_timestamp();
   kernel<<<1,1>>>(10);//average * RATIO);
   cudaDeviceSynchronize();
   // uint64_t elapsed = cudamperf_get_timestamp();
   // elapsed -= timestamp;
   cudamperf_exit_context();
}
void executeGPUTicksConstant(long long ticks) {
   cudamperf_enter_context();
   // uint64_t timestamp = cudamperf_get_timestamp();
   kernel<<<1,1>>>(ticks * RATIO);
   cudaDeviceSynchronize();
   // uint64_t elapsed = cudamperf_get_timestamp();
   // elapsed -= timestamp;
   cudamperf_exit_context();
}