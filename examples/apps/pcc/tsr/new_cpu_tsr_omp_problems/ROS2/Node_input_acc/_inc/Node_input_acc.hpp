// This code is auto-generated

#include <chrono>
#include <memory>

#include "rclcpp/rclcpp.hpp"
#include "std_msgs/msg/string.hpp"

#include "runnables.hpp"

using namespace std::chrono_literals;
using std::placeholders::_1;
#define CONSOLE_ENABLED

#if defined(_OPENMP)
int check_Node_input_acc(int *original, int *replicated){ return 1;}
#endif
class Node_input_acc : public rclcpp::Node
{
	private:
		rclcpp::TimerBase::SharedPtr timer_5mstrigger_;

	public:
		Node_input_acc()
		: Node("node_input_acc")
		{
			timer_5mstrigger_ = this->create_wall_timer(
					5ms, std::bind(&Node_input_acc::timer_5mstrigger_callback, this));
		}
	void timer_5mstrigger_callback() {
	#ifdef CONSOLE_ENABLED
		std::cout << "Timer_5mstrigger_callback (5ms)" << std::endl;
	#endif
		extern int label2[240];
		extern int userinput_acc[1];
		extern int label3[1];
		extern int label4[289];
		extern int label1[1];
		extern int label0[1];
		extern int pcc_speed_setpoint[1];
		extern int objects_acc[3750];
		int dummy;
		
		 #if defined(_OPENMP)
		 #pragma omp taskgraph tdg_type(static) nowait
		#endif
		{
		#if defined(_OPENMP)
		 #pragma omp task depend(out:label1)
		#elif defined(_OMPSS)
		 #pragma oss task depend(out:label1)
		#endif
		run_run1();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:label1)
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:label1)
		#endif
		run_run2();
		}
		#pragma omp taskwait;
	}
	
	
};
