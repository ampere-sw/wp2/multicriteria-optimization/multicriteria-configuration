// This code is auto-generated

#pragma once 
#include <string>

#include "rclcpp/rclcpp.hpp"
#include "std_msgs/msg/string.hpp"


void publish_to_recognizedspeedlimit(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher);


void publish_to_pccspeedsetpoint(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher);


void publish_to_operationsetpoint(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher);


void publish_to_torqueDemand(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher);


void publish_to_speed(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher);


void publish_to_userspeedsetpoint(rclcpp::Publisher<std_msgs::msg::String>::SharedPtr& publisher);

