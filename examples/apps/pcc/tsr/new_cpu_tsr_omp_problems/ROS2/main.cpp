#include "Node_classification_tsr.hpp"
#include "Node_cominput_tsr.hpp"
#include "Node_control_behavior_acc.hpp"
#include "Node_databroker_acc.hpp"
#include "Node_databroker_ecm.hpp"
#include "Node_databroker_pcc.hpp"
#include "Node_detection_tsr.hpp"
#include "Node_gaussian_filter_tsr.hpp"
#include "Node_input_acc.hpp"
#include "Node_isr_10_ecm.hpp"
#include "Node_isr_11_ecm.hpp"
#include "Node_isr_1_ecm.hpp"
#include "Node_isr_2_ecm.hpp"
#include "Node_isr_3_ecm.hpp"
#include "Node_isr_4_ecm.hpp"
#include "Node_isr_5_ecm.hpp"
#include "Node_isr_6_ecm.hpp"
#include "Node_isr_7_ecm.hpp"
#include "Node_isr_8_ecm.hpp"
#include "Node_isr_9_ecm.hpp"
#include "Node_output_acc.hpp"
#include "Node_output_tsr.hpp"
#include "Node_perception_acc.hpp"
#include "Node_prediction_pcc.hpp"
#include "Node_preprocessing_acc.hpp"
#include "Node_resizing_tsr.hpp"
#include "Node_segmentation_to_background_tsr.hpp"
#include "Node_segmentation_tsr.hpp"
#include "Node_task_1000ms_ecm.hpp"
#include "Node_task_100ms_ecm.hpp"
#include "Node_task_10ms_ecm.hpp"
#include "Node_task_1ms_ecm.hpp"
#include "Node_task_200ms_ecm.hpp"
#include "Node_task_20ms_ecm.hpp"
#include "Node_task_2ms_ecm.hpp"
#include "Node_task_50ms_ecm.hpp"
#include "Node_task_5ms_ecm.hpp"
#include "Node_trajectory_optimizer_pcc.hpp"
#include "Node_world_model_acc.hpp"
#include <extrae.h>

int main(int argc, char *argv[])
{
	setvbuf(stdout, NULL, _IONBF, BUFSIZ);
	rclcpp::init(argc, argv);
	auto node1 = new Node_cominput_tsr();
	auto node2 = new Node_resizing_tsr();
	auto node3 = new Node_gaussian_filter_tsr();
	auto node4 = new Node_segmentation_tsr();
	auto node5 = new Node_segmentation_to_background_tsr();
	auto node6 = new Node_detection_tsr();
	auto node7 = new Node_classification_tsr();
	auto node8 = new Node_output_tsr();

	Extrae_init();
	int NUM_ITER = 2;

	for(int i = 0; i < NUM_ITER; i++){
		
		#pragma omp parallel 
		#pragma omp single
		{
			node1->timer_trigger_callback();
		}

		#pragma omp parallel 
		#pragma omp single
		{
			node2->timer_preprocessing_callback();
		}
		#pragma omp parallel 
		#pragma omp single
		{
			node3->gausian_filter_service_callback(NULL,NULL);
		}
		#pragma omp parallel 
		#pragma omp single
		{
			node4->segmentation_service_callback(NULL,NULL);
		}
		#pragma omp parallel 
		#pragma omp single
		{
			node5->segmentation_to_bckground_service_callback(NULL,NULL);
		}
		#pragma omp parallel 
		#pragma omp single
		{
			node6->detection_service_callback(NULL,NULL);
		}
		#pragma omp parallel 
		#pragma omp single
		{
			node7->classification_service_callback(NULL,NULL);
		}
		#pragma omp parallel 
		#pragma omp single
		{
		node8->timer_output_callback();
		}
	}
	rclcpp::shutdown();

	Extrae_fini();
	return 0;
}