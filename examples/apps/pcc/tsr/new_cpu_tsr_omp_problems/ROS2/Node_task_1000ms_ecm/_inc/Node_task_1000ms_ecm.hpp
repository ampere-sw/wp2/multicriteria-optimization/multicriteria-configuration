// This code is auto-generated

#include <chrono>
#include <memory>

#include "rclcpp/rclcpp.hpp"
#include "std_msgs/msg/string.hpp"

#include "runnables.hpp"

using namespace std::chrono_literals;
using std::placeholders::_1;
#define CONSOLE_ENABLED

#if defined(_OPENMP)
int check_Node_task_1000ms_ecm(int *original, int *replicated){ return 1;}
#endif
class Node_task_1000ms_ecm : public rclcpp::Node
{
	private:
		rclcpp::TimerBase::SharedPtr timer_periodic_1000ms_;

	public:
		Node_task_1000ms_ecm()
		: Node("node_task_1000ms_ecm")
		{
			timer_periodic_1000ms_ = this->create_wall_timer(
					1000ms, std::bind(&Node_task_1000ms_ecm::timer_periodic_1000ms_callback, this));
		}
	void timer_periodic_1000ms_callback() {
	#ifdef CONSOLE_ENABLED
		std::cout << "Timer_periodic_1000ms_callback (1000ms)" << std::endl;
	#endif
		run_runnable_1000ms_0();
		run_runnable_1000ms_1();
		run_runnable_1000ms_2();
		run_runnable_1000ms_3();
		run_runnable_1000ms_4();
		run_runnable_1000ms_5();
		run_runnable_1000ms_6();
		run_runnable_1000ms_7();
		run_runnable_1000ms_8();
		run_runnable_1000ms_9();
		run_runnable_1000ms_10();
		run_runnable_1000ms_11();
		run_runnable_1000ms_12();
		run_runnable_1000ms_13();
		run_runnable_1000ms_14();
		run_runnable_1000ms_15();
		run_runnable_1000ms_16();
		run_runnable_1000ms_17();
		run_runnable_1000ms_18();
		run_runnable_1000ms_19();
		run_runnable_1000ms_20();
		run_runnable_1000ms_21();
		run_runnable_1000ms_22();
		run_runnable_1000ms_23();
		run_runnable_1000ms_24();
		run_runnable_1000ms_25();
		run_runnable_1000ms_26();
		run_runnable_1000ms_27();
		run_runnable_1000ms_28();
		run_runnable_1000ms_29();
		run_runnable_1000ms_30();
		run_runnable_1000ms_31();
		run_runnable_1000ms_32();
		run_runnable_1000ms_33();
		run_runnable_1000ms_34();
		run_runnable_1000ms_35();
		run_runnable_1000ms_36();
		run_runnable_1000ms_37();
		run_runnable_1000ms_38();
		run_runnable_1000ms_39();
		run_runnable_1000ms_40();
		run_runnable_1000ms_41();
		run_runnable_1000ms_42();
		run_runnable_1000ms_43();
	}
	
	
};
