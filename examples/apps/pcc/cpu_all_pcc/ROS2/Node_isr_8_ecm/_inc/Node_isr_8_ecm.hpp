// This code is auto-generated

#include <chrono>
#include <memory>

#include "rclcpp/rclcpp.hpp"
#include "std_msgs/msg/string.hpp"

#include "runnables.hpp"

using namespace std::chrono_literals;
using std::placeholders::_1;
#define CONSOLE_ENABLED

#if defined(_OPENMP)
int check_Node_isr_8_ecm(int *original, int *replicated){ return 1;}
#endif
class Node_isr_8_ecm : public rclcpp::Node
{
	private:

	public:
		Node_isr_8_ecm()
		: Node("node_isr_8_ecm")
		{
		}
	void timer_sporadic_1700us_1800us_isr_8_callback() {
	#ifdef CONSOLE_ENABLED
		std::cout << "Timer_sporadic_1700us_1800us_isr_8_callback (1700us)" << std::endl;
	#endif
		extern int label_6003[1];
		int dummy;
		
		 #if defined(_OPENMP)
		 #pragma omp taskgraph tdg_type(static) nowait
		#endif
		{run_runnable_sporadic_1700us_1800us_0();
		run_runnable_sporadic_1700us_1800us_1();
		run_runnable_sporadic_1700us_1800us_2();
		run_runnable_sporadic_1700us_1800us_3();
		run_runnable_sporadic_1700us_1800us_4();
		
		#if defined(_OPENMP)
		 #pragma omp task replicated(2, dummy, check_Node_isr_8_ecm)
		#elif defined(_OMPSS)
		 #pragma oss task replicated(2, dummy, check_Node_isr_8_ecm)
		#endif
		run_runnable_sporadic_1700us_1800us_5();
		run_runnable_sporadic_1700us_1800us_6();
		}
		#pragma omp taskwait;
	}
	
	
};
