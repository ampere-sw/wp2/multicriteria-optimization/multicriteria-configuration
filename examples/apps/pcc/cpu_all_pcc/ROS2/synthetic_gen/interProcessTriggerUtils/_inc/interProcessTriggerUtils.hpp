// This code is auto-generated

#include <string>

#include "rclcpp/rclcpp.hpp"
#include "std_msgs/msg/string.hpp"

#include "gausian_filter_service/srv/gausian_filter_service.hpp"
void call_service_gausian_filter(rclcpp::Client<gausian_filter_service::srv::GausianFilterService>::SharedPtr& client);

#include "segmentation_service/srv/segmentation_service.hpp"
void call_service_segmentation(rclcpp::Client<segmentation_service::srv::SegmentationService>::SharedPtr& client);

#include "segmentation_to_bckground_service/srv/segmentation_to_bckground_service.hpp"
void call_service_segmentation_to_bckground(rclcpp::Client<segmentation_to_bckground_service::srv::SegmentationToBckgroundService>::SharedPtr& client);

#include "detection_service/srv/detection_service.hpp"
void call_service_detection(rclcpp::Client<detection_service::srv::DetectionService>::SharedPtr& client);

#include "classification_service/srv/classification_service.hpp"
void call_service_classification(rclcpp::Client<classification_service::srv::ClassificationService>::SharedPtr& client);

#include "trigger_trajectory_optimizer_pcc_service/srv/trigger_trajectory_optimizer_pcc_service.hpp"
void call_service_trigger_trajectory_optimizer_pcc(rclcpp::Client<trigger_trajectory_optimizer_pcc_service::srv::TriggerTrajectoryOptimizerPccService>::SharedPtr& client);

#include "trigger_perception_acc_service/srv/trigger_perception_acc_service.hpp"
void call_service_trigger_perception_acc(rclcpp::Client<trigger_perception_acc_service::srv::TriggerPerceptionAccService>::SharedPtr& client);

#include "trigger_world_model_acc_service/srv/trigger_world_model_acc_service.hpp"
void call_service_trigger_world_model_acc(rclcpp::Client<trigger_world_model_acc_service::srv::TriggerWorldModelAccService>::SharedPtr& client);

