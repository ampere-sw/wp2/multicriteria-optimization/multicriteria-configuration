// This code is auto-generated

#include <chrono>
#include <memory>

#include "rclcpp/rclcpp.hpp"
#include "std_msgs/msg/string.hpp"

#include "runnables.hpp"
#include "trigger_perception_acc_service/srv/trigger_perception_acc_service.hpp"

using namespace std::chrono_literals;
using std::placeholders::_1;
#define CONSOLE_ENABLED

#if defined(_OPENMP)
int check_Node_perception_acc(int *original, int *replicated){ return 1;}
#endif
class Node_perception_acc : public rclcpp::Node
{
	private:
		rclcpp::Service<trigger_perception_acc_service::srv::TriggerPerceptionAccService>::SharedPtr trigger_perception_acc_service;
		rclcpp::Client<trigger_world_model_acc_service::srv::TriggerWorldModelAccService>::SharedPtr trigger_world_model_acc_client;

	public:
		Node_perception_acc()
		: Node("node_perception_acc")
		{
			trigger_perception_acc_service = this->create_service<trigger_perception_acc_service::srv::TriggerPerceptionAccService>(
				"trigger_perception_acc_service", 
				std::bind(&Node_perception_acc::trigger_perception_acc_service_callback, this, std::placeholders::_1, std::placeholders::_2));
			trigger_world_model_acc_client =  this->create_client<trigger_world_model_acc_service::srv::TriggerWorldModelAccService>("trigger_world_model_acc_service");
		}
	
	void trigger_perception_acc_service_callback(const std::shared_ptr<trigger_perception_acc_service::srv::TriggerPerceptionAccService::Request> request,
		std::shared_ptr<trigger_perception_acc_service::srv::TriggerPerceptionAccService::Response> response) {
			(void)request;
			(void)response;
	#ifdef CONSOLE_ENABLED
		std::cout << "Starting trigger_perception_acc_service_callback" << std::endl;
	#endif
		extern int label234[1];
		extern int label75[1];
		extern int label21[1];
		extern int label243[1];
		extern int label201[1];
		extern int label160[1];
		extern int label147[1];
		extern int label216[1];
		extern int label229[1];
		extern int label150[1];
		extern int label172[1];
		extern int label100[1];
		extern int label97[1];
		extern int label169[1];
		extern int label61[1];
		extern int label210[1];
		extern int label64[1];
		extern int label261[1];
		extern int label107[301];
		extern int label92[1];
		extern int label168[1];
		extern int label23[1];
		extern int label194[1];
		extern int label154[1];
		extern int label247[1];
		extern int label166[1];
		extern int label89[1];
		extern int label211[1];
		extern int label57[1];
		extern int label164[1];
		extern int label205[1];
		extern int label180[1];
		extern int label44[1];
		extern int label170[1];
		extern int label155[1];
		extern int label10[455];
		extern int label179[1];
		extern int label55[1];
		extern int label223[1];
		extern int label146[1];
		extern int label110[1];
		extern int label119[1];
		extern int label62[1];
		extern int label136[1];
		extern int label103[1];
		extern int label88[1];
		extern int label99[316];
		extern int label151[1];
		extern int label250[1];
		extern int label198[1];
		extern int label185[1];
		extern int label200[1];
		extern int label249[1];
		extern int label140[275];
		extern int label90[306];
		extern int label29[1];
		extern int label13[407];
		extern int label206[1];
		extern int label197[1];
		extern int label161[1];
		extern int label20[1];
		extern int label16[387];
		extern int label95[1];
		extern int label54[285];
		extern int label209[1];
		extern int label236[1];
		extern int label192[1];
		extern int label232[1];
		extern int label221[283];
		extern int label77[1];
		extern int label102[1];
		extern int label112[1];
		extern int label125[1];
		extern int label235[1];
		extern int label189[1];
		extern int label215[1];
		extern int label87[1];
		extern int label245[1];
		extern int label134[1];
		extern int label184[1];
		extern int label175[1];
		extern int label228[1];
		extern int label244[1];
		extern int label254[1];
		extern int label231[1];
		extern int label116[238];
		extern int label133[1];
		extern int label137[1];
		extern int label56[1];
		extern int label108[1];
		extern int label214[1];
		extern int label84[1];
		extern int label149[1];
		extern int label74[1];
		extern int label219[1];
		extern int label109[1];
		extern int label188[1];
		extern int label257[1];
		extern int label191[245];
		extern int label246[254];
		extern int label259[1];
		extern int label204[301];
		extern int label25[486];
		extern int label58[1];
		extern int label31[337];
		extern int label101[1];
		extern int label157[1];
		extern int label86[1];
		extern int label48[1];
		extern int label122[1];
		extern int label174[313];
		extern int label9[1];
		extern int label199[1];
		extern int label91[1];
		extern int label142[1];
		extern int label123[1];
		extern int label138[1];
		extern int label11[1];
		extern int label80[1];
		extern int label38[1];
		extern int label60[1];
		extern int label163[1];
		extern int label264[317];
		extern int label139[1];
		extern int label46[1];
		extern int label145[1];
		extern int label68[1];
		extern int label32[1];
		extern int label52[1];
		extern int label242[1];
		extern int label222[1];
		extern int label104[1];
		extern int label43[1];
		extern int label162[1];
		extern int label118[1];
		extern int label124[246];
		extern int label79[1];
		extern int label252[1];
		extern int label42[1];
		extern int label135[1];
		extern int label114[1];
		extern int label63[281];
		extern int label256[1];
		extern int label263[1];
		extern int label255[219];
		extern int label12[1];
		extern int label224[1];
		extern int label195[1];
		extern int label167[1];
		extern int label177[1];
		extern int label83[1];
		extern int label238[1];
		extern int label131[233];
		extern int label225[1];
		extern int label196[269];
		extern int label94[1];
		extern int label248[1];
		extern int label218[1];
		extern int label251[1];
		extern int label35[1];
		extern int label171[1];
		extern int label121[1];
		extern int label33[1];
		extern int label72[231];
		extern int label260[1];
		extern int label187[1];
		extern int label106[1];
		extern int label156[269];
		extern int label8[1];
		extern int label230[277];
		extern int label34[1];
		extern int label152[1];
		extern int label193[1];
		extern int label51[1];
		extern int label253[1];
		extern int label207[1];
		extern int label141[1];
		extern int label203[1];
		extern int label53[1];
		extern int label132[1];
		extern int label120[1];
		extern int label69[1];
		extern int label241[1];
		extern int label47[1];
		extern int label111[1];
		extern int label93[1];
		extern int label113[1];
		extern int label240[1];
		extern int label233[1];
		extern int label181[1];
		extern int label212[279];
		extern int label237[274];
		extern int label66[1];
		extern int label115[1];
		extern int label208[1];
		extern int label7[412];
		extern int label202[1];
		extern int label37[1];
		extern int label144[1];
		extern int label173[1];
		extern int label28[476];
		extern int label148[270];
		extern int label50[1];
		extern int label178[1];
		extern int label76[1];
		extern int label39[1];
		extern int label81[308];
		extern int label183[229];
		extern int label159[1];
		extern int label15[1];
		extern int label19[436];
		extern int label186[1];
		extern int label129[1];
		extern int label217[1];
		extern int label158[1];
		extern int label227[1];
		extern int label36[1];
		extern int label262[1];
		extern int label117[1];
		extern int label49[297];
		extern int label182[1];
		extern int label40[312];
		extern int label98[1];
		extern int label130[1];
		extern int label105[1];
		extern int label59[1];
		extern int label176[1];
		extern int label143[1];
		extern int label82[1];
		extern int label65[1];
		extern int label128[1];
		extern int label165[271];
		extern int label258[1];
		extern int label239[1];
		extern int label45[1];
		extern int label78[1];
		extern int label226[1];
		extern int label153[1];
		extern int label70[1];
		extern int label6[1];
		extern int label190[1];
		extern int label126[1];
		extern int label14[1];
		extern int label85[1];
		extern int label213[1];
		extern int label96[1];
		extern int label71[1];
		extern int label22[452];
		extern int label73[1];
		extern int label220[1];
		extern int label41[1];
		extern int label67[1];
		extern int label127[1];
		int dummy;
		
		 #if defined(_OPENMP)
		 #pragma omp taskgraph tdg_type(static) nowait
		#endif
		{
		#if defined(_OPENMP)
		 #pragma omp task
		#elif defined(_OMPSS)
		 #pragma oss task
		#endif
		run_run13();
		
		#if defined(_OPENMP)
		 #pragma omp task
		#elif defined(_OMPSS)
		 #pragma oss task
		#endif
		run_run14();
		
		#if defined(_OPENMP)
		 #pragma omp task
		#elif defined(_OMPSS)
		 #pragma oss task
		#endif
		run_run15();
		
		#if defined(_OPENMP)
		 #pragma omp task
		#elif defined(_OMPSS)
		 #pragma oss task
		#endif
		run_run16();
		
		#if defined(_OPENMP)
		 #pragma omp task
		#elif defined(_OMPSS)
		 #pragma oss task
		#endif
		run_run17();
		
		#if defined(_OPENMP)
		 #pragma omp task
		#elif defined(_OMPSS)
		 #pragma oss task
		#endif
		run_run18();
		
		#if defined(_OPENMP)
		 #pragma omp task
		#elif defined(_OMPSS)
		 #pragma oss task
		#endif
		run_run19();
		
		#if defined(_OPENMP)
		 #pragma omp task
		#elif defined(_OMPSS)
		 #pragma oss task
		#endif
		run_run20();
		
		#if defined(_OPENMP)
		 #pragma omp task
		#elif defined(_OMPSS)
		 #pragma oss task
		#endif
		run_run21();
		
		#if defined(_OPENMP)
		 #pragma omp task
		#elif defined(_OMPSS)
		 #pragma oss task
		#endif
		run_run22();
		
		#if defined(_OPENMP)
		 #pragma omp task
		#elif defined(_OMPSS)
		 #pragma oss task
		#endif
		run_run23();
		
		#if defined(_OPENMP)
		 #pragma omp task
		#elif defined(_OMPSS)
		 #pragma oss task
		#endif
		run_run24();
		
		#if defined(_OPENMP)
		 #pragma omp task
		#elif defined(_OMPSS)
		 #pragma oss task
		#endif
		run_run25();
		
		#if defined(_OPENMP)
		 #pragma omp task
		#elif defined(_OMPSS)
		 #pragma oss task
		#endif
		run_run26();
		
		#if defined(_OPENMP)
		 #pragma omp task
		#elif defined(_OMPSS)
		 #pragma oss task
		#endif
		run_run27();
		
		#if defined(_OPENMP)
		 #pragma omp task
		#elif defined(_OMPSS)
		 #pragma oss task
		#endif
		run_run28();
		
		#if defined(_OPENMP)
		 #pragma omp task
		#elif defined(_OMPSS)
		 #pragma oss task
		#endif
		run_run29();
		
		#if defined(_OPENMP)
		 #pragma omp task
		#elif defined(_OMPSS)
		 #pragma oss task
		#endif
		run_run30();
		
		#if defined(_OPENMP)
		 #pragma omp task
		#elif defined(_OMPSS)
		 #pragma oss task
		#endif
		run_run31();
		
		#if defined(_OPENMP)
		 #pragma omp task
		#elif defined(_OMPSS)
		 #pragma oss task
		#endif
		run_run32();
		
		#if defined(_OPENMP)
		 #pragma omp task
		#elif defined(_OMPSS)
		 #pragma oss task
		#endif
		run_run33();
		
		#if defined(_OPENMP)
		 #pragma omp task
		#elif defined(_OMPSS)
		 #pragma oss task
		#endif
		run_run34();
		
		#if defined(_OPENMP)
		 #pragma omp task
		#elif defined(_OMPSS)
		 #pragma oss task
		#endif
		run_run35();
		
		#if defined(_OPENMP)
		 #pragma omp task
		#elif defined(_OMPSS)
		 #pragma oss task
		#endif
		run_run36();
		
		#if defined(_OPENMP)
		 #pragma omp task
		#elif defined(_OMPSS)
		 #pragma oss task
		#endif
		run_run37();
		
		#if defined(_OPENMP)
		 #pragma omp task
		#elif defined(_OMPSS)
		 #pragma oss task
		#endif
		run_run38();
		
		#if defined(_OPENMP)
		 #pragma omp task
		#elif defined(_OMPSS)
		 #pragma oss task
		#endif
		run_run39();
		
		#if defined(_OPENMP)
		 #pragma omp task
		#elif defined(_OMPSS)
		 #pragma oss task
		#endif
		run_run40();
		
		#if defined(_OPENMP)
		 #pragma omp task
		#elif defined(_OMPSS)
		 #pragma oss task
		#endif
		run_run41(trigger_world_model_acc_client);
		}
		#pragma omp taskwait;
	}
	
};
