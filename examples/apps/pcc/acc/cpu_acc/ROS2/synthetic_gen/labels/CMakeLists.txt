# LABELS_LIB ################################################################
####
add_library(LABELS_LIB STATIC
	${CMAKE_CURRENT_LIST_DIR}/_src/labels.cpp
)

target_include_directories(LABELS_LIB
	PUBLIC ${CMAKE_CURRENT_LIST_DIR}/_inc
)	
