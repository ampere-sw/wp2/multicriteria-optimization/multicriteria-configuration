// This code is auto-generated

#include <chrono>
#include <memory>

#include "rclcpp/rclcpp.hpp"
#include "std_msgs/msg/string.hpp"

#include "runnables.hpp"
#include "segmentation_service/srv/segmentation_service.hpp"

using namespace std::chrono_literals;
using std::placeholders::_1;
#define CONSOLE_ENABLED

#if defined(_OPENMP)
int check_Node_segmentation_tsr(int *original, int *replicated){ return 1;}
#endif
class Node_segmentation_tsr : public rclcpp::Node
{
	private:
		rclcpp::Service<segmentation_service::srv::SegmentationService>::SharedPtr segmentation_service;
		rclcpp::Client<segmentation_to_bckground_service::srv::SegmentationToBckgroundService>::SharedPtr segmentation_to_bckground_client;

	public:
		Node_segmentation_tsr()
		: Node("node_segmentation_tsr")
		{
			segmentation_service = this->create_service<segmentation_service::srv::SegmentationService>(
				"segmentation_service", 
				std::bind(&Node_segmentation_tsr::segmentation_service_callback, this, std::placeholders::_1, std::placeholders::_2));
			segmentation_to_bckground_client =  this->create_client<segmentation_to_bckground_service::srv::SegmentationToBckgroundService>("segmentation_to_bckground_service");
		}
	
	void segmentation_service_callback(const std::shared_ptr<segmentation_service::srv::SegmentationService::Request> request,
		std::shared_ptr<segmentation_service::srv::SegmentationService::Response> response) {
			(void)request;
			(void)response;
	#ifdef CONSOLE_ENABLED
		std::cout << "Starting segmentation_service_callback" << std::endl;
	#endif
		run_segmentation(segmentation_to_bckground_client);
	}
	
};
