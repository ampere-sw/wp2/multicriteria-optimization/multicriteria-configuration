// This code is auto-generated

#include <chrono>
#include <memory>

#include "rclcpp/rclcpp.hpp"
#include "std_msgs/msg/string.hpp"

#include "runnables.hpp"

using namespace std::chrono_literals;
using std::placeholders::_1;
#define CONSOLE_ENABLED

#if defined(_OPENMP)
int check_Node_control_behavior_acc(int *original, int *replicated){ return 1;}
#endif
class Node_control_behavior_acc : public rclcpp::Node
{
	private:
		rclcpp::TimerBase::SharedPtr timer_periodic_20ms_;

	public:
		Node_control_behavior_acc()
		: Node("node_control_behavior_acc")
		{
			timer_periodic_20ms_ = this->create_wall_timer(
					20ms, std::bind(&Node_control_behavior_acc::timer_periodic_20ms_callback, this));
		}
	void timer_periodic_20ms_callback() {
	#ifdef CONSOLE_ENABLED
		std::cout << "Timer_periodic_20ms_callback (20ms)" << std::endl;
	#endif
		extern int label343_div[2];
		extern int label443[1];
		extern int label454_div[1];
		extern int label399_div[2];
		extern int label497[1];
		extern int label422[1];
		extern int label471_div[1];
		extern int label367_div[2];
		extern int label353_div[2];
		extern int label300_div[2];
		extern int label486_div[1];
		extern int label475_div[1];
		extern int label434_div[2];
		extern int label397_div[24];
		extern int label316[1];
		extern int label510[1];
		extern int label272[1];
		extern int label446[1];
		extern int label383_div[2];
		extern int label266_div[2];
		extern int label448[1];
		extern int label298[1];
		extern int label400[1];
		extern int label414_div[2];
		extern int label280_div[2];
		extern int label521_div[12];
		extern int label307_div[17];
		extern int label479[1];
		extern int label450_div[1];
		extern int label288_div[18];
		extern int label336[1];
		extern int label433_div[2];
		extern int label422_div[2];
		extern int label472_div[1];
		extern int label477[1];
		extern int label471[1];
		extern int label291[1];
		extern int label420_div[2];
		extern int label312_div[17];
		extern int label456_div[1];
		extern int label505_div[1];
		extern int label447[1];
		extern int label333_div[2];
		extern int label368[1];
		extern int label298_div[17];
		extern int label299[1];
		extern int label440[1];
		extern int label397[1];
		extern int label382[19];
		extern int label503_div[1];
		extern int label322_div[22];
		extern int label442[1];
		extern int label477_div[1];
		extern int label518_div[1];
		extern int label511_div[12];
		extern int label481[1];
		extern int label374[1];
		extern int label498_div[1];
		extern int label501[1];
		extern int label278_div[21];
		extern int label389_div[2];
		extern int label364_div[19];
		extern int label520_div[1];
		extern int label484_div[1];
		extern int label359[1];
		extern int label387_div[21];
		extern int label359_div[16];
		extern int label285_div[2];
		extern int label485_div[1];
		extern int label464_div[16];
		extern int label505[1];
		extern int label377_div[22];
		extern int label314_div[2];
		extern int label439_div[2];
		extern int label453[1];
		extern int label329_div[2];
		extern int label511[1];
		extern int label299_div[2];
		extern int label493[1];
		extern int label373_div[20];
		extern int label398[1];
		extern int label466[1];
		extern int label504[1];
		extern int label502_div[1];
		extern int label519[1];
		extern int label371[1];
		extern int label418[1];
		extern int label502[1];
		extern int label307[1];
		extern int label406_div[20];
		extern int label389[1];
		extern int label462[1];
		extern int label360[1];
		extern int label495[1];
		extern int label491[1];
		extern int label292[1];
		extern int pcc_speed_setpoint[1];
		extern int label513[1];
		extern int label363[1];
		extern int label490_div[1];
		extern int label517[1];
		extern int label460[1];
		extern int label332[1];
		extern int label509_div[1];
		extern int label364[1];
		extern int label508[1];
		extern int label483_div[1];
		extern int label367[1];
		extern int label325[1];
		extern int label496[1];
		extern int label373[1];
		extern int label496_div[15];
		extern int label381[1];
		extern int label416[1];
		extern int label419[2];
		extern int label519_div[1];
		extern int label491_div[1];
		extern int label473_div[1];
		extern int label513_div[1];
		extern int label363_div[2];
		extern int label348[1];
		extern int label411[1];
		extern int label424[2];
		extern int label265_div[2];
		extern int label327[1];
		extern int label379[2];
		extern int label500_div[1];
		extern int label265[1];
		extern int label416_div[24];
		extern int label380_div[2];
		extern int label467[1];
		extern int label492[1];
		extern int label421_div[17];
		extern int label281[1];
		extern int label466_div[1];
		extern int label420[1];
		extern int label292_div[2];
		extern int label475[1];
		extern int label434[1];
		extern int label289_div[2];
		extern int label488[1];
		extern int label289[1];
		extern int label515_div[1];
		extern int label493_div[1];
		extern int label327_div[16];
		extern int label476[1];
		extern int label449[1];
		extern int label272_div[2];
		extern int label447_div[1];
		extern int label283_div[17];
		extern int label445[1];
		extern int label357[1];
		extern int label291_div[2];
		extern int label371_div[2];
		extern int label406[1];
		extern int label515[1];
		extern int label310_div[2];
		extern int label518[1];
		extern int label316_div[2];
		extern int label380[1];
		extern int label498[1];
		extern int label391[2];
		extern int label500[1];
		extern int label278[1];
		extern int label442_div[1];
		extern int label343[1];
		extern int label374_div[2];
		extern int label469[1];
		extern int label438_div[2];
		extern int label425_div[2];
		extern int label368_div[19];
		extern int label273[2];
		extern int label508_div[1];
		extern int label516_div[12];
		extern int label473[1];
		extern int label407_div[2];
		extern int label514[1];
		extern int label399[1];
		extern int label463[1];
		extern int label370[1];
		extern int label474_div[12];
		extern int label453_div[1];
		extern int label476_div[1];
		extern int label507[1];
		extern int label358_div[2];
		extern int label314[1];
		extern int label482[1];
		extern int label353[1];
		extern int label356[1];
		extern int label480[1];
		extern int label325_div[2];
		extern int label512[1];
		extern int label266[1];
		extern int label346_div[19];
		extern int label481_div[1];
		extern int label356_div[2];
		extern int label483[1];
		extern int label384[1];
		extern int label300[1];
		extern int label480_div[1];
		extern int label456[1];
		extern int label458_div[1];
		extern int label315_div[2];
		extern int label366[2];
		extern int label398_div[2];
		extern int label516[1];
		extern int label485[1];
		extern int label467_div[1];
		extern int label404[2];
		extern int label501_div[12];
		extern int label446_div[14];
		extern int label370_div[2];
		extern int label482_div[11];
		extern int label455_div[13];
		extern int label329[1];
		extern int label463_div[1];
		extern int label509[1];
		extern int label522[1];
		extern int label346[1];
		extern int label407[1];
		extern int label341_div[20];
		extern int label522_div[1];
		extern int label433[1];
		extern int label414[1];
		extern int label354[1];
		extern int label507_div[1];
		extern int label460_div[11];
		extern int label411_div[20];
		extern int label448_div[1];
		extern int label469_div[16];
		extern int label445_div[1];
		extern int label358[1];
		extern int label489_div[1];
		extern int label378_div[2];
		extern int label439[1];
		extern int label357_div[2];
		extern int label418_div[2];
		extern int label392[16];
		extern int label312[1];
		extern int label341[1];
		extern int label525_div[16];
		extern int label322[1];
		extern int label402[1];
		extern int label315[1];
		extern int label468_div[1];
		extern int label478_div[14];
		extern int label381_div[2];
		extern int label283[1];
		extern int label384_div[2];
		extern int label470_div[1];
		extern int label405_div[2];
		extern int label444_div[1];
		extern int label354_div[24];
		extern int label478[1];
		extern int label523_div[1];
		extern int label426[18];
		extern int label465_div[1];
		extern int label506[1];
		extern int label462_div[1];
		extern int label452_div[1];
		extern int label520[1];
		extern int label303_div[17];
		extern int label421[1];
		extern int label438[1];
		extern int label497_div[1];
		extern int label310[1];
		extern int label510_div[1];
		extern int label293[1];
		extern int label293_div[18];
		extern int label427_div[2];
		extern int label524[1];
		extern int label461[1];
		extern int label332_div[18];
		extern int label425[1];
		extern int label383[1];
		extern int label486[1];
		extern int label449_div[1];
		extern int label489[1];
		extern int label378[1];
		extern int label512_div[1];
		extern int label494[1];
		extern int label499_div[1];
		extern int label377[1];
		extern int label402_div[20];
		extern int label459_div[1];
		extern int label454[1];
		extern int label405[1];
		extern int label324[2];
		extern int label458[1];
		extern int label336_div[22];
		extern int label488_div[1];
		extern int label525[1];
		extern int label487_div[13];
		extern int label313[1];
		extern int label474[1];
		extern int label468[1];
		extern int label451[1];
		extern int label361[2];
		extern int label339_div[2];
		extern int label487[1];
		extern int label303[1];
		extern int label492_div[14];
		extern int label452[1];
		extern int label427[1];
		extern int label450[1];
		extern int label444[1];
		extern int label490[1];
		extern int label440_div[2];
		extern int label465[1];
		extern int label386[2];
		extern int label506_div[14];
		extern int label400_div[2];
		extern int label441[19];
		extern int label472[1];
		extern int label457_div[1];
		extern int label523[1];
		extern int label317[1];
		extern int label459[1];
		extern int label451_div[1];
		extern int label464[1];
		extern int label455[1];
		extern int label457[1];
		extern int label313_div[2];
		extern int label280[1];
		extern int label348_div[2];
		extern int label288[1];
		extern int label285[1];
		extern int label281_div[2];
		extern int label521[1];
		extern int label499[1];
		extern int label301[1];
		extern int label317_div[16];
		extern int label333[1];
		extern int label495_div[1];
		extern int label517_div[1];
		extern int label387[1];
		extern int label339[1];
		extern int label470[1];
		extern int label461_div[1];
		extern int label360_div[2];
		extern int label503[1];
		extern int label301_div[2];
		extern int label484[1];
		int dummy;
		
		 #if defined(_OPENMP)
		 #pragma omp taskgraph tdg_type(static) nowait
		#endif
		{
		#if defined(_OPENMP)
		 #pragma omp task depend(out:label465,label497,label444,label461,label452,label449,label462,label480,label523,label517,label502)
		#elif defined(_OMPSS)
		 #pragma oss task depend(out:label465,label497,label444,label461,label452,label449,label462,label480,label523,label517,label502)
		#endif
		run_run80();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:label444) depend(out:label446)
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:label444) depend(out:label446)
		#endif
		run_run81();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:label446,label449) depend(out:label450)
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:label446,label449) depend(out:label450)
		#endif
		run_run82();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:label452)
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:label452)
		#endif
		run_run83();
		
		#if defined(_OPENMP)
		 #pragma omp task
		#elif defined(_OMPSS)
		 #pragma oss task
		#endif
		run_run84();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:label450,label461,label462)
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:label450,label461,label462)
		#endif
		run_run85();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:label465) depend(out:label466)
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:label465) depend(out:label466)
		#endif
		run_run86();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(out:label472)
		#elif defined(_OMPSS)
		 #pragma oss task depend(out:label472)
		#endif
		run_run87();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:label472)
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:label472)
		#endif
		run_run88();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:label466,label480)
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:label466,label480)
		#endif
		run_run89();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(out:label485)
		#elif defined(_OMPSS)
		 #pragma oss task depend(out:label485)
		#endif
		run_run90();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(out:label490)
		#elif defined(_OMPSS)
		 #pragma oss task depend(out:label490)
		#endif
		run_run91();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:label490)
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:label490)
		#endif
		run_run92();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:label497)
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:label497)
		#endif
		run_run93();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:label502)
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:label502)
		#endif
		run_run94();
		
		#if defined(_OPENMP)
		 #pragma omp task
		#elif defined(_OMPSS)
		 #pragma oss task
		#endif
		run_run95();
		
		#if defined(_OPENMP)
		 #pragma omp task
		#elif defined(_OMPSS)
		 #pragma oss task
		#endif
		run_run96();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:label517)
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:label517)
		#endif
		run_run97();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:label485,label523)
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:label485,label523)
		#endif
		run_run98();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(out:label502_div,label480_div,label523_div,label497_div,label444_div,label461_div,label462_div,label449_div,label517_div)
		#elif defined(_OMPSS)
		 #pragma oss task depend(out:label502_div,label480_div,label523_div,label497_div,label444_div,label461_div,label462_div,label449_div,label517_div)
		#endif
		run_run80_div();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:label444_div) depend(out:label446_div)
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:label444_div) depend(out:label446_div)
		#endif
		run_run81_div();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:label446_div,label449_div) depend(out:label450_div)
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:label446_div,label449_div) depend(out:label450_div)
		#endif
		run_run82_div();
		
		#if defined(_OPENMP)
		 #pragma omp task
		#elif defined(_OMPSS)
		 #pragma oss task
		#endif
		run_run83_div();
		
		#if defined(_OPENMP)
		 #pragma omp task
		#elif defined(_OMPSS)
		 #pragma oss task
		#endif
		run_run84_div();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:label461_div,label450_div,label462_div)
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:label461_div,label450_div,label462_div)
		#endif
		run_run85_div();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(out:label466_div)
		#elif defined(_OMPSS)
		 #pragma oss task depend(out:label466_div)
		#endif
		run_run86_div();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(out:label472_div)
		#elif defined(_OMPSS)
		 #pragma oss task depend(out:label472_div)
		#endif
		run_run87_div();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:label472_div)
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:label472_div)
		#endif
		run_run88_div();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:label480_div,label466_div)
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:label480_div,label466_div)
		#endif
		run_run89_div();
		
		#if defined(_OPENMP)
		 #pragma omp task
		#elif defined(_OMPSS)
		 #pragma oss task
		#endif
		run_run90_div();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(out:label490_div)
		#elif defined(_OMPSS)
		 #pragma oss task depend(out:label490_div)
		#endif
		run_run91_div();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:label490_div)
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:label490_div)
		#endif
		run_run92_div();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:label497_div)
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:label497_div)
		#endif
		run_run93_div();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:label502_div)
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:label502_div)
		#endif
		run_run94_div();
		
		#if defined(_OPENMP)
		 #pragma omp task
		#elif defined(_OMPSS)
		 #pragma oss task
		#endif
		run_run95_div();
		
		#if defined(_OPENMP)
		 #pragma omp task
		#elif defined(_OMPSS)
		 #pragma oss task
		#endif
		run_run96_div();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:label517_div)
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:label517_div)
		#endif
		run_run97_div();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:label523_div)
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:label523_div)
		#endif
		run_run98_div();
		
		#if defined(_OPENMP)
		 #pragma omp task
		#elif defined(_OMPSS)
		 #pragma oss task
		#endif
		run_acc_databroker();
		}
		#pragma omp taskwait;
	}
	
	
};
