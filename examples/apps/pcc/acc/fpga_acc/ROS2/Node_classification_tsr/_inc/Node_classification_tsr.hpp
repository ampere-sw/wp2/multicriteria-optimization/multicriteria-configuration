// This code is auto-generated

#include <chrono>
#include <memory>

#include "rclcpp/rclcpp.hpp"
#include "std_msgs/msg/string.hpp"

#include "classification_service/srv/classification_service.hpp"
#include "runnables.hpp"

using namespace std::chrono_literals;
using std::placeholders::_1;
#define CONSOLE_ENABLED

#if defined(_OPENMP)
int check_Node_classification_tsr(int *original, int *replicated){ return 1;}
#endif
class Node_classification_tsr : public rclcpp::Node
{
	private:
		rclcpp::Service<classification_service::srv::ClassificationService>::SharedPtr classification_service;

	public:
		Node_classification_tsr()
		: Node("node_classification_tsr")
		{
			classification_service = this->create_service<classification_service::srv::ClassificationService>(
				"classification_service", 
				std::bind(&Node_classification_tsr::classification_service_callback, this, std::placeholders::_1, std::placeholders::_2));
		}
	
	void classification_service_callback(const std::shared_ptr<classification_service::srv::ClassificationService::Request> request,
		std::shared_ptr<classification_service::srv::ClassificationService::Response> response) {
			(void)request;
			(void)response;
	#ifdef CONSOLE_ENABLED
		std::cout << "Starting classification_service_callback" << std::endl;
	#endif
		run_classification1();
		run_classification2();
		run_classification3();
		run_classification4();
		run_classification5();
		run_classification6();
		run_classification7();
		run_classification8();
		run_classification9();
		run_classification10();
	}
	
};
