#include "Node_control_behavior_acc.hpp"
#include "Node_databroker_acc.hpp"
#include "Node_input_acc.hpp"
#include "Node_output_acc.hpp"
#include "Node_perception_acc.hpp"
#include "Node_preprocessing_acc.hpp"
#include "Node_world_model_acc.hpp"
#include <extrae.h>
int main(int argc, char *argv[])
{
	std::vector<std::thread> threads;
	Extrae_init();
	setvbuf(stdout, NULL, _IONBF, BUFSIZ);
	rclcpp::init(argc, argv);

//  - acc_input : periodic 5ms
//  - acc_preprocessing :  periodic 66ms
//  - acc_perception : stimulus "trigger_perception_acc"
//  - acc_world_model : stimulus "trigger_world_model_acc"
//  - acc_control : periodic 20ms
//  - acc_output : periodic 5ms

	auto input = new Node_input_acc();
	auto preprocessing = new Node_preprocessing_acc();
	auto perception = new Node_perception_acc();
	auto world_model = new Node_world_model_acc();
	auto control = new Node_control_behavior_acc();
	auto output = new Node_output_acc();
	
	int NUM_ITER = 10;
	for (int i = 0; i< NUM_ITER; i++){
		#pragma omp parallel
		#pragma omp single
		input->timer_5mstrigger_callback();
		
		#pragma omp parallel
		#pragma omp single
		preprocessing->timer_66mstrigger_callback();
		
		#pragma omp parallel
		#pragma omp single
		perception->trigger_perception_acc_service_callback(NULL,NULL);
		
		#pragma omp parallel
		#pragma omp single
		world_model->trigger_world_model_acc_service_callback(NULL,NULL);
		
		#pragma omp parallel
		#pragma omp single
		control->timer_periodic_20ms_callback();
		
		#pragma omp parallel
		#pragma omp single
		output->timer_5mstrigger_callback();
	}
	rclcpp::shutdown();
	Extrae_fini();
	return 0;
}