// This code is auto-generated

#include <chrono>
#include <memory>

#include "rclcpp/rclcpp.hpp"
#include "std_msgs/msg/string.hpp"

#include "detection_service/srv/detection_service.hpp"
#include "runnables.hpp"

using namespace std::chrono_literals;
using std::placeholders::_1;
#define CONSOLE_ENABLED

#if defined(_OPENMP)
int check_Node_detection_tsr(int *original, int *replicated){ return 1;}
#endif
class Node_detection_tsr : public rclcpp::Node
{
	private:
		rclcpp::Service<detection_service::srv::DetectionService>::SharedPtr detection_service;
		rclcpp::Client<classification_service::srv::ClassificationService>::SharedPtr classification_client;

	public:
		Node_detection_tsr()
		: Node("node_detection_tsr")
		{
			detection_service = this->create_service<detection_service::srv::DetectionService>(
				"detection_service", 
				std::bind(&Node_detection_tsr::detection_service_callback, this, std::placeholders::_1, std::placeholders::_2));
			classification_client =  this->create_client<classification_service::srv::ClassificationService>("classification_service");
		}
	
	void detection_service_callback(const std::shared_ptr<detection_service::srv::DetectionService::Request> request,
		std::shared_ptr<detection_service::srv::DetectionService::Response> response) {
			(void)request;
			(void)response;
	#ifdef CONSOLE_ENABLED
		std::cout << "Starting detection_service_callback" << std::endl;
	#endif
		int dummy;
		
		 #if defined(_OPENMP)
		 #pragma omp taskgraph tdg_type(static) nowait
		#endif
		{
		#if defined(_OPENMP)
		 #pragma omp task
		#elif defined(_OMPSS)
		 #pragma oss task
		#endif
		run_detection(classification_client);
		}
		#pragma omp taskwait;
	}
	
};
