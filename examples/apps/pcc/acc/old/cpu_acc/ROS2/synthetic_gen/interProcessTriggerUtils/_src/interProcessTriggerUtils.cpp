// This code is auto-generated

#include "interProcessTriggerUtils.hpp"

using namespace std::chrono_literals;

void call_service_gausian_filter(rclcpp::Client<gausian_filter_service::srv::GausianFilterService>::SharedPtr& client) {
	
	auto request = std::make_shared<gausian_filter_service::srv::GausianFilterService::Request>();
	
	while (!client->wait_for_service(50ms)) {
		if (!rclcpp::ok()) {
			RCLCPP_ERROR(rclcpp::get_logger("rclcpp"), "Interrupted while waiting for the service. Exiting.");
			exit;
		}
		RCLCPP_INFO(rclcpp::get_logger("rclcpp"), "service not available, waiting again...");
	}
	auto result = client->async_send_request(request);	
}

void call_service_segmentation(rclcpp::Client<segmentation_service::srv::SegmentationService>::SharedPtr& client) {
	
	auto request = std::make_shared<segmentation_service::srv::SegmentationService::Request>();
	
	while (!client->wait_for_service(50ms)) {
		if (!rclcpp::ok()) {
			RCLCPP_ERROR(rclcpp::get_logger("rclcpp"), "Interrupted while waiting for the service. Exiting.");
			exit;
		}
		RCLCPP_INFO(rclcpp::get_logger("rclcpp"), "service not available, waiting again...");
	}
	auto result = client->async_send_request(request);	
}

void call_service_segmentation_to_bckground(rclcpp::Client<segmentation_to_bckground_service::srv::SegmentationToBckgroundService>::SharedPtr& client) {
	
	auto request = std::make_shared<segmentation_to_bckground_service::srv::SegmentationToBckgroundService::Request>();
	
	while (!client->wait_for_service(50ms)) {
		if (!rclcpp::ok()) {
			RCLCPP_ERROR(rclcpp::get_logger("rclcpp"), "Interrupted while waiting for the service. Exiting.");
			exit;
		}
		RCLCPP_INFO(rclcpp::get_logger("rclcpp"), "service not available, waiting again...");
	}
	auto result = client->async_send_request(request);	
}

void call_service_detection(rclcpp::Client<detection_service::srv::DetectionService>::SharedPtr& client) {
	
	auto request = std::make_shared<detection_service::srv::DetectionService::Request>();
	
	while (!client->wait_for_service(50ms)) {
		if (!rclcpp::ok()) {
			RCLCPP_ERROR(rclcpp::get_logger("rclcpp"), "Interrupted while waiting for the service. Exiting.");
			exit;
		}
		RCLCPP_INFO(rclcpp::get_logger("rclcpp"), "service not available, waiting again...");
	}
	auto result = client->async_send_request(request);	
}

void call_service_classification(rclcpp::Client<classification_service::srv::ClassificationService>::SharedPtr& client) {
	
	auto request = std::make_shared<classification_service::srv::ClassificationService::Request>();
	
	while (!client->wait_for_service(50ms)) {
		if (!rclcpp::ok()) {
			RCLCPP_ERROR(rclcpp::get_logger("rclcpp"), "Interrupted while waiting for the service. Exiting.");
			exit;
		}
		RCLCPP_INFO(rclcpp::get_logger("rclcpp"), "service not available, waiting again...");
	}
	auto result = client->async_send_request(request);	
}

void call_service_trigger_trajectory_optimizer_pcc(rclcpp::Client<trigger_trajectory_optimizer_pcc_service::srv::TriggerTrajectoryOptimizerPccService>::SharedPtr& client) {
	
	auto request = std::make_shared<trigger_trajectory_optimizer_pcc_service::srv::TriggerTrajectoryOptimizerPccService::Request>();
	
	while (!client->wait_for_service(50ms)) {
		if (!rclcpp::ok()) {
			RCLCPP_ERROR(rclcpp::get_logger("rclcpp"), "Interrupted while waiting for the service. Exiting.");
			exit;
		}
		RCLCPP_INFO(rclcpp::get_logger("rclcpp"), "service not available, waiting again...");
	}
	auto result = client->async_send_request(request);	
}

void call_service_trigger_perception_acc(rclcpp::Client<trigger_perception_acc_service::srv::TriggerPerceptionAccService>::SharedPtr& client) {
	
	auto request = std::make_shared<trigger_perception_acc_service::srv::TriggerPerceptionAccService::Request>();
	
	while (!client->wait_for_service(50ms)) {
		if (!rclcpp::ok()) {
			RCLCPP_ERROR(rclcpp::get_logger("rclcpp"), "Interrupted while waiting for the service. Exiting.");
			exit;
		}
		RCLCPP_INFO(rclcpp::get_logger("rclcpp"), "service not available, waiting again...");
	}
	auto result = client->async_send_request(request);	
}

void call_service_trigger_world_model_acc(rclcpp::Client<trigger_world_model_acc_service::srv::TriggerWorldModelAccService>::SharedPtr& client) {
	
	auto request = std::make_shared<trigger_world_model_acc_service::srv::TriggerWorldModelAccService::Request>();
	
	while (!client->wait_for_service(50ms)) {
		if (!rclcpp::ok()) {
			RCLCPP_ERROR(rclcpp::get_logger("rclcpp"), "Interrupted while waiting for the service. Exiting.");
			exit;
		}
		RCLCPP_INFO(rclcpp::get_logger("rclcpp"), "service not available, waiting again...");
	}
	auto result = client->async_send_request(request);	
}
