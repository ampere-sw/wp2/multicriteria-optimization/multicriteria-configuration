// This code is auto-generated

#include <chrono>
#include <memory>

#include "rclcpp/rclcpp.hpp"
#include "std_msgs/msg/string.hpp"

#include "runnables.hpp"

using namespace std::chrono_literals;
using std::placeholders::_1;
#define CONSOLE_ENABLED

#if defined(_OPENMP)
int check_Node_output_tsr(int *original, int *replicated){ return 1;}
#endif
class Node_output_tsr : public rclcpp::Node
{
	private:
		rclcpp::TimerBase::SharedPtr timer_output_;
		rclcpp::Publisher<std_msgs::msg::String>::SharedPtr recognizedspeedlimit_publisher;

	public:
		Node_output_tsr()
		: Node("node_output_tsr")
		{
			timer_output_ = this->create_wall_timer(
					5ms, std::bind(&Node_output_tsr::timer_output_callback, this));
			recognizedspeedlimit_publisher = this->create_publisher<std_msgs::msg::String>("recognizedspeedlimit", 10);
		}
	void timer_output_callback() {
	#ifdef CONSOLE_ENABLED
		std::cout << "Timer_output_callback (5ms)" << std::endl;
	#endif
		run_output(recognizedspeedlimit_publisher);
	}
	
	
};
