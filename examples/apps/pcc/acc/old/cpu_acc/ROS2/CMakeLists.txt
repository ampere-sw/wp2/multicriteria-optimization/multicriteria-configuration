# CMakeLists for Nodes
cmake_minimum_required(VERSION 3.5)
project(amalthea_ros2_model)

# Default to C++14
if(NOT CMAKE_CXX_STANDARD)
	set(CMAKE_CXX_STANDARD 14)
endif()

if(CMAKE_COMPILER_IS_GNUCXX OR CMAKE_CXX_COMPILER_ID MATCHES "Clang")
	add_compile_options(-Wall -Wextra -Wpedantic)
endif()

include_directories($ENV{EXTRAE_HOME}/include)
link_directories($ENV{EXTRAE_HOME}/lib)

find_package(ament_cmake REQUIRED)
find_package(rclcpp REQUIRED)
find_package(std_msgs REQUIRED)
find_package(OpenMP)

if(OPENMP_FOUND)
    set (CMAKE_C_FLAGS "${CMAKE_C_FLAGS} ${OpenMP_C_FLAGS} -fopenmp-taskgraph -static-tdg")
    set (CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${OpenMP_CXX_FLAGS} -fopenmp-taskgraph -static-tdg")
    set (CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} ${OpenMP_EXE_LINKER_FLAGS}")
endif()
find_package(classification_service REQUIRED)
find_package(detection_service REQUIRED)
find_package(gausian_filter_service REQUIRED)
find_package(segmentation_service REQUIRED)
find_package(segmentation_to_bckground_service REQUIRED)
find_package(trigger_perception_acc_service REQUIRED)
find_package(trigger_trajectory_optimizer_pcc_service REQUIRED)
find_package(trigger_world_model_acc_service REQUIRED)

add_library(CHANNELSEND_UTILS  STATIC
	synthetic_gen/channelSendUtils/_src/channelSendUtils.cpp
)
ament_target_dependencies(CHANNELSEND_UTILS rclcpp std_msgs classification_service detection_service gausian_filter_service segmentation_service segmentation_to_bckground_service trigger_perception_acc_service trigger_trajectory_optimizer_pcc_service trigger_world_model_acc_service)
target_include_directories(CHANNELSEND_UTILS
    PUBLIC synthetic_gen/channelSendUtils/_inc/
)

add_library(INTERPROCESSTRIGGER_UTIL STATIC
	  synthetic_gen/interProcessTriggerUtils/_src/interProcessTriggerUtils.cpp
)
ament_target_dependencies(INTERPROCESSTRIGGER_UTIL rclcpp std_msgs classification_service detection_service gausian_filter_service segmentation_service segmentation_to_bckground_service trigger_perception_acc_service trigger_trajectory_optimizer_pcc_service trigger_world_model_acc_service)
target_include_directories(INTERPROCESSTRIGGER_UTIL
	PUBLIC synthetic_gen/interProcessTriggerUtils/_inc
)

add_subdirectory (synthetic_gen/labels)
add_subdirectory (synthetic_gen/ticksUtils)

# RUNNABLES_LIB ################################################################

####
add_library(RUNNABLES_LIB STATIC
	synthetic_gen/runnables/_src/runnables.cpp
)

target_include_directories(RUNNABLES_LIB
	PUBLIC synthetic_gen/runnables/_inc
)	

target_include_directories(RUNNABLES_LIB
	PUBLIC 
	synthetic_gen/ticksUtils/_inc
	synthetic_gen/labels/_inc
	synthetic_gen/channelSendUtils/_inc
	synthetic_gen/interProcessTriggerUtils/_inc
)
target_link_libraries(RUNNABLES_LIB
	PRIVATE LABELS_LIB TICKS_UTILS CHANNELSEND_UTILS INTERPROCESSTRIGGER_UTIL
)


# ********** Node Node_classification_tsr **********

add_library(Node_classification_tsr_LIB STATIC
	Node_classification_tsr/_inc/Node_classification_tsr.hpp
)

target_include_directories(Node_classification_tsr_LIB
	PUBLIC
	synthetic_gen/runnables/_inc
	synthetic_gen/ticksUtils/_inc
	synthetic_gen/labels/_inc
	synthetic_gen/channelSendUtils/_inc
	synthetic_gen/interProcessTriggerUtils/_inc
)

target_link_libraries(Node_classification_tsr_LIB 
	PRIVATE  RUNNABLES_LIB LABELS_LIB TICKS_UTILS CHANNELSEND_UTILS INTERPROCESSTRIGGER_UTIL
)


# ********** Node Node_cominput_tsr **********

add_library(Node_cominput_tsr_LIB STATIC
	Node_cominput_tsr/_inc/Node_cominput_tsr.hpp
)

target_include_directories(Node_cominput_tsr_LIB
	PUBLIC
	synthetic_gen/runnables/_inc
	synthetic_gen/ticksUtils/_inc
	synthetic_gen/labels/_inc
	synthetic_gen/channelSendUtils/_inc
	synthetic_gen/interProcessTriggerUtils/_inc
)

target_link_libraries(Node_cominput_tsr_LIB 
	PRIVATE  RUNNABLES_LIB LABELS_LIB TICKS_UTILS CHANNELSEND_UTILS INTERPROCESSTRIGGER_UTIL
)


# ********** Node Node_control_behavior_acc **********

add_library(Node_control_behavior_acc_LIB STATIC
	Node_control_behavior_acc/_inc/Node_control_behavior_acc.hpp
)

target_include_directories(Node_control_behavior_acc_LIB
	PUBLIC
	synthetic_gen/runnables/_inc
	synthetic_gen/ticksUtils/_inc
	synthetic_gen/labels/_inc
	synthetic_gen/channelSendUtils/_inc
	synthetic_gen/interProcessTriggerUtils/_inc
)

target_link_libraries(Node_control_behavior_acc_LIB 
	PRIVATE  RUNNABLES_LIB LABELS_LIB TICKS_UTILS CHANNELSEND_UTILS INTERPROCESSTRIGGER_UTIL
)


# ********** Node Node_databroker_acc **********

add_library(Node_databroker_acc_LIB STATIC
	Node_databroker_acc/_inc/Node_databroker_acc.hpp
)

target_include_directories(Node_databroker_acc_LIB
	PUBLIC
	synthetic_gen/runnables/_inc
	synthetic_gen/ticksUtils/_inc
	synthetic_gen/labels/_inc
	synthetic_gen/channelSendUtils/_inc
	synthetic_gen/interProcessTriggerUtils/_inc
)

target_link_libraries(Node_databroker_acc_LIB 
	PRIVATE  RUNNABLES_LIB LABELS_LIB TICKS_UTILS CHANNELSEND_UTILS INTERPROCESSTRIGGER_UTIL
)


# ********** Node Node_databroker_ecm **********

add_library(Node_databroker_ecm_LIB STATIC
	Node_databroker_ecm/_inc/Node_databroker_ecm.hpp
)

target_include_directories(Node_databroker_ecm_LIB
	PUBLIC
	synthetic_gen/runnables/_inc
	synthetic_gen/ticksUtils/_inc
	synthetic_gen/labels/_inc
	synthetic_gen/channelSendUtils/_inc
	synthetic_gen/interProcessTriggerUtils/_inc
)

target_link_libraries(Node_databroker_ecm_LIB 
	PRIVATE  RUNNABLES_LIB LABELS_LIB TICKS_UTILS CHANNELSEND_UTILS INTERPROCESSTRIGGER_UTIL
)


# ********** Node Node_databroker_pcc **********

add_library(Node_databroker_pcc_LIB STATIC
	Node_databroker_pcc/_inc/Node_databroker_pcc.hpp
)

target_include_directories(Node_databroker_pcc_LIB
	PUBLIC
	synthetic_gen/runnables/_inc
	synthetic_gen/ticksUtils/_inc
	synthetic_gen/labels/_inc
	synthetic_gen/channelSendUtils/_inc
	synthetic_gen/interProcessTriggerUtils/_inc
)

target_link_libraries(Node_databroker_pcc_LIB 
	PRIVATE  RUNNABLES_LIB LABELS_LIB TICKS_UTILS CHANNELSEND_UTILS INTERPROCESSTRIGGER_UTIL
)


# ********** Node Node_detection_tsr **********

add_library(Node_detection_tsr_LIB STATIC
	Node_detection_tsr/_inc/Node_detection_tsr.hpp
)

target_include_directories(Node_detection_tsr_LIB
	PUBLIC
	synthetic_gen/runnables/_inc
	synthetic_gen/ticksUtils/_inc
	synthetic_gen/labels/_inc
	synthetic_gen/channelSendUtils/_inc
	synthetic_gen/interProcessTriggerUtils/_inc
)

target_link_libraries(Node_detection_tsr_LIB 
	PRIVATE  RUNNABLES_LIB LABELS_LIB TICKS_UTILS CHANNELSEND_UTILS INTERPROCESSTRIGGER_UTIL
)


# ********** Node Node_gaussian_filter_tsr **********

add_library(Node_gaussian_filter_tsr_LIB STATIC
	Node_gaussian_filter_tsr/_inc/Node_gaussian_filter_tsr.hpp
)

target_include_directories(Node_gaussian_filter_tsr_LIB
	PUBLIC
	synthetic_gen/runnables/_inc
	synthetic_gen/ticksUtils/_inc
	synthetic_gen/labels/_inc
	synthetic_gen/channelSendUtils/_inc
	synthetic_gen/interProcessTriggerUtils/_inc
)

target_link_libraries(Node_gaussian_filter_tsr_LIB 
	PRIVATE  RUNNABLES_LIB LABELS_LIB TICKS_UTILS CHANNELSEND_UTILS INTERPROCESSTRIGGER_UTIL
)


# ********** Node Node_input_acc **********

add_library(Node_input_acc_LIB STATIC
	Node_input_acc/_inc/Node_input_acc.hpp
)

target_include_directories(Node_input_acc_LIB
	PUBLIC
	synthetic_gen/runnables/_inc
	synthetic_gen/ticksUtils/_inc
	synthetic_gen/labels/_inc
	synthetic_gen/channelSendUtils/_inc
	synthetic_gen/interProcessTriggerUtils/_inc
)

target_link_libraries(Node_input_acc_LIB 
	PRIVATE  RUNNABLES_LIB LABELS_LIB TICKS_UTILS CHANNELSEND_UTILS INTERPROCESSTRIGGER_UTIL
)


# ********** Node Node_isr_10_ecm **********

add_library(Node_isr_10_ecm_LIB STATIC
	Node_isr_10_ecm/_inc/Node_isr_10_ecm.hpp
)

target_include_directories(Node_isr_10_ecm_LIB
	PUBLIC
	synthetic_gen/runnables/_inc
	synthetic_gen/ticksUtils/_inc
	synthetic_gen/labels/_inc
	synthetic_gen/channelSendUtils/_inc
	synthetic_gen/interProcessTriggerUtils/_inc
)

target_link_libraries(Node_isr_10_ecm_LIB 
	PRIVATE  RUNNABLES_LIB LABELS_LIB TICKS_UTILS CHANNELSEND_UTILS INTERPROCESSTRIGGER_UTIL
)


# ********** Node Node_isr_11_ecm **********

add_library(Node_isr_11_ecm_LIB STATIC
	Node_isr_11_ecm/_inc/Node_isr_11_ecm.hpp
)

target_include_directories(Node_isr_11_ecm_LIB
	PUBLIC
	synthetic_gen/runnables/_inc
	synthetic_gen/ticksUtils/_inc
	synthetic_gen/labels/_inc
	synthetic_gen/channelSendUtils/_inc
	synthetic_gen/interProcessTriggerUtils/_inc
)

target_link_libraries(Node_isr_11_ecm_LIB 
	PRIVATE  RUNNABLES_LIB LABELS_LIB TICKS_UTILS CHANNELSEND_UTILS INTERPROCESSTRIGGER_UTIL
)


# ********** Node Node_isr_1_ecm **********

add_library(Node_isr_1_ecm_LIB STATIC
	Node_isr_1_ecm/_inc/Node_isr_1_ecm.hpp
)

target_include_directories(Node_isr_1_ecm_LIB
	PUBLIC
	synthetic_gen/runnables/_inc
	synthetic_gen/ticksUtils/_inc
	synthetic_gen/labels/_inc
	synthetic_gen/channelSendUtils/_inc
	synthetic_gen/interProcessTriggerUtils/_inc
)

target_link_libraries(Node_isr_1_ecm_LIB 
	PRIVATE  RUNNABLES_LIB LABELS_LIB TICKS_UTILS CHANNELSEND_UTILS INTERPROCESSTRIGGER_UTIL
)


# ********** Node Node_isr_2_ecm **********

add_library(Node_isr_2_ecm_LIB STATIC
	Node_isr_2_ecm/_inc/Node_isr_2_ecm.hpp
)

target_include_directories(Node_isr_2_ecm_LIB
	PUBLIC
	synthetic_gen/runnables/_inc
	synthetic_gen/ticksUtils/_inc
	synthetic_gen/labels/_inc
	synthetic_gen/channelSendUtils/_inc
	synthetic_gen/interProcessTriggerUtils/_inc
)

target_link_libraries(Node_isr_2_ecm_LIB 
	PRIVATE  RUNNABLES_LIB LABELS_LIB TICKS_UTILS CHANNELSEND_UTILS INTERPROCESSTRIGGER_UTIL
)


# ********** Node Node_isr_3_ecm **********

add_library(Node_isr_3_ecm_LIB STATIC
	Node_isr_3_ecm/_inc/Node_isr_3_ecm.hpp
)

target_include_directories(Node_isr_3_ecm_LIB
	PUBLIC
	synthetic_gen/runnables/_inc
	synthetic_gen/ticksUtils/_inc
	synthetic_gen/labels/_inc
	synthetic_gen/channelSendUtils/_inc
	synthetic_gen/interProcessTriggerUtils/_inc
)

target_link_libraries(Node_isr_3_ecm_LIB 
	PRIVATE  RUNNABLES_LIB LABELS_LIB TICKS_UTILS CHANNELSEND_UTILS INTERPROCESSTRIGGER_UTIL
)


# ********** Node Node_isr_4_ecm **********

add_library(Node_isr_4_ecm_LIB STATIC
	Node_isr_4_ecm/_inc/Node_isr_4_ecm.hpp
)

target_include_directories(Node_isr_4_ecm_LIB
	PUBLIC
	synthetic_gen/runnables/_inc
	synthetic_gen/ticksUtils/_inc
	synthetic_gen/labels/_inc
	synthetic_gen/channelSendUtils/_inc
	synthetic_gen/interProcessTriggerUtils/_inc
)

target_link_libraries(Node_isr_4_ecm_LIB 
	PRIVATE  RUNNABLES_LIB LABELS_LIB TICKS_UTILS CHANNELSEND_UTILS INTERPROCESSTRIGGER_UTIL
)


# ********** Node Node_isr_5_ecm **********

add_library(Node_isr_5_ecm_LIB STATIC
	Node_isr_5_ecm/_inc/Node_isr_5_ecm.hpp
)

target_include_directories(Node_isr_5_ecm_LIB
	PUBLIC
	synthetic_gen/runnables/_inc
	synthetic_gen/ticksUtils/_inc
	synthetic_gen/labels/_inc
	synthetic_gen/channelSendUtils/_inc
	synthetic_gen/interProcessTriggerUtils/_inc
)

target_link_libraries(Node_isr_5_ecm_LIB 
	PRIVATE  RUNNABLES_LIB LABELS_LIB TICKS_UTILS CHANNELSEND_UTILS INTERPROCESSTRIGGER_UTIL
)


# ********** Node Node_isr_6_ecm **********

add_library(Node_isr_6_ecm_LIB STATIC
	Node_isr_6_ecm/_inc/Node_isr_6_ecm.hpp
)

target_include_directories(Node_isr_6_ecm_LIB
	PUBLIC
	synthetic_gen/runnables/_inc
	synthetic_gen/ticksUtils/_inc
	synthetic_gen/labels/_inc
	synthetic_gen/channelSendUtils/_inc
	synthetic_gen/interProcessTriggerUtils/_inc
)

target_link_libraries(Node_isr_6_ecm_LIB 
	PRIVATE  RUNNABLES_LIB LABELS_LIB TICKS_UTILS CHANNELSEND_UTILS INTERPROCESSTRIGGER_UTIL
)


# ********** Node Node_isr_7_ecm **********

add_library(Node_isr_7_ecm_LIB STATIC
	Node_isr_7_ecm/_inc/Node_isr_7_ecm.hpp
)

target_include_directories(Node_isr_7_ecm_LIB
	PUBLIC
	synthetic_gen/runnables/_inc
	synthetic_gen/ticksUtils/_inc
	synthetic_gen/labels/_inc
	synthetic_gen/channelSendUtils/_inc
	synthetic_gen/interProcessTriggerUtils/_inc
)

target_link_libraries(Node_isr_7_ecm_LIB 
	PRIVATE  RUNNABLES_LIB LABELS_LIB TICKS_UTILS CHANNELSEND_UTILS INTERPROCESSTRIGGER_UTIL
)


# ********** Node Node_isr_8_ecm **********

add_library(Node_isr_8_ecm_LIB STATIC
	Node_isr_8_ecm/_inc/Node_isr_8_ecm.hpp
)

target_include_directories(Node_isr_8_ecm_LIB
	PUBLIC
	synthetic_gen/runnables/_inc
	synthetic_gen/ticksUtils/_inc
	synthetic_gen/labels/_inc
	synthetic_gen/channelSendUtils/_inc
	synthetic_gen/interProcessTriggerUtils/_inc
)

target_link_libraries(Node_isr_8_ecm_LIB 
	PRIVATE  RUNNABLES_LIB LABELS_LIB TICKS_UTILS CHANNELSEND_UTILS INTERPROCESSTRIGGER_UTIL
)


# ********** Node Node_isr_9_ecm **********

add_library(Node_isr_9_ecm_LIB STATIC
	Node_isr_9_ecm/_inc/Node_isr_9_ecm.hpp
)

target_include_directories(Node_isr_9_ecm_LIB
	PUBLIC
	synthetic_gen/runnables/_inc
	synthetic_gen/ticksUtils/_inc
	synthetic_gen/labels/_inc
	synthetic_gen/channelSendUtils/_inc
	synthetic_gen/interProcessTriggerUtils/_inc
)

target_link_libraries(Node_isr_9_ecm_LIB 
	PRIVATE  RUNNABLES_LIB LABELS_LIB TICKS_UTILS CHANNELSEND_UTILS INTERPROCESSTRIGGER_UTIL
)


# ********** Node Node_output_acc **********

add_library(Node_output_acc_LIB STATIC
	Node_output_acc/_inc/Node_output_acc.hpp
)

target_include_directories(Node_output_acc_LIB
	PUBLIC
	synthetic_gen/runnables/_inc
	synthetic_gen/ticksUtils/_inc
	synthetic_gen/labels/_inc
	synthetic_gen/channelSendUtils/_inc
	synthetic_gen/interProcessTriggerUtils/_inc
)

target_link_libraries(Node_output_acc_LIB 
	PRIVATE  RUNNABLES_LIB LABELS_LIB TICKS_UTILS CHANNELSEND_UTILS INTERPROCESSTRIGGER_UTIL
)


# ********** Node Node_output_tsr **********

add_library(Node_output_tsr_LIB STATIC
	Node_output_tsr/_inc/Node_output_tsr.hpp
)

target_include_directories(Node_output_tsr_LIB
	PUBLIC
	synthetic_gen/runnables/_inc
	synthetic_gen/ticksUtils/_inc
	synthetic_gen/labels/_inc
	synthetic_gen/channelSendUtils/_inc
	synthetic_gen/interProcessTriggerUtils/_inc
)

target_link_libraries(Node_output_tsr_LIB 
	PRIVATE  RUNNABLES_LIB LABELS_LIB TICKS_UTILS CHANNELSEND_UTILS INTERPROCESSTRIGGER_UTIL
)


# ********** Node Node_perception_acc **********

add_library(Node_perception_acc_LIB STATIC
	Node_perception_acc/_inc/Node_perception_acc.hpp
)

target_include_directories(Node_perception_acc_LIB
	PUBLIC
	synthetic_gen/runnables/_inc
	synthetic_gen/ticksUtils/_inc
	synthetic_gen/labels/_inc
	synthetic_gen/channelSendUtils/_inc
	synthetic_gen/interProcessTriggerUtils/_inc
)

target_link_libraries(Node_perception_acc_LIB 
	PRIVATE  RUNNABLES_LIB LABELS_LIB TICKS_UTILS CHANNELSEND_UTILS INTERPROCESSTRIGGER_UTIL
)


# ********** Node Node_prediction_pcc **********

add_library(Node_prediction_pcc_LIB STATIC
	Node_prediction_pcc/_inc/Node_prediction_pcc.hpp
)

target_include_directories(Node_prediction_pcc_LIB
	PUBLIC
	synthetic_gen/runnables/_inc
	synthetic_gen/ticksUtils/_inc
	synthetic_gen/labels/_inc
	synthetic_gen/channelSendUtils/_inc
	synthetic_gen/interProcessTriggerUtils/_inc
)

target_link_libraries(Node_prediction_pcc_LIB 
	PRIVATE  RUNNABLES_LIB LABELS_LIB TICKS_UTILS CHANNELSEND_UTILS INTERPROCESSTRIGGER_UTIL
)


# ********** Node Node_preprocessing_acc **********

add_library(Node_preprocessing_acc_LIB STATIC
	Node_preprocessing_acc/_inc/Node_preprocessing_acc.hpp
)

target_include_directories(Node_preprocessing_acc_LIB
	PUBLIC
	synthetic_gen/runnables/_inc
	synthetic_gen/ticksUtils/_inc
	synthetic_gen/labels/_inc
	synthetic_gen/channelSendUtils/_inc
	synthetic_gen/interProcessTriggerUtils/_inc
)

target_link_libraries(Node_preprocessing_acc_LIB 
	PRIVATE  RUNNABLES_LIB LABELS_LIB TICKS_UTILS CHANNELSEND_UTILS INTERPROCESSTRIGGER_UTIL
)


# ********** Node Node_resizing_tsr **********

add_library(Node_resizing_tsr_LIB STATIC
	Node_resizing_tsr/_inc/Node_resizing_tsr.hpp
)

target_include_directories(Node_resizing_tsr_LIB
	PUBLIC
	synthetic_gen/runnables/_inc
	synthetic_gen/ticksUtils/_inc
	synthetic_gen/labels/_inc
	synthetic_gen/channelSendUtils/_inc
	synthetic_gen/interProcessTriggerUtils/_inc
)

target_link_libraries(Node_resizing_tsr_LIB 
	PRIVATE  RUNNABLES_LIB LABELS_LIB TICKS_UTILS CHANNELSEND_UTILS INTERPROCESSTRIGGER_UTIL
)


# ********** Node Node_segmentation_to_background_tsr **********

add_library(Node_segmentation_to_background_tsr_LIB STATIC
	Node_segmentation_to_background_tsr/_inc/Node_segmentation_to_background_tsr.hpp
)

target_include_directories(Node_segmentation_to_background_tsr_LIB
	PUBLIC
	synthetic_gen/runnables/_inc
	synthetic_gen/ticksUtils/_inc
	synthetic_gen/labels/_inc
	synthetic_gen/channelSendUtils/_inc
	synthetic_gen/interProcessTriggerUtils/_inc
)

target_link_libraries(Node_segmentation_to_background_tsr_LIB 
	PRIVATE  RUNNABLES_LIB LABELS_LIB TICKS_UTILS CHANNELSEND_UTILS INTERPROCESSTRIGGER_UTIL
)


# ********** Node Node_segmentation_tsr **********

add_library(Node_segmentation_tsr_LIB STATIC
	Node_segmentation_tsr/_inc/Node_segmentation_tsr.hpp
)

target_include_directories(Node_segmentation_tsr_LIB
	PUBLIC
	synthetic_gen/runnables/_inc
	synthetic_gen/ticksUtils/_inc
	synthetic_gen/labels/_inc
	synthetic_gen/channelSendUtils/_inc
	synthetic_gen/interProcessTriggerUtils/_inc
)

target_link_libraries(Node_segmentation_tsr_LIB 
	PRIVATE  RUNNABLES_LIB LABELS_LIB TICKS_UTILS CHANNELSEND_UTILS INTERPROCESSTRIGGER_UTIL
)


# ********** Node Node_task_1000ms_ecm **********

add_library(Node_task_1000ms_ecm_LIB STATIC
	Node_task_1000ms_ecm/_inc/Node_task_1000ms_ecm.hpp
)

target_include_directories(Node_task_1000ms_ecm_LIB
	PUBLIC
	synthetic_gen/runnables/_inc
	synthetic_gen/ticksUtils/_inc
	synthetic_gen/labels/_inc
	synthetic_gen/channelSendUtils/_inc
	synthetic_gen/interProcessTriggerUtils/_inc
)

target_link_libraries(Node_task_1000ms_ecm_LIB 
	PRIVATE  RUNNABLES_LIB LABELS_LIB TICKS_UTILS CHANNELSEND_UTILS INTERPROCESSTRIGGER_UTIL
)


# ********** Node Node_task_100ms_ecm **********

add_library(Node_task_100ms_ecm_LIB STATIC
	Node_task_100ms_ecm/_inc/Node_task_100ms_ecm.hpp
)

target_include_directories(Node_task_100ms_ecm_LIB
	PUBLIC
	synthetic_gen/runnables/_inc
	synthetic_gen/ticksUtils/_inc
	synthetic_gen/labels/_inc
	synthetic_gen/channelSendUtils/_inc
	synthetic_gen/interProcessTriggerUtils/_inc
)

target_link_libraries(Node_task_100ms_ecm_LIB 
	PRIVATE  RUNNABLES_LIB LABELS_LIB TICKS_UTILS CHANNELSEND_UTILS INTERPROCESSTRIGGER_UTIL
)


# ********** Node Node_task_10ms_ecm **********

add_library(Node_task_10ms_ecm_LIB STATIC
	Node_task_10ms_ecm/_inc/Node_task_10ms_ecm.hpp
)

target_include_directories(Node_task_10ms_ecm_LIB
	PUBLIC
	synthetic_gen/runnables/_inc
	synthetic_gen/ticksUtils/_inc
	synthetic_gen/labels/_inc
	synthetic_gen/channelSendUtils/_inc
	synthetic_gen/interProcessTriggerUtils/_inc
)

target_link_libraries(Node_task_10ms_ecm_LIB 
	PRIVATE  RUNNABLES_LIB LABELS_LIB TICKS_UTILS CHANNELSEND_UTILS INTERPROCESSTRIGGER_UTIL
)


# ********** Node Node_task_1ms_ecm **********

add_library(Node_task_1ms_ecm_LIB STATIC
	Node_task_1ms_ecm/_inc/Node_task_1ms_ecm.hpp
)

target_include_directories(Node_task_1ms_ecm_LIB
	PUBLIC
	synthetic_gen/runnables/_inc
	synthetic_gen/ticksUtils/_inc
	synthetic_gen/labels/_inc
	synthetic_gen/channelSendUtils/_inc
	synthetic_gen/interProcessTriggerUtils/_inc
)

target_link_libraries(Node_task_1ms_ecm_LIB 
	PRIVATE  RUNNABLES_LIB LABELS_LIB TICKS_UTILS CHANNELSEND_UTILS INTERPROCESSTRIGGER_UTIL
)


# ********** Node Node_task_200ms_ecm **********

add_library(Node_task_200ms_ecm_LIB STATIC
	Node_task_200ms_ecm/_inc/Node_task_200ms_ecm.hpp
)

target_include_directories(Node_task_200ms_ecm_LIB
	PUBLIC
	synthetic_gen/runnables/_inc
	synthetic_gen/ticksUtils/_inc
	synthetic_gen/labels/_inc
	synthetic_gen/channelSendUtils/_inc
	synthetic_gen/interProcessTriggerUtils/_inc
)

target_link_libraries(Node_task_200ms_ecm_LIB 
	PRIVATE  RUNNABLES_LIB LABELS_LIB TICKS_UTILS CHANNELSEND_UTILS INTERPROCESSTRIGGER_UTIL
)


# ********** Node Node_task_20ms_ecm **********

add_library(Node_task_20ms_ecm_LIB STATIC
	Node_task_20ms_ecm/_inc/Node_task_20ms_ecm.hpp
)

target_include_directories(Node_task_20ms_ecm_LIB
	PUBLIC
	synthetic_gen/runnables/_inc
	synthetic_gen/ticksUtils/_inc
	synthetic_gen/labels/_inc
	synthetic_gen/channelSendUtils/_inc
	synthetic_gen/interProcessTriggerUtils/_inc
)

target_link_libraries(Node_task_20ms_ecm_LIB 
	PRIVATE  RUNNABLES_LIB LABELS_LIB TICKS_UTILS CHANNELSEND_UTILS INTERPROCESSTRIGGER_UTIL
)


# ********** Node Node_task_2ms_ecm **********

add_library(Node_task_2ms_ecm_LIB STATIC
	Node_task_2ms_ecm/_inc/Node_task_2ms_ecm.hpp
)

target_include_directories(Node_task_2ms_ecm_LIB
	PUBLIC
	synthetic_gen/runnables/_inc
	synthetic_gen/ticksUtils/_inc
	synthetic_gen/labels/_inc
	synthetic_gen/channelSendUtils/_inc
	synthetic_gen/interProcessTriggerUtils/_inc
)

target_link_libraries(Node_task_2ms_ecm_LIB 
	PRIVATE  RUNNABLES_LIB LABELS_LIB TICKS_UTILS CHANNELSEND_UTILS INTERPROCESSTRIGGER_UTIL
)


# ********** Node Node_task_50ms_ecm **********

add_library(Node_task_50ms_ecm_LIB STATIC
	Node_task_50ms_ecm/_inc/Node_task_50ms_ecm.hpp
)

target_include_directories(Node_task_50ms_ecm_LIB
	PUBLIC
	synthetic_gen/runnables/_inc
	synthetic_gen/ticksUtils/_inc
	synthetic_gen/labels/_inc
	synthetic_gen/channelSendUtils/_inc
	synthetic_gen/interProcessTriggerUtils/_inc
)

target_link_libraries(Node_task_50ms_ecm_LIB 
	PRIVATE  RUNNABLES_LIB LABELS_LIB TICKS_UTILS CHANNELSEND_UTILS INTERPROCESSTRIGGER_UTIL
)


# ********** Node Node_task_5ms_ecm **********

add_library(Node_task_5ms_ecm_LIB STATIC
	Node_task_5ms_ecm/_inc/Node_task_5ms_ecm.hpp
)

target_include_directories(Node_task_5ms_ecm_LIB
	PUBLIC
	synthetic_gen/runnables/_inc
	synthetic_gen/ticksUtils/_inc
	synthetic_gen/labels/_inc
	synthetic_gen/channelSendUtils/_inc
	synthetic_gen/interProcessTriggerUtils/_inc
)

target_link_libraries(Node_task_5ms_ecm_LIB 
	PRIVATE  RUNNABLES_LIB LABELS_LIB TICKS_UTILS CHANNELSEND_UTILS INTERPROCESSTRIGGER_UTIL
)


# ********** Node Node_trajectory_optimizer_pcc **********

add_library(Node_trajectory_optimizer_pcc_LIB STATIC
	Node_trajectory_optimizer_pcc/_inc/Node_trajectory_optimizer_pcc.hpp
)

target_include_directories(Node_trajectory_optimizer_pcc_LIB
	PUBLIC
	synthetic_gen/runnables/_inc
	synthetic_gen/ticksUtils/_inc
	synthetic_gen/labels/_inc
	synthetic_gen/channelSendUtils/_inc
	synthetic_gen/interProcessTriggerUtils/_inc
)

target_link_libraries(Node_trajectory_optimizer_pcc_LIB 
	PRIVATE  RUNNABLES_LIB LABELS_LIB TICKS_UTILS CHANNELSEND_UTILS INTERPROCESSTRIGGER_UTIL
)


# ********** Node Node_world_model_acc **********

add_library(Node_world_model_acc_LIB STATIC
	Node_world_model_acc/_inc/Node_world_model_acc.hpp
)

target_include_directories(Node_world_model_acc_LIB
	PUBLIC
	synthetic_gen/runnables/_inc
	synthetic_gen/ticksUtils/_inc
	synthetic_gen/labels/_inc
	synthetic_gen/channelSendUtils/_inc
	synthetic_gen/interProcessTriggerUtils/_inc
)

target_link_libraries(Node_world_model_acc_LIB 
	PRIVATE  RUNNABLES_LIB LABELS_LIB TICKS_UTILS CHANNELSEND_UTILS INTERPROCESSTRIGGER_UTIL
)

add_library(MAIN_OBJ OBJECT main.cpp)

add_custom_command(
    COMMAND echo ""
    OUTPUT main_tdg.cpp
    DEPENDS main.cpp
)
target_include_directories(MAIN_OBJ
	PUBLIC
	Node_classification_tsr/_inc
	Node_cominput_tsr/_inc
	Node_control_behavior_acc/_inc
	Node_databroker_acc/_inc
	Node_databroker_ecm/_inc
	Node_databroker_pcc/_inc
	Node_detection_tsr/_inc
	Node_gaussian_filter_tsr/_inc
	Node_input_acc/_inc
	Node_isr_10_ecm/_inc
	Node_isr_11_ecm/_inc
	Node_isr_1_ecm/_inc
	Node_isr_2_ecm/_inc
	Node_isr_3_ecm/_inc
	Node_isr_4_ecm/_inc
	Node_isr_5_ecm/_inc
	Node_isr_6_ecm/_inc
	Node_isr_7_ecm/_inc
	Node_isr_8_ecm/_inc
	Node_isr_9_ecm/_inc
	Node_output_acc/_inc
	Node_output_tsr/_inc
	Node_perception_acc/_inc
	Node_prediction_pcc/_inc
	Node_preprocessing_acc/_inc
	Node_resizing_tsr/_inc
	Node_segmentation_to_background_tsr/_inc
	Node_segmentation_tsr/_inc
	Node_task_1000ms_ecm/_inc
	Node_task_100ms_ecm/_inc
	Node_task_10ms_ecm/_inc
	Node_task_1ms_ecm/_inc
	Node_task_200ms_ecm/_inc
	Node_task_20ms_ecm/_inc
	Node_task_2ms_ecm/_inc
	Node_task_50ms_ecm/_inc
	Node_task_5ms_ecm/_inc
	Node_trajectory_optimizer_pcc/_inc
	Node_world_model_acc/_inc
	$ENV{EXTRAE_HOME}/include
)

target_link_libraries(MAIN_OBJ Node_classification_tsr_LIB Node_cominput_tsr_LIB Node_control_behavior_acc_LIB Node_databroker_acc_LIB Node_databroker_ecm_LIB Node_databroker_pcc_LIB Node_detection_tsr_LIB Node_gaussian_filter_tsr_LIB Node_input_acc_LIB Node_isr_10_ecm_LIB Node_isr_11_ecm_LIB Node_isr_1_ecm_LIB Node_isr_2_ecm_LIB Node_isr_3_ecm_LIB Node_isr_4_ecm_LIB Node_isr_5_ecm_LIB Node_isr_6_ecm_LIB Node_isr_7_ecm_LIB Node_isr_8_ecm_LIB Node_isr_9_ecm_LIB Node_output_acc_LIB Node_output_tsr_LIB Node_perception_acc_LIB Node_prediction_pcc_LIB Node_preprocessing_acc_LIB Node_resizing_tsr_LIB Node_segmentation_to_background_tsr_LIB Node_segmentation_tsr_LIB Node_task_1000ms_ecm_LIB Node_task_100ms_ecm_LIB Node_task_10ms_ecm_LIB Node_task_1ms_ecm_LIB Node_task_200ms_ecm_LIB Node_task_20ms_ecm_LIB Node_task_2ms_ecm_LIB Node_task_50ms_ecm_LIB Node_task_5ms_ecm_LIB Node_trajectory_optimizer_pcc_LIB Node_world_model_acc_LIB omptrace)
ament_target_dependencies(MAIN_OBJ rclcpp std_msgs classification_service detection_service gausian_filter_service segmentation_service segmentation_to_bckground_service trigger_perception_acc_service trigger_trajectory_optimizer_pcc_service trigger_world_model_acc_service)

add_executable(MAIN_BIN main.cpp)

target_include_directories(MAIN_BIN
	PUBLIC
	Node_classification_tsr/_inc
	Node_cominput_tsr/_inc
	Node_control_behavior_acc/_inc
	Node_databroker_acc/_inc
	Node_databroker_ecm/_inc
	Node_databroker_pcc/_inc
	Node_detection_tsr/_inc
	Node_gaussian_filter_tsr/_inc
	Node_input_acc/_inc
	Node_isr_10_ecm/_inc
	Node_isr_11_ecm/_inc
	Node_isr_1_ecm/_inc
	Node_isr_2_ecm/_inc
	Node_isr_3_ecm/_inc
	Node_isr_4_ecm/_inc
	Node_isr_5_ecm/_inc
	Node_isr_6_ecm/_inc
	Node_isr_7_ecm/_inc
	Node_isr_8_ecm/_inc
	Node_isr_9_ecm/_inc
	Node_output_acc/_inc
	Node_output_tsr/_inc
	Node_perception_acc/_inc
	Node_prediction_pcc/_inc
	Node_preprocessing_acc/_inc
	Node_resizing_tsr/_inc
	Node_segmentation_to_background_tsr/_inc
	Node_segmentation_tsr/_inc
	Node_task_1000ms_ecm/_inc
	Node_task_100ms_ecm/_inc
	Node_task_10ms_ecm/_inc
	Node_task_1ms_ecm/_inc
	Node_task_200ms_ecm/_inc
	Node_task_20ms_ecm/_inc
	Node_task_2ms_ecm/_inc
	Node_task_50ms_ecm/_inc
	Node_task_5ms_ecm/_inc
	Node_trajectory_optimizer_pcc/_inc
	Node_world_model_acc/_inc
	$ENV{EXTRAE_HOME}/include
)

target_link_libraries(MAIN_BIN Node_classification_tsr_LIB Node_cominput_tsr_LIB Node_control_behavior_acc_LIB Node_databroker_acc_LIB Node_databroker_ecm_LIB Node_databroker_pcc_LIB Node_detection_tsr_LIB Node_gaussian_filter_tsr_LIB Node_input_acc_LIB Node_isr_10_ecm_LIB Node_isr_11_ecm_LIB Node_isr_1_ecm_LIB Node_isr_2_ecm_LIB Node_isr_3_ecm_LIB Node_isr_4_ecm_LIB Node_isr_5_ecm_LIB Node_isr_6_ecm_LIB Node_isr_7_ecm_LIB Node_isr_8_ecm_LIB Node_isr_9_ecm_LIB Node_output_acc_LIB Node_output_tsr_LIB Node_perception_acc_LIB Node_prediction_pcc_LIB Node_preprocessing_acc_LIB Node_resizing_tsr_LIB Node_segmentation_to_background_tsr_LIB Node_segmentation_tsr_LIB Node_task_1000ms_ecm_LIB Node_task_100ms_ecm_LIB Node_task_10ms_ecm_LIB Node_task_1ms_ecm_LIB Node_task_200ms_ecm_LIB Node_task_20ms_ecm_LIB Node_task_2ms_ecm_LIB Node_task_50ms_ecm_LIB Node_task_5ms_ecm_LIB Node_trajectory_optimizer_pcc_LIB Node_world_model_acc_LIB omptrace)
ament_target_dependencies(MAIN_BIN rclcpp std_msgs classification_service detection_service gausian_filter_service segmentation_service segmentation_to_bckground_service trigger_perception_acc_service trigger_trajectory_optimizer_pcc_service trigger_world_model_acc_service)

target_sources(MAIN_BIN PRIVATE main_tdg.cpp)
add_dependencies(MAIN_BIN MAIN_OBJ)

install(TARGETS
	# Node_classification_tsr
	# Node_cominput_tsr
	# Node_control_behavior_acc
	# Node_databroker_acc
	# Node_databroker_ecm
	# Node_databroker_pcc
	# Node_detection_tsr
	# Node_gaussian_filter_tsr
	# Node_input_acc
	# Node_isr_10_ecm
	# Node_isr_11_ecm
	# Node_isr_1_ecm
	# Node_isr_2_ecm
	# Node_isr_3_ecm
	# Node_isr_4_ecm
	# Node_isr_5_ecm
	# Node_isr_6_ecm
	# Node_isr_7_ecm
	# Node_isr_8_ecm
	# Node_isr_9_ecm
	# Node_output_acc
	# Node_output_tsr
	# Node_perception_acc
	# Node_prediction_pcc
	# Node_preprocessing_acc
	# Node_resizing_tsr
	# Node_segmentation_to_background_tsr
	# Node_segmentation_tsr
	# Node_task_1000ms_ecm
	# Node_task_100ms_ecm
	# Node_task_10ms_ecm
	# Node_task_1ms_ecm
	# Node_task_200ms_ecm
	# Node_task_20ms_ecm
	# Node_task_2ms_ecm
	# Node_task_50ms_ecm
	# Node_task_5ms_ecm
	# Node_trajectory_optimizer_pcc
	# Node_world_model_acc
	MAIN_BIN
	DESTINATION lib/${PROJECT_NAME}
)

ament_package()

