// This code is auto-generated

#include <chrono>
#include <memory>

#include "rclcpp/rclcpp.hpp"
#include "std_msgs/msg/string.hpp"

#include "runnables.hpp"

using namespace std::chrono_literals;
using std::placeholders::_1;
#define CONSOLE_ENABLED

#if defined(_OPENMP)
int check_Node_databroker_acc(int *original, int *replicated){ return 1;}
#endif
class Node_databroker_acc : public rclcpp::Node
{
	private:
		rclcpp::Subscription<std_msgs::msg::String>::SharedPtr pccspeedsetpoint_subscription_;

	public:
		Node_databroker_acc()
		: Node("node_databroker_acc")
		{
			pccspeedsetpoint_subscription_ = this->create_subscription<std_msgs::msg::String>(
				"pccspeedsetpoint", rclcpp::QoS(10), std::bind(&Node_databroker_acc::pccspeedsetpoint_subscription_callback, this, _1));
		}
	void pccspeedsetpoint_subscription_callback(const std_msgs::msg::String::SharedPtr msg) {
	#ifdef CONSOLE_ENABLED
		std::cout << "ROS2: subscription callback " << msg->data << std::endl ;
	#endif
		run_acc_databroker();
	}
	
	
};
