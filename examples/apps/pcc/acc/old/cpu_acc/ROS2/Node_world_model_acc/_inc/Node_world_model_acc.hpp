// This code is auto-generated

#include <chrono>
#include <memory>

#include "rclcpp/rclcpp.hpp"
#include "std_msgs/msg/string.hpp"

#include "runnables.hpp"
#include "trigger_world_model_acc_service/srv/trigger_world_model_acc_service.hpp"

using namespace std::chrono_literals;
using std::placeholders::_1;
#define CONSOLE_ENABLED

#if defined(_OPENMP)
int check_Node_world_model_acc(int *original, int *replicated){ return 1;}
#endif
class Node_world_model_acc : public rclcpp::Node
{
	private:
		rclcpp::Service<trigger_world_model_acc_service::srv::TriggerWorldModelAccService>::SharedPtr trigger_world_model_acc_service;

	public:
		Node_world_model_acc()
		: Node("node_world_model_acc")
		{
			trigger_world_model_acc_service = this->create_service<trigger_world_model_acc_service::srv::TriggerWorldModelAccService>(
				"trigger_world_model_acc_service", 
				std::bind(&Node_world_model_acc::trigger_world_model_acc_service_callback, this, std::placeholders::_1, std::placeholders::_2));
		}
	
	void trigger_world_model_acc_service_callback(const std::shared_ptr<trigger_world_model_acc_service::srv::TriggerWorldModelAccService::Request> request,
		std::shared_ptr<trigger_world_model_acc_service::srv::TriggerWorldModelAccService::Response> response) {
			(void)request;
			(void)response;
	#ifdef CONSOLE_ENABLED
		std::cout << "Starting trigger_world_model_acc_service_callback" << std::endl;
	#endif
		extern int label316[1];
		extern int label291[1];
		extern int label266[1];
		extern int label434[1];
		extern int label273[2];
		extern int label314[1];
		extern int label438[1];
		extern int label440[1];
		extern int label370[1];
		extern int label404[2];
		extern int label425[1];
		extern int label391[2];
		extern int label289[1];
		extern int label315[1];
		extern int label272[1];
		extern int label367[1];
		extern int label269[20];
		extern int label389[1];
		extern int label384[1];
		extern int label343[1];
		extern int label353[1];
		extern int label381[1];
		extern int label292[1];
		extern int label333[1];
		extern int label422[1];
		extern int label374[1];
		extern int label383[1];
		extern int label414[1];
		extern int label363[1];
		extern int label379[2];
		extern int label420[1];
		extern int label360[1];
		extern int label380[1];
		extern int label280[1];
		extern int label339[1];
		extern int label327[1];
		extern int label366[2];
		extern int label300[1];
		extern int label358[1];
		extern int label310[1];
		extern int label356[1];
		extern int label329[1];
		int dummy;
		
		 #if defined(_OPENMP)
		 #pragma omp taskgraph tdg_type(static) nowait
		#endif
		{
		#if defined(_OPENMP)
		 #pragma omp task depend(out:label329,label356,label310,label358,label300,label366,label339,label280,label380,label360,label420,label379,label363,label414,label383,label374,label422,label292,label381,label353,label343,label384,label389,label367,label272,label289,label391,label425,label404,label370,label440,label438,label314,label273,label434,label266,label291,label316)
		#elif defined(_OMPSS)
		 #pragma oss task depend(out:label329,label356,label310,label358,label300,label366,label339,label280,label380,label360,label420,label379,label363,label414,label383,label374,label422,label292,label381,label353,label343,label384,label389,label367,label272,label289,label391,label425,label404,label370,label440,label438,label314,label273,label434,label266,label291,label316)
		#endif
		run_run42();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:label266) depend(out:label269)
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:label266) depend(out:label269)
		#endif
		run_run43();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:label273) depend(out:label272)
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:label273) depend(out:label272)
		#endif
		run_run44();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:label272)
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:label272)
		#endif
		run_run45();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:label280)
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:label280)
		#endif
		run_run46();
		
		#if defined(_OPENMP)
		 #pragma omp task
		#elif defined(_OMPSS)
		 #pragma oss task
		#endif
		run_run47();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:label292,label291,label289)
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:label292,label291,label289)
		#endif
		run_run48();
		
		#if defined(_OPENMP)
		 #pragma omp task
		#elif defined(_OMPSS)
		 #pragma oss task
		#endif
		run_run49();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:label300)
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:label300)
		#endif
		run_run50();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:label272)
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:label272)
		#endif
		run_run51();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:label310)
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:label310)
		#endif
		run_run52();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:label314,label316) depend(out:label315)
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:label314,label316) depend(out:label315)
		#endif
		run_run53();
		
		#if defined(_OPENMP)
		 #pragma omp task
		#elif defined(_OMPSS)
		 #pragma oss task
		#endif
		run_run54();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(out:label327)
		#elif defined(_OMPSS)
		 #pragma oss task depend(out:label327)
		#endif
		run_run55();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:label329)
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:label329)
		#endif
		run_run56();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:label315) depend(out:label333)
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:label315) depend(out:label333)
		#endif
		run_run57();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:label339)
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:label339)
		#endif
		run_run58();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:label343)
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:label343)
		#endif
		run_run59();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:label333)
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:label333)
		#endif
		run_run60();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:label327) depend(out:label353)
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:label327) depend(out:label353)
		#endif
		run_run61();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:label356,label358)
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:label356,label358)
		#endif
		run_run62();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:label363,label360)
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:label363,label360)
		#endif
		run_run63();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:label333,label367,label366)
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:label333,label367,label366)
		#endif
		run_run64();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:label370)
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:label370)
		#endif
		run_run65();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:label374,label353)
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:label374,label353)
		#endif
		run_run66();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:label380,label381,label379)
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:label380,label381,label379)
		#endif
		run_run67();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:label384,label383)
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:label384,label383)
		#endif
		run_run68();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:label389,label391)
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:label389,label391)
		#endif
		run_run69();
		
		#if defined(_OPENMP)
		 #pragma omp task
		#elif defined(_OMPSS)
		 #pragma oss task
		#endif
		run_run70();
		
		#if defined(_OPENMP)
		 #pragma omp task
		#elif defined(_OMPSS)
		 #pragma oss task
		#endif
		run_run71();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:label404,label269)
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:label404,label269)
		#endif
		run_run72();
		
		#if defined(_OPENMP)
		 #pragma omp task
		#elif defined(_OMPSS)
		 #pragma oss task
		#endif
		run_run73();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:label414)
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:label414)
		#endif
		run_run74();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:label420)
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:label420)
		#endif
		run_run75();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:label422,label425)
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:label422,label425)
		#endif
		run_run76();
		
		#if defined(_OPENMP)
		 #pragma omp task
		#elif defined(_OMPSS)
		 #pragma oss task
		#endif
		run_run77();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:label434)
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:label434)
		#endif
		run_run78();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:label440,label438)
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:label440,label438)
		#endif
		run_run79();
		}
		#pragma omp taskwait;
	}
	
};
