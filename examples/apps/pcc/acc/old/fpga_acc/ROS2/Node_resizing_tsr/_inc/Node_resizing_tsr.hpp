// This code is auto-generated

#include <chrono>
#include <memory>

#include "rclcpp/rclcpp.hpp"
#include "std_msgs/msg/string.hpp"

#include "runnables.hpp"

using namespace std::chrono_literals;
using std::placeholders::_1;
#define CONSOLE_ENABLED

#if defined(_OPENMP)
int check_Node_resizing_tsr(int *original, int *replicated){ return 1;}
#endif
class Node_resizing_tsr : public rclcpp::Node
{
	private:
		rclcpp::TimerBase::SharedPtr timer_preprocessing_;
		rclcpp::Client<gausian_filter_service::srv::GausianFilterService>::SharedPtr gausian_filter_client;

	public:
		Node_resizing_tsr()
		: Node("node_resizing_tsr")
		{
			timer_preprocessing_ = this->create_wall_timer(
					33ms, std::bind(&Node_resizing_tsr::timer_preprocessing_callback, this));
			gausian_filter_client =  this->create_client<gausian_filter_service::srv::GausianFilterService>("gausian_filter_service");
		}
	void timer_preprocessing_callback() {
	#ifdef CONSOLE_ENABLED
		std::cout << "Timer_preprocessing_callback (33ms)" << std::endl;
	#endif
		int dummy;
		
		 #if defined(_OPENMP)
		 #pragma omp taskgraph tdg_type(static) nowait
		#endif
		{
		#if defined(_OPENMP)
		 #pragma omp task
		#elif defined(_OMPSS)
		 #pragma oss task
		#endif
		run_resizing(gausian_filter_client);
		}
		#pragma omp taskwait;
	}
	
	
};
