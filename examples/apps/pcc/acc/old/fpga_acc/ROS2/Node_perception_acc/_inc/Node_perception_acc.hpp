// This code is auto-generated

#include <chrono>
#include <memory>

#include "rclcpp/rclcpp.hpp"
#include "std_msgs/msg/string.hpp"

#include "runnables.hpp"
#include "trigger_perception_acc_service/srv/trigger_perception_acc_service.hpp"

using namespace std::chrono_literals;
using std::placeholders::_1;
#define CONSOLE_ENABLED

#if defined(_OPENMP)
int check_Node_perception_acc(int *original, int *replicated){ return 1;}
#endif
class Node_perception_acc : public rclcpp::Node
{
	private:
		rclcpp::Service<trigger_perception_acc_service::srv::TriggerPerceptionAccService>::SharedPtr trigger_perception_acc_service;
		rclcpp::Client<trigger_world_model_acc_service::srv::TriggerWorldModelAccService>::SharedPtr trigger_world_model_acc_client;

	public:
		Node_perception_acc()
		: Node("node_perception_acc")
		{
			trigger_perception_acc_service = this->create_service<trigger_perception_acc_service::srv::TriggerPerceptionAccService>(
				"trigger_perception_acc_service", 
				std::bind(&Node_perception_acc::trigger_perception_acc_service_callback, this, std::placeholders::_1, std::placeholders::_2));
			trigger_world_model_acc_client =  this->create_client<trigger_world_model_acc_service::srv::TriggerWorldModelAccService>("trigger_world_model_acc_service");
		}
	
	void trigger_perception_acc_service_callback(const std::shared_ptr<trigger_perception_acc_service::srv::TriggerPerceptionAccService::Request> request,
		std::shared_ptr<trigger_perception_acc_service::srv::TriggerPerceptionAccService::Response> response) {
			(void)request;
			(void)response;
	#ifdef CONSOLE_ENABLED
		std::cout << "Starting trigger_perception_acc_service_callback" << std::endl;
	#endif
		extern int label69[1];
		extern int label160[1];
		extern int label117[1];
		extern int label209[1];
		extern int label58[1];
		extern int label164[1];
		extern int label34[1];
		extern int label217[1];
		extern int label239[1];
		extern int label260[1];
		extern int label48[1];
		extern int label94[1];
		extern int label227[1];
		extern int label108[1];
		extern int label262[1];
		extern int label101[1];
		extern int label44[1];
		extern int label47[1];
		extern int label228[1];
		extern int label211[1];
		extern int label55[1];
		extern int label192[1];
		extern int label152[1];
		extern int label159[1];
		extern int label184[1];
		extern int label103[1];
		extern int label40[312];
		extern int label138[1];
		extern int label127[1];
		extern int label195[1];
		extern int label54[285];
		extern int label206[1];
		extern int label238[1];
		extern int label229[1];
		extern int label89[1];
		extern int label166[1];
		extern int label65[1];
		extern int label213[1];
		extern int label222[1];
		extern int label259[1];
		extern int label136[1];
		extern int label177[1];
		extern int label119[1];
		extern int label115[1];
		extern int label191[245];
		extern int label181[1];
		extern int label225[1];
		extern int label121[1];
		extern int label36[1];
		extern int label190[1];
		extern int label250[1];
		extern int label173[1];
		extern int label124[246];
		extern int label253[1];
		extern int label201[1];
		extern int label199[1];
		extern int label131[233];
		extern int label142[1];
		extern int label156[269];
		extern int label134[1];
		extern int label208[1];
		int dummy;
		
		 #if defined(_OPENMP)
		 #pragma omp taskgraph tdg_type(static) nowait
		#endif
		{
		#if defined(_OPENMP)
		 #pragma omp task depend(out:label208,label134,label142,label199,label201,label253,label173,label250,label190,label36,label121,label225,label181,label115,label119,label177,label136,label259,label222,label213,label65,label166,label89,label229,label238,label206,label195,label127,label138,label184,label159,label152,label192,label211,label228,label47,label44,label101,label262,label227,label94,label48,label260,label239,label217,label34,label164,label209,label117,label160,label69)
		 #pragma omp target  map(from:label208[0:0],label134[0:0],label142[0:0],label199[0:0],label201[0:0],label253[0:0],label173[0:0],label250[0:0],label190[0:0],label36[0:0],label121[0:0],label225[0:0],label181[0:0],label115[0:0],label119[0:0],label177[0:0],label136[0:0],label259[0:0],label222[0:0],label213[0:0],label65[0:0],label166[0:0],label89[0:0],label229[0:0],label238[0:0],label206[0:0],label195[0:0],label127[0:0],label138[0:0],label184[0:0],label159[0:0],label152[0:0],label192[0:0],label211[0:0],label228[0:0],label47[0:0],label44[0:0],label101[0:0],label262[0:0],label227[0:0],label94[0:0],label48[0:0],label260[0:0],label239[0:0],label217[0:0],label34[0:0],label164[0:0],label209[0:0],label117[0:0],label160[0:0],label69[0:0])
		#elif defined(_OMPSS)
		 #pragma oss task depend(out:label208,label134,label142,label199,label201,label253,label173,label250,label190,label36,label121,label225,label181,label115,label119,label177,label136,label259,label222,label213,label65,label166,label89,label229,label238,label206,label195,label127,label138,label184,label159,label152,label192,label211,label228,label47,label44,label101,label262,label227,label94,label48,label260,label239,label217,label34,label164,label209,label117,label160,label69) map(from:label208[0:0],label134[0:0],label142[0:0],label199[0:0],label201[0:0],label253[0:0],label173[0:0],label250[0:0],label190[0:0],label36[0:0],label121[0:0],label225[0:0],label181[0:0],label115[0:0],label119[0:0],label177[0:0],label136[0:0],label259[0:0],label222[0:0],label213[0:0],label65[0:0],label166[0:0],label89[0:0],label229[0:0],label238[0:0],label206[0:0],label195[0:0],label127[0:0],label138[0:0],label184[0:0],label159[0:0],label152[0:0],label192[0:0],label211[0:0],label228[0:0],label47[0:0],label44[0:0],label101[0:0],label262[0:0],label227[0:0],label94[0:0],label48[0:0],label260[0:0],label239[0:0],label217[0:0],label34[0:0],label164[0:0],label209[0:0],label117[0:0],label160[0:0],label69[0:0]) copy_deps
		#endif
		run_run13();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:label34,label36) depend(out:label40)
		 #pragma omp target  map(to:label34[0:0],label36[0:0]) map(from:label40[0:312])
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:label34,label36) depend(out:label40) map(to:label34[0:0],label36[0:0]) map(from:label40[0:312]) copy_deps
		#endif
		run_run14();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:label47,label44) depend(out:label48)
		 #pragma omp target  map(to:label47[0:0],label44[0:0]) map(from:label48[0:0])
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:label47,label44) depend(out:label48) map(to:label47[0:0],label44[0:0]) map(from:label48[0:0]) copy_deps
		#endif
		run_run15();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:label40,label48) depend(out:label54)
		 #pragma omp target  map(to:label40[0:312],label48[0:0]) map(from:label54[0:285])
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:label40,label48) depend(out:label54) map(to:label40[0:312],label48[0:0]) map(from:label54[0:285]) copy_deps
		#endif
		run_run16();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(out:label58,label55)
		 #pragma omp target  map(from:label58[0:0],label55[0:0])
		#elif defined(_OMPSS)
		 #pragma oss task depend(out:label58,label55) map(from:label58[0:0],label55[0:0]) copy_deps
		#endif
		run_run17();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:label65,label69)
		 #pragma omp target  map(to:label65[0:0],label69[0:0])
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:label65,label69) map(to:label65[0:0],label69[0:0]) copy_deps
		#endif
		run_run18();
		
		#if defined(_OPENMP)
		 #pragma omp task
		 #pragma omp target 
		#elif defined(_OMPSS)
		 #pragma oss task copy_deps
		#endif
		run_run19();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:label89)
		 #pragma omp target  map(to:label89[0:0])
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:label89) map(to:label89[0:0]) copy_deps
		#endif
		run_run20();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:label94)
		 #pragma omp target  map(to:label94[0:0])
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:label94) map(to:label94[0:0]) copy_deps
		#endif
		run_run21();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:label54) depend(out:label101,label103)
		 #pragma omp target  map(to:label54[0:285]) map(from:label101[0:0],label103[0:0])
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:label54) depend(out:label101,label103) map(to:label54[0:285]) map(from:label101[0:0],label103[0:0]) copy_deps
		#endif
		run_run22();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:label115) depend(out:label108)
		 #pragma omp target  map(to:label115[0:0]) map(from:label108[0:0])
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:label115) depend(out:label108) map(to:label115[0:0]) map(from:label108[0:0]) copy_deps
		#endif
		run_run23();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:label121,label108,label117,label119) depend(out:label124)
		 #pragma omp target  map(to:label121[0:0],label108[0:0],label117[0:0],label119[0:0]) map(from:label124[0:246])
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:label121,label108,label117,label119) depend(out:label124) map(to:label121[0:0],label108[0:0],label117[0:0],label119[0:0]) map(from:label124[0:246]) copy_deps
		#endif
		run_run24();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:label103,label48,label127) depend(out:label131)
		 #pragma omp target  map(to:label103[0:0],label48[0:0],label127[0:0]) map(from:label131[0:233])
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:label103,label48,label127) depend(out:label131) map(to:label103[0:0],label48[0:0],label127[0:0]) map(from:label131[0:233]) copy_deps
		#endif
		run_run25();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:label138,label134,label136)
		 #pragma omp target  map(to:label138[0:0],label134[0:0],label136[0:0])
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:label138,label134,label136) map(to:label138[0:0],label134[0:0],label136[0:0]) copy_deps
		#endif
		run_run26();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:label142,label55)
		 #pragma omp target  map(to:label142[0:0],label55[0:0])
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:label142,label55) map(to:label142[0:0],label55[0:0]) copy_deps
		#endif
		run_run27();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:label40,label152) depend(out:label156)
		 #pragma omp target  map(to:label40[0:312],label152[0:0]) map(from:label156[0:269])
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:label40,label152) depend(out:label156) map(to:label40[0:312],label152[0:0]) map(from:label156[0:269]) copy_deps
		#endif
		run_run28();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:label164,label159,label160)
		 #pragma omp target  map(to:label164[0:0],label159[0:0],label160[0:0])
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:label164,label159,label160) map(to:label164[0:0],label159[0:0],label160[0:0]) copy_deps
		#endif
		run_run29();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:label166,label173)
		 #pragma omp target  map(to:label166[0:0],label173[0:0])
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:label166,label173) map(to:label166[0:0],label173[0:0]) copy_deps
		#endif
		run_run30();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:label177,label181)
		 #pragma omp target  map(to:label177[0:0],label181[0:0])
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:label177,label181) map(to:label177[0:0],label181[0:0]) copy_deps
		#endif
		run_run31();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:label101,label184,label190) depend(out:label191)
		 #pragma omp target  map(to:label101[0:0],label184[0:0],label190[0:0]) map(from:label191[0:245])
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:label101,label184,label190) depend(out:label191) map(to:label101[0:0],label184[0:0],label190[0:0]) map(from:label191[0:245]) copy_deps
		#endif
		run_run32();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:label124,label156,label54,label195,label192,label131)
		 #pragma omp target  map(to:label124[0:246],label156[0:269],label54[0:285],label195[0:0],label192[0:0],label131[0:233])
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:label124,label156,label54,label195,label192,label131) map(to:label124[0:246],label156[0:269],label54[0:285],label195[0:0],label192[0:0],label131[0:233]) copy_deps
		#endif
		run_run33();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:label201,label124,label199)
		 #pragma omp target  map(to:label201[0:0],label124[0:246],label199[0:0])
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:label201,label124,label199) map(to:label201[0:0],label124[0:246],label199[0:0]) copy_deps
		#endif
		run_run34();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:label208,label209,label206,label191) depend(out:label211)
		 #pragma omp target  map(to:label208[0:0],label209[0:0],label206[0:0],label191[0:245]) map(from:label211[0:0])
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:label208,label209,label206,label191) depend(out:label211) map(to:label208[0:0],label209[0:0],label206[0:0],label191[0:245]) map(from:label211[0:0]) copy_deps
		#endif
		run_run35();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:label217,label213)
		 #pragma omp target  map(to:label217[0:0],label213[0:0])
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:label217,label213) map(to:label217[0:0],label213[0:0]) copy_deps
		#endif
		run_run36();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:label228,label225,label222,label229,label227)
		 #pragma omp target  map(to:label228[0:0],label225[0:0],label222[0:0],label229[0:0],label227[0:0])
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:label228,label225,label222,label229,label227) map(to:label228[0:0],label225[0:0],label222[0:0],label229[0:0],label227[0:0]) copy_deps
		#endif
		run_run37();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:label58,label211)
		 #pragma omp target  map(to:label58[0:0],label211[0:0])
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:label58,label211) map(to:label58[0:0],label211[0:0]) copy_deps
		#endif
		run_run38();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:label239,label238)
		 #pragma omp target  map(to:label239[0:0],label238[0:0])
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:label239,label238) map(to:label239[0:0],label238[0:0]) copy_deps
		#endif
		run_run39();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:label253,label250)
		 #pragma omp target  map(to:label253[0:0],label250[0:0])
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:label253,label250) map(to:label253[0:0],label250[0:0]) copy_deps
		#endif
		run_run40();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:label260,label259,label262)
		 #pragma omp target  map(to:label260[0:0],label259[0:0],label262[0:0])
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:label260,label259,label262) map(to:label260[0:0],label259[0:0],label262[0:0]) copy_deps
		#endif
		run_run41(trigger_world_model_acc_client);
		}
		#pragma omp taskwait;
	}
	
};
