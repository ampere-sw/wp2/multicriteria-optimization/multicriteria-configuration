// This code is auto-generated

#include <chrono>
#include <memory>

#include "rclcpp/rclcpp.hpp"
#include "std_msgs/msg/string.hpp"

#include "runnables.hpp"

using namespace std::chrono_literals;
using std::placeholders::_1;
#define CONSOLE_ENABLED

#if defined(_OPENMP)
int check_Node_preprocessing_acc(int *original, int *replicated){ return 1;}
#endif
class Node_preprocessing_acc : public rclcpp::Node
{
	private:
		rclcpp::TimerBase::SharedPtr timer_66mstrigger_;
		rclcpp::Client<trigger_perception_acc_service::srv::TriggerPerceptionAccService>::SharedPtr trigger_perception_acc_client;

	public:
		Node_preprocessing_acc()
		: Node("node_preprocessing_acc")
		{
			timer_66mstrigger_ = this->create_wall_timer(
					66ms, std::bind(&Node_preprocessing_acc::timer_66mstrigger_callback, this));
			trigger_perception_acc_client =  this->create_client<trigger_perception_acc_service::srv::TriggerPerceptionAccService>("trigger_perception_acc_service");
		}
	void timer_66mstrigger_callback() {
	#ifdef CONSOLE_ENABLED
		std::cout << "Timer_66mstrigger_callback (66ms)" << std::endl;
	#endif
		extern int label23[1];
		extern int label15[1];
		extern int label21[1];
		extern int label6[1];
		extern int label9[1];
		extern int label12[1];
		extern int label20[1];
		int dummy;
		
		 #if defined(_OPENMP)
		 #pragma omp taskgraph tdg_type(static) nowait
		#endif
		{
		#if defined(_OPENMP)
		 #pragma omp task depend(out:label20,label12,label9,label6,label21,label15,label23)
		 #pragma omp target  map(from:label20[0:0],label12[0:0],label9[0:0],label6[0:0],label21[0:0],label15[0:0],label23[0:0])
		#elif defined(_OMPSS)
		 #pragma oss task depend(out:label20,label12,label9,label6,label21,label15,label23) map(from:label20[0:0],label12[0:0],label9[0:0],label6[0:0],label21[0:0],label15[0:0],label23[0:0]) copy_deps
		#endif
		run_run3();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:label6)
		 #pragma omp target  map(to:label6[0:0])
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:label6) map(to:label6[0:0]) copy_deps
		#endif
		run_run4();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:label9)
		 #pragma omp target  map(to:label9[0:0])
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:label9) map(to:label9[0:0]) copy_deps
		#endif
		run_run5();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:label12)
		 #pragma omp target  map(to:label12[0:0])
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:label12) map(to:label12[0:0]) copy_deps
		#endif
		run_run6();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:label15)
		 #pragma omp target  map(to:label15[0:0])
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:label15) map(to:label15[0:0]) copy_deps
		#endif
		run_run7();
		
		#if defined(_OPENMP)
		 #pragma omp task
		 #pragma omp target 
		#elif defined(_OMPSS)
		 #pragma oss task copy_deps
		#endif
		run_run8();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:label20,label21)
		 #pragma omp target  map(to:label20[0:0],label21[0:0])
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:label20,label21) map(to:label20[0:0],label21[0:0]) copy_deps
		#endif
		run_run9();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:label23)
		 #pragma omp target  map(to:label23[0:0])
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:label23) map(to:label23[0:0]) copy_deps
		#endif
		run_run10();
		
		#if defined(_OPENMP)
		 #pragma omp task
		 #pragma omp target 
		#elif defined(_OMPSS)
		 #pragma oss task copy_deps
		#endif
		run_run11();
		
		#if defined(_OPENMP)
		 #pragma omp task
		 #pragma omp target 
		#elif defined(_OMPSS)
		 #pragma oss task copy_deps
		#endif
		run_run12(trigger_perception_acc_client);
		}
		#pragma omp taskwait;
	}
	
	
};
