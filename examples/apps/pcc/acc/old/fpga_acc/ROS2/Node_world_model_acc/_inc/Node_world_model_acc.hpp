// This code is auto-generated

#include <chrono>
#include <memory>

#include "rclcpp/rclcpp.hpp"
#include "std_msgs/msg/string.hpp"

#include "runnables.hpp"
#include "trigger_world_model_acc_service/srv/trigger_world_model_acc_service.hpp"

using namespace std::chrono_literals;
using std::placeholders::_1;
#define CONSOLE_ENABLED

#if defined(_OPENMP)
int check_Node_world_model_acc(int *original, int *replicated){ return 1;}
#endif
class Node_world_model_acc : public rclcpp::Node
{
	private:
		rclcpp::Service<trigger_world_model_acc_service::srv::TriggerWorldModelAccService>::SharedPtr trigger_world_model_acc_service;

	public:
		Node_world_model_acc()
		: Node("node_world_model_acc")
		{
			trigger_world_model_acc_service = this->create_service<trigger_world_model_acc_service::srv::TriggerWorldModelAccService>(
				"trigger_world_model_acc_service", 
				std::bind(&Node_world_model_acc::trigger_world_model_acc_service_callback, this, std::placeholders::_1, std::placeholders::_2));
		}
	
	void trigger_world_model_acc_service_callback(const std::shared_ptr<trigger_world_model_acc_service::srv::TriggerWorldModelAccService::Request> request,
		std::shared_ptr<trigger_world_model_acc_service::srv::TriggerWorldModelAccService::Response> response) {
			(void)request;
			(void)response;
	#ifdef CONSOLE_ENABLED
		std::cout << "Starting trigger_world_model_acc_service_callback" << std::endl;
	#endif
		extern int label300[1];
		extern int label266[1];
		extern int label420[1];
		extern int label292[1];
		extern int label291[1];
		extern int label414[1];
		extern int label272[1];
		extern int label370[1];
		extern int label327[1];
		extern int label422[1];
		extern int label425[1];
		extern int label366[2];
		extern int label404[2];
		extern int label333[1];
		extern int label391[2];
		extern int label329[1];
		extern int label353[1];
		extern int label438[1];
		extern int label360[1];
		extern int label434[1];
		extern int label343[1];
		extern int label310[1];
		extern int label374[1];
		extern int label363[1];
		extern int label379[2];
		extern int label380[1];
		extern int label381[1];
		extern int label314[1];
		extern int label356[1];
		extern int label339[1];
		extern int label389[1];
		extern int label280[1];
		extern int label315[1];
		extern int label440[1];
		extern int label383[1];
		extern int label316[1];
		extern int label269[20];
		extern int label384[1];
		extern int label289[1];
		extern int label273[2];
		extern int label358[1];
		extern int label367[1];
		int dummy;
		
		 #if defined(_OPENMP)
		 #pragma omp taskgraph tdg_type(static) nowait
		#endif
		{
		#if defined(_OPENMP)
		 #pragma omp task depend(out:label367,label358,label273,label289,label384,label316,label383,label440,label280,label389,label339,label356,label314,label381,label380,label379,label363,label374,label310,label343,label434,label360,label438,label353,label329,label391,label404,label366,label425,label422,label370,label272,label414,label291,label292,label420,label266,label300)
		 #pragma omp target  map(from:label367[0:0],label358[0:0],label273[0:2],label289[0:0],label384[0:0],label316[0:0],label383[0:0],label440[0:0],label280[0:0],label389[0:0],label339[0:0],label356[0:0],label314[0:0],label381[0:0],label380[0:0],label379[0:2],label363[0:0],label374[0:0],label310[0:0],label343[0:0],label434[0:0],label360[0:0],label438[0:0],label353[0:0],label329[0:0],label391[0:2],label404[0:2],label366[0:2],label425[0:0],label422[0:0],label370[0:0],label272[0:0],label414[0:0],label291[0:0],label292[0:0],label420[0:0],label266[0:0],label300[0:0])
		#elif defined(_OMPSS)
		 #pragma oss task depend(out:label367,label358,label273,label289,label384,label316,label383,label440,label280,label389,label339,label356,label314,label381,label380,label379,label363,label374,label310,label343,label434,label360,label438,label353,label329,label391,label404,label366,label425,label422,label370,label272,label414,label291,label292,label420,label266,label300) map(from:label367[0:0],label358[0:0],label273[0:2],label289[0:0],label384[0:0],label316[0:0],label383[0:0],label440[0:0],label280[0:0],label389[0:0],label339[0:0],label356[0:0],label314[0:0],label381[0:0],label380[0:0],label379[0:2],label363[0:0],label374[0:0],label310[0:0],label343[0:0],label434[0:0],label360[0:0],label438[0:0],label353[0:0],label329[0:0],label391[0:2],label404[0:2],label366[0:2],label425[0:0],label422[0:0],label370[0:0],label272[0:0],label414[0:0],label291[0:0],label292[0:0],label420[0:0],label266[0:0],label300[0:0]) copy_deps
		#endif
		run_run42();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:label266) depend(out:label269)
		 #pragma omp target  map(to:label266[0:0]) map(from:label269[0:20])
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:label266) depend(out:label269) map(to:label266[0:0]) map(from:label269[0:20]) copy_deps
		#endif
		run_run43();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:label273) depend(out:label272)
		 #pragma omp target  map(to:label273[0:2]) map(from:label272[0:0])
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:label273) depend(out:label272) map(to:label273[0:2]) map(from:label272[0:0]) copy_deps
		#endif
		run_run44();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:label272)
		 #pragma omp target  map(to:label272[0:0])
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:label272) map(to:label272[0:0]) copy_deps
		#endif
		run_run45();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:label280)
		 #pragma omp target  map(to:label280[0:0])
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:label280) map(to:label280[0:0]) copy_deps
		#endif
		run_run46();
		
		#if defined(_OPENMP)
		 #pragma omp task
		 #pragma omp target 
		#elif defined(_OMPSS)
		 #pragma oss task copy_deps
		#endif
		run_run47();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:label289,label291,label292)
		 #pragma omp target  map(to:label289[0:0],label291[0:0],label292[0:0])
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:label289,label291,label292) map(to:label289[0:0],label291[0:0],label292[0:0]) copy_deps
		#endif
		run_run48();
		
		#if defined(_OPENMP)
		 #pragma omp task
		 #pragma omp target 
		#elif defined(_OMPSS)
		 #pragma oss task copy_deps
		#endif
		run_run49();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:label300)
		 #pragma omp target  map(to:label300[0:0])
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:label300) map(to:label300[0:0]) copy_deps
		#endif
		run_run50();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:label272)
		 #pragma omp target  map(to:label272[0:0])
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:label272) map(to:label272[0:0]) copy_deps
		#endif
		run_run51();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:label310)
		 #pragma omp target  map(to:label310[0:0])
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:label310) map(to:label310[0:0]) copy_deps
		#endif
		run_run52();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:label314,label316) depend(out:label315)
		 #pragma omp target  map(to:label314[0:0],label316[0:0]) map(from:label315[0:0])
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:label314,label316) depend(out:label315) map(to:label314[0:0],label316[0:0]) map(from:label315[0:0]) copy_deps
		#endif
		run_run53();
		
		#if defined(_OPENMP)
		 #pragma omp task
		 #pragma omp target 
		#elif defined(_OMPSS)
		 #pragma oss task copy_deps
		#endif
		run_run54();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(out:label327)
		 #pragma omp target  map(from:label327[0:0])
		#elif defined(_OMPSS)
		 #pragma oss task depend(out:label327) map(from:label327[0:0]) copy_deps
		#endif
		run_run55();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:label329)
		 #pragma omp target  map(to:label329[0:0])
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:label329) map(to:label329[0:0]) copy_deps
		#endif
		run_run56();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:label315) depend(out:label333)
		 #pragma omp target  map(to:label315[0:0]) map(from:label333[0:0])
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:label315) depend(out:label333) map(to:label315[0:0]) map(from:label333[0:0]) copy_deps
		#endif
		run_run57();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:label339)
		 #pragma omp target  map(to:label339[0:0])
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:label339) map(to:label339[0:0]) copy_deps
		#endif
		run_run58();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:label343)
		 #pragma omp target  map(to:label343[0:0])
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:label343) map(to:label343[0:0]) copy_deps
		#endif
		run_run59();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:label333)
		 #pragma omp target  map(to:label333[0:0])
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:label333) map(to:label333[0:0]) copy_deps
		#endif
		run_run60();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:label327) depend(out:label353)
		 #pragma omp target  map(to:label327[0:0]) map(from:label353[0:0])
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:label327) depend(out:label353) map(to:label327[0:0]) map(from:label353[0:0]) copy_deps
		#endif
		run_run61();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:label358,label356)
		 #pragma omp target  map(to:label358[0:0],label356[0:0])
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:label358,label356) map(to:label358[0:0],label356[0:0]) copy_deps
		#endif
		run_run62();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:label360,label363)
		 #pragma omp target  map(to:label360[0:0],label363[0:0])
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:label360,label363) map(to:label360[0:0],label363[0:0]) copy_deps
		#endif
		run_run63();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:label367,label333,label366)
		 #pragma omp target  map(to:label367[0:0],label333[0:0],label366[0:2])
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:label367,label333,label366) map(to:label367[0:0],label333[0:0],label366[0:2]) copy_deps
		#endif
		run_run64();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:label370)
		 #pragma omp target  map(to:label370[0:0])
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:label370) map(to:label370[0:0]) copy_deps
		#endif
		run_run65();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:label353,label374)
		 #pragma omp target  map(to:label353[0:0],label374[0:0])
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:label353,label374) map(to:label353[0:0],label374[0:0]) copy_deps
		#endif
		run_run66();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:label381,label380,label379)
		 #pragma omp target  map(to:label381[0:0],label380[0:0],label379[0:2])
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:label381,label380,label379) map(to:label381[0:0],label380[0:0],label379[0:2]) copy_deps
		#endif
		run_run67();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:label384,label383)
		 #pragma omp target  map(to:label384[0:0],label383[0:0])
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:label384,label383) map(to:label384[0:0],label383[0:0]) copy_deps
		#endif
		run_run68();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:label389,label391)
		 #pragma omp target  map(to:label389[0:0],label391[0:2])
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:label389,label391) map(to:label389[0:0],label391[0:2]) copy_deps
		#endif
		run_run69();
		
		#if defined(_OPENMP)
		 #pragma omp task
		 #pragma omp target 
		#elif defined(_OMPSS)
		 #pragma oss task copy_deps
		#endif
		run_run70();
		
		#if defined(_OPENMP)
		 #pragma omp task
		 #pragma omp target 
		#elif defined(_OMPSS)
		 #pragma oss task copy_deps
		#endif
		run_run71();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:label269,label404)
		 #pragma omp target  map(to:label269[0:20],label404[0:2])
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:label269,label404) map(to:label269[0:20],label404[0:2]) copy_deps
		#endif
		run_run72();
		
		#if defined(_OPENMP)
		 #pragma omp task
		 #pragma omp target 
		#elif defined(_OMPSS)
		 #pragma oss task copy_deps
		#endif
		run_run73();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:label414)
		 #pragma omp target  map(to:label414[0:0])
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:label414) map(to:label414[0:0]) copy_deps
		#endif
		run_run74();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:label420)
		 #pragma omp target  map(to:label420[0:0])
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:label420) map(to:label420[0:0]) copy_deps
		#endif
		run_run75();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:label425,label422)
		 #pragma omp target  map(to:label425[0:0],label422[0:0])
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:label425,label422) map(to:label425[0:0],label422[0:0]) copy_deps
		#endif
		run_run76();
		
		#if defined(_OPENMP)
		 #pragma omp task
		 #pragma omp target 
		#elif defined(_OMPSS)
		 #pragma oss task copy_deps
		#endif
		run_run77();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:label434)
		 #pragma omp target  map(to:label434[0:0])
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:label434) map(to:label434[0:0]) copy_deps
		#endif
		run_run78();
		
		#if defined(_OPENMP)
		 #pragma omp task depend(in:label438,label440)
		 #pragma omp target  map(to:label438[0:0],label440[0:0])
		#elif defined(_OMPSS)
		 #pragma oss task depend(in:label438,label440) map(to:label438[0:0],label440[0:0]) copy_deps
		#endif
		run_run79();
		}
		#pragma omp taskwait;
	}
	
};
