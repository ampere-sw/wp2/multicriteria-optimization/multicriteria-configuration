{
    "general": {
        "base_dir": "./examples/apps/pcc/tsr",
        "working_dir": "./results/tsr/profiler/",
        "analysis_dir": "./results/tsr/analysis/",
        "final_dir": "./results/tsr//final/",
        "config_name": "synthetic_tasks.cfg",
        "clear_dirs": true
    },
    "app": {
        "name": "MAIN_BIN",
        "amalthea_model": "./examples/apps/pcc/PCC_Bosch_Jetson.amxmi",
        "latency_constraint": "Req Deadline EC_TSR",
        "_OR_event_chain_constraint": "EffectChainTsr",
        "variants": {
            "cpu": {
                "dir": "./cpu_tsr",
                "_build": {
                    "dynamic_mapping": "colcon build",
                    "_dynamic_mapping": "/bin/bash ROS2/build.sh",
                    "static_mapping": "/bin/bash ROS2/build.sh",
                    "timeout": "3600"
                },
                "dots_dir": "build/amalthea_ros2_model",
                "tdg.cpp_file": "build/amalthea_ros2_model/main_tdg.cpp",
                "run": ". ./install/setup.sh && ros2 run amalthea_ros2_model MAIN_BIN",
                "exec_name": "MAIN_BIN",
                "extrae_results_dir": "./",
                "output_prefix": "cpu",
                "iterations": 1,
                "outliers": 3
            },
            "gpu": {
                "dir": "./gpu_tsr",
                "_build": {
                    "dynamic_mapping": "colcon build",
                    "_dynamic_mapping": "/bin/bash ROS2/build.sh",
                    "static_mapping": "/bin/bash ROS2/build.sh",
                    "timeout": "3600"
                },
                "dots_dir": "build/amalthea_ros2_model",
                "tdg.cpp_file": "build/amalthea_ros2_model/main_tdg.cpp",
                "run": "sudo -E env \"PATH=$PATH\" \"LD_LIBRARY_PATH=$LD_LIBRARY_PATH\" bash -c '. install/setup.sh && ros2 run amalthea_ros2_model MAIN_BIN'",
                "_run_no_sudo": ". install/setup.sh && ros2 run amalthea_ros2_model MAIN_BIN",
                "exec_name": "MAIN_BIN",
                "cudamperf_results_file": "MAIN_BIN_cudamperf.json",
                "extrae_results_dir": "./",
                "output_prefix": "gpu",
                "iterations": 1,
                "outliers": 3
            }
        }
    },
    "platform": {
        "selected": "Xavier",
        "platforms": {
            "Xavier": {
                "setup": [
                    "echo 'ondemand' | sudo tee /sys/devices/system/cpu/cpufreq/policy0/scaling_governor",
                    "sudo nvpmodel -m 0",
                    "sudo jetson_clocks --fan",
                    "sudo jetson_clocks",
                    "echo 'userspace' | sudo tee /sys/devices/system/cpu/cpufreq/policy0/scaling_governor"
                ],
                "cpu": {
                    "cmd": [
                        "echo {frequency} | sudo tee /sys/devices/system/cpu/cpufreq/policy0/scaling_min_freq",
                        "echo {frequency} | sudo tee /sys/devices/system/cpu/cpufreq/policy0/scaling_max_freq",
                        "echo {frequency} | sudo tee /sys/devices/system/cpu/cpufreq/policy0/scaling_setspeed",
                        {"OMP_NUM_THREADS": "{threads}"}
                    ],
                    
                    "args": {
                        "frequency": [2265600],
                        "threads": [8]
                    },
                    "_args": {
                        "frequency": [729600, 1190400, 2265600],
                        "threads": [8]
                    },
                    "other_way_to_define_args_is_with_ranges": {
                        "threads":{
                            "start": 1,
                            "stop": 10,
                            "step": 1
                        }
                    },
                    "wait": 200,
                    "num_threads": "threads"
                },
                "gpu": {
                    "cmd": [
                        "echo {frequency} | sudo tee /sys/devices/17000000.gv11b/devfreq/17000000.gv11b/max_freq",
                        "echo {frequency} | sudo tee /sys/devices/17000000.gv11b/devfreq/17000000.gv11b/min_freq",
                        "echo {frequency} | sudo tee /sys/devices/17000000.gv11b/devfreq/17000000.gv11b/max_freq"
                    ],
                    
                    "args": {
                        "frequency": [1377000]
                    },
                    "__args": {
                        "frequency": [624750, 1032750, 1377000]
                    },
                    "_args": {
                        "frequency": [
                            624750, 675750, 828750, 905250, 1032750, 1198500, 1236750, 1338750, 1377000
                        ]   
                    },
                    "wait": 200
                },
                "cleanup": [
                    "echo 'ondemand' | sudo tee /sys/devices/system/cpu/cpufreq/policy0/scaling_governor"
                ]
            },
            "Synth": {
                "setup": "echo getting Xavier ready for profiling",
                "cpu": {
                    "cmd": [
                        "echo setting min cpu as {frequency}kHz",
                        "echo setting max cpu as {frequency}kHz"
                    ],
                    "args": {
                        "frequency": [729600, 1190400, 2265600]
                    },
                    "wait": 0,
                    "num_threads": 8
                },
                "gpu": {
                    "cmd": [
                        "echo setting gpu as {frequency}kHz"
                    ],
                    "args": {
                        "frequency": [624750, 1032750, 1377000]
                    },
                    "wait": 0
                },
                "cleanup": [
                    "echo reverting mode to 'ondemand'"
                ]
            },
            "Xavier_synth": {
                "setup": "echo getting Xavier ready for profiling",
                "cpu": {
                    "cmd": [
                        "echo setting min cpu as {frequency}kHz",
                        "echo setting max cpu as {frequency}kHz",
                        "echo setting number of threads as {threads}"
                    ],
                    "args": {
                        "frequency": [729600, 1190400, 2265600],
                        "threads": [8]
                    },
                    "wait": 0,
                    "num_threads": "threads"
                },
                "gpu": {
                    "cmd": "echo setting gpu as {frequency}MHz",
                    "args": {
                        "frequency": [
                            1600
                        ]
                    },
                    "wait": 0
                },
                "order_by": [
                    "cpu.threads",
                    "cpu.frequency",
                    "gpu.frequency"
                ],
                "group_by": "cpu.threads",
                "cleanup": [
                    "echo reverting mode to 'ondemand'"
                ]
            },
            "AMD_Ryzen_7_5800H": {
                "setup": [
                    "echo '1' | sudo tee /proc/sys/kernel/perf_event_paranoid > /dev/null",
                    "modprobe cpufreq_userspace",
                    "sudo cpupower frequency-set --governor userspace > /dev/null"
                ],
                "cpu": {
                    "cmd": [
                        "sudo cpupower --cpu all frequency-set --freq {frequency}KHz > /dev/null",
                        {"OMP_NUM_THREADS": "{threads}"}
                    ],
                    "args": {
                        
                        "frequency": {
                            "start": 500,
                            "stop": 4100,
                            "step": 500
                        },
                        "threads": {
                            "start": 2,
                            "stop": 18,
                            "step": 2
                        }
                    },
                    "wait": 200,
                    "num_threads": "threads"
                },
                "gpu": {},
                "fpga": {},
                "order_by": [
                    "cpu.frequency",
                    "cpu.threads"
                ],
                "group_by": "cpu.frequency",
                "cleanup": [
                    "sudo cpupower frequency-set --governor ondemand > /dev/null"
                ]
            },
            "default": {
                "cpu": {
                    "cmd": ":"
                },
                "gpu": {
                    "cmd": ":"
                }
            }
        }
    },
    "optimization": {
        "RT": {
            "mapping_algorithms": [
                {
                    "task2thread": "BestFit",
                    "queue": "BestFit",
                    "queue_per_thread": true
                },
                {
                    "task2thread": "BestFit",
                    "queue": "FIFO",
                    "queue_per_thread": true
                },
                {
                    "task2thread": "BFS",
                    "queue": "FIFO",
                    "queue_per_thread": false
                },
                {
                    "task2thread": "SEQR",
                    "queue": "FIFO",
                    "queue_per_thread": true
                }
            ],
            "_pmc_ratios": {
                "L1_miss_ratio": {
                    "dividend": "carmel:::L1D_CACHE",
                    "divisor": "carmel:::L1D_CACHE_REFILL"
                }
            }
        },
        "energy": {
            "pmc_naming": {
                "Synth": {
                    "clk": "CPU-CLOCK",
                    "0x8": "r0x08",
                    "0x14": "r0x14",
                    "0x24": "r0x24",
                    "0x86": "r0x86"
                },
                "Xavier_synth": {
                    "clk": "CPU-CLOCK",
                    "0x8": "INSTRUCTIONS",
                    "0x14": "PERF_COUNT_HW_CACHE_L1I",
                    "0x24": "STALLED-CYCLES-BACKEND"
                },
                "Xavier": {
                    "clk": "CPU-CLOCK",
                    "0x8": "r0x08",
                    "0x14": "r0x14",
                    "0x24": "r0x24",
                    "0x86": "r0x86"
                },
                "Xavier_with_carmel_component": {
                    "clk": "carmel:::CPU_CYCLES",
                    "0x8": "carmel:::INST_RETIRED",
                    "0x14": "carmel:::L1I_CACHE",
                    "0x24": "carmel:::STALL_BACKEND"
                },
                "AMD_Ryzen_7_5800H": {
                   "clk": "CPU-CLOCK",
                    "0x8": "INSTRUCTIONS",
                    "0x14": "PERF_COUNT_HW_CACHE_L1I",
                    "0x24": "STALLED-CYCLES-BACKEND"
                }
            }
        }
    }
}