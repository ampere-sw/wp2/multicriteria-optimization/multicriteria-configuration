{
    "general": {
        "base_dir": "./examples/apps/heat/",
        "working_dir": "./working_dir/",
        "final_dir": "./results/",
        "config_name": "heat.cfg"
    },
    "app": {
        "name": "heat",
        "tdgs": [
            {
                "id": 1,
                "constraints": {
                    "deadline": 60000000,
                    "_deadline":400000000,
                    "energy_budget": 40
                }
            }
        ],
        "tdg_cpp_file": "./tdg.cpp",
        "variants": [
            {
                "dir": "./",
                "build": {
                    "dynamic_mapping": "make heat_var1",
                    "static_mapping": "make heat_var1_static"
                },
                "run": "./heat_var1 test.dat",
                "exec_name": "heat_var1",
                "output_prefix": "normal/heat_var1",
                "iterations": 3,
                "uses_gpu": false
            }
        ]
    },
    "platform": {
        "selected": "Xavier",
        "platforms": {
            "Xavier": {
                "setup": [
                    "sudo nvpmodel -m 0",
                    "sudo jetson_clocks --fan",
                    "sudo jetson_clocks",
                    "echo 'userspace' | sudo tee /sys/devices/system/cpu/cpufreq/policy0/scaling_governor"
                ],
                "cpu": {
                    "cmd": [
                        "echo {frequency} | sudo tee /sys/devices/system/cpu/cpufreq/policy0/scaling_min_freq",
                        "echo {frequency} | sudo tee /sys/devices/system/cpu/cpufreq/policy0/scaling_max_freq",
                        "echo {frequency} | sudo tee /sys/devices/system/cpu/cpufreq/policy0/scaling_setspeed",
                        {"OMP_NUM_THREADS": "{threads}"}
                    ],
                    "args": {
                        "frequency": [729600, 1190400, 2265600],
                        "threads": [4,8]
                    },
                    "other_way_to_define_args_is_with_ranges": {
                        "threads":{
                            "start": 1,
                            "stop": 10,
                            "step": 1
                        }
                    },
                    "wait": 200,
                    "num_threads": "threads"
                },
                "gpu": {
                    "cmd": [
                        "echo {frequency} | sudo tee /sys/devices/17000000.gv11b/devfreq/17000000.gv11b/max_freq",
                        "echo {frequency} | sudo tee /sys/devices/17000000.gv11b/devfreq/17000000.gv11b/min_freq",
                        "echo {frequency} | sudo tee /sys/devices/17000000.gv11b/devfreq/17000000.gv11b/max_freq"
                    ],
                    "args": {
                        "frequency": [
                            624750, 675750, 828750, 905250, 1032750, 1198500, 1236750, 1338750, 1377000
                        ]
                    },
                    "wait": 200
                },
                "order_by": [
                    "cpu.freq",
                    "cpu.threads",
                    "gpu.freq"
                ],
                "_not_using_group_by": "cpu.threads",
                "cleanup": [
                    "echo 'ondemand' | sudo tee /sys/devices/system/cpu/cpufreq/policy0/scaling_governor"
                ]
            },
            "Synth": {
                "setup": "echo getting Xavier ready for profiling",
                "cpu": {
                    "cmd": [
                        "echo setting min cpu as {frequency}kHz",
                        "echo setting max cpu as {frequency}kHz"
                    ],
                    "args": {
                        "frequency": [729600, 1190400, 2265600]
                    },
                    "wait": 0,
                    "num_threads": 8
                },
                "gpu": {
                },
                "cleanup": [
                    "echo reverting mode to 'ondemand'"
                ]
            },
            "Xavier_synth": {
                "setup": "echo getting Xavier ready for profiling",
                "cpu": {
                    "cmd": [
                        "echo setting min cpu as {frequency}kHz",
                        "echo setting max cpu as {frequency}kHz",
                        "echo setting number of threads as {threads}"
                    ],
                    "args": {
                        "frequency": [729600, 1190400, 2265600],
                        "threads": [8]
                    },
                    "wait": 0,
                    "num_threads": "threads"
                },
                "gpu": {
                    "cmd": "echo setting gpu as {frequency}MHz",
                    "args": {
                        "frequency": [
                            1600
                        ]
                    },
                    "wait": 0
                },
                "order_by": [
                    "cpu.threads",
                    "cpu.frequency",
                    "gpu.frequency"
                ],
                "group_by": "cpu.threads",
                "cleanup": [
                    "echo reverting mode to 'ondemand'"
                ]
            },
            "AMD_Ryzen_7_5800H": {
                "setup": [
                    "echo '1' | sudo tee /proc/sys/kernel/perf_event_paranoid > /dev/null",
                    "modprobe cpufreq_userspace",
                    "sudo cpupower frequency-set --governor userspace > /dev/null"
                ],
                "cpu": {
                    "cmd": [
                        "sudo cpupower --cpu all frequency-set --freq {frequency}KHz > /dev/null",
                        {"OMP_NUM_THREADS": "{threads}"}
                    ],
                    "args": {
                        
                        "frequency": {
                            "start": 500,
                            "stop": 4100,
                            "step": 500
                        },
                        "threads": {
                            "start": 2,
                            "stop": 18,
                            "step": 2
                        }
                    },
                    "wait": 200,
                    "num_threads": "threads"
                },
                "gpu": {},
                "order_by": [
                    "cpu.frequency",
                    "cpu.threads"
                ],
                "group_by": "cpu.frequency",
                "cleanup": [
                    "sudo cpupower frequency-set --governor ondemand > /dev/null"
                ]
            },
            "default": {
                "cpu": {
                    "cmd": ":"
                },
                "gpu": {
                    "cmd": ":"
                }
            }
        }
    },
    "optimization": {
        "RT": {
            "mapping_algorithms": [
                {
                    "task2thread": "BestFit",
                    "queue": "BestFit",
                    "queue_per_thread": true
                },
                {
                    "task2thread": "BestFit",
                    "queue": "FIFO",
                    "queue_per_thread": true
                },
                {
                    "task2thread": "BFS",
                    "queue": "FIFO",
                    "queue_per_thread": false
                },
                {
                    "task2thread": "SEQR",
                    "queue": "FIFO",
                    "queue_per_thread": true
                }
            ],
            "_pmc_ratios": {
                "L1_miss_ratio": {
                    "dividend": "carmel:::L1D_CACHE",
                    "divisor": "carmel:::L1D_CACHE_REFILL"
                }
            }
        },
        "energy": {
            "pmc_naming": {
                "Synth": {
                    "clk": "CPU-CLOCK",
                    "0x8": "INSTRUCTIONS",
                    "0x14": "PERF_COUNT_HW_CACHE_L1I",
                    "0x24": "STALLED-CYCLES-BACKEND"
                },
                "Xavier_synth": {
                    "clk": "CPU-CLOCK",
                    "0x8": "INSTRUCTIONS",
                    "0x14": "PERF_COUNT_HW_CACHE_L1I",
                    "0x24": "STALLED-CYCLES-BACKEND"
                },
                "Xavier": {
                    "clk": "CPU-CLOCK",
                    "0x8": "r0x08",
                    "0x14": "r0x14",
                    "0x24": "r0x24",
                    "0x86": "r0x86"
                },
                "Xavier_with_carmel_component": {
                    "clk": "carmel:::CPU_CYCLES",
                    "0x8": "carmel:::INST_RETIRED",
                    "0x14": "carmel:::L1I_CACHE",
                    "0x24": "carmel:::STALL_BACKEND"
                },
                "AMD_Ryzen_7_5800H": {
                   "clk": "CPU-CLOCK",
                    "0x8": "INSTRUCTIONS",
                    "0x14": "PERF_COUNT_HW_CACHE_L1I",
                    "0x24": "STALLED-CYCLES-BACKEND"
                }
            }
        }
    }
}