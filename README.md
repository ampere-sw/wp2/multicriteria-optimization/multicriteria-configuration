# multicriteria-optimization-flow

This project is one of the contributions to WP2, in the context of providing a multi-criteria optimization configuration. It contains two tools:

- Profiler
- multi-criteria optimization configuration

Each section describes what to do use each of the provided tools.

## Requirements
These are the requirements to run the time analysis API:
* Python 3.10
* time-predictability: install via <code>pip install -r requirements.txt</code>
* Extrae: https://gitlab.bsc.es/ampere-sw/wp2/extrae
* LLVM (AMPERE version): https://gitlab.bsc.es/ampere-sw/wp2/llvm
* clang: included in AMPERE version of LLVM
* tdg-instrumentation script: https://gitlab.bsc.es/ampere-sw/wp2/tdg-instrumentation-script

## Installation
The first step is to clone this project. Then, one can either create a virtual environment (to keep the project contents contained in a controlled environment) or simply jump this step to the next one and install the command line tools global to the system. 

#### Suggestion: use a Python Virtual Environment
To install this tool we suggest that a [python virtual environment](https://docs.python.org/3/library/venv.html) is used to keep the dependencies of the project localized. Inside the project folder run the following commands.

If in windows:
```PowerShell
> python3 -m venv <env_name>
> <env_name>\Scripts\activate
```

If in linux:
```sh
$ python3 -m venv <env_name>
$ source <env_name>/bin/activate
```

This will then put your command line with the format:
```sh
(env_name) \my\current\dir> 
or 
(env_name) /my/current/dir$ 
```

This means that all the Note that whenever you open a new terminal you will have to run again the command <code>source <name_for_the_env_folder/bin/activate</code> to again enter the environment variable. In the examples below a virtual environment named '.venv' is used. Therefore, every time an example of the command line appears with <code>(.venv) /a/path$</code> means that we are inside that environment.

#### Install the executables

Now we have to install the tools, including its dependencies. Since we are inside a virtual environment, the dependencies will only be installed in this environment, and not globally, thus not polluting the system.

Just run the following command and all dependencies are installed, and the executables are created and added to this environment.

```sh
$ pip install -r requirements.txt
$ python setup.py build
```

From this point forward one can use the tool as a command line tool or as an API.

## Usage
There are two ways of using this project: as a module or as command line tools. Please see the one that fits best for your project.

### As a module
Import the tool to be used and follow the instructions in the subsections below.

#### Profiler tool
This module is used to perform a profiling over a set of executables, with different power_mode modes, over a target platform. 
It takes as input a "base configuration", in which the previous info (and other attributes) are specified. For more information regarding the base configuration file please see section [Base configuration File](#baseconfig).

The use of this tool is very simple:
```py
from multicriteria.profiling.profiler import profile
...
base_config = '/path/to/baseconfig.json'
profile(base_config)
```
The tool will execute as specified in the configuration and will generate a set of files: 

1. an intermediate configuration json file
2. a set of 'TDG.json' files that contain the performance results obtained from the executions

These files can then be used by the multi-criteria optimization configuration tool.
Note that this tool is automatically invoked by the multi-criteria optimization configuration tool. More information in the following section.

#### multi-criteria Optimization Configuration tool
This module is used to perform the complete flow of the multi-criteria optimization configuration, from the profiling phase to the specification of the most fitting configuration per criterion.
It takes as input a "base configuration" or an "intermediate configuration". For more information regarding configuration files please see section [Base Configuration File](#baseconfig) and [Intermediate Configuration File](#interconfig).

The use of this tool is very simple, and the function to use is dependent on the type of configuration it starts with:
```py
import multicriteria.manager
...
base_config = '/path/to/baseconfig.cfg'
manager.manage_from_base(base_config)

########### OR #############
inter_config = '/path/to/intermediate_config.cfg'
manage_from_intermediate(inter_config)
```

The tool will execute as specified in the configuration and will generate a set of files in the final/results folder:

1. a TDG.json file with the best configuration for time performance
2. a TDG.json file with the best configuration for energy performance

### As a command line tool
#### Profiler tool
To execute as a command line, after installation, one can run the profile-codes tool without argument to obtain the command options:
```sh
(.venv) my/work/place$ profile-codes
usage: <baseconfig.cfg>
```
The tool requires a base configuration json file as input. For more information regarding the base configuration file please see section [Base configuration File](#baseconfig). The output is the same as if the tool  was used as a module.

#### Multi-criteria Optimization Configuration tool
To execute as a command line, after installation, one can run the multi-criteria-config tool without argument to obtain the command options:
```sh
(.venv) my/work/place$ multi-criteria-config
usage: [options] <config.cfg>
     -=================================== Options ===========================-
    | option | argument  | description                                        |
     --------|-----------|----------------------------------------------------
    | -h     | n/a       | show this message                                  |
    | -i     | n/a       | start process from an intermediate configuration   |
     -=======================================================================-
```

The tool requires a base configuration json or an intermediate configuration file as input. To use an intermediate configuration file one has to specify the -i option. For more information regarding configuration files please see section [Base Configuration File](#baseconfig) and [Intermediate Configuration File](#interconfig). The output is the same as if the tool was used as a module.


## Base Configuration File
<a name="baseconfig"></a>
The base configuration file requires a specific JSON structure. The following is the main structure of a configuration (for a specific example please see examples/fixtures/base_configs/heat.cfg):

```
{
    "num_threads": int,
    "multiple_tdgs": bool,
    "app_name": str,
    "base_dir": dir_path,
    "working_dir": dir_path,
    "final_dir": dir_path,
    "config_name": str,
    "constraints":{
        "deadline": int,
        "energy_budget": int
    },
    "power_modes": [ "<power_mode_name>",...],
    "platform_settings": {
        "selected": "<hardware_name>",
        "platforms":{
            "<hardware_name>": { 
                "power_modes": {
                    "<power_mode_name>":{
                        "command": str_command_to_change_power_mode,
                        "wait": int
                    },
                    ...
                }
            },
            ...
        }
    },
    "executables": [
        {
            "dir": dir_path,
            "command": str_command_to_run_exec,
            "exec_name": str,
            "output_prefix": relative_path_with_prefix,
            "iterations": int
        },
        ...
    ],
    "RT": {
        "mapping_algorithms": [
            {
                "task2thread": str,
                "queue": str,
                "queue_per_thread": bool
            },
            ...
        ]
    },
    "energy": {
        ...
    }
}
```
The following table explains each property of the configuration:

| **Property**       | **Parent Property** | **Type**           | **Description**                                                                                                                                                                             | **Default**            |
|--------------------|---------------------|--------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|------------------------|
| num_threads        |                     | int                | number of OpenMPthreads available                                                                                                                                                           | 1                      |
| multiple_tdg       |                     | bool               | specify if the TDG files have multiple TDGs inside                                                                                                                                          | false                  |
| app_name           |                     | string             | name of the target application                                                                                                                                                              |                        |
| base_dir           |                     | string             | directory representing the base directory for the executing environment                                                                                                                     |                        |
| working_dir        |                     | string             | directory in which the intermediate configuration and the resulting tdg files will be generated                                                                                             |                        |
| final_dir          |                     | string             | final destination of the selected configurations for each criteria                                                                                              |                        |
| config_name        |                     | string             | name for the intermediate configuration file                                                                                                                                                |                        |
| constraints        |                     | {}                 | object containing a set of constraints the criteria must take into account                                                                                                                  |                        |
| deadline           | constraints         | int                | maximum execution time of a TDG                                                                                                                                                             | -1 (no constraint)     |
| energy_budget      | constraints         | int                | maximum energy budget a TDG can have                                                                                                                                                        | -1 (no constraint)     |
| power_modes        |                     | [power_mode_name+]  | array of ints or strings that map to the power_modes available inside a "hardware" specification                                                                                            |                        |
| platform_settings  |                     | {}                 | platforms specification and settings (such as the current target hardware)                                                                                                                  |                        |
| selected           | platform_settings   | <hardware_name>    | name of the selected hardware (defined inside "platforms")                                                                                                                                  |                        |
| platforms          | platform_settings   | {<hardware_name>+} | object in which each key->value represent the name of a hardware (key) and its specification (value)                                                                                        |                        |
| <hardware_name>    | hardware            | {}                 | specification of a hardware named '<hardware_name>'                                                                                                                                         |                        |
| power_modes        | <hardware_name>     | {}                 | an object with a set of power_modes that map to the list of power_modes previously listed. Not all power_modes have to be specified, but the existing ones must match the ones in the list. |                        |
| <power_mode_name>   | power_modes         | {power_mode_name+}  | an object named "<power_mode_name>" that specifies how to set the hardware to the specified power_mode                                                                                        |                        |
| command            | <power_mode_name>    | str                | command to execute to change the target hardware to that power_mode                                                                                                                          |                        |
| wait               | <power_mode_name>    | int                | time to wait after running the command to change the power_mode, before starting the profiling                                                                                               | 0 (no wait)            |
| executables        |                     | [<executable>]     | list of executables and how to run them                                                                                                                                                     |                        |
| dir                | <executable>        | str                | path, relative to base_dir, where the executable is located (a cd <base_dir>/<dir> will be performed prior to execution)                                                                    |                        |
| command            | <executable>        | str                | command to execute the executable (inside the specified dir)                                                                                                                                |                        |
| exec_name          | <executable>        | str                | name for this executable                                                                                                                                                                    |                        |
| output_prefix      | <executable>        | str                | relative path and output prefix for the resulting tdg ".json" file.  To this name is appended the power_mode in which the executable was executed                                            |                        |
| iterations         | <executable>        | int                | number of times to execute the executable                                                                                                                                                   |                        |
| RT                 |                     | {}                 | set of properties for the timing criterion                                                                                                                                                  |                        |
| mapping_algorithms | RT                  | [<map_algorithm>+] | a list of mapping algorithms to use in the mapping exploration phase                                                                                                                        | pre-defined algorithms |
| task2thread        | <map_algorithm>     | str                | name of the heuristic to be used for the task to thread mapping phase                                                                                                                       |                        |
| queue              | <map_algorithm>     | str                | name of the heuristic to be used for the queue to thread allocation phase                                                                                                                   |                        |
| queue_per_thread   | <map_algorithm>     | bool               | specifies if the heuristics combination work with a single queue or with a queue per thread approach                                                                                        |                        |
| energy             |                     | {}                 | object containing properties for the energy criterion                                                                                                                                       |                        |

When running the "profile-codes" tool with a base configuration, this tool is responsible to generate the intermediate configuration and the resulting .json TDG files in the "working_dir" location.

## Intermediate Configuration File
<a name="interconfig"></a>
The intermediate configuration file requires a specific JSON structure. This configuration file is used to guide the multi-criteria optimization flow and is continuously updated by the tool. The following is the main structure of a configuration (for a specific example please see examples/fixtures/intermediate_configs/heat/heat.cfg):

```
{
    "num_threads": int,
    "multiple_tdgs": bool,
    "app_name": str,
    "base_dir": str,
    "working_dir": str,
    "output_dir": str,
    "constraints": {
        "deadline": int,
        "energy_budget": int
    },
    "selected": {
        "RT": int,
        "energy": int
    },
    "power_modes": {
        "RT": [<power_mode_name>,...],
        "energy": [<power_mode_name>,...]
    },
    "optimization_order": {
        "RT": [int+],
        "energy": [int+]
    },
    "tdgs": [
        {
            "file": str,
            "id": str,
            "RT": {
                "mapping_algorithms": [
                    {
                        "task2thread": str,
                        "queue": str,
                        "queue_per_thread": bool
                    },
                    ...
                ],
            },
            "energy": {
                "power_mode": <power_mode_name>
            }
        },
        ...
    ]
}
```

The following table explains each property of the configuration:


| **Property**       | **Parent Property** | **Type**            | **Description**                                                                                                                                                                                                                                                                          | **Default**            |
|--------------------|---------------------|---------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|------------------------|
| num_threads        |                     | int                 | number of OpenMPthreads available                                                                                                                                                                                                                                                        | 1                      |
| multiple_tdg       |                     | bool                | specify if the TDG files have multiple TDGs inside                                                                                                                                                                                                                                       | false                  |
| app_name           |                     | string              | name of the target application                                                                                                                                                                                                                                                           |                        |
| base_dir           |                     | string              | directory representing the base directory for this configuration and the existing TDGs.  The files in this folder will be constantly updated during execution, unless the "working_dir" is specified.                                                                                    |                        |
| working_dir        |                     | string              | directory to which the contents of base_dir will be copied and used.  This property allows the tool to work in the contents of "base_dir" without changing the contents of that folder. Use this property if you intent to keep the original versions of the configuration and TDG files | none                   |
| output_dir         |                     | string              | final destination of the selected configurations for each criteria                                                                                                                                                                                                                       |                        |
| config_name        |                     | string              | name for the intermediate configuration file                                                                                                                                                                                                                                             |                        |
| constraints        |                     | {}                  | object containing a set of constraints the criteria must take into account                                                                                                                                                                                                               |                        |
| deadline           | constraints         | int                 | maximum execution time of a TDG                                                                                                                                                                                                                                                          | -1 (no constraint)     |
| energy_budget      | constraints         | int                 | maximum energy budget a TDG can have                                                                                                                                                                                                                                                     | -1 (no constraint)     |
| selected           |                     | {}                  | object containing the position of the best TDG configuration per criterion                                                                                                                                                                                                               |                        |
| RT                 | selected            | int                 | position in the "tdgs" property considered as the best configuration for timing                                                                                                                                                                                                          | -1 (not selected)      |
| energy             | selected            | int                 | position in the "tdgs" property considered as the best configuration for energy                                                                                                                                                                                                          | -1 (not selected)      |
| power_modes        |                     | {}                  | object containing the power_modes still available for the tool to experiment for each criterion                                                                                                                                                                                          |                        |
| RT                 | power_modes         | [<power_mode_name>+] | power_modes available for the tool to experiment for timing                                                                                                                                                                                                                              |                        |
| energy             | power_modes         | [<power_mode_name>+] | power_modes still available for the tool to experiment for each criterion                                                                                                                                                                                                                |                        |
| optimization_order |                     | {}                  | object containing the TDG indices the tool optimized and selected for a given criteria, in a given power_mode                                                                                                                                                                             |                        |
| RT                 | optmization_order   | [int+]              | TDG indices the tool optimized and selected for timing in a given power_mode                                                                                                                                                                                                              | [] (no index selected) |
| energy             | optmization_order   | [int+]              | TDG indices the tool optimized and selected for energy in a given power_mode                                                                                                                                                                                                              | [] (no index selected) |
| tdgs               |                     | [<tdg_config>]      | a set of tdg configurations that will be used for the exploration                                                                                                                                                                                                                        |                        |
| file               | <tdg_config>        | str                 | TDG json file path, relative to "base_dir"                                                                                                                                                                                                                                               |                        |
| id                 | <tdg_config>        | str                 | a name or id for this TDG file                                                                                                                                                                                                                                                           |                        |
| RT                 | <tdg_config>        | {}                  | set of properties for the timing criterion                                                                                                                                                                                                                                               |                        |
| mapping_algorithms | RT                  | [<map_algorithm>+]  | a list of mapping algorithms to use in the mapping exploration phase                                                                                                                                                                                                                     | pre-defined algorithms |
| task2thread        | <map_algorithm>     | str                 | name of the heuristic to be used for the task to thread mapping phase                                                                                                                                                                                                                    |                        |
| queue              | <map_algorithm>     | str                 | name of the heuristic to be used for the queue to thread allocation phase                                                                                                                                                                                                                |                        |
| queue_per_thread   | <map_algorithm>     | bool                | specifies if the heuristics combination work with a single queue or with a queue per thread approach                                                                                                                                                                                     |                        |
| energy             | <tdg_config>        | {}                  | object containing properties for the energy criterion                                                                                                                                                                                                                                    |                        |
## Examples
To see how the tools can be used and see them running, two examples are provided inside the "examples" folder. The first example starts from the profiling phase, where the user must first compile the benchmark in order to create the executables and then use the tool. The second example starts directly from an intermediate configuration, which already contains the necessary information and results to perform the exploration.

### Example 1: From profiling phase to multi-criteria optimization
This first example uses the "heat" benchmark and before using the profiling or the multi-criteria tools it is necessary to create the executable.
These are the steps the example will follow:

1. Compile heat
2. Update heat base file so paths, executables and power modes are correct
3. run profile tool with base config, will obtain the working dir folder
4. see intermediate results
5. run multi-criteria-config from intermediate file
6. see final results in results folder

Note that one can go directly from step 2 to 5, as the multi-criteria-config tool accepts a base configuration as input, and will automatically run the profile tool. In this example we are following the suggested steps so the viewer can see what is happening between stages.
The following sections describe in detail the steps.

#### 1. **Compile heat**
To compile the heat benchmark, as is, we first need to go inside the example's folder:

```bash
$ cd <repo_dir>/examples/apps/heat
```

Then just execute the make tool as follow:
```
> make heat_extrae
```
Note that for this step both LLVM and Extrae headers/libs are necessary to compile. The Makefile assumes that the following environment libraries (and paths) exist:

* $AMPERE_PATH - path to the installed programs related to ampere
* $OMP_PATH - path that leads to OpenMP libraries, which should be from  AMPERE version of LLVM
* 'clang' executable - should map to AMPERE version of LLVM

If the system is configured differently, please adapt the makefile so the paths and executables point to the correct configuration.

This will create an executable and the base TDG file that will be used in the next steps.

Before continuing return to the base folder of the project:

```bash
$ cd -
  - OR -
$ cd ../../../
  - OR -
$ cd <repo_dir>
```

#### 2. **Update Base Config File**
The fixtures folder contains an example of a base configuration file for the heat benchmark, more specifically for the "heat_extrae" executable that was created in the previous step. More specifically, the configuration file path is: <code>examples/fixtures/base_configs/heat.cfg</code>.

In principle, almost all the predefined configuration defined in the file can be used. The properties that might have to be changed are:

* working_dir and final_dir: to use other locations
* num_threads: to use a different number of threads
* constraint: to define different level of constraints
* power_modes and platform_settings: to add a setting customized to your system
* executables.dir and executables.command: if you changed the executables from the original location

Regarding the power_modes and platform_settings, these two might be the ones more important to be adapted to your system. For an NVIDIA Jetson AGX Xavier board, the example already contains a setting for this board. Just set the platform_settings.selected property as "Xavier". The example also contains another hardware setting for a Linux computer that contains the "cpufreqctl" tool (from [cpufreq](https://github.com/konkor/cpufreq)) to adapt the power mode. Both of them have three power_mode modes, which were named "powersave", "balanced" and "performance" to map directly to the list of power_modes in the "power_modes" property. If your system is neither a Xavier nor a Linux computer with the cpufreq tool, then please specify in "platforms" your hardware settings and how to change the power_mode via command line. If you prefer a simple approach, where only one mode is used, you can set the "selected" to "default", which is an example of an hardware that does nothing to change power_mode mode and will only provide one result. The example contains that configuration example, and it is similar to the following:
```
"default":{
    "power_modes": {
        "balanced":{
            "command": ":" //no-op command
        }
    }
}
```
#### 3. **Run profile tool**
Now that the base configuration is setted, simply run the "profile-codes" tool with the configuration:

```
$ profile-codes examples/fixtures/base_configs/heat.cfg
```

After its execution, the tool will have generated a set of files inside the "working_dir" directory. 

#### 4. **Intermediate Results**
The profile tool will generate the intermediate results that will be used by the multi-criteria optimization tool. The structure will be similar to the following:

```bash
$ tree working_dir/
working_dir/
├── heat.cfg
└── normal
    ├── heat_pm_balanced.json
    ├── heat_pm_performance.json
    └── heat_pm_powersave.json

``` 
Where the heat.cfg file is the intermediate configuration used in the multi-criteria optimization phase, and the .json files contain the TDGs with the measurements obtained during the profiling. All of these files will be updated during the optimization phase.

#### 5. **Run multi-criteria-config from intermediate file**
We can now run the multi-criteria optimization tool by using the command:
```bash
$ multi-criteria-config -i working_dir/heat.cfg
```
Note that we used the -i option so we can tell the tool that it is starting its work from the intermediate file. As explained previously, instead of the intermediate file we could have given the multi-criteria-config tool the base configuration file, instead of going steps 3 and 4. The decision of the examples' flow was purely for showing purposes.   

The tool will then analyse the existing TDGs, and apply the necessary optimizations in order to search for an optimized configuration, per criterion, that fits the specified constraints. 

#### 6. **Final Results**
The final results,obtained from the previous step, can be seen in the folder that was specified in the base configuration as "final_dir". Its contents will be similar to the following example:

```bash
$ tree results/
results/
└── normal
    └── heat_pm_powersave.json
```

This means that the TDG in "heat_pm_powersave.json" have the best configuration for the timing criterion that fits the specified constrains. 

## Example 2: multi-criteria optimization (Starting from intermediate config.)
For this example, in principle, nothing has to be adapted. The multi-criteria-config tool can be executed with the example provided in the directory <code>examples/fixtures/intermediate_configs/heat/heat.cfg<code>. In this example no compilation nor profiling is necessary, as the TDGs provided are already annotated with measurements.

Note that to guarantee that the contents on this folder are not overwritten, we use the "working_dir" property in the configuration so the contents of the folder are copied to that directory. So it is possible to safely use this intermediate configuration, which will not be overwritten during execution.

To execute the example, simply run:

```bash
$ multi-criteria-config -i examples/fixtures/intermediate_configs/heat/heat.cfg
```

The output will be similar to the previous example. 

## Authors and acknowledgment
ISEP Contributors:
  - Tiago Carvalho
  - Luís Miguel Pinho
  - Mohammad Gharajeh

## License
Copyright 2022 Instituto Superior de Engenharia do Porto. Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at

	http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and limitations under the License.

## Project status
Under Development in the context of the [AMPERE Project](https://ampere-euproject.eu/).


## Related Repositories
Multi-criteria configuration: https://gitlab.bsc.es/ampere-sw/wp2/multicriteria-optimization/multicriteria-configuration
