time-predictability @git+https://gitlab.bsc.es/ampere-sw/WP3/time-predictability.git
power-modeling @git+https://gitlab.bsc.es/ampere-sw/WP3/power-modeling.git
catkin-pkg==0.5.2
empy==3.3.4
lark==1.1.5
python-dotenv==1.0.0 