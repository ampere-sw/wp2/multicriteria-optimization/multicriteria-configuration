#
# Copyright 2022 Instituto Superior de Engenharia do Porto
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# 	http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

from setuptools import setup, find_packages

with open('requirements.txt') as f:
    requirements = f.readlines()

setup(
    name='multi-criteria-configuration',
    version='0.1',
    packages=find_packages(include=['multicriteria', 'multicriteria.*']),
    license='Licensed under the Apache License, Version 2.0. Copyright 2022 Instituto Superior de Engenharia do Porto.',
    long_description=open('README.md').read(),
    url='https://gitlab.bsc.es/ampere-sw/wp2/multicriteria-optimization/multicriteria-configuration.git',
    entry_points={
        'console_scripts':[
            'multi-criteria-config = multicriteria.tools.cmd:multicriteria_config_flow',
            'single-criterion-opt  = multicriteria.tools.cmd:singlecriterion_opt',
            'profile-codes         = multicriteria.tools.cmd:profile_codes',
            'slg-main-extrae       = multicriteria.tools.cmd:slg_extrae_in_main',
            'slg-make-extrae       = multicriteria.tools.cmd:slg_extrae_in_make'
        ]
    }
)