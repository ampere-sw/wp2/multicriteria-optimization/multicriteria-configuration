#
# Copyright 2022 Instituto Superior de Engenharia do Porto
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# 	http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

import json
import os
from typing import Dict
from multicriteria.configs.multi_crit_config_constants import *
from time_predictability.meta_parallel.tdg import TDG
from time_predictability.meta_parallel.app import App

def clear_results(config: Dict):
    base_dir = config[GENERAL][BASE_DIR]
    for tdg_config in config[APP][TDGS_FILES]:
        #first load the TDG
        tdg_file = tdg_config[FILE]
        file = os.path.join(base_dir,tdg_file)
        # tdgs = TDG.read_json(file,tdg_config[ID],config[MULTIPLE_TDGS])
        app: App = App.read_json(file)
        
        #nothing to be done, just remove the results when generating json
        out_file_name = tdg_file
        out_file_path = os.path.join(base_dir,out_file_name)
        with open(out_file_path, 'w') as out_file:
            app_json = app.to_json(keep_results=False)
            json.dump(app_json,out_file,indent=2)


def clear_metrics(config: Dict):
    base_dir = config[GENERAL][BASE_DIR]
    for tdg_config in config[APP][TDGS_FILES]:
        #first load the TDG
        tdg_file = tdg_config[FILE]
        file = os.path.join(base_dir,tdg_file)
        # tdgs = TDG.read_json(file,tdg_config[ID],config[MULTIPLE_TDGS])
        app: App = App.read_json(file)
        
        #nothing to be done, just remove the results when generating json
        out_file_name = tdg_file
        out_file_path = os.path.join(base_dir,out_file_name)
        with open(out_file_path, 'w') as out_file:
            app_json = app.to_json(keep_metrics=False)
            json.dump(app_json,out_file,indent=2)
