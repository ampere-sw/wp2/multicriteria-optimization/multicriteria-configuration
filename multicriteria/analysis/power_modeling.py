


import json
import os
from typing import Dict
from multicriteria.analysis.config.pmc_per_freq import get_cpu_pmcs, get_device_pmcs, get_cpu_pmc_format, get_device_pmc_format
from multicriteria.configs.multi_crit_config_constants import *
from time_predictability.meta_parallel.tdg import TDG, Task
from time_predictability.meta_parallel.app import App
import power_model
from power_model.common.utils import load_json


base_dir_name = os.path.dirname(__file__)
DEFAULT_CONFIG_DIR = os.path.join(base_dir_name, 'config')
DEFAULT_PLATFORM   = os.path.join(DEFAULT_CONFIG_DIR, 'platform.json')
DEFAULT_CPU_MODEL  = os.path.join(DEFAULT_CONFIG_DIR, 'cpu_model.json')
DEFAULT_GPU_MODEL  = os.path.join(DEFAULT_CONFIG_DIR, 'gpu_model.json')

def estimate_power(config):

    model = power_model.PowerModel(
        platform_json= DEFAULT_PLATFORM, #'./config/platform.json',
        cpu_model_json= DEFAULT_CPU_MODEL, #'./config/cpu_model.json',
        gpu_model_json= DEFAULT_GPU_MODEL #'./config/gpu_model.json'
    )

    accepted_frequencies = model.cpu_model['model'].keys()


    base_dir = config[GENERAL][BASE_DIR]
    # pmc_naming = config[OPTIMIZATION][CONFIGURATION][ENERGY]['pmc_naming']
    # pmc_naming = pmc_naming[config[COMPILATION][PLATFORM][NAME]]
    platform = config[COMPILATION][PLATFORM][NAME]
    # cpu_pmc_naming = config[COMPILATION][PLATFORM][CPU][PERFORMANCE_COUNTERS]

    # gpu_pmc_naming = None #lazy loading

    for tdg_config in config[APP][TDGS_FILES]:
        if not tdg_config[PROFILED]:
            continue
        tdg_file = tdg_config[FILE]
        file = os.path.join(base_dir,tdg_file)
        app: App = App.read_json(file)
        
        freq_cpu_khz = tdg_config[CPU]['frequency']
        freq_cpu = freq_cpu_khz*1000 #khz to hz

        cpu_feats = get_cpu_pmcs(freq_cpu_khz)

        if cpu_feats == None:
            print("[ENERGY-ANALYSIS] CPU Frequency",freq_cpu,"is not supported by the power model, will ignore TDG file",tdg_file)
            continue
        
        
        freq_gpu_khz = -1
        freq_gpu = -1
        gpu_feats = None
        if GPU in tdg_config and "frequency" in tdg_config[GPU]:
            freq_gpu_khz = tdg_config[GPU]['frequency']
            freq_gpu = freq_gpu_khz*1000 #khz to hz

            gpu_feats = get_device_pmcs(GPU,freq_gpu_khz)

            if gpu_feats == None:
                print("[ENERGY-ANALYSIS] GPU Frequency",freq_gpu,"is not supported by the power model, will ignore TDG file",tdg_file)
                continue

        print("[ENERGY-ANALYSIS] Estimating power for each task in file ",tdg_file)
        num_tdgs = str(len(app.tdgs))
        for i, tdg in enumerate(app.tdgs):
            print("[ENERGY-ANALYSIS]  ("+str(i+1)+"/"+num_tdgs+") estimating energy consumption for TDG ",tdg.id)
            energy = 0
            task: Task
            for task in tdg.tasks.values():
                if task.is_virtual:
                    continue
                sample_per = task.metrics.avg_time() * 10**-9 #from ns to s
                if task.spec != None and task.spec == "GPU":
                    # gpu_pmc_naming = config[COMPILATION][PLATFORM][GPU][PERFORMANCE_COUNTERS] if gpu_pmc_naming == None else gpu_pmc_naming
                    pmcs_gpu = {
                        feat: {0:task.metrics.get_pmc_metrics(get_device_pmc_format(platform,'GPU',feat))['avg']} \
                              for feat in gpu_feats
                    }
                    power = model.power_gpu( pmcs_gpu, freq_gpu, sample_per)
                elif task.spec != None and task.spec == "FPGA":
                    #for now do nothing
                    continue
                else:
                    pmcs_cpu = {
                        'clk': task.metrics.get_pmc_metrics(get_cpu_pmc_format(platform,'clk'))['avg'],
                        'conf_pmcs': {
                            feat: task.metrics.get_pmc_metrics(get_cpu_pmc_format(platform,feat))['avg'] \
                                for feat in cpu_feats if feat != 'clk' 
                            # specific_pmc: task.metrics.get_pmc_metrics(pmc_naming[specific_pmc])['avg'],
                            # '0x14': task.metrics.get_pmc_metrics(pmc_naming['0x14'])['avg'],
                            # '0x24': task.metrics.get_pmc_metrics(pmc_naming['0x24'])['avg']
                        }
                    }
                    core_i = 0 #task.static_thread if task.static_thread > -1 else 0
                    power = model.power_cpu_core_i(core_i,pmcs_cpu, freq_cpu, sample_per)
                task.metrics.set_custom_metric('power',power)
                energy += power * (task.metrics.wcet() * 10**-9)# sample_per_cpu #or WCET?
            tdg.set_metric("energy",energy)

        
        out_file_name = tdg_file
        out_file_path = os.path.join(base_dir,out_file_name)
        with open(out_file_path, 'w') as out_file:
            app_json = app.to_json(keep_results=True)
            json.dump(app_json,out_file,indent=2)