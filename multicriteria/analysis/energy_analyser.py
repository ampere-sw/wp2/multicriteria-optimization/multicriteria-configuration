


import json
import os
from typing import Dict
from multicriteria.configs.multi_crit_config_constants import *
from time_predictability.meta_parallel.tdg import TDG

def analyse(config_name: str) -> Dict:
    config: Dict
    with open(config_name, 'r') as cfgfile:
        config = json.load(cfgfile)
    
    candidates = config[OPTIMIZATION][OPTIMIZATION_ORDER][RT]
    print("[ENERGY-ANALYSIS] Checking candidates",candidates)
    #see if any candidate is useful, otherwise just clear selected of RT
    for candidate in candidates:
        tdg = config[APP][TDGS_FILES][candidate]
        print("[ENERGY-ANALYSIS] Checking candidate "+tdg[ID])
        if  tdg[CPU]['frequency'] < 2000000 \
            and tdg[CPU]['threads'] <= 6:
            config[OPTIMIZATION][SELECTED][RT] = candidate
            print("[ENERGY-ANALYSIS] "+tdg[ID]+" is within budget.")
            break
    if config[OPTIMIZATION][SELECTED][RT] == -1:
        print("[ENERGY-ANALYSIS] none of the candidates was within budget")
    candidates.clear()
    with open(config_name, 'w') as cfgfile:
        json.dump(config,cfgfile,indent=4)
    
    
