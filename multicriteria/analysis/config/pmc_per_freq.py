def get_cpu_pmcs(freq):
    freq = str(freq)
    if freq not in features_per_freq["CPU"]:
        return None
    return features_per_freq["CPU"][freq]
def get_device_pmcs(device:str,freq):
    device = device.upper()
    if device not in features_per_freq:
        raise Exception("Unknown PMCs for device "+device)
    freq = str(freq)
    if freq not in features_per_freq[device]:
        return None
    return features_per_freq[device][freq]

features_per_freq = {
    "CPU": {
        "729600": ["clk","0x8","0x14","0x24"],
        "1190400": ["clk","0x8","0x14","0x24"],
        "2265600": ["clk","0x8","0x14","0x24"]
    },
    "GPU": {
      "624750": [
            "active_cycles_pm",
            "active_warps_pm",
            "inst_executed",
            "inst_issued1",
            "thread_inst_executed",
            "inst_issued0",
            "l2_subp1_read_tex_sector_queries",
            "l2_subp0_read_tex_sector_queries"
        ],
        "675750": [
            "active_cycles_pm",
            "active_warps_pm",
            "inst_executed",
            "inst_issued1",
            "not_predicated_off_thread_inst_executed",
            "thread_inst_executed",
            "l2_subp0_read_tex_sector_queries",
            "l2_subp1_read_tex_sector_queries"
        ],
        "828750": [
            "active_cycles_pm",
            "active_warps_pm",
            "not_predicated_off_thread_inst_executed",
            "global_load",
            "inst_executed",
            "inst_issued1",
            "l2_subp0_read_tex_sector_queries",
            "l2_subp1_read_tex_sector_queries"
        ],
        "905250": [
            "active_cycles_pm",
            "active_warps_pm",
            "inst_executed",
            "inst_issued1",
            "thread_inst_executed",
            "inst_issued0",
            "l2_subp0_read_tex_sector_queries",
            "l2_subp1_read_tex_sector_queries"
        ],
        "1032750": [
            "active_cycles_pm",
            "active_warps_pm",
            "inst_executed",
            "inst_issued1",
            "thread_inst_executed",
            "inst_issued0",
            "l2_subp1_read_tex_sector_queries",
            "l2_subp0_read_tex_sector_queries"
        ],
        "1198500": [
            "active_cycles_pm",
            "active_warps_pm",
            "inst_executed",
            "inst_issued1",
            "thread_inst_executed",
            "inst_issued0",
            "l2_subp1_read_tex_sector_queries",
            "l2_subp0_read_tex_sector_queries"
        ],
        "1236750": [
            "active_cycles_pm",
            "active_warps_pm",
            "inst_executed",
            "inst_issued1",
            "thread_inst_executed",
            "not_predicated_off_thread_inst_executed",
            "l2_subp1_read_tex_sector_queries",
            "l2_subp0_read_tex_sector_queries"
        ],
        "1338750": [
            "active_cycles_pm",
            "active_warps_pm",
            "global_store",
            "global_load",
            "inst_executed",
            "inst_issued1",
            "l2_subp0_read_tex_sector_queries",
            "l2_subp1_read_tex_sector_queries"
        ],
        "1377000": [
            "active_cycles_pm",
            "active_warps_pm",
            "not_predicated_off_thread_inst_executed",
            "inst_issued0",
            "inst_executed",
            "inst_issued1",
            "l2_subp0_read_tex_sector_queries",
            "l2_subp1_read_tex_sector_queries"
        ]
    },
    "FPGA": {} #no specific performance counters used for FPGA
}

def get_cpu_pmc_format(platform, pmc):
    return get_device_pmc_format(platform,"CPU",pmc)

def get_device_pmc_format(platform:str, device:str, pmc):
    
    pmc = str(pmc)
    pmcs = get_all_pmc_formats(platform,device)
    if pmc not in pmcs:
        raise Exception("Unknown performance counter "+pmc+" for device " + device+ " in platform "+platform+". Available: "+pmcs.keys())
    
    return pmcs[pmc]

def  get_all_pmc_formats(platform, device):
    device = device.upper()
    if platform not in pmc_format:
        raise Exception("Unknown Platform "+platform+". Available platforms: "+pmc_format.keys())
    if device not in pmc_format[platform]:
        print("[PROFILER] Unknown device "+device +" for platform "+platform)
        return {}
    return pmc_format[platform][device]

def get_pmc_name_from_format(platform, device, format):
    device = device.upper()
    if platform not in pmc_format:
        raise Exception("Unknown Platform "+platform+". Available platforms: "+pmc_format.keys())
    if device not in pmc_format[platform]:
        raise Exception("Unknown device "+device +" for platform "+platform)
    for key,value in pmc_format[platform][device].items():
        # print(format,"vs",value)
        if value == format:
            # print("RETURNING "+key)
            return key

pmc_format = {
    "Xavier": {
        "CPU": {
            "clk": "CPU-CLOCK",
            "0x8": "r0x08",
            "0x14": "r0x14",
            "0x24": "r0x24",
            "0x86": "r0x86"
        },
        "GPU": {
            "active_cycles_pm": "100663390",
            "active_warps_pm": "100663391",
            "inst_executed": "100663367",
            "inst_issued1": "100663366",
            "thread_inst_executed": "100663368",
            "inst_issued0": "100663365",
            "l2_subp1_read_tex_sector_queries": "100663314",
            "l2_subp0_read_tex_sector_queries": "100663313",
            "not_predicated_off_thread_inst_executed": "100663369",
            "global_load": "100663379",
            "global_store": "100663380"
        }
    },
    "Synth": {
        "CPU": {
            "clk": "CPU-CLOCK",
            "0x8": "r0x08",
            "0x14": "r0x14",
            "0x24": "r0x24",
            "0x86": "r0x86"
        },
        "GPU": {
            "active_cycles_pm": "100663390",
            "active_warps_pm": "100663391",
            "inst_executed": "100663367",
            "inst_issued1": "100663366",
            "thread_inst_executed": "100663368",
            "inst_issued0": "100663365",
            "l2_subp1_read_tex_sector_queries": "100663314",
            "l2_subp0_read_tex_sector_queries": "100663313",
            "not_predicated_off_thread_inst_executed": "100663369",
            "global_load": "100663379",
            "global_store": "100663380"
        }
    }
}

