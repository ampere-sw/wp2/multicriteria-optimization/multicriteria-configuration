#
# Copyright 2022 Instituto Superior de Engenharia do Porto
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# 	http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#


import json
import os
from typing import Dict
from multicriteria.configs.multi_crit_config_constants import *
from time_predictability.meta_parallel.tdg import TDG
from time_predictability.meta_parallel.app import App

def analyse(config: Dict, makespan_from_static_mapping=False) -> Dict:
    base_dir = config[GENERAL][BASE_DIR]
    # app_name = config[APP][NAME]
    # tdgs = config[APP][TDGS_INFO]
    ratios = []
    if "pmc_ratios" in config[OPTIMIZATION][CONFIGURATION][RT]:
        ratios = config[OPTIMIZATION][CONFIGURATION][RT]["pmc_ratios"]
        ratios = [(name,ratio["dividend"],ratio["divisor"]) for name,ratio in ratios.items()]

    tot_tdgs = len(config[APP][TDGS_FILES])
    for tdg_i , tdg_config in enumerate(config[APP][TDGS_FILES]):
        if not tdg_config[PROFILED]:
            continue
        #first load the TDG
        tdg_file = tdg_config[FILE]
        print("[MULTI-CRIT][ANALYSIS] Analysing file",tdg_i+1,"out of",tot_tdgs,":",tdg_file)
        file = os.path.join(base_dir,tdg_file)
        # tdgs = TDG.read_json(file,tdg_config[ID],config[MULTIPLE_TDGS])
        app: App = App.read_json(file)
        # out_jsons = {}
        

        for tdg in app.tdgs:
            id = "amalthea task "+tdg.amalthea_task_id +"(tdg "+tdg.id+")" if tdg.amalthea_task_id != None else "tdg "+tdg.id
            print("[MULTI-CRIT][ANALYSIS] \tAnalysing",id)

            # tdg.set_path(file)
            # tdg.set_relative_path(tdg_file)
            #calculate wcets
            tdg.calc_tasks_wcet()
            tdg.calc_tasks_avg_time()
            tdg.calc_tasks_pmcs_metrics()
            tdg.volume()
            tdg.avg_makespan()
            tdg.worst_makespan()
            tdg.max_parallelism()
            if makespan_from_static_mapping:
                tdg.calc_makespan_if_mapping()
            if ratios:
                tdg.calc_tasks_pmcs_ratios(ratios)
            #done, save to file
            # out_jsons[key] = tdg.to_json({},{},True)

        out_file_name = tdg_file
        out_file_path = os.path.join(base_dir,out_file_name)
        with open(out_file_path, 'w') as out_file:
            app_json = app.to_json(keep_results=True)
            json.dump(app_json,out_file,indent=2)
            # # if not config[MULTIPLE_TDGS]:
            # #     json.dump(out_jsons['single'],out_file,indent=4)
            # else:
            #     json.dump(out_jsons,out_file,indent=4)