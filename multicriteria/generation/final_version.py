from ast import Dict
import os

from multicriteria.configs.multi_crit_config_constants import *
from time_predictability.meta_parallel.utils import annotateAmaltheaModel

def generate(config: Dict):
    fd = config[GENERAL][OUTPUT_DIR]
    final_tdg = config[APP][OPTIMIZED_TDG]
    amalthea_model = config[APP][AMALTHEA_MODEL]
    final_amalthea_model = os.path.join(fd,os.path.basename(amalthea_model))
    annotateAmaltheaModel(amalthea_model,final_tdg,final_amalthea_model)