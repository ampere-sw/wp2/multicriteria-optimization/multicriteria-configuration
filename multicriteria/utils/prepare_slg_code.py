#
# Copyright 2022 Instituto Superior de Engenharia do Porto
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# 	http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
inf_loop = "for(;;)"
loop_with_iter = "for(int __loop_iter=0;__loop_iter<{};__loop_iter++)"
include_loc = "void"
init_loc = "argv){"
fini_loc = "}"

link_location = "clang"
main_location = "clang"
link_extrae = "-L/opt/ampere/extrae/lib -lomptrace"
include_extrae = "-I/opt/ampere/extrae/include"
old_dwarf_mode = "-fdebug-default-version=3"

def extrae_in_main(main_file,num_iter=1, output_file=None):
    output_file = output_file if output_file != None else main_file
    main_contents: str
    with open(main_file, "r") as main:
        main_contents = main.read()
    
    main_contents = main_contents.replace(inf_loop,loop_with_iter.format(num_iter))
    
    first_func = main_contents.index(include_loc)
    init_extrae = main_contents.index(init_loc) + len(init_loc)
    last_bracket = main_contents.rindex(fini_loc)

    main_contents = main_contents[:first_func] \
        +"#include <extrae.h>\n"+main_contents[first_func:init_extrae] \
        +"Extrae_init();"+main_contents[init_extrae:last_bracket] \
        +"Extrae_fini();"+main_contents[last_bracket:]
    # main_contents = main_contents.replace("void", "#include <extrae.h>\nvoid",1)
    # main_contents = main_contents.replace("argv){", "argv){Extrae_init();",1)
    
    # main_contents = main_contents[:last_bracket]+"Extae_fini();"+main_contents[last_bracket:]
    
    with open(output_file, "w") as output:
        output.write(main_contents)

def extrae_in_make(make_file, output_file=None, old_dwarf=True):
    output_file = output_file if output_file != None else make_file
    make_contents: str
    with open(make_file, "r") as makefile:
        make_contents = makefile.read()
    
    link_code = " " +(link_extrae if not old_dwarf else old_dwarf_mode+" "+link_extrae )
    inc_code = " " + (include_extrae if not old_dwarf else old_dwarf_mode+" "+include_extrae )


    exec_clang_loc = make_contents.find(link_location) + len(link_location)
    main_o_loc = make_contents.find(main_location,exec_clang_loc) + len(main_location)
    
    make_contents = make_contents[:exec_clang_loc] \
        +link_code + make_contents[exec_clang_loc:main_o_loc] \
        +inc_code  + make_contents[main_o_loc:] \
        

    print(make_contents)

    with open(output_file, "w") as output:
        output.write(make_contents)
