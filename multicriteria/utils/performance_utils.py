import os
from multicriteria.configs.multi_crit_config_constants import *
from time_predictability.meta_parallel.tdg import TDG
from time_predictability.meta_parallel.app import App
from typing import List,Tuple

VARIANT = "variant"
TDG_ID = "tdg_id"
RESPONSE_TIME = "response_time"
ENERGY = "energy"
DEADLINE = "deadline"
ENERGY_BUDGET = "energy_budget"
RESPECTS_DEADLINE = "respects_deadline"
RESPECTS_ENERGY_BUDGET = "respects_energy_budget"

def value_of(line: Tuple, column_name: str, header: Tuple):
    return line[header.index(column_name)]

def optimize_per_criteria(table:List[Tuple])->Tuple[Tuple,Tuple]:
    """
    Optimizes for RT and energy:
    * for RT : min(RESPONSE_TIME)
    * for energy: min(ENERGY)
    """
    header:Tuple = table[0]
    #this is not enought for multi_tdgs
    best_rt = min(table[1:], key=lambda line: value_of(line,RESPONSE_TIME,header))
    best_en = min(table[1:], key=lambda line: value_of(line,ENERGY,header))
    return best_rt, best_en

def filter_table(table:List[Tuple]) -> List[Tuple]:
    header:Tuple = table[0]
    #this is not enought for multi_tdgs: solution is get id of tdg (i.e. variants) that have Falses, and remove all occurrences with that id
    return [line for line in table \
        if value_of(line,RESPECTS_DEADLINE,header) != False \
            and value_of(line,RESPECTS_ENERGY_BUDGET,header) != False]

def build_performance_table(config) -> List[Tuple]:
    config_signature = config[OPTIMIZATION][POWER_MODES][SIGNATURE]
    cpu_config_signature = tuple( c for c in config_signature if c.startswith("cpu."))
    gpu_config_signature = tuple( c for c in config_signature if c.startswith("gpu."))
    header = cpu_config_signature \
        + gpu_config_signature \
        + ("variant","tdg_id","amalthea_task","response_time","energy","deadline","energy_budget","respects_deadline","respects_energy_budget")
    
    table = [header]
    base_dir = config[GENERAL][BASE_DIR]

    for tdg_config in config[APP][TDGS_FILES]:

        config_line = []
        c: str
        for c in cpu_config_signature:
            c_name = c[4:] #.removeprefix("cpu.")
            config_line.append(tdg_config[CPU][c_name])
        if tdg_config[VARIANT] == "gpu":
            for c in gpu_config_signature:
                c_name = c[4:] #c.removeprefix("gpu.")
                config_line.append(tdg_config[GPU][c_name] if c_name in tdg_config[GPU] else "N/A")
        else: 
            config_line.append("N/A")
        config_line.append(tdg_config[ID])
        
        tdg_file = tdg_config[FILE]
        file = os.path.join(base_dir,tdg_file)
        # tdgs = TDG.read_json(file,tdg_config[ID],config[MULTIPLE_TDGS])
        app: App = App.read_json(file)
        # out_jsons = {}
        config_line = tuple(config_line)
        for tdg in app.tdgs:
            energy = tdg.tdg_metrics.get("energy") if "energy" in tdg.tdg_metrics else "unknown"
            line = config_line + (tdg.id, tdg.amalthea_task_id, tdg.makespan(), energy)
            # tdg_info = next(tdg_info for tdg_info in config[APP][TDGS_FILES] if tdg_info[ID] == tdg.id)
            deadline = tdg.constraints[DEADLINE] if DEADLINE in tdg.constraints else -1#tdg_info[CONSTRAINTS][DEADLINE]
            energy_budget = tdg.constraints[ENERGY_BUDGET] if ENERGY_BUDGET in tdg.constraints else -1 #tdg_info[CONSTRAINTS][ENERGY_BUDGET]
            line += (deadline if deadline >-1 else "N/A", energy_budget if energy_budget >-1 else "N/A")
            in_deadline = deadline < 0 or tdg.makespan() <= deadline
            in_energy_cost = "unkown" if energy == "unknown" else energy_budget < 0 or energy <= energy_budget
            line += (in_deadline, in_energy_cost)
            table.append(line)
    
    return table

