from typing import Dict
from time_predictability.meta_parallel.app import App
from time_predictability.meta_parallel.tdg_schema import *
import re

    
def rename_pmcs(tdg_file_name: str, prv_file_name: str, cudamperf_file_name: str = None):
    with open(tdg_file_name,'r') as tdg_file:
        app = json.load(tdg_file)
    
    pmc_names = {}
    with open(prv_file_name,'r') as prv_file:
        lines = prv_file.readlines()
        for line in lines:
            tokens = re.split("\s+",line.strip())
            if tokens[0] == '7' and (tokens[1].startswith('42') or tokens[1].startswith('100')):
                pmc_names[tokens[1]] = tokens[2]
            
    
    contents = next(iter(app.values()))
    default_results_prop = ["thread","execution_begin_time","execution_end_time","execution_total_time"]
    event_ids = pmc_names.keys()
    for task_graph in contents: #array of task_graphs
        for node in task_graph[NODES].values():
            
            if not RESULTS in node:
                continue
            result: Dict
            for result in node[RESULTS]:
                #all results except the defaults are pmc results
                #sorting by the default position in extrae
                for pm_key in sorted(result.keys()-default_results_prop):
                    if pm_key in event_ids:
                        result[pmc_names[pm_key]] = result.pop(pm_key)

    with open(tdg_file_name,'w') as tdg_file:
        json.dump(app,tdg_file,indent=2)