#
# Copyright 2022 Instituto Superior de Engenharia do Porto
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# 	http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#



def new_optimization_config():
    return {
        # "num_threads": -1,
        # "multiple_tdgs": False,
        "general": {
            "base_dir": None,
            "output_dir": None,
        },
        "app": {
            "name": None,
            "amalthea_model": None,
            "tdgs_files": []
        },
        "optimization": {
            "power_modes":{
                "RT": [],
                "energy": [],
                "signature": []
            },
            "optimization_order":{
                "RT": [] ,
                "energy": [] 
            },
            "selected":{
                "RT": -1, 
                "energy": -1 
            },
            "configuration": {
                "RT": {},
                "energy": {}
            }
        },
        "compilation": {
            "code_base_dir": "./",
            "variants": [],
            "platform": {
                "name": "unknown",
                "setup": [],
                "cpu": {
                    "cmd": [],
                    "wait": 0
                },
                "cleanup": [],
            }
        }
    }
def new_tdg_file_config(file_name, id, variant_mode, cpu_conf, device_conf, profiled, map_algs=None):
    if not map_algs:
        map_algs = default_map_algs()


    tdg_config= {
        "file": file_name,
        "id": id,
        "cpu": {},
        variant_mode: {},
        "variant": variant_mode,
        "profiled": profiled
        # "RT":  {
        #     "mapping_algorithms": map_algs,
        #     "best_mapping": {
        #         "algorithm": -1,
        #         "makespan": -1
        #     }
        # },
        # "energy": {
        #     "power_mode": -1,
        #     "energy_consumption": [],
        #     "max_inst_power": []
        # }
    }

    if cpu_conf and len(cpu_conf) > 0:
        tdg_config["cpu"] = {k:v for k,v in cpu_conf.items()}


    if device_conf and len(device_conf) > 0:
        tdg_config[variant_mode] = {k:v for k,v in device_conf.items()}

    return tdg_config

def heat_config_example():
    return {
        "num_threads": 4,
        "app_name": "heat",
        "base_dir": "./examples/apps/heat/",
        "working_dir": "./working_dir/",
        "final_dir": "./results/",
        "config_name": "heat.cfg",
        "constraints":{
            "deadline": 400000000,
            "energy_budget": -1
        },
        "power_modes": ['0','1','2'],
        "platform_settings": {
            "selected": "Lenovo5-15ACH6H",
            "platforms":{
                "Xavier": {
                    "power_modes": {
                        '0':{
                            "command": "sudo nvpmodel -m 2",
                        },
                        '1':{
                            "command": "sudo nvpmodel -m 5",
                        },
                        '2':{
                            "command": "sudo nvpmodel -m 7",
                        }
                    }
                },
                "Lenovo5-15ACH6H": {
                    "power_modes": {
                        '0':{
                            "command": "echo switch to power mode 0",
                        }
                    }
                },
            }
        },
        "executables": [{
            "dir": "benchmarks/heat/",
            "command": "./heat_extrae test.dat",
            "exec_name": "heat_extrae",
            "output_prefix": "normal/heat",
            "iterations": 10
        }],
        "RT": {
            "mapping_algorithms": [
                {
                    "task2thread": "BestFit",
                    "queue": "BestFit",
                    "queue_per_thread": True
                },
                {
                    "task2thread": "BestFit",
                    "queue": "FIFO",
                    "queue_per_thread": True
                },
                {
                    "task2thread": "BFS",
                    "queue": "FIFO",
                    "queue_per_thread": False
                },
                {
                    "task2thread": "SEQR",
                    "queue": "FIFO",
                    "queue_per_thread": True
                }
            ]
        },
        "energy": {
            "power_mode": -1,
            "energy_consumption": [],
            "max_inst_power": []
        }
    }

def default_map_algs():
    return [
                {
                    "task2thread": "BestFit",
                    "queue": "BestFit",
                    "queue_per_thread": True,
                    "active": True,
                    "makespan": -1
                },
                {
                    "task2thread": "BestFit",
                    "queue": "FIFO",
                    "queue_per_thread": True,
                    "active": True,
                    "makespan": -1
                },
                {
                    "task2thread": "BFS",
                    "queue": "FIFO",
                    "queue_per_thread": False,
                    "active": True,
                    "makespan": -1
                },
                {
                    "task2thread": "SEQR",
                    "queue": "FIFO",
                    "queue_per_thread": True,
                    "active": True,
                    "makespan": -1
                }
        ]