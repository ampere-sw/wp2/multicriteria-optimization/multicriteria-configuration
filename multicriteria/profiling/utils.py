import os
from sys import stderr, stdout
from typing import Dict, List
import time
from multicriteria.analysis.config.pmc_per_freq import get_all_pmc_formats, get_cpu_pmc_format, get_cpu_pmcs, get_device_pmc_format, get_device_pmcs
from multicriteria.configs.profiler_config_constants import NAME, ARGS, CMD, PERFORMANCE_COUNTERS, SETUP,CPU,GPU,FPGA,CLEANUP,WAIT
from multicriteria.configs.utils import arg_to_list
from time_predictability.meta_parallel.app import App
from time_predictability.meta_parallel.tdg import TDG
from time_predictability.meta_parallel.task import Task

from time_predictability.meta_parallel.tdg_schema import CONSTRAINTS, PERIOD, DEADLINE
import subprocess

TEMPLATES_NAME = "templates"
MAX_CMD_TIME = 300

def set_env_var(name,value):
    os.environ[name] = value

def rm_env_var(name):
    del os.environ[name]

def get_log_dir(wd):  
  log_dir = os.path.join(wd,"logs")
  os.makedirs(log_dir,exist_ok=True)
  return log_dir

def run_process(cmd, file_name_prefix, print_message: bool = True, timeout:float = MAX_CMD_TIME):
    if print_message:
        print("[PROFILER]Running command: ",cmd)

    ret = 1
    result:subprocess.CompletedProcess
    if file_name_prefix:
      out = file_name_prefix+"_out.log"
      err = file_name_prefix+"_err.log"
      with open(out,"a") as outF, open(err,"a") as errF:
          outF.write("[PROFILER]Output channel for command: " + cmd+"\n")
          errF.write("[PROFILER]Error channel for command: " + cmd+"\n")
          result = subprocess.run(cmd,shell=True,stderr=errF,stdout=outF, timeout=timeout)
          if result.returncode != 0:
            print("[WARNING]Process returned exit status",result.returncode,". Please see the output of the process in files:")
            print("[WARNING]stdout:",os.path.abspath(out))
            print("[WARNING]stderr:",os.path.abspath(err))
    else:
      result = subprocess.run(cmd,shell=True,stderr=stderr,stdout=stdout, timeout=timeout)
      if result.returncode != 0:
          print("[WARNING]Process returned exit status",result.returncode,". Please see the output in the console.")
    res = result.returncode
    return ret

def run(cmd, conf: Dict = None, out_file_prefix: str = None, print_message: bool = True, timeout:float = MAX_CMD_TIME):
    ret: int = 0
    if isinstance(cmd,dict):
        for k,v in cmd.items():
            if isinstance(v,str) and conf:
                v = v.format(**conf)
            else:
                v = str(v)
            if print_message:
                print("[PROFILER]Setting env. variable: $"+k+" =",v)
            os.environ[k] = v
    else:
        if isinstance(cmd,str) and conf:
                cmd = cmd.format(**conf)
        ret = run_process(cmd,out_file_prefix,print_message,timeout)
    return ret

def normalize_array(obj,prop):
    if prop not in obj or obj[prop] == None:
        obj[prop] = []
    if not isinstance(obj[prop],list):
        obj[prop] = [obj[prop]]
    return obj[prop]

def normalize_platform_cmds(platform):
    normalize_array(platform,SETUP)
    if CPU not in platform:
        raise Exception("CPU configuration must be setted with at least 'num_threads' defined")
        # run_with_cpu_conf(exec, None, platform, bd, wd, tdg_configs)
    normalize_array(platform[CPU],CMD)
    if GPU not in platform:
        # print("[PROFILER][WARNING] GPU configuration setup not defined for the target platform. The tool will ignore setup for GPU.")
        platform[GPU] = {}
    if FPGA not in platform:
        # print("[PROFILER][WARNING] GPU configuration setup not defined for the target platform. The tool will ignore setup for GPU.")
        platform[FPGA] = {}
    
    normalize_array(platform[GPU],CMD)
    normalize_array(platform[FPGA],CMD)
    normalize_array(platform,CLEANUP)

def freq_from_config(target, conf:Dict, device):
    if CMD not in device:
        print("[PROFILER]Using the",target,"as is")
    if conf:
        print("[PROFILER]Setting",target,"with arguments:", conf)
    for cmd in device[CMD]:
        run(cmd, conf)

    if WAIT in device and device[WAIT] > 0:
        print("[PROFILER]Waiting for",device[WAIT],"ms for the system to be ready.")
        time.sleep(device[WAIT]*(10**-3))

#method is assuming amalthea task ids and runnables exist
def merge_info_from_apps(cpu_app: App,device_app: App):
    
    # if cpu_app.end_to_end_deadline != None:
    #     device_app.end_to_end_deadline = cpu_app.end_to_end_deadline
    tdg: TDG
    cpu_names = [tdg.amalthea_task_id for tdg in cpu_app.tdgs]
    device_names = [tdg.amalthea_task_id for tdg in device_app.tdgs]
    cpu_names.sort()
    device_names.sort()
    if cpu_names != device_names:
        raise Exception("""BOTH versions of the application should have the same Amalthea tasks.
        In CPU version: {}
        In device version: {}""".format(cpu_names,device_names))
    for cpu_tdg in cpu_app.tdgs:
        device_tdg = device_app.get_tdg_by_amalthea_id(cpu_tdg.amalthea_task_id)
        # device_tdg.ins = cpu_tdg.ins.copy()
        # device_tdg.outs = cpu_tdg.outs.copy()
        # device_tdg.constraints[PERIOD] = cpu_tdg.constraints[PERIOD]
        # device_tdg.constraints[DEADLINE] = cpu_tdg.constraints[DEADLINE]
        device_task: Task
        for device_task in device_tdg.task_list():
            cpu_task = cpu_tdg.get_task_by_runnable_name(device_task.runnable_id)
            if device_task.spec != None:
                cpu_task.spec = "ARM"



def build_profiling_tools_files(platform_name, platform: Dict, variant:str,wd) -> Dict:
    templates_dir = os.path.join(wd,TEMPLATES_NAME)
    os.makedirs(templates_dir,exist_ok=True)
    
    tools_config_files = {
        "CPU_DEFAULT": _build_extrae_file(templates_dir,"extrae_cpu.xml",
                                          get_all_pmc_formats(platform_name, CPU).values()),
         CPU: {}
    }

    if variant:
      pmc_values =  get_all_pmc_formats(platform_name, variant).values()
      if variant.upper() == "GPU":
          tools_config_files[variant+"_DEFAULT"] = _build_cudamperf_file(templates_dir,"cudamperf_default.txt",list(pmc_values)[:8]) #because we can only use 8 perf counters
      else:
          tools_config_files[variant+"_DEFAULT"] = _build_extrae_file(templates_dir,"extrae_"+variant.lower()+".xml",pmc_values)
      tools_config_files[variant] = {}
    

    if ARGS in platform[CPU] and "frequency" in platform[CPU][ARGS]:
        cpu_freqs = arg_to_list(platform[CPU][ARGS]["frequency"])
        for cpu_freq in cpu_freqs:
          expected_cpu_pmcs = get_cpu_pmcs(cpu_freq)
          if expected_cpu_pmcs == None: #i.e. measure all
            tools_config_files[CPU][cpu_freq] = tools_config_files["CPU_DEFAULT"]
            continue
          cpu_pmcs = [get_cpu_pmc_format(platform_name,pmc) for pmc in expected_cpu_pmcs]
          tools_config_files[CPU][cpu_freq] =  _build_extrae_file(templates_dir,"extrae_cpu_"+str(cpu_freq)+".xml",cpu_pmcs)
    
    if variant and ARGS in platform[variant] and "frequency" in platform[variant][ARGS]:
        device_freqs = arg_to_list(platform[variant][ARGS]["frequency"])
        for device_freq in device_freqs:
          expected_device_pmcs = get_device_pmcs(variant,device_freq)

          if expected_device_pmcs == None: #i.e. measure all
            tools_config_files[variant][device_freq] = tools_config_files[variant+"_DEFAULT"]
            continue
          device_pmcs = [get_device_pmc_format(platform_name,variant,pmc) for pmc in expected_device_pmcs]
          if variant.upper() == "GPU":
              tools_config_files[variant][device_freq] = _build_cudamperf_file(templates_dir,"cudamperf_"+str(device_freq)+".txt",device_pmcs)
          else:
            tools_config_files[variant][device_freq] = _build_extrae_file(templates_dir,"extrae_"+variant.lower()+"_"+str(device_freq)+".xml",device_pmcs)

    return tools_config_files

def _build_cudamperf_file(dir, file_name, perf_counters: List[str]):
    file = os.path.join(dir,file_name)

    with open(file,"w") as out:
        out.write(str(len(perf_counters))+"\n"+"\n".join([str(p) for p in perf_counters]))
    return file

def _build_extrae_file(dir, extrae_name, perf_counters: List[str]):
    extrae_file = os.path.join(dir,extrae_name)

    with open(extrae_file,"w") as out:
        out.write(build_extrae_content(perf_counters))
    return extrae_file


def build_extrae_content(performance_counters:List[str]):
    extrae_home = os.getenv('EXTRAE_HOME')
    if extrae_home == None:
        raise Exception("Environment variable EXTRAE_HOME must be specified")
    return """<?xml version='1.0'?>

  <trace enabled="yes"
  home="{}"
  initial-mode="detail"
  type="paraver"
  >
  <openmp enabled="yes" ompt="no">
    <locks enabled="no" />
		<taskloop enabled="no" />
    <counters enabled="yes" />
  </openmp>

  <pthread enabled="no">
    <locks enabled="no" />
    <counters enabled="yes" />
  </pthread>

  <counters enabled="yes">
    <cpu enabled="yes" starting-set-distribution="1">
      <set enabled="yes" domain="all" changeat-time="0">
        {}
      </set>
    </cpu>
    <network enabled="no" />
    <resource-usage enabled="no" />
    <memory-usage enabled="no" />
  </counters>

  <storage enabled="no">
    <trace-prefix enabled="yes">TRACE</trace-prefix>
    <size enabled="no">5</size>
    <temporal-directory enabled="yes">/scratch</temporal-directory>
    <final-directory enabled="yes">/gpfs/scratch/bsc41/bsc41273</final-directory>
  </storage>

  <buffer enabled="yes">
    <size enabled="yes">5000000</size>
    <circular enabled="no" />
  </buffer>

  <trace-control enabled="yes">
    <file enabled="no" frequency="5M">/gpfs/scratch/bsc41/bsc41273/control</file>
    <global-ops enabled="no"></global-ops>
  </trace-control>

  <others enabled="yes">
    <minimum-time enabled="no">10M</minimum-time>
    <finalize-on-signal enabled="yes" 
      SIGUSR1="no" SIGUSR2="no" SIGINT="yes"
      SIGQUIT="yes" SIGTERM="yes" SIGXCPU="yes"
      SIGFPE="yes" SIGSEGV="yes" SIGABRT="yes"
    />
    <flush-sampling-buffer-at-instrumentation-point enabled="yes" />
  </others>

  <sampling enabled="no" type="virtual" period="50m" variability="10m" />

  <dynamic-memory enabled="no" />

  <input-output enabled="no" internals="no"/>

	<syscall enabled="no" />

  <merge enabled="yes" 
    synchronization="default"
    tree-fan-out="16"
    max-memory="512"
    joint-states="yes"
    keep-mpits="no"
    sort-addresses="yes"
    overwrite="yes"
  />

</trace>
""".format(extrae_home, ",".join(performance_counters))