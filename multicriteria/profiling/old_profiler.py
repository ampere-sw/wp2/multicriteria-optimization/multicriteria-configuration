#
# Copyright 2022 Instituto Superior de Engenharia do Porto
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# 	http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
from copy import deepcopy
from functools import reduce
import glob
import json
from lib2to3.pgen2.token import COMMA
import shutil
import sys
from dotenv import load_dotenv
import os
import time
from typing import Dict, List, Tuple
from multicriteria.configs.profiler_config_schema import *
from multicriteria.utils.pmc_renaming import rename_pmcs
from multicriteria.utils.templates import new_optimization_config, new_tdg_file_config
from collections import namedtuple
from multicriteria.configs.utils import combine, by_key_tuple_sorter, CompactJSONEncoder
from multicriteria.configs.multi_crit_config_constants import *
from multicriteria.profiling.utils import get_log_dir, merge_info_from_apps, run,normalize_platform_cmds,freq_from_config, build_profiling_tools_files, TEMPLATES_NAME, set_env_var
from time_predictability.meta_parallel.app import App, END_TO_END_DEADLINE
from time_predictability.meta_parallel.utils import setAmxmiInfo, EventChainMode


DEVICE = "device"

if "AMPERE_SRC_PATH" not in os.environ:
    raise Exception("Environment variale 'AMPERE_SRC_PATH' must be defined to look for 'tdg-instrumentation-script'")

tdg_instr_path = os.getenv("AMPERE_SRC_PATH")+'/tdg-instrumentation-script/parsePrvAndTdg.py'
if not os.path.exists(tdg_instr_path):
    raise Exception("Could not find tdg-instrumentation script in path: "+tdg_instr_path)

tdg_annotation = "python3 ${AMPERE_SRC_PATH}/tdg-instrumentation-script/parsePrvAndTdg.py"



def profile(base_config: Dict) -> Dict:
    """
    config: a base configuration. Will execute using the different power modes    
    returns: a control configuration based on the results obtained
    """
    ProfileSchema.validate_json(base_config)
    load_dotenv()
    first_dir = os.getcwd()
    first_dir = os.path.abspath(first_dir)
    bd = base_config[GENERAL][BASE_DIR]
    bd = os.path.abspath(bd)
    wd = base_config[GENERAL][WORKING_DIR]
    fd = base_config[GENERAL][FINAL_DIR]
    base_config[GENERAL][CLEAR_DIRS] = base_config[GENERAL][CLEAR_DIRS] if CLEAR_DIRS in base_config[GENERAL] else False
    if not os.path.exists(wd):
        os.makedirs(wd)
    elif base_config[GENERAL][CLEAR_DIRS]:
        print("clearing results")
        shutil.rmtree(wd,ignore_errors=True)
        os.makedirs(wd)
    wd = os.path.abspath(wd)

    if not os.path.exists(fd):
        os.makedirs(fd)
    elif base_config[GENERAL][CLEAR_DIRS]:
        shutil.rmtree(wd,ignore_errors=True)
        os.makedirs(wd)
    fd = os.path.abspath(fd)

    tdg_configs,modes = build_tdgs(base_config,bd,wd)
    
    os.chdir(first_dir)

    gen_config = new_optimization_config()
    gen_config[GENERAL][BASE_DIR] = os.path.abspath(base_config[GENERAL][WORKING_DIR])
    if ANALYSIS_DIR in base_config[GENERAL]:
        ad = os.path.abspath(base_config[GENERAL][ANALYSIS_DIR]) 
        gen_config[GENERAL][WORKING_DIR] = ad

        if not os.path.exists(ad):
            os.makedirs(ad)
        elif base_config[GENERAL][CLEAR_DIRS]:
            shutil.rmtree(ad,ignore_errors=True)
            os.makedirs(ad)
            
    gen_config[GENERAL][OUTPUT_DIR] = os.path.abspath(base_config[GENERAL][FINAL_DIR])
    
    gen_config[APP][NAME] = base_config[APP][NAME]
    gen_config[APP][AMALTHEA_MODEL] = base_config[APP][AMALTHEA_MODEL]
    gen_config[APP][TDGS_FILES] = tdg_configs
    
    gen_config[COMPILATION][CODE_BASE_DIR] = os.path.abspath(base_config[GENERAL][BASE_DIR])

    # migrated to each variant
    # gen_config[COMPILATION][TDG_CPP_FILE] = base_config[APP][TDG_CPP_FILE]
    gen_config[COMPILATION][VARIANTS] = base_config[APP][VARIANTS]
    platform = base_config[PLATFORM][PLATFORMS][base_config[PLATFORM][SELECTED]]
    gen_config[COMPILATION][PLATFORM][NAME] = base_config[PLATFORM][SELECTED]
    gen_config[COMPILATION][PLATFORM][SETUP] = platform[SETUP]
    gen_config[COMPILATION][PLATFORM][CPU][CMD] = platform[CPU][CMD]
    if WAIT in platform[CPU]:
        gen_config[COMPILATION][PLATFORM][CPU][WAIT] = platform[CPU][WAIT]
    if PERFORMANCE_COUNTERS in platform[CPU]:
        gen_config[COMPILATION][PLATFORM][CPU][PERFORMANCE_COUNTERS] = platform[CPU][PERFORMANCE_COUNTERS]
    
    variants = base_config[APP][VARIANTS]
    device = GPU if GPU in variants else FPGA if FPGA in variants else None

    if device:
        gen_config[COMPILATION][PLATFORM][device] = {
            CMD: platform[device][CMD] if CMD in platform[device] else [],
            WAIT: platform[device][WAIT] if WAIT in platform[device] else 0,
            PERFORMANCE_COUNTERS: platform[device][PERFORMANCE_COUNTERS] if PERFORMANCE_COUNTERS in platform[device] else {}
        }
    gen_config[COMPILATION][PLATFORM][CLEANUP] = platform[CLEANUP]

    mode_signature = []
    if ARGS in platform[CPU]:
        mode_signature.extend("cpu."+k for k in platform[CPU][ARGS].keys())
    num_device_props = 0
    if device in platform and ARGS in platform[device]:
        keys = platform[device][ARGS].keys()
        num_device_props = len(keys)
        mode_signature.extend(device+"."+k for k in keys)
    mode_signature.append(TDGS)

    if ORDER_BY in platform:
        modes.sort(key= lambda m: by_key_tuple_sorter(m,platform[ORDER_BY]))
    grouped = []
    if GROUP_BY in platform:
        device,arg = platform[GROUP_BY].split(".")
        key_group = []
        mode:Dict
        for mode in modes:
            mode_tuple = [v for v in mode[CPU].values()]
            device_values = mode[device].values()
            if not device_values:
                mode_tuple.extend([None]*num_device_props)
            else:
                mode_tuple.extend(device_values)
            mode_tuple.append(mode[TDGS])
            val_key = mode[device][arg] if arg in mode[device] else None
            if val_key not in key_group:
                key_group.append(val_key)
                grouped.append([mode_tuple])
            else:
                index = key_group.index(val_key)
                grouped[index].append(mode_tuple)
    else:
        group = []
        for mode in modes:
            mode_tuple = [v for v in mode[CPU].values()]
            
            if not device or not mode[device].values():
               mode_tuple.extend([None]*num_device_props)
            else:
                mode_tuple.extend(mode[device].values())
            mode_tuple.append(mode[TDGS])
            group.append(mode_tuple)
        grouped = [group]

    
    gen_config[OPTIMIZATION][POWER_MODES][RT] = grouped
    gen_config[OPTIMIZATION][POWER_MODES][ENERGY] = grouped
    gen_config[OPTIMIZATION][POWER_MODES][SIGNATURE] = mode_signature    
    if OPTIMIZATION in base_config:
        gen_config[OPTIMIZATION][CONFIGURATION] = base_config[OPTIMIZATION]

    out_file_path = os.path.join(wd,base_config[GENERAL][CONFIG_NAME])
    with open(out_file_path, "w") as out_file:
        json.dump(gen_config,out_file,indent=4,cls=CompactJSONEncoder)
    print("[PROFILER]Finished! Configuration file generated:",out_file_path)

    return gen_config
    # tdg.dot $1.prv $1.pcf



def build_tdgs(base_config, bd,wd):
    selected_platform_name = base_config[PLATFORM][SELECTED]
    
    platform = base_config[PLATFORM][PLATFORMS][selected_platform_name]
    normalize_platform_cmds(platform)
    # we will have two variants: CPU and GPU (we are not considering FPGA for now)
    variants = base_config[APP][VARIANTS]

    json_templates = compile_executables(base_config[APP], bd,wd)

    extrae_files = build_profiling_tools_files(selected_platform_name,platform,json_templates[DEVICE],wd)
      
    tdg_configs = []

    run_in_platform(platform,variants,json_templates,extrae_files,bd,wd,tdg_configs)
    modes = []
    tdg_index = []
    for i, tdg in enumerate(tdg_configs):
        mode = {
            "cpu": {k: v for k,v in tdg["cpu"].items() if k != NUM_THREADS}, #if "cpu" in tdg else {},
            # "gpu":  {k: v for k,v in tdg["gpu"].items()} if "gpu" in tdg else {},
            # "fpga": {k: v for k,v in tdg["fpga"].items()} if "fpga" in tdg else {},
        }
        if json_templates[DEVICE]:
            device = json_templates[DEVICE]
            mode[device] = {k: v for k,v in tdg[device].items()} if device in tdg else {}
        if not mode in modes:
            modes.append(mode)
            tdg_index.append([i])
        else:
            tdg_index[modes.index(mode)].append(i)

    for i in range(0,len(modes)):
        modes[i]["tdgs"] = tdg_index[i]
        
    return tdg_configs, modes

def add_num_threads(cpu_conf,cpu):
    """
    in cpu_conf: current cpu mode
    in cpu: cpu settings of target device
    """
    num_threads_config = cpu[NUM_THREADS]
    if isinstance(num_threads_config,int): #if is an integer number, then it is a constant number
        cpu_conf[NUM_THREADS] = num_threads_config
    elif isinstance(num_threads_config,str):
        value_in_conf = cpu_conf[num_threads_config]
        cpu_conf[NUM_THREADS] = int(value_in_conf)
    else:
        value_in_conf = cpu_conf[num_threads_config]
        arg: List = cpu[ARGS][num_threads_config]
        pos = arg.index(value_in_conf)
        v = num_threads_config[MAP]
        if isinstance(v,dict):
           cpu_conf[NUM_THREADS] = range(v["start"],v["stop"],v["step"])[pos]
        else:
            cpu_conf[NUM_THREADS] = v[pos]


def build_base_app(variant,bd, app_name) -> App:
    dots_location = os.path.join(bd,variant[DIR])
    dots_location = os.path.join(dots_location,variant[DOTS_DIR])
    dots_files:List[str] = glob.glob(dots_location+r"/*.dot")
    app:App = App.read_dots(dots_files,app_name)
    return app

def build_variant(type, variant,bd,log_file_prefix) -> None:
    exec_dir = variant[DIR]
    cd = os.path.join(bd, exec_dir)
    curDir = os.getcwd()
    os.chdir(cd)
    with open(log_file_prefix,'a') as log:
        message = "[PROFILER]Building variant {}: {}".format(type,variant[EXEC_NAME])
        message2 = "[PROFILER]\tin directory:"+os.getcwd()
        log.write(message)
        log.write(message2)
    print(message)
    print(message2)
    build = variant[BUILD][DYNAMIC_MAPPING]
    if isinstance(build,str):
        build = [build]
    for cmd in build:
        run(cmd,out_file_prefix=log_file_prefix)
    os.chdir(curDir)

def compile_executables(app_config,bd,wd) -> Dict[str,str]:
    variants = app_config[VARIANTS]
    app_name = app_config[NAME]
    log_dir = get_log_dir(wd)
    cpu_log_file = os.path.join(log_dir,"building_"+app_name+"_cpu")

    if CPU not in variants:
        raise Exception("CPU variant has to be defined")

    cpu_variant = variants[CPU]
    build_variant(CPU, cpu_variant,bd,cpu_log_file)
    app: App = build_base_app(cpu_variant, bd, app_name)
    templates_dir = os.path.join(wd,TEMPLATES_NAME)
    os.makedirs(templates_dir,exist_ok=True)
    base_cpu_json = os.path.join(templates_dir,app_name+"_cpu.json")
    with open(base_cpu_json,"w") as out:
        json.dump(app.to_json(), out,indent=2)

    device = GPU if GPU in variants else FPGA if FPGA in variants else None
    ret = {CPU: base_cpu_json, 
           DEVICE: device, 
           END_TO_END_DEADLINE: -1,
           PROFILE: {
               CPU: cpu_variant[PROFILE] if PROFILE in cpu_variant else True
           }}
    if AMALTHEA_MODEL in app_config:
        ecMode: EventChainMode = None
        ecName: str = None
        if LATENCY_CONSTRAINT in app_config:
            ecMode = EventChainMode.LATENCY_CONSTRAINT
            ecName = app_config[LATENCY_CONSTRAINT]
        elif EVENT_CHAIN_CONSTRAINT in app_config:
            ecMode = EventChainMode.EVENT_CHAIN
            ecName = app_config[EVENT_CHAIN_CONSTRAINT] 
        setAmxmiInfo(app_config[AMALTHEA_MODEL],base_cpu_json,ecMode,ecName,base_cpu_json)


    cpu_app: App = App.read_json(base_cpu_json,False)
    ret[END_TO_END_DEADLINE] = cpu_app.end_to_end_deadline
    if device != None:
        device_variant = variants[device]
        device_log_file = os.path.join(log_dir,"building_"+app_name+"_"+device)
        build_variant(device, device_variant,bd,device_log_file)
        device_app: App = build_base_app(device_variant, bd, app_name)
        base_device_json = os.path.join(templates_dir,app_name+"_"+device.lower()+".json")
        with open(base_device_json,"w") as out:
            json.dump(device_app.to_json(), out,indent=2)
        setAmxmiInfo(app_config[AMALTHEA_MODEL],base_device_json,ecMode,ecName,base_device_json)
        device_app: App = App.read_json(base_device_json,False)
        merge_info_from_apps(cpu_app,device_app)
        with open(base_cpu_json,"w") as out:
            json.dump(cpu_app.to_json(), out,indent=2)
        with open(base_device_json,"w") as out:
            json.dump(device_app.to_json(), out,indent=2)
        ret[device] = base_device_json
        ret[PROFILE][device] = variants[device][PROFILE] if PROFILE in variants[device] else True
    return ret

def run_in_platform(platform,variants, json_templates,extrae_files, bd,wd,tdg_configs):
    """
    run the different executables in different frequencies
    """
    # if SETUP in platform:
    if platform[SETUP]:
        print("[PROFILER][SETUP]Initial setup for the target platform using the provided command(s)")
        for cmd in platform[SETUP]:
            run(cmd)
    
    device = json_templates[DEVICE]

    device_configs = get_device_freq_configs(platform, json_templates[DEVICE])

    profile_cpu = json_templates[PROFILE][CPU]
    profile_device = json_templates[PROFILE][device] if device else False
    anyProfile = profile_cpu or (device and profile_device)
    if not profile_cpu:
        use_only_ticks(variants[CPU],CPU,json_templates[CPU],wd,tdg_configs)
    if device and not profile_device:
        use_only_ticks(variants[device],device,json_templates[device],wd,tdg_configs)

    if anyProfile:
        cpu_freq_settings = platform[CPU]
        if ARGS in cpu_freq_settings:
            cpu_args_comb = combine(cpu_freq_settings[ARGS].items())
        else:
            cpu_args_comb = [{}] #i.e. run once the command without specific arguments

        #first run variant using only CPU
        for cpu_conf in cpu_args_comb:
            freq_from_config(CPU, cpu_conf, platform[CPU])
            add_num_threads(cpu_conf,platform[CPU])
            if profile_cpu:
                run_in_cpu_only(variants[CPU],cpu_conf,json_templates[CPU],extrae_files,bd,wd,tdg_configs)
            if profile_device:
                run_with_cpu_conf(variants[device], device, cpu_conf, device_configs, platform, json_templates[device], extrae_files, bd,wd,tdg_configs)
    
    if platform[CLEANUP]:
        print("Cleaning up platform with the provided command(s)")
        for cmd in platform[CLEANUP]:
            print("[CLEANUP] ",cmd)
            run(cmd,print_message=False)




def run_with_cpu_conf(variant, device, cpu_conf, device_configs, platform, device_template, extrae_files, bd, wd, tdg_configs):

    if not device_configs:
        print("[PROFILER][WARNING] Running an executable using "+device.upper()+" but no configuration was found for the GPU.")
        print("[PROFILER][WARNING] If this was intended please add an empty '"+device+"' property to specify this behavior.")
        run_executable(variant, device, cpu_conf,None,device_template, extrae_files, bd, wd, tdg_configs)
    else:
        for device_conf in device_configs:
            freq_from_config(device, device_conf, platform[device])
            run_executable(variant, device, cpu_conf,device_conf,device_template, extrae_files, bd, wd, tdg_configs)


def use_only_ticks(variant,device, base_json_file, wd, tdg_configs):
    
    output_prefix = variant[OUTPUT_PREFIX]
    id = output_prefix + "_only_ticks"
    
    json_relative_path = id +".json"
    tdg_json_path = os.path.join(wd,json_relative_path)
    tdg_json_dir = os.path.dirname(tdg_json_path)
    if not os.path.exists(tdg_json_dir):
        os.makedirs(tdg_json_dir)
    run("cp "+base_json_file + " "+tdg_json_path)
    add_metadata(tdg_json_path, {"cpu":{},device:{}, "variant": device, "profiled":False})
    tdg_config = new_tdg_file_config(json_relative_path,id, device, {},{}, False, None)
    tdg_configs.append(tdg_config)

def run_in_cpu_only(variant, cpu_conf:Dict, base_json_file, extrae_files, bd,wd, tdg_configs):
    
    extrae_file = extrae_files[CPU][cpu_conf["frequency"]] if "frequency" in cpu_conf else extrae_files["CPU_DEFAULT"]
    
    set_env_var("EXTRAE_CONFIG_FILE",extrae_file)
    log_file = os.path.join(wd,"profiling.log")
    log_output = log_file
    exec_dir = variant[DIR]
    cd = os.path.join(bd, exec_dir)
    os.chdir(cd)
    with open(log_file,'a') as log:
        message = "[PROFILER]Running {} with cpu {}".format(variant[EXEC_NAME], cpu_conf)
        message2 = "[PROFILER]\tin directory:"+os.getcwd() 
        log.write(message)
        log.write(message2)
        print(message)
        print(message2)
    
    its = variant[ITERATIONS]
    run_cmd = variant[RUN]
    exec_name = variant[EXEC_NAME]
    extrae_results_dir = variant[EXTRAE_RESULTS_DIR]
    extrae_results_dir = os.path.join(cd,extrae_results_dir)
    extrae_results_file = os.path.join(extrae_results_dir, exec_name)
    #first iteration  is no longer necessary, as we now generate first a template json file and we copy it before executing
    # run(run_cmd,out_file=log_output)

    # run(tdg_annotation+" " + base_json_file +" "+exec_name+".prv "+exec_name+".pcf",print_message=False)
    #we need to copy the base json file, otherwise it will overwrite it, 
    
    output_prefix = variant[OUTPUT_PREFIX]
    
    id = output_prefix
    if cpu_conf and len(cpu_conf) >0:
        to_use = [key for key in cpu_conf.keys() if key not in [NUM_THREADS]]
        id += "_with_[cpu"+reduce(lambda a,k: a+"_"+k+"_"+str(cpu_conf[k]), to_use,"")+"]"
    
    json_relative_path = id +".json"
    tdg_json_path = os.path.join(wd,json_relative_path)
    tdg_json_dir = os.path.dirname(tdg_json_path)
    if not os.path.exists(tdg_json_dir):
        os.makedirs(tdg_json_dir)
    run("cp "+base_json_file + " "+tdg_json_path)
    for i in range(0,its):
        print("[PROFILER] Iteration "+str(i+1)+":")
        #now run for the other iterations
        run(run_cmd,out_file_prefix=log_output)
        #but use the json in the working dir
        run(tdg_annotation+" "+ tdg_json_path +" "+exec_name+".prv "+exec_name+".pcf",print_message=True)

    add_metadata(tdg_json_path, {"cpu":cpu_conf,"gpu":{}, "variant": CPU, "profiled": True})
    rename_pmcs(tdg_json_path, extrae_results_file+".pcf")
    # map_algorithms_clone = deepcopy(map_algorithms) if map_algorithms else None
    tdg_config = new_tdg_file_config(json_relative_path,id, CPU, cpu_conf, {}, True, None)
    tdg_configs.append(tdg_config)#, map_algorithms_clone))


def run_executable(variant, device, cpu_conf:Dict,device_conf:Dict, device_template, extrae_files, bd,wd, tdg_configs):
    
    log_dir = get_log_dir(wd)
    profile_log_file = os.path.join(log_dir,"profiling_variants")
    # log_file = os.path.join(wd,"profiling.log")
    # log_output = log_file
    exec_dir = variant[DIR]
    cd = os.path.join(bd, exec_dir)
    os.chdir(cd)
    with open(profile_log_file,'a') as log:
        message = "[PROFILER]Running {} with cpu {} and "+device+" {}".format(variant[EXEC_NAME], cpu_conf,device_conf)
        message2 = "[PROFILER]\tin directory:"+os.getcwd() 
        log.write(message)
        log.write(message2)
        print(message)
        print(message2)
    
    its = variant[ITERATIONS]
    run_cmd = variant[RUN]
    exec_name = variant[EXEC_NAME]
    extrae_results_dir = variant[EXTRAE_RESULTS_DIR]
    extrae_results_dir = os.path.join(cd,extrae_results_dir)
    extrae_results_file = os.path.join(extrae_results_dir, exec_name)
    #first iteration  is no longer necessary, as we now generate first a template json file and we copy it before executing
    # run(run_cmd,out_file=log_output)

    # run(tdg_annotation+" " + base_json_file +" "+exec_name+".prv "+exec_name+".pcf",print_message=False)
    #we need to copy the base json file, otherwise it will overwrite it, 
    
    output_prefix = variant[OUTPUT_PREFIX]
    
    id = output_prefix
    if cpu_conf and len(cpu_conf) >0:
        to_use = [key for key in cpu_conf.keys() if key not in [NUM_THREADS]]
        id += "_with_[cpu"+reduce(lambda a,k: a+"_"+k+"_"+str(cpu_conf[k]), to_use,"")+"]"
    
    if device_conf and len(device_conf) >0:
        to_use = [key for key in device_conf.keys()]
        id += "_with_["+device+reduce(lambda a,k: a+"_"+k+"_"+str(device_conf[k]), to_use,"")+"]"
  
  
    json_relative_path_CPU = id +"_CPU_MEASUREMENTS.json"
    json_relative_path_device = id +"_"+device.upper()+"_MEASUREMENTS.json"
    tdg_json_path_cpu = os.path.join(wd,json_relative_path_CPU)
    tdg_json_path_device = os.path.join(wd,json_relative_path_device)

    tdg_json_dir = os.path.dirname(tdg_json_path_cpu)
    if not os.path.exists(tdg_json_dir):
        os.makedirs(tdg_json_dir)
    run("cp "+device_template + " "+tdg_json_path_cpu)
    run("cp "+device_template + " "+tdg_json_path_device)

    
    extrae_file = extrae_files[CPU][cpu_conf["frequency"]] if "frequency" in cpu_conf else extrae_files["CPU_DEFAULT"]
    set_env_var("EXTRAE_CONFIG_FILE",extrae_file)

    for i in range(0,its):
        print("[PROFILER] Iteration "+str(i+1)+" for CPU measurements:")
        #now run for the other iterations
        run(run_cmd,out_file_prefix=log_output)
        #but use the json in the working dir
        run(tdg_annotation+" "+ tdg_json_path_cpu +" "+exec_name+".prv "+exec_name+".pcf",print_message=True)
    
    rename_pmcs(tdg_json_path_cpu, extrae_results_file+".pcf")

    extrae_file = extrae_files[GPU][device_conf["frequency"]] if "frequency" in device_conf else extrae_files[device.upper()+"_DEFAULT"]
    set_env_var("EXTRAE_CONFIG_FILE",extrae_file)

    for i in range(0,its):
        print("[PROFILER] Iteration "+str(i+1)+" for GPU measurements:")
        #now run for the other iterations
        it_log_file = os.path.join(log_dir,exec_name+"_"+device+"_(it "+str(i+1)+")")
        run(run_cmd,out_file_prefix=it_log_file)
        #but use the json in the working dir
        run(tdg_annotation+" "+ tdg_json_path_device +" "+exec_name+".prv "+exec_name+".pcf",print_message=True)

    rename_pmcs(tdg_json_path_device, extrae_results_file+".pcf")

    json_relative_path = id +".json"
    tdg_json_path = os.path.join(wd,json_relative_path)

    merge_results(tdg_json_path_cpu, tdg_json_path_device, tdg_json_path)

    os.remove(tdg_json_path_cpu)
    os.remove(tdg_json_path_device)

    add_metadata(tdg_json_path, {"cpu":cpu_conf,device:device_conf, "variant": device, "profiled": True})
    # map_algorithms_clone = deepcopy(map_algorithms) if map_algorithms else None
    tdg_config = new_tdg_file_config(json_relative_path,id, device, cpu_conf, device_conf, True)
    tdg_configs.append(tdg_config)#, map_algorithms_clone))


def merge_results(tdg_json_path_cpu, tdg_json_path_device, json_relative_path):
    with open(tdg_json_path_cpu,'r') as tdg_file:
        cpu:Dict = json.load(tdg_file)
    with open(tdg_json_path_device,'r') as tdg_file:
        device:Dict = json.load(tdg_file)
    
    cpu_app_key = next(key for key in cpu if key != "end_to_end_deadline")
    cpu_tdgs:List = cpu[cpu_app_key]
    device_app_key = next(key for key in device if key != "end_to_end_deadline")
    device_tdgs:List = device[device_app_key]
    
    for c in cpu_tdgs:
        for d in device_tdgs:
            if c["amalthea_task_id"] != d["amalthea_task_id"]:
                continue
            for k, task_d in d["nodes"].items():
                d_ref = task_d["runnable_id"] if "runnable_id" in task_d else k
                #if this task has spec as GPU or FPGA
                if "spec" in task_d and task_d["spec"] in ["GPU","FPGA"]:
                   for j, task_c in c["nodes"].items():
                        #find corresponding version in CPU mode and replace its results
                        c_ref = task_c["runnable_id"] if "runnable_id" in task_c else j

                        if d_ref == c_ref and "results" in task_d:
                           task_c["results"] = task_d["results"]
                           break
            break

    with open(json_relative_path,'w') as tdg_file:
        json.dump(cpu,tdg_file,indent=2)

def add_metadata(tdg_file_name: str, metadata: Dict):
    with open(tdg_file_name,'r') as tdg_file:
        apps = json.load(tdg_file)
    taskgraphs = next(iter(apps.values())) #get first property (only has one!)
    for task_graph in taskgraphs: #array of task_graphs
        if "metadata" not in task_graph:
            task_graph["metadata"] = {}
        task_graph["metadata"].update(metadata)
    with open(tdg_file_name,'w') as tdg_file:
        json.dump(apps,tdg_file,indent=2)

def get_device_freq_configs(platform, device):
    
    if device == None:
        return []
    
    device_freq_settings = platform[device]
    if ARGS in device_freq_settings:
        device_freq_configs = combine(device_freq_settings[ARGS].items())
    elif device_freq_settings[CMD]:
            device_freq_configs = [None] #i.e. run once the command
    else:
        device_freq_configs = []

    return device_freq_configs




def OLD_build_tdgs(base_config, bd,wd):
    # power mode is deprecated, now each platform has its own setup of cpu/gpu frequencies
    # power_modes = base_config[POWER_MODES]
    selected_platform_name = base_config[PLATFORM][SELECTED]
    
    platform = base_config[PLATFORM][PLATFORMS][selected_platform_name]
    normalize_platform_cmds(platform)
    # we will have two variants: CPU and GPU (we are not considering FPGA for now)
    variants = base_config[APP][VARIANTS]
    tdg_ids = [tdg for tdg in base_config[APP][TDGS]]
    # map_algorithms = base_config[RT][MAPPING_ALGORITHMS] if RT in base_config and MAPPING_ALGORITHMS in base_config[RT] else None
    
    tdg_configs = []

    compile_executables(variants,bd,wd)

    run_in_platform(platform,variants,tdg_ids,bd,wd,tdg_configs)

    modes = []
    tdg_index = []
    for i, tdg in enumerate(tdg_configs):
        mode = {
            "cpu": {k: v for k,v in tdg["cpu"].items() if k != NUM_THREADS}, #if "cpu" in tdg else {},
            "gpu":  {k: v for k,v in tdg["gpu"].items()} if "gpu" in tdg else {},
        }
        
        if not mode in modes:
            modes.append(mode)
            tdg_index.append([i])
        else:
            tdg_index[modes.index(mode)].append(i)

    for i in range(0,len(modes)):
        modes[i]["tdgs"] = tdg_index[i]
        

    return tdg_configs, modes