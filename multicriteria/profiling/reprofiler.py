#
# Copyright 2022 Instituto Superior de Engenharia do Porto
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# 	http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
from copy import deepcopy
from functools import reduce
import json
from lib2to3.pgen2.token import COMMA
from dotenv import load_dotenv
import os
import time
from typing import Dict, List, Tuple
from multicriteria.configs.multi_crit_config_constants import *
from time_predictability.meta_parallel.tdg import TDG
from time_predictability.meta_parallel.app import App
from multicriteria.analysis.config.pmc_per_freq import get_all_pmc_formats, get_cpu_pmc_format, get_cpu_pmcs, get_device_pmc_format, get_device_pmcs
from multicriteria.profiling.utils import MAX_CMD_TIME, _build_extrae_file, get_log_dir, run,normalize_platform_cmds,freq_from_config, set_env_var, rm_env_var
from multicriteria.utils.pmc_renaming import rename_pmcs
import re  

if "AMPERE_SRC_PATH" not in os.environ:
    raise Exception("Environment variale 'AMPERE_SRC_PATH' must be defined to look for 'tdg-instrumentation-script'")

tdg_instr_path = os.getenv("AMPERE_SRC_PATH")+'/tdg-instrumentation-script/parsePrvAndTdg.py'
if not os.path.exists(tdg_instr_path):
    raise Exception("Could not find tdg-instrumentation script in path: "+tdg_instr_path)

tdg_annotation = "python3 ${AMPERE_SRC_PATH}/tdg-instrumentation-script/parsePrvAndTdg.py"

# def get_group_tdg_definition(tdg_cpp_file_lines:List[str])->Dict:
#     tdg_defs = {}
#     search_line = "struct kmp_node_info kmp_tdg_"
#     l = 0
#     while l < len(tdg_cpp_file_lines):
#         line = tdg_cpp_file_lines[l].strip()
#         if line.strip().startswith(search_line):
#             first_bracket = line.index("[")
#             id = line[len(search_line):first_bracket]
#             start = l
#             while not line.endswith(";"):
#                 l+=1
#                 line = tdg_cpp_file_lines[l].strip()
#             tdg_defs[id] = [start,l]
#         l+=1
#     return tdg_defs

def get_group_tdg_definition(tdg_cpp_file_lines:List[str])->Dict:
    
    tdg_defs = {}
    node_decl_search_line = "struct kmp_node_info"
    tdg_id_search_line = "__kmpc_taskgraph(loc_ref, gtid, "
    fini_after_id = ", entry"
    l = 0
    while l < len(tdg_cpp_file_lines):
        line = tdg_cpp_file_lines[l].strip()
        if line.strip().startswith(node_decl_search_line):
            #getting lines with nodes declarations
            start = l
            while not line.endswith(";"):
                l+=1
                line = tdg_cpp_file_lines[l].strip()
            last = l
            #now getting the id...
            while not line.strip().startswith(tdg_id_search_line):
                l+=1
                line = tdg_cpp_file_lines[l].strip()
            
            id = line[len(tdg_id_search_line):line.index(fini_after_id)]
            tdg_defs[id] = [start,last]
        l+=1
    # print(tdg_defs)
    # exit
    return tdg_defs
        

def get_tdg_cpp_contents(tdg_cpp_file_lines:List[str],groups,mappings:Dict):
    tdg_cpp_file_lines = tdg_cpp_file_lines.copy()
    for id,tdg in mappings.items():
        group = groups[str(id)]
        for l in range(group[0],group[1]+1):
            line = tdg_cpp_file_lines[l]
            if "static_id" not in line:
                continue
            static_id = line.index("static_id") + len("static_id")
            comma = line.index(",",static_id)
            static_id = line[static_id:comma].replace("=","").strip()
            static_thread = tdg[static_id]
            
            line = re.sub("static_thread\s+=\s+(-)?\d+,","static_thread = "+str(static_thread)+",",line)
            tdg_cpp_file_lines[l] = line

    return "".join(tdg_cpp_file_lines)

def compile(variant,code_base_dir,wd):
    log_dir = get_log_dir(wd)
    log_file = os.path.join(log_dir,"rebuilding_cpu")
    log_file = os.path.abspath(log_file)
    exec_dir = variant[DIR]
    cd = os.path.join(code_base_dir, exec_dir)
    first_dir = os.getcwd()
    os.chdir(cd)
    print("[PROFILER]Building variant",type,":",variant[EXEC_NAME])
    print("[PROFILER]\tin directory:",os.getcwd())

    build = variant[BUILD][STATIC_MAPPING]
    timeout = float(variant[BUILD][TIMEOUT]) if TIMEOUT in variant[BUILD] else MAX_CMD_TIME
    if isinstance(build,str):
        build = [build]
    
    for cmd in build:
        run(cmd,out_file_prefix=log_file, timeout=timeout)
    os.chdir(first_dir)

def execute(variant,code_base_dir,tdg_file_path,cpu_conf, wd):
    log_dir = get_log_dir(wd)

    exec_dir = variant[DIR]
    cd = os.path.join(code_base_dir, exec_dir)
    first_dir = os.getcwd()
    os.chdir(cd)
    print("[PROFILER]Running {} with cpu {}".format(variant[EXEC_NAME], cpu_conf))
    print("[PROFILER]\tin directory:"+os.getcwd())
    
 
    its = variant[ITERATIONS]
    run_cmd = variant[RUN]
    exec_name = variant[EXEC_NAME]
    extrae_results_dir = variant[EXTRAE_RESULTS_DIR]
    extrae_results_dir = os.path.join(cd,extrae_results_dir)
    extrae_results_file = os.path.join(extrae_results_dir, exec_name)

    output_prefix = variant[OUTPUT_PREFIX]
    id = output_prefix
    description = ""
    if cpu_conf and len(cpu_conf) >0:
        to_use = [key for key in cpu_conf.keys() if key not in [NUM_THREADS]]
        description += "_with_[cpu"+reduce(lambda a,k: a+"_"+k+"_"+str(cpu_conf[k]), to_use,"")+"]"
    id += description
    base_log_name = os.path.join(log_dir,exec_name+"_IN_CPU_"+description+"(it_")

    #now run for "its" iterations
    for i in range(0,its):
        print("[PROFILER] Iteration "+str(i+1)+":")
        it_log_file = base_log_name+str(i+1)+")"
        run(run_cmd,out_file_prefix=it_log_file)
        #but use the json in the working dir
        run(tdg_annotation+" "+ tdg_file_path +" "+exec_name+".prv "+exec_name+".pcf",print_message=True)


    rename_pmcs(tdg_file_path, exec_name+".pcf")
    # map_algorithms_clone = deepcopy(map_algorithms) if map_algorithms else None
    os.chdir(first_dir)

def setup_system(platform, cpu_config):
    freq_from_config("CPU",cpu_config,platform[CPU])


def profile_static_mapping(config: Dict) -> Dict:
    """
    CURRRENTLY ONLY WORKING WITH CPU!
    1. run COMPILATION.PLATFORM.SETUP
    2. for each tdg:
        1. set tdg file with static mapping
        2. get variant with tdg_file.variant index
        3. compile executable 
        4. setup platform using platform.cpu|gpu.cmd with config in tdg_file.cpu|gpu
        5. run with variant.run for variant.iterations. should prepend with OMP_TASK_SCHEDULE=static 
            1. use existing tdg (path in tdg_file.file) to gather results
    3. run COMPILATION.PALTFORM.CLEANUP
    """
    print("[PROFILER]Performing a profiling with static mappings")
    wd = config[GENERAL][BASE_DIR]
    platform = config[COMPILATION][PLATFORM]
    normalize_platform_cmds(platform)
    # 1. run COMPILATION.PLATFORM.SETUP
    if platform[SETUP]:
        print("[PROFILER][SETUP]Initial setup for platform",platform[NAME],"using the provided command(s)")
        for cmd in platform[SETUP]:
            run(cmd)


    templates_dir = os.path.join(wd,"templates")

    os.makedirs(templates_dir,exist_ok=True)
    extrae_file = _build_extrae_file(templates_dir,"extrae_cpu.xml",
                                          get_all_pmc_formats(platform[NAME], CPU).values())
    set_env_var("EXTRAE_CONFIG_FILE",extrae_file)
    set_env_var("OMP_TASK_SCHEDULE","static")
    tdg_files = config[APP][TDGS_FILES]
    num_tdg_files = str(len(tdg_files))
    for i,tdg_file in enumerate(tdg_files):
        tdg_file_name = tdg_file[FILE]
        comp_variant = config[COMPILATION][VARIANTS][tdg_file[VARIANT]]
        print("[PROFILER] Processing file",str(i+1)+"/"+num_tdg_files,":",tdg_file_name)

        tdg_cpp_file_path = os.path.join(config[COMPILATION][CODE_BASE_DIR],comp_variant[DIR])
        tdg_cpp_file_path = os.path.join(tdg_cpp_file_path,comp_variant[TDG_CPP_FILE])

        tdg_file_path = os.path.join(config[GENERAL][BASE_DIR],tdg_file_name)


        with open(tdg_cpp_file_path,"r") as f:
            tdg_cpp_file_lines = f.readlines()
        groups: Dict = get_group_tdg_definition(tdg_cpp_file_lines)

        app: App = App.read_json(tdg_file_path)
        # 1. set tdg file with static mapping
        mappings = app.get_mappings()
        new_tdg_cpp_contents = get_tdg_cpp_contents(tdg_cpp_file_lines,groups,mappings)
        with open(tdg_cpp_file_path,"w") as f:
            f.write(new_tdg_cpp_contents)

        # 2. get variant with tdg_file.variant index
        # variant_index = tdg_file[VARIANT]
        # variant = config[COMPILATION][VARIANTS][variant_index]
        # print(variant)
        # 3. compile executable 
        compile(comp_variant, config[COMPILATION][CODE_BASE_DIR],config[GENERAL][BASE_DIR])
        # 4. setup platform using platform.cpu|gpu.cmd with config in tdg_file.cpu|gpu
        # uses_gpu =  USES_GPU not in variant or variant[USES_GPU]
        setup_system(platform,tdg_file[CPU])#,tdg_file[GPU],uses_gpu)
    #     5. run with variant.run for variant.iterations. should prepend with OMP_TASK_SCHEDULE=static 
    #           1. use existing tdg (path in tdg_file.file) to gather results
        execute(comp_variant,config[COMPILATION][CODE_BASE_DIR],tdg_file_path,tdg_file[CPU], wd)
    # 3. run COMPILATION.PALTFORM.CLEANUP
    if platform[CLEANUP]:
        print("[PROFILER][CLEANUP]Cleaning up platform with the provided command(s)")
        for cmd in platform[CLEANUP]:
            print("[CLEANUP] ",cmd)
            run(cmd,print_message=False)

    rm_env_var("EXTRAE_CONFIG_FILE")
    rm_env_var("OMP_TASK_SCHEDULE")