#
# Copyright 2022 Instituto Superior de Engenharia do Porto
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# 	http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
from getopt import getopt
import json
import sys

from multicriteria.analysis.utils import clear_results, clear_metrics
from multicriteria.manager import manage_from_base, manage_from_intermediate, single_criterion_optimization

from multicriteria.profiling.profiler import profile
from multicriteria.utils.prepare_slg_code import extrae_in_main, extrae_in_make



def profile_codes():
    args = sys.argv[1:]    
    opts, args = getopt(args,"h")
    for o, v in opts:
        if o in "-h":
            print(profiler_help_message)
            return
        else:
            
            assert False, "unhandled option " + o
    if len(args) < 1:
        print(profiler_help_message)
        return

    config_file = args[0]
    
    
    config = None
    try:
        with open(config_file, 'r') as cfgfile:
            config = json.load(cfgfile)
    except Exception as err:
        print("Problem when opening configuration file",config_file)
        raise err
    profile(config)
    
def multicriteria_config_flow():
    args = sys.argv[1:]    
    opts, args = getopt(args,"hi")
    if len(args) < 1:
        print(multicriteria_help_message)
        return
    config_file = args[0]
    intermediate = False
    for o, v in opts:
        if o in "-h":
            print(multicriteria_help_message)
            return
        if o in "-i":
            intermediate = True
        else:
            assert False, "unhandled option " + o
    if intermediate:
        inter_config_file = manage_from_intermediate(config_file)
    else:
        inter_config_file = manage_from_base(config_file)
    return inter_config_file

def singlecriterion_opt():
    inter_config_file = multicriteria_config_flow()
    single_criterion_optimization(inter_config_file)




def slg_extrae_in_main():
    args = sys.argv[1:]    
    opts, args = getopt(args,"h")
    for o, v in opts:
        if o in "-h":
            print("usage: <main.c file>")
            return
        else:
            assert False, "unhandled option " + o
    if len(args) < 1:
        print("usage: <main.c file>")
        return

    main_file = args[0]
    
    extrae_in_main(main_file)


def slg_extrae_in_make():
    args = sys.argv[1:]    
    opts, args = getopt(args,"h")
    for o, v in opts:
        if o in "-h":
            print("usage: <Makefile file>")
            return
        else:
            assert False, "unhandled option " + o
    if len(args) < 1:
        print("usage: <main.c file>")
        return

    make_file = args[0]
    
    extrae_in_make(make_file)
    

profiler_help_message = """usage: <baseconfig.cfg>"""


multicriteria_help_message = """usage: [option] <baseconfig.cfg>
     -=================================== Options ===========================-
    | option | argument  | description                                        |
     --------|-----------|----------------------------------------------------
    | -h     | n/a       | show this message                                  |
    | -i     | n/a       | start process from an intermediate configuration   |
     -=======================================================================-"""
