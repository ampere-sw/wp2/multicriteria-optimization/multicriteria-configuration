#
# Copyright 2022 Instituto Superior de Engenharia do Porto
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# 	http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import json
import os
from typing import Dict

from multicriteria.configs.multi_crit_config_constants import *
from time_predictability.mapping_exploration.explorer import ExplorationConfig, MappingExploration
from time_predictability.meta_parallel.tdg import TDG
from time_predictability.meta_parallel.app import App
import functools
import operator


def explore_mappings(config: Dict):
    base_dir = config[GENERAL][BASE_DIR]
    tdgs_info = config[APP][TDGS_FILES]
    # tdgs_info = {t["id"]: t for t in tdgs_info } #converting to a map so it is more easy to access
    
    total_tdgs = str(len(tdgs_info))
    for i,tdg_config in enumerate(tdgs_info):
        tdg_file_name = os.path.join(base_dir,tdg_config[FILE])
        print("[MULTI-CRIT][EXPLORATION]",str(i+1)+"/"+total_tdgs,"Exploring TDG file",tdg_file_name,".")

        app: App = App.read_json(tdg_file_name)
        makespans__per_tdg = {}
        for tdg in app.tdgs:
            # tdg_info = tdgs_info[tdg.id]
            id = "amalthea task "+tdg.amalthea_task_id +"(tdg "+tdg.id+")" if tdg.amalthea_task_id != None else "tdg "+tdg.id
            print("[MULTI-CRIT][EXPLORATION] \tAnalysing",id)

            exp_config:ExplorationConfig = ExplorationConfig(tdg_config[CPU][NUM_THREADS])

            for alg in config[OPTIMIZATION][CONFIGURATION][RT][MAPPING_ALGORITHMS]:
                exp_config.add_algorithm_from_dict(alg)

            explorer:MappingExploration = MappingExploration(tdg,exp_config)
            success = explorer.explore()

            if not success:
                print('[MULTI-CRIT][EXPLORATION][Warning] \tThe exploration was not able to find a static mapping for the tdg',tdg.id,'.')
                makespans__per_tdg = None
                break
                
            alg_info = explorer.config.algorithms_info[explorer.best_position]
            print('[MULTI-CRIT][EXPLORATION] \tThe best mapping found for the tdg',tdg.id,'is '+alg_info['task2thread']+'+'+alg_info['queue'] + ' with a makespan of '+str(explorer.best_makespan))
            explorer.apply_map()
            makespans__per_tdg[tdg.id] = explorer.best_makespan
            tdg.set_metric("mapping_algorithm",alg_info)

        if not makespans__per_tdg:
            print("[MULTI-CRIT][EXPLORATION][Warning] TDG file",tdg_file_name," will be ignored as no mapping was found")
            continue

        with open(tdg_file_name, 'w') as outfile:
            app_json = app.to_json()
            json.dump(app_json, outfile,indent=2)
    
    print("[MULTI-CRIT][EXPLORATION]Finished exploration")

def optimize(config: Dict):
    base_dir = config[GENERAL][BASE_DIR]
    tdgs_info = config[APP][TDGS_INFO]
    tdgs_info = {t["id"]: t for t in tdgs_info } #converting to a map so it is more easy to access
    signature = config[OPTIMIZATION][POWER_MODES][SIGNATURE]
    remaining_power_modes = config[OPTIMIZATION][POWER_MODES][RT]
    # num_threads = config[NUM_THREADS]
    # deadline = config[CONSTRAINTS][DEADLINE]

    if not remaining_power_modes:
        print("[MULTI-CRIT][WARNING]No configurations (i.e. power_modes) are available for the analysis")
        return
    #remove last power_mode (should be the highest one)
    #we will always go from the highest to lowest
    power_modes = remaining_power_modes.pop()
    
    selected_tdgs = []
    makespans = {}
    for mode in power_modes:
        mode = {x:y for x,y in zip(signature,mode)}
        print("[MULTI-CRIT]Exploring for power mode ",mode,"===")
        makespans_of_mode = {}
        for i,tdg_index in enumerate(mode[TDGS]):
            tdg_config = config[APP][TDGS_FILES][tdg_index]
            tdg_file_name = os.path.join(base_dir,tdg_config[FILE])
            print("[MULTI-CRIT]",str(i+1)+") Exploring TDG file",tdg_file_name,".")

            app: App = App.read_json(tdg_file_name)
            
            makespans__per_tdg = {}
            for tdg in app.tdgs:
                tdg_info = tdgs_info[tdg.id]
                exp_config:ExplorationConfig = ExplorationConfig(tdg_config[CPU][NUM_THREADS],tdg_info[CONSTRAINTS][DEADLINE])
                for alg in config[OPTIMIZATION][CONFIGURATION][RT][MAPPING_ALGORITHMS]:
                    exp_config.add_algorithm_from_dict(alg)

                explorer:MappingExploration = MappingExploration(tdg,exp_config)
                success = explorer.explore()

                if not success:
                    print('[MULTI-CRIT][Warning] The exploration was not able to find a static mapping for the tdg',tdg.id,'providing a makespan lower than the given deadline.')
                    makespans__per_tdg = None
                    break
                    
                alg_info = explorer.config.algorithms_info[explorer.best_position]
                print('[MULTI-CRIT]The best mapping found for the tdg',tdg.id,'is '+alg_info['task2thread']+'+'+alg_info['queue'] + ' with a makespan of '+str(explorer.best_makespan))
                explorer.apply_map()
                makespans__per_tdg[tdg.id] = explorer.best_makespan
                tdg.set_metric("mapping_algorithm",alg_info)

            if not makespans__per_tdg:
                print("[MULTI-CRIT][Warning] TDG file",tdg_file_name," will be ignored as no mapping was found (used mode",mode,")")
                continue

            with open(tdg_file_name, 'w') as outfile:
                app_json = app.to_json()
                json.dump(app_json, outfile,indent=2)
            #we are right now considering the sum of all MAKESPANS to order the favorite MAPPINGS
            #The idea would be to use a metric that calculates the WCRT_UB and order by the lowest
            makespans_of_mode[tdg_index] = sum(makespans__per_tdg.values())


        if not makespans_of_mode:
            print("[MULTI-CRIT][Warning] Could not find any TDG in this mode providing fitting mappings:",mode,")")
            continue
        makespans.update(makespans_of_mode)
    
    print("[MULTI-CRIT]Finished exploration (remaining sets:",len(remaining_power_modes),")")


    ordered_tdgs = [key for key,value in sorted(makespans.items(), key= lambda x: x[1])]    
    config[OPTIMIZATION][OPTIMIZATION_ORDER][RT] = ordered_tdgs

    


    # for index, tdg_config in enumerate(config[TDGS]):
    #     #We only want the tdgs that are using the target power_mode
    #     if tdg_config[ENERGY][POWER_MODE] != power_mode:
    #         continue

    #     print("\tProcessing tdg",tdg_config[FILE])

    #     tdg_file_obj = os.path.join(base_dir,tdg_config[FILE])
    #     tdgs = TDG.read_json(tdg_file_obj,tdg_config[ID],config[MULTIPLE_TDGS])
    #     if not config[MULTIPLE_TDGS]:
    #         tdgs = {'': tdgs}
    #     out_json = {}
    #     makespans__per_tdg = {}
    #     for name,tdg in tdgs.items():
    #         exp_config:ExplorationConfig = ExplorationConfig(num_threads,deadline)
    #         for alg in tdg_config[RT][MAPPING_ALGORITHMS]:
    #             exp_config.add_algorithm_from_dict(alg)

    #         explorer:MappingExploration = MappingExploration(tdg,exp_config)
    #         success = explorer.explore()

    #         if success:
    #             alg_info = explorer.config.algorithms_info[explorer.best_position]
    #             print('\tThe best mapping found for the tdg '+name+' is '+alg_info['task2thread']+'+'+alg_info['queue'] + ' with a makespan of '+str(explorer.best_makespan))
    #             explorer.apply_map()
    #             makespans__per_tdg[name] = explorer.best_makespan
    #             out_json[name] = tdg.to_json()
    #         else:
    #             print('\t[Warning] The exploration was not able to find a static mapping for the tdg '+name+' providing a makespan lower than the given deadline.')
    #             break
        
    #     if not all(t in out_json for t in tdgs):
    #         print("\t[Warning] TDG file",tdg_file_obj," will be ignored as no mapping was found for power_mode",power_mode,".")
    #         continue
        
    #     with open(tdg_file_obj, 'w') as outfile:
    #         if config[MULTIPLE_TDGS]:
    #             json.dump(out_json, outfile,indent=4)
    #         else:
    #             json.dump(out_json[''], outfile,indent=4)

    #     # right now we are ordering the configurations by the "maximum makespan" of the accumulated values of all tdgs
    #     selected_tdgs.append((index, functools.reduce(operator.add,makespans__per_tdg.values())))
    
    # selected_tdgs.sort(key=lambda s: s[1])
    # config[OPTIMIZATION_ORDER][RT] = [s[0] for s in selected_tdgs]