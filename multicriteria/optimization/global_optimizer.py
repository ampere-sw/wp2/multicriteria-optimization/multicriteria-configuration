import os
import random
from typing import Dict
from multicriteria.configs.multi_crit_config_constants import *
from time_predictability.meta_parallel.tdg_schema import *
from time_predictability.meta_parallel.tdg import TDG
from time_predictability.meta_parallel.app import App


def optimize(config: Dict):
    
    #see if any candidate is useful, otherwise just clear selected of RT
    random_tdg = random.choice(config[APP][TDGS_FILES])

    print("[GLOBAL-OPTIMIZER-SYNTH] using "+random_tdg[ID]+" as basis for optimization.")
    num_threads = random_tdg[CPU][NUM_THREADS] if NUM_THREADS in random_tdg[CPU] else 4
    tdg_path = os.path.join(config[GENERAL][BASE_DIR],random_tdg[FILE])
    app: App = App.read_json(tdg_path)
    for tdg in app.tdgs:
        tdg.scheduling_parameters = [ 
            { 
            DEADLINE: random.randint(100,10000), 
            RUNTIME:  random.randint(100,10000),
            PERIOD:   random.randint(100,10000),
            } for i in range(0,num_threads) 
        ]
        for task in tdg.task_list():
            if task.spec != None:
                task.spec = random.choice(["GPU","ARM"])
            if task.spec == None or task.spec == "ARM":
                task.static_thread = random.randint(0,num_threads)

    final_tdg = os.path.join(config[GENERAL][OUTPUT_DIR],"final_tdg.json")
    with open(final_tdg, 'w') as out_file:
        json.dump(app.to_json(),out_file,indent=4)
    config[APP][OPTIMIZED_TDG] = os.path.abspath(final_tdg)