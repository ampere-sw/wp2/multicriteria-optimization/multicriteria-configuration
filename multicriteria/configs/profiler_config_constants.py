#
# Copyright 2022 Instituto Superior de Engenharia do Porto
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# 	http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

GENERAL = "general"
APP = "app"
PLATFORM = "platform"
OPTIMIZATION = "optimization"


# GENERAL {
BASE_DIR = "base_dir"
WORKING_DIR = "working_dir"
ANALYSIS_DIR = "analysis_dir"
FINAL_DIR = "final_dir"
CONFIG_NAME = "config_name"
# } END_GENERAL

# APP{
NAME = "name"
AMALTHEA_MODEL = "amalthea_model"
LATENCY_CONSTRAINT = "latency_constraint"
EVENT_CHAIN_CONSTRAINT = "event_chain_constraint"
TDGS = "tdgs"
TDG_CPP_FILE = "tdg_cpp_file"
# TDGS [
# TDG {
ID = "id"
CONSTRAINTS = "constraints"
# CONSTRAINTS {
DEADLINE = "deadline"
ENERGY_BUDGET = "energy_budget"
# } END_CONSTRAINTS
# } END_TDG
# ] END_TDGS
VARIANTS = "variants"
# OLD_EXECUTABLES = "executables"
# EXECUTABLES [
# EXECUTABLE {
DIR = "dir"
BUILD = "build"
DOTS_DIR = "dots_dir"
TDG_CPP_FILE = "tdg.cpp_file"
EXTRAE_RESULTS_DIR = "extrae_results_dir"
CUDAMPERF_RESULTS_FILE = "cudamperf_results_file"
DYNAMIC_MAPPING = "dynamic_mapping"
STATIC_MAPPING = "static_mapping"
RUN = "run"
ARGS = "args"
EXEC_NAME = "exec_name"
OUTPUT_PREFIX = "output_prefix"
USES_GPU = "uses_gpu"
ITERATIONS = "iterations"
PROFILE="profile"
# } END EXECUTABLE
# ] END EXECUTABLES
# } END_APP

# PLATFORM {
SELECTED = "selected"
PLATFORMS = "platforms"
# PLATFORMS [
# SPECIFIC_PLAT_CONFIG
SETUP = "setup"
CMD = "cmd"
CLEANUP = "cleanup"
CPU = "cpu"
GPU = "gpu"
FPGA = "fpga"
ORDER_BY = "order_by"
GROUP_BY = "group_by"
# COMMAND = "command"
WAIT = "wait"
NUM_THREADS = "num_threads"
PERFORMANCE_COUNTERS = "performance_counters"
PERFORMANCE_COUNTERS = "performance_counters"
# NUM_THREADS { if has to be more complex, i.e., map arg to value
ARG = "arg"
MAP = "map"
# END_NUM_THREADS
# } SPECIFIC_PLAT_CONFIG
# ] END_PLATFORMS
# } END_PLATFORM

# OPTIMIZATION {
RT = "RT"
# {
MAPPING_ALGORITHMS = "mapping_algorithms"
# [
#  {
TASK2THREAD = "task2thread"
QUEUE = "queue"
QUEUE_PER_THREAD = "queue_per_thread"
#  }
# }
ENERGY = "energy"
# {
# TODO
# }
POWER_MODE = "power_mode"
ENERGY_CONSUMPTION = "energy_consumption"
MAX_INST_POWER = "max_inst_power"
# }
# } END_OPTIMIZATION

START="start"
STOP="stop"
STEP="step"