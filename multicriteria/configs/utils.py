
import json
from numbers import Number

def combine(args):
    current_list = [{}]
    for k,v in args:
        new_list = []
        v = arg_to_list(v)
        # if isinstance(v,dict):
        #     #convert range in v to a list of values
        #     v = list(range(v["start"],v["stop"],v["step"]))
        for el in v:
            for t in current_list:
                new = {k2:v2 for k2,v2 in t.items()}
                new[k] = el
                new_list.append(new)
        current_list = new_list
    return current_list  

def arg_to_list(v):
    if isinstance(v,tuple):
        return list(v)
    if isinstance(v,dict):
        #convert range in v to a list of values
        return list(range(v["start"],v["stop"],v["step"]))
    if isinstance(v,str) or isinstance(v,Number):
        return [v]
    return v

def by_key_tuple_sorter(m, sort_by):
    out = []
    for prop in sort_by: #reversed(sort_by):
        device,arg = prop.split(".")
        out.append((arg not in m[device],m[device].get(arg,None)))
    return out


class CompactJSONEncoder(json.JSONEncoder):
    """A JSON Encoder that puts small lists on single lines."""

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.indentation_level = 0

    def encode(self, o):
        """Encode JSON object *o* with respect to single line lists."""

        if isinstance(o, (list, tuple)):
            if self._is_single_line_list(o):
                return "[" + ", ".join(json.dumps(el) for el in o) + "]"
            else:
                self.indentation_level += 1
                output = [self.indent_str + self.encode(el) for el in o]
                self.indentation_level -= 1
                return "[\n" + ",\n".join(output) + "\n" + self.indent_str + "]"

        elif isinstance(o, dict):
            self.indentation_level += 1
            output = [self.indent_str + f"{json.dumps(k)}: {self.encode(v)}" for k, v in o.items()]
            self.indentation_level -= 1
            return "{\n" + ",\n".join(output) + "\n" + self.indent_str + "}"

        else:
            return json.dumps(o)

    def _is_single_line_list(self, o):
        if isinstance(o, (list, tuple)):
            
            return all(self._check_mode_list(el) for el in o) \
                   or (not any(isinstance(el, (list, tuple, dict)) for el in o)\
                   and len(o) <= 2\
                   and len(str(o)) - 2 <= 60)

    #Single line list if list is contains (int | [int+])+ 
    def _check_mode_list(self, el):
        if isinstance(el,(int,str)) or el == None:
            return True
        elif isinstance(el, (list, tuple)):
            return all(isinstance(inel,int) for inel in el)
        return False
    @property
    def indent_str(self) -> str:
        return " " * self.indentation_level * self.indent
    
    def iterencode(self, o, **kwargs):
        """Required to also work with `json.dump`."""
        return self.encode(o)