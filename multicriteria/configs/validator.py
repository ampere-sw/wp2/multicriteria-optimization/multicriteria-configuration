from jsonschema import Validator
class DefaultValidator(Validator):
    def __init__(self,schema, *args, **kwargs):
        super(DefaultValidator,self).__init__(schema,*args,**kwargs)
    def validate_properties(self, properties, instance, schema):
        super(DefaultValidator, self).validate_properties(properties, instance, schema)
        for k, v in properties.iteritems():
            if k not in instance and "default" in v:
                default = v["default"]
                super(DefaultValidator, self)._validate(default, v)
                instance[k] = default