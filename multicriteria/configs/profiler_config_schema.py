#
# Copyright 2022 Instituto Superior de Engenharia do Porto
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# 	http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
from argparse import ArgumentParser
import argparse
from io import TextIOWrapper
import json
from types import SimpleNamespace

from jsonschema import validate

from multicriteria.configs.profiler_config_constants import *
from multicriteria.configs.validator import DefaultValidator


class ProfileSchema:


    def validate_json(json_obj):
        validate(json_obj, ProfileSchema.config_schema())
    
    def config_schema():
        return {
            "type": "object",
            "properties": {
                GENERAL: ProfileSchema.general_config_schema,
                APP: ProfileSchema.app_config_schema,
                PLATFORM: ProfileSchema.platforms_configuration_schema,
                OPTIMIZATION: ProfileSchema.optimization_schema
            }
        }
    
    general_config_schema = {"type": "object",
        "properties": {
            BASE_DIR: {"type": "string"},
            WORKING_DIR: {"type": "string"},
            ANALYSIS_DIR: {"type": "string"},
            FINAL_DIR: {"type": "string"},
            CONFIG_NAME: {"type": "string"}
        }
    }

    _tdg_config_schema = {"type": "object",
        "properties": {
            ID: {"type": "integer"},
            CONSTRAINTS: {"type": "object",
                "properties": {
                    DEADLINE: {"type": "number", "default": -1},
                    ENERGY_BUDGET: {"type": "number", "default": -1},
                }
            }
        }
    }

    _command_schema = {
        "oneOf": [
            {"type": "string"}, #single command
            {"type": "object"}, #single object containing env variables
            {"type": "array", #containing commands and objects with env variables
                "items": {"type": ["string", "object"]}
            }
        ]
    }

    _build_schema = {"type": "object",
        "properties": {
            DYNAMIC_MAPPING: _command_schema,
            STATIC_MAPPING: _command_schema
        }
    }

    _device_variant_schema = {"type": "object",
        "properties": {
            DIR: {"type": "string"},
            BUILD: _build_schema,
            DOTS_DIR: {"type": "string"},
            TDG_CPP_FILE: {"type": "string"},
            SETUP: _command_schema,
            RUN: {"type": "string"},
            EXEC_NAME: {"type": "string"},
            EXTRAE_RESULTS_DIR: {"type": "string"},
            OUTPUT_PREFIX: {"type": "string"},
            ITERATIONS: {"type": "integer"},
            PROFILE: {"type": "boolean", "default": True}
        }
    }

    _variant_schema = {"type": "object",
        "properties": {
            CPU: _device_variant_schema,
            GPU: _device_variant_schema,
            FPGA: _device_variant_schema,
        }
    }

    app_config_schema = {"type": "object",
        "properties": {
            NAME: {"type": "string"},
            AMALTHEA_MODEL: {"type": "string"},
            LATENCY_CONSTRAINT: {"type": "string"},
            EVENT_CHAIN_CONSTRAINT: {"type": "string"},
            VARIANTS:  _variant_schema
        }
    }


    
    _range_type = {"type": "object", #range
        "properties": {
            START: {"type": "number"},
            STOP: {"type": "number"},
            STEP: {"type": "number"}
        }
    }

    _args_schema = {"type": "object",
        "additionalProperties": {
            "anyOf": [
                {
                    "type": "array",
                    "items": {"type": ["number", "string", "boolean", "null"]}
                },
                _range_type
            ]
        }
    }

    _performance_counters_schema = {"type": "object",
        "additionalProperties": {"type": "string"}
    }

    _device_schema = {"type": "object",
        "properties": {
            CMD: _command_schema,
            ARGS: _args_schema,
            WAIT: {"type": "integer"},
            PERFORMANCE_COUNTERS: _performance_counters_schema
        }
    }

    _platform_definition_schema = {"type": "object",
        "additionalProperties": {
            "type": "object",
            "properties": {
                SETUP: _command_schema,
                CLEANUP: _command_schema,
                CPU: {"type": "object",
                    "properties": {
                        CMD: _command_schema,
                        ARGS: _args_schema,
                        WAIT: {"type": "integer"},
                        NUM_THREADS: {
                            "oneOf": [
                                {"type": "integer"}, #constant value throughout the executions
                                {"type": "string"}, #value is the same as the specified argument name
                                {"type": "object", #same as "string", but argument must be mapped to a value
                                    "properties": {
                                        ARG: {"type": "string"}, #the argument name
                                        MAP: {"oneOf": [
                                            {
                                                "type": "array",
                                                "items": {"type": "integer"}
                                            },
                                            _range_type
                                            
                                        ]}
                                        # { "type": "array", #the value to map each element of "argument"
                                        #     "items": { "type": "integer"}
                                        # }
                                    }
                                }
                            ]
                        }
                    }
                },
                GPU: _device_schema,
                FPGA: _device_schema,
                ORDER_BY: {"type": "array",
                    "items": {"type": "string"}
                },
                GROUP_BY: {"type": "string"}
            }
        }
    }
    
    platforms_configuration_schema = {"type": "object",
        "properties": {
            SELECTED: {"type": "string"},
            PLATFORMS: _platform_definition_schema
        }
    }

    _rt_config_schema = {"type": "object",
        "properties": {
            MAPPING_ALGORITHMS: {
                "type": "array",
                "items": {
                    "type": "object",
                    "properties": {
                        TASK2THREAD: {"type": "string"},
                        QUEUE: {"type": "string"},
                        QUEUE_PER_THREAD: {"type": "boolean"},
                    }
                }
            },

        }
    }

    _energy_config_schema = {"type": "object",
        # TODO
    }

    optimization_schema = {"type": "object",
        "properties": {
            RT: _rt_config_schema,
            ENERGY: _energy_config_schema
        }
    }

