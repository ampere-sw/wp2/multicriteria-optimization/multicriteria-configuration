#
# Copyright 2022 Instituto Superior de Engenharia do Porto
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# 	http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
from argparse import ArgumentParser
import argparse
from io import TextIOWrapper
import json
from types import SimpleNamespace


NUM_THREADS = "num_threads"
MULTIPLE_TDGS = "multiple_tdgs"
APP_NAME = "app_name"
BASE_DIR = "base_dir"
OUTPUT_DIR = "output_dir"

CONSTRAINTS = "constraints"
#{
DEADLINE = "deadline"
ENERGY_BUDGET = "energy_budget"
#}

SELECTED = "selected"
#{
RT = "RT"
ENERGY = "energy"
#}

POWER_MODES = "power_modes"
#{
# RT 
# ENERGY
#}

OPTIMIZATION_ORDER = "optimization_order"
#{
#RT
#ENERGY
#}

TDGS = "tdgs"
#[
# {
FILE = "file"
ID = "id"
#RT
#  {
MAPPING_ALGORITHMS = "mapping_algorithms"
#   [
#     {
TASK2THREAD = "task2thread"
QUEUE = "queue"
QUEUE_PER_THREAD = "queue_per_thread"
ACTIVE = "active"
MAKESPAN = "makespan"
#     }
#   ],
BEST_MAPPING = "best_mapping"
#   {
ALGORITHM = "algorithm"
#makespan
#   },
#  },
#  {
POWER_MODE = "power_mode"
ENERGY_CONSUMPTION = "energy_consumption"
MAX_INST_POWER = "max_inst_power"
#  }
# }
#]
