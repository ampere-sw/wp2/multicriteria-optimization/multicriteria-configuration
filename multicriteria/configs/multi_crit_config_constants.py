#
# Copyright 2022 Instituto Superior de Engenharia do Porto
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# 	http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

GENERAL = "general"
APP = "app"
OPTIMIZATION = "optimization"
COMPILATION = "compilation"

#GENERAL {
BASE_DIR = "base_dir"
WORKING_DIR = "working_dir"
OUTPUT_DIR = "output_dir"
CLEAR_DIRS = "clear_dirs"
#} END_GENERAL 

#APP{
NAME = "name"
AMALTHEA_MODEL = "amalthea_model"
OPTIMIZED_TDG = "optimized_tdg"
#TDGS_INFO [
#TDG {
ID = "id"
CONSTRAINTS = "constraints"
#CONSTRAINTS {
DEADLINE = "deadline"
ENERGY_BUDGET = "energy_budget"
#} END_CONSTRAINTS
#} END_TDG
#] END_TDGS_INFO
TDGS_FILES = "tdgs_files"
#TDGS_FILES [
#TDGS_FILE {
FILE = "file"
#ID
CPU = "cpu"
#CPU {
NUM_THREADS = "num_threads"
    #...
#} END_CPU
GPU = "gpu"
#GPU {
    #...
#} END_GPU
VARIANT = "variant"
PROFILED= "profiled"
#} END_TDGS_FILE
#] END_TDGS_FILES


# OPTIMIZATION {
POWER_MODES = "power_modes"

OPTIMIZATION_ORDER = "optimization_order"
SELECTED = "selected"
CONFIGURATION = "configuration"

SIGNATURE = "signature"
TDGS = "tdgs"
RT = "RT"
ENERGY = "energy"

MAPPING_ALGORITHMS = "mapping_algorithms"
# [
#  {
TASK2THREAD = "task2thread"
QUEUE = "queue"
QUEUE_PER_THREAD = "queue_per_thread"
#  }
# }
#ENERGY = "energy"
#{
# TODO
#}
ENERGY_CONSUMPTION = "energy_consumption"
MAX_INST_POWER = "max_inst_power"
#}
#} END_OPTIMIZATION

#COMPILATION
CODE_BASE_DIR = "code_base_dir"
TDG_CPP_FILE = "tdg.cpp_file"
VARIANTS = "variants"
DIR = "dir"
BUILD = "build"
EXTRAE_RESULTS_DIR = "extrae_results_dir"
DYNAMIC_MAPPING = "dynamic_mapping"
STATIC_MAPPING = "static_mapping"
TIMEOUT = "timeout"
RUN = "run"
ARGS = "args"
EXEC_NAME = "exec_name"
OUTPUT_PREFIX = "output_prefix"
USES_GPU = "uses_gpu"
ITERATIONS = "iterations"

PLATFORM = "platform"
SETUP = "setup"
CPU = "cpu"
CMD = "cmd"
WAIT = "wait"
PERFORMANCE_COUNTERS = "performance_counters"
GPU = "gpu"
CLEANUP = "cleanup"

START="start"
STOP="stop"
STEP="step"