#
# Copyright 2022 Instituto Superior de Engenharia do Porto
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# 	http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
from argparse import ArgumentParser
import argparse
from io import TextIOWrapper
import json
from types import SimpleNamespace

from jsonschema import validate

from multicriteria.configs.multi_crit_config_constants import *


class MulticriteriaConfigSchema:


    def validate_json(json_obj):
        validate(json_obj, MulticriteriaConfigSchema.config_schema())
    
    def config_schema():
        return {
            "type": "object",
            "properties": {
                GENERAL: MulticriteriaConfigSchema.general_config_schema,
                APP: MulticriteriaConfigSchema.app_config_schema,
                OPTIMIZATION: MulticriteriaConfigSchema.optimization_schema,
                COMPILATION: MulticriteriaConfigSchema.compilation_schema
            }
        }
    
    general_config_schema = {"type": "object",
        "properties": {
            BASE_DIR: {"type": "string"},
            WORKING_DIR: {"type": "string"},
            OUTPUT_DIR: {"type": "string"},
        }
    }

    _tdg_config_schema = {"type": "object",
        "properties": {
            ID: {"type": "integer"},
            CONSTRAINTS: {"type": "object",
                "properties": {
                    DEADLINE: {"type": "number", "default": -1},
                    ENERGY_BUDGET: {"type": "number", "default": -1},
                }
            }
        }
    }

    _tdgs_file_schema = {"type": "object",
        "properties": {
            FILE: {"type": "string"},
            ID: {"type": "string"},
            CPU: {"type": "object",
                "properties": {
                    NUM_THREADS: {"type": "integer"}
                }
            },
            GPU: {"type": "object"},
            VARIANT: {"type": "number"},
            PROFILED: {"type": "boolean", "default": True},
        }
    }

    _command_schema = {
        "oneOf": [
            {"type": "string"}, #single command
            {"type": "object"}, #single object containing env variables
            {"type": "array", #containing commands and objects with env variables
                "items": {"type": ["string", "object"]}
            }
        ]
    }

    _build_schema = {"type": "object",
        "properties": {
            DYNAMIC_MAPPING: _command_schema,
            STATIC_MAPPING: _command_schema
        }
    }

    _device_variant_schema = {"type": "object",
        "properties": {
            DIR: {"type": "string"},
            BUILD: _build_schema,
            TDG_CPP_FILE: {"type": "string"},
            SETUP: _command_schema,
            RUN: {"type": "string"},
            EXEC_NAME: {"type": "string"},
            EXTRAE_RESULTS_DIR: {"type": "string"},
            OUTPUT_PREFIX: {"type": "string"},
            ITERATIONS: {"type": "integer"},
        }
    }

    _variant_schema = {"type": "object",
        "properties": {
            CPU: _device_variant_schema,
            GPU: _device_variant_schema
        }
    }

    app_config_schema = {"type": "object",
        "properties": {
            NAME: {"type": "string"},
            AMALTHEA_MODEL: {"type": "string"},
            VARIANTS:  _variant_schema
        }
    }

    _array_of_ints = {"type": "array",
        # "items":{"type": "integer"}
    }

    _mode_tuple = {"type": "array", 
        "items": {
            "type": "array", 
            "items": {
                "anyOf": [
                    {"type": "integer"},
                    {"type": "null"},
                    _array_of_ints
            ]}
        }
    }

    _power_mode_schema = {"type": "object",
        "properties": {
            RT: _mode_tuple,
            ENERGY:  _mode_tuple
        }
    }
    
    _optimization_order_schema = {"type": "object",
        "properties": {
            RT: {"type": "array",
                "items": {"type": "integer"}
            },
            ENERGY: {"type": "array",
                "items": {"type": "integer"}
            }
        }
    }
    _selected_opt_schema =  {"type": "object",
        "properties": {
            RT: {"type": "integer", "default": "-1"},
            ENERGY: {"type": "integer", "default": "-1"},
        }
    }

    _rt_config_schema = {"type": "object",
        "properties": {
            MAPPING_ALGORITHMS: {
                "type": "array",
                "items": {
                    "type": "object",
                    "properties": {
                        TASK2THREAD: {"type": "string"},
                        QUEUE: {"type": "string"},
                        QUEUE_PER_THREAD: {"type": "boolean"},
                    }
                }
            },

        }
    }

    _energy_config_schema = {"type": "object",
        # TODO
    }

    _opt_config_schema = {"type": "object",
        "properties": {
            RT: _rt_config_schema,
            ENERGY: _energy_config_schema
        }
    }

    optimization_schema = {"type": "object",
        "properties": {
           POWER_MODES: _power_mode_schema,
           OPTIMIZATION_ORDER: _optimization_order_schema,
           SELECTED: _selected_opt_schema,
           CONFIGURATION: _opt_config_schema
        }
    }

    _command_schema = {
        "oneOf": [
            {"type": "string"}, #single command
            {"type": "object"}, #single object containing env variables
            {"type": "array", #containing commands and objects with env variables
                "items": {"type": ["string", "object"]}
            }
        ]
    }

    _build_schema = {"type": "object",
        "properties": {
            DYNAMIC_MAPPING: _command_schema,
            STATIC_MAPPING: _command_schema
        }
    }

    _variant_schema = {"type": "object",
        "properties": {
            DIR: {"type": "string"},
            BUILD: _build_schema,
            RUN: {"type": "string"},
            EXEC_NAME: {"type": "string"},
            OUTPUT_PREFIX: {"type": "string"},
            ITERATIONS: {"type": "integer"},
            USES_GPU: {"type": "boolean", "default": "true"},
        }
    }

    _range_type = {"type": "object", #range
        "properties": {
            START: {"type": "number"},
            STOP: {"type": "number"},
            STEP: {"type": "number"}
        }
    }

    _args_schema = {"type": "object",
        "additionalProperties": {
            "anyOf": [
                {
                    "type": "array",
                    "items": {"type": ["number", "string", "boolean", "null"]}
                },
                _range_type
            ]
        }
    }

    _performance_counters_schema = {"type": "object",
        "additionalProperties": {"type": "string"}
    }

    _platform_definition_schema = {"type": "object",
        "properties": {
            NAME: {"type": "string"},
            SETUP: _command_schema,
            CPU: {"type": "object",
                "properties": {
                    CMD: _command_schema,
                    WAIT: {"type": "integer"},
                    PERFORMANCE_COUNTERS: _performance_counters_schema
                }
            },
            GPU: {"type": "object",
                "properties": {
                    CMD: _command_schema,
                    WAIT: {"type": "integer"},
                    PERFORMANCE_COUNTERS: _performance_counters_schema
                }
            },
            CLEANUP: _command_schema
        }
    }
    compilation_schema = {"type": "object",
        "properties": {
           CODE_BASE_DIR: {"type": "string"},
           VARIANTS:  _variant_schema,
           PLATFORM: _platform_definition_schema
        }
    }

