#
# Copyright 2022 Instituto Superior de Engenharia do Porto
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# 	http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import json
import os
import shutil
from typing import Dict
from multicriteria.analysis.utils import clear_results, clear_metrics
from multicriteria.configs.multi_crit_config_schema import *
from multicriteria.configs.profiler_config_schema import BASE_DIR, CONFIG_NAME, WORKING_DIR, GENERAL
from multicriteria.generation.final_version import generate
from multicriteria.optimization.global_optimizer import optimize
# from multicriteria.configs.intermediateconfig import FILE, POWER_MODES, OUTPUT_DIR, RT, SELECTED, TDGS

from multicriteria.profiling.profiler import profile
from multicriteria.profiling.reprofiler import profile_static_mapping
from multicriteria.analysis.wcet_analyser import analyse as timing_analysis
from multicriteria.analysis.power_modeling import estimate_power
from multicriteria.analysis.energy_analyser import analyse as analyse_for_energy
from multicriteria.optimization.rt_optimizer import explore_mappings
from multicriteria.utils.performance_utils import *

def copy_file(source,destiny):
    destiny_dir = os.path.dirname(destiny)
    if not os.path.exists(destiny_dir):
        os.makedirs(destiny_dir)

    print("[MULTI-CRIT]Copying file to:",destiny)
    shutil.copy(source,destiny)

def copy_tdg(config,tdg_file_info,dir):
    working_tdg_path = os.path.join(config[GENERAL][BASE_DIR],tdg_file_info[FILE])
    final_tdg_path = os.path.join(config[GENERAL][OUTPUT_DIR],dir)
    final_tdg_path = os.path.join(final_tdg_path,tdg_file_info[FILE])
    copy_file(working_tdg_path,final_tdg_path)

def save_results(config,table,filtered_table,best_rt,best_en):
    header = table[0]
    tdg_info_map = {tdg_info[ID]: tdg_info for tdg_info in config[APP][TDGS_FILES]}

    rt_id = value_of(best_rt,VARIANT,header)
    print("[MULTI-CRIT]TDG considered for 'RT':",rt_id)
    copy_tdg(config,tdg_info_map[rt_id],"RT_OPTIMIZED")

    en_id = value_of(best_en,VARIANT,header)
    print("[MULTI-CRIT]TDG considered for 'energy':",en_id)
    # next(tdg_file[ID] == en_id for tdg_file in config[APP][TDGS_FILES])
    copy_tdg(config,tdg_info_map[en_id],"ENERGY_OPTIMIZED")

    tdg_ids = set([value_of(line,VARIANT,header) for line in filtered_table[1:]])
    print("[MULTI-CRIT]Saving other acceptible TDGs in 'OTHER' folder")
    for tdg_id in tdg_ids:
        copy_tdg(config,tdg_info_map[tdg_id],"OTHER")
    print("[MULTI-CRIT]Saving results table in csv files")
    out_csv = os.path.join(config[GENERAL][OUTPUT_DIR],'results.csv')
    with open(out_csv, 'w') as csv_file:
        csv_file.write("\n".join( ",".join(map(str,line)) for line in table ))


def manage_from_base(baseconfig_file: str):
    baseconfig: Dict
    with open(baseconfig_file, 'r') as cfgfile:
        baseconfig = json.load(cfgfile)
    profile(baseconfig)
    interConfig = os.path.join(baseconfig[GENERAL][WORKING_DIR],baseconfig[GENERAL][CONFIG_NAME])
    manage_from_intermediate(interConfig)
    return interConfig

def manage_from_intermediate(interconfig_file: str):  
    interconfig_file = os.path.abspath(interconfig_file)
    config: Dict

    def load_config_file():
        nonlocal config
        print("[MULTI-CRIT]Loading config file from ",interconfig_file)
        with open(interconfig_file, 'r') as cfgfile:
            config = json.load(cfgfile)
        MulticriteriaConfigSchema.validate_json(config)

    def update_config_file():
        print("[MULTI-CRIT]Saving config file as ",interconfig_file)
        with open(interconfig_file, 'w') as cfgfile:
            json.dump(config,cfgfile,indent=4)

    load_config_file()
    
    #0: copy files to working dir if necessary
    if WORKING_DIR in config[GENERAL] and config[GENERAL][WORKING_DIR] != config[GENERAL][BASE_DIR]:
        work_dir = os.path.abspath(config[GENERAL][WORKING_DIR])
        print("[MULTI-CRIT]Copying files from base dir",config[GENERAL][BASE_DIR])
        print("[MULTI-CRIT]             to working dir",work_dir)
        if not os.path.exists(work_dir):
            print("[MULTI-CRIT]..Making working dir: ",work_dir)
            os.makedirs(work_dir,exist_ok=True)
        for tdg in config[APP][TDGS_FILES]:
            origin = os.path.join(config[GENERAL][BASE_DIR],tdg[FILE])
            destiny = os.path.join(work_dir,tdg[FILE])
            destiny = os.path.abspath(destiny)
            dest_dir = os.path.dirname(destiny)
            # dest_dir = os.path.abspath(dest_dir)
            if not os.path.exists(dest_dir):
                print("[MULTI-CRIT]..Making sub dir: ",dest_dir)
                os.makedirs(dest_dir,exist_ok=True)
            print("[MULTI-CRIT]...Copying file:",origin)
            print("[MULTI-CRIT].............to:",destiny)
            shutil.copy(origin,destiny)
        config[GENERAL][BASE_DIR] = work_dir
        config[GENERAL].pop(WORKING_DIR)
        config_file_name = os.path.basename(interconfig_file)
        interconfig_file = os.path.join(work_dir,config_file_name)
        update_config_file()
  
    final_dir = config[GENERAL][OUTPUT_DIR]
    os.makedirs(final_dir,exist_ok=True)

    #1: calculate timing metrics
    print("[MULTI-CRIT]Phase 1. Adding WCETs and PMCs metrics")    
    timing_analysis(config)
    
    #2: clear results in tdg files (from this point on they are only adding overhead when processing the json files)
    print("[MULTI-CRIT]Phase 2. Clearing profiling results")    
    clear_results(config)
    #power measurement
    estimate_power(config)

    # optimize(config)
    # update_config_file()

    # generate(config)
    return interconfig_file

def single_criterion_optimization(interconfig_file: str):
    interconfig_file = os.path.abspath(interconfig_file)
    config: Dict

    def load_config_file():
        nonlocal config
        print("[MULTI-CRIT]Loading config file from ",interconfig_file)
        with open(interconfig_file, 'r') as cfgfile:
            config = json.load(cfgfile)
        MulticriteriaConfigSchema.validate_json(config)

    def update_config_file():
        print("[MULTI-CRIT]Saving config file as ",interconfig_file)
        with open(interconfig_file, 'w') as cfgfile:
            json.dump(config,cfgfile,indent=4)

    load_config_file()
    
    #3: Explore Mappings and set static_mapping in each tdg file
    print("[MULTI-CRIT]Phase 4. Running static mapping exploration")    
    explore_mappings(config)
    
    # #3.1: Clear metrics as the current ones reflect dynamic mappings
    # print("[MULTI-CRIT]Phase 5. Clearing metrics to gather new results")
    # clear_metrics(config)

    # #4 reprofile tdgs
    # print("[MULTI-CRIT]Phase 6. Running profiler with static mapping")
    # profile_static_mapping(config)

    # #7: calculate new timing metrics
    # print("[MULTI-CRIT]Phase 7. Adding WCETs and PMCs metrics (now with static mapping)") 
    # timing_analysis(config,makespan_from_static_mapping=True)
    print("[MULTI-CRIT]Phase 10. Building table of results")
    table = build_performance_table(config)
    print("\n".join(",".join(str(el) for el in line) for line in table))
    
    
    # print("\n\n")
    # print("[MULTI-CRIT]Phase 11. Filtering table with unwanted lines")
    # filtered_table = filter_table(table)
    # print("\n".join(",".join(str(el) for el in line) for line in filtered_table))
    filtered_table = table
    # print("\n\n")

    # if len(filtered_table) == 1: #means only the header is there!
    #     print("[MULTI-CRIT]Could not find any configuration that suffices for both energy and RT. Optimization will stop.")
    #     return

    print("[MULTI-CRIT]Phase 12. Selecting TDGs for each criteria")
    best_rt,best_en = optimize_per_criteria(filtered_table)

    print("Best Response Time:",best_rt)
    print("Lowest Energy Consumption:",best_en)

    print("[MULTI-CRIT]Phase 13. Saving final results and the selected TDGs in the final directory")
    save_results(config,table,filtered_table,best_rt,best_en)


def format_before_sssa(config):
    #3: Explore Mappings and set static_mapping in each tdg file
    print("[MULTI-CRIT]Phase 4. Running static mapping exploration")    
    explore_mappings(config)
    
    #3.1: Clear metrics as the current ones reflect dynamic mappings
    print("[MULTI-CRIT]Phase 5. Clearing metrics to gather new results")
    clear_metrics(config)

    #4 reprofile tdgs
    print("[MULTI-CRIT]Phase 6. Running profiler with static mapping")
    profile_static_mapping(config)

    #7: calculate new timing metrics
    print("[MULTI-CRIT]Phase 7. Adding WCETs and PMCs metrics (now with static mapping)") 
    timing_analysis(config,makespan_from_static_mapping=True)
    
    #2: clear results in tdg files (from this point on they are only adding overhead when processing the json files)
    print("[MULTI-CRIT]Phase 8. Clearing profiling results")
    clear_results(config)

    print("[MULTI-CRIT]Phase 9. Estimating power per task")
    estimate_power(config)

    print("[MULTI-CRIT]Phase 10. Building table of results")
    table = build_performance_table(config)
    print("\n".join(",".join(str(el) for el in line) for line in table))
    
    
    print("\n\n")
    print("[MULTI-CRIT]Phase 11. Filtering table with unwanted lines")
    filtered_table = filter_table(table)
    print("\n".join(",".join(str(el) for el in line) for line in filtered_table))
    
    print("\n\n")

    if len(filtered_table) == 1: #means only the header is there!
        print("[MULTI-CRIT]Could not find any configuration that suffices for both energy and RT. Optimization will stop.")
        return

    print("[MULTI-CRIT]Phase 12. Selecting TDGs for each criteria")
    best_rt,best_en = optimize_per_criteria(filtered_table)

    print("Best Response Time:",best_rt)
    print("Lowest Energy Consumption:",best_en)

    print("[MULTI-CRIT]Phase 13. Saving final results and the selected TDGs in the final directory")
    save_results(config,table,filtered_table,best_rt,best_en)



    
    # #3. while no configuration is selected for RT, and there is still power_modes left to explore
    # config_signature = [c for c in config[OPTIMIZATION][POWER_MODES][SIGNATURE] if c != "tdgs"]
    # print("[MULTI-CRIT]Phase 3. RT Optimization - Exploring TDGs by configuration: "+ str(config_signature))
    # while config[OPTIMIZATION][SELECTED][RT] == -1 and config[OPTIMIZATION][POWER_MODES][RT]:

    #     #3.1 search fitting mapping for the given tdgs, with the next highest power_mode
    #     optimize_for_rt(config)
    #     update_config_file()

    #     #3.2 send to energy analyzer to see if it is within budget
    #     #TODO
    #     analyse_for_energy(interconfig_file) #this is an example of a function that is going to read directly from the config file
    #     load_config_file()

    # if config[OPTIMIZATION][SELECTED][RT] == -1:
    #     print("[MULTI-CRIT][WARNING] Unable to find a suitable configuration for 'RT'. Aborting RT optimization.")
    # else:
    #     tdg = config[APP][TDGS_FILES][config[OPTIMIZATION][SELECTED][RT]]
    #     print("[MULTI-CRIT]TDG considered for 'RT':",tdg[FILE])
    #     #save considered script in final config folder
    #     working_tdg_path = os.path.join(config[GENERAL][BASE_DIR],tdg[FILE])
    #     final_tdg_path = os.path.join(config[GENERAL][OUTPUT_DIR],"RT_OPTIMIZED/"+tdg[FILE])
    #     final_tdg_dir = os.path.dirname(final_tdg_path)
    #     if not os.path.exists(final_tdg_dir):
    #         os.makedirs(final_tdg_dir)

    #     print("[MULTI-CRIT]Copying file to:",final_tdg_path)
    #     shutil.copy(working_tdg_path,final_tdg_path)

    #TODO: add other optimization tools
    #TODO: energy opt. tool
    #TODO: Global Optimizer(SSSA)
    
    #TODO: Sched Analyser(BSC)
